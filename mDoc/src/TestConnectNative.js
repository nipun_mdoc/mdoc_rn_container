import {NativeModules, Platform} from 'react-native';

const testConnectNative = NativeModules.TestConnectNative;

const TestConnectNative = {
    sendMessage: msg => {
        testConnectNative.sendMessageToNative(msg);
    },

    sendCallback: callback => {
        testConnectNative.sendCallbackToNative(callback);
    },

    exitRN: tag => {
        if (Platform.OS === 'ios') {
            testConnectNative.dismissViewController(tag);
        }
    },

    goToNative: tag => {
        if (Platform.OS === 'ios') {
            testConnectNative.goToSecondViewController(tag);
        }
    },
};

export default TestConnectNative;

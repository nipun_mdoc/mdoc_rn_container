package de.mdoc.activities

import android.content.IntentFilter
import android.os.Bundle
import de.mdoc.R
import de.mdoc.constants.MdocConstants

class MdocActivity: MainActivity() {

    override fun getResourceId(): Int {
        return R.layout.activity_mdoc
    }

    override fun init(savedInstanceState: Bundle?) {
        handleClickListeners()
        val filter = IntentFilter()
        filter.addAction(MdocConstants.APPOINTMENTS_NUMBER)
        registerReceiver(notifyReceiver, filter)

        registerReceiver(notifyReceiver, IntentFilter(MdocConstants.NOTIFICATION_FILTER))
        getNotifications()
        if (!resources.getBoolean(R.bool.phone)) {
            if (savedInstanceState != null) {
                isOpenMenu = savedInstanceState.getBoolean(IS_OPEN_MENU)
            }

            if (isOpenMenu) {
                openMenu()
            } else {
                collapseMenu()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(notifyReceiver)
    }
}

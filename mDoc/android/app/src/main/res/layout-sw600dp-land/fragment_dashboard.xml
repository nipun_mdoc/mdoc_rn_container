<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>
        <import type="android.view.View"/>
        <import type="de.mdoc.util.MdocAppHelper"/>
        <import type="de.mdoc.constants.MdocConstants"/>

        <variable
            name="dashboardViewModel"
            type="de.mdoc.modules.dashboard.DashboardViewModel"/>
        <variable
            name="welcomeWidgetViewModel"
            type="de.mdoc.modules.dashboard.welcome_widget.WelcomeWidgetViewModel"/>
        <variable
            name="welcomeWidgetHandler"
            type="de.mdoc.modules.dashboard.welcome_widget.WelcomeWidgetHandler" />
        <variable
            name="stayViewModel"
            type="de.mdoc.modules.my_stay.widget.MyStayWidgetViewModel"/>
        <variable
            name="appointmentViewModel"
            type="de.mdoc.modules.appointments.AppointmentWidgetViewModel"/>

        <variable
            name="appointmentItemCallback"
            type="de.mdoc.modules.appointments.adapters.DayAppointmentAdapter.AppointmentDayCallback" />

        <variable
            name="mealViewModel"
            type="de.mdoc.modules.meal_plan.view_model.MealWidgetViewModel"/>

        <variable
            name="mealPlanWidgetHandler"
            type="de.mdoc.modules.meal_plan.MealPlanWidgetHandler" />

        <variable
            name="journeyWidgetViewModel"
            type="de.mdoc.modules.patient_journey.PatientJourneyWidgetViewModel"/>

        <variable
            name="patientJourneyWidgetHandler"
            type="de.mdoc.modules.patient_journey.PatientJourneyHandler" />

        <variable
            name="conversationsWidgetViewmodel"
            type="de.mdoc.modules.messages.widget.ConversationsWidgetViewModel"/>

        <variable
            name="conversationHandler"
            type="de.mdoc.modules.messages.widget.ConversationsWidgetHandler" />

        <variable
            name="isMyStayWidgetEnabled"
            type="Boolean" />

        <variable
            name="medicationWidgetViewModel"
            type="de.mdoc.modules.medications.medication_widget.MedicationWidgetViewModel" />

        <variable
            name="medicationsViewModel"
            type="de.mdoc.modules.medications.MedicationsViewModel"/>

        <variable
            name="medicationsHandler"
            type="de.mdoc.modules.medications.MedicationsHandler" />

        <variable
            name="goalsWidgetViewModel"
            type="de.mdoc.modules.mental_goals.goals_widget.GoalsWidgetViewModel"/>

        <variable
            name="goalsWidgetHandler"
            type="de.mdoc.modules.mental_goals.goals_widget.GoalsWidgetHandler" />

        <variable
            name="vitalsWidgetViewModel"
            type="de.mdoc.modules.vitals.vitals_widget.VitalsWidgetViewModel"/>

        <variable
            name="vitalsWidgetHandler"
            type="de.mdoc.modules.vitals.vitals_widget.VitalsWidgetHandler" />

        <variable
            name="newsWidgetViewModel"
            type="de.mdoc.modules.messages.news_widget.NewsWidgetViewModel"/>
        <variable
            name="newsWidgetHandler"
            type="de.mdoc.modules.messages.news_widget.NewsWidgetHandler" />
        <variable
            name="covid19WidgetViewModel"
            type="de.mdoc.modules.covid19.widget.Covid19WidgetViewModel"/>

        <variable
            name="covid19WidgetHandler"
            type="de.mdoc.modules.covid19.widget.Covid19WidgetHandler" />

        <variable
            name="shouldDisplayWidgets"
            type="Boolean" />

        <variable
            name="configHandler"
            type="de.mdoc.pojo.ConfigurationData" />


    </data>

    <androidx.core.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <androidx.constraintlayout.widget.ConstraintLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <View
                android:layout_width="match_parent"
                android:layout_height="160dp"
                android:layout_marginTop="80dp"
                android:background="@drawable/dashboard_header_gradient"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent" />

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:orientation="vertical"
                app:layout_constraintBottom_toBottomOf="parent"
                app:layout_constraintEnd_toEndOf="parent"
                app:layout_constraintStart_toStartOf="parent"
                app:layout_constraintTop_toTopOf="parent">

                <View
                    android:id="@+id/dummyForFocus"
                    android:layout_width="wrap_content"
                    android:layout_height="1dp"
                    android:focusable="true"
                    android:focusableInTouchMode="true" />

                <include
                    android:id="@+id/headerWidget"
                    layout="@layout/view_dashboard_header" />

                <include
                    android:id="@+id/pairRl"
                    layout="@layout/widget_pair_device"
                    android:layout_width="match_parent"
                    android:layout_height="37dp"
                    android:layout_marginStart="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:background="@drawable/pair_rounded"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_PAIR, MdocConstants.IS_PAIR_BUTTON_VISIBLE)?View.VISIBLE:View.GONE}" />

                <include
                    android:id="@+id/welcomeWidget"
                    layout="@layout/welcome_message"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="15dp"
                    android:layout_marginEnd="15dp"
                    android:layout_marginBottom="5dp"
                    android:visibility="@{welcomeWidgetViewModel.shouldShowContent ? View.VISIBLE:View.GONE}"
                    app:welcomeWidgetHandler="@{welcomeWidgetHandler}"
                    app:welcomeWidgetViewModel="@{welcomeWidgetViewModel}"
                    tools:visibility="visible" />

            <LinearLayout
                android:id="@+id/widgetsContainer"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="vertical"
                android:visibility="@{shouldDisplayWidgets ? View.VISIBLE:View.GONE}">

                <include
                    android:id="@+id/airAndPollenWidget"
                    layout="@layout/dashboard_widget_air_and_pollen"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginTop="16dp"
                    android:layout_marginEnd="16dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_AIR_POLLEN, MdocConstants.widget)?View.VISIBLE:View.GONE}"/>

                <include
                    android:id="@+id/covidHashtag"
                    layout="@layout/layout_covid_hashtag"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginTop="10dp"
                    android:layout_marginBottom="10dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_COVID_HASHTAG, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/appointmentWidget"
                    layout="@layout/layout_appointment_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:appointmentViewModel="@{appointmentViewModel}"
                    app:appointmentItemCallback="@{appointmentItemCallback}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/covid19Widget"
                    layout="@layout/layout_covid19_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{(configHandler.getWidgetVisibility(MdocConstants.MODULE_COVID19, MdocConstants.widget))?View.VISIBLE:View.GONE}"
                    app:covid19WidgetHandler="@{covid19WidgetHandler}"
                    app:covid19WidgetViewModel="@{covid19WidgetViewModel}"
                    tools:visibility="visible" />


                <include
                    android:id="@+id/mediaWidget"
                    layout="@layout/layout_media_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MEDIA, MdocConstants.widget)?View.VISIBLE:View.GONE}" />

                <include
                    layout="@layout/layout_postbox_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MESSAGE, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:conversationHandler="@{conversationHandler}"
                    app:conversationsWidgetViewmodel="@{conversationsWidgetViewmodel}"
                    tools:visibility="gone" />

                <include
                    android:id="@+id/newsWidget"
                    layout="@layout/layout_news_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_CONSENT, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:newsWidgetHandler="@{newsWidgetHandler}"
                    app:newsWidgetViewModel="@{newsWidgetViewModel}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/questionnaireLl"
                    layout="@layout/dashboard_widget_questionnaire"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="@dimen/size_16dp"
                    android:layout_marginTop="10dp"
                    android:layout_marginEnd="@dimen/size_16dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_QUESTIONNAIRES, MdocConstants.widget)?View.VISIBLE:View.GONE}" />

                <include
                    android:id="@+id/medicationWidget"
                    layout="@layout/layout_medication_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MEDICATION_PLAN, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:medicationWidgetViewModel="@{medicationWidgetViewModel}"
                    app:medicationsViewModel="@{medicationsViewModel}"
                    app:medicationsHandler="@{medicationsHandler}"
                    tools:visibility="visible" />
                <include
                    android:id="@+id/vitalsWidget"
                    layout="@layout/layout_vitals_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="5dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_VITALS, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:vitalsWidgetHandler="@{vitalsWidgetHandler}"
                    app:vitalsWidgetViewModel="@{vitalsWidgetViewModel}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/goalsWidget"
                    layout="@layout/layout_goals_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="5dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MENTAL_GOALS, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:goalsWidgetHandler="@{goalsWidgetHandler}"
                    app:goalsWidgetViewModel="@{goalsWidgetViewModel}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/mealPlanWidget"
                    layout="@layout/layout_meal_plan_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MEALS, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:mealViewModel="@{mealViewModel}"
                    app:handler="@{mealPlanWidgetHandler}"/>

                <include
                    android:id="@+id/myStayWidget"
                    layout="@layout/layout_my_stay"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MY_STAY, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:stayViewModel="@{stayViewModel}"
                    tools:visibility="visible" />

                <include
                    android:id="@+id/myClinicWidget"
                    layout="@layout/layout_my_clinic"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginStart="16dp"
                    android:layout_marginEnd="16dp"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_MY_CLINIC, MdocConstants.widget)?View.VISIBLE:View.GONE}" />

                <include
                    android:id="@+id/patientJourneyWidget"
                    layout="@layout/layout_patient_journey_widget"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:visibility="@{configHandler.getWidgetVisibility(MdocConstants.MODULE_PATIENT_JOURNEY, MdocConstants.widget)?View.VISIBLE:View.GONE}"
                    app:journeyWidgetViewModel="@{journeyWidgetViewModel}"
                    app:patientJourneyWidgetHandler="@{patientJourneyWidgetHandler}" />
            </LinearLayout>

                <LinearLayout
                    android:id="@+id/checklistLl"
                    android:layout_width="match_parent"
                    android:layout_height="match_parent"
                    android:layout_marginStart="@dimen/size_16dp"
                    android:layout_marginEnd="@dimen/size_16dp"
                    android:layout_marginBottom="@dimen/size_16dp"
                    android:background="@drawable/platinum_rounded_rectangle"
                    android:orientation="vertical"
                    android:visibility="gone">

                    <RelativeLayout
                        android:id="@+id/checklistRl"
                        android:layout_width="match_parent"
                        android:layout_height="@dimen/size_46dp">

                        <ImageView
                            android:id="@+id/checklistIconTv"
                            android:layout_width="@dimen/size_18dp"
                            android:layout_height="wrap_content"
                            android:layout_centerVertical="true"
                            android:layout_marginStart="@dimen/size_16dp"
                            android:src="@drawable/questionnaire_gray"
                            tools:ignore="ContentDescription" />

                        <TextView
                            android:id="@+id/checklistPhoneTv"
                            style="@style/PrimaryFontRegular"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_centerVertical="true"
                            android:layout_marginStart="@dimen/size_16dp"
                            android:layout_marginTop="@dimen/size_6dp"
                            android:layout_toEndOf="@+id/checklistIconTv"
                            android:text="@string/checklist"
                            android:textAllCaps="false"
                            android:textSize="16sp" />

                    </RelativeLayout>
                </LinearLayout>


            </LinearLayout>

        </androidx.constraintlayout.widget.ConstraintLayout>

    </androidx.core.widget.NestedScrollView>
</layout>
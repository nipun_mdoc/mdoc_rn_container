package de.mdoc.pojo;

import java.util.ArrayList;

public class Coverage {

    private ArrayList<Payor> payor;
    private Type type;
    private ArrayList<ContactPoint> identifier;



    public ArrayList<ContactPoint> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(ArrayList<ContactPoint> identifier) {
        this.identifier = identifier;
    }

    public ArrayList<Payor> getPayor() {
        return payor;
    }

    public void setPayor(ArrayList<Payor> payor) {
        this.payor = payor;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getReferenceId(){
        for(Payor p : payor){
            if(p.getReference().contains("organization")){
                int index = p.getReference().lastIndexOf("/");
                if (index != -1) {
                     return p.getReference().substring(index+1);
                }
            }
        }
        return null;
    }

    public String getOfficialIdentifier(){
        for(ContactPoint i : identifier){
            if(i.getUse().equals("OFFICIAL")){
                return i.getValue();
            }
        }
        return "";
    }
}

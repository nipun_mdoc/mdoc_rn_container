package de.mdoc.network.request

data class AllClinicsRequest (var query: String = "", var findByAllCases: Boolean = false)
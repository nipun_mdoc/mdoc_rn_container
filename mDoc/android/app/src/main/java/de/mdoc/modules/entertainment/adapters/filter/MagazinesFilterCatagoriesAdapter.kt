package de.mdoc.modules.entertainment.adapters.filter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterSharedViewModel
import de.mdoc.modules.medications.create_plan.data.ListItem
import kotlinx.android.synthetic.main.common_filter_checkbox_item.view.*

class MagazinesFilterCatagoriesAdapter(private var items : MutableList<ListItem>, private val viewModel: MagazineFilterSharedViewModel)
    : RecyclerView.Adapter<MagazinesFilterCatagoriesAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val v =  LayoutInflater.from(parent.context)
                .inflate(R.layout.common_filter_checkbox_item,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return when (items) {
            null -> 0
            else -> items.size
        }
    }

    override fun onBindViewHolder(holder: ViewHolder,
                                  position: Int) {
        holder.bindData(items[position])
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        private var view: View = itemView
        private lateinit var data: ListItem

        init {
            view.checkBoxItem.setOnCheckedChangeListener {buttonView, isChecked ->
                data.activatedByUser = isChecked
                items[position] = data
                viewModel.categoriesTemp.value = items
            }
        }

        fun bindData (data: ListItem) {
            this.data = data
            view.checkBoxItem.text = this.data?.display
            view.checkBoxItem.isChecked = this.data.activatedByUser
        }
    }
}
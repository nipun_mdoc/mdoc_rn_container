package de.mdoc.data.create_bt_device

data class ObservationResponseData(
        val basedOn: List<Any>,
        val category: List<Category>,
        val code: CodeObservation,
        val component: List<Component>,
        val cts: Long,
        val device: Device,
        val effectiveDateTime: Long,
        val extension: List<Any>,
        val id: String,
        val identifier: List<Any>,
        val modifierExtension: List<Any>,
        val performer: List<Performer>,
        val referenceRange: List<Any>,
        val related: List<Any>,
        val resourceType: String,
        val status: String,
        val subjectObservation: SubjectObservation,
        val textObservation: TextObservation,
        val uts: Long,
        val valueQuantity: ValueQuantityObservation?
)
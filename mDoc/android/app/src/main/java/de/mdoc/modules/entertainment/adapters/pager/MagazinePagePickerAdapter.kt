package de.mdoc.modules.entertainment.adapters.pager

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.widget.TextViewCompat
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import kotlinx.android.synthetic.main.entertainment_magazine_page_picker_item.view.*

class MagazinePagePickerAdapter(private val items: IntArray, private val bookmarks: List<Int>? = emptyList(), private val delegate: MagazinePagePickerAdapterDelegate)
    : RecyclerView.Adapter<MagazinePagePickerAdapter.ViewHolder>() {

    interface MagazinePagePickerAdapterDelegate {
        fun onSelected(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.entertainment_magazine_page_picker_item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder,
                                  position: Int) {

        val bookmark = bookmarks?.find { it -> it == position }
        holder.bind(items[position], bookmark)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val txtMagazinePage: TextView = itemView.txtMagazinePage
        private val imgBookmark: ImageView = itemView.imgBookmark

        init {
            txtMagazinePage.setOnClickListener(this)
        }

        fun bind(position: Int, bookmark: Int?) {
            txtMagazinePage.text = position.toString()
            when (bookmark) {
                null -> {
                    txtMagazinePage.typeface = Typeface.defaultFromStyle(Typeface.NORMAL)
                    TextViewCompat.setTextAppearance(txtMagazinePage, R.style.PrimaryFontMedium)
                    txtMagazinePage.setTextColor(ContextCompat.getColor(itemView.context, R.color.gray_808080))
                    imgBookmark.setImageResource(android.R.color.transparent)
                }
                else -> {
                    txtMagazinePage.typeface = Typeface.defaultFromStyle(Typeface.BOLD)
                    TextViewCompat.setTextAppearance(txtMagazinePage, R.style.PrimaryFontSemiBold)
                    txtMagazinePage.setTextColor(ContextCompat.getColor(itemView.context, R.color.black))
                    imgBookmark.setImageResource(R.drawable.ic_bookmark_24_px)
                }
            }
        }

        override fun onClick(p0: View?) {
            delegate.onSelected(position)
        }
    }
}
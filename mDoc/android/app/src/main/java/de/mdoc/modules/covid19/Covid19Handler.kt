package de.mdoc.modules.covid19

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment.Companion.NEGATIVE
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment.Companion.POSITIVE
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import org.joda.time.format.DateTimeFormat

class Covid19Handler {
    fun openEmergencyContacts(context: Context) {
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(R.id.navigation_emergency_contacts)
    }

    fun openTestHistory(context: Context, covid19ViewModel: Covid19ViewModel) {
        val action = Covid19FragmentDirections.actionCovid19FragmentToTestHistoryFragment(
                covid19ViewModel.allHealthStatus.value?.toTypedArray() ?: arrayOf())

        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(action)
    }

    fun setTestStatus(healthStatus: HealthStatus?): String {
        var status = healthStatus?.healthStatus ?: ""
        AppPersistence.covidStatusCoding?.forEach {
            if (status == it.code) {
                status = it.display ?: ""
            }
        }
        return status
    }

    fun getUpdateTime(context: Context, healthStatus: HealthStatus?): String {
        val time = healthStatus?.uts
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm")
        val lastName: String
        return if (time != null) {
            lastName = if (healthStatus.isCratedByAdmin()) {
                healthStatus.lastName ?: "admin"
            }
            else {
                MdocAppHelper.getInstance().userLastName ?: "user"
            }
            context.resources.getString(R.string.covid_update_time, formatter.print(time), lastName)
        }
        else {
            ""
        }
    }

    fun setTestTime(context: Context, healthStatus: HealthStatus?): String {
        val time = healthStatus?.testDate
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm")

        return if (time != null) {
            context.resources.getString(R.string.covid_test_time, formatter.print(time))
        }
        else {
            ""
        }
    }

    fun setEmergencyCardText(context: Context): String {
        return if (MdocAppHelper.getInstance()
                .publicUserDetailses.hasValidEmergencyContact() || !MdocAppHelper.getInstance().emergencyInfo.isNullOrEmpty()) {
            context.resources.getString(R.string.covid_widget_emergency_show_data)
        }
        else {
            context.resources.getString(R.string.covid_widget_emergency_description)
        }
    }

    fun setBackgroundTint(context: Context, healthStatus: HealthStatus?): ColorStateList {
        val color = when (healthStatus?.healthStatus ?: "") {
            POSITIVE -> {
                R.color.covid_positive
            }

            NEGATIVE -> {
                R.color.covid_negative
            }

            else     -> {
                R.color.gray_e6e6e6
            }
        }

        return ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

public class FileConfig implements Serializable {

    private boolean patientFileUploadAllowed;

    public boolean isPatientFileUploadAllowed() {
        return patientFileUploadAllowed;
    }

    public void setPatientFileUploadAllowed(boolean patientFileUploadAllowed) {
        this.patientFileUploadAllowed = patientFileUploadAllowed;
    }
}

package de.mdoc.modules.profile.data

data class EmergencyInfoRequest(val emergencyInfo: String)
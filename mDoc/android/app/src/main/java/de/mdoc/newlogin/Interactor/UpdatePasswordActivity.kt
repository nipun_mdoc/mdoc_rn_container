package de.mdoc.newlogin.Interactor

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ImageViewCompat
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.ActivityUpdatePasswordBinding
import de.mdoc.newlogin.AuthListener
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.newlogin.RegistratioToastListener
import de.mdoc.newlogin.UpdatePasswordRequest
import de.mdoc.newlogin.UpdatePasswordViewModel
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.activity_update_password.*
import java.util.*

class UpdatePasswordActivity : AppCompatActivity(), AuthListener, RegistratioToastListener {

    private var updatePasswordViewModel: UpdatePasswordViewModel? = null
    private var binding: ActivityUpdatePasswordBinding? = null

    private var isPasswordValidated: Boolean = false
    private lateinit var llPasswordValidator : LinearLayout
    private lateinit var layout : View
    companion object {
        const val PASSWORD_CHANGE = 2211
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        updatePasswordViewModel = ViewModelProviders.of(this).get(UpdatePasswordViewModel::class.java)
        binding = DataBindingUtil.setContentView(this@UpdatePasswordActivity, R.layout.activity_update_password)
        binding?.setLifecycleOwner(this)
        binding?.setUpdatePasswordViewModel(updatePasswordViewModel)

        llPasswordValidator = findViewById(R.id.llPasswordValidator)
        layout = findViewById(R.id.passwordDialog)
        val txtInfo1 = layout.findViewById<TextView>(R.id.txtInfo1)
        val sLength = resources.getString(R.string.info1)
        val sFinal = String.format(sLength, resources.getInteger(R.integer.password_length))
        txtInfo1.text = sFinal
        txtPassword1.doAfterTextChanged { text ->
            validatePassword(text.toString())
        }

        updatePasswordViewModel!!.token = intent.getStringExtra(MdocConstants.DEEP_LINK_URI)

        var shakeAnimation: Animation? = AnimationUtils.loadAnimation(this, R.anim.shake)
        updatePasswordViewModel!!.requestData.observe(this, Observer<UpdatePasswordRequest> { updatePasswordRequest ->

            if (TextUtils.isEmpty(Objects.requireNonNull(updatePasswordRequest)!!.confirmPassword)) {
                txtConfrimPasswordlayout?.setError(" ")
                txtConfrimPasswordlayout.errorIconDrawable = null
                txtConfrimPasswordlayout?.requestFocus();
                txtConfrimPasswordlayout.startAnimation(shakeAnimation)

            } else if (!updatePasswordRequest!!.confirmPassword.equals(updatePasswordRequest!!.password)) {
                txtPasswordlayout?.setError(" ")
                txtPasswordlayout.errorIconDrawable = null
                txtPasswordlayout?.requestFocus();
                txtPasswordlayout.startAnimation(shakeAnimation)

            } else if ((updatePasswordRequest)!!.password!!.length < 8 || !isPasswordValidated) {
                txtPasswordlayout?.setError(" ")
                txtPasswordlayout.errorIconDrawable = null
                txtPasswordlayout?.requestFocus();
                txtPasswordlayout.startAnimation(shakeAnimation)

            } else {
                updatePasswordViewModel?.loadData(updatePasswordRequest!!, this)
            }
        })

        updatePasswordViewModel?.authListener = this
        updatePasswordViewModel?.toastListener = this

        if (savedInstanceState == null) {
            BandwidthCheck.initBandwidthCheck()
        }
    }
    private lateinit var encryptionService: EncryptionServices
    private lateinit var systemServices: SystemServices
    override fun sucess() {
        if (MdocAppHelper.getInstance().isOptIn) {
            encryptionService = EncryptionServices(this)
            systemServices = SystemServices(this)

            if (encryptionService.getMasterKey() == null) {
                encryptionService.createMasterKey()
            }
            systemServices.authenticateFingerprint(successAction = {

                if (encryptionService.getFingerprintKey() == null) {
                    encryptionService.createFingerprintKey()
                }
                MdocAppHelper.getInstance().isOptIn = true
                MdocAppHelper.getInstance().userPassword = txtPassword1.text.toString()
                setResult(Activity.RESULT_OK)
                finish();
            }, cancelAction = {
                setResult(Activity.RESULT_OK)
                finish();
            }, context1 = this@UpdatePasswordActivity)
        } else {
            MdocAppHelper.getInstance().userPassword = txtPassword1.text.toString()
            setResult(Activity.RESULT_OK)
            finish();
        }
    }

    override fun onLoadCaptcha(response: CaptchaResponse?) {
    }

    override fun isCaptchaVisible(): Boolean? {
        TODO("Not yet implemented")
    }

    override fun clearCaptchaField() {
        TODO("Not yet implemented")
    }
    override fun changePassword(token: String) {
        TODO("Not yet implemented")
    }

    override fun showMobileNumberScreen() {
        TODO("Not yet implemented")
    }

    override fun failer() {
    }
    override fun showOTPScreen(){
        TODO("Not yet implemented")
    }
    override fun onImpressumClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)    }
    override fun onDatenschutzDieClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)  }
    override fun onAGBClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)   }

    override fun onCloseClick() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onScanClick() {
    }

    private fun validatePassword(password: String) {

        var passwordEightChar = layout.findViewById<ImageView>(R.id.passwordEightChar)
        var passwordNumricChar = layout.findViewById<ImageView>(R.id.passwordNumricChar)
        var passwordLowerChar = layout.findViewById<ImageView>(R.id.passwordLowerChar)
        var passwordUpperChar = layout.findViewById<ImageView>(R.id.passwordUpperChar)
        var passwordSpecialChar = layout.findViewById<ImageView>(R.id.passwordSpecialChar)

        var lower = false
        var upper = false
        var numbers = false
        var special = false

        if(password.length > 0) {
            for (i in 0..password.length - 1) {
                val c = password[i]
                if (c.isDigit()) numbers = true
                else if (c.isLowerCase()) lower = true
                else if (c.isUpperCase()) upper = true
                else special = true
            }
        }

        if (password.length < resources.getInteger(R.integer.password_length)) {
            passwordEightChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordEightChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!numbers) {
            passwordNumricChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordNumricChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!lower) {
            passwordLowerChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordLowerChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!upper) {
            passwordUpperChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordUpperChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!special) {
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)))
        }else{
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if(password.length >= 8 && numbers && lower && upper && special){
            isPasswordValidated = true
        }else{
            isPasswordValidated = false
        }
    }

    override fun registrationToast(view: View?) {
        if (view != null) {
            if(llPasswordValidator.visibility == View.GONE){
                llPasswordValidator.visibility = View.VISIBLE
            }else{
                llPasswordValidator.visibility = View.GONE
            }
        }
    }

}
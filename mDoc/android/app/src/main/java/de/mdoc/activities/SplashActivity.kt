package de.mdoc.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AlertDialog
import com.google.gson.Gson
import com.scottyab.rootbeer.RootBeer
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.newlogin.Interactor.LogiActivty
import de.mdoc.newlogin.Interactor.UpdatePasswordActivity
import de.mdoc.newlogin.Model.UpdatePasswordResponseData
import de.mdoc.newlogin.UpdatePasswordRequest
import de.mdoc.pojo.ConfigurationData
import de.mdoc.react.RNModuleActivity
import de.mdoc.util.MdocAppHelper
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*


/**
 * Created by ema on 4/6/17.
 */
class SplashActivity: MdocBaseActivity() {

    private var data = ""
    private var token = ""
    private var deepLinkUri: Uri? = null
    override fun getResourceId() = R.layout.activity_spash

    override fun init(savedInstanceState: Bundle?) {
        intent?.getStringExtra(MdocConstants.EXTRA)
            ?.let {
                data = it
            }
        deepLinkUri = intent?.data
//        getConfiguration("")

        val intent = Intent(this, RNModuleActivity::class.java)
        intent.putExtra("app_name", resources.getString(R.string.app_name))
        startActivity(intent)
        finish()
    }

    private fun hasRootDetection() = resources.getBoolean(R.bool.has_root_detection)

    private fun checkRoot(isUpdatePassword: Boolean) {
        val rootBeer = RootBeer(this)

        if (rootBeer.isRooted) {
            showDialog()
        }
        else {
            timeoutHandler(isUpdatePassword)
        }
    }

    private fun isAccountActivated(token: String) {

        var updatePasswordRequest = UpdatePasswordRequest()
        updatePasswordRequest!!.client_id = this.getString(R.string.keycloack_clinic_Id)
        updatePasswordRequest!!.client_secret = this.getString(R.string.client_secret)
        updatePasswordRequest!!.token = token

        MdocAppHelper.getInstance().accessToken = null
        val call = RestClient.getAuthServiceLogin().isAccountActivated(updatePasswordRequest, Locale.getDefault().language)
        call!!.enqueue(object : Callback<UpdatePasswordResponseData?> {
            override fun onResponse(call: Call<UpdatePasswordResponseData?>, response: Response<UpdatePasswordResponseData?>) {

                if (response.isSuccessful) {

                    if (response.body()!!.data != null && response.body()!!.data.passwordResetToken != null) {
                        navigateToUpdatePassword(response.body()!!.data.passwordResetToken)
                    } else {
                        timeoutHandler(false)
                    }
                } else {
                    timeoutHandler(false)
                }
            }

            override fun onFailure(call: Call<UpdatePasswordResponseData?>, t: Throwable) {
                timeoutHandler(false)
            }
        })
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.root_dialog_title))
        builder.setMessage(getString(R.string.root_device_message))

        builder.setPositiveButton(R.string.mdtp_ok
        ) { _, _ ->
            this.finish()
        }
        val dialog = builder.create()
        dialog.setCanceledOnTouchOutside(false)
        dialog.show()
    }

    private fun timeoutHandler(isUpdatePassword: Boolean) {

        runOnUiThread(Runnable {
            Handler().postDelayed({
                if (BuildConfig.FLAV != "kiosk") {
                    if (isUpdatePassword) {
                        navigateToUpdatePassword(token)
                    } else {
                        navigateToLogin()
                    }
                } else {
                    navigateToSelectClinic()
                }
            }, SPLASH_DISPLAY_LENGTH)
        })
    }

    private fun navigateToSelectClinic() {
        val clinicIntent = Intent(this@SplashActivity, KioskClinicActivity::class.java)
        clinicIntent.putExtra(MdocConstants.EXTRA, data)

        this@SplashActivity.startActivity(clinicIntent)
        this@SplashActivity.finish()
    }

    private fun navigateToLogin() {
        var loginIntent : Intent
        if(getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
            loginIntent = Intent(this@SplashActivity, LogiActivty::class.java)
        }else{
            loginIntent = Intent(this@SplashActivity, LoginActivity::class.java)
        }
        loginIntent.putExtra(MdocConstants.EXTRA, data)
        loginIntent.putExtra(MdocConstants.DEEP_LINK_URI, deepLinkUri.toString());
        this@SplashActivity.startActivity(loginIntent)
        this@SplashActivity.finish()
    }

    private fun navigateToUpdatePassword(token: String) {
        var loginIntent : Intent
        loginIntent = Intent(this@SplashActivity, UpdatePasswordActivity::class.java)
        loginIntent.putExtra(MdocConstants.DEEP_LINK_URI, token);
        this@SplashActivity.startActivity(loginIntent)
        this@SplashActivity.finish()
    }


    private fun getConfiguration(clientId: String) {

        MdocManager.getConfigurations(object : Callback<ConfigurationData?> {
            override fun onResponse(call: Call<ConfigurationData?>, response: Response<ConfigurationData?>) {
                try {
//                    var gson = Gson()
//                    val configurationData = gson.fromJson(response.body?.string(), ConfigurationData::class.java)
//                    var sortedList = (configurationData?.list)?.sortedWith(compareBy { it.order })
//                    var ddd: ConfigurationData? = configurationData;
                    MdocAppHelper.getInstance().configurationData = response?.body()
                    proceedLogin()
                } catch (e: Exception) {
                    proceedLogin()
                }
            }

            override fun onFailure(call: Call<ConfigurationData?>, t: Throwable) {}
        })

//        val client = OkHttpClient()
//        val request = Request.Builder()
//               .url(resources.getString(R.string.base_url).replace("api/", "") + "config/assets/mobile")
//               .build()
//
//        client.newCall(request).enqueue(object : okhttp3.Callback {
//
//            override fun onFailure(call: okhttp3.Call, e: IOException) {
//                proceedLogin()
//            }
//
//            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
//                try {
//                    var gson = Gson()
//                    val configurationData = gson.fromJson(response.body?.string(), ConfigurationData::class.java)
//                    var sortedList = (configurationData?.list)?.sortedWith(compareBy { it.order })
//                    var ddd: ConfigurationData? = configurationData;
//                    MdocAppHelper.getInstance().configurationData = configurationData
//                    proceedLogin()
//                } catch (e: Exception) {
//                    proceedLogin()
//                }
//
//            }
//
//
//        })

    }

    private fun proceedLogin(){
        if(deepLinkUri == null) {
            if (hasRootDetection()) {
                checkRoot(false)
            } else {
                timeoutHandler(false)
            }
        }else{
            if(deepLinkUri.toString().contains("activate-account")){
                val bits: List<String> = deepLinkUri.toString().split("/")
                token = bits[bits.size - 1]
                isAccountActivated(token)
            }else if(deepLinkUri.toString().contains("reset-password")){
                val bits: List<String> = deepLinkUri.toString().split("/")
                token = bits[bits.size - 1]
                navigateToUpdatePassword(token)
            }else{
                if (hasRootDetection()) {
                    checkRoot(false)
                } else {
                    timeoutHandler(false)
                }
            }
        }
    }

    companion object {
        const val SPLASH_DISPLAY_LENGTH = 1000L
    }

    override fun onStart() {
        super.onStart()
        MdocManager.logout()
    }
}
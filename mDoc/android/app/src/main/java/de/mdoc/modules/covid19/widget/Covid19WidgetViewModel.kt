package de.mdoc.modules.covid19.widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.modules.medications.data.MedicationAdministration
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class Covid19WidgetViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    var hasHealthStatus = MutableLiveData(false)

    var healthStatus1: MutableLiveData<HealthStatus> = MutableLiveData(HealthStatus())

    fun getHealthStatus() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                isShowContent.value = false
                healthStatus1.value=mDocService.getHealthStatus(PHYSICIAN).data?.list?.sortedByDescending { it.cts }?.getOrNull(0)
                hasHealthStatus.value=!healthStatus1.value?.healthStatus.isNullOrEmpty()
                isShowContent.value = true
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                hasHealthStatus.value=false
            }
        }
    }
    companion object{
        const val PHYSICIAN = "*physician*"
    }
}
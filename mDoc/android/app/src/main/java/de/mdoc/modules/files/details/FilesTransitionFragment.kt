package de.mdoc.modules.files.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentFilesTransitionBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class FilesTransitionFragment : NewBaseFragment() {

    val args:FilesTransitionFragmentArgs by navArgs()
    override val navigationItem: NavigationItem = NavigationItem.FilesPreview
    private val filesTransitionViewModel by viewModel {
        FilesTransitionViewModel(
                RestClient.getService(), args.fileId?:"")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentFilesTransitionBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_files_transition, container, false)

        binding.lifecycleOwner = this
        binding.filesTransitionViewModel = filesTransitionViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        filesTransitionViewModel.proceed.observe(viewLifecycleOwner, Observer {
            if(it){
                viewFile(context, filesTransitionViewModel.file.value ?: null)
                filesTransitionViewModel.proceed.value = false
            }
        })

        filesTransitionViewModel.getFileById()
    }
}
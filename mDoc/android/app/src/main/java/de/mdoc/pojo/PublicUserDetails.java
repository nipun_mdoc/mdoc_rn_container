package de.mdoc.pojo;

import java.util.ArrayList;
import java.util.Map;

import de.mdoc.modules.profile.data.Contact;

/**
 * Created by ema on 1/23/17.
 */

public class PublicUserDetails {

    private Address address;
    private String firstName;
    private String gender;
    private String language;
    private String lastName;
    private String username;
    private String email;
    private String phone;
    private String printPatientPlan;
    private long born;
    private boolean privacyAccepted;
    private boolean termsAccepted;
    private String genderCode;
    private ArrayList<Contact> emergencyContacts;
    private boolean masterdataUpdated;
    private Boolean emailRequestShown;
    private Map<String, String> lastSeenUnitByCode;

    public PublicUserDetails() {
        address = new Address();
        emergencyContacts = new ArrayList<>();
    }

    public boolean isMasterdataUpdated() {
        return masterdataUpdated;
    }

    public void setMasterdataUpdated(boolean masterdataUpdated) {
        this.masterdataUpdated = masterdataUpdated;
    }

    public long getBorn() {
        return born;
    }

    public void setBorn(long born) {
        this.born = born;
    }

    public ArrayList<Contact> getEmergencyContacts() {
        return emergencyContacts;
    }

    public Boolean hasValidEmergencyContact() {
        if (emergencyContacts != null && !emergencyContacts.isEmpty()) {
            for (Contact contact : emergencyContacts) {
                if (contact.getPhone() != null && !contact.getPhone().isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setEmergencyContacts(ArrayList<Contact> emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public void addEmergencyContact(Contact emergencyContact) {
        if (emergencyContacts != null) {
            emergencyContacts.add(emergencyContact);
        }
    }

    public Map<String, String> getLastSeenUnitByCode() {
        return lastSeenUnitByCode;
    }

    public void setLastSeenUnitByCode(Map<String, String> lastSeenUnitByCode) {
        this.lastSeenUnitByCode = lastSeenUnitByCode;
    }

    public boolean isTermsAccepted() {
        return termsAccepted;
    }

    public void setTermsAccepted(boolean termsAccepted) {
        this.termsAccepted = termsAccepted;
    }

    public boolean isPrivacyAccepted() {
        return privacyAccepted;
    }

    public void setPrivacyAccepted(boolean privacyAccepted) {
        this.privacyAccepted = privacyAccepted;
    }

    public String getPrintPatientPlan() {
        return printPatientPlan;
    }

    public void setPrintPatientPlan(String printPatientPlan) {
        this.printPatientPlan = printPatientPlan;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

    public Boolean getEmailRequestShown() {
        return emailRequestShown;
    }

    public void setEmailRequestShown(Boolean emailRequestShown) {
        this.emailRequestShown = emailRequestShown;
    }
}

package de.mdoc.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ema on 5/8/17.
 */

public class Image implements Serializable{

    private String image;
    @SerializedName("_id")
    private String id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

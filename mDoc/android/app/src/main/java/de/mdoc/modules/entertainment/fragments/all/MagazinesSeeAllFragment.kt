package de.mdoc.modules.entertainment.fragments.all

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.entertainment.adapters.all.MagazinesSeeAllAdapter
import de.mdoc.modules.entertainment.repository.MagazinesRepository
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterSharedViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineSeeAllViewModel
import de.mdoc.util.setActionTitle
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.common_search_view.*
import kotlinx.android.synthetic.main.fragment_magazines_see_all.*


class MagazinesSeeAllFragment : MdocFragment() {

    private lateinit var sharedViewModel: MagazineFilterSharedViewModel
    private lateinit var activity: MdocActivity
    private lateinit var category: String

    private var savedGridLayoutState: Int = 0

    override val navigationItem: NavigationItem = NavigationItem.EntertainmentSeeAll

    private val viewModel by viewModel {
        MagazineSeeAllViewModel(category = category, repository = MagazinesRepository())
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_magazines_see_all
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        val bundle = arguments
        if (bundle != null) {
            category = bundle.getString(MdocConstants.MAGAZINE_CATEGORY)
                    ?: getString(R.string.entertainment)
        }

        sharedViewModel = mdocActivity?.run {
            ViewModelProviders.of(this).get(MagazineFilterSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionTitle(category)

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                findNavController().navigate(R.id.action_entertainmentSeeAll_to_entertainmentSearch)
            }
        }

        // Observe magazines changes
        viewModel.magazines.observe(viewLifecycleOwner) {
            gvItems.apply {
                if (it?.data?.list != null) {
                    adapter = when (sharedViewModel.hasActiveLanguages()) {
                        true -> {
                            MagazinesSeeAllAdapter(sharedViewModel.filterLanguages(it), activity)
                        }
                        else -> {
                            MagazinesSeeAllAdapter(it.data.list, activity)
                        }
                    }
                }

                gvItems.setSelection(savedGridLayoutState)
            }
        }

        viewModel.reloadData()
    }

    override fun onStop() {
        super.onStop()
        savedGridLayoutState = gvItems.firstVisiblePosition
    }
}
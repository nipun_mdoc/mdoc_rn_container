package de.mdoc.modules.appointments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseAppointmentViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.day.AllDayViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.day.AppointmentDayViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.day.MetaDataDayViewHolder
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.getMonth
import de.mdoc.util.getYear
import de.mdoc.util.isDateDifferent
import java.util.*


class DayAppointmentAdapter(val context: Context, private val appointmentCallback: AppointmentDayCallback? = null, private val isFromWidget: Boolean = false) :
        RecyclerView.Adapter<BaseAppointmentViewHolder>() {

    interface AppointmentDayCallback {
        fun onDayItemClicked(item: AppointmentDetails, context: View?)
        fun showAppointmentDetails(item: AppointmentDetails, context: View?)
    }

    private val items: ArrayList<AppointmentDetails> = arrayListOf()

    fun setItems(list: List<AppointmentDetails>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun removeItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAppointmentViewHolder {
        return when (viewType) {
            ALL_DAY_ITEM -> {
                AllDayViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_all_day_appointment, parent, false))
            }
            META_DATA -> {
                MetaDataDayViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_appointment, parent, false), isFromWidget)

            }
            else -> {
                AppointmentDayViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_appointment, parent, false), isFromWidget)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseAppointmentViewHolder, position: Int) {
        if (position > 0){
            val previousItem = items[position-1]
            val currentItem = items[position]
            val hasHeader = currentItem.start.isDateDifferent(previousItem.start)
            holder.bind(items[position], context, appointmentCallback, hasHeader)
        } else {
            holder.bind(items[position], context, appointmentCallback)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when {
            item.isAllDay -> ALL_DAY_ITEM
            item.isMetaDataNeeded || item.isMetaDataPopulated || item.isCanceled -> META_DATA
            else -> DEFAULT_ITEM
        }
    }

    companion object {
        const val ALL_DAY_ITEM = 0
        const val DEFAULT_ITEM = 1
        const val META_DATA = 2
    }
}
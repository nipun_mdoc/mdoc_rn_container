package de.mdoc.network.response;

import de.mdoc.pojo.ConfigurationData;
import de.mdoc.pojo.NotificationData;

/**
 * Created by ema on 7/14/17.
 */

public class ConfigurationResponse extends MdocResponse{


    private ConfigurationData data;

    public ConfigurationData getData() {
        return data;
    }

    public void setData(ConfigurationData data) {
        this.data = data;
    }
}

package de.mdoc.modules.my_clinic.widget

import android.app.Activity
import android.view.View
import androidx.lifecycle.LifecycleOwner
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.mdoc.activities.navigation.isTablet
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.layout_my_clinic.view.*
import org.joda.time.format.DateTimeFormat

class MyClinicWidgetBinding(private val lifecycleOwner: LifecycleOwner,
                            private val view: View,
                            viewModel: MyClinicWidgetViewModel,
                            private val callback: Callback
): LifecycleOwner by lifecycleOwner{
    interface Callback {

        fun openMyClinic()

    }
    init {
        viewModel.loadData()

        view.setOnClickListener {
            callback.openMyClinic()

        }
        viewModel.widgetData.observe {
            fillClinicWidgetData(it)
        }

    }
    private fun fillClinicWidgetData(clinicData:CurrentClinicData?){
         val formatter = DateTimeFormat.forPattern("dd MMMM")

        val phone = clinicData?.clinicDetails?.contactDetails?.phone

        if (clinicData?.clinicDetails?.images?.getOrNull(0)?.image?.isEmpty() == false) {
            Glide.with(view.context.applicationContext)
                    .load(clinicData.clinicDetails.images[0].image)
                    .fitCenter()
                    .into( view.clinicIv)
        }
        view.shortDescriptionTv?.text = MdocUtil.fromHtml(clinicData?.clinicDetails?.description)

        if (view.context.isTablet()) {
            val checkIn=formatter.print(clinicData?.planDetails?.checkIn?:0)
            val checkOut=formatter.print(clinicData?.planDetails?.checkOut?:0)

            view.checkinTv?.text = checkIn
            view.checkoutTv?.text = checkOut
            view.drNameTv?.text = clinicData?.clinicDetails?.headOfClinic?.name

            if (clinicData?.clinicDetails?.headOfClinic?.image?.isNotEmpty() == true) {
                Glide.with(view.context.applicationContext)
                        .load(clinicData.clinicDetails.headOfClinic.image)
                        .apply(RequestOptions().override(150, 150).fitCenter())
                        .into((view.drImageIv!!))
            }
        } else {
            view.clinicNameTv?.text = clinicData?.clinicDetails?.name
        }

        view.callClinicRl?.setOnClickListener {
            MdocUtil.callPhone(view.context as Activity, phone)

        }
    }
}

package de.mdoc.modules.mental_goals.onboard_mental_goals

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.fragment_onboard_first.*

class FirstOnBoardFragment: MdocBaseFragment() {
    override fun setResourceId(): Int {
        return R.layout.fragment_onboard_first
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    var listener:ItemSelectionListener?=null
    lateinit var activity: MdocActivity


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()
    }

    private fun setupListeners() {
        txt_next.setOnClickListener {
            listener?.pagerIndex(1)
        }
        txt_skip_intro?.setOnClickListener {
            val action= MainNavDirections.globalActionToMentalGoalCreation()
            findNavController().navigate(action)
            MdocAppHelper.getInstance().isShowGoalsOnboarding = false

        }
    }

    fun setItemSelectionListener(listener:ItemSelectionListener){
        this.listener=listener
    }
}
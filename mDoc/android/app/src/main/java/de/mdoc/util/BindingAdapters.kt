package de.mdoc.util

import android.content.res.ColorStateList
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.os.Build
import android.text.Html
import android.view.View
import android.view.View.*
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import androidx.databinding.BindingAdapter
import com.amulyakhare.textdrawable.TextDrawable
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.textfield.TextInputEditText
import de.mdoc.R
import org.joda.time.DateTime
import org.joda.time.Instant
import org.joda.time.format.DateTimeFormat

@BindingAdapter("imageUrl")
fun loadImageWithGlide(imageView: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(imageView.context)
            .load(url)
            .placeholder(R.drawable.ic_no_clinic_placeholder)
            .error(R.drawable.ic_no_clinic_placeholder)
            .centerCrop()
            .dontAnimate()
            .into(imageView)
    }
    else {
        Glide.with(imageView.context)
            .load("")
            .placeholder(R.drawable.ic_no_clinic_placeholder)
            .error(R.drawable.ic_no_clinic_placeholder)
            .centerCrop()
            .dontAnimate()
            .into(imageView)
    }
}

@BindingAdapter("doctorImageUrl")
fun loadDoctorImageWithGlide(imageView: ImageView, url: String?) {
    if (!url.isNullOrEmpty()) {
        Glide.with(imageView.context)
            .load(url)
            .placeholder(R.drawable.ic_no_doctor_placeholder)
            .error(R.drawable.ic_no_doctor_placeholder)
            .dontAnimate()
            .apply(RequestOptions().override(150, 150)
                .fitCenter())
            .into(imageView)
    }
    else {
        Glide.with(imageView.context)
            .load("")
            .placeholder(R.drawable.ic_no_doctor_placeholder)
            .error(R.drawable.ic_no_doctor_placeholder)
            .dontAnimate()
            .apply(RequestOptions().override(150, 150)
                .fitCenter())
            .into(imageView)
    }
}

@BindingAdapter(value = ["millisecond", "timeFormat"], requireAll = false)
fun setTimeAsString(view: TextView, time: Long, format: String) {
    val formatter = DateTimeFormat.forPattern(format)
    if (time != 0L) {
        view.text = formatter.print(time)
    }
    else {
        view.text = ""
    }
}

@BindingAdapter("android:visibleGone")
fun setVisibility(view: View, item: Any?) {
    item?.let {
        if (it is String) {
            view.visibility = if (it.isEmpty()) GONE else VISIBLE
        }
        else {
            view.visibility = VISIBLE
        }
    }
}

@BindingAdapter("isBold")
fun setBold(view: TextView, isBold: Boolean) {
    if (isBold) {
        view.setTypeface(null, Typeface.BOLD)
    }
    else {
        view.setTypeface(null, Typeface.NORMAL)
    }
}

@BindingAdapter("android:src")
fun setImageViewResource(view: ImageView, resId: Int) {
    view.setImageResource(resId)
}

@BindingAdapter(value = ["base64Image", "placeholder"], requireAll = false)
fun loadBase64Image(imageView: ImageView, base64String: String?, placeholder: Drawable?) {
    if (!base64String.isNullOrEmpty()) {

        imageView.setImageBitmap(base64String.toBitmap())
    }
    else {
        if (placeholder != null) {
            imageView.setImageDrawable(placeholder)
        }
        else {
            imageView.setImageBitmap("".toBitmap())
        }
    }
}

@BindingAdapter("isSelected")
fun isSelected(textView: View, isSelected: Boolean?) {
    val activity = textView.context
    if (isSelected == true) {
        textView.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(activity, R.color.colorPrimary))
    }
    else {
        textView.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(activity, R.color.colorSecondary32))
    }
}

@BindingAdapter(value = ["dateTime", "timeFormat"], requireAll = false)
fun setTimeFromDateTime(view: TextInputEditText, time: DateTime?, format: String) {
    val formatter = DateTimeFormat.forPattern(format)
    if (time == null) {
        view.setText("")
    }
    else {
        view.setText(formatter.print(time))
    }
}

@BindingAdapter(value = ["textDrawable", "isFromDashboardWidget"], requireAll = false)
fun setTextDrawable(view: ImageView, letter: String?, isFromDashboardWidget: Boolean?) {
    val isPrimaryColor = isFromDashboardWidget ?: false
    val generator = ColorGenerator.MATERIAL // or use DEFAULT
    val newLetter = letter ?: "A"
    val color = if(isPrimaryColor) ContextCompat.getColor(view.context, R.color.colorPrimary) else generator.getColor(newLetter)
    val drawable = TextDrawable.builder()
        .buildRound(newLetter, color)
    view.setImageDrawable(drawable)
}
@BindingAdapter("textEmergencyDrawable")
fun setTextEmergencyDrawable(view: ImageView, letter: String?) {
    val newLetter = letter ?: "A"
    val drawable = TextDrawable.builder()
        .buildRound(newLetter, R.color.chat_bubble_E2E6F2)
    view.setImageDrawable(drawable)
}

@BindingAdapter("grayTextDrawable")
fun setGrayTextDrawable(view: ImageView, letter: String?) {
    var newLetter = letter ?: "A"
    val drawable = TextDrawable.builder()
        .buildRound(newLetter, ContextCompat.getColor(view.context, R.color.gray_a8a8a8))
    view.setImageDrawable(drawable)
}

@BindingAdapter("timestampInboxTime")
fun setTimeFromForInbox(view: TextView, time: Instant?) {
    val formatterForToday = DateTimeFormat.forPattern("HH:mm")
    val formatterForPast = DateTimeFormat.forPattern("dd.MM.yyyy")

    if (time == null) {
        view.text = ""
    }
    else if (MdocUtil.isToday(time.millis)) {
        view.text = formatterForToday.print(time.millis)
    }
    else {
        view.text = formatterForPast.print(time.millis)
    }
}
@BindingAdapter("newsWidgetTime")
fun setTimeForNewsWidget(view: TextView, time: Instant?) {
    val formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm")
    if (time == null) {
        view.text = ""
    }
    else {
        view.text = formatter.print(time.millis)
    }
}

@BindingAdapter("txtFromHtml")
fun setTextFromHtml(view: View, input: Any?) {
    if (view is TextView) {
        view.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(input.toString(), Html.FROM_HTML_MODE_COMPACT)
        }
        else {
            HtmlCompat.fromHtml(input.toString() ?: "", HtmlCompat.FROM_HTML_MODE_LEGACY)
        }
    }
}

@BindingAdapter("isVisibleInvisible")
fun isVisibleInvisible(view: View, isVisible: Boolean?) {
    isVisible?.let { isTrue ->
        view.visibility = if (isTrue) VISIBLE else INVISIBLE
    }
}

@BindingAdapter("isVisibleGone")
fun isVisibleGone(view: View, isVisible: Boolean?) {
    isVisible?.let { isTrue ->
        view.visibility = if (isTrue) VISIBLE else GONE
    }
}
package de.mdoc.pojo;

import java.util.Objects;

/**
 * Created by ema on 12/29/16.
 */

public class ExtendedQuestion extends Question {

    private Section section;
    private Answer answer;
    private String compositeIndex;

    public ExtendedQuestion(Section section, Question question, Answer answer, String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
        this.section = section;
        this.index = question.index;
        this.uuid = question.uuid;
        this.pollQuestionType = question.pollQuestionType;
        this.pollQuestionExternalType = question.pollQuestionExternalType;
        this.ordinal = question.ordinal;
        this.showContent = question.showContent;
        this.userId = question.userId;
        this.pollSectionId = question.pollSectionId;
        this.pollId = question.pollId;
        this.pollAssignId = question.pollAssignId;
        this.id = question.id;
        this.questionData = question.questionData;
        this.childQuestionsIds = question.childQuestionsIds;
        this.showIf = question.showIf;
        this.answer = answer;
        this.pollQuestionTextType = question.pollQuestionTextType;
        this.required = question.required;
    }

    public ExtendedQuestion(Question question, Answer answer, String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
        this.index = question.index;
        this.uuid = question.uuid;
        this.pollQuestionType = question.pollQuestionType;
        this.pollQuestionExternalType = question.pollQuestionExternalType;
        this.ordinal = question.ordinal;
        this.showContent = question.showContent;
        this.userId = question.userId;
        this.pollSectionId = question.pollSectionId;
        this.pollId = question.pollId;
        this.pollAssignId = question.pollAssignId;
        this.id = question.id;
        this.questionData = question.questionData;
        this.childQuestionsIds = question.childQuestionsIds;
        this.showIf = question.showIf;
        this.answer = answer;
        this.pollQuestionTextType = question.pollQuestionTextType;
        this.required = question.required;
    }

    public String getCompositeIndex() {
        return compositeIndex;
    }

    public void setCompositeIndex(String compositeIndex) {
        this.compositeIndex = compositeIndex;
    }

    public Section getSection() {
        return section;
    }

    public String getSectionName () {
        return section.getName(selectedLanguage);
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uuid);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof ExtendedQuestion) {
            ExtendedQuestion q = (ExtendedQuestion) obj;
            return this.getId().equals(q.getId());
        }
        return false;
    }

    public String getOptionValue(Option option) {
        return option.value.get(selectedLanguage);
    }
}

package de.mdoc.modules.devices.available_devices

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.devices.adapters.VitalAvailableAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.pojo.MyDevices
import de.mdoc.pojo.ProvidersData
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.fragment_available_vital.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


class AvailableVitalFragment : MdocBaseFragment() {

    override fun setResourceId(): Int {
        return R.layout.fragment_available_vital
    }

    override fun init(savedInstanceState: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getConnectedProviders()
    }

    private fun setupAdapter(items: ArrayList<MyDevices>) {
        context?.let {
            rv_available_vital?.layoutManager = GridLayoutManager(it, 2)
            rv_available_vital?.adapter = VitalAvailableAdapter(it, items)
        }
    }

    private fun getConnectedProviders() {
        val userId = MdocAppHelper.getInstance().userId
        progressBar?.visibility = View.VISIBLE

        MdocManager.getConnectedDevices(userId, object : Callback<ProvidersData> {
            override fun onResponse(call: Call<ProvidersData>, response: Response<ProvidersData>) {
                if (response.body()?.data?.providers != null) {


                    val deviceList: ArrayList<MyDevices> = ArrayList()
                    response.body()!!.data.providers.forEach {
                        deviceList.add(MyDevices(it.device, it.device, it.linkUrl))
                    }
                    setupAdapter(deviceList)
                    progressBar?.visibility = View.GONE

                }
            }

            override fun onFailure(call: Call<ProvidersData>, t: Throwable) {
                Timber.v("failed")
                progressBar?.visibility = View.GONE
            }
        })
    }
}
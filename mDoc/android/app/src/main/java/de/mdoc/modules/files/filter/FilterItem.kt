package de.mdoc.modules.files.filter

import android.content.Context

data class FilterItem(
    val category: FilterCategory,
    val isChecked: Boolean
) {

    fun getName(context: Context): String {
        return category.getName(context)
    }

}
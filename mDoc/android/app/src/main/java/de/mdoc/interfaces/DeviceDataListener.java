package de.mdoc.interfaces;

import android.bluetooth.BluetoothGatt;

import java.util.ArrayList;

import de.mdoc.modules.devices.data.BleMessage;

public interface DeviceDataListener {
    void genericDeviceResponse(ArrayList<Byte> list, BluetoothGatt gatt, BleMessage message);
}
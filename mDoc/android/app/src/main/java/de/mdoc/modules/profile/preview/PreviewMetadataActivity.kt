package de.mdoc.modules.profile.preview

import android.content.Intent
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.ActivityMetadataPreviewBinding
import de.mdoc.modules.profile.patient_details.PatientDetailsActivity
import de.mdoc.modules.profile.patient_details.PatientDetailsViewModel
import de.mdoc.modules.profile.preview.adapters.PreviewPagerAdapter
import de.mdoc.util.ColorUtil
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.activity_mdoc.*
import kotlinx.android.synthetic.main.activity_metadata_preview.*

class PreviewMetadataActivity : AppCompatActivity(), ViewPager.OnPageChangeListener, View.OnClickListener {

    private val patientDetailsViewModel by viewModel {
        PatientDetailsViewModel()
    }

    lateinit var appointmentId: String
    var uts: Long? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMetadataPreviewBinding = DataBindingUtil.setContentView(this, R.layout.activity_metadata_preview)
        binding.patientDetailsViewModel = patientDetailsViewModel
        binding.lifecycleOwner = this

        parseIntent()
        setUpdateTime()
        btnMetadataEdit.setOnClickListener(this)
        metadataPrevious.setOnClickListener(this)
        metadataNext.setOnClickListener(this)
        metadataFinish.setOnClickListener(this)
        btnBack.setOnClickListener(this)
        btnBack.colorFilter = PorterDuffColorFilter(ColorUtil.contrastColor(resources, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)
        btnMetadataEdit.colorFilter = PorterDuffColorFilter(ColorUtil.contrastColor(resources, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP)

        patientDetailsViewModel.isFirstFragmentReady.observe(this, Observer {
            if (it) {
                initAdapter()
            }
        })
    }

    private fun parseIntent() {
        appointmentId = intent.getStringExtra(PatientDetailsActivity.APPOINTMENT_ID)
        uts = intent.getLongExtra(MdocConstants.APPOINTMENT_UTS, 0)
    }

    private fun initAdapter() {
        metadataPreviewContainer.adapter = PreviewPagerAdapter(supportFragmentManager)
        metadataPreviewContainer.addOnPageChangeListener(this)
        metadataPreviewContainer.post(Runnable { this.onPageSelected(metadataPreviewContainer.currentItem) })
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        val realPosition = position+1
        val maxPositions = metadataPreviewContainer.adapter?.count?:0
        txtPage.text = getString(R.string.meta_data_step_of,  realPosition, maxPositions)
        progressMetadataPreview.progress = MdocUtil.calculateProgress(realPosition, maxPositions)

        when (position) {
            0 -> {
                metadataPrevious.visibility = View.INVISIBLE
                metadataNext.visibility = View.VISIBLE
                metadataFinish.visibility = View.GONE
            }
            in 1..4 -> {
                metadataPrevious.visibility = View.VISIBLE
                metadataNext.visibility = View.VISIBLE
                metadataFinish.visibility = View.GONE
            }
            else -> {
                metadataPrevious.visibility = View.VISIBLE
                metadataNext.visibility = View.GONE
                metadataFinish.visibility = View.VISIBLE
            }
        }
    }

    private fun setUpdateTime() {
        uts?.let {
            val hour = MdocUtil.getTimeFromMs(it, MdocConstants.HOUR_AND_MINUTES)
            val date = MdocUtil.getTimeFromMs(it, MdocConstants.DAY_MONTH_DOT_FORMAT)
            txtUpdateTime?.text = resources.getString(R.string.last_updated_date_at_hour, date, hour)
        }
    }

    override fun onClick(v: View?) {
        when (v) {
            btnMetadataEdit -> {
                finish()
                val loginIntent = Intent(this, PatientDetailsActivity::class.java)
                loginIntent.putExtra(PatientDetailsActivity.APPOINTMENT_ID, appointmentId)
                startActivity(loginIntent)
            }
            metadataPrevious -> {
                metadataPreviewContainer.setCurrentItem(metadataPreviewContainer.currentItem - 1, true)
            }
            metadataNext -> {
                metadataPreviewContainer.setCurrentItem(metadataPreviewContainer.currentItem + 1, true)
            }
            metadataFinish -> {
                finish()
            }
            btnBack -> {
                finish()
            }
        }
    }
}
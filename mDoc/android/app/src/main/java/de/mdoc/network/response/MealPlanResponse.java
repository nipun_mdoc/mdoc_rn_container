package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.MealPlan;

/**
 * Created by ema on 11/3/17.
 */

public class MealPlanResponse {

    private ArrayList<MealPlan> data;

    public ArrayList<MealPlan> getData() {
        return data;
    }

    public void setData(ArrayList<MealPlan> data) {
        this.data = data;
    }
}

package de.mdoc.modules.devices.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.data.configuration.ConfigurationDetailsMultiValue
import de.mdoc.modules.devices.available_devices.AvailableDevicesFragmentDirections
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.util.toBitmap

class DynamicMedicalAvailableAdapter(
        var context: Context,
        var items: List<FhirConfigurationData>,
        var configuration: ConfigurationDetailsMultiValue
) : RecyclerView.Adapter<DynamicMedicalAvailableViewHolder>() {

    override fun onCreateViewHolder(
            parent: ViewGroup, viewType: Int): DynamicMedicalAvailableViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_dynamic_available_medical_device,
                        parent, false)
        context = parent.context

        return DynamicMedicalAvailableViewHolder(context,view)
    }

    override fun onBindViewHolder(holder: DynamicMedicalAvailableViewHolder, position: Int) {

        val item = items[position]
        holder.updateDevices(item)

        holder.constraintLayout.setOnClickListener {

            if(item.deviceModel!!.contains("BF600")) {
                val action = AvailableDevicesFragmentDirections.actionToFragmentInitializeBf600(item)
                it.findNavController().navigate(action)

            } else {
                val action = AvailableDevicesFragmentDirections.actionToFragmentLoadingDevice(deviceData = item)
                it.findNavController().navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class DynamicMedicalAvailableViewHolder(
        var context: Context,itemView: View) : RecyclerView.ViewHolder(itemView) {

    var imageView: ImageView = itemView.findViewById<View>(R.id.imageMyDevice) as ImageView
    var constraintLayout: ConstraintLayout =
            itemView.findViewById<View>(R.id.myDeviceRl) as ConstraintLayout
    var textView: TextView = itemView.findViewById<View>(R.id.txt_device_name) as TextView


    fun updateDevices(device: FhirConfigurationData) {

        imageView.setImageBitmap(device.deviceImage?.imageBase64?.toBitmap())
        textView.text= device.deviceImage?.display
    }
}
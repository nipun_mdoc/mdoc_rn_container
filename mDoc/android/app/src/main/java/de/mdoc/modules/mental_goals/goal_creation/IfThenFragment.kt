package de.mdoc.modules.mental_goals.goal_creation

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.MentalGoalsUtil.hasEmptyIfThenFields
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import de.mdoc.modules.mental_goals.mental_goal_adapters.IfThenAdapter
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_when_then.*

class IfThenFragment: MdocFragment() {
    lateinit var activity: MdocActivity
    private var adapterItems = arrayListOf<IfThenPlan>()

    override fun setResourceId(): Int {
        return R.layout.fragment_when_then
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        progressBar?.progress = 5
        setupAdapterData()
        val ifThenAdapter = IfThenAdapter(activity, adapterItems, false)
        rv_if_then?.layoutManager = LinearLayoutManager(
                activity,
                RecyclerView.HORIZONTAL,
                false
                                                       )
        rv_if_then?.adapter = ifThenAdapter
    }

    private fun setupAdapterData() {
        adapterItems.clear()

        if (!MentalGoalsUtil.ifThenItems.isNullOrEmpty()) {
            adapterItems = ArrayList(MentalGoalsUtil.ifThenItems ?: emptyList())
        }
        else {
            val adapterItem = IfThenPlan(null, 1, "", "")
            adapterItems.addAll(arrayListOf(adapterItem))
        }
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }

        txt_next?.setOnClickListener {
            if (hasEmptyIfThenFields(adapterItems)) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.fill_all_fields))
            }
            else {
                MentalGoalsUtil.ifThenItems = ArrayList(adapterItems)
                val action = IfThenFragmentDirections.actionIfThenFragmentToCreateGoalFragment()
                findNavController().navigate(action)
            }
        }

        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }

        ic_info?.setOnClickListener {
            val action = IfThenFragmentDirections.actionIfThenFragmentToIfThenInfoBottomSheetDialogFragment()
            findNavController().navigate(action)
        }

        btn_add_plan?.setOnClickListener {
            adapterItems.add(
                    IfThenPlan(null, adapterItems.size + 1, "", "")
                            )
            rv_if_then?.adapter?.notifyDataSetChanged()
            rv_if_then?.smoothScrollToPosition(adapterItems.size - 1)
        }
    }
}
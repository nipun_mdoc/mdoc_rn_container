package de.mdoc.modules.files.filter

import androidx.lifecycle.ViewModel
import de.mdoc.viewmodel.combine
import de.mdoc.viewmodel.liveData
import org.joda.time.LocalDate

class FilterViewModel(
    private val repository: FilterRepository
) : ViewModel() {

    private val initialFilter: FilesFilter = FilesFilter(categories = null, from = null, to = null)
    val from = liveData<LocalDate?>(null)
    val to = liveData<LocalDate?>(null)

    private val filterCategories = liveData<List<FilterCategory>>(emptyList())
    private val selectedFilters = liveData<Set<FilterCategory.SingleCategory>>(emptySet())

    val isClearFilterButtonEnabled =
        combine(from, to, selectedFilters) { fromValue, toValue, filters ->
            fromValue != null || toValue != null || filters.isNotEmpty()
        }

    val categoryFilterItems = combine(filterCategories, selectedFilters) { all, selected ->
        all.map {
            if (it is FilterCategory.AllCategories) {
                FilterItem(it, selected.isEmpty())
            } else {
                FilterItem(it, selected.contains(it))
            }
        }
    }

    init {
        repository.getInitialFilters { list ->
            from.set(initialFilter.from)
            to.set(initialFilter.to)
            filterCategories.set(list)
            val initCategories = initialFilter.categories
            selectedFilters.set(
                if (initCategories == null) {
                    emptySet()
                } else {
                    list.filterIsInstance(FilterCategory.SingleCategory::class.java).filter {
                        initCategories.contains(it.data)
                    }.toSet()
                }
            )
        }
        from.observe { fromValue ->
            val toValue = to.get()
            if (fromValue != null && toValue != null && fromValue.isAfter(toValue)) {
                to.set(fromValue)
            }
        }
        to.observe { toValue ->
            val fromValue = from.get()
            if (toValue != null && fromValue != null && toValue.isBefore(fromValue)) {
                from.set(toValue)
            }
        }
    }

    fun onCategoryClicked(category: FilterCategory) {
        val current = selectedFilters.get()
        val next = mutableSetOf<FilterCategory.SingleCategory>()
        if (category is FilterCategory.SingleCategory) {
            next.addAll(current)
            if (current.contains(category)) {
                next.remove(category)
            } else {
                next.add(category)
            }
        }
        selectedFilters.set(next)
    }

    fun onDeleteAllFiltersClicked() {
        from.set(null)
        to.set(null)
        selectedFilters.set(emptySet())
    }

    fun getFilter(): FilesFilter {
        val categories = selectedFilters.get()
            .map {
                it.data
            }
        return FilesFilter(
            categories = if (categories.isNotEmpty()) categories else null,
            from = from.get(),
            to = to.get()
        )
    }

}
package de.mdoc.modules.patient_journey.journey_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMediaTransitionBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.patient_journey.openMedia
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class MediaTransitionFragment: NewBaseFragment() {
    var mediaId =""
    override val navigationItem: NavigationItem = NavigationItem.FilesPreview
    private val args:MediaTransitionFragmentArgs by navArgs()
    private val mediaTransitionViewModel by viewModel {
        MediaTransitionViewModel(
                RestClient.getService(), mediaId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentMediaTransitionBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_media_transition, container, false)
        mediaId=args.mediaId?:""
        binding.lifecycleOwner = this
        binding.mediaTransitionViewModel = mediaTransitionViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mediaTransitionViewModel.proceed.observe(viewLifecycleOwner, Observer {
            if(it){
                openMedia(mediaTransitionViewModel.media.value, context)
                mediaTransitionViewModel.proceed.value = false
            }
        })
    }

    companion object {
        val BACK_STACK_TAG = MediaTransitionFragment::class.java.canonicalName
    }
}
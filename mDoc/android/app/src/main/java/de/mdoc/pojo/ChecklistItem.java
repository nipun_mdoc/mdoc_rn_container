package de.mdoc.pojo;

import java.io.Serializable;

public class ChecklistItem implements Serializable {
    private String title, introduction, total, current, date, checkListType, pollId, pollAssignId, pollVotingId, pollVotingActionId, defaultLanguage;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public String getTitle() {
        return title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public String getTotal() {
        return total;
    }

    public String getCurrent() {
        return current;
    }

    public String getDate() {
        return date;
    }

    public String getCheckListType() {
        return checkListType;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getPollVotingId() {
        return pollVotingId;
    }

    public void setPollVotingId(String pollVotingId) {
        this.pollVotingId = pollVotingId;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public String getPollVotingActionId() {
        return pollVotingActionId;
    }

    public void setPollVotingActionId(String pollVotingActionId) {
        this.pollVotingActionId = pollVotingActionId;
    }

    public ChecklistItem(String title, String introduction, String total, String current, String date, String checkListType, String pollId, String pollAssignId, String pollVotingId) {
        this.title = title;
        this.introduction = introduction;
        this.total = total;
        this.current = current;
        this.date = date;
        this.checkListType = checkListType;
        this.pollId = pollId;
        this.pollAssignId = pollAssignId;
        this.pollVotingId = pollVotingId;
    }

    public ChecklistItem(QuestionnaireData item) {
        this.title = item.getName();
        this.introduction = item.getIntroduction();
        this.total = String.valueOf(item.getAnswearsDetails().getTotal());
        this.current = String.valueOf(item.getAnswearsDetails().getCompleted());
        this.date = item.getCreated().toString();
        this.checkListType = item.getChecklistType();
        this.pollId = item.getPollId();
        this.pollAssignId = item.getPollAssignId();
        this.pollVotingId = item.getAnswearsDetails().getPollVotingActionId();
        this.pollVotingActionId = item.getAnswearsDetails().getPollVotingActionId();
        this.defaultLanguage = item.getDefaultLanguage();
    }
}

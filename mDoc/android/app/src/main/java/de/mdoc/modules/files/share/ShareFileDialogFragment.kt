package de.mdoc.modules.files.share

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.pojo.FileListItem
import de.mdoc.util.serializableArgument
import kotlinx.android.synthetic.main.share_file_with_clinic_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShareFileDialogFragment : DialogFragment() {

    private var item by serializableArgument<FileListItem>("file_item")

    fun <T> newInstance(item: FileListItem, listener: T): ShareFileDialogFragment
            where T : Fragment, T : ShareFileListener {
        return ShareFileDialogFragment().also {
            it.item = item
            it.setTargetFragment(listener, 0)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(de.mdoc.R.layout.share_file_with_clinic_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fileNameTv.text = item.name
        fileTitleTv.text = item.description

        iconIv.setImageResource(item.type.imageRes)

        cancelBtn.setOnClickListener {
            dismiss()
        }

        shareBtn.setOnClickListener {
            shareFile()

        }
    }

    private fun shareFile() {
        buttonsLl.visibility = View.INVISIBLE
        progress.showNow()

        MdocManager.shareFile(item.id, object : Callback<MdocResponse> {

            override fun onResponse(
                call: Call<MdocResponse>,
                response: Response<MdocResponse>
            ) {
                if (response.isSuccessful) {
                    contentLl.visibility = View.GONE
                    succRl.visibility = View.VISIBLE
                    (targetFragment as? ShareFileListener)?.onShareFileCompleted()
                } else {
                    contentLl.visibility = View.GONE
                    errorRl.visibility = View.VISIBLE
                }
                progress.hideNow()
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                progress.hideNow()
                contentLl.visibility = View.GONE
                errorRl.visibility = View.VISIBLE
            }
        })
    }


    override fun onResume() {
        // Sets the height and the width of the DialogFragment
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.setLayout(width, height)

        super.onResume()
    }
}

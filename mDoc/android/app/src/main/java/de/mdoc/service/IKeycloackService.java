package de.mdoc.service;

import de.mdoc.network.response.KeycloackLoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by ema on 7/21/17.
 */

public interface IKeycloackService {

    @FormUrlEncoded
    @POST("token")
    Call<KeycloackLoginResponse> loginWithKeyCloack(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("totp") String totp, @Field("device_name") String deviceName);

    @FormUrlEncoded
    @POST("token")
    Call<KeycloackLoginResponse> loginWithKeyCloackCode(@Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("code") String code, @Field("redirect_uri") String redirectUri, @Field("device_name") String deviceName);

    @FormUrlEncoded
    @POST("token")
    Call<KeycloackLoginResponse> loginWithChildAccount(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("totp") String totp, @Field("device_name") String deviceName, @Field("request_subject") String childUsername);

    @FormUrlEncoded
    @POST("token")
    Call<KeycloackLoginResponse> loginWithKeyCloackEmotp(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("emotp") String emotp, @Field("device_name") String deviceName);

    @FormUrlEncoded
    @POST("token")
    Call<KeycloackLoginResponse> sendRequestForEmail(@Field("username") String username, @Field("password") String password, @Field("grant_type") String grant_type, @Field("client_id") String client_id, @Field("client_secret") String client_secret, @Field("emotp") String emotp, @Field("device_name") String deviceName);

}

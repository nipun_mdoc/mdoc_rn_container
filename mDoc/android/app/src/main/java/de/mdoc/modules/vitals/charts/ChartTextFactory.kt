package de.mdoc.modules.vitals.charts

import android.app.Activity
import android.graphics.Typeface
import android.text.SpannableString
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.widget.TextView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.devices.data.ChartData
import de.mdoc.util.round
import java.text.DecimalFormat
import kotlin.math.roundToInt

fun fillChartText(chartData: ChartData) {
    val metricCodes = chartData.metrics ?: emptyList()
    val activity = chartData.context as Activity
    val rawData = chartData.rawData
    val txtAverage = activity.findViewById<TextView>(R.id.txt_average)
    val txtMinMax = activity.findViewById<TextView>(R.id.txt_min_max_value)
    val minPrefix = activity.getString(R.string.minimum)
    val maxPrefix = " / " + activity.getString(R.string.maximum)
    val df = DecimalFormat("#,###")
    val prefix = activity.getString(R.string.average)
    val totalPrefix = activity.getString(R.string.total_steps)

    when {
        metricCodes.contains(MdocConstants.METRIC_CODE_BLOOD_PRESSURE_SYS)-> {
            val sufixMeasurement=" mmHg"
            val systolicData = chartData.rawData?.getSystolicData()
            val diastolicData = chartData.rawData?.getDiastolicData()

            val averageMeasurementSys: Int = systolicData?.avg?.roundToInt() ?: 0
            val averageMeasurementDia: Int = diastolicData?.avg?.roundToInt() ?: 0

            val measurement = "$prefix $averageMeasurementSys SYS $sufixMeasurement  $averageMeasurementDia DIA $sufixMeasurement"
            txtAverage.text = measurement
            txtMinMax.text = ""

            val formatMinSys = df.format(systolicData?.min?:0).toString()
            val formatMaxSys = df.format(systolicData?.max?:0).toString()
            val formatMinDia = df.format(diastolicData?.min?:0).toString()
            val formatMaxDia = df.format(diastolicData?.max?:0).toString()
            val spanMinSys = SpannableString(formatMinSys)
            spanMinSys.setSpan(StyleSpan(Typeface.BOLD), 0, spanMinSys.length, 0)
            val spanMinDia = SpannableString(formatMinDia)
            spanMinDia.setSpan(StyleSpan(Typeface.BOLD), 0, spanMinDia.length, 0)
            val spanMaxSys = SpannableString(formatMaxSys)
            spanMaxSys.setSpan(StyleSpan(Typeface.BOLD), 0, spanMaxSys.length, 0)
            val spanMaxDia = SpannableString(formatMaxDia)
            spanMaxDia.setSpan(StyleSpan(Typeface.BOLD), 0, spanMaxDia.length, 0)

            txtMinMax.append("SYS ")
            txtMinMax.append(minPrefix)
            txtMinMax.append(spanMinSys)
            txtMinMax.append(maxPrefix)
            txtMinMax.append(spanMaxSys)
            txtMinMax.append("\n")
            txtMinMax.append("DIA ")
            txtMinMax.append(minPrefix)
            txtMinMax.append(spanMinDia)
            txtMinMax.append(maxPrefix)
            txtMinMax.append(spanMaxDia)
        }

        metricCodes.contains(MdocConstants.METRIC_CODE_STEPS_METER) -> {
            txtMinMax.text = ""
            var allSteps=0
            var minValue = 0.0
            var maxValue= 0.0

            if(!rawData!!.data.isNullOrEmpty()) {
                allSteps= rawData.data.first().data.list.sumBy { it.totalSum.toInt()}
                minValue = rawData.data.getOrNull(0)?.min?:0.0
                maxValue = rawData.data.getOrNull(0)?.max?:0.0
            }

            val formatSumMeasurement = df.format(allSteps)
            val spanMeasurement = SpannableString(formatSumMeasurement)
            spanMeasurement.setSpan(RelativeSizeSpan(1.4f), 0, spanMeasurement.length, 0)
            txtAverage.text = ""
            txtAverage.append(totalPrefix)
            txtAverage.append(" ")
            txtAverage.append(spanMeasurement)
            txtAverage.append(" ")
            txtAverage.append(chartData.unit)

            // Min max
            txtMinMax.text = "$minPrefix${formatSpan(minValue)}$maxPrefix${formatSpan(maxValue)}"
        }
        else->{
            var averageMeasurement = 0.0
            var minValue = 0.0
            var maxValue= 0.0
            txtMinMax.text = ""
            if(!rawData!!.data.isNullOrEmpty()){
                averageMeasurement = rawData.data.getOrNull(0)?.avg?:0.0
                minValue = rawData.data.getOrNull(0)?.min?:0.0
                maxValue = rawData.data.getOrNull(0)?.max?:0.0
            }

            val average = "$prefix ${averageMeasurement.round(1)} ${chartData.unit}"
            txtAverage.text = average
            txtMinMax.append(minPrefix)
            txtMinMax.append(formatSpan(minValue))
            txtMinMax.append(maxPrefix)
            txtMinMax.append(formatSpan(maxValue))
        }
    }
}

private fun formatSpan(inputInt:Double):SpannableString{
    val span = SpannableString(inputInt.round(1).toString())
    span.setSpan(StyleSpan(Typeface.BOLD), 0, span.length, 0)
    return span
}
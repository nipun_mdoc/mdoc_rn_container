package de.mdoc.modules.questionnaire.questions

import de.mdoc.constants.MdocConstants
import de.mdoc.interfaces.IPostAnswerForMatrix
import de.mdoc.pojo.Answer
import de.mdoc.pojo.ExtendedQuestion

fun createQuestion(question: ExtendedQuestion?, listener: IPostAnswerForMatrix): QuestionType {
    return when (question?.pollQuestionType) {
        MdocConstants.SCORING_SCALE -> {
            if (question.questionData.scoringScaleOrientation == "HORIZONTAL") {
                ScoringScaleQuestionType()
            } else {
                ScoringScaleQuestionVerticalType()
            }
        }
        MdocConstants.TEXT -> TextQuestionType()
        MdocConstants.INPUT -> InputQuestionType()
        MdocConstants.RADIO -> RadioQuestionType()
        MdocConstants.CHECKBOX -> CheckboxQuestionType()
        MdocConstants.RATING -> RatingQuestionType()
        MdocConstants.DROPDOWN -> DropdownQuestionType()
        MdocConstants.RADIO_MATRIX -> MatrixQuestionType(listener)

        else -> InputQuestionType() // fallback type to prevent crash in case when new type introduced on backend
    }
}
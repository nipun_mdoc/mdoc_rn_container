package de.mdoc.data.create_bt_device

data class CodeObservation(
        val coding: ArrayList<CodingObservation>){

    fun loincCode():String{
        for(item in coding){
            if(item.system == "http://loinc.org"){
                return item.code
            }
        }
        return ""
    }
    fun metricDescription():String{
        for(item in coding){
            if(item.system == "http://loinc.org"){
                return item.display
            }
        }
        return ""
    }
}
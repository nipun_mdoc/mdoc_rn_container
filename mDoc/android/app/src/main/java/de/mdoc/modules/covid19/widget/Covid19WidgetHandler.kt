package de.mdoc.modules.covid19.widget

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper

class Covid19WidgetHandler {

    fun openCovidModule(context: Context) {
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(R.id.navigation_covid19)
    }

    fun openEmergencyContacts(context: Context) {
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(R.id.navigation_emergency_contacts)
    }

    fun setTestStatus(healthStatus: HealthStatus?): String {
        var status = healthStatus?.healthStatus ?: ""
        AppPersistence.covidStatusCoding?.forEach {
            if (status == it.code) {
                status = it.display ?: ""
            }
        }
        return status
    }

    fun setEmergencyCardText(context: Context): String {
        return if (MdocAppHelper.getInstance()
                .publicUserDetailses.hasValidEmergencyContact() || !MdocAppHelper.getInstance().emergencyInfo.isNullOrEmpty()) {
            context.resources.getString(R.string.covid_widget_emergency_show_data)
        }
        else {
            context.resources.getString(R.string.covid_widget_emergency_description)
        }
    }

    fun setBackgroundTint(context: Context, healthStatus: HealthStatus?): ColorStateList {
        val color = when (healthStatus?.healthStatus ?: "") {
            CovidStatusChangeBottomSheetFragment.POSITIVE -> {
                R.color.covid_positive
            }

            CovidStatusChangeBottomSheetFragment.NEGATIVE -> {
                R.color.covid_negative
            }

            else                                          -> {
                R.color.gray_e6e6e6
            }
        }

        return ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }
}
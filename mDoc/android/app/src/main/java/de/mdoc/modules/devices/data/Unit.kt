package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Unit(
        val active: Boolean?=null,
        val code: String?=null,
        val display: String?=null,
        val system: String?=null
): Parcelable

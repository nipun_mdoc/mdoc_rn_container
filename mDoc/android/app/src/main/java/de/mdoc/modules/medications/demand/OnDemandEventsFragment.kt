package de.mdoc.modules.medications.demand

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMedicationsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.MedicationsViewModel
import de.mdoc.modules.medications.adapters.MedicationOnDemandAdapter
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_medications.*

class OnDemandEventsFragment : NewBaseFragment() {

    private val medicationsViewModel by viewModel {
        MedicationsViewModel(
                RestClient.getService())
    }

    override val navigationItem: NavigationItem
        get() = NavigationItem.EMPTY

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMedicationsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_medications, container, false)
        binding.lifecycleOwner = this
        binding.medicationsViewModel = medicationsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        medicationsViewModel.onDemandEvents.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                rv_medication.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = MedicationOnDemandAdapter(it, medicationsViewModel)
                }
            }
        })

        medicationsViewModel.getOnDemandEvents()
    }
}
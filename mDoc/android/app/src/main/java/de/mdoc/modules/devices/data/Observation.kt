package de.mdoc.modules.devices.data

data class Observation(
        val avg: Double,
        val count: Int,
        val cts: Long,
        val id: String,
        val max: Double,
        val metricCode: String,
        val min: Double,
        val period: Long,
        val periodType: String,
        val totalSum: Double,
        val user: User,
        val uts: Long,
        val unit:String,
        val statisticByAllUnits:List<Observation>

)
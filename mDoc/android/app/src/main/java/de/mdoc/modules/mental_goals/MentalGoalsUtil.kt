package de.mdoc.modules.mental_goals

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan

object MentalGoalsUtil {
    var goalId: String = ""
    var goalTitle: String = ""
    var goalDescription: String = ""
    var timePeriod: Long = 0
    var ifThenItems: ArrayList<IfThenPlan>? = null
    var actionItems: ArrayList<ActionItem>? = null
    var status: String = ""
    var coachCreated = false
    var customDateSelected = false
    var showCreatedMessage = false
    var statusChange: String = ""

    fun clearData() {
        goalDescription = ""
        goalTitle = ""
        status = ""
        timePeriod = 0
        ifThenItems = null
        actionItems = null
        customDateSelected = false
        coachCreated = false
    }

    fun clearGoalStatus() {
        statusChange = ""
    }

    fun hasEmptyIfThenFields(adapterItems: ArrayList<IfThenPlan>): Boolean {
        for (item in adapterItems) {
            if (item.ifValue.isEmpty() || item.thenValue.isEmpty()) {
                return true
            }
        }
        return false
    }

    fun hasEmptyActionItems(actionItems: ArrayList<ActionItem>): Boolean {
        for (item in actionItems) {
            if (item.item.isNullOrEmpty()) {
                return true
            }
        }
        return false
    }

    fun hideKeyboard(activity: Activity) {
        val inputManager = activity
            .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        // check if no view has focus:
        val currentFocusedView = activity.currentFocus
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    }
}
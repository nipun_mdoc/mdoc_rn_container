package de.mdoc.modules.questionnaire.finish

import androidx.lifecycle.ViewModel
import de.mdoc.viewmodel.liveData

class ScoringResultViewModel(
    private val pollVotingId: String,
    private val shouldLoadScoringResult: Boolean,
    private val resultScreenTypeCode: String
) : ViewModel() {

    private val repository = ScoringResultRepository()

    val inProgress = liveData<Boolean>()
    val results = liveData<List<VotingScoreResultModel>>()

    init {
        if (shouldLoadScoringResult) {
            inProgress.set(true)
            repository.loadScoringResult(pollVotingId, resultScreenTypeCode) {
                inProgress.set(false)
                results.set(it)
            }
        } else {
            results.set(emptyList())
        }
    }

}

class VotingScoreResultModel(
    val label: String?,
    val title: String?,
    val description: String?,
    val score: String?,
    val color: ScoreColor?,
    val resultScreenType: String?
)
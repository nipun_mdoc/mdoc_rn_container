package de.mdoc.data.responses

import de.mdoc.data.create_bt_device.BtDevice

data class SingleDeviceResponse(
        val data: BtDevice
){
    fun getManualDeviceId():String?{
        return data.id
    }
}
package de.mdoc.data.create_bt_device

data class BtDevice(
        val contact: List<Any>,
        val cts: Long,
        val extension: List<Extension>,
        val id: String,
        val identifier: List<Identifier>,
        val manufactureDate: Long,
        val model: String,
        val modifierExtension: List<Any>,
        val note: List<Any>,
        val patient: Patient,
        val resourceType: String,
        val safety: List<Any>,
        val status: String,
        val type: Type,
        val udi: Udi?=null,
        val uts: Long
){
    fun getObservationTime():Long{

        for(item in extension){
            if(item.url=="https://mdoc.one/fhir/extensions/last_observation"){
                return item.valueDate
            }
        }
        return 0
    }
}

data class Extension(
        val valueDate:Long,
        val url:String
)
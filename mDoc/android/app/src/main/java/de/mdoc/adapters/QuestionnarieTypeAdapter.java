package de.mdoc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.QuestionnaireData;

/**
 * Created by ema on 12/26/16.
 */

public class QuestionnarieTypeAdapter extends BaseAdapter {

    ArrayList<QuestionnaireData> data;
    Context context;
    Typeface light;

    public QuestionnarieTypeAdapter(Context context, ArrayList<QuestionnaireData> data){
        this.context = context;
        this.data = data;
        this.light = ResourcesCompat.getFont(context, R.font.hk_grotesk_regular);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView =  LayoutInflater.from(context).inflate(R.layout.questionnarie_type_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        QuestionnaireData item = data.get(position);

        holder.questionnarieTypeName.setText(item.getName());
        holder.questionnarieTypeName.setTypeface(light);
        return convertView;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {

        @BindView(R.id.questionnarieTypeName)
        TextView questionnarieTypeName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

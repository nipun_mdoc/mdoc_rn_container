package de.mdoc.network.response

import de.mdoc.network.RestClient
import de.mdoc.network.response.APIError.Companion.fromResponse
import retrofit2.Response
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.concurrent.getOrSet

/**
 * Created by ema on 11/3/16.
 */

private val format = ThreadLocal<DateFormat>()

fun Response<*>.getErrorDetails(): String {
    return try {
        fromResponse(this)?.toFormattedString() ?: this.toString()
    } catch (e: Exception) {
        Timber.w(e)
        this.toString()
    }
}

data class APIError(
    val code: String,
    val timestamp: String,
    val description: String,
    val message: String,
    val status: String
) {

    companion object {
        fun fromResponse(response: Response<*>): APIError? {
            return try {
                val body = response.errorBody()
                if (body != null) {
                    val converter = RestClient.getRetrofit()
                        .responseBodyConverter<APIError>(APIError::class.java, arrayOfNulls(0))
                    converter.convert(body)
                } else null
            } catch (e: Exception) {
                null
            }
        }
    }

    fun toFormattedString(): String {
        val formatter = format.getOrSet {
            SimpleDateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.FULL)
        }
        val time = try {
            formatter.format(Date(timestamp))
        } catch (e: Exception) {
            Timber.w(e)
            "Timestamp=$timestamp"
        }
        return "$code $time $description $message $status"
    }
}

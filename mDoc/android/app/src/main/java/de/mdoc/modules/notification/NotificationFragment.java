package de.mdoc.modules.notification;

import android.app.AlertDialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.modules.notification.adapters.NotificationAdapter;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.NotificationSeenRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.network.response.NotificationResponse;
import de.mdoc.pojo.NotificationListItem;
import de.mdoc.util.FragmentExtensionsKt;
import de.mdoc.util.MdocAppHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 6/22/17.
 */

public class NotificationFragment extends MdocFragment {

    @BindView(R.id.recylerViewNotification)
    RecyclerView notificationsRv;

    MdocActivity activity;
    public NotificationAdapter adapter;


    @Override
    protected int setResourceId() {
        return R.layout.fragment_notification;
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Notifications;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        setHasOptionsMenu(true);

        getNotifications();

        notificationsRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(notificationsRv);
    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            final int position = viewHolder.getAdapterPosition(); //get position which is swipe

            if (direction == ItemTouchHelper.LEFT) {

                AlertDialog.Builder builder = new AlertDialog.Builder(activity); //alert for confirm to delete
                builder.setMessage(R.string.delete_notification_message);

                builder.setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() { //when click on DELETE
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NotificationListItem notification = MdocAppHelper.getInstance().getNotificationByPosition(position);
                        if (notification != null) {
                            deleteSingleNotification(notification.getId());
                            adapter.notifyItemRemoved(position);
                            MdocAppHelper.getInstance().deleteNotification(notification.getId());
                        }
                    }

                }).setNegativeButton(R.string.cancellation, new DialogInterface.OnClickListener() {  //not removing items if cancel is done
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.notifyItemRemoved(position + 1);    //notifies the RecyclerView Adapter that data in adapter has been removed at a particular position.
                        adapter.notifyItemRangeChanged(position, adapter.getItemCount());   //notifies the RecyclerView Adapter that positions of element in adapter has been changed from position(removed element index to end of list), please update it.
                    }
                }).show();  //show alert dialog
            }
        }
    };

    private void deleteSingleNotification(String id) {
        MdocManager.deleteNotification(id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(isAdded()) {
                    MdocAppHelper.getInstance().deleteNotification(id);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void deleteAllNotifcations() {
        MdocManager.deleteAllNotifications(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(isAdded()) {
                    MdocAppHelper.getInstance().deleteAllNotifications();
                    adapter.notifyDataSetChanged();
                    notificationsRv.setAdapter(null);
                    MdocAppHelper.getInstance().setAllNotificationsSeen();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void getNotifications() {
        MdocManager.postForAllNotifcations(0, MdocConstants.PUSH_NOTIFICATION_LIST_LIMIT,getContext(),  new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        ArrayList<NotificationListItem> data = response.body().getData().getList();
                        MdocAppHelper.getInstance().setNotifications(data);
                        adapter = new NotificationAdapter(activity, data);
                        notificationsRv.setAdapter(adapter);

                        ArrayList<String> notiIDs = new ArrayList<>();

                        for (NotificationListItem item : data) {
                            notiIDs.add(item.getId());
                        }
                        setSeenNotifications(notiIDs);
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
            }
        });
    }

    private void setSeenNotifications(ArrayList<String> ids) {
        NotificationSeenRequest request = new NotificationSeenRequest();
        request.setNotificationIds(ids);
        request.setSeen(true);

        MdocManager.setAllNotificationsSeen(request, new Callback<MdocResponse>() {
            @Override
            public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        Timber.v("Response is success");
                        MdocAppHelper.getInstance().setAllNotificationsSeen();
                    } else {
                        Timber.w("setAllNotificationsSeen %s", APIErrorKt.getErrorDetails(response));
                    }
                }
            }

            @Override
            public void onFailure(Call<MdocResponse> call, Throwable t) {
                Timber.w(t, "setAllNotificationsSeen");
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        FragmentExtensionsKt.onCreateOptionsMenu(this, menu, inflater, R.menu.menu_notifications);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.clearAllNotifications) {
            deleteAllNotifcations();
        }
        return super.onOptionsItemSelected(item);
    }
}

package de.mdoc.modules.airandpollen.data

import android.content.Context
import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import de.mdoc.R

enum class PollenLevel(
    @StringRes val nameRes: Int,
    @ColorRes val colorRes: Int
) {
    None(R.string.pollen_level_none, R.color.pollen_level_0),
    Low(R.string.pollen_level_low, R.color.pollen_level_1),
    Medium(R.string.pollen_level_medium, R.color.pollen_level_2),
    High(R.string.pollen_level_high, R.color.pollen_level_3);


    fun getName(context: Context): String {
        return context.getString(nameRes)
    }

    fun getColor(context: Context): Int {
        return ContextCompat.getColor(context, colorRes)
    }

}

data class AirAndPollenInfo(
    val type: PollenType,
    val amount: Int
) {

    val level: PollenLevel = when {
        amount in 1..2 -> PollenLevel.Low
        amount in 3..4 -> PollenLevel.Medium
        amount >= 5 -> PollenLevel.High
        else -> PollenLevel.None
    }

}

fun List<AirAndPollenInfo>.hasAmountData (): Boolean {
    return find { it.amount >= 0 } != null
}
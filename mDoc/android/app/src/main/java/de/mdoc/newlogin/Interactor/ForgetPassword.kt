package de.mdoc.newlogin.Interactor

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.mdoc.R
import de.mdoc.databinding.ActivityForgetPasswordBinding
import de.mdoc.newlogin.AuthListener
import de.mdoc.newlogin.ForgetViewModel
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.activity_forget_password.*
import kotlinx.android.synthetic.main.activity_forget_password.txtEmailAddress
import kotlinx.android.synthetic.main.activity_logi.*
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.*
import de.mdoc.newlogin.Model.ForgetPasswordRequestData

class ForgetPassword : AppCompatActivity(), AuthListener {

    private var forgetViewModel: ForgetViewModel? = null
    private var binding: ActivityForgetPasswordBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        forgetViewModel = ViewModelProviders.of(this).get(ForgetViewModel::class.java)
        binding = DataBindingUtil.setContentView(this@ForgetPassword, R.layout.activity_forget_password)
        binding?.setLifecycleOwner(this)
        binding?.setForgetViewModel(forgetViewModel)

        forgetViewModel?.authListener = this

//        if(MdocAppHelper.getInstance().username != null) {
//            txtEmailAddress.setText(MdocAppHelper.getInstance().username)
//            forgetViewModel?.uesr_edit?.value = MdocAppHelper.getInstance().username
//            txtEmailAddress.setSelection(MdocAppHelper.getInstance().username.length)
//        }

        var shakeAnimation: Animation? = AnimationUtils.loadAnimation(this, R.anim.shake)
        forgetViewModel!!.user.observe(this, Observer<ForgetPasswordRequestData> { userRequestModel ->

            txtLoginLayout.setError(null);

            if (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.username)) {
                txtLoginLayout?.setError(" ")
                txtLoginLayout.errorIconDrawable = null
                txtLoginLayout?.requestFocus();
                txtLoginLayout.startAnimation(shakeAnimation)
            }  else {
                var loginUserData = ForgetPasswordRequestData()
                forgetViewModel?.loadData((userRequestModel)!!.username!!, loginUserData, this)
            }
        })

    }
    override fun sucess() {
        finish()
//        Toast.makeText(applicationContext, "Success1", Toast.LENGTH_SHORT).show()
    }

    override fun failer() {
        Toast.makeText(applicationContext, "Failure", Toast.LENGTH_SHORT).show()
    }

    override fun onLoadCaptcha(response: CaptchaResponse?) {
    }

    override fun isCaptchaVisible(): Boolean? {
        return linearCaptchaMain?.isVisible
    }

    override fun clearCaptchaField() {

    }

    override fun changePassword(token: String) {
        TODO("Not yet implemented")
    }

    override fun showMobileNumberScreen() {
        TODO("Not yet implemented")
    }

    override fun showOTPScreen(){
        TODO("Not yet implemented")
    }

    override fun onImpressumClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)    }
    override fun onDatenschutzDieClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)  }
    override fun onAGBClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)   }

    override fun onCloseClick() {
        finish()
    }

    override fun onScanClick() {
        TODO("Not yet implemented")
    }

}
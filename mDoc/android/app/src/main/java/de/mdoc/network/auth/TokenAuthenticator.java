package de.mdoc.network.auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.network.response.KeycloackLoginResponse;
import de.mdoc.newlogin.Interactor.LogiActivty;
import de.mdoc.util.MdocAppHelper;
import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

/**
 * Created by ema on 7/21/17.
 */

public class TokenAuthenticator implements Authenticator {
    private final String baseUrl;
    private final Context context;

    public TokenAuthenticator(Context context, String baseUrl) {
        this.context = context;
        this.baseUrl = baseUrl;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        if (calculateResponseCount(response) >= 2) {
            return null; // If we've failed 2 times, give up.
        }

        int responseCode = refreshToken(baseUrl, MdocAppHelper.getInstance().getRefreshTokenNoHeader(), context.getString(R.string.keycloack_clinic_Id), context.getString(R.string.client_secret)); // isRefreshedToken();

        if (responseCode == 200) {
            //refresh is successful
            String newaccess = MdocAppHelper.getInstance().getAccessToken();

            // make current request with new access token
            return response.request().newBuilder()
                    .header("Authorization", newaccess)
                    .build();

        } else {
            // refresh failed , maybe you can logout user
            // returning null is critical here , because if you do not return null
            // it will try to refresh token continuously like 1000 times.
            // also you can try 2-3-4 times by depending you before logging out your user
            Intent i = new Intent(context, LogiActivty.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(LoginActivity.REFRESH_TOKEN_RESPONSE_CODE, responseCode);
                context.startActivity(i);
                if (context instanceof Activity) {
                    ((Activity) context).finish();
                }
            return null;
        }
    }

    private int calculateResponseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }

    private int refreshToken(String url,String refresh,String cid,String csecret) throws IOException{
        URL refreshUrl=new URL(url+"token");
        HttpURLConnection urlConnection = (HttpURLConnection) refreshUrl.openConnection();
        urlConnection.setDoInput(true);
        urlConnection.setRequestMethod("POST");
        urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        urlConnection.setUseCaches(false);
        String urlParameters  = "grant_type=refresh_token&client_id="+cid+"&client_secret="+csecret+"&refresh_token="+refresh;

        urlConnection.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = urlConnection.getResponseCode();

        if(responseCode==200){
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            // this gson part is optional , you can read response directly from Json too
            Gson gson = new Gson();
            KeycloackLoginResponse refreshTokenResult=gson.fromJson(response.toString(),KeycloackLoginResponse.class);
            MdocAppHelper.getInstance().setAccessToken(refreshTokenResult.getToken_type() + " " + refreshTokenResult.getAccess_token());
            MdocAppHelper.getInstance().setRefreshTokenNoHeader(refreshTokenResult.getRefresh_token());

        }
        return responseCode;
    }
}

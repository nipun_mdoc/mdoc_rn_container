package de.mdoc.modules.devices.data

import java.io.Serializable

data class ScaleUser(var dob: Long? = null,
                     var gender: Int? = null,
                     var height: Int? = null,
                     var activityLevel: Int? = null): Serializable
package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import android.os.Build
import de.mdoc.interfaces.BleResponseListener
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList


class LungBleWrapper(private val context: WeakReference<Context>, deviceAddress: String) {

    private var bluetoothDevice: BluetoothDevice? = null
    private var myBleCallback: MyBleCallback? = null

    private var characteristicWrite: BluetoothGattCharacteristic? = null
    private var acquiredMeasurementResponseArrayList: ArrayList<Byte>? = null
    private var bleListener: WeakReference<BleResponseListener>? = null
    private var isFirstDescriptor: Boolean = true

    fun setBleResponseListener(bleListener: WeakReference<BleResponseListener>) {
        this.bleListener = bleListener
    }

    init {
        myBleCallback = MyBleCallback(WeakReference<LungBleWrapper>(this))

        context.get()?.let {
            bluetoothDevice = getBluetoothDevice(it, deviceAddress)
        }
    }

    fun connect(autoConnect: Boolean) {
        doConnect(autoConnect)
    }

    private fun doConnect(autoConnect: Boolean) {
        context.get()?.let {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                bluetoothDevice?.connectGatt(it, autoConnect, myBleCallback, BluetoothDevice.TRANSPORT_LE)
            } else {
                bluetoothDevice?.connectGatt(it, autoConnect, myBleCallback)
            }
        }
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }


    private inner class MyBleCallback(val weakReference: WeakReference<LungBleWrapper>) : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)

            if (status == BluetoothGatt.GATT_SUCCESS) {
                when (newState) {
                    BluetoothGatt.STATE_CONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                        gatt.discoverServices()
                    }
                    BluetoothGatt.STATE_DISCONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_DISCONNECTED $gatt $status")
                        gatt.close()
                    }
                }
            } else if (status == 133) {
                // Error
                Timber.d("onConnectionStateChange: STATE_ERROR $gatt $status")
                gatt.close()
                connect(false)
            } else {
                gatt.close()
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Timber.d("onServicesDiscovered: $gatt")
                writeIndication(gatt, CHARACTERISTIC_WRITE_1)
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {

                if (weakReference.get()?.isFirstDescriptor == true) {
                    weakReference.get()?.isFirstDescriptor = false

                    val characteristicNotification = gatt.getService(UUID.fromString(CUSTOM_SERVICE)).getCharacteristic(UUID.fromString(CHARACTERISTIC_WRITE_2))

                    gatt.setCharacteristicNotification(characteristicNotification, true)

                    val descriptorLocal = characteristicNotification.getDescriptor(UUID.fromString(CHARACTERISTIC_NOTIFICATION_DESC))

                    descriptorLocal.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                    gatt.writeDescriptor(descriptorLocal)

                } else {

                    if (weakReference.get()?.acquiredMeasurementResponseArrayList == null) {
                        weakReference.get()?.acquiredMeasurementResponseArrayList = ArrayList()
                    }

                    weakReference.get()?.characteristicWrite = gatt
                            .getService(UUID.fromString(CUSTOM_SERVICE))
                            .getCharacteristic(UUID.fromString(CHARACTERISTIC_WRITE_3))

                    weakReference.get()?.characteristicWrite?.value = TIO_COMMAND

                    gatt.writeCharacteristic(weakReference.get()?.characteristicWrite)
                }

            } else {
                Timber.d("onDescriptorWrite: FAILED$gatt")
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
            //  Get response of service in raw data, concentrate and show in human readable format.
            super.onCharacteristicChanged(gatt, characteristic)

            Timber.d("onCharacteristicChanged: %s", Arrays.toString(characteristic.value))

            for (item in characteristic.value) {
                weakReference.get()?.acquiredMeasurementResponseArrayList?.add(item)
            }

            val measurementsResponse = weakReference.get()?.acquiredMeasurementResponseArrayList
            Timber.d("DeviceOutputOnCharacteristicChanged: %s", measurementsResponse?.toString())

            if (measurementsResponse != null &&
                    measurementsResponse.size > 18 &&
                    measurementsResponse.getOrNull(measurementsResponse.size - 2) == 0x03.toByte()) {

                weakReference.get()?.bleListener?.get()?.dataFromDeviceCallback(measurementsResponse, gatt)
                gatt.disconnect()

                Timber.d("Last data: %s", measurementsResponse.getOrNull(measurementsResponse.size))
                Timber.d("Last data: all $measurementsResponse")
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            Timber.i("onCharacteristicRead $characteristic")
            val value = characteristic!!.value
            Timber.i("onCharacteristicRead Value: ${String(value)}")
        }
    }

    private fun writeIndication(gatt: BluetoothGatt, characteristic: String) {
        Timber.i("writeIndication Value: $characteristic")

        val characteristicNotificationOne = gatt.getService(UUID.fromString(CUSTOM_SERVICE))
                .getCharacteristic(UUID.fromString(characteristic))

        characteristicNotificationOne.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

        gatt.setCharacteristicNotification(characteristicNotificationOne, true)

        val descriptorOne = characteristicNotificationOne.getDescriptor(
                UUID.fromString(CHARACTERISTIC_NOTIFICATION_DESC))
        descriptorOne.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        gatt.writeDescriptor(descriptorOne)
    }

    companion object {
        private const val CUSTOM_SERVICE = "0000fefb-0000-1000-8000-00805f9b34fb"
        private const val CHARACTERISTIC_WRITE_1 = "00000004-0000-1000-8000-008025000000"
        private const val CHARACTERISTIC_WRITE_2 = "00000002-0000-1000-8000-008025000000"
        private const val CHARACTERISTIC_WRITE_3 = "00000003-0000-1000-8000-008025000000"
        private const val CHARACTERISTIC_NOTIFICATION_DESC = "00002902-0000-1000-8000-00805f9b34fb"
        private val TIO_COMMAND = byteArrayOf(0xff.toByte())
    }
}
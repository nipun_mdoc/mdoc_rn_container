package de.mdoc.modules.media.data

data class MediaLightItem(
    val basedOn: List<Any?>? = listOf(),
    val bodySite: List<Any?>? = listOf(),
    val bucket: String? = "",
    val categories: List<String?>? = listOf(),
    val category: String?,
    val categorySystem: String? = "",
    val content: Content? = Content(),
    val extension: List<Any?>? = listOf(),
    val hidden: Boolean? = false,
    val id: String? = "",
    val identifier: List<Any?>? = listOf(),
    val mediaSharing: MediaSharing? = MediaSharing(),
    val meta: Meta? = Meta(),
    val modifierExtension: List<Any?>? = listOf(),
    val name: String? = "",
    val note: List<Note?>? = listOf(),
    val occurrenceDateTime: Long? = 0,
    val `operator`: Operator? = Operator(),
    val publisherFullName: String? = "",
    val reasonCode: List<Any?>? = listOf(),
    val resourceType: String? = "",
    val thumbails: List<Thumbail?>? = listOf(),
    val type: String? = "",

    val mediaType: String? = "",
    val publicUrl: String? = "",
    val pdfId: String? = ""
) {
    data class Content(
        val contentType: String? = "",
        val contentTypeWellKnown: String? = "",
        val `data`: Data? = Data(),
        val title: String? = "",
        val url: String? = ""
    ) {
        data class Data(
            val value: String? = ""
        )
    }

    data class MediaSharing(
        val clinicDepartment: List<ClinicDepartment?>? = listOf(),
        val shareWithAll: Boolean? = false,
        val userIdList: List<Any?>? = listOf()
    ) {
        data class ClinicDepartment(
            val departmentIdList: List<Any?>? = listOf()
        )
    }

    data class Meta(
        val profile: List<Any?>? = listOf(),
        val security: List<Any?>? = listOf(),
        val tag: List<Tag?>? = listOf()
    ) {
        data class Tag(
            val active: Boolean? = false,
            val code: String? = "",
            val display: String? = "",
            val system: String? = ""
        )
    }

    data class Note(
        val authorReference: AuthorReference? = AuthorReference(),
        val authorString: String? = "",
        val text: String? = ""
    ) {
        data class AuthorReference(
            val reference: String? = ""
        )
    }

    data class Operator(
        val reference: String? = ""
    )

    data class Thumbail(
        val basedOn: List<Any?>? = listOf(),
        val bodySite: List<Any?>? = listOf(),
        val bucket: String? = "",
        val categories: List<String?>? = listOf(),
        val category: String? = "",
        val categorySystem: String? = "",
        val content: Content? = Content(),
        val extension: List<Any?>? = listOf(),
        val hidden: Boolean? = false,
        val id: String? = "",
        val identifier: List<Identifier?>? = listOf(),
        val mediaSharing: MediaSharing? = MediaSharing(),
        val meta: Meta? = Meta(),
        val modifierExtension: List<Any?>? = listOf(),
        val name: String? = "",
        val note: List<Note?>? = listOf(),
        val occurrenceDateTime: Long? = 0,
        val `operator`: Operator? = Operator(),
        val reasonCode: List<Any?>? = listOf(),
        val resourceType: String? = "",
        val type: String? = "",
        val publicUrl: String? = ""
    ) {
        data class Content(
            val contentType: String? = "",
            val contentTypeWellKnown: String? = "",
            val `data`: Data? = Data(),
            val title: String? = "",
            val url: String? = ""
        ) {
            class Data(
            )
        }

        data class Identifier(
            val value: String? = ""
        )

        data class MediaSharing(
            val clinicDepartment: List<ClinicDepartment?>? = listOf(),
            val shareWithAll: Boolean? = false,
            val userIdList: List<Any?>? = listOf()
        ) {
            data class ClinicDepartment(
                val departmentIdList: List<Any?>? = listOf()
            )
        }

        data class Meta(
            val profile: List<Any?>? = listOf(),
            val security: List<Any?>? = listOf(),
            val tag: List<Tag?>? = listOf()
        ) {
            fun getDisplayByCode(code: String?):String? {
              tag?.forEach {
                    if(it?.code==code){
                        return it?.display
                    }
                }
                return ""
            }
            data class Tag(
                val active: Boolean? = false,
                val code: String? = "",
                val display: String? = "",
                val system: String? = ""
            )
        }

        data class Note(
            val authorReference: AuthorReference? = AuthorReference(),
            val authorString: String? = "",
            val text: String? = ""
        ) {
            data class AuthorReference(
                val reference: String? = ""
            )
        }

        data class Operator(
            val reference: String? = ""
        )
    }
}
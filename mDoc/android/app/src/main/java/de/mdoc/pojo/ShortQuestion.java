package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by ema on 5/24/17.
 */

public class ShortQuestion implements Serializable{

    protected String pollId;
    protected String pollAssignId;
    protected String id;
    protected ArrayList<OptionExtension> options;
    protected Map<String,String> question;
    protected Map<String,String> description;

    public ShortQuestion(String pollId, String pollAssignId, String id, ArrayList<OptionExtension> options, Map<String,String> question, Map<String,String> description) {
        this.pollId = pollId;
        this.pollAssignId = pollAssignId;
        this.id = id;
        this.options = options;
        this.question = question;
        this.description = description;
    }

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<OptionExtension> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<OptionExtension> options) {
        this.options = options;
    }

    public String getQuestion(String language) {
        return question.get(language);
    }

    public void setQuestion(Map<String,String> question) {
        this.question = question;
    }

    public String getDescription(String language) {
        return description.get(language);
    }

    public void setDescription(Map<String,String> description) {
        this.description = description;
    }

}

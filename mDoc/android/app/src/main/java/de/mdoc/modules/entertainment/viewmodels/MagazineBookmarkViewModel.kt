package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.entertainment.fragments.overview.MagazinesOverviewFragment
import de.mdoc.network.request.MediaReadingHistoryUpsert
import de.mdoc.pojo.entertainment.MediaReadingHistoryResponseDto
import de.mdoc.service.IMdocService
import de.mdoc.util.KBus
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class MagazineBookmarkViewModel(private val mDocService: IMdocService, private val mediaId: String? = null) : ViewModel() {

    var bookmark = MutableLiveData<MediaReadingHistoryResponseDto>()

    init {
        if (mediaId != null) {
            getMagazineReadingHistory(mediaId)
        }
    }

    fun addOrRemove(pageNumber: Int) {

        // Case when there is no bookmarks for current magazines
        if (bookmark.value == null) {
            getAndUpdateMagazineReadingHistory(mediaId, pageNumber, arrayOf(pageNumber))
        } else {
            when (bookmark.value?.bookMarkedPages?.contains(pageNumber)) {

                // Case when bookmark should be added
                false -> {
                    var updated = bookmark.value
                    updated?.bookMarkedPages?.add(pageNumber)
                    viewModelScope.launch {
                        withContext(NonCancellable) {
                            updateBookmark(updated)
                        }
                    }

                }
                // Case when bookmark should be removed
                else -> {
                    var updated = bookmark.value
                    val elementForDelete = updated?.bookMarkedPages?.find { it -> it == pageNumber }
                    if (elementForDelete != null) {
                        updated?.bookMarkedPages?.remove(elementForDelete)
                        viewModelScope.launch {
                            withContext(NonCancellable) {
                                updateBookmark(updated)
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun addBookmark(bok: Array<Int>?, page: Int?, read: Boolean) {
        val addRequest = MediaReadingHistoryUpsert(mediaId = mediaId, bookMarkedPages = bok, pageNumber = page, read = read)
        try {
            val response = mDocService.createMagazineReadingHistory(addRequest)
            if (response.data != null) {
                bookmark.value = response.data
            }
            KBus.post(MagazinesOverviewFragment.StatisticRefresh())

        } catch (e: java.lang.Exception) {
            Timber.d(e)
        }
    }

    private suspend fun updateBookmark(bookmark: MediaReadingHistoryResponseDto?) {
        val updateRequest = MediaReadingHistoryUpsert(mediaId = mediaId, bookMarkedPages = bookmark?.bookMarkedPages?.toTypedArray(), pageNumber = bookmark?.pageNumber, patientId = bookmark?.patientId, read = bookmark?.read)
        try {

            val response = mDocService.updateMagazineReadingHistory(bookmark?.id, updateRequest)
            if (response.data != null) {
                this@MagazineBookmarkViewModel.bookmark.value = response.data
            }
            KBus.post(MagazinesOverviewFragment.StatisticRefresh())

        } catch (e: java.lang.Exception) {
            Timber.d(e)
        }
    }

    private fun getMagazineReadingHistory(id: String?) {
        viewModelScope.launch {
            withContext(NonCancellable) {
                try {
                    val response = mDocService.getMagazineReadingHistory(id)
                    val list = response.data?.list

                    if (!list.isNullOrEmpty()) {
                        bookmark.value = list.last()
                    }

                } catch (e: java.lang.Exception) {
                    Timber.d(e)
                }
            }
        }
    }

    private fun getAndUpdateMagazineReadingHistory(mediaId: String?, pageNumber: Int, bookmarks: Array<Int>? = emptyArray(), read: Boolean = false) {
        viewModelScope.launch {
            withContext(NonCancellable) {
                try {

                    val response = mDocService.getMagazineReadingHistory(mediaId)
                    val list = response.data?.list

                    if (!list.isNullOrEmpty()) {
                        bookmark.value = list.last()
                    }

                    if (bookmark.value == null) {
                        addBookmark(bookmarks, pageNumber, read)
                    } else {
                        var updated = bookmark.value
                        updated?.pageNumber = pageNumber

                        if (read) {
                            updated?.read = read
                        }
                        updateBookmark(updated)
                    }

                } catch (e: java.lang.Exception) {
                    Timber.d(e)
                }
            }
        }
    }

    fun saveMagazineReadingHistory(pageNumber: Int) {
        if (bookmark.value == null) {
            getAndUpdateMagazineReadingHistory(mediaId, pageNumber)
        } else {
            var updated = bookmark.value
            updated?.pageNumber = pageNumber
            viewModelScope.launch {
                withContext(NonCancellable) {
                    updateBookmark(updated)
                }
            }
        }
    }

    fun markAsFinished(pageNumber: Int) {
        if (bookmark.value == null) {
            getAndUpdateMagazineReadingHistory(mediaId =  mediaId, pageNumber = pageNumber, read = true)
        } else {
            var updated = bookmark.value
            updated?.pageNumber = pageNumber
            updated?.read = true
            viewModelScope.launch {
                withContext(NonCancellable) {
                    updateBookmark(updated)
                }
            }
        }
    }
}
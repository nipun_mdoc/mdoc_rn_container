package de.mdoc.modules.medications


object MedicationUtil {

    fun dosageToString(dosage: Double): String {
        val intPart = trimDoubleToDot(dosage.toString())
        val addition = when (nullIntPart(dosage.toString())) {
            "0.5"  -> "1/2"
            "0.33"  -> "1/3"
            "0.25" -> "1/4"
            "0.66" -> "2/3"
            "0.75" -> "3/4"
            else   -> ""
        }

        return "$intPart $addition"
    }

    private fun nullIntPart(input: String): String {
        return when {
            input.contains(".") -> {
                val indexOfDot = input.indexOf(".")
                "0" + input.substring(indexOfDot, input.length)
            }

            else                -> input
        }
    }

    private fun trimDoubleToDot(input: String): String {
        return when {
            input.contains(".") -> {
                val indexOfDot = input.indexOf(".")
                input.substring(0, indexOfDot)
            }

            else                -> input
        }
    }
}

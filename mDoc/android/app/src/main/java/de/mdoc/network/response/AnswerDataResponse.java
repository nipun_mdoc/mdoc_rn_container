package de.mdoc.network.response;


import de.mdoc.pojo.AnswearDetails;

/**
 * Created by ema on 1/16/17.
 */

public class AnswerDataResponse  extends MdocResponse {

    private AnswearDetails data;

    public AnswearDetails getData() {
        return data;
    }

    public void setData(AnswearDetails data) {
        this.data = data;
    }
}

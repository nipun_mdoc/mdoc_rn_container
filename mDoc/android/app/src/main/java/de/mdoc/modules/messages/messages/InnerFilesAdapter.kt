package de.mdoc.modules.messages.messages

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.interfaces.IDownloadId
import de.mdoc.interfaces.PermissionGrantedListener
import de.mdoc.modules.messages.data.FileListItem
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_file_message.view.*
import java.io.File
import java.util.*

class InnerFilesAdapter(var context: Context, var items: ArrayList<FileListItem>, var listener : IDownloadId) : RecyclerView.Adapter<InnerFilesViewHolder>(), PermissionGrantedListener{

    override fun permissionGranted(isGranted: Boolean) {
        if(isGranted){
            downloadViaManager(url, item)
        }else{
            MdocUtil.showToastShort(context, "Permission denided")
        }
    }

    var downloadID : Long?= null
    var url : String = ""
    var item : FileListItem? = null

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): InnerFilesViewHolder {
        context = parent.context
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_file_message, parent, false)


        return InnerFilesViewHolder(view)
    }

    override fun onBindViewHolder(holder: InnerFilesViewHolder, position: Int) {
        item = items[position]
        holder.fileNameTv.text = item?.name
        holder.viewFileTv.setOnClickListener {
            url = context.getString(R.string.base_url) + "v2/common/files/download/" + item?.id + "?bucket=messaging"
            downloadViaManager(url, item)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun downloadViaManager(url: String, item: FileListItem?) {

        val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + item?.name?.replace(
                        "\\s".toRegex(),
                        ""
                )?.replace(":", "")
        )

        val request = DownloadManager.Request(Uri.parse(url))
                .setTitle(item?.name)// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file)) // Uri of the destination file
                .setVisibleInDownloadsUi(true)
                .setMimeType(getMimeFromFileName(item?.name ?: ""))
                .addRequestHeader(MdocConstants.AUTHORIZATION, MdocAppHelper.getInstance().accessToken)
                .addRequestHeader(MdocConstants.DELEGATED_CASE, MdocAppHelper.getInstance().caseId)
                .addRequestHeader(MdocConstants.ACCEPT_LANGUAGE, Locale.getDefault().language)
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true)// Set if download is allowed on roaming network

        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadID = downloadManager.enqueue(request) // enqueue puts the download request in the queue.

        listener?.passDownloadId(downloadID)
    }

    private fun getMimeFromFileName(fileName: String): String? {
        val map = MimeTypeMap.getSingleton()
        val ext = MimeTypeMap.getFileExtensionFromUrl(
                fileName.replace("\\s".toRegex(), "").replace(
                        ":",
                        ""
                )
        )
        return map.getMimeTypeFromExtension(ext)
    }

}

class InnerFilesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val fileNameTv: TextView = itemView.fileNameTv as TextView
    val viewFileTv: TextView = itemView.viewFileTv as TextView
}
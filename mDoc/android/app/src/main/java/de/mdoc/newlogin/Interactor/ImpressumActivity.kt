package de.mdoc.newlogin.Interactor

import android.annotation.TargetApi
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.mdoc.R


class ImpressumActivity : AppCompatActivity() {

    lateinit var externalUrl : WebView
    lateinit var llBottomBack : LinearLayout
    lateinit var ll_close : LinearLayout
    lateinit var url : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dilog)

        var iin = intent
        var b = iin.extras

        if (b != null) {
            url = (b["url"] as String?).toString()

        }

        llBottomBack = findViewById(R.id.llBottomBack)
        ll_close = findViewById(R.id.ll_close)
        llBottomBack.setOnClickListener(View.OnClickListener { view: View? ->
            finish();
        })
        ll_close.setOnClickListener(View.OnClickListener { view: View? ->
            finish();
        })
        externalUrl = findViewById(R.id.externalUrl)
        externalUrl.getSettings().setJavaScriptEnabled(true);

        externalUrl.loadDataWithBaseURL("", url, "text/html", "UTF-8", "");

    }
}
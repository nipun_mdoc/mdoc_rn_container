package de.mdoc.util

import android.os.Bundle
import android.os.Parcelable
import androidx.fragment.app.Fragment
import java.io.Serializable
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

private const val message = "Avoid using it if you can. A new way is to pass data using navigation components."

@Deprecated(message = message)
fun Fragment.intArgument(
    argumentKey: String,
    defaultValue: Int = 0
): ReadWriteProperty<Fragment, Int> {
    return ArgumentHolder(argumentKey, { bundle, key ->
        bundle.getInt(key, defaultValue)
    }, { bundle, key, value ->
        bundle.putInt(key, value)
    })
}

@Deprecated(message = message)
fun Fragment.longArguments(
        argumentKey: String,
        defaultValue: Long = 0
                        ): ReadWriteProperty<Fragment, Long> {
    return ArgumentHolder(argumentKey, { bundle, key ->
        bundle.getLong(key, defaultValue)
    }, { bundle, key, value ->
        bundle.putLong(key, value)
    })
}

@Deprecated(message = message)
fun Fragment.stringArgument(
    argumentKey: String,
    defaultValue: String = ""
): ReadWriteProperty<Fragment, String> {
    return ArgumentHolder(argumentKey, { bundle, key ->
        bundle.getString(key, defaultValue)
    }, { bundle, key, value ->
        bundle.putString(key, value)
    })
}

@Deprecated(message = message)
fun Fragment.booleanArgument(
    argumentKey: String,
    defaultValue: Boolean
): ReadWriteProperty<Fragment, Boolean> {
    return ArgumentHolder(argumentKey, { bundle, key ->
        bundle.getBoolean(key, defaultValue)
    }, { bundle, key, value ->
        bundle.putBoolean(key, value)
    })
}

@Deprecated(message = message)
fun <T : Parcelable> Fragment.parcelableArgument(
    argumentKey: String,
    defaultValueProvider: () -> T = {
        throw IllegalStateException("Argument $argumentKey is not set")
    }
): ReadWriteProperty<Fragment, T> {
    return ArgumentHolder(argumentKey, { bundle, key ->
        bundle.getParcelable<T>(key) ?: defaultValueProvider.invoke()
    }, { bundle, key, value ->
        bundle.putParcelable(key, value)
    })
}

@Deprecated(message = message)
fun <T : Serializable> Fragment.serializableArgument(
    argumentKey: String,
    defaultValueProvider: () -> T = {
        throw IllegalStateException("Argument $argumentKey is not set")
    }
): ReadWriteProperty<Fragment, T> {
    return ArgumentHolder<T>(argumentKey, { bundle, key ->
        (bundle.getSerializable(key) as? T) ?: defaultValueProvider.invoke()
    }, { bundle, key, value ->
        bundle.putSerializable(key, value)
    })
}

@Deprecated(message = message)
private class ArgumentHolder<T>(
    private val key: String,
    private val get: (Bundle, String) -> T,
    private val set: (Bundle, String, T) -> Unit
) : ReadWriteProperty<Fragment, T> {

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        if (thisRef.arguments == null) {
            thisRef.arguments = Bundle()
        }
        return get.invoke(thisRef.arguments!!, key)
    }

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: T) {
        if (thisRef.arguments == null) {
            thisRef.arguments = Bundle()
        }
        set.invoke(thisRef.arguments!!, key, value)
    }
}
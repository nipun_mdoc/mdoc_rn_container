package de.mdoc.modules.booking.master_data

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.data.requests.MetaDataRequest
import de.mdoc.network.request.CodingRequest
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class MasterDataViewModel(private val mDocService: IMdocService): ViewModel() {


    var genderList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())
    var countryList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())
    var titleList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())
    var federalStateList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())

    init {
        getCoding()
    }

    fun getCoding() {
        viewModelScope.launch {
            try {
                getCodingAsync()
            } catch (e: java.lang.Exception) {
            }
        }
    }

    private suspend fun getCodingAsync() {
        titleList.value = mDocService.getCodingKt(CodingRequest(
                TITLE_CODING)).data.list
        genderList.value = mDocService.getCodingKt(CodingRequest(
                GENDER_CODING)).data.list
        countryList.value = mDocService.getCodingKt(CodingRequest(
                COUNTRY_CODING)).data.list
        federalStateList.value = mDocService.getCodingKt(CodingRequest(
                FEDERAL_STATE_CODING)).data.list
    }

    fun postMasterData(request: MetaDataRequest,onSuccess:()->Unit,onError:()->Unit) {
        viewModelScope.launch {
            try {
                mDocService.postMasterDataKt(request)
                onSuccess()
            } catch (e: java.lang.Exception) {
                onError()

            }
        }
    }

    companion object {
        const val TITLE_CODING = "https://www.hl7.org/fhir/v2/title"
        const val GENDER_CODING = "https://www.hl7.org/fhir/v2/0001"
        const val COUNTRY_CODING = "https://mdoc.one/hl7/fhir/countryList"
        const val FEDERAL_STATE_CODING = "https://www.hl7.org/fhir/v2/federalState"
    }
}
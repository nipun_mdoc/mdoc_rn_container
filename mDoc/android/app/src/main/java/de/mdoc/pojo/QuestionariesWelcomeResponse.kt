package de.mdoc.pojo

import de.mdoc.network.response.CodingResponse
import de.mdoc.network.response.QuestionnaireDetailsResponse
import java.util.ArrayList

data class QuestionariesWelcomeResponse (val codingResponse: CodingResponse? = null,
                                         val detailsResponse: QuestionnaireDetailsResponse? = null,
                                         val answersResponse: QuestionnairesAnswersResponse? = null)


fun QuestionariesWelcomeResponse.isCodingValid (): Boolean {
    return codingResponse?.data?.list?.size?:0 > 0
}

fun QuestionariesWelcomeResponse.getCodingData(): ArrayList<CodingItem>? {
    return codingResponse?.data?.list
}

fun QuestionariesWelcomeResponse.getSupportedLanguages (): ArrayList<String>? {
    return detailsResponse?.data?.supportedLanguages
}

fun QuestionariesWelcomeResponse.getAnswersLanguage(): String? {
    return answersResponse?.data?.pollVotingActionDetails?.language
}

fun QuestionariesWelcomeResponse.getDefaultLanguage(): String? {
    return detailsResponse?.data?.defaultLanguage
}

fun QuestionariesWelcomeResponse.hasAnyAnswers ():Boolean {
    return detailsResponse?.data?.answerDetails?.completed?:0 > 0
}
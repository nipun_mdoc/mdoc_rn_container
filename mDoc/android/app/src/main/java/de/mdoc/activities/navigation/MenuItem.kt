package de.mdoc.activities.navigation

import androidx.annotation.BoolRes
import androidx.annotation.IdRes
import de.mdoc.R

enum class MenuItem(
        @IdRes val menuContainerId: Int,
        @IdRes val menuTextId: Int,
        @IdRes val menuIconId: Int,
        @IdRes val menuShortContainerId: Int,
        @IdRes val menuShortIconId: Int,
        @BoolRes val configEnabledResIds: IntArray) {

    Covid19(
            menuContainerId = R.id.covid19Rl,
            menuTextId = R.id.covid19Tv,
            menuIconId = R.id.covid19Iv,
            menuShortContainerId = R.id.covid19ShortRl,
            menuShortIconId = R.id.covid19ShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_covid19_module)
    ),
    Dashboard(
            menuContainerId = R.id.dashboardRl,
            menuTextId = R.id.dashboardTv,
            menuIconId = R.id.dashboardIv,
            menuShortContainerId = R.id.dashboardShortRl,
            menuShortIconId = R.id.dashboardShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_dashboard)
    ),

    Therapy(
            menuContainerId = R.id.therapyRl,
            menuTextId = R.id.therapyTv,
            menuIconId = R.id.therapyIv,
            menuShortContainerId = R.id.therapyShortRl,
            menuShortIconId = R.id.therapyShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_therapy)
    ),

    MealPlan(
            menuContainerId = R.id.mealRl,
            menuTextId = R.id.mealTv,
            menuIconId = R.id.mealIv,
            menuShortContainerId = R.id.mealShortRl,
            menuShortIconId = R.id.mealShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_meal)
    ),

    AirAndPollen(
            menuContainerId = R.id.airAndPollenRl,
            menuTextId = R.id.airAndPollenTv,
            menuIconId = R.id.airAndPollenIv,
            menuShortContainerId = R.id.airAndPollenShortRl,
            menuShortIconId = R.id.airAndPollenShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_air_and_pollen)
    ),

    Questionnaire(
            menuContainerId = R.id.questionnaireRl,
            menuTextId = R.id.questionnaireTv,
            menuIconId = R.id.questionnaireIv,
            menuShortContainerId = R.id.questionnairesShortRl,
            menuShortIconId = R.id.questionnairesShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_questionnaires)
          ),
    MyClinic(
            menuContainerId = R.id.myClinicRl,
            menuTextId = R.id.myClinicTv,
            menuIconId = R.id.myClinicIv,
            menuShortContainerId = R.id.myClinicShortRl,
            menuShortIconId = R.id.myClinicShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_my_clinic)
    ),

    CheckList(
            menuContainerId = R.id.checklistRl,
            menuTextId = R.id.checklistTv,
            menuIconId = R.id.checklistIv,
            menuShortContainerId = R.id.checklistShortRl,
            menuShortIconId = R.id.checklistShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_checklist)
    ),

    MyProfile(
            menuContainerId = R.id.profileRl,
            menuTextId = R.id.profileTv,
            menuIconId = R.id.myprofileIv,
            menuShortContainerId = R.id.profileShortRl,
            menuShortIconId = R.id.profileShortIv,
            configEnabledResIds = intArrayOf(R.bool.my_profile)
    ),

    Vitals(
            menuContainerId = R.id.statisticsRl,
            menuTextId = R.id.statisticsTv,
            menuIconId = R.id.statisticsIv,
            menuShortContainerId = R.id.statisticsShortRl,
            menuShortIconId = R.id.statisticsShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_vitals)
    ),

    HelpSupport(
            menuContainerId = R.id.helpSupportRl,
            menuTextId = R.id.helpSupportTv,
            menuIconId = R.id.helpSupportIv,
            menuShortContainerId = R.id.helpSupportShortRl,
            menuShortIconId = R.id.helpShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_help_and_support)
    ),

    Media(
            menuContainerId = R.id.mediaRl,
            menuTextId = R.id.mediaTv,
            menuIconId = R.id.mediaIv,
            menuShortContainerId = R.id.mediaShortRl,
            menuShortIconId = R.id.mediaShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_media)
    ),
    PatientJourney(
            menuContainerId = R.id.patientJourneyRl,
            menuTextId = R.id.patientJourneyTv,
            menuIconId = R.id.patientJourneyIv,
            menuShortContainerId = R.id.patientJourneyShortRl,
            menuShortIconId = R.id.patientJourneyShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_patient_journey)
    ),

    Files(
            menuContainerId = R.id.filesRl,
            menuTextId = R.id.filesTv,
            menuIconId = R.id.filesIv,
            menuShortContainerId = R.id.filesShortRl,
            menuShortIconId = R.id.filesShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_files)
    ),

    Diary(
            menuContainerId = R.id.diaryRl,
            menuTextId = R.id.diaryTv,
            menuIconId = R.id.diaryIv,
            menuShortContainerId = R.id.diaryShortRl,
            menuShortIconId = R.id.diaryShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_diary)
    ),

    FamilyAccount(
            menuContainerId = R.id.childsRl,
            menuTextId = R.id.childTv,
            menuIconId = R.id.childIv,
            menuShortContainerId = R.id.childShortRl,
            menuShortIconId = R.id.childShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_family_acc)
    ),

    Consent(
            menuContainerId = R.id.thiemeRl,
            menuTextId = R.id.thiemeTv,
            menuIconId = R.id.thiemeIv,
            menuShortContainerId = R.id.thiemeShortRl,
            menuShortIconId = R.id.thiemeShortIv,
            configEnabledResIds = intArrayOf( R.bool.has_consent)
          ),

    Notifications(
            menuContainerId = R.id.notificationRl,
            menuTextId = R.id.notificationTv,
            menuIconId = R.id.notifyIv,
            menuShortContainerId = R.id.notificationShortRl,
            menuShortIconId = R.id.notificationShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_notifications)
    ),

    Exercise(
            menuContainerId = R.id.exerciseRl,
            menuTextId = R.id.exerciseTv,
            menuIconId = R.id.exerciseIv,
            menuShortContainerId = R.id.exerciseShortRl,
            menuShortIconId = R.id.exersiceShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_exercises)
    ),

    BookAppointment(
            menuContainerId = R.id.bookingRl,
            menuTextId = R.id.bookingTv,
            menuIconId = R.id.bookingIv,
            menuShortContainerId = R.id.bookingShortRl,
            menuShortIconId = R.id.bookingShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_booking)
    ),

    MentalGoals(
            menuContainerId = R.id.mentalGoalsRl,
            menuTextId = R.id.mentalGoalsTv,
            menuIconId = R.id.mentalGoalsIv,
            menuShortContainerId = R.id.mentalGoalsShortRl,
            menuShortIconId = R.id.mentalGoalsShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_mental_goals)
    ),

    Medication(
            menuContainerId = R.id.medicationRl,
            menuTextId = R.id.medicationTv,
            menuIconId = R.id.medicationIv,
            menuShortContainerId = R.id.medicationsShortRl,
            menuShortIconId = R.id.medicationsShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_medications_plan)
    ),

    Devices(
            menuContainerId = R.id.devicesRl,
            menuTextId = R.id.devicesTv,
            menuIconId = R.id.devicesIv,
            menuShortContainerId = R.id.devicesShortRl,
            menuShortIconId = R.id.devicesShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_devices, R.bool.has_vitals)
    ),
    Entertainment(
            menuContainerId = R.id.entertainmentRl,
            menuTextId = R.id.entertainmentTv,
            menuIconId = R.id.entertainmentIv,
            menuShortContainerId = R.id.entertainmentShortRl,
            menuShortIconId = R.id.entertainmentShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_entertainment)
    ),
    Messages(
            menuContainerId = R.id.messagesRl,
            menuTextId = R.id.messagesTv,
            menuIconId = R.id.messagesIv,
            menuShortContainerId = R.id.messagesShortRl,
            menuShortIconId = R.id.messagesShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_messages)
    ),
    ExternalServices(
            menuContainerId = R.id.externalServicesRl,
            menuTextId = R.id.externalServicesTv,
            menuIconId = R.id.externalServicesIv,
            menuShortContainerId = R.id.externalServicesShortRl,
            menuShortIconId = R.id.externalServicesShortIv,
            configEnabledResIds = intArrayOf(R.bool.has_external_services)
    );
}

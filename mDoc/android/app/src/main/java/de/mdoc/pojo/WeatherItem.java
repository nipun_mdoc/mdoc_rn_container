package de.mdoc.pojo;

import java.util.ArrayList;

/**
 * Created by ema on 11/1/17.
 */

public class WeatherItem {

    private long dt;
    private String dt_txt;
    private ArrayList<Weather> weather;
    private Temperature main;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public String getDt_txt() {
        return dt_txt;
    }

    public void setDt_txt(String dt_txt) {
        this.dt_txt = dt_txt;
    }

    public ArrayList<Weather> getWeather() {
        return weather;
    }

    public void setWeather(ArrayList<Weather> weather) {
        this.weather = weather;
    }

    public Temperature getMain() {
        return main;
    }

    public void setMain(Temperature main) {
        this.main = main;
    }
}

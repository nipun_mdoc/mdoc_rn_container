package de.mdoc.network.response

data class PollVotingScoreResponse(
    val code: String,
    val timestamp: Long,
    val description: String,
    val message: String,
    var data: VotingScoreResponseData
)

data class VotingScoreResponseData(
    val pollId: String,
    val caseId: String,
    val completed: Boolean,
    val acquiredScore: Double,
    val medicalScores: List<VotingMedicalScore>
)
data class VotingMedicalScore(
    val label: String,
    val color: String,
    val description: String,
    val acquiredScoreString: String,
    val title: String
)
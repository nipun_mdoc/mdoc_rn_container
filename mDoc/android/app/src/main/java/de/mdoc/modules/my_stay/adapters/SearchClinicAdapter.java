package de.mdoc.modules.my_stay.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.ClinicDetails;

/**
 * Created by kodecta-mac on 7/26/17.
 */

public class SearchClinicAdapter extends BaseAdapter {

    private List<ClinicDetails> clinicDetails;
    Context context;
    private Typeface regular;

    public SearchClinicAdapter(List<ClinicDetails> clinicDetails, Context context) {
        this.clinicDetails = clinicDetails;
        this.context = context;
        this.regular = ResourcesCompat.getFont(context, R.font.hk_grotesk_regular);
    }

    @Override
    public int getCount() {
        return clinicDetails.size();
    }

    @Override
    public Object getItem(int i) {
        return clinicDetails.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    static class ViewHolder {

        @BindView(R.id.myStayIv)
        ImageView clinicIv;
        @BindView(R.id.locationTv)
        TextView locationTv;
        @BindView(R.id.clinicNameTv)
        TextView clinicNameTv;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView =  LayoutInflater.from(context).inflate(R.layout.clinic_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        ClinicDetails item = (ClinicDetails) getItem(position);

        holder.clinicNameTv.setTypeface(regular);
        holder.locationTv.setTypeface(regular);
        if(item.getImages() != null  && item.getImages().size() > 0 && item.getImages().get(0).getImage() != null &&!item.getImages().get(0).getImage().isEmpty()){
            Glide.with(context).load(item.getImages().get(0).getImage()).placeholder(R.drawable.ic_no_clinic_small).error(R.drawable.ic_no_clinic_small).fitCenter().into(holder.clinicIv);
        }else{
            Glide.with(context).load(R.drawable.ic_no_clinic_small).fitCenter().into(holder.clinicIv);
        }
        holder.clinicNameTv.setText(item.getName());

        return convertView;
    }
}

package de.mdoc.modules.vitals

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.data.create_bt_device.ObservationResponseData
import de.mdoc.modules.devices.DevicesUtil
import de.mdoc.modules.devices.data.DevicesForUserRequest
import de.mdoc.service.IMdocService
import de.mdoc.storage.AppPersistence.medicalDeviceMacAddresses
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class VitalsViewModel(
        private val mDocService: IMdocService
                     ): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var vitalsData = MutableLiveData(listOf<ObservationResponseData>())


    fun getLatestObservations() {
        viewModelScope.launch {
            try {
                getLatestObservationsApi()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowLoadingError.value = true            }
        }
    }

     private suspend fun getLatestObservationsApi() = coroutineScope {
        isLoading.value = true
        isShowNoData.value = false
        val medicalDevicesResponse = async {
            val userDevices = mDocService.coroutineGetDevicesForUser(
                    DevicesForUserRequest(deviceId = FirebaseInstanceId.getInstance().id))
                .data
            medicalDeviceMacAddresses = DevicesUtil.filterMacAddresses(userDevices)
        }
        val latestObservationsResponse = async {
            vitalsData.value = mDocService.coroutineGetLatestObservations()
                .data
        }

        awaitAll(medicalDevicesResponse, latestObservationsResponse)

        isLoading.value = false

        if (vitalsData.value.isNullOrEmpty()) {
            isShowContent.value = false
            isShowNoData.value = true
        }
        else {
            isShowContent.value = true
            isShowNoData.value = false
        }
    }
}
package de.mdoc.modules.medications.adapters.viewholders

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import de.mdoc.R
import de.mdoc.modules.medications.ItemOverviewFragment
import de.mdoc.modules.medications.MedicationsViewModel
import de.mdoc.modules.medications.data.MedicationAdministration
import kotlinx.android.synthetic.main.placeholder_inner_medication_dashboard.view.*
import kotlinx.android.synthetic.main.placeholder_medication_dashboard.view.*

class MedicationOnDemandViewHolder(itemView: View) : BaseMedicationViewHolder(itemView), View.OnClickListener {

    private val txtTitle: TextView = itemView.onDemandDescription.txt_item_name
    private val txtDescription: TextView = itemView.onDemandDescription.txt_dosage_info

    private lateinit var _item: MedicationAdministration
    private lateinit var _viewModel: MedicationsViewModel

    init {
        itemView.onDemandDescription.visibility = View.VISIBLE
        txtTime.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        itemView.setOnClickListener(this)

        txtDescription.maxLines = 2
        txtDescription.ellipsize = TextUtils.TruncateAt.END
    }

    fun bind(item: MedicationAdministration, viewModel: MedicationsViewModel) {
        _item = item
        _viewModel = viewModel

        txtTime.text = item.medication?.description
        txtTitle.setText(R.string.med_plan_intake_information)

        if (!item.dosageInstruction.isNullOrEmpty()) {
            item.dosageInstruction[0].let {
                txtDescription.text = it?.patientInstruction
            }
        }
    }

    override fun onClick(view: View?) {
       when (view) {
           itemView -> {
               ItemOverviewFragment()
                       .also {
                           it.medicationAdministration = _item
                           it.medicationRequestId = _item.id
                           it.isOnDemandCase = true
                           it.parentViewModel = _viewModel
                       }
                       .show((itemView.context as AppCompatActivity).supportFragmentManager, "itemOverview")
           }
       }
    }
}

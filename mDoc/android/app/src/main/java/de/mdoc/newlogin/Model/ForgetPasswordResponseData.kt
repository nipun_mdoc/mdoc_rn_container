package de.mdoc.newlogin.Model

data class ForgetPasswordResponseData(
    val code: String,
    val `data`: Data2,
    val message: String,
    val timestamp: Long
)

data class Data2(
    val email: String
)

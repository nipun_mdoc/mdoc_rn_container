package de.mdoc.newlogin.Model

class RegisterRequestData() {
    var email: String? = ""
    var password: String? = ""
    var confirmPassword: String? = ""
    var firstName: String? = ""
    var lastName: String? = ""
    var username: String? = ""
    var client_secret: String? = ""
    var client_id: String? = ""
    var mobilePhone: String? = ""
    var phone: String? = ""


    var contacts : List<RegisterRequestContactData>? = null
}
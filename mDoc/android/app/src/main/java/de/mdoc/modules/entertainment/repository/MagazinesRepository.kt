package de.mdoc.modules.entertainment.repository
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_
import de.mdoc.pojo.entertainment.getPublicationDate
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class MagazinesRepository {

    fun getMediaById (pdfId: String, pageNumber: String, onResult: (Bitmap) -> Unit,  onError: () -> Unit){
        MdocManager.getMediaById(pdfId, pageNumber, object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val thread = Thread {
                        val bytes = response.body()?.bytes()
                        val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes!!.size)

                        if (bitmap != null) {
                            onResult(bitmap)
                        } else {
                            onError.invoke()
                        }
                    }
                    thread.start()

                } else  {
                    onError.invoke()
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.d(t)
                onError.invoke()
            }
        })
    }

    fun getMagazinesByCategory (category: String, onResult: (ResponseSearchResultsMediaLightDTO_?) -> Unit) {
        MdocManager.getEntertainmentStatisticAll(category, object : Callback<ResponseSearchResultsMediaLightDTO_> {
            override fun onResponse(call: Call<ResponseSearchResultsMediaLightDTO_>, response: Response<ResponseSearchResultsMediaLightDTO_>) {
                if (response.isSuccessful) {
                    // Sort magazines by publicationDate - DESC
                    response.body()?.data?.list?.sortWith(compareByDescending{it.getPublicationDate()})
                    onResult.invoke(response.body())
                }
            }

            override fun onFailure(call: Call<ResponseSearchResultsMediaLightDTO_>, t: Throwable) {
                Timber.d(t)
            }
        })
    }
}
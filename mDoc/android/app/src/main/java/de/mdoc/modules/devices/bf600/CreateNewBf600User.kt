package de.mdoc.modules.devices.bf600

import android.bluetooth.BluetoothGatt
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.interfaces.DeviceDataListener
import de.mdoc.modules.devices.data.BleMessage
import de.mdoc.modules.devices.data.ScaleUser
import de.mdoc.pojo.CodingItem
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import kotlinx.android.synthetic.main.fragment_create_bf600_user.*
import rx.android.schedulers.AndroidSchedulers
import java.util.*
import java.util.concurrent.TimeUnit

class CreateNewBf600User: MdocFragment(), DeviceDataListener {

    lateinit var activity: MdocActivity

    val calendar: Calendar = Calendar.getInstance()
    var genderSelected: Int = 255
    private var selectedDate: Long? = null
    private var shouldCancelConnection = true
    private var scaleUser: ScaleUser? = null

    private val args: CreateNewBf600UserArgs by navArgs()

    override fun setResourceId() = R.layout.fragment_create_bf600_user

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        args.bf600Wrapper?.setBleResponseListener(this@CreateNewBf600User)
    }

    override val navigationItem: NavigationItem = NavigationItem.Devices

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed {
            shouldCancelConnection = false
            findNavController().popBackStack(R.id.fragmentInitializeBf600, true)
        }

        shouldCancelConnection = true
        if (scaleUser?.dob != null) {
            et_date_picker?.setText(MdocUtil.getParsedDate(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar))
            selectedDate = calendar.timeInMillis
        }
        setupOnClickListeners()

        setupGenderSpinner()

        listenCalendar()

        listenHeight()
    }

    private fun setupOnClickListeners() {
        txt_cancel?.setOnClickListener {
            findNavController().popBackStack()
        }
        create_user?.setOnClickListener {
            if (isValidEntry()) {
                navigateToPinEntry()
            }
            else {
                showError()
            }
        }

        et_date_picker?.setOnClickListener {
            showDatePicker()
        }
    }

    private fun showDatePicker() {
        val currentCalendar: Calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog =
                android.app.DatePickerDialog(activity, { _, year, month, dayOfMonth ->
                    calendar.set(Calendar.YEAR, year)
                    calendar.set(Calendar.MONTH, month)
                    calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    if (calendar.timeInMillis > currentCalendar.timeInMillis) {
                        calendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR))
                        calendar.set(Calendar.MONTH, currentCalendar.get(Calendar.MONTH))
                        calendar.set(Calendar.DAY_OF_MONTH,
                                currentCalendar.get(Calendar.DAY_OF_MONTH))
                    }

                    selectedDate = calendar.timeInMillis
                    et_date_picker?.setText(
                            MdocUtil.getParsedDate(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar))
                }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.maxDate = Calendar.getInstance()
            .timeInMillis
        datePickerDialog.show()
    }

    private fun setupGenderSpinner() {
        val genderList: ArrayList<CodingItem> = arrayListOf(
                CodingItem("", "255", resources.getString(R.string.select_gender)),
                CodingItem("", "0", resources.getString(R.string.male)),
                CodingItem("", "1", resources.getString(R.string.female)),
                CodingItem("", "2", resources.getString(R.string.unspecified)))
        val spinnerAdapter = SpinnerAdapter(activity, genderList)
        gender_spinner?.adapter = spinnerAdapter

        gender_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                genderSelected = genderList.getOrNull(p2)?.code?.toInt() ?: 255
                if (hasGender()) {
                    err_gender?.visibility = View.GONE
                }
                if (p2 == 0) {
                    disableButton()
                }
                else {
                    if (hasHeight() && hasDate() && hasGender()) {
                        enableButton()
                    }
                    else {
                        disableButton()
                    }
                }
            }
        }
    }

    private fun listenCalendar() {
        RxTextView.textChanges(et_date_picker)
            .debounce(100,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (hasDate()) {
                    et_date.error = null
                }
                if (hasHeight() && hasDate() && hasGender()) {
                    enableButton()
                }
                else {
                    disableButton()
                }
            }
    }

    private fun listenHeight() {
        RxTextView.textChanges(height)
            .debounce(100,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (hasHeight()) {
                    til_height.error = null
                }
                if (hasHeight() && hasDate() && hasGender()) {
                    enableButton()
                }
                else {
                    disableButton()
                }
            }
    }

    private fun isValidEntry() = hasHeight() && hasGender() && hasDate()

    private fun showError() {
        if (!hasGender()) {
            err_gender?.visibility = View.VISIBLE
        }
        else {
            err_gender?.visibility = View.GONE
        }
        if (!hasHeight()) {
            til_height.error = resources.getString(R.string.devices_err_body_height, MIN_HEIGHT, MAX_HEIGHT)
        }
        else {
            til_height.error = null
        }
        if (!hasDate()) {
            et_date.error = resources.getString(R.string.devices_scale_add_birth_date)
        }
        else {
            et_date.error = null
        }
    }

    private fun navigateToPinEntry() {
        val height = height.text.toString()
        scaleUser = ScaleUser(selectedDate, genderSelected, height.toInt())
        shouldCancelConnection = false

        val action = CreateNewBf600UserDirections.actionToFragmentEnterPinBf600(
                bf600Wrapper = args.bf600Wrapper,
                deviceData = args.deviceData,
                deviceId = args.deviceId,
                scaleUser = scaleUser)

        findNavController().navigate(action)
    }

    private fun disableButton() {
        create_user?.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(activity, R.color.gray_ccccc))
    }

    private fun enableButton() {
        create_user?.backgroundTintList = ColorStateList.valueOf(
                ContextCompat.getColor(activity, R.color.colorPrimary))
    }
    override fun genericDeviceResponse(list: ArrayList<Byte>?,
                                       gatt: BluetoothGatt?,
                                       message: BleMessage?) {
        activity.runOnUiThread {
            if (message== BleMessage.CONNECTION_TIMEOUT) {
                connectionTimeoutAction(this)
            }
        }
    }
    override fun onPause() {
        super.onPause()
        if (shouldCancelConnection) {
            args.bf600Wrapper?.disconnect()
        }
    }

    private fun hasHeight(): Boolean {
        return try {
            height.text.toString().toInt() in MIN_HEIGHT..MAX_HEIGHT
        } catch (e: NumberFormatException) {
            false
        }
    }

    private fun hasGender() = genderSelected != 255
    private fun hasDate() = selectedDate != null

    companion object {
        //30 to 250 cm is valid height
        const val MIN_HEIGHT = 30
        const val MAX_HEIGHT = 250
    }
}



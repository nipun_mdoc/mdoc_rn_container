package de.mdoc.modules.entertainment.fragments.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.entertainment.adapters.filter.MagazinesFilterLanguageAdapter
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterSharedViewModel
import de.mdoc.modules.medications.create_plan.data.ListItem
import kotlinx.android.synthetic.main.fragment_entertainment_filter_languages.*

class MagazinesFilterLanguagesFragment : DialogFragment() {

    private lateinit var activity: MdocActivity
    private lateinit var sharedViewModel: MagazineFilterSharedViewModel

    private var languages: MutableList<ListItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MdocActivity
        setStyle(
                STYLE_NO_TITLE,
                android.R.style.Theme_Light_NoTitleBar_Fullscreen
        )

        sharedViewModel = activity.run {
            ViewModelProviders.of(this).get(MagazineFilterSharedViewModel::class.java)
        }

        val json = Gson().toJson(sharedViewModel.languagesTemp.value)
        val listType = object : TypeToken<MutableList<ListItem>>() {}.type
        languages = Gson().fromJson<MutableList<ListItem>>(json, listType)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(de.mdoc.R.layout.fragment_entertainment_filter_languages, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvLanguages.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            adapter = MagazinesFilterLanguageAdapter(languages, sharedViewModel)
        }

        setOnClickListener()
    }

    private fun setOnClickListener() {
        btnCloseFilter.setOnClickListener {
            dismiss()
        }
    }
}
package de.mdoc.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment

import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.activities.navigation.setCurrentNavigationItem

abstract class NewBaseFragment:Fragment() {

    abstract val navigationItem: NavigationItem

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setCurrentNavigationItem(navigationItem)
    }
}
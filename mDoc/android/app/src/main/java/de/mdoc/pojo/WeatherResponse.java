package de.mdoc.pojo;

import java.util.ArrayList;

/**
 * Created by ema on 11/1/17.
 */

public class WeatherResponse {

    private String cod;
    private String message;
    private int cnt;
    private ArrayList<WeatherItem> list;
    private WeatherCity city;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public ArrayList<WeatherItem> getList() {
        return list;
    }

    public void setList(ArrayList<WeatherItem> list) {
        this.list = list;
    }

    public WeatherCity getCity() {
        return city;
    }

    public void setCity(WeatherCity city) {
        this.city = city;
    }
}

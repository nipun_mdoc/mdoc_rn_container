package de.mdoc.newlogin.Interactor

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.activities.CaseActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.ScanActivity
import de.mdoc.activities.login.BiometricPromptDialogFragment
import de.mdoc.activities.login.BiometricPromptDialogFragmentCallback
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.activities.login.TermsPrivacyDialogHandler
import de.mdoc.activities.login.clinicselection.ClinicSelectionActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.ActivityLogiBinding
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.newlogin.AuthListener
import de.mdoc.newlogin.LoginUser
import de.mdoc.newlogin.LoginViewModel
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.pojo.Case
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.activity_logi.*
import de.mdoc.phonenumberkit.PhoneNumberKit
import showToastLong
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern


class LogiActivty : AppCompatActivity(), LoginHelper.LoginCallback, AuthListener,
        TermsPrivacyDialogHandler.TermsPrivacyCallback,
        BiometricPromptDialogFragmentCallback, CommonDialogFragment.OnButtonClickListener {
    private var loginViewModel: LoginViewModel? = null
    private var binding: ActivityLogiBinding? = null


    private lateinit var encryptionService: EncryptionServices
    private lateinit var systemServices: SystemServices

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 100
        const val REFRESH_TOKEN_RESPONSE_CODE = "REFRESH_TOKEN_RESPONSE_CODE"
        const val FORCED_LOGOUT = "FORCED_LOGOUT"
        const val LOGIN_SCREEN_CONDITION = "redirect_uri=mdocapp"
    }

    private val progressHandler = ProgressHandler(this)
    private var refreshTokenCode = 200
    private val loginHelper by lazy {
        LoginHelper(this, this, progressHandler)
    }
    private var _cases: ArrayList<Case>? = null
    private val termsPrivacyDialogHandler by lazy {
        TermsPrivacyDialogHandler(this, this, progressHandler)
    }
    private var isFingerprintAuthenticated: Boolean = false
    private var deepLinkUri: Uri? = null
    private var isForcedLogout = false
    private var isResendOTP = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)
        binding = DataBindingUtil.setContentView(this@LogiActivty, R.layout.activity_logi)
        binding?.setLifecycleOwner(this)
        binding?.setLoginViewModel(loginViewModel)
        loginViewModel?.authListener = this

        val phoneNumberKit = PhoneNumberKit(this)
        phoneNumberKit.attachToInput(layoutMobileNUmber, 49)
        phoneNumberKit.setupCountryPicker(
                activity = this,
                searchEnabled = true
        )

        if(MdocAppHelper.getInstance().username != null) {
            txtEmailAddress.setText(MdocAppHelper.getInstance().username)
            loginViewModel?.uesr_edit?.value = MdocAppHelper.getInstance().username
            txtEmailAddress.setSelection(MdocAppHelper.getInstance().username.length)
        }

        var shakeAnimation: Animation? = AnimationUtils.loadAnimation(this, R.anim.shake)
        loginViewModel!!.user.observe(this, Observer<LoginUser> { userRequestModel ->

            txtUserNamelayout.setError(null);
            txtPasswordLayout.setError(null);
            txtCaptchaLayout.setError(null);
            layoutMobileNUmber.setError(null);
            layoutSmsOtp.setError(null);

            if (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.username)) {
                txtUserNamelayout?.setError(" ")
                txtUserNamelayout.errorIconDrawable = null
                txtUserNamelayout?.requestFocus();
                txtUserNamelayout.startAnimation(shakeAnimation)
                vibrate(200)
            } else if ((userRequestModel)!!.password!!.length < 8) {
                txtPasswordLayout?.setError(" ")
                txtPasswordLayout.errorIconDrawable = null
                txtPasswordLayout?.requestFocus();
                txtPasswordLayout.startAnimation(shakeAnimation)
                vibrate(200)
            } else if (linearCaptchaMain.visibility == View.VISIBLE && (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.captcha))) {
                txtCaptchaLayout?.setError(" ")
                txtCaptchaLayout.errorIconDrawable = null
                txtCaptchaLayout?.requestFocus();
                txtCaptchaLayout.startAnimation(shakeAnimation)
                vibrate(200)
            } else if (layoutVerifyMobile.visibility == View.VISIBLE && (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.mobileNumber))) {
                layoutMobileNUmber?.setError(" ")
                layoutMobileNUmber.errorIconDrawable = null
                layoutMobileNUmber?.requestFocus();
                layoutMobileNUmber.startAnimation(shakeAnimation)
                vibrate(200)
            } else if (layoutVerifyOTP.visibility == View.VISIBLE && (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.smsotp)) && !isResendOTP) {
                layoutSmsOtp?.setError(" ")
                layoutSmsOtp.errorIconDrawable = null
                layoutSmsOtp?.requestFocus();
                layoutSmsOtp.startAnimation(shakeAnimation)
                vibrate(200)
            } else {
                isResendOTP = false
                loginViewModel?.loadData(userRequestModel!!, this)
            }
        })

        encryptionService = EncryptionServices(this)
        systemServices = SystemServices(this)

        if (encryptionService.getMasterKey() == null) {
            encryptionService.createMasterKey()
        }
        initBiometricButton()
        parseIntent();
        shouldHideScanQrButton()
        if (shouldPromptBiometric()) {
            loginWithBiometrics()
        }
        shouldShowLoginWithBiometricButton()

        if (savedInstanceState == null) {
            BandwidthCheck.initBandwidthCheck()
        }


    }
    val maxCounter: Long = 60000
    val diff: Long = 1000
    val f: NumberFormat = DecimalFormat("00")
    val timer = object : CountDownTimer(maxCounter, diff) {
        override fun onTick(millisUntilFinished: Long) {
            val diff = maxCounter - millisUntilFinished
            resendOtp?.text = "00 : " + f.format(millisUntilFinished / 1000)
        }
        override fun onFinish() {
            resendOtp?.isEnabled = true
            resendOtp?.text = resources?.getString(R.string.resend_otp)
            resendOtp?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_icon_resend, 0, 0, 0);
        }
    }
    private fun startResendCounter(){
        timer?.onFinish()
        resendOtp?.isEnabled = false
        resendOtp?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        timer?.start()
    }

    fun validateUserName(text: String?): Boolean {
        val regex = "^(?![_.])(?!.*[_.]{2})[a-z0-9._]+(?<![@_.])\$"
        val p: Pattern = Pattern.compile(regex)
        if (text == null) {
            return false
        }
        val m: Matcher = p.matcher(text)
        return m.matches()
    }

    fun vibrate(duration: Int) {
        var vibs = this.getSystemService(VIBRATOR_SERVICE) as Vibrator
        vibs.vibrate(duration.toLong())
    }

    private fun parseIntent() {
        refreshTokenCode = intent.getIntExtra(LogiActivty.REFRESH_TOKEN_RESPONSE_CODE, 200)
        val deepLink = intent.getStringExtra(MdocConstants.DEEP_LINK_URI)

        deepLinkUri = if (deepLink != null && deepLink != "null") {
            Uri.parse(deepLink)
        }
        else {
            null
        }
        if (refreshTokenCode != 200) {
            showToastLong(getString(R.string.login_activity_session_expired))
        }
        isForcedLogout = intent.getBooleanExtra(LogiActivty.FORCED_LOGOUT, false)
    }

    private fun initBiometricButton() {
        if (shouldPromptBiometric()) {
            biometricLogin.visibility = View.VISIBLE
        }else{
            biometricLogin.visibility = View.GONE
        }
        biometricLogin?.setOnClickListener {
            loginWithBiometrics()
        }
    }

    private fun loginWithBiometrics() {

        systemServices.authenticateFingerprint(successAction = {
            isFingerprintAuthenticated = true
            loginViewModel?.password_edit?.value = MdocAppHelper.getInstance().userPassword
            loginViewModel?.createLlginRequest(this);
        }, cancelAction = {
        }, context1 = this@LogiActivty)
    }


    private fun decideFlowBasedOnCases() {
        if (_cases == null) {
            navigateToDashboard()
        } else {
            openCases(_cases)
        }
    }

    private fun navigateToDashboard() {
        val intent = Intent(this, MdocActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

        if (BuildConfig.FLAV != "kiosk") {
            finish()
        }
    }

    private fun openCases(cases: ArrayList<Case>?) {
        val intent = Intent(applicationContext, CaseActivity::class.java)
        intent.putExtra(MdocConstants.CASES, cases)
        startActivity(intent)
        finish()
    }

    private fun shouldHideScanQrButton() {
        if (MdocAppHelper.getInstance().hasSecretKey()) {
            scanQrCode.visibility = View.GONE
        }
    }

    private fun shouldPromptBiometric(): Boolean {
        return MdocAppHelper.getInstance().isOptIn && !isFingerprintAuthenticated  && !isForcedLogout
    }

    override fun onBiometricPromptDialogButtonClick(isPositiveButton: Boolean) {
        MdocAppHelper.getInstance()
                .isBiometricPrompted = true
        if (isPositiveButton) {
            optInBiometricAuthentication()
        }
        else {
            decideFlowBasedOnCases()
            MdocAppHelper.getInstance()
                    .userPassword = null
        }
    }

    private fun shouldShowLoginWithBiometricButton() {
        biometricLogin?.visibility = if (MdocAppHelper.getInstance().isOptIn) View.VISIBLE
        else View.GONE
    }

    private fun callScanActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_REQUEST_CODE
                )
            }
            else {
                startActivity(Intent(this, ScanActivity::class.java))
            }
        }
        else {
            startActivity(Intent(this, ScanActivity::class.java))
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(Intent(this, ScanActivity::class.java))
            }
            else {
                MdocUtil.showToastLong(this, "Permissions not granted!")
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ClinicSelectionActivity.CLINIC_SELECTION_RESULT) {
            progressHandler.showProgress()
            if (resultCode == Activity.RESULT_OK) {
                val handler = Handler()
                handler.postDelayed({
                    loginHelper.loginWithSession()
                }, 7000)
            }
        }else if(requestCode == UpdatePasswordActivity.PASSWORD_CHANGE){
            if (resultCode == Activity.RESULT_OK) {
                sucess()
            }
        }
    }

    override fun openTermsAndConditions() {
        termsPrivacyDialogHandler.openTermsFromCoding()
    }

    override fun openPrivacyPolicy() {
        termsPrivacyDialogHandler.openPrivacyFromCoding()
    }

    override fun openNextActivity(cases: ArrayList<Case>?) {
        _cases = cases

        if (MdocAppHelper.getInstance()
                        .isOptIn && MdocConstants.IS_UPDAE_PASSWORD) {
            MdocConstants.IS_UPDAE_PASSWORD = false
            SystemServices(this).authenticateFingerprint(successAction = {
                systemServices.optIn(MdocConstants.NEW_PASSWORD)
                decideFlowBasedOnCases()
            }, cancelAction = {
                this.runOnUiThread {
                    MdocAppHelper.getInstance().isOptIn = false
                    decideFlowBasedOnCases()
                }
            }, context1 = this@LogiActivty)

        } else {
            if (systemServices.canOptIn()) {
                BiometricPromptDialogFragment(this).also { it.isCancelable = false }
                        .showNow(supportFragmentManager, "BiometricPromptDialogFragment")
            } else {
                decideFlowBasedOnCases()
            }
        }


    }

    override fun onPrivacyPolicyAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onTermsAndConditionsAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onPrivacyPolicyDeclined() {
//        loadInitialPage()
    }

    override fun onTermsAndConditionsDeclined() {
//        loadInitialPage()
    }

    override fun openClinics() {
        val intent = Intent(this, ClinicSelectionActivity::class.java)
        startActivityForResult(intent, ClinicSelectionActivity.CLINIC_SELECTION_RESULT)
    }

    var isShowing = false;
    override fun onMissingSecret() {
        if (isShowing) return
        isShowing = true
        CommonDialogFragment.Builder()
                .title(resources.getString(R.string.login_not_possible))
                .description(resources.getString(R.string.login_not_possible_description))
                .setPositiveButton(resources.getString(R.string.button_ok))
                .setOnClickListener(this)
                .build().showNow(supportFragmentManager, CommonDialogFragment::class.simpleName)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        isShowing = false
    }

    private fun optInBiometricAuthentication() {
        systemServices.authenticateFingerprint(successAction = {
            Toast.makeText(applicationContext,
                    resources.getString(R.string.biometric_auth_success), Toast.LENGTH_SHORT)
                    .show()
            val token = MdocAppHelper.getInstance()
                    .secretKey

            if (encryptionService.getFingerprintKey() == null) {
                encryptionService.createFingerprintKey()
            }
            MdocAppHelper.getInstance().isOptIn = true
            MdocAppHelper.getInstance().secretKey = token
            val password = MdocAppHelper.getInstance().userPassword
            MdocAppHelper.getInstance().userPassword = loginViewModel?.password_edit?.value
                    decideFlowBasedOnCases()
        }, cancelAction = {
            this.runOnUiThread {
                decideFlowBasedOnCases()
            }
        }, context1 = this@LogiActivty)
    }


    override fun sucess() {

        if (MdocAppHelper.getInstance().secretKey == null && MdocConstants.KEY_MISSING) {
            MdocConstants.KEY_MISSING = false
            loginHelper.fetchSecretKey();
        } else {
            loginHelper.loginWithSession()
        }
//        Toast.makeText(applicationContext, "Success1", Toast.LENGTH_SHORT).show()
    }

    override fun failer() {
        Toast.makeText(applicationContext, "Success112", Toast.LENGTH_SHORT).show()
    }

    override fun onLoadCaptcha(response: CaptchaResponse?) {
        linearCaptchaMain.visibility = View.VISIBLE
        val base64String = response?.data?.image?.replace("data:image/jpeg;base64,", "")
        val decodedString: ByteArray = android.util.Base64.decode(base64String, android.util.Base64.DEFAULT)
        val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imageViewCaptcha.setImageBitmap(decodedByte)
    }


    override fun isCaptchaVisible(): Boolean? {
        return linearCaptchaMain?.isVisible
    }

    override fun clearCaptchaField() {
        txtCaptchaLayout?.editText?.setText("")
    }
    override fun changePassword(token: String) {
        if(layoutVerifyOTP?.isVisible!! || layoutVerifyMobile?.isVisible!!){
            layoutVerifyOTP?.visibility = View.GONE
            layoutVerifyMobile?.visibility = View.GONE
            layoutLogin?.visibility = View.VISIBLE
        }
        var loginIntent : Intent
        loginIntent = Intent(this, UpdatePasswordActivity::class.java)
        loginIntent.putExtra(MdocConstants.DEEP_LINK_URI, token);
        startActivityForResult(loginIntent, UpdatePasswordActivity.PASSWORD_CHANGE)
    }

    fun backToLogin(v: View){
        layoutLogin?.visibility = View.VISIBLE
        layoutVerifyMobile?.visibility = View.GONE
        layoutVerifyOTP?.visibility = View.GONE
    }

    fun backToChangePhone(v: View){
        loginViewModel?.smsotp?.value = ""
        showMobileNumberScreen()
    }

    fun resendOTP(v: View){
        isResendOTP = true
        loginViewModel?.onLoginClick(v)
    }

    override fun showMobileNumberScreen() {
        loginViewModel?.mobilenumber?.value = "49"
       layoutLogin?.visibility = View.GONE
        layoutVerifyMobile?.visibility = View.VISIBLE
        layoutVerifyOTP?.visibility = View.GONE
        layoutMobileNUmber?.requestFocus()
    }
    override fun showOTPScreen(){
        layoutLogin?.visibility = View.GONE
        layoutVerifyMobile?.visibility = View.GONE
        layoutVerifyOTP?.visibility = View.VISIBLE
        layoutSmsOtp?.requestFocus()
        startResendCounter()
    }
    override fun onImpressumClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)    }
    override fun onDatenschutzDieClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)  }
    override fun onAGBClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)   }

    override fun onCloseClick() {
        finish()
    }

    override fun onScanClick() {
        callScanActivity();
    }

}
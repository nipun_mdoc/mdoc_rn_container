package de.mdoc.pojo;

import java.util.ArrayList;

import de.mdoc.network.response.MdocResponse;


public class ExerciseResponseNew extends MdocResponse {

    private ArrayList<ExerciseDataCategories> data;

    public ArrayList<ExerciseDataCategories> getData() {
        return data;
    }

    public void setData(ArrayList<ExerciseDataCategories> data) {
        this.data = data;
    }
}

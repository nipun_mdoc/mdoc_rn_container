package de.mdoc.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.activities.navigation.setCurrentNavigationItem

abstract class MdocFragment : MdocBaseFragment() {

    abstract val navigationItem: NavigationItem

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setCurrentNavigationItem(navigationItem)
        return super.onCreateView(inflater, container, savedInstanceState)
    }
}
package de.mdoc.util

import android.app.Dialog
import android.content.res.Resources
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.core.view.forEach
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import de.mdoc.R
import de.mdoc.activities.MainActivity


fun Fragment.showSnackbar(message: String, view: View) {
    val snackBar = Snackbar.make(
            view,
            message, Snackbar.LENGTH_LONG
                                )
    snackBar.show()
}

fun Fragment.deviceRequestFailed() {
    if (this.isAdded) {
        Toast.makeText(this.context, this.resources.getText(R.string.err_request_failed), Toast.LENGTH_LONG)
            .show()

        val isBackToInitBf600 = findNavController().popBackStack(R.id.fragmentInitializeBf600, true)
        if (!isBackToInitBf600) {
            findNavController().popBackStack()
        }
    }
}

fun Fragment.onCreateOptionsMenu(menu: Menu, inflater: MenuInflater, menuId: Int) {
    inflater.inflate(menuId, menu)
    menu.applyContrastColor(resources)
}

private fun Menu.applyContrastColor (resources: Resources) {
    forEach {
        it.icon?.setTint(ColorUtil.contrastColor(resources))

        if (it.title != null) {
            val spanString = SpannableString(it.title.toString())
            spanString.setSpan(ForegroundColorSpan(ColorUtil.contrastColor(resources)), 0, spanString.length, 0)
            it.title = spanString
        }
    }
}

// In order to use this function menu item must contain app:actionLayout="@layout/view_menu_item_badge_layout"
fun Fragment.onCreateOptionsMenuWithBadge (menu: Menu, inflater: MenuInflater,menuId: Int, badgeId: Int) : TextView {
    inflater.inflate(menuId, menu)
    menu.applyContrastColor(resources)

    val menuItem = menu.findItem(badgeId)
    val actionView = menuItem.actionView
    val badgeView = actionView.findViewById<TextView>(R.id.menuActionItemBadge)
    val badgeIcon = actionView.findViewById<ImageView>(R.id.menuActionItemIcon)

    menuItem?.let {
        badgeIcon.setImageDrawable(it.icon)
    }

    actionView.setOnClickListener {
        onOptionsItemSelected (menuItem)
    }

    return badgeView
}

fun Fragment.updateBadge(menu: Menu, value: Int?, badgeId: Int) {
    val menuItem = menu.findItem(badgeId)
    val actionView = menuItem.actionView
    val badgeView = actionView.findViewById<TextView>(R.id.menuActionItemBadge)

    if (value == null || value == 0) {
        badgeView?.visibility = View.GONE
    } else {
        badgeView?.text = value.toString()
        badgeView?.visibility = View.VISIBLE
    }
}

fun Fragment.hideActionBar(){
    activity?.let  {
       if (it is MainActivity) {
           it.supportActionBar?.hide()
       }
    }
}

fun Fragment.showActionBar(){
    activity?.let  {
        if (it is MainActivity) {
            it.supportActionBar?.show()
        }
    }
}

fun Fragment.setActionTitle (title: String? = "") {
    activity?.let  {
        if (it is MainActivity) {
            it.supportActionBar?.title = title
        }
    }
}

fun Fragment.handleOnBackPressed (listener: (()->Unit)? = null) {
    val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
           listener?.invoke()
        }
    }
    requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
}

fun Fragment.showProgressDialog(): Dialog {
    return ProgressDialog(requireContext()).apply {
        setCancelable(false)
        show()
    }
}

fun Fragment.showCancelableProgressDialog(): Dialog {
    return ProgressDialog(requireContext()).apply {
        show()
    }
}


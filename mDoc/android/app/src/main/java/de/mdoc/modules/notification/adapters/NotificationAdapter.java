package de.mdoc.modules.notification.adapters;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.Constants;
import de.mdoc.constants.MdocConstants;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.NotificationListItem;
import de.mdoc.util.MdocAppHelper;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kodecta-mac on 8/7/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder> {

    private Context context;
    private ArrayList<NotificationListItem> notifications;

    public NotificationAdapter(Context context, ArrayList<NotificationListItem> notifications) {
        this.context = context;
        this.notifications = notifications;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_item, parent, false);
        context = parent.getContext();

        return new NotificationViewHolder(view);
    }

    public void refreshData() {
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {
        final NotificationListItem item = notifications.get(position);

        holder.typeTv.setText(item.getTitle());
        holder.descTv.setText(item.getDescription());

        if (item.isSeen()) {
            holder.timeTv.setAlpha(0.5f);
            holder.typeTv.setAlpha(0.5f);
            holder.descTv.setAlpha(0.5f);
            holder.notificationIv.setAlpha(0.5f);
        }

        long time = item.getCreatedTime();
        long now = System.currentTimeMillis();

        CharSequence ago = DateUtils.getRelativeTimeSpanString(time, now, DateUtils.MINUTE_IN_MILLIS);
        holder.timeTv.setText(ago );

        holder.rootRl.setOnClickListener(view -> {
            if (item.getCallToAction() != null && item.getCallToAction().getType() != null &&  item.getCallToAction().getType().equals(MdocConstants.DIGITAL_SIGNATURE)) {
                Navigation.findNavController(((MdocActivity)context),R.id.navigation_host_fragment).navigate(R.id.navigation_messages);
            }
            if (item.getReferenceType() != null && !item.getReferenceType().isEmpty() && item.getReferenceType().equals(Constants.REFERENCE_TYPE_MESSAGE)) {
                deleteSingleNotification(item.getId());
            }
        });
    }

    private void deleteSingleNotification(String id) {
        MdocManager.deleteNotification(id, new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                MdocAppHelper.getInstance().deleteNotification(id);
                Navigation.findNavController(((MdocActivity)context),R.id.navigation_host_fragment).navigate(R.id.navigation_messages);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @Override
    public int getItemCount() {
        if (notifications != null) {
            return notifications.size();
        } else {
         return 0;
        }
    }
}

class NotificationViewHolder extends RecyclerView.ViewHolder {

    public TextView typeTv, descTv, timeTv;
    public ImageView notificationIv;
    public RelativeLayout rootRl;

    public NotificationViewHolder(View itemView) {
        super(itemView);

        typeTv = itemView.findViewById(R.id.typeTv);
        descTv = itemView.findViewById(R.id.descTv);
        timeTv = itemView.findViewById(R.id.timeTv);
        notificationIv = itemView.findViewById(R.id.notificationIv);
        rootRl = itemView.findViewById(R.id.rootRl);
    }
 }

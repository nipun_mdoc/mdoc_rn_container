package de.mdoc.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.webkit.CookieManager
import android.webkit.JavascriptInterface
import android.webkit.WebSettings
import android.webkit.WebView
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.activities.login.*
import de.mdoc.activities.login.clinicselection.ClinicSelectionActivity
import de.mdoc.activities.navigation.isPhone
import de.mdoc.biometric.EasyFingerPrint
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.ActivityNewLoginBinding
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.pojo.Case
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.activity_new_login.*
import kotlinx.android.synthetic.main.activity_new_login_webview.*
import kotlinx.android.synthetic.main.activity_new_login_webview.view.*
import showToastLong
import timber.log.Timber

class LoginActivity: AppCompatActivity(),
        LoginWebViewClient.LoginWebCallback, LoginFlow.LoginCallback,
        TermsPrivacyDialogHandler.TermsPrivacyCallback,
        BiometricPromptDialogFragmentCallback , CommonDialogFragment.OnButtonClickListener{

    private lateinit var encryptionService: EncryptionServices
    private lateinit var systemServices: SystemServices

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 100
        const val REFRESH_TOKEN_RESPONSE_CODE = "REFRESH_TOKEN_RESPONSE_CODE"
        const val FORCED_LOGOUT = "FORCED_LOGOUT"
        const val LOGIN_SCREEN_CONDITION = "redirect_uri=mdocapp"
    }

    private val progressHandler = ProgressHandler(this)
    private var refreshTokenCode = 200
    private val loginUtil = LoginUtil(this)
    private val loginFlow by lazy {
        LoginFlow(this, this, progressHandler, loginUtil)
    }
    private var _cases: ArrayList<Case>? = null
    private val termsPrivacyDialogHandler by lazy {
        TermsPrivacyDialogHandler(this, this, progressHandler)
    }
    private var isFingerprintAuthenticated: Boolean = false
    private var deepLinkUri: Uri? = null
    private var isForcedLogout = false
    private var _loginWebViewClient: LoginWebViewClient? = null
    private var _webView: WebView? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (isPhone()) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }

        MdocConstants.caseList = null
        DataBindingUtil.setContentView<ActivityNewLoginBinding>(this, R.layout.activity_new_login)

        encryptionService = EncryptionServices(this)
        systemServices = SystemServices(this)

        if (encryptionService.getMasterKey() == null) {
            encryptionService.createMasterKey()
        }
        initBiometricButton()
        parseIntent()

        if (savedInstanceState == null) {
            BandwidthCheck.initBandwidthCheck()
        }
        btnTryAgain.setOnClickListener {
            layoutInflater.inflate(R.layout.activity_new_login_webview, webViewContainer, true)
            loadInitialPage()
        }

        pairRl?.setOnClickListener {
            scanQrDialog()
        }

        loadInitialPage()
    }

    private fun initBiometricButton() {
        biometricLogin?.setOnClickListener {
            loginWithBiometrics()
        }
    }

    private fun parseIntent() {
        refreshTokenCode = intent.getIntExtra(REFRESH_TOKEN_RESPONSE_CODE, 200)
        val deepLink = intent.getStringExtra(MdocConstants.DEEP_LINK_URI)

        deepLinkUri = if (deepLink != null && deepLink != "null") {
            Uri.parse(deepLink)
        }
        else {
            null
        }
        if (refreshTokenCode != 200) {
            showToastLong(getString(R.string.login_activity_session_expired))
        }
        isForcedLogout = intent.getBooleanExtra(FORCED_LOGOUT, false)
    }

    @SuppressLint("SetJavaScriptEnabled", "ClickableViewAccessibility")
    private fun loadInitialPage() {
        _loginWebViewClient = LoginWebViewClient(this, refreshTokenCode)
        _webView = webViewContainer.webView
        _webView?.visibility = View.INVISIBLE
        _webView?.settings?.javaScriptEnabled = true
        _webView?.settings?.domStorageEnabled = true
        _webView?.settings?.databaseEnabled = true
        _webView?.webViewClient = _loginWebViewClient

        _webView?.addJavascriptInterface(object: Any() {
            @JavascriptInterface
            fun onScanQrClick() {
                Timber.i("JS:onScanQrClick")
                callScanActivity()
            }
        }, "appInterface")

        tryAgainError.visibility = View.GONE
        if (deepLinkUri == null || deepLinkUri.toString() == "null" || deepLinkUri?.path.equals("/")) {
            loadInitPage()
        }
        else {
            _webView?.loadUrl(deepLinkUri.toString())
        }
        //Hide scan button if it is not immediately visible on screen
        _webView?.setOnTouchListener { _, motionEvent ->
            if (motionEvent.action == MotionEvent.ACTION_MOVE) {
                shouldHideScanQrButton()
            }
            return@setOnTouchListener false
        }
    }

    override fun onFailedToLoad() {
        val webView = webViewContainer.webView
        webView.destroy()
        webViewContainer.removeAllViews()
        tryAgainError.visibility = View.VISIBLE
        biometricLogin?.visibility = View.GONE
    }

    private fun callScanActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(
                        arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_REQUEST_CODE
                )
            }
            else {
                startActivity(Intent(this, ScanActivity::class.java))
            }
        }
        else {
            startActivity(Intent(this, ScanActivity::class.java))
        }
    }

    override fun setProgress(isInProgress: Boolean) {
        progressBar.visibility = if (isInProgress) View.VISIBLE else View.GONE
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(Intent(this, ScanActivity::class.java))
            }
            else {
                MdocUtil.showToastLong(this, "Permissions not granted!")
            }
        }
    }

    override fun executeLogin(url: String) {
        loginFlow.keycloackLoginServerTime(url)
    }

    override fun reloadInitialPage() {
        loadInitPage()
    }

    private fun loadInitPage() {

        val url = getString(R.string.keycloack_base_url) +
                "auth?scope=openid+email+profile&redirect_uri=mdocapp%3A%2F%2Fmdocapp%2F&client_id=" +
                getString(R.string.keycloack_clinic_Id) +
                "&response_type=code&response_mode=query"
        webView.settings.apply {
            setAppCacheEnabled(false)
            cacheMode = WebSettings.LOAD_NO_CACHE
        }
        webView.clearCache(true)
        webView.clearHistory()

        webView.loadUrl(url)

        CookieManager.getInstance()
            .removeAllCookies(null)
        CookieManager.getInstance()
            .flush()
    }

    override fun openTermsAndConditions() {
        termsPrivacyDialogHandler.openTermsFromCoding()
    }

    override fun openPrivacyPolicy() {
        termsPrivacyDialogHandler.openPrivacyFromCoding()
    }

    override fun onPrivacyPolicyAccepted() {
        loginFlow.loginWithSession()
    }

    override fun onTermsAndConditionsAccepted() {
        loginFlow.loginWithSession()
    }

    override fun onPrivacyPolicyDeclined() {
        loadInitialPage()
    }

    override fun onTermsAndConditionsDeclined() {
        loadInitialPage()
    }

    override fun openClinics() {
        val intent = Intent(this, ClinicSelectionActivity::class.java)
        startActivityForResult(intent, ClinicSelectionActivity.CLINIC_SELECTION_RESULT)
    }

    override fun onPageLoaded(url: String?) {
        if (shouldPromptBiometric(url)) {
            loginWithBiometrics()
        }
        shouldHideScanQrButton()
        shouldShowLoginWithBiometricButton(url)
    }

    private fun shouldShowLoginWithBiometricButton(url: String?) {
        biometricLogin?.visibility = if (url?.contains(
                        LOGIN_SCREEN_CONDITION) == true && MdocAppHelper.getInstance().isOptIn) View.VISIBLE
        else View.GONE
    }

    override fun openNextActivity(cases: ArrayList<Case>?) {
        _cases = cases

       if (MdocAppHelper.getInstance()
                .isOptIn && MdocConstants.IS_UPDAE_PASSWORD){
           MdocConstants.IS_UPDAE_PASSWORD = false
            SystemServices(this).authenticateFingerprint(successAction = {
                systemServices.optIn(MdocConstants.NEW_PASSWORD)
                decideFlowBasedOnCases()
            }, cancelAction = {
                this.runOnUiThread {
                    MdocAppHelper.getInstance().isOptIn = false
                    decideFlowBasedOnCases()
                }
            }, context1 = this@LoginActivity)

//           var easyFingerPrint = EasyFingerPrint(this)
//           easyFingerPrint.setTittle(resources.getString(R.string.sign_in))
//                   .setSubTittle(resources.getString(R.string.confirm_identity))
//                   .setColorPrimary(R.color.colorPrimary)
//                   .setIcon(ContextCompat.getDrawable(this,R.mipmap.ic_launcher))
//                   .setListern(object : EasyFingerPrint.ResultFingerPrintListern{
//                       override fun onError(mensage: String, code: Int) {
//
//                           when(code){
//                               EasyFingerPrint.CODE_ERRO_CANCEL -> { easyFingerPrint.cancelScan()
//                                       decideFlowBasedOnCases()
//                               } // TO DO
//                               EasyFingerPrint.CODE_ERRO_GREATER_ANDROID_M -> { } // TO DO
//                               EasyFingerPrint.CODE_ERRO_HARDWARE_NOT_SUPPORTED -> { } // TO DO
//                               EasyFingerPrint.CODE_ERRO_NOT_ABLED -> { } // TO DO
//                               EasyFingerPrint.CODE_ERRO_NOT_FINGERS -> { } // TO DO
//                               EasyFingerPrint.CODE_NOT_PERMISSION_BIOMETRIC -> { } // TO DO
//                           }
//
//                       }
//
//                       override fun onSucess(cryptoObject: FingerprintManagerCompat.CryptoObject?) {
//                           systemServices.optIn(MdocConstants.NEW_PASSWORD)
//                           decideFlowBasedOnCases()
//                       }
//
//                   })
//                   .startScan()


        }else{
           if (systemServices.canOptIn()) {
               BiometricPromptDialogFragment(this).also { it.isCancelable = false }
                       .showNow(supportFragmentManager, "BiometricPromptDialogFragment")
           }
           else {
               decideFlowBasedOnCases()
           }
       }
    }

    private fun decideFlowBasedOnCases() {
        if(resources.getBoolean(R.bool.has_multi_case)) {
            if (_cases == null) {
                navigateToDashboard()
            } else {
                openCases(_cases)
            }
        }else{
            navigateToDashboard()
        }
    }

    private fun navigateToDashboard() {
        webView?.visibility = View.GONE
        val intent = Intent(this, MdocActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

        if (BuildConfig.FLAV != "kiosk") {
            finish()
        }
    }

    private fun openCases(cases: ArrayList<Case>?) {
        val intent = Intent(applicationContext, CaseActivity::class.java)
        intent.putExtra(MdocConstants.CASES, cases)
        startActivity(intent)
        finish()
    }

    private fun optInBiometricAuthentication() {
        systemServices.authenticateFingerprint(successAction = {
            Toast.makeText(applicationContext,
                    resources.getString(R.string.biometric_auth_success), Toast.LENGTH_SHORT)
                    .show()
            val token = MdocAppHelper.getInstance()
                    .secretKey

            if (encryptionService.getFingerprintKey() == null) {
                encryptionService.createFingerprintKey()
            }
            val password = MdocAppHelper.getInstance()
                    .userPassword

            MdocAppHelper.getInstance()
                    .isOptIn = true
            MdocAppHelper.getInstance()
                    .userPassword = password

            MdocAppHelper.getInstance()
                    .secretKey = token


            decideFlowBasedOnCases()
        }, cancelAction = {
            this.runOnUiThread {
                decideFlowBasedOnCases()
            }
        }, context1 = this@LoginActivity)



//        var easyFingerPrint = EasyFingerPrint(this)
//        easyFingerPrint.setTittle(resources.getString(R.string.sign_in))
//                .setSubTittle(resources.getString(R.string.confirm_identity))
//                .setColorPrimary(R.color.colorPrimary)
//                .setIcon(ContextCompat.getDrawable(this,R.mipmap.ic_launcher))
//                .setListern(object : EasyFingerPrint.ResultFingerPrintListern{
//                    override fun onError(mensage: String, code: Int) {
//
//                        when(code){
//                            EasyFingerPrint.CODE_ERRO_CANCEL -> { easyFingerPrint.cancelScan()
//                                decideFlowBasedOnCases()
//                            } // TO DO
//                            EasyFingerPrint.CODE_ERRO_GREATER_ANDROID_M -> { } // TO DO
//                            EasyFingerPrint.CODE_ERRO_HARDWARE_NOT_SUPPORTED -> { } // TO DO
//                            EasyFingerPrint.CODE_ERRO_NOT_ABLED -> { } // TO DO
//                            EasyFingerPrint.CODE_ERRO_NOT_FINGERS -> { } // TO DO
//                            EasyFingerPrint.CODE_NOT_PERMISSION_BIOMETRIC -> { } // TO DO
//                        }
//
//                    }
//
//                    override fun onSucess(cryptoObject: FingerprintManagerCompat.CryptoObject?) {
//                        Toast.makeText(applicationContext,
//                                resources.getString(R.string.biometric_auth_success), Toast.LENGTH_SHORT)
//                                .show()
//                        val token = MdocAppHelper.getInstance()
//                                .secretKey
//
//                        if (encryptionService.getFingerprintKey() == null) {
//                            encryptionService.createFingerprintKey()
//                        }
//                        val password = MdocAppHelper.getInstance()
//                                .userPassword
//
//                        MdocAppHelper.getInstance()
//                                .isOptIn = true
//                        MdocAppHelper.getInstance()
//                                .userPassword = password
//
//                        MdocAppHelper.getInstance()
//                                .secretKey = token
//
//
//                        decideFlowBasedOnCases()
//                    }
//
//                })
//                .startScan()
    }

    fun showNewFragmentAddToBackStack(frag: Fragment, container: Int, bundle: Bundle) {
        frag.arguments = bundle
        val fragmentManager = supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }

    override fun onDestroy() {
        super.onDestroy()
        progressHandler.recycle()
    }

    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    private fun scanQrDialog() {
        val builder = AlertDialog.Builder(this)
        val inflater = this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_scan_qr, null)
        builder.setView(view)
        val dialog = builder.show()
        view.findViewById<Button>(R.id.scanBtn).visibility = View.GONE
        view.findViewById<EditText>(R.id.secretEt).visibility = View.GONE
        val insertBtn = view.findViewById<Button>(R.id.insertBtn)
        val qrCodeUrl = view.findViewById<EditText>(R.id.qrCodeUrl)
        qrCodeUrl.visibility = View.VISIBLE

        insertBtn.setOnClickListener {
            val url = qrCodeUrl.text.toString()
            dialog.dismiss()
            if (url.isNotEmpty()) {
                MdocAppHelper.getInstance().secretKey = url
            }
        }
    }
    private fun loginWithBiometrics() {
        systemServices.authenticateFingerprint(successAction = {
            isFingerprintAuthenticated = true
            _webView?.let { _loginWebViewClient?.injectPassword(it) }
        }, cancelAction = {
        }, context1 = this@LoginActivity)
    }

    private fun shouldHideScanQrButton() {
        if (MdocAppHelper.getInstance().hasSecretKey()) {
            val script = "document.getElementById('kc-form-buttons-scan').style.display = 'none'"
            webView.evaluateJavascript(script) { value -> println(value) }
        }
    }

    private fun shouldPromptBiometric(url: String?): Boolean {
        return MdocAppHelper.getInstance().isOptIn && !isFingerprintAuthenticated && (url?.contains(
                LOGIN_SCREEN_CONDITION) == true) && !isForcedLogout
    }

    override fun onBiometricPromptDialogButtonClick(isPositiveButton: Boolean) {
        MdocAppHelper.getInstance()
            .isBiometricPrompted = true
        if (isPositiveButton) {
            optInBiometricAuthentication()
        }
        else {
            decideFlowBasedOnCases()
            MdocAppHelper.getInstance()
                .userPassword = null
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ClinicSelectionActivity.CLINIC_SELECTION_RESULT) {
            progressHandler.showProgress()
            if (resultCode == Activity.RESULT_OK) {
                val handler = Handler()
                handler.postDelayed({
                    loginFlow.loginWithSession()
                }, 7000)
            }
        }
    }
    var isShowing = false;
    override fun onMissingSecret() {
        if(isShowing) return
        isShowing = true
        CommonDialogFragment.Builder()
                .title(resources.getString(R.string.login_not_possible))
                .description(resources.getString(R.string.login_not_possible_description))
                .setPositiveButton(resources.getString(R.string.button_ok))
                .setOnClickListener(this)
                .build().showNow(supportFragmentManager, CommonDialogFragment::class.simpleName)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        isShowing = false
    }


}
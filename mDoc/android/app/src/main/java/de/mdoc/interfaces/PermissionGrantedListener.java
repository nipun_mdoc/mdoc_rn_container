package de.mdoc.interfaces;

public interface PermissionGrantedListener {
    void permissionGranted(boolean isGranted);
}

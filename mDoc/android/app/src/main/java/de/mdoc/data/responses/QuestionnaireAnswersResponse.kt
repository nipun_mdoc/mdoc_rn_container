package de.mdoc.data.responses

import de.mdoc.data.questionnaire.Data

data class QuestionnaireAnswersResponse(
        val data: ArrayList<Data>
)
package de.mdoc.newlogin.network;


import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import de.mdoc.BuildConfig;
import okhttp3.OkHttpClient;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    public static final String BASE_URL =  "icgRestful/api/";


    private static Retrofit retrofit;


    private static final OkHttpClient client =
            new OkHttpClient.Builder()
                    .connectTimeout(500, TimeUnit.SECONDS)
                    .readTimeout(500, TimeUnit.SECONDS)
                    .writeTimeout(500, TimeUnit.SECONDS)
                    .build();

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit =
                    new Retrofit.Builder()
                            .baseUrl(getBaseUrl())
                            .addConverterFactory(getGsonConverterFactory())
                            .client(client)
                            .build();
        }
        return retrofit;
    }

    private static String getBaseUrl() {
        if (BuildConfig.DEBUG) return BASE_URL;
        else return BASE_URL;
    }


    public static Retrofit getClient1() {
        return getClient();
    }

    public static Retrofit getLoginClient() {
        return getClient1();
    }

    public static Retrofit getClient3() {
        return getClient();
    }

    public static Retrofit getClientJson() {
        return getClient();
    }

    public static Converter.Factory getGsonConverterFactory() {
        return GsonConverterFactory.create(new GsonBuilder().disableHtmlEscaping().create());
    }


}

package de.mdoc.modules.messages.messages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentSelectGroupBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.pojo.CodingItem
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.debounce
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_select_group.*

class SelectGroupFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Messages
    private val messagesViewModel by viewModel {
        MessagesViewModel(
                RestClient.getService())
    }
    var oldQuery = ""

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentSelectGroupBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_select_group, container, false)

        binding.lifecycleOwner = this
        binding.handler = MessagesHandler()
        binding.messagesViewModel = messagesViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_MESSAGE, MdocConstants.CAN_SELECT_RECEIVER)) {
            sendToPlaceholder?.visibility = View.VISIBLE
            setupDropdownSelection()
            observeSearchParameters()
        }
        else {
            sendToPlaceholder?.visibility = View.GONE
        }
    }

    private fun setupDropdownSelection() {
//        val listOptions = mutableListOf(CodingItem(code = LETTERBOX_CODE, display = resources.getString(R.string.messages_letterbox)))
//        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_MESSAGE, MdocConstants.HIDE_SENDER_PRPOFESSIONAL)) {
//            listOptions.add(CodingItem(code = PROFESSIONAL_CODE, display = resources.getString(R.string.messages_professional)))
//        }

        val listOptions = mutableListOf<CodingItem>()

        if (!resources.getBoolean(R.bool.is_hidden_send_to_letterbox)){
            listOptions.add(CodingItem(code = LETTERBOX_CODE, display = resources.getString(R.string.messages_letterbox)))
        }
        if(!MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_MESSAGE, MdocConstants.HIDE_SENDER_PRPOFESSIONAL)) {
            listOptions.add(CodingItem(code = PROFESSIONAL_CODE, display = resources.getString(R.string.messages_professional)))
        }

        val adapterMessageType = SpinnerAdapter(requireContext(), listOptions)
        messageSpinner?.adapter = adapterMessageType
        messageSpinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>,
                                        view: View?,
                                        position: Int,
                                        id: Long) {
                val selection = listOptions.getOrNull(position)?.code
                if (selection == LETTERBOX_CODE) {
                    et_layout?.visibility = View.GONE
                    messagesViewModel.getCodingAPI()
                }
                else {
                    et_layout?.visibility = View.VISIBLE
                    messagesViewModel.clearContent()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun observeSearchParameters() {
        handleSearchBox()
        debounceSearchQuery()
    }

    private fun handleSearchBox() {
        messagesViewModel.query.observe(viewLifecycleOwner, Observer {
            when {
                it.isEmpty()    -> {
                    messagesViewModel.clearContent()
                    et_layout?.endIconDrawable = null
                    oldQuery = ""
                }

                it.isNotEmpty() -> {
                    et_layout?.endIconDrawable =
                            ContextCompat.getDrawable(activity as MdocActivity, R.drawable.ic_close_24dp)
                    et_layout?.setEndIconOnClickListener {
                        messagesViewModel.query.value = ""
                        oldQuery = ""
                        messagesViewModel.clearContent()
                    }
                }

                else            -> {
                    et_layout?.endIconDrawable = null
                }
            }
        })
    }

    private fun debounceSearchQuery() {
        messagesViewModel.query.debounce(500)
            .observe(viewLifecycleOwner, Observer { query ->
                if (query.isNotEmpty()) {
                    if (oldQuery != query) {
                        messagesViewModel.getProfessionalsApi()
                        oldQuery = query
                    }
                }
            })
    }

    companion object {
        const val LETTERBOX_CODE = "letterbox"
        const val PROFESSIONAL_CODE = "professional"
    }
}
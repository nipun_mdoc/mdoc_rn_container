package de.mdoc.modules.files

import androidx.lifecycle.ViewModel
import de.mdoc.modules.files.data.FilesData
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.files.filter.FilesFilter
import de.mdoc.network.request.SearchFileRequest
import de.mdoc.pojo.CodingData
import de.mdoc.pojo.FileListItem
import de.mdoc.viewmodel.*
import timber.log.Timber

class FilesViewModel(
    private val repository: FilesRepository,
    val filter: ValueLiveData<FilesFilter> = liveData(FilesFilter(categories = null, from = null, to = null))
) : ViewModel() {


    val isLoading = liveData(true)
    val isError = liveData(false)
    private val allFiles = liveData<List<FileListItem>>()
    val files = combine(allFiles, filter) { list, filesFilter ->
        list.filter(filesFilter::isMatch)
    }
    val isHaveData = files.map {
        it.isNotEmpty()
    }
    val isNoData = isHaveData.not()
    val categories = liveData<CodingData>()

    val isFileDeleteInProgress = liveData(false)
    val onFileDeleteFailedEvent = liveData<String>()
    val onFileDeleted = liveData("")


    private fun onDataLoaded(data: FilesData) {
        isLoading.set(false)
        allFiles.set(data.files.list)
        categories.set(data.categoryCoding)
    }

    private fun onError(e: Throwable) {
        isLoading.set(false)
        isError.set(true)
    }

    fun deleteFile(id: String) {
        isFileDeleteInProgress.set(true)
        repository.deleteFile(id, {
            isFileDeleteInProgress.set(false)
            onFileDeleted.set(id)
            onFileDeleted(id)
        }, {
            isFileDeleteInProgress.set(false)
            onFileDeleteFailedEvent.set(id)
        })
    }

    private fun onFileDeleted(id: String) {
        allFiles.update { list ->
            list.filter {
                it.id != id
            }
        }
    }

    fun reloadData() {
        Timber.d("reloadData")
        isLoading.set(true)
        repository.getFilesData(SearchFileRequest(0, 0, ""), ::onDataLoaded, ::onError)
    }
}


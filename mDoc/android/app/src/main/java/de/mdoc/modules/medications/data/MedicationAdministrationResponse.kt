package de.mdoc.modules.medications.data

import android.content.Context
import de.mdoc.R
import de.mdoc.modules.medications.MedicationUtil
import de.mdoc.modules.medications.create_plan.data.MedicationAdapterData
import de.mdoc.storage.AppPersistence
import org.joda.time.DateTime
import java.util.*

data class MedicationAdministrationResponse(
        val code: String? = "",
        val `data`: Data? = Data(),
        val message: String? = "",
        val timestamp: Long? = 0
)

fun MedicationAdministrationResponse.hasMoreData(): Boolean{
    return data?.list?.size?:0 > 3
}

fun MedicationAdministrationResponse.prepareAdapterData(take: Int = 0): List<MedicationAdapterData> {
    val adapterMap: TreeMap<Long, ArrayList<MedicationAdministration>> = TreeMap()
    val adapterItems = arrayListOf<MedicationAdapterData>()

    var sorted = data?.list?.sortedBy {
        it.effectivePeriodStart
    }

    if (take > 0) {
        sorted = sorted?.take(3)
    }

    sorted?.forEach {
                val time = it.effectivePeriodStart ?: DateTime.now()
                val trimTime =
                        DateTime(time.year, time.monthOfYear, time.dayOfMonth, time.hourOfDay,
                                time.minuteOfHour, 0, 0)
                val key = trimTime.withMillisOfSecond(0)
                        ?.withSecondOfMinute(0)
                        ?.millis ?: 0

                if (!adapterMap.containsKey(key)) {
                    if (it.medication != null) {
                        adapterMap[key] = arrayListOf()
                        adapterMap[key]?.add(it)
                    }
                } else {
                    if (it.medication != null) {
                        adapterMap[key]?.add(it)
                    }
                }
            }

    adapterMap.forEach { (timeOfDay, arrayList) ->
        val timeTrunk = DateTime(timeOfDay)
        val timeOfDayTrunk =
                de.mdoc.modules.medications.create_plan.data.TimeOfDay(timeTrunk.hourOfDay,
                        timeTrunk.minuteOfHour,
                        timeTrunk.dayOfMonth,
                        timeTrunk.monthOfYear,
                        timeTrunk.year)

        if (arrayList.isNotEmpty()) {
            val uniqueTime = DateTime(timeTrunk.year, timeTrunk.monthOfYear, timeTrunk.dayOfMonth, 0, 0)
            val contains = adapterItems.any { item -> item.fullTimeOfDay.toString() == uniqueTime.toString() }

            if (contains) {
                adapterItems.add(MedicationAdapterData(null, timeOfDayTrunk, arrayList))
            } else {
                adapterItems.add(MedicationAdapterData(uniqueTime, timeOfDayTrunk, arrayList))
            }
        }
    }

    return adapterItems
}

fun MedicationAdministration.getDosageInfo(): String {
    val dosage = medicationRequest?.dosageInstruction?.getOrNull(0)?.doseQuantity ?: 0.0
    val dosageUnit = medicationRequest?.dosageInstruction?.getOrNull(0)?.doseQuantityCode
    var unit = dosageUnit

    if (dosageUnit != null) {
        AppPersistence.medicationUnitsCoding?.forEach {
            if (it.code == dosageUnit) {
                unit = it.display.toString()
            }
        }
    }

    return MedicationUtil.dosageToString(dosage) + " " + unit
}

data class Data(
        var list: List<MedicationAdministration>? = listOf(),
        val moreDataAvailable: Boolean? = false,
        val totalCount: Int? = 0
               )

data class MedicationAdministration(
        val id: String?= null,
        val effectivePeriodStart: DateTime? = null,
        val medication: Medication? = null,
        val medicationId: String? = null,
        val medicationRequest: MedicationRequest? = MedicationRequest(),
        val medicationRequestId: String? = "",
        val dosageInstruction: List<DosageInstruction?>? = listOf(),
        val status: String? = "",
        val subject: String? = ""
                                   )

data class Medication(
        val code: String? = null,
        val compounds: List<Compound?>? = null,
        val cts: Long? = null,
        val description: String? = null,
        val form: String? = null,
        val id: String? = null,
        val image: String? = null,
        val imageContentType: String? = null,
        val manualEntry: Boolean? = null,
        val manufacturer: String? = null,
        val manufacturerCode: String? = null,
        val patientId: String? = null,
        val prescriptionReasons: List<String?>? = null,
        val size: String? = null,
        val unit: String? = "",
        val uts: Long? = null
                     ) {

    fun pznInfo(context: Context): String {
        val result = if (code.isNullOrEmpty()) {
            context.resources.getString(R.string.med_plan_not_available)
        }
        else {
            code
        }
        return "PZN - $result"
    }

    fun getCompound(): String {
        return compounds?.getOrNull(0)?.name ?: ""
    }

    fun dosageInfo(): String {
        return when {
            !form.isNullOrEmpty() -> "$form - $size $unit"
            !size.isNullOrEmpty() -> "$size $unit"
            else                  -> "$unit"
        }
    }

    fun getPrescriptionReason(): String {
        return prescriptionReasons?.getOrNull(0) ?: ""
    }
}

data class Compound(
        val id: String? = "",
        val name: String? = ""
                   )

data class MedicationRequest(
        val authoredOn: String? = "",
        val cts: Long? = 0,
        val dispenseRequest: DispenseRequest? = DispenseRequest(),
        val dosageInstruction: List<DosageInstruction?>? = listOf(),
        val id: String? = "",
        val medicationId: String? = "",
        val requester: String? = "",
        val showReminder: Boolean? = false,
        val status: String? = "",
        val subject: String? = "",
        val uts: Long? = 0
                            )

data class DispenseRequest(
        val infinite: Boolean? = false,
        val validityPeriod: ValidityPeriod? = ValidityPeriod()
                          )

data class ValidityPeriod(
        val end: DateTime? = null,
        val start: DateTime? = null
                         )

data class DosageInstruction(
        val doseQuantity: Double? = 0.0,
        val doseQuantityCode: String? = "",
        val patientInstruction: String? = "",
        val sequence: Int? = 0,
        val text: String? = "",
        val timing: Timing? = Timing()
                            )

data class Timing(
        val event: List<String?>? = listOf(),
        val repeat: Repeat? = Repeat()
                 )

data class Repeat(
        val dayOfWeek: List<String?>? = listOf(),
        val frequency: Int? = 0,
        val period: Int? = 0,
        val periodUnit: String? = "",
        val timeOfDay: List<TimeOfDay?>? = listOf()
                 )

data class TimeOfDay(
        val hour: Int? = 0,
        val minute: Int? = 0
                    )

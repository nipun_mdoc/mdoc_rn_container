package de.mdoc.modules.questionnaire.finish

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder
import kotlinx.android.synthetic.main.fragment_scoring_result_item.view.*

object ScoringResult :
    ListItemTypeAdapter<VotingScoreResultModel, ListItemViewHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): ListItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.fragment_scoring_result_item, parent, false
        )
        return ListItemViewHolder(view)
    }

    override fun onBind(
        position: Int,
        item: VotingScoreResultModel,
        holder: ListItemViewHolder
    ) {
            if (item.resultScreenType.equals(MdocConstants.QUESTIONNAIRE_SMILEY_TEXT)){
                with(holder.itemView) {
                    score.visibility = View.GONE
                    color.visibility = View.VISIBLE

                    when (item.color?.key) {
                        ScoreColor.Red.key -> {
                            setRedImageAndText()
                        }
                        ScoreColor.Green.key -> {
                            setGreenImageAndText()
                        }
                        ScoreColor.Orange.key-> {
                            setOrangeImageAndText()
                        }
                        ScoreColor.Yellow.key-> {
                            setYellowImageAndText()
                        }
                        else -> {
                            score.visibility = View.VISIBLE
                            color.visibility = View.GONE
                            score.text = item.score.toString()
                        }
                    }
                    label.text = item.label
                    title.text = item.title
                    description.text = MdocUtil.fromHtml(item.description)

                }

            } else if (item.resultScreenType.equals(MdocConstants.QUESTIONNAIRE_POINTS)){
                with(holder.itemView) {
                    score.visibility = View.VISIBLE
                    color.visibility = View.GONE

                    label.text = item.label
                    item.color?.colorRes?.let {
                        scoreImage.setColorFilter(
                                ContextCompat.getColor(context, it),
                                android.graphics.PorterDuff.Mode.SRC_IN
                        )
                    }
                    score.text = item.score.toString()
                    title.text = item.title
                    description.text = MdocUtil.fromHtml(item.description)
                }
        }
    }

    private fun View.setRedImageAndText() {
        scoreImage.setImageResource(R.drawable.red)
        color.text = context.getString(R.string.red)
        color.setTextColor(ContextCompat.getColor(context, R.color.scoring_result_red))
    }

    private fun View.setGreenImageAndText() {
        scoreImage.setImageResource(R.drawable.green)
        color.text = context.getString(R.string.green)
        color.setTextColor(ContextCompat.getColor(context, R.color.scoring_result_green))
    }

    private fun View.setOrangeImageAndText() {
        scoreImage.setImageResource(R.drawable.orange)
        color.text = context.getString(R.string.orange)
        color.setTextColor(ContextCompat.getColor(context, R.color.scoring_result_orange))
    }

    private fun View.setYellowImageAndText() {
        scoreImage.setImageResource(R.drawable.yellow_scoring)
        color.text = context.getString(R.string.yellow)
        color.setTextColor(ContextCompat.getColor(context, R.color.scoring_result_yellow))
    }
}
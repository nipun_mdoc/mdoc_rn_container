package de.mdoc.modules.profile.patient_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.network.RestClient
import de.mdoc.network.request.CodingRequest
import de.mdoc.network.request.InsuranceRequest
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.PatientDetailsData
import de.mdoc.service.IMdocService
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class PatientDetailsViewModel: ViewModel() {
    var isLoading = MutableLiveData(false)
    var isFirstFragmentReady = MutableLiveData(false)
    var currentNavigationItem = MutableLiveData(0)
    var healthInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var accidentInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var militaryInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var disabilityInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var supplementaryInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var supplementaryAccidentInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    val mDocService: IMdocService = RestClient.getService()
    val insuranceClassList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())
    val supplementaryInsuranceClassList: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(arrayListOf())
    val relationshipStatusList: MutableLiveData<ArrayList<InsuranceDataKt>> =
            MutableLiveData(arrayListOf())
    var error: MutableLiveData<ErrorWrapper> = MutableLiveData(ErrorWrapper())

    init {
        getInsurance()
        getCoding()
        getPatientDetails()
        getLatestCase()
    }

    private fun getInsurance() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                getInsuranceAsync()
                isLoading.value = false
            } catch (e: Exception) {
                error.value = ErrorWrapper(true, "getInsuranceAsync", e.localizedMessage)
                isLoading.value = false
            }
        }
    }

    private fun getCoding() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                getCodingAsync()
                isLoading.value = false
            } catch (e: Exception) {
                isLoading.value = false

                error.value = ErrorWrapper(true, "getCodingAsync", e.localizedMessage)
            }
        }
    }

    private fun getPatientDetails() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                getPatientDetailsAsync()
                isLoading.value = false
                isFirstFragmentReady.value = true
            } catch (e: Exception) {
                isLoading.value = false

                error.value = ErrorWrapper(true, "getPatientDetailsAsync", e.localizedMessage)
            }
        }
    }

    private fun getLatestCase() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                getLatestCaseAsync()
                isLoading.value = false
            } catch (e: Exception) {
                isLoading.value = false

                error.value = ErrorWrapper(true, "getLatestCaseAsync", e.localizedMessage)
            }
        }
    }

    private suspend fun getLatestCaseAsync() {
        val id = mDocService.getLatestCase().data?.account?.get(0)?.reference ?: ""

        PatientDetailsHelper.getInstance()
            .coveragesResponse = mDocService.getCoverageById(id)
    }

    private suspend fun getPatientDetailsAsync() {
        PatientDetailsHelper.getInstance()
            .patientDetailsData = mDocService.getPatientDetails()
            .data ?: PatientDetailsData()

        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.familyDoctorId?.let {
            PatientDetailsHelper.getInstance()
                .familyDoctorData = mDocService.getProfessionalDetails(it)
                .data ?: PatientDetailsData()
        }
    }

    private suspend fun getCodingAsync() = coroutineScope {
        val insuranceClass = async {
            insuranceClassList.value = mDocService.getCodingKt(CodingRequest(INSURANCE_CLASS))
                .data.list
        }
        val supplementaryInsuranceClass = async {
            supplementaryInsuranceClassList.value =
                    mDocService.getCodingKt(CodingRequest(SUPPLEMENTARY_INSURANCE_CLASS))
                        .data.list
        }
        val relationshipStatus = async {
            val response = mDocService.getCodingKt(CodingRequest(RELATIONSHIP_STATUS_CODE))
            val tempList = response?.data?.list?.map {InsuranceDataKt(it.code,it.display)}
            tempList?.let {
                relationshipStatusList.value = ArrayList(it)
            }

            PatientDetailsHelper.getInstance().relationshipCodingResponse = response
        }

        val genderCodingStatus = async {
            val tempResponse = mDocService.getCodingKt(CodingRequest(GENDER_CODING))
            PatientDetailsHelper.getInstance().genderCodingResponse = tempResponse
        }

        val nationalityCodingStatus = async {
            val tempResponse = mDocService.getCodingKt(CodingRequest(NATIONALITY_CODING))
            PatientDetailsHelper.getInstance().nationalityCodingResponse = tempResponse
        }

        val languageCodingStatus = async {
            val tempResponse = mDocService.getCodingKt(CodingRequest(LANGUAGE_CODING))
            PatientDetailsHelper.getInstance().languageCodingResponse = tempResponse
        }

        val civilStatusCoding = async {
            val tempResponse = mDocService.getCodingKt(CodingRequest(CIVIL_STATUS_CODING))
            PatientDetailsHelper.getInstance().civilStatusCodingResponse = tempResponse
        }

        val religionStatusCoding = async {
            val tempResponse = mDocService.getCodingKt(CodingRequest(RELIGION_CODING))
            PatientDetailsHelper.getInstance().religionCodingResponse = tempResponse
        }

        awaitAll(insuranceClass, supplementaryInsuranceClass, relationshipStatus, genderCodingStatus, nationalityCodingStatus, languageCodingStatus, civilStatusCoding, religionStatusCoding)
    }

    private suspend fun getInsuranceAsync() = coroutineScope {
        val insurance = async {
            healthInsurances.value = mDocService.getInsurance(InsuranceRequest("krank"))
                .data
        }
        val accident = async {
            accidentInsurances.value = mDocService.getInsurance(InsuranceRequest("unfall"))
                .data
        }
        val military = async {
            militaryInsurances.value = mDocService.getInsurance(InsuranceRequest("militar"))
                .data
        }
        val disability = async {
            disabilityInsurances.value = mDocService.getInsurance(InsuranceRequest("invalid"))
                .data
        }
        val supplementary = async {
            supplementaryInsurances.value = mDocService.getInsurance(InsuranceRequest("zusatz"))
                .data
        }
        val supplementaryAccident = async {
            supplementaryAccidentInsurances.value =
                    mDocService.getInsurance(InsuranceRequest("unfall_zusatz"))
                        .data
        }
        awaitAll(insurance, accident, military, disability, supplementary, supplementaryAccident)
    }

    companion object {
        const val INSURANCE_CLASS = "https://mdoc.one/hl7/fhir/ukb-usz/insuranceTypes"
        const val SUPPLEMENTARY_INSURANCE_CLASS = "https://mdoc.one/hl7/fhir/ukb-usz/supplementaryInsuranceClass"
        const val RELATIONSHIP_STATUS_CODE = "https://www.hl7.org/fhir/v2/0063"
        const val GENDER_CODING = "https://www.hl7.org/fhir/v2/0001"
        const val NATIONALITY_CODING = "https://mdoc.one/hl7/fhir/countryList"
        const val LANGUAGE_CODING = "https://mdoc.one/hl7/fhir/ukb-usz/languages"
        const val CIVIL_STATUS_CODING = "https://www.hl7.org/fhir/v2/0002"
        const val RELIGION_CODING = "https://www.hl7.org/fhir/v2/0006"
    }
}
package de.mdoc.modules.devices.data

enum class BleMessage {
    USER_LIST, NO_USERS, USER_CREATED, LOG_IN_SUCCESS, USER_MEASUREMENT, LOG_IN_FAIL,CONNECTION_TIMEOUT
}
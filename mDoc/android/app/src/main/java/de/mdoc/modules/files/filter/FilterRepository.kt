package de.mdoc.modules.files.filter

import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.pojo.CodingItem
import timber.log.Timber

class FilterRepository(
    private val filesRepository: FilesRepository
) {

    fun getInitialFilters(onSuccess: (List<FilterCategory>) -> Unit) {
        filesRepository.getCategoryCoding({
            buildFilters(it.list, onSuccess)
        }, {
            Timber.w(it)
        })
    }

    private fun buildFilters(source: List<CodingItem>, onSuccess: (List<FilterCategory>) -> Unit) {
        val result = mutableListOf<FilterCategory>()
        result.add(FilterCategory.AllCategories)
        var ids = 2L
        result.addAll(
            source.map {
                FilterCategory.SingleCategory(it, ids++)
            }
        )
        onSuccess.invoke(result)
    }

}
package de.mdoc.modules.questionnaire

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.data.questionnaire.Answer
import de.mdoc.data.questionnaire.AnswersRequest
import de.mdoc.data.responses.QuestionnaireAnswersResponse
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.questionnaire.questionnaire_adapter.QuestionnaireAnswersAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.getErrorDetails
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_questionnaire_answers_history.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class QuestionnaireAnswersFragment: MdocFragment() {

    lateinit var activity: MdocActivity
    var singleUseData: ArrayList<Answer>? = ArrayList()
    private val args: QuestionnaireAnswersFragmentArgs by navArgs()

    override fun setResourceId(): Int {
        return R.layout.fragment_questionnaire_answers_history
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireHistory

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_questionnaire_answers?.visibility = View.GONE

        progress_questionnaire_answer?.showNow()

        getSingleQuestionnaireAnswers()
    }

    private fun getSingleQuestionnaireAnswers() {
        val pollVotingActionsId: String = args.pollVotingActionId ?: ""
        val request = AnswersRequest()
        request.includeAnswers = true

        request.pollVotingActionId = pollVotingActionsId
        val from = request.from
        val to = request.to
        val pollVotingActionId = request.pollVotingActionId
        val pollId = request.pollId
        val includeAnswers = request.includeAnswers

        MdocManager.getAnsweredQuestionnaires(from, to, pollVotingActionId, pollId, includeAnswers,
                object: Callback<QuestionnaireAnswersResponse> {
                    override fun onResponse(call: Call<QuestionnaireAnswersResponse>,
                                            response: Response<QuestionnaireAnswersResponse>) {
                        if (response.isSuccessful) {
                            singleUseData = response.body()?.data?.getOrNull(0)?.questionnaires?.getOrNull(
                                    0)?.pollVotingActions?.getOrNull(0)?.answers
                            var lang = response.body()?.data?.getOrNull(0)?.questionnaires?.getOrNull(
                                    0)?.pollVotingActions?.getOrNull(0)?.language
                            singleUseData?.sortBy { it.question?.index }
                            if (singleUseData != null) {
                                rv_questionnaire_answers?.visibility = View.VISIBLE
                                rv_questionnaire_answers?.layoutManager = LinearLayoutManager(activity)
                                rv_questionnaire_answers?.adapter =
                                        QuestionnaireAnswersAdapter(activity, singleUseData!!, lang)
                                txt_finish_time?.text = parseFinishDate(
                                        response.body()?.data?.getOrNull(0)?.questionnaires?.getOrNull(
                                                0)?.pollVotingActions?.getOrNull(0)?.finished
                                                ?: 0)
                            }
                            progress_questionnaire_answer?.hideNow()
                        }
                        else {
                            Timber.w("getAnsweredQuestionnaires ${response.getErrorDetails()}")
                            progress_questionnaire_answer?.hideNow()
                        }
                    }

                    override fun onFailure(call: Call<QuestionnaireAnswersResponse>, t: Throwable) {
                        progress_questionnaire_answer?.hideNow()
                        Timber.w(t, "getAnsweredQuestionnaires")
                    }
                })
    }

    private fun parseFinishDate(input: Long): String {
        val date = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, input)
        val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, input)

        return resources.getString(R.string.closed_history_questionnaire, date, time)
    }

}
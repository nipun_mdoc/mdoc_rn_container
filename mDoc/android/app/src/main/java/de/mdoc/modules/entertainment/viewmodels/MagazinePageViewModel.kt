package de.mdoc.modules.entertainment.viewmodels

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import de.mdoc.modules.entertainment.repository.MagazinesRepository
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MagazinePageViewModel(
        private val mdocAppHelper: MdocAppHelper,
        private val repository: MagazinesRepository): ViewModel () {

    val mediaLiveData = liveData<Bitmap>()
    val isError = liveData(false)
    val isProgress = liveData(false)

    fun getMediaById (pdfId: String, pageNumber: String) {
        isError.set(false)
        repository.getMediaById (pdfId, pageNumber, ::onDataLoaded, ::onError)
    }

    private fun onDataLoaded(data: Bitmap) {
        GlobalScope.launch {
            withContext(Dispatchers.Main){
                mediaLiveData.set(data)
                isProgress.set(false)
            }
        }
    }

    private fun onError () {
        GlobalScope.launch {
            withContext(Dispatchers.Main){
                isError.set(true)
                isProgress.set(false)
            }
        }
    }
}
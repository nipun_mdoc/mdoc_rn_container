package de.mdoc.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import androidx.core.view.forEachIndexed
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import de.mdoc.R
import de.mdoc.activities.navigation.MenuItem
import de.mdoc.activities.navigation.NavigationHandler
import de.mdoc.activities.navigation.NavigationViewModel
import de.mdoc.activities.navigation.isTablet
import de.mdoc.activities.navigation.permission.LowPermission
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.notification.adapters.NotificationAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.network.response.NotificationResponse
import de.mdoc.newlogin.Interactor.LogiActivty
import de.mdoc.pojo.ConfigurationData
import de.mdoc.util.ColorUtil
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.util.ProgressDialog
import kotlinx.android.synthetic.main.activity_mdoc.*
import kotlinx.android.synthetic.main.activity_mdoc.collapsedMenuLl
import kotlinx.android.synthetic.main.menu_layout.*
import kotlinx.android.synthetic.main.menu_layout_collapsed.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import com.google.gson.Gson;
import de.mdoc.pojo.ConfigurationListItem
import de.mdoc.pojo.SubConfigurationData

abstract class MainActivity: MdocBaseActivity() {

    val IS_OPEN_MENU = "is_open_menu"
    var data = ""
    var isOpenMenu = true
    var parentProgressDialog: ProgressDialog? = null
    private val model: MainActivityViewModel  by viewModels()

    val notifyReceiver = object: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            getNotifications()
        }
    }
    private lateinit var navigationViewModel: NavigationViewModel
    private lateinit var navigationHandler: NavigationHandler

    fun getNavigationViewModel(): NavigationViewModel {
        return navigationViewModel
    }
    public var navController: NavController? = null;
    public var navigationBar: Toolbar? = null;
    public var bottomBar: BottomNavigationView? = null;



    override fun onCreate(savedInstanceState: Bundle?) {

        navigationViewModel = ViewModelProviders.of(this)
            .get(NavigationViewModel::class.java)
        navigationViewModel.onMenuItemClickListener = ::onMenuItemClick

        navigationHandler = NavigationHandler(this, navigationViewModel, this)
        parentProgressDialog = ProgressDialog(this)
        MdocAppHelper.getInstance().notificationIndex.observe(this, Observer {
            navigationViewModel.countOfNotifications.value = it ?: 0
        })
        super.onCreate(savedInstanceState)
        setupNavigation()
        getAppointmentDataFromCoding()
        menuContainer?.let {
            navigationHandler.setupMenu(it)
        }
    }
    private fun getAppointmentDataFromCoding(){


        MdocManager.getCoding("https://mdoc.one/coding/metadata_triggers_notification",
                object: Callback<CodingResponse> {
                    override fun onResponse(call: Call<CodingResponse>,
                                            response: Response<CodingResponse>) {
                        if (response.isSuccessful) {
                            for (item in response.body()!!.data?.list!!) {
                                if(item?.code == "TITLE"){
                                    MdocConstants.appointment_title = item?.display.toString()
                                    break
                                }
                            }

                        }
                    }

                    override fun onFailure(call: Call<CodingResponse>, t: Throwable) {
                    }
                })

    }

    private fun setupNavigation() {
        navController = findNavController(R.id.navigation_host_fragment)
        navigationBar = findViewById<Toolbar>(R.id.navigationBar)
        bottomBar = findViewById<BottomNavigationView>(R.id.bottom_navigation_view)
        val appBarConfiguration = AppBarConfiguration(navController!!.graph)
        findViewById<Toolbar>(R.id.navigationBar).setupWithNavController(navController!!, appBarConfiguration)

        bottom_navigation_view?.let { it ->
            inflateBottomMenu(it)

            NavigationUI.setupWithNavController(it, navController!!)
            val badge: BadgeDrawable = it.getOrCreateBadge(R.id.notificationFragment)

            navigationViewModel.countOfNotifications.observe(this, Observer { counter ->
                if(counter != null && counter > 0) {
                    badge.number = counter
                    badge.isVisible = true
                } else {
                    badge.isVisible = false
                }
            })
        }


        setSupportActionBar(navigationBar)
        visibilityNavElements(navController!!)
        navigationBar?.setTitleTextColor(ColorUtil.contrastColor(resources))
    }

    var isDashBoardClicked: Boolean = false
    fun hideBottomAndTopBar(){
        navigationBar?.visibility = View.GONE
        bottomBar?.visibility = View.GONE
    }

    fun showBottomAndTopBar(){
        if(!isDashBoardClicked){
            navigationBar?.visibility = View.VISIBLE
        }
        bottomBar?.visibility = View.VISIBLE
    }

    private fun setBackIconColor(id: Int) {
        val baseColor = when (id) {
            R.id.moreFragment -> {
                R.color.colorNavHeaderMore
            } else -> {
                R.color.colorPrimary
            }
        }

        val closeIcon =  ContextCompat.getDrawable(this, R.drawable.ic_arrow_back)
        closeIcon?.colorFilter = PorterDuffColorFilter(ColorUtil.contrastColor(resources, baseColor), PorterDuff.Mode.SRC_ATOP)
        navigationBar?.navigationIcon = closeIcon
    }

    private fun inflateBottomMenu (it: BottomNavigationView) {
        if (LowPermission(resources).hasPermissionToDisplay()) {
            it.inflateMenu(R.menu.menu_main_bottom_navigation)
        } else {
            it.inflateMenu(R.menu.menu_main_bottom_low_permission)
        }

        it.menu.forEachIndexed { index, element ->
            val configs = MdocAppHelper.getInstance().configurationData.list.filter { it.id== MdocAppHelper.getInstance().getPairedIdForBottomNav(getResources().getResourceEntryName(element.itemId)) } as ArrayList<ConfigurationListItem>
            var isVisible = false;
            var label = "";
            var imageUrl = "";
            if(configs != null && configs.size > 0) {
                val con = configs[0];
                if (con.isEnabled) {
                    var subConfigs = con.subConfig.filter { it.id == MdocConstants.bottom_nav } as ArrayList<SubConfigurationData>
                    if(subConfigs != null && subConfigs.size > 0) {
                        if (subConfigs[0].isEnabled) {
                            isVisible = true;
                            label = con.featureTitle;
//                    imageUrl = con.icon
                        }
                    }
                }
            }
            if(!isVisible){
                it.menu.getItem(index).setVisible(false)
            }else{
//                if(label.isNotEmpty()) {
//                    it.menu.getItem(index).title = label;
//                }
//                if(imageUrl.isNotEmpty()) {
//                    Picasso.get()
//                            .load(imageUrl)
//                            .error(R.drawable.circle_white)
//                            .into(object : com.squareup.picasso.Target {
//                                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
//                                    val mBitmapDrawable = BitmapDrawable(resources, bitmap)
//                                    it.menu.getItem(index).setIcon(mBitmapDrawable)
//                                }
//
//                                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
//
//                                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
//                            })
//                }
            }
        }


    }

    private fun visibilityNavElements(navController: NavController) {
        val destinationsWithoutTabBar = listOf(
                R.id.fragmentMedicationPlan,
                R.id.fragmentConfirmMedication,
                R.id.entertainmentMagazine,
                R.id.startGoalCreationFragment,
                R.id.goalPeriodFragment,
                R.id.actionItemsFragment,
                R.id.stopAndThinkFragment,
                R.id.ifThenFragment,
                R.id.createGoalFragment,
                R.id.goalDetailFragment,
                R.id.noteGoalStatusFragment,
                R.id.changeGoalStatusFragment,
                R.id.mediaVideoFragment)

        navController.addOnDestinationChangedListener { _, destination, _ ->

            setNavHeaderColor(destination.id)
            setStatusBarColor(destination.id)
            setBackIconColor(destination.id)

            when (destination.id) {
                in destinationsWithoutTabBar -> {
                    bottom_navigation_view?.visibility = View.GONE
                    navigationBar?.setPadding(0,0,0,0);
                }

                R.id.dashboardFragment -> {
                    isDashBoardClicked = true
                    bottom_navigation_view?.visibility = View.VISIBLE
                    navigationBar?.visibility = View.GONE
                    navigationBar?.setPadding(0,0,0,0);
                }

                R.id.documentReviewFragment -> {
                    isDashBoardClicked = false
                    bottom_navigation_view?.visibility = View.GONE
                    navigationBar?.visibility = View.GONE
                    navigationBar?.setPadding(0,0,0,0);
                }

                R.id.signDocumentFragment -> {
                    isDashBoardClicked = false
                    bottom_navigation_view?.visibility = View.GONE
                    navigationBar?.visibility = View.GONE
                    navigationBar?.setPadding(0,0,0,0);
                }

                R.id.moreFragment -> {
                    isDashBoardClicked = false
                    bottom_navigation_view?.visibility = View.VISIBLE
                    navigationBar?.visibility = View.VISIBLE
                    navigationBar?.logo = ContextCompat.getDrawable(this, R.drawable.toolbar_logo)
                    navigationBar?.setPadding(0,0,MdocUtil.dpToPx(30, this),0);
                }
                else                         -> {
                    isDashBoardClicked = false
                    bottom_navigation_view?.visibility = View.VISIBLE
                    navigationBar?.logo = null
                    navigationBar?.visibility = View.VISIBLE
                    navigationBar?.setPadding(0,0,0,0);
                }
            }
        }
    }

    private fun setNavHeaderColor (id: Int) {
        if (id == R.id.moreFragment) {
            navigationBar?.setBackgroundColor(ContextCompat.getColor(this, R.color.colorNavHeaderMore))
        } else {
            navigationBar?.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
        }
    }

    private fun setStatusBarColor (id: Int) {
        when (id) {
            R.id.dashboardFragment -> {
                setCustomStatusBar()
            }

            R.id.moreFragment -> {
                when (val color = ContextCompat.getColor(this, R.color.colorNavHeaderMore)) {
                    ContextCompat.getColor(this, R.color.colorPrimary) -> {
                        setDefaultStatusBar()
                    }
                    else -> {
                        setCustomStatusBar(color)
                    }
                }
            }
            else -> {
                setDefaultStatusBar()
            }
        }
    }

    private fun setCustomStatusBar(color:Int = ContextCompat.getColor(this, R.color.white)) {
        window.statusBarColor = color
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
    }

    private fun setDefaultStatusBar() {
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimary)
        window.decorView.systemUiVisibility =
                window.decorView.systemUiVisibility and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
    }

    private fun onMenuItemClick(menuItem: MenuItem) {
        val hostId = R.id.navigation_host_fragment
        when (menuItem) {
            MenuItem.Dashboard        -> {
                isDashBoardClicked = true
                findNavController(hostId).popBackStack(R.id.dashboardFragment, false)
            }

            MenuItem.Questionnaire    -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_questionnaire)
            }

            MenuItem.Therapy          -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.global_action_to_tab2)
            }

            MenuItem.CheckList        -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_checklist)
            }

            MenuItem.Vitals           -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.global_action_vitals)
            }

            MenuItem.Media            -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_media)
            }

            MenuItem.MyProfile        -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_profile)
            }

            MenuItem.BookAppointment  -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_appointment_booking)
            }

            MenuItem.Devices          -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_devices)
            }

            MenuItem.AirAndPollen     -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_air_and_pollen)
            }

            MenuItem.HelpSupport      -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_help_support)
            }

            MenuItem.Diary            -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_diary)
            }

            MenuItem.Entertainment    -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.entertainment_navigation_graph)
            }

            MenuItem.ExternalServices -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.externalServicesFragment)
            }

            MenuItem.Covid19          -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_covid19)
            }

            MenuItem.PatientJourney   -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_patient_journey)
            }

            MenuItem.Files            -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_files)
            }

            MenuItem.Medication       -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_medications)
            }

            MenuItem.Messages         -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_messages)
            }

            MenuItem.MyClinic         -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_my_stay)
            }

            MenuItem.MentalGoals      -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_mental_goal)
            }

            MenuItem.Consent          -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.consentFragment)
            }

            MenuItem.FamilyAccount    -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.navigation_family_account)
            }

            MenuItem.Notifications -> {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.notificationFragment)
            }

            MenuItem.MealPlan         ->  {
                isDashBoardClicked = false
                if (isTablet()) {
                    if (resources.getBoolean(R.bool.can_user_choose_meal)) {
                        findNavController(hostId).navigate(R.id.fragmentMealPlanTabletNew)
                    } else {
                        findNavController(hostId).navigate(R.id.fragmentMealPlanCantChooseTablet)
                    }
                }
                else {
                    if (resources.getBoolean(R.bool.can_user_choose_meal)) {
                        findNavController(hostId).navigate(R.id.fragmentMealPlanNew)
                    } else {
                        findNavController(hostId).navigate(R.id.fragmentMealPlanCantChoose)
                    }
                }
            }

            MenuItem.Exercise         ->  {
                isDashBoardClicked = false
                findNavController(hostId).navigate(R.id.fragmentExercises)
            }
        }
    }

    fun openMenu() {
        openMenuLl?.visibility = View.VISIBLE
        collapsedMenuLl?.visibility = View.GONE
        model.setOpenMenu(true)
    }

    fun collapseMenu() {
        openMenuLl?.visibility = View.GONE
        collapsedMenuLl?.visibility = View.VISIBLE
        model.setOpenMenu(false)
    }

    override fun onSaveInstanceState(savedInstanceState: Bundle) {
        super.onSaveInstanceState(savedInstanceState)
        savedInstanceState.putBoolean(IS_OPEN_MENU, isOpenMenu)
    }
    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun getNotifications() {
        MdocManager.postForAllNotifcations(0, MdocConstants.PUSH_NOTIFICATION_LIST_LIMIT,applicationContext, object: Callback<NotificationResponse> {
            override fun onResponse(
                    call: Call<NotificationResponse>,
                    response: Response<NotificationResponse>
                                   ) {
                if (response.isSuccessful) {
                    val data = response.body()?.data?.list
                    MdocAppHelper.getInstance().setNotifications(data)
                }
            }

            override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {}
        })
    }

    fun handleClickListeners() {
        shortLogoRl?.setOnClickListener {
            onShortLogoRlClick()
        }

        logoRl?.setOnClickListener {
            onLogoRlClick()
        }

        signoutRl?.setOnClickListener {
            onSignoutRlClick()
        }

        signoutShortIv?.setOnClickListener {
            onSignoutRlClick()
        }
    }

    fun onSignoutRlClick() {
        var loginIntent : Intent
        if(getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
            loginIntent = Intent(this, LogiActivty::class.java)
        }else{
            loginIntent = Intent(this, LoginActivity::class.java)
        }
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(loginIntent)
        this.finish()
    }

    fun onLogoRlClick() {
        isOpenMenu = false
        collapseMenu()
    }

    private fun onShortLogoRlClick() {
        isOpenMenu = true
        openMenu()
    }

    override fun onDestroy() {
        super.onDestroy()
        navigationViewModel.onMenuItemClickListener = {}
    }

    override fun onOptionsItemSelected(item: android.view.MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}

class MainActivityViewModel : ViewModel() {
    val isOpenMenu = MutableLiveData<Boolean>()
    fun setOpenMenu(result: Boolean) {
        isOpenMenu.value = result
    }

    fun getOpenMenu(): LiveData<Boolean>?{
        return isOpenMenu
    }
}
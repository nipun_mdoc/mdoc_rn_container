package de.mdoc.data.responses

import de.mdoc.data.create_bt_device.BtDevice

data class DevicesResponse(
        val data:List<BtDevice>){

    fun getManualDeviceId():String?{
        for(item in data){
            if (item.model=="MANUAL"){

                return item.id
            }
        }
        return null
    }
}
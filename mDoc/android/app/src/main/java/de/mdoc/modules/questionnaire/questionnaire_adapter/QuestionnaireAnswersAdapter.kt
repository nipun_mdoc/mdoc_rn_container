package de.mdoc.modules.questionnaire.questionnaire_adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.data.questionnaire.Answer
import kotlinx.android.synthetic.main.placeholder_questionnaire_answers.view.*
import java.util.*
import kotlin.collections.ArrayList

class QuestionnaireAnswersAdapter(var context: Context, var answers:ArrayList<Answer>, var lang: String?) : RecyclerView.Adapter<QuestionnaireAnswersViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionnaireAnswersViewHolder {


        val view = LayoutInflater.from(parent.context).inflate(R.layout.placeholder_questionnaire_answers, parent, false)

        context = parent.context

        return QuestionnaireAnswersViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuestionnaireAnswersViewHolder, position: Int) {



        val answerObject = answers[position]
        val ordinal=answerObject.question?.ordinal
        var ques = "";
        if(answerObject.question?.questionData?.question?.get(lang) == null){
            if(answerObject.question?.questionData?.question?.get("en") == null){
                ques = answerObject.question?.questionData?.question?.get("default")!!;
            }else{
                ques = answerObject.question?.questionData?.question.get("en")!!;
            }
        }else{
            ques = answerObject.question?.questionData?.question.get(lang)!!
        }
//        if(answerObject.question?.questionData?.question?.get(Locale.getDefault().language) == null){
//            ques = answerObject.question?.questionData?.question?.get("en")!!;
//        }else{
//            ques = answerObject.question?.questionData?.question?.get(Locale.getDefault().language)!!
//        }
        val question=ordinal+" "+ques
        holder.txtQuestion.text = question

        val answer=answerObject.answerData

        if(answerObject.selection is ArrayList<*>&&!answerObject.selection.isNullOrEmpty()) {
            if (!answerObject.selection.isNullOrEmpty()) {
                val selectionList = fillSelectionItems(answerObject.selection)
                holder.txtAnswer.text = ""
                for (item in selectionList) {

                    answerObject.question?.questionData?.options?.forEach {option ->

                        if (item == option.key) {
                            var ans = "";
                            if(option.value.get(lang) == null){
                                if(option.value.get("en") == null){
                                    ans = option.value.get("default")!!;
                                }else{
                                    ans = option.value.get("en")!!;
                                }
                            }else{
                                ans = option.value.get(lang)!!
                            }
                            holder.txtAnswer.append(ans)
                            holder.txtAnswer.append(" \n")

                        }
                    }
                }

            }

        }
        else{
            holder.txtAnswer.text = answer
            holder.txtAnswer.append(" \n")

        }

    }

    private fun fillSelectionItems(selection: List<Any>): ArrayList<Any> {
        val selectionItems= ArrayList<Any>()
        for(item in selection){
            selectionItems.add(item)
        }
        return selectionItems
    }

    override fun getItemCount(): Int {
        return answers.size
    }

}

class QuestionnaireAnswersViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txtQuestion:TextView=itemView.txt_question
    val txtAnswer:TextView=itemView.txt_answer


}


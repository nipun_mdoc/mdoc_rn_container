package de.mdoc.modules.media

import android.content.pm.ActivityInfo
import android.net.Uri
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import android.widget.MediaController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.hideActionBar
import kotlinx.android.synthetic.main.fragment_media_video.*

class MediaVideoFragment: MdocBaseFragment() {

    private val args: MediaVideoFragmentArgs by navArgs()

    override fun setResourceId(): Int {
        return R.layout.fragment_media_video
    }

    override fun init(savedInstanceState: Bundle?) {}

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        hideActionBar()
        super.onViewCreated(view, savedInstanceState)
        if(savedInstanceState == null) {
            activity?.apply {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
            }
        }
        handleOnBackPressed {
            navigateBack()
        }
        initPlayer()
    }

    private fun navigateBack() {
        activity?.apply {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        val hasTransitionFragment = findNavController().popBackStack(R.id.mediaTransitionFragment, true)

        if (!hasTransitionFragment) {
            findNavController().popBackStack()
        }
    }

    private fun initPlayer(){
        val mc = object: MediaController(activity) {
            override fun dispatchKeyEvent(event: KeyEvent): Boolean {
                if (event.keyCode == KeyEvent.KEYCODE_BACK) {
                    if (event.action == KeyEvent.ACTION_DOWN) {
                        navigateBack()
                        return true
                    }
                    else if (event.action == KeyEvent.ACTION_UP) {
                        return true
                    }
                }
                return super.dispatchKeyEvent(event)
            }
        }

        mc.setAnchorView(videoView)
        mc.setMediaPlayer(videoView)
        val video = Uri.parse(args.videoUrl)
        videoView.setMediaController(mc)
        videoView.setVideoURI(video)
        videoView.requestFocus()
        videoView.start()
    }
}


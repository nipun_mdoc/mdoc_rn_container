package de.mdoc.modules.mental_goals.overview.data

data class GamificationData(
    val countNew: Int,
    val countAlmostReached: Int,
    val countCompleted: Int
)
package de.mdoc.modules.meal_plan.view_model

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.ListView
import de.mdoc.R
import de.mdoc.util.MdocAppHelper
import java.util.ArrayList

class MealHelperClick {

    fun showAditivesDialog(view: View, infoList: ArrayList<String>) {
        val builder = AlertDialog.Builder(view.context)
        val inflater = view.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.aditives_layout, null)
        builder.setView(view)
        val dialog = builder.show()
        val closeIv = view.findViewById<ImageView>(R.id.closeIv)
        val aditivesLv = view.findViewById<ListView>(R.id.aditivesLv)
        val hashMapAllergens = MdocAppHelper.getInstance().allergens
        val hashMapAdditives = MdocAppHelper.getInstance().additives

        val showAllergens = java.util.ArrayList<String>()

        for (j in infoList.indices) {
            val value = hashMapAdditives[infoList[j]]
            if (value != null) {
                showAllergens.add(" • $value")
            }
        }

        for (j in infoList.indices) {
            val value = hashMapAllergens[infoList[j]]
            if (value != null) {
                showAllergens.add(" • $value")
            }
        }

        val additivesAdapter = ArrayAdapter(view.context, android.R.layout.simple_list_item_1, showAllergens)

        aditivesLv.adapter = additivesAdapter

        closeIv.setOnClickListener { dialog.dismiss() }
    }
}
package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.QuestionnaireData;


/**
 * Created by ema on 12/23/16.
 */

public class QuestionnaireResponse extends MdocResponse {

    private ArrayList<QuestionnaireData> data;

    public ArrayList<QuestionnaireData> getData() {
        return data;
    }

    public void setData(ArrayList<QuestionnaireData> data) {
        this.data = data;
    }
}

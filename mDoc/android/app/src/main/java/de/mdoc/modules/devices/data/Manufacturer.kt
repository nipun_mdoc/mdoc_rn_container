package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Manufacturer(val system: String,
                        val code:String,
                        val display:String,
                        val cts:String,
                        val uts:String,
                        val imageBase64:String,
                        val name: String):Parcelable

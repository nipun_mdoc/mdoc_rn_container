package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ema on 10/17/17.
 */

public class MealPlan implements Serializable {

    private String clinicId;
    private String date;
    private long dateNumber;
    private String mealPlanId;
    private String name;
    private ArrayList<MealOffer> offers;
    private MealChoice mealChoice;
    private boolean mealPlanRatingRule;

    public boolean isMealPlanRatingRule() {
        return mealPlanRatingRule;
    }

    public void setMealPlanRatingRule(boolean mealPlanRatingRule) {
        this.mealPlanRatingRule = mealPlanRatingRule;
    }

    public MealChoice getMealChoice() {
        return mealChoice;
    }

    public void setMealChoice(MealChoice mealChoice) {
        this.mealChoice = mealChoice;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public long getDateNumber() {
        return dateNumber;
    }

    public void setDateNumber(long dateNumber) {
        this.dateNumber = dateNumber;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<MealOffer> getOffers() {
        return offers;
    }

    public void setOffers(ArrayList<MealOffer> offers) {
        this.offers = offers;
    }
}

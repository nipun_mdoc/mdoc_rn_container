package de.mdoc.data.responses

import de.mdoc.data.media.MediaListItem
import de.mdoc.data.media.MediaStatisticsData

data class MediaStatisticsResponse(
        val data: MediaListItem
)
package de.mdoc.data.media

data class Content(
        val category: String,
        val coverURL: String,
        val cts: Long,
        val id: String,
        val magazineId: String,
        val magazineName: String,
        val mediaType: String,
        val occurrenceDateTime: Long,
        val `operator`: String,
        val pdfId: String,
        val pdfURL: String,
        val publicationDate: String,
        val publicationId: String,
        val publisherName: String,
        val videoURL: String,
        val uts: Long,
        val description:String
)
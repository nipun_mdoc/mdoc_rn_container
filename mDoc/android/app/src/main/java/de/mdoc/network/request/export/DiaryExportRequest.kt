package de.mdoc.network.request.export

data class DiaryExportRequest(val clinicIds: List<String>, val caseId: String, val from: Long? = null, val to: Long? = null, val diaryConfigId: List<String>? = null)
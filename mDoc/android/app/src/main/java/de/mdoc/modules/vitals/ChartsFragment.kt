package de.mdoc.modules.vitals

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.data.create_bt_device.MedicalDeviceAdapterData
import de.mdoc.data.requests.ObservationStatisticsRequest
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.devices.DevicesUtil
import de.mdoc.modules.devices.data.ChartData
import de.mdoc.modules.devices.data.Data
import de.mdoc.modules.devices.data.ObservationPeriodType
import de.mdoc.modules.devices.data.ObservationResponse
import de.mdoc.modules.vitals.FhirConfigurationUtil.codesForComposite
import de.mdoc.modules.vitals.FhirConfigurationUtil.getGraphType
import de.mdoc.modules.vitals.FhirConfigurationUtil.getTitleByCode
import de.mdoc.modules.vitals.FhirConfigurationUtil.isCompositeByCode
import de.mdoc.modules.vitals.charts.createChart
import de.mdoc.modules.vitals.charts.fillChartText
import de.mdoc.modules.vitals.export.VitalsExportFragment
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.getErrorDetails
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocUtil
import de.mdoc.util.onCreateOptionsMenu
import kotlinx.android.synthetic.main.fragment_vitals_chart.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class ChartsFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Vitals
    lateinit var activity: MdocActivity
    lateinit var item: MedicalDeviceAdapterData
    private var mainDate = Date()
    private var mainCalendar: GregorianCalendar = GregorianCalendar()
    private var metricCodeConst: String = ""

    private val args: ChartsFragmentArgs by navArgs()
    lateinit var metricCode :ArrayList<String>
    lateinit var graphTypeToSend: String
    lateinit var measurementUnit : String

    private var request: ObservationStatisticsRequest? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity
        return inflater.inflate(R.layout.fragment_vitals_chart, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        item = DevicesUtil.deviceItem ?: MedicalDeviceAdapterData()
        metricCode = args.metricCode.toCollection(ArrayList())
        graphTypeToSend = args.graphTypeToSend
        measurementUnit = args.measurementUnit
        metricCodeConst = metricCode.getOrNull(0) ?: ""
        fillCodesForCompositeMeasure()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        val chartTitle = getTitleByCode(metricCodeConst)
        txt_chart_title?.text = chartTitle
        setListeners()

        tabSelector(txt_day_label)
    }

    private fun setListeners() {
        txt_day_label.setOnClickListener {
            tabSelector(txt_day_label)
        }

        txt_week_label.setOnClickListener {
            tabSelector(txt_week_label)
        }

        txt_month_label.setOnClickListener {
            tabSelector(txt_month_label)
        }

        txt_year_label.setOnClickListener {
            tabSelector(txt_year_label)
        }

        ic_arrow_left.setOnClickListener {
            subtractDatePeriod(clickedTab())
        }

        ic_arrow_right.setOnClickListener {
            addDatePeriod(clickedTab())
        }
    }

    private fun tabSelector(textView: TextView) {
        textView.background = activity.getDrawable(R.drawable.rectangle_tag_history_device)

        when (textView) {
            txt_day_label   -> {
                mainCalendar.time = mainDate
                mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
                mainCalendar.set(Calendar.MINUTE, 0)
                mainCalendar.set(Calendar.SECOND, 0)
                mainCalendar.set(Calendar.MILLISECOND, 0)
                val calendarTo = mainCalendar.clone() as Calendar
                calendarTo.add(Calendar.HOUR, 23)
                val parsedDate = MdocUtil.getParsedDate("EEEE", mainCalendar) + ", " +
                        MdocUtil.getParsedDate("dd", mainCalendar) + "." +
                        MdocUtil.getParsedDate("MM", mainCalendar) + "." +
                        MdocUtil.getParsedDate("yyyy", mainCalendar)
                txt_selector_label.text = parsedDate
                txt_average_date.text = parsedDate

                txt_week_label.background = null
                txt_month_label.background = null
                txt_year_label.background = null

                getObservationStatistics(ObservationPeriodType.DAY, generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarTo.timeInMillis,
                        periodType = ObservationPeriodType.HOUR.name))
            }

            txt_week_label  -> {
                mainCalendar.time = mainDate
                mainCalendar.firstDayOfWeek = Calendar.MONDAY
                mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
                mainCalendar.set(Calendar.MINUTE, 0)
                mainCalendar.set(Calendar.SECOND, 0)
                mainCalendar.set(Calendar.MILLISECOND, 0)
                mainCalendar.set(Calendar.DAY_OF_WEEK, mainCalendar.firstDayOfWeek)
                val calendarTo = mainCalendar.clone() as Calendar
                calendarTo.add(Calendar.DAY_OF_WEEK, 7)
                calendarTo.add(Calendar.HOUR_OF_DAY, -1)
                val parsedDate = MdocUtil.getParsedDate("dd", mainCalendar) +
                        ".${MdocUtil.getParsedDate("MM", mainCalendar)}" +
                        ". - ${MdocUtil.getParsedDate("dd", calendarTo)}" +
                        ".${MdocUtil.getParsedDate("MM", calendarTo)}."

                txt_selector_label.text = parsedDate
                txt_average_date.text = parsedDate


                txt_day_label.background = null
                txt_month_label.background = null
                txt_year_label.background = null

                getObservationStatistics(ObservationPeriodType.WEEK, generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarTo.timeInMillis,
                        periodType = ObservationPeriodType.DAY.name))
            }

            txt_month_label -> {
                mainCalendar.time = mainDate
                mainCalendar.set(Calendar.HOUR_OF_DAY, 1)
                mainCalendar.set(Calendar.MINUTE, 0)
                mainCalendar.set(Calendar.SECOND, 0)
                mainCalendar.set(Calendar.MILLISECOND, 0)
                mainCalendar.set(Calendar.DAY_OF_MONTH, mainCalendar.getActualMinimum(Calendar.DAY_OF_MONTH))
                val calendarTo = mainCalendar.clone() as Calendar
                calendarTo.set(Calendar.DAY_OF_MONTH, calendarTo.getActualMaximum(Calendar.DAY_OF_MONTH))
                calendarTo.add(Calendar.HOUR_OF_DAY, 23)
                val parsedDate = MdocUtil.getParsedDate("MMMM", mainCalendar) + "  " +
                        MdocUtil.getParsedDate("yyyy", mainCalendar)
                txt_selector_label.text = parsedDate
                txt_average_date.text = parsedDate

                txt_day_label.background = null
                txt_week_label.background = null
                txt_year_label.background = null

                getObservationStatistics(ObservationPeriodType.MONTH, generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarTo.timeInMillis,
                        periodType = ObservationPeriodType.DAY.name))
            }

            txt_year_label  -> {
                mainCalendar.time = mainDate
                mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
                mainCalendar.set(Calendar.MINUTE, 0)
                mainCalendar.set(Calendar.SECOND, 0)
                mainCalendar.set(Calendar.MILLISECOND, 0)
                mainCalendar.set(Calendar.DAY_OF_YEAR, mainCalendar.getActualMinimum(Calendar.DAY_OF_YEAR))
                val calendarTo = mainCalendar.clone() as Calendar
                calendarTo.set(Calendar.DAY_OF_YEAR, calendarTo.getActualMaximum(Calendar.DAY_OF_YEAR))
                calendarTo.add(Calendar.HOUR_OF_DAY, 23)
                val parsedDate = MdocUtil.getParsedDate("yyyy", mainCalendar)
                txt_selector_label.text = parsedDate
                txt_average_date.text = parsedDate

                txt_day_label.background = null
                txt_week_label.background = null
                txt_month_label.background = null

                getObservationStatistics(ObservationPeriodType.YEAR, generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarTo.timeInMillis,
                        periodType = ObservationPeriodType.MONTH.name))
            }
        }
    }

    private fun addDatePeriod(textView: TextView) {
        when (textView) {
            txt_day_label   -> {
                moveDay(1)
            }

            txt_week_label  -> {
                moveWeek(7)
            }

            txt_month_label -> {
                moveMonth(1)
            }

            txt_year_label  -> {
                moveYear(1)
            }
        }
    }

    private fun subtractDatePeriod(textView: TextView) {
        when (textView) {
            txt_day_label   -> {
                moveDay(-1)
            }

            txt_week_label  -> {
                moveWeek(-7)
            }

            txt_month_label -> {
                moveMonth(-1)
            }

            txt_year_label  -> {
                moveYear(-1)
            }
        }
    }

    private fun moveDay(dayToMove: Int) {
        mainCalendar.add(Calendar.DATE, dayToMove)
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)
        val parsedDate = MdocUtil.getParsedDate("EEEE", mainCalendar) + ", " +
                MdocUtil.getParsedDate("dd", mainCalendar) + "." +
                MdocUtil.getParsedDate("MM", mainCalendar) + "." +
                MdocUtil.getParsedDate("yyyy", mainCalendar)
        txt_selector_label.text = parsedDate
        txt_average_date.text = parsedDate
        val calendarEndDay = mainCalendar.clone() as GregorianCalendar
        calendarEndDay.add(Calendar.HOUR_OF_DAY, 23)
        calendarEndDay.add(Calendar.MINUTE, 59)

        getObservationStatistics(
                periodType = ObservationPeriodType.DAY,
                request = generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarEndDay.timeInMillis,
                        periodType = ObservationPeriodType.HOUR.name
                                         ))
    }

    private fun moveWeek(dayToMove: Int) {
        mainCalendar.add(Calendar.DAY_OF_MONTH, dayToMove)
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)
        val calendarToWeek = mainCalendar.clone() as GregorianCalendar
        calendarToWeek.add(Calendar.DAY_OF_WEEK, 7)
        calendarToWeek.add(Calendar.HOUR_OF_DAY, -1)
        val parsedDate = MdocUtil.getParsedDate("dd", mainCalendar) +
                ".${MdocUtil.getParsedDate("MM", mainCalendar)}" +
                ". - ${MdocUtil.getParsedDate("dd", calendarToWeek)}" +
                ".${MdocUtil.getParsedDate("MM", calendarToWeek)}."

        txt_selector_label.text = parsedDate
        txt_average_date.text = parsedDate

        getObservationStatistics(
                periodType = ObservationPeriodType.WEEK,
                request = generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarToWeek.timeInMillis,
                        periodType = ObservationPeriodType.DAY.name
                                         ))
    }

    private fun moveMonth(monthToMove: Int) {
        mainCalendar.add(Calendar.MONTH, monthToMove)
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)
        val calendarToMonth = mainCalendar.clone() as GregorianCalendar
        calendarToMonth.set(Calendar.DAY_OF_MONTH, calendarToMonth.getActualMaximum(Calendar.DAY_OF_MONTH))
        calendarToMonth.add(Calendar.HOUR_OF_DAY, 23)
        val parsedDate = MdocUtil.getParsedDate("MMMM", mainCalendar) + "  " +
                MdocUtil.getParsedDate("yyyy", mainCalendar)
        txt_selector_label.text = parsedDate
        txt_average_date.text = parsedDate

        getObservationStatistics(
                periodType = ObservationPeriodType.MONTH,
                request = generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarToMonth.timeInMillis,
                        periodType = ObservationPeriodType.DAY.name
                                         ))
    }

    private fun moveYear(yearToMove: Int) {
        mainCalendar.add(Calendar.YEAR, yearToMove)
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)
        val calendarToYear = mainCalendar.clone() as GregorianCalendar
        calendarToYear.set(Calendar.DAY_OF_YEAR, calendarToYear.getActualMaximum(Calendar.DAY_OF_YEAR))
        calendarToYear.add(Calendar.HOUR_OF_DAY, 23)
        val parsedDate = MdocUtil.getParsedDate("yyyy", mainCalendar)
        txt_selector_label.text = parsedDate
        txt_average_date.text = parsedDate

        getObservationStatistics(
                periodType = ObservationPeriodType.YEAR,
                request = generateRequest(
                        from = mainCalendar.timeInMillis,
                        to = calendarToYear.timeInMillis,
                        periodType = ObservationPeriodType.MONTH.name
                                         ))
    }

    private fun clickedTab(): TextView {
        when {
            txt_day_label.background != null   -> {
                return txt_day_label
            }

            txt_week_label.background != null  -> {
                return txt_week_label
            }

            txt_month_label.background != null -> {
                return txt_month_label
            }

            txt_year_label.background != null  -> {
                return txt_year_label
            }
        }
        return txt_day_label
    }

    private fun fillCodesForCompositeMeasure() {
        if (isCompositeByCode(metricCode[0])) {
            metricCode = codesForComposite(metricCode[0])
        }
    }

    private fun getObservationStatistics(periodType: ObservationPeriodType, request: ObservationStatisticsRequest) {
        graphTypeToSend = getGraphType(metricCodeConst)

        MdocManager.getObservationStatistics(request, object: Callback<ObservationResponse> {
            override fun onResponse(call: Call<ObservationResponse>, response: Response<ObservationResponse>) {
                if (response.isSuccessful) {
                    try {
                        val chartData = ChartData()

                        with(chartData) {
                            this.metrics = getMetricsForCharts(response.body()!!.data)
                            this.context = activity
                            this.rawData = response.body()
                            this.period = periodType
                            this.maxDaysInMonth = mainCalendar.getActualMaximum(Calendar.DAY_OF_MONTH)
                            this.unit = measurementUnit
                        }

                        createChart(graphTypeToSend, chartData)
                        fillChartText(chartData)
                    } catch (e: Exception) {
                        Timber.w("Exception %s", e.message)
                    }
                }
                else {
                    Timber.w("getObservationStatistics ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<ObservationResponse>, t: Throwable) {
                Timber.w(t, "getObservationStatistics")
            }
        })
    }

    private fun generateRequest(from: Long, to: Long, periodType: String): ObservationStatisticsRequest {
        val req =   ObservationStatisticsRequest().apply {
            this.from = from
            this.to = to
            this.metricsCode = metricCode
            this.periodType = periodType
            this.lastSeenUnitByCode = AppPersistence.lastSeenUnitByCode
        }
        request = req
        return req
    }

    fun getMetricsForCharts(data: ArrayList<Data>): ArrayList<String> {
        val codes = arrayListOf<String>()
        data.forEach {
            codes.add(it.metricCode)
        }
        return codes
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_vitals_chart)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItemExport) {
            request?.let {itr ->
                VitalsExportFragment().also {
                    it.request = itr
                }.show(parentFragmentManager, VitalsExportFragment::class.simpleName)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
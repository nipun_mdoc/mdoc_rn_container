package de.mdoc.modules.files.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseFileItemViewHolder (itemView: View): RecyclerView.ViewHolder(itemView)
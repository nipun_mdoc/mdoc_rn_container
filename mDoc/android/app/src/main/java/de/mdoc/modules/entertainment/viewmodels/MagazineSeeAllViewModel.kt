package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.ViewModel
import de.mdoc.modules.entertainment.repository.MagazinesRepository
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class MagazineSeeAllViewModel(private val category: String, private val repository: MagazinesRepository) : ViewModel() {

    private val _magazines = liveData<ResponseSearchResultsMediaLightDTO_?>()

    val magazines = _magazines

    fun reloadData() {
        Timber.d("Reload data")
        repository.getMagazinesByCategory(category, ::onResult)
    }

    private fun onResult(result: ResponseSearchResultsMediaLightDTO_?) {
        magazines.set(result)
    }
}
package de.mdoc.util.ChartFormatter;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

public class YearFormatter implements IAxisValueFormatter {

    @Override
    public String getFormattedValue(float value, AxisBase axis) {
        int resultInt = Math.round(value);

        String[] months = {"J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"};
        if (resultInt < months.length) {
            return months[resultInt];
        } else {
            return resultInt + "";
        }
    }
}
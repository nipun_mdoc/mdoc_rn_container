package de.mdoc.modules.profile.changepassword

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.google.gson.JsonObject
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.security.SystemServices
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.toggle
import kotlinx.android.synthetic.main.fragment_my_profile.*
import kotlinx.android.synthetic.main.password_change.*
import kotlinx.android.synthetic.main.password_change.progressBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val PASSWORD = "password"

class ChangePasswordDialogFragment : DialogFragment() {
    lateinit var activity: MdocActivity
    private lateinit var systemServices: SystemServices
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(ChangePasswordViewModel::class.java)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState).apply {
            window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        activity = getActivity() as MdocActivity
        systemServices = SystemServices(activity)
        return inflater.inflate(R.layout.password_change, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var passwordInfoText = String.format(resources.getString(R.string.info1), resources.getInteger(R.integer.password_length))
        passwordInfoText = "$passwordInfoText\n${resources.getString(R.string.info2)}\n${resources.getString(R.string.info3)}\n" +
                "${resources.getString(R.string.info4)}\n" +
                "${resources.getString(R.string.info5)}"
        passwordInfoTv.text = passwordInfoText

        viewModel.isShowPassword.observe(this) { isShow ->
            if (isShow) {
                showHideNewPassTv.text = getString(R.string.hide)
                newPasswordEdit.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                showHideNewPassTv.text = getString(R.string.show)
                newPasswordEdit.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            newPasswordEdit.setSelection(newPasswordEdit.text.toString().length)
        }

        viewModel.isShowPasswordConfirmation.observe(this) { isShow ->
            if (isShow) {
                showHideConfPassTv.text = getString(R.string.hide)
                confirmPassEdit.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                showHideConfPassTv.text = getString(R.string.show)
                confirmPassEdit.transformationMethod = PasswordTransformationMethod.getInstance()
            }
            confirmPassEdit.setSelection(confirmPassEdit.text.toString().length)
        }

        viewModel.isInProgress.observe(this) { inProgress ->
            progressBar.visibility = if (inProgress) View.VISIBLE else View.GONE
            val enabled = !inProgress
            newPasswordEdit.isEnabled = enabled
            confirmPassEdit.isEnabled = enabled
            showHideNewPassTv.isEnabled = enabled
            showHideConfPassTv.isEnabled = enabled
            btnCancelChange.isEnabled = enabled
            btnConfirmChange.isEnabled = enabled
        }

        showHideNewPassTv.setOnClickListener {
            viewModel.isShowPassword.toggle()
        }

        showHideConfPassTv.setOnClickListener {
            viewModel.isShowPasswordConfirmation.toggle()
        }

        btnCancelChange.setOnClickListener {
            dismiss()
        }

        btnConfirmChange.setOnClickListener {
            val newPass = newPasswordEdit.text.toString()
            val confirmPass = confirmPassEdit.text.toString()
            if (newPass == confirmPass) {
                if(isValidPassword(confirmPass)) {
                    changePassword(newPasswordEdit.text.toString())
                } else {
                    Toast.makeText(
                            context,
                            resources.getString(R.string.invalid_password),
                            Toast.LENGTH_LONG
                    ).show()
                }
            } else {
                Toast.makeText(
                    context,
                    resources.getString(R.string.password_mismatch),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)
        super.onResume()

    }

    private fun isValidPassword(password: String) : Boolean {
        if (password.length <= resources.getInteger(R.integer.password_length)) {
            return false
        } else{
            var lower = false
            var upper = false
            var numbers = false
            var special = false

            if(password.length > 0) {
                for (element in password) {
                    if (element.isDigit()) numbers = true
                    else if (element.isLowerCase()) lower = true
                    else if (element.isUpperCase()) upper = true
                    else special = true
                }
            }
            return  lower && upper && numbers && special
        }
    }
    private fun changePassword(newPassword: String) {
        viewModel.isInProgress.set(true)
        val body = JsonObject()
        body.addProperty(PASSWORD, newPassword)
        MdocManager.changePasswordProfile(body, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                viewModel.isInProgress.set(false)
                if (isAdded) {
                    if (response.isSuccessful) {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.password_change_success),
                            Toast.LENGTH_SHORT
                        ).show()
                        if (MdocAppHelper.getInstance()
                                .isOptIn){

                            SystemServices(activity).authenticateFingerprint(successAction = {
                                systemServices.optIn(newPassword)
                            }, cancelAction = {
                            }, context1 = getActivity() as Activity)

                        }
                        //check biometric enabled then change password in biometric
                    } else {
                        Toast.makeText(
                            context,
                            resources.getString(R.string.something_went_wrong),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    dismiss()
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                viewModel.isInProgress.set(false)
                Toast.makeText(
                    context,
                    "Error",
                    Toast.LENGTH_SHORT
                ).show()
                dismiss()
            }
        })
    }

}
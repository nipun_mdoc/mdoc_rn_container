package de.mdoc.modules.media.view

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import de.mdoc.R
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.view_media_image.view.*

class ImageMediaView(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_media_image, this)
    }

    fun load(url: String) {
        if (url.isNotEmpty()) {
            val userAuth = MdocAppHelper.getInstance().accessToken
            val glideUrl = GlideUrl(url, LazyHeaders.Builder().addHeader("Authorization", userAuth).build())

            Glide.with(this)
                    .load(glideUrl)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imageIv)
        }
    }
}
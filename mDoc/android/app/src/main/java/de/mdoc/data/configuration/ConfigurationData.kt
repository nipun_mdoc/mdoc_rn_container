package de.mdoc.data.configuration

data class ConfigurationData( val applicationEvent: String,
                              val configurationDetailsMultiValue: ConfigurationDetailsMultiValue,
                              val configurationType: String,
                              val id: String
)
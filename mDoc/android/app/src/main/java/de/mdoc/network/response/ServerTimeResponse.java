package de.mdoc.network.response;

public class ServerTimeResponse extends MdocResponse {

    private long data;

    public long getData() {
        return data;
    }

    public void setData(long data) {
        this.data = data;
    }
}

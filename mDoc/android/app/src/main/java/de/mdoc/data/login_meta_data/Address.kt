package de.mdoc.data.login_meta_data

data class Address(var houseNumber:String="",
                   var street:String="",
                   var city:String="",
                   var postalCode:String="",
                   var country:String="",
                   var countryCode:String="",
                   var state:String="",
                   var description:String="")
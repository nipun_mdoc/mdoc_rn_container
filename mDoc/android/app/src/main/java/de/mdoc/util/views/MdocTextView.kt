package de.mdoc.util.views

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.google.android.material.textview.MaterialTextView
import de.mdoc.R
import de.mdoc.util.ColorUtil


class MdocTextView : MaterialTextView {

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(attrs, defStyleAttr)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        init(attrs, defStyleAttr)
    }

    constructor(context: Context) : super(context) {
        init()
    }

    private fun init(attrs: AttributeSet? = null, defStyleAttr: Int = 0) {
        // Colors
        val contrastColor = ColorUtil.contrastColor(resources)
        val primaryColor = ContextCompat.getColor(context, R.color.colorPrimary)

        // Set text color
        this.setTextColor(contrastColor)

        // Check for attributes
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.RoundCropLayout, defStyleAttr, 0)
        val corners = attributes.getDimension(R.styleable.RoundCropLayout_corners, 0.0f)

        val shape = GradientDrawable()
        shape.cornerRadius = corners
        shape.setColor(primaryColor)

        // Set background
        background = shape
    }
}
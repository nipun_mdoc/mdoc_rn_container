package de.mdoc.modules.medications.create_plan.data.response

data class MedicationPlanResponse(
    val code: String? = "",
    val `data`: Data? = Data(),
    val message: String? = "",
    val timestamp: Long? = 0
) {
    data class Data(
        val authoredOn: String? = "",
        val cts: Long? = 0,
        val dispenseRequest: DispenseRequest? = DispenseRequest(),
        val dosageInstruction: List<DosageInstruction?>? = listOf(),
        val id: String? = "",
        val medicationId: String? = "",
        val requester: String? = "",
        val showReminder: Boolean? = false,
        val status: String? = "",
        val subject: String? = "",
        val uts: Long? = 0
    ) {
        data class DispenseRequest(
            val infinite: Boolean? = false
        )

        data class DosageInstruction(
            val doseQuantity: Double? = 0.0,
            val doseQuantityCode: String? = "",
            val patientInstruction: String? = "",
            val sequence: Int? = 0,
            val text: String? = "",
            val timing: Timing? = Timing()
        ) {
            data class Timing(
                val repeat: Repeat? = Repeat()
            ) {
                data class Repeat(
                    val dayOfWeek: List<Any?>? = listOf(),
                    val period: Int? = 0,
                    val periodUnit: String? = "",
                    val timeOfDay: List<TimeOfDay?>? = listOf()
                ) {
                    data class TimeOfDay(
                        val hour: Int? = 0,
                        val minute: Int? = 0
                    )
                }
            }
        }
    }
}
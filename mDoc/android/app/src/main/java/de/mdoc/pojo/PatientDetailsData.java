package de.mdoc.pojo;

public class PatientDetailsData {

    private String userId;
    private long uts;
    private long cts;
    private PublicUserDetails publicUserDetails;
    private ExtendedUserDetails extendedUserDetails;

    public PatientDetailsData() {
        publicUserDetails = new PublicUserDetails();
        extendedUserDetails = new ExtendedUserDetails();
    }

    public ExtendedUserDetails getExtendedUserDetails() {
        return extendedUserDetails;
    }

    public void setExtendedUserDetails(ExtendedUserDetails extendedUserDetails) {
        this.extendedUserDetails = extendedUserDetails;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getUts() {
        return uts;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    public long getCts() {
        return cts;
    }

    public void setCts(long cts) {
        this.cts = cts;
    }

    public PublicUserDetails getPublicUserDetails() {
        return publicUserDetails;
    }

    public void setPublicUserDetails(PublicUserDetails publicUserDetails) {
        this.publicUserDetails = publicUserDetails;
    }
}

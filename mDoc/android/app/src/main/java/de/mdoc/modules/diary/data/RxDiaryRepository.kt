package de.mdoc.modules.diary.data

import de.mdoc.constants.MdocConstants
import de.mdoc.data.diary.Data
import de.mdoc.data.diary.DiaryConfigResponse
import de.mdoc.network.response.DiaryResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.pojo.DiaryList
import de.mdoc.pojo.DiaryListUpdate
import de.mdoc.pojo.deleteDiaryPjo
import de.mdoc.service.IMdocService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


class RxDiaryRepository(private val mdocService: IMdocService) {
    private val disposables = CompositeDisposable()

    fun getAllDiaries(
        request: DiarySearchRequest?,
        resultListener: (List<DiaryList>) -> Unit,
        errorListener: (Throwable) -> Unit
    ) {

        mdocService.getAllDiaries(request).enqueue(object : Callback<DiaryResponse> {
            override fun onResponse(
                    call: Call<DiaryResponse>,
                    response: Response<DiaryResponse>
            ) {
                val result = response.body()
                if (result != null) {
                    parseData(result, resultListener, errorListener)
                } else {
                    Timber.w("getAllDiaries ${response.getErrorDetails()}")
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<DiaryResponse>, t: Throwable) {
                Timber.w(t, "getAllDiaries")
                errorListener.invoke(t)
            }
        })
    }

    fun getDiariesUpdateRequest(
            id: String,request: DiaryListUpdate,
            resultListener: (List<deleteDiaryPjo>) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {

        mdocService.updateDiary(id,request).enqueue(object : Callback<deleteDiaryPjo> {
            override fun onResponse(
                    call: Call<deleteDiaryPjo>,
                    response: Response<deleteDiaryPjo>
            ) {
                val result = response.body()
                if (result != null) {
                    parseUpdateDeleteData(result, resultListener, errorListener)
                } else {
                    Timber.w("getAllDiaries ${response.getErrorDetails()}")
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<deleteDiaryPjo>, t: Throwable) {
                Timber.w(t, "getAllDiaries")
                errorListener.invoke(t)
            }
        })
    }

    fun getDiariesDeleteRequest(
            id: String,
            resultListener: (List<deleteDiaryPjo>) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {

        mdocService.deleteDiary(id).enqueue(object : Callback<deleteDiaryPjo> {
            override fun onResponse(
                    call: Call<deleteDiaryPjo>,
                    response: Response<deleteDiaryPjo>
            ) {
                val result = response.body()
                if (result != null) {
                    parseUpdateDeleteData(result, resultListener, errorListener)
                } else {
                    Timber.w("getAllDiaries ${response.getErrorDetails()}")
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<deleteDiaryPjo>, t: Throwable) {
                Timber.w(t, "getAllDiaries")
                errorListener.invoke(t)
            }
        })
    }

    private fun parseData(
            response: DiaryResponse,
            resultListener: (List<DiaryList>) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {
        Single.just(response).map(::mapResponse)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resultListener, errorListener)
                .addTo(disposables)
    }

    private fun parseUpdateDeleteData(
            response: deleteDiaryPjo,
            resultListener: (List<deleteDiaryPjo>) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {
        Single.just(response).map(::mapDeletResponse)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resultListener, errorListener)
                .addTo(disposables)
    }


    private fun mapDeletResponse(response: deleteDiaryPjo): List<deleteDiaryPjo> {
        val filteredList = mutableListOf<deleteDiaryPjo>()
        return filteredList
    }

    private fun mapResponse(response: DiaryResponse): List<DiaryList> {
        var month = 0
        val diaryList = response.data.list.sortedByDescending { it.start }
        val filteredList = mutableListOf<DiaryList>()

        diaryList.forEach{
            if(month != it.monthValue) {
                filteredList.add(DiaryList(MdocConstants.DIARY_HEADER, it.monthValue))
            }
            //override empty title with code from coding
            if (it.diaryConfig?.title == null) {
                it.diaryConfig?.title = it.diaryConfig?.coding?.display
            }

            filteredList.add(it)
            month = it.monthValue
        }
        if (diaryList.isNotEmpty()) {
            filteredList.add(DiaryList(MdocConstants.DIARY_FOOTER, 0))
        }

        return filteredList
    }

    fun diaryConfiguration(
        resultListener: (List<Data>) -> Unit,
        errorListener: (Throwable) -> Unit
    ) {
        mdocService.diaryConfig.enqueue(object : Callback<DiaryConfigResponse> {
            override fun onResponse(
                call: Call<DiaryConfigResponse>,
                response: Response<DiaryConfigResponse>
            ) {
                val result = response.body()
                if (result != null) {
                    parseConfigData(result, resultListener, errorListener)
                } else {
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<DiaryConfigResponse>, t: Throwable) {
            }
        })
    }


    private fun parseConfigData(
        response: DiaryConfigResponse,
        resultListener: (List<Data>) -> Unit,
        errorListener: (Throwable) -> Unit
    ) {
        val obs: Disposable = Single.just(response).map(::mapDiaryResponse)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(resultListener, errorListener)

        obs.addTo(disposables)
    }

    private fun mapDiaryResponse(response: DiaryConfigResponse): List<Data> {

        return response.data
    }

    fun recycle() {
        disposables.dispose()
    }

}

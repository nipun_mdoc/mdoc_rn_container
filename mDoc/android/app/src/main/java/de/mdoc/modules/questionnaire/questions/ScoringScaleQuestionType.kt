package de.mdoc.modules.questionnaire.questions

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.scoring_scale_layout.view.*

internal class ScoringScaleQuestionType : QuestionType() {
    var start = 0;
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.scoring_scale_layout, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value=false

        view.beginningScaleTv?.text = question.scaleBeginningText
        view.endScaleTv?.text = question.scaleEndText

        start = ((question.questionData.scoringScaleMin * 1) - 1).toInt()
        val end = question.questionData.scoringScaleMax * 1
        val label = view.label
        val seekbar = view.seekBar

        seekbar.max = end.toInt()
        seekbar.min = start.toInt()
        val color = ContextCompat.getColor(view.context, R.color.colorPrimary)
        seekbar.progressDrawable.colorFilter =
                PorterDuffColorFilter(ContextCompat.getColor(view.context, R.color.colorPrimary),
                        PorterDuff.Mode.SRC_IN)


        seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                seekbar.thumb = getThumb(progress,label)
                answers.clear()
                answers.add(progress.toString())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {}

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        // Fill answer if exists
        if (question.answer != null) {
            seekbar.post {
                val progress = (question.answer.answerData.toFloatOrNull() ?: 0f) * 1
                seekbar.progress = (progress).toInt()
                seekbar.thumb = getThumb(progress.toInt(), label)
            }

            answers.add(question.answer.answerData)
        }else{
            seekbar.thumb = getThumb(start, label)
        }

        mandatoryCheck(mandatory, null, question)
        return view
    }

    fun getThumb(progress: Int, label: TextView): Drawable? {
        if(progress == start) {
            label.text = "Select"
            questionIsAnswered.value = false
        }
        else {
            label.text = progress.toString() + ""
            questionIsAnswered.value = true
            isNewAnswer = true
        }

        label.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap = Bitmap.createBitmap(label.measuredWidth, label.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        label.layout(0, 0, label.measuredWidth, label.measuredHeight)
        label.draw(canvas)
        return BitmapDrawable(bitmap)
    }

    override fun onSaveQuestion(questionView: View, question: ExtendedQuestion) {
        super.onSaveQuestion(questionView, question)
        isNewAnswer = true
    }

}
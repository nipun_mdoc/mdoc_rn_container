package de.mdoc.modules.booking.clinic_selection

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentClinicSelectionBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel


class ClinicSelectionFragment: NewBaseFragment() {


    override val navigationItem: NavigationItem = NavigationItem.BookingClinicSelection

    private val clinicSelectionViewModel by viewModel {
        ClinicSelectionViewModel(
                RestClient.getService(), context)
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentClinicSelectionBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_clinic_selection, container, false)

        binding.lifecycleOwner = this
        binding.handler = ClinicSelectionHandler()
        binding.clinicSelectionViewModel = clinicSelectionViewModel

        return binding.root
    }

}

package de.mdoc.util

import android.content.Context
import androidx.annotation.StringRes
import de.mdoc.R

enum class DaysInWeek (
        val position:Int,
        val jsonName: String,
        @StringRes val nameResId: Int
                     ) {
    MONDAY(1,"MONDAY", R.string.monday),
    TUESDAY(2,"TUESDAY", R.string.tuesday),
    WEDNESDAY(3,"WEDNESDAY", R.string.wednesday),
    THURSDAY(4,"THURSDAY", R.string.thursday),
    FRIDAY(5,"FRIDAY", R.string.friday),
    SATURDAY(6,"SATURDAY", R.string.saturday),
    SUNDAY(7,"SUNDAY", R.string.sunday);

    fun getName(context: Context?): String? {
        return context?.getString(nameResId)
    }

}
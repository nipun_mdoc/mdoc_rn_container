package de.mdoc.activities.login

import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import de.mdoc.R
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.util.MdocAppHelper
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class DateOfBirthDialog {

    private val calendar = Calendar.getInstance()
    private val year = calendar.get(Calendar.YEAR)
    private val month = calendar.get(Calendar.MONTH)
    private val day = calendar.get(Calendar.DAY_OF_MONTH)
    private val dateFormat = "dd.MM.yyyy"
    private val sdf = SimpleDateFormat(dateFormat, Locale.US)

    fun showDateOfBirthDialog(activity: Activity, userId: String){

        val builder = AlertDialog.Builder(activity)
        val inflater = LayoutInflater.from(activity)
        val view = inflater.inflate(R.layout.layout_date_of_birth, null)
        builder.setView(view)
        val dialog = builder.show()
        dialog.setCancelable(false)
        val send = view.findViewById<View>(R.id.btn_send) as Button
        val  date = view.findViewById<View>(R.id.dobTv) as TextView

        val datePick = DatePickerDialog(activity, { _, year, monthOfYear, dayOfMonth ->

            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            date.text = sdf.format(calendar.time)
        }, year, month, day)

        datePick.datePicker.maxDate = System.currentTimeMillis()

        send.setOnClickListener {
            if (!date.text.contains("_")) {
                setUserDate(userId, calendar.timeInMillis.toString(), dialog)
                dialog.dismiss()
            }
        }

        date.setOnClickListener {
            datePick.show()
        }
    }

    private fun setUserDate(userId: String, userDate: String, dialog: AlertDialog){
        MdocManager.setUserBirthdate(userId, userDate, object : Callback<MdocResponse>{
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful){
                    MdocAppHelper.getInstance().publicUserDetailses.born = calendar.timeInMillis
                    dialog.dismiss()
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                Timber.d(t)
            }
        })
    }
}
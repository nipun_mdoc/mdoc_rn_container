package de.mdoc.modules.booking.specialities;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Locale;

import de.mdoc.R;
import de.mdoc.modules.booking.data.BookingAdditionalFields;
import de.mdoc.network.response.SpecialtyServices;

public class SpecialtyAdapter extends RecyclerView.Adapter<SpecialtyAdapter.SpecialtyViewHolder> implements Filterable {

    private ArrayList<SpecialtyServices> specialtyServices;
    private ArrayList<SpecialtyServices> filteredSpecialtyServices;
    private ArrayList<String> specialityNames;

    private String specialtyTitle;
    private BookingAdditionalFields bookingAdditionalFields;

    public SpecialtyAdapter(ArrayList<SpecialtyServices> specialtyServices, String title, ArrayList<String> specialityNames, BookingAdditionalFields bookingAdditionalFields) {
        this.specialtyServices = specialtyServices;
        this.filteredSpecialtyServices = specialtyServices;
        this.specialtyTitle = title;
        this.specialityNames = specialityNames;
        this.bookingAdditionalFields = bookingAdditionalFields;
    }

    @Override
    public SpecialtyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_speciality, parent, false);

        return new SpecialtyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SpecialtyViewHolder holder, int position) {
        final SpecialtyServices item = filteredSpecialtyServices.get(position);

        holder.setupUi(item);

        holder.specialityLv.setOnClickListener(v -> {

            bookingAdditionalFields.setAppointmentTypeDisplay(item.getTitle());
            bookingAdditionalFields.setAppointmentType(item.getServiceIds().get(0).getServiceId());
            bookingAdditionalFields.setDepartmentDescription(item.getDescription());
            bookingAdditionalFields.setSpecialityName(item.getServiceId(bookingAdditionalFields.getSpecialtyProvider()));
            bookingAdditionalFields.setSpecialtyDisplay(item.getServiceIds().get(0).getServiceId());
            bookingAdditionalFields.setSpecialtyCode(bookingAdditionalFields.getSpecialtyCode());

            NavDirections directions = SpecialitiesFragmentDirections.Companion.actionSpecialitiesFragmentToBookAppointmentFragment(bookingAdditionalFields);
            Navigation.findNavController(v).navigate(directions);
        });
    }

    @Override
    public int getItemCount() {
        return filteredSpecialtyServices.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filteredSpecialtyServices = specialtyServices;
                } else {
                    ArrayList<SpecialtyServices> filteredList = new ArrayList<>();
                    for (SpecialtyServices row : specialtyServices) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (row.getTitle().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))) {
                            filteredList.add(row);
                        }
                    }

                    filteredSpecialtyServices = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filteredSpecialtyServices;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filteredSpecialtyServices = (ArrayList<SpecialtyServices>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }

    class SpecialtyViewHolder extends RecyclerView.ViewHolder {
        private TextView specialtyTitleTv, specialityDescTv;
        public LinearLayout specialityLv;

        public SpecialtyViewHolder(View itemView) {
            super(itemView);
            specialtyTitleTv = itemView.findViewById(R.id.specialityTitleTv);
            specialityDescTv = itemView.findViewById(R.id.specialtyDescriptionTv);
            specialityLv = itemView.findViewById(R.id.specialityLv);
        }

        public void setupUi(SpecialtyServices specialtyServices) {
            specialtyTitleTv.setText(specialtyServices.getTitle());
            specialityDescTv.setText(specialtyServices.getDescription());
        }
    }
}

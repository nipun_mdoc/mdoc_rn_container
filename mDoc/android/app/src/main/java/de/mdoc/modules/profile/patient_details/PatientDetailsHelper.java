package de.mdoc.modules.profile.patient_details;

import android.content.Context;

import java.util.ArrayList;

import de.mdoc.network.response.CodingResponse;
import de.mdoc.network.response.CoverageResponse;
import de.mdoc.pojo.Coverage;
import de.mdoc.pojo.PatientDetailsData;

public class PatientDetailsHelper  {

    private static PatientDetailsHelper instance;
    private Context context;
    private PatientDetailsData patientDetailsData;
    private PatientDetailsData familyDoctorData;
    private ArrayList<Coverage> coverages;
    private CoverageResponse coveragesResponse;
    public CodingResponse genderCodingResponse;
    public CodingResponse nationalityCodingResponse;
    public CodingResponse languageCodingResponse;
    public CodingResponse civilStatusCodingResponse;
    public CodingResponse religionCodingResponse;
    public CodingResponse relationshipCodingResponse;

    public static PatientDetailsHelper getInstance(){
        if(instance == null){
            instance = new PatientDetailsHelper();
            instance.familyDoctorData = new PatientDetailsData();
            instance.patientDetailsData = new PatientDetailsData();
        }
        return instance;
    }

    public void init(Context cont){
        this.context = cont;
    }

    public PatientDetailsData getPatientDetailsData() {
        return patientDetailsData;
    }

    public void setPatientDetailsData(PatientDetailsData patientDetailsData) {
        this.patientDetailsData = patientDetailsData;
    }

    public PatientDetailsData getFamilyDoctorData() {
        return familyDoctorData;
    }

    public void setFamilyDoctorData(PatientDetailsData familyDoctorData) {
        this.familyDoctorData = familyDoctorData;
    }

    public ArrayList<Coverage> getCoverages() {
        return coverages;
    }

    public void setCoverages(ArrayList<Coverage> coverages) {
        this.coverages = coverages;
    }

    public CoverageResponse getCoveragesResponse() {
        return coveragesResponse;
    }

    public void setCoveragesResponse(CoverageResponse coveragesResponse) {
        this.coveragesResponse = coveragesResponse;
    }
}

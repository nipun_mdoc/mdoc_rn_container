package de.mdoc.modules.diary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.data.diary.Data
import kotlinx.android.synthetic.main.fragment_dialog_media.*

class DiaryBottomSheetDialogFragment : BottomSheetDialogFragment(){

    var category: ArrayList<Data> = arrayListOf()
    private lateinit var categoryAdapter: DiaryCategoryAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_dialog_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        category = arguments?.getSerializable(MdocConstants.CATEGORY_LIST) as ArrayList<Data>

        txt_title?.text = getString(R.string.please_select_category)
        categoryAdapter= DiaryCategoryAdapter(activity as MdocActivity, category, this)
        rv_category?.layoutManager = LinearLayoutManager(activity)
        rv_category?.adapter = categoryAdapter
        img_close?.setOnClickListener {
            dismiss()
        }

    }

    companion object {
        fun newInstance(categoryList: List<Data>?): DiaryBottomSheetDialogFragment {
            val args = Bundle().also {
                it.putSerializable(MdocConstants.CATEGORY_LIST, ArrayList(categoryList!!))
            }
            val fragment = DiaryBottomSheetDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }


}
package de.mdoc.activities.login

import android.graphics.Bitmap
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.MdocApp
import de.mdoc.R
import de.mdoc.activities.login.LoginUtil.Companion.MDOCAPP_URI
import de.mdoc.constants.MdocConstants
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.ServerTimeResponse
import de.mdoc.security.SystemServices
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.token_otp.Token
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginWebViewClient(private val callback: LoginWebCallback, private val refreshTokenCode: Int): WebViewClient() {

    private var isPasswordUpdate = false

    interface LoginWebCallback {

        fun executeLogin(url: String)

        fun setProgress(isInProgress: Boolean)

        fun onFailedToLoad()

        fun onPageLoaded(url: String?)

        fun reloadInitialPage()

        fun onMissingSecret()

    }

    override fun shouldInterceptRequest(view: WebView?, request: WebResourceRequest?): WebResourceResponse? {
        if (shallGetPasswordFromWebView(request?.url.toString(), view)) {
            (view!!.context as AppCompatActivity).runOnUiThread {
                getPasswordFromWeb(view)
            }
        }
        return super.shouldInterceptRequest(view, request)
    }

    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        if (url.contains("UPDATE_PASSWORD")) {
            isPasswordUpdate = true
        }

        if (url.contains("code=")) {
            if (url.startsWith(MDOCAPP_URI)) {
                callback.executeLogin(url)
                shallGetPasswordFromWebView(view)
            }
            else {
                callback.reloadInitialPage()
            }
            return true
        }


        view.loadUrl(url)

        return true
    }

    private fun shallGetPasswordFromWebView(view: WebView) {
        if (isPasswordUpdate) {
            MdocConstants.IS_UPDAE_PASSWORD = true
            getNewPasswordFromWeb(view)
        }
    }

    override fun onPageStarted(view: WebView, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        callback.setProgress(true)
        inject(view)
    }

    override fun onPageFinished(view: WebView, url: String?) {
        super.onPageFinished(view, url)
        callback.setProgress(false)
        if(reloadCOunter==6) {
            callback.onMissingSecret()
            return
        }
        // in very very rare situations scripts are not processed from onPageStarted()
        // so we hammer it again for sure :)
        callback.onPageLoaded(url)

        view.visibility = View.VISIBLE
        inject(view)

        if (url?.contains("session_state") == true) {
            view.visibility = View.INVISIBLE
        }
        else {
            view.visibility = View.VISIBLE
        }
        //This will stop infinite loading when user tries to loging without otp with elevated user
        val hasOtpForm = "document.getElementById('kc-totp-login-form')"
        view.evaluateJavascript(hasOtpForm) { value ->
            if (value == "{}" && MdocAppHelper.getInstance().secretKey.isNullOrEmpty() && url != null && url.contains(
                            "login-actions")) {
                MdocConstants.KEY_MISSING = true;
                injectOtp(System.currentTimeMillis(), view)
            }
        }
        /*
            The reason, why we are checking for refreshTokenCode == 200,
            is to prevent infinite loop in case when refresh_token gets expired.

            Ticket: https://mdocplatform.atlassian.net/browse/MDPD-8507
         */

        if (MdocAppHelper.getInstance().secretKey != null && refreshTokenCode == 200) {
            MdocManager.getServerTime(object: Callback<ServerTimeResponse> {
                override fun onResponse(
                        call: Call<ServerTimeResponse>,
                        response: Response<ServerTimeResponse>
                ) {
                    if (response.isSuccessful) {
                        injectOtp(response.body()!!.data, view)
                    }
                    else {
                        injectOtp(System.currentTimeMillis(), view)
                    }
                }

                override fun onFailure(call: Call<ServerTimeResponse>, t: Throwable) {
                    injectOtp(System.currentTimeMillis(), view)
                }
            })
        }
        else if (MdocAppHelper.getInstance().secretKey != null) {
            injectOtp(System.currentTimeMillis(), view)
        }

    }

    private fun inject(webView: WebView) {
        injectUsernameIfEmpty(webView)
    }

    private fun injectUsernameIfEmpty(webView: WebView) {
        val field = "document.getElementById('username').value"
        webView.evaluateJavascript(field) { value ->
            if (value == "\"\"") {
                injectUserName(webView)
            }
        }
    }


    private fun injectUserName(webView: WebView) {
        if (MdocAppHelper.getInstance().username != null) {
            val script = "document.getElementById('username').value='" + MdocAppHelper.getInstance().username + "'"
            webView.evaluateJavascript(script) { value -> println(value) }
        }
    }

    var reloadCOunter = 0;
    private fun injectOtp(time: Long, view: WebView) {
        var otp: String? = ""
        if (MdocAppHelper.getInstance().secretKey != null) {
            try {
                val token =
                        Token(MdocAppHelper.getInstance().secretKey)
                val code = token.generateCodes(time)
                if (code == null)
                    Toast.makeText(view.context, view.resources.getString(R.string.err_time_token),
                            Toast.LENGTH_LONG)
                            .show()
                else
                    otp = code.getCurrentCode(time)
            } catch (e: Token.TokenUriInvalidException) {
                e.printStackTrace()
            }
        }
        var script = "" ;
        script = if(MdocAppHelper.getInstance().secretKey == null && MdocConstants.KEY_MISSING)
            "document.getElementById('key_missing').value=1"
        else
            "document.getElementById('totp').value='$otp'"
        view.evaluateJavascript(script) { value -> println(value) }
        val deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL
        val instanceId = FirebaseInstanceId.getInstance()
                .id
        val deviceFriendly = "$deviceName # $instanceId"
        val scriptFriendlyName = "document.getElementById('friendly_name').value='$deviceFriendly'"
        view.evaluateJavascript(scriptFriendlyName) { value -> println(value) }
        val scriptSubmit = "document.getElementById('kc-otp-login').click()"
        view.evaluateJavascript(scriptSubmit) { value ->

            reloadCOunter++
            if(reloadCOunter>=6){
                callback.reloadInitialPage()
            }
        }
    }

    private fun getPasswordFromWeb(view: WebView) {
        val scriptGetPassword = "document.getElementById('password').value"
        view.evaluateJavascript(scriptGetPassword) { value ->
            if (value != null && value.length > 2 && value != "null") {
                println(value)
                MdocAppHelper.getInstance()
                        .userPassword = value.replace("\"", "")
            }
        }
    }

    private fun getNewPasswordFromWeb(view: WebView) {
        val scriptGetNewPasswordName = "document.getElementById('password-new').value"
        view.evaluateJavascript(scriptGetNewPasswordName) { value ->
            println(value)
            if (value != null && value.length > 2 && value != "null")
                MdocAppHelper.getInstance()
                        .userPassword = value.replace("\"", "")
            MdocConstants.NEW_PASSWORD = value.replace("\"", "")
        }
    }

    fun injectPassword(webView: WebView) {
        if (MdocAppHelper.getInstance().userPassword != null) {
            webView.visibility = View.INVISIBLE
            val script = "document.getElementById('password').value='${MdocAppHelper.getInstance().userPassword}'"
            webView.evaluateJavascript(script) { value -> println(value) }
            overrideLogin(webView)
        }
        else {
            MdocAppHelper.getInstance()
                    .isOptIn = false
        }
    }

    private fun overrideLogin(webView: WebView) {
        val passwordField = "document.getElementById('password').value"
        webView.evaluateJavascript(passwordField) { value ->
            if (value != "\"\"") {
                automaticLogin(webView)
            }
        }
    }

    private fun automaticLogin(webView: WebView) {
        val scriptLogin = "document.getElementById('kc-login').click()"
        webView.evaluateJavascript(scriptLogin) { value ->
            println(value)
        }
    }

    private fun shallGetPasswordFromWebView(url: String, view: WebView?): Boolean {
        return ((url.contains("login-actions/authenticate")) || url.contains("login-actions/registration"))
                && view != null && !MdocAppHelper.getInstance().isBiometricPrompted
    }

    override fun onReceivedError(
            view: WebView,
            request: WebResourceRequest?,
            error: WebResourceError?
    ) {
        super.onReceivedError(view, request, error)
        callback.setProgress(false)
        callback.onFailedToLoad()
    }

}
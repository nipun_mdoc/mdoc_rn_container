package de.mdoc.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import de.mdoc.pojo.NewAppointmentSchedule;

public class ReschedulingResponse extends MdocResponse {

    @SerializedName("data")
    @Expose
    private NewAppointmentSchedule data;

    public NewAppointmentSchedule getData() {
        return data;
    }

    public void setData(NewAppointmentSchedule data) {
        this.data = data;
    }
}

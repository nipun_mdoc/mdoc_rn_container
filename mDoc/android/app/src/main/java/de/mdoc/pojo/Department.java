package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/20/17.
 */

public class Department implements Serializable {

    private String name;
    private String description;
    private HeadOfClinic headOfDepartment;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public HeadOfClinic getHeadOfDepartment() {
        return headOfDepartment;
    }

    public void setHeadOfDepartment(HeadOfClinic headOfDepartment) {
        this.headOfDepartment = headOfDepartment;
    }
}

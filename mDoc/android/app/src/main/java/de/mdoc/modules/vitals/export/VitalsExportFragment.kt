package de.mdoc.modules.vitals.export

import android.os.Bundle
import android.view.View
import de.mdoc.data.requests.ObservationStatisticsRequest
import de.mdoc.modules.common.export.BaseExportFragment
import de.mdoc.network.RestClient
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_goal_export_options.*

class VitalsExportFragment : BaseExportFragment() {

    private val viewModel by viewModel {
        VitalsExportViewModel(RestClient.getService(), this)
    }

    lateinit var request: ObservationStatisticsRequest

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindVisibleGone(downloadExportProgress, viewModel.isLoading)
    }

    override fun downloadPdf() {
        viewModel.download(request, ExportType.PDF, context)
    }

    override fun downloadXml() {
       viewModel.download(request, ExportType.XLSX, context)
    }
}
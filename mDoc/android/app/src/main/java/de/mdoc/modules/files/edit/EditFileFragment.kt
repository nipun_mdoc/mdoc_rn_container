package de.mdoc.modules.files.edit

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.FileUpdate
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.pojo.CodingItem
import de.mdoc.util.MdocUtil
import de.mdoc.util.getErrorMessage
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.edit_file_fragment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class EditFileFragment : MdocFragment() {

    private val args: EditFileFragmentArgs by navArgs()

    private val viewModel by viewModel {
        EditFileViewModel(
                FilesRepository(
                        RestClient.getService(),
                        FileCacheRepository
                )
        )
    }

    private var categoryCode = ""

    override fun setResourceId(): Int {
        return R.layout.edit_file_fragment
    }

    override fun init(savedInstanceState: Bundle?) {
    }

    override val navigationItem: NavigationItem = NavigationItem.Files

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        titleEdt2.setText(args.file?.description)

        viewModel.categories.observe(this) { list ->
            categorySpn2.adapter = SpinnerAdapter(context!!, list)

            val index = list.indexOfFirst {
                it.code == args.file?.category
            }
            if (index >= 0) {
                categorySpn2.setSelection(index)
            }
        }

        categorySpn2.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                    parent: AdapterView<*>, view: View?, position: Int, id: Long
            ) {
                categoryCode = (parent.adapter?.getItem(position) as? CodingItem)?.code ?: ""
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // unused
            }
        }

        saveBtn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                updateFile(FileUpdate(args.file))
            }
        })

        cancelBtn2.setOnClickListener(object : View.OnClickListener {
            override fun onClick(p0: View?) {
                findNavController().popBackStack()
            }
        })
    }

    fun updateFile(request: FileUpdate) {
        if (titleEdt2.text.toString().isEmpty()) {
            request.description = args.file?.name
        } else {
            request.description = titleEdt2.text.toString()
        }

        request.category = categoryCode

        MdocManager.updateFile(request, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    findNavController().popBackStack()
                } else {
                    Timber.w("updateFile ${response.getErrorDetails()}")
                    MdocUtil.showToastShort(context, response.getErrorMessage())
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                Timber.w(t, "updateFile")
                MdocUtil.showToastShort(context, "Error " + t.message)
            }
        })
    }
}
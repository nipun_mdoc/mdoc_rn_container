package de.mdoc.modules.questionnaire.widget

import androidx.lifecycle.ViewModel
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class QuestionnaireWidgetViewModel(
    private val repository: QuestionnaireWidgetRepository
) : ViewModel() {

    /** Is show progress bar that data is loading now */
    val isLoading = liveData<Boolean>(false)
    /** Is show loading error */
    val isShowLoadingError = liveData<Boolean>(false)
    /** List of section to be shown */
    val content = liveData<List<SectionData>>()
    /** Is content of sections is visible */
    val isShowContent = liveData<Boolean>(false)
    /** Is show message that there is no assigned questionnaires */
    val isShowNoQuestionnairesMessage = liveData<Boolean>(false)

    fun loadData() {
        isLoading.set(true)
        repository.getData(::onDataLoaded, ::onError)
    }

    private fun onDataLoaded(result: List<SectionData>) {
        isLoading.set(false)
        if (result.isNotEmpty()) {
            content.set(result)
            isShowContent.set(true)
        } else {
            isShowNoQuestionnairesMessage.set(true)
        }
    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        isLoading.set(false)
        isShowLoadingError.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }

}
package de.mdoc.pojo;

import java.util.ArrayList;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by AdisMulabdic on 1/16/18.
 */

public class ChildResponse extends MdocResponse {
    private ArrayList<Child> data;

    public ArrayList<Child> getData() {
        return data;
    }

    public void setData(ArrayList<Child> data) {
        this.data = data;
    }
}

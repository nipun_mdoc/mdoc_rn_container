package de.mdoc.util.views

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.google.android.material.floatingactionbutton.FloatingActionButton
import de.mdoc.R
import de.mdoc.util.ColorUtil

class MdocFloatingButton(context: Context, attrs: AttributeSet) : FloatingActionButton(context, attrs) {

    init {
        // Colors
        val contrastColor = ColorUtil.contrastColor(resources)
        val primaryColor = ContextCompat.getColor(context, R.color.colorPrimary)

        // Background color
        backgroundTintList = ColorStateList.valueOf(primaryColor)

        // Tint
        drawable.mutate().setTint(contrastColor)
    }
}
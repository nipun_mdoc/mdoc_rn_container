package de.mdoc.modules.diary

import android.graphics.BitmapFactory
import android.graphics.Color.parseColor
import android.graphics.drawable.GradientDrawable
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.diary.Data
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import kotlinx.android.synthetic.main.placeholder_diary_categories.view.*
import java.util.*

class DiaryCategoryAdapter(var context: MdocActivity,
                           var category: ArrayList<Data>,
                           var dialogBottomDialogFragment: DiaryBottomSheetDialogFragment?):
        RecyclerView.Adapter<DiaryCategoriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DiaryCategoriesViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_diary_categories, parent, false)
        return DiaryCategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: DiaryCategoriesViewHolder, position: Int) {
        val activity = context as MdocActivity
        val categoryItem = category[position]
        val title = if (
            categoryItem.title == null) {
            categoryItem.coding.display
        }
        else {
            categoryItem.title
        }
        //        holder.categoryTitle.text = title
        holder.categoryTitle.text = title

        categoryItem.coding.imageBase64?.let {
            var base64Image = "";

            base64Image = if (it.split(",").size > 1) {
                it.split(",")[1]
            }
            else {
                it.split(",")[0]
            }
            val decodedString = Base64.decode(base64Image, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            holder.categoryIcon.setImageBitmap(decodedByte)

            categoryItem.coding.color?.let {
                val strokeWidth = 5
                val strokeColor = parseColor(it)
                val fillColor = parseColor("#ffffff")
                val gD = GradientDrawable()
                gD.setColor(fillColor)
                gD.shape = GradientDrawable.OVAL
                gD.setStroke(strokeWidth, strokeColor)
                holder.categoryIcon.background = gD
            }
        }

        holder.containerLl.setOnClickListener {
            dialogBottomDialogFragment?.dismiss()
            val questionnaireAdditionalFields =
                    QuestionnaireAdditionalFields(
                            showFreeText = categoryItem.freeTextEntryAllowed,
                            allowSharing = categoryItem.shareAllowed,
                            autoShare = categoryItem.autoShare,
                            isDiary = true,
                            shouldAssignQuestionnaire = true,
                            diaryConfigId = categoryItem.id,
                            pollId = categoryItem.pollId)
            val action = if (categoryItem.pollId != null) {
                MainNavDirections.globalActionToQuestion(
                        questionnaireAdditionalFields)
            }
            else {
                DiaryDashboardFragmentDirections.actionDiaryDashboardFragmentToDiaryEntryFragment(
                        questionnaireAdditionalFields)
            }
            findNavController(activity, R.id.navigation_host_fragment).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }
}

class DiaryCategoriesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val categoryTitle: TextView = itemView.categoryTitle
    val categoryIcon: ImageView = itemView.categoryIcon
    val containerLl: LinearLayout = itemView.containerLl
}

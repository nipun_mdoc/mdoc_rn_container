package de.mdoc.modules.booking.confirm_booking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentBookingFailedBinding
import de.mdoc.fragments.NewBaseFragment
import kotlinx.android.synthetic.main.fragment_booking_failed.*

class BookingFailedFragment : NewBaseFragment() {

    private val args: BookingFailedFragmentArgs by navArgs()

    override val navigationItem: NavigationItem = NavigationItem.BookingFailed

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentBookingFailedBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_booking_failed, container, false)

        binding.failMessage = getFailMessage(args.failCode)
        binding.failTitle = getFailTitle(args.failCode)
        return binding.root
    }

    private fun getFailMessage(failMessage: String): String {
        return if (failMessage == NO_SLOTS_MSG) {
            resources.getString(R.string.booking_not_available)
        } else {
            resources.getString(R.string.booking_error_message)
        }
    }

    private fun getFailTitle(failMessage: String): String {
        return if (failMessage == NO_SLOTS_MSG) {
            resources.getString(R.string.booking_request_failed)
        } else {
            resources.getString(R.string.booking_error_title)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {

            override fun handleOnBackPressed() {
                val isBackToMasterData = findNavController().popBackStack(R.id.fragmentMasterData,false)

                if (!isBackToMasterData) {
                    val isBackToConfirmBooking = findNavController().popBackStack(R.id.confirmBookingFragment,false)

                    if (!isBackToConfirmBooking) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        txt_try_again?.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    companion object {
        const val NO_SLOTS_MSG = "REQUESTEDSLOTNOTAVAILABLE"
    }
}
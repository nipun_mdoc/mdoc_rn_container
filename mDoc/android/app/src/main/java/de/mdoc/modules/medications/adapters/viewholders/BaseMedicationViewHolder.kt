package de.mdoc.modules.medications.adapters.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.placeholder_medication_dashboard.view.*

abstract class BaseMedicationViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    protected val txtTime: TextView = itemView.txt_time
    private val timeHeader: TextView = itemView.txt_current_time_shown

    init {
        timeHeader.visibility = View.GONE
    }
}
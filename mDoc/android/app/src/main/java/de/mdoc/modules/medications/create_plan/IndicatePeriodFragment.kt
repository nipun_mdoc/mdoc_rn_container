package de.mdoc.modules.medications.create_plan

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentIndicatePeriodBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import kotlinx.android.synthetic.main.fragment_indicate_period.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class IndicatePeriodFragment internal constructor(val viewModel: CreatePlanViewModel):
        NewBaseFragment() {

    lateinit var activity: MdocActivity
    private val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity
        val binding: FragmentIndicatePeriodBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_indicate_period, container, false)
        binding.createPlanViewModel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
    }

    private fun setupListeners() {
        tiet_from.setOnClickListener {
            showMonthPickerFrom()
        }
        tiet_to.setOnClickListener {
            showMonthPickerTo()
        }
    }

    private fun showMonthPickerFrom() {
        val initTime = viewModel.dispenseRequest.value?.validityPeriod?.start as DateTime
        val mYear = initTime.year
        val mMonth = initTime.monthOfYear - 1
        val mDay = initTime.dayOfMonth
        val datePickerDialog = DatePickerDialog(activity, { _, year, month, dayOfMonth ->
            viewModel.dispenseRequest.value?.validityPeriod?.start =
                    DateTime(year, month + 1, dayOfMonth, 0, 0)

            if ((viewModel.dispenseRequest.value?.validityPeriod?.start as DateTime).millis >
                (viewModel.dispenseRequest.value?.validityPeriod?.end as DateTime).millis) {
                val start = viewModel.dispenseRequest.value?.validityPeriod?.start as DateTime

                viewModel.dispenseRequest.value?.validityPeriod?.end =
                        DateTime(start.year, start.monthOfYear, start.dayOfMonth, 23, 59)


                tiet_to.setText(formatter.print(start.millis))
            }

            tiet_from.setText(formatter.print(
                    (viewModel.dispenseRequest.value?.validityPeriod?.start as DateTime).millis))
        }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = DateTime.now()
            .millis
        datePickerDialog.show()
    }

    private fun showMonthPickerTo() {
        val initTime = viewModel.dispenseRequest.value?.validityPeriod?.end as DateTime
        val mYear = initTime.year
        val mMonth = initTime.monthOfYear - 1
        val mDay = initTime.dayOfMonth
        val datePickerDialog = DatePickerDialog(activity, { _, year, month, dayOfMonth ->
            viewModel.dispenseRequest.value?.validityPeriod?.end =
                    DateTime(year, month + 1, dayOfMonth, 23, 59)

            tiet_to.setText(formatter.print(
                    (viewModel.dispenseRequest.value?.validityPeriod?.end as DateTime).millis))
        }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate =
                (viewModel.dispenseRequest.value?.validityPeriod?.start as DateTime).millis

        datePickerDialog.show()
    }

    companion object {
        val TAG = IndicatePeriodFragment::class.java.canonicalName
    }
}

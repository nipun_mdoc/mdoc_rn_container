package de.mdoc.modules.digital_signature


import android.graphics.*
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.github.barteksc.pdfviewer.PDFView
import de.mdoc.R
import de.mdoc.databinding.DocumentReviewFragmentBinding
import de.mdoc.network.RestClient
import de.mdoc.pojo.digitalsignature.FormMetadataDTO
import de.mdoc.util.ColorUtil
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.document_review_fragment.*


/**
 * Created by Trenser on 30/12/20.
 */

class DocumentReviewFragment : Fragment() {

    private var pdfView: PDFView? = null
    private var currentPageView: TextView? = null
    private var currentFileName: TextView? = null

    var id: String? = null
    var checkBoxBitmap: Bitmap? = null
    var currentPageWidth: Float? = null
    var currentPageHeight: Float? = null


    private val documentReviewViewModel by viewModel {
        DocumentReviewViewModel(RestClient.getService(), arguments?.getString("docId"))
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: DocumentReviewFragmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.document_review_fragment, container, false)
        binding.lifecycleOwner = this
        binding.documentReviewViewModel = documentReviewViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        documentReviewViewModel.setLoading(true);
        view.findViewById<ImageButton>(R.id.reviewToolbarBack)?.setOnClickListener {
            findNavController().popBackStack()
        }
        pdfView = view.findViewById(R.id.reviewPdfViewer)
        currentPageView = view.findViewById(R.id.reviewPageNumber)
        currentFileName = view.findViewById(R.id.reviewFileName)
        val continueSignView: Button = view.findViewById(R.id.reviewNext)
        val reviewDocDetailsLayout: LinearLayout = view.findViewById(R.id.reviewDocDetailsLayout)

        reviewDocumentTxt.setTextColor(ColorUtil.contrastColor(resources))
        reviewToolbarBack.setColorFilter(ColorUtil.contrastColor(resources))

        id = arguments?.getString("docId");
        documentReviewViewModel.getDocumentData(id!!, onSuccess = { pdfData ->
            documentReviewViewModel.getDocumentMetaData(id!!, onSuccess = {
                documentReviewViewModel.signatureDocumentInfoData.observe(viewLifecycleOwner, Observer { item ->
                    run {
                        currentFileName?.text = item.title ?: item.fileName
                    }
                })

                pdfView?.fromStream(pdfData)
                        ?.enableDoubletap(false)
                        ?.onDraw { canvas, pageWidth, pageHeight, displayedPage ->
                            currentPageView?.text = getString(R.string.page_count,(displayedPage + 1).toString(),(pdfView?.pageCount).toString())
                            continueSignView.isEnabled = displayedPage + 1 == pdfView?.pageCount
                            pdfView?.invalidate()
                            currentPageHeight = pageHeight
                            currentPageWidth = pageWidth

                            documentReviewViewModel.getFormMetadataDTO().observe(viewLifecycleOwner, Observer<List<FormMetadataDTO?>> { item: List<FormMetadataDTO?>? ->
                                item?.map {
                                    if (it != null) {
                                        if (it.formType == "CHECKBOX" && displayedPage == it.pageNumber - 1) {
                                            val paint = Paint()
                                            paint.color = Color.GREEN
                                            val x: Float = pageWidth * it.x.toFloat()
                                            val y: Float = pageHeight * it.y.toFloat()
                                            val width: Float = pageWidth * it.width.toFloat()
                                            val height: Float = pageHeight * it.height.toFloat()

                                            checkBoxBitmap = BitmapFactory.decodeResource(resources, (if (it.checked) R.drawable.ic_checkbox_enabled else R.drawable.ic_checkbox_disabled))
                                                    .copy(Bitmap.Config.ARGB_8888, true)

                                            val scaledBitmap = scaleBitmap(checkBoxBitmap as Bitmap, width.toInt(), height.toInt())
                                            canvas?.drawBitmap(scaledBitmap!!, x, y, paint)

                                        }
                                    }
                                }
                            })
                        }
                        ?.onTap { e ->
                            val currentPageXOffset = -pdfView!!.currentXOffset % currentPageWidth!!
                            val mappedX: Float = currentPageXOffset + e.x
                            val currentPageYOffset = -pdfView!!.currentYOffset % currentPageHeight!!
                            val mappedY: Float = currentPageYOffset + e.y

                            updateCheckboxStatus(mappedX.toInt(), mappedY.toInt())

                            true
                        }
                        ?.onLoad {
                            reviewDocDetailsLayout.visibility = View.VISIBLE
                            documentReviewViewModel.setLoading(false);
                        }
                        ?.load()
            }, onError = {})
        }, onError = {})


        continueSignView?.setOnClickListener {
            documentReviewViewModel.postSignInApi(onSuccess = { url ->
                if(url != null){
                    val bundle = bundleOf("url" to url,"isFromReviewDocument" to true)
                    findNavController().navigate(R.id.signDocumentFragment, bundle)
                } else {
                    MdocUtil.showToastLong(context, getString(R.string.something_went_wrong))
                }
            }, onError = {})
        }

    }

    private fun scaleBitmap(bitmap: Bitmap, wantedWidth: Int, wantedHeight: Int): Bitmap? {
        val output = Bitmap.createBitmap(wantedWidth, wantedHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)
        val m = Matrix()
        m.setScale(wantedWidth.toFloat() / bitmap.width, wantedHeight.toFloat() / bitmap.height)
        canvas.drawBitmap(bitmap, m, Paint())
        return output
    }

    private fun updateCheckboxStatus(touchX: Int, touchY: Int) {
        val currentPage = pdfView?.currentPage
        documentReviewViewModel.getFormMetadataDTO().observe(viewLifecycleOwner, Observer<List<FormMetadataDTO?>> { items: List<FormMetadataDTO?>? ->
            if (items != null) {
                for (it in items)
                    if (it != null) {
                        val id = it.id
                        if (it.formType == "CHECKBOX" && currentPage == it.pageNumber - 1) {
                            val ex: Int = (currentPageWidth!!.times(it.x.toFloat())).toInt()
                            val ey: Int = (currentPageHeight!!.times(it.y.toFloat())).toInt()
                            val width: Int = (currentPageWidth!!.times(it.width.toFloat())).toInt()
                            val height: Int = (currentPageHeight!!.times(it.height.toFloat())).toInt()

                            val r = Rect(ex, ey, ex + width, ey + height)
                            if (r.contains(touchX, touchY)) {
                                documentReviewViewModel.setFormMetaDataById(id)
                                break
                            }
                        }
                    }
            }
        })
    }
}
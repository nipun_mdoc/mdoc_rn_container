package de.mdoc.modules.profile.preview.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.profile.preview.PreviewDataFactory
import de.mdoc.modules.profile.preview.adapters.PreviewDataAdapter
import de.mdoc.modules.profile.preview.data.Preview
import kotlinx.android.synthetic.main.fragment_metadata_preview.*


class PreviewMetadataFragment : Fragment(R.layout.fragment_metadata_preview) {

    lateinit var datasource: List<Preview>
    var headerTitle: Int = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvMetadataPreview.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            adapter = PreviewDataAdapter(datasource)
        }

        if (headerTitle != -1) {
            metaDataPreviewTitle.setText(headerTitle)
        }
    }
}

package de.mdoc.modules.airandpollen.items

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.ImageViewCompat
import de.mdoc.R
import de.mdoc.modules.airandpollen.data.AirAndPollenInfo
import de.mdoc.modules.airandpollen.data.PollenLevel
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder
import kotlinx.android.synthetic.main.air_and_pollen_info_line.view.*

class AirAndPollenItemAdapter :
    ListItemTypeAdapter<AirAndPollenInfo, AirAndPollenItemHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): AirAndPollenItemHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.air_and_pollen_info_line, parent, false)
        return AirAndPollenItemHolder(view)
    }

    override fun onBind(position: Int, item: AirAndPollenInfo, holder: AirAndPollenItemHolder) {
        holder.name.setText(item.type.nameResId)
        val fillColor = item.level.getColor(holder.itemView.context)
        val grayColor = PollenLevel.None.getColor(holder.itemView.context)
        holder.images.forEachIndexed { index, imageView ->
            val color = if (item.amount > index) fillColor else grayColor
            ImageViewCompat.setImageTintList(imageView, ColorStateList.valueOf(color))
        }
        holder.level.text = item.level.getName(holder.itemView.context)
    }

}

class AirAndPollenItemHolder(view: View) : ListItemViewHolder(view) {

    val name = view.name
    val images = listOf(
        view.pollenLevel1,
        view.pollenLevel2,
        view.pollenLevel3,
        view.pollenLevel4,
        view.pollenLevel5,
        view.pollenLevel6
    )
    val level = view.level

}
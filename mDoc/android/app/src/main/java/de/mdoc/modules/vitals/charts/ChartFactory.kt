package de.mdoc.modules.vitals.charts

import android.app.Activity
import de.mdoc.modules.devices.data.ChartData

fun createChart(graphType: String, chartData: ChartData) {
    val metricCodes = chartData.metrics ?: emptyList()
    val activity = chartData.context as Activity
    val rawData = chartData.rawData
    val period = chartData.period
    val lastDayOfMonth = chartData.maxDaysInMonth
    val unit = chartData.unit

    when (graphType) {
        "line" -> {
            if (rawData?.isComposite() == true) {
                drawCompositeLineChart(
                        activity,
                        period!!,
                        compositeObservationToEntry(rawData.data, period),
                        metricCodes,
                        lastDayOfMonth
                                      )
            }
            else {
                drawLineChart(
                        activity,
                        period!!,
                        observationToChartEntry(rawData!!.data.getOrNull(0)?.data?.list, period),
                        lastDayOfMonth,
                        unit)
            }
        }

        "bar"  -> {
            if (rawData?.isComposite() == true) {
                drawCompositeBarChart(
                        activity, period!!,
                        compositeObservationToBarEntry(rawData.data, period),
                        metricCodes,
                        lastDayOfMonth
                                     )
            }
            else {
                drawBarChart(
                        activity,
                        period!!,
                        parseDataForBarGraph(rawData!!.data, period),
                        lastDayOfMonth,
                        unit)
            }
        }
    }
}
package de.mdoc.modules.appointments.video_conference.data

import com.opentok.android.Session
import com.opentok.android.Subscriber

data class ActiveSpeakerData(
        var participantName: String? = null,
        var streamId: String? = null,
        var session: Session,
        var subscriber: Subscriber? = null
)

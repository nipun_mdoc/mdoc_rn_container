package de.mdoc.newlogin.Interactor

import android.app.Dialog
import android.content.Context
import de.mdoc.R
import de.mdoc.activities.login.EnterEmailDialogFragment
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.activities.login.SaveMailDialogFragment
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.SaveEmailRequest
import de.mdoc.network.response.*
import de.mdoc.pojo.Case
import de.mdoc.pojo.SecretModel
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*

class LoginHelper(
        private val context: Context,
        private val loginCallback: LoginCallback,
        private val progressHandler: ProgressHandler
): SaveMailDialogFragment.DialogSaveCallback {

    var userId: String = ""
    var enterEmailDialog: EnterEmailDialogFragment? = null

    interface LoginCallback {

        fun openTermsAndConditions()

        fun openPrivacyPolicy()

        fun openNextActivity(cases: ArrayList<Case>?)

        fun openClinics()

        fun onMissingSecret ()
    }

    fun fetchSecretKey() {

        val deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL


        MdocManager.fetchSecret(deviceName, object : Callback<SecretModel> {
            override fun onResponse(call: Call<SecretModel>, response: Response<SecretModel>) {
                if (response.isSuccessful) {
                    val secret = response?.body()?.data?.key.toString()
                    MdocAppHelper.getInstance().secretKey = secret
                    loginWithSession()
                }else{
                    progressHandler.hideProgress()
                    if(response?.code()==404){
//                        loginCallback.reloadInitialPage()
                        loginCallback.onMissingSecret()
                    }
                }
            }

            override fun onFailure(call: Call<SecretModel>, t: Throwable) {

            }
        })
    }

    fun loginWithSession() {
        progressHandler.showProgress()


            MdocManager.loginWithSession(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.isSuccessful) {

                        progressHandler.hideProgress()
                        response.body()
                                ?.data?.publicUserDetails?.lastSeenUnitByCode?.let {
                                    AppPersistence.lastSeenUnitByCode = it
                                }
                        MdocAppHelper.getInstance().emergencyInfo =
                                response.body()?.data?.extendedUserDetails?.emergencyInfo
                        MdocAppHelper.getInstance()
                                .publicUserDetailses =
                                response.body()
                                        ?.data?.publicUserDetails

                        MdocAppHelper.getInstance()
                                .userLastName =
                                response.body()
                                        ?.data?.publicUserDetails?.lastName
                        MdocAppHelper.getInstance()
                                .userFirstName =
                                response.body()
                                        ?.data?.publicUserDetails?.firstName
                        MdocAppHelper.getInstance()
                                .username =
                                response.body()
                                        ?.data?.publicUserDetails?.username
                        MdocAppHelper.getInstance()
                                .paperPrintInfo =
                                response.body()
                                        ?.data?.publicUserDetails?.printPatientPlan
                        MdocAppHelper.getInstance()
                                .userType = response.body()
                                ?.data?.userType
                        MdocAppHelper.getInstance()
                                .userId = response.body()
                                ?.data?.userId

                        MdocAppHelper.getInstance()
                                .clinicId =
                                response.body()
                                        ?.data?.accesses?.cases?.getOrNull(0)
                                        ?.clinicId
                        MdocAppHelper.getInstance()
                                .email = response.body()
                                ?.data?.publicUserDetails?.email
                        updateFirebaseToken()

                        response.body()
                                ?.data?.accesses?.tenantPublicDocument?.fileConfig?.isPatientFileUploadAllowed?.let {
                                    MdocAppHelper.getInstance()
                                            .setPatientFileUploadAllowed(it)
                                }
                        //privacy and policy
                        // here we must delete boolean after privacy is implemented on backend!!
                        if(response.body()?.data?.accesses?.cases != null && response.body()?.data?.accesses?.cases?.size == 0){
                            loginCallback.openClinics()
                        }else {
                            if (context.resources.getBoolean(R.bool.privacy_and_terms)) {
                                if (response.body()?.data?.publicUserDetails?.isPrivacyAccepted == false) {
                                    loginCallback.openPrivacyPolicy()
                                } else if (response.body()?.data?.publicUserDetails?.isTermsAccepted == false) {
                                    loginCallback.openTermsAndConditions()
                                } else if (response.body()?.data?.publicUserDetails?.email.isNullOrEmpty()) {
                                    if (response.body()?.data?.publicUserDetails?.emailRequestShown == null || response.body()?.data?.publicUserDetails?.emailRequestShown == false) {
                                        showEmailDialog(response)
                                    } else {
                                        setLoginData(response)
                                    }
                                } else {
                                    setLoginData(response)
                                }
                            } else {
                                setLoginData(response)
                            }
                        }
                    } else {
                        Timber.w("loginWithSession ${response.getErrorDetails()}")

                        loginCallback.openClinics()
                        progressHandler.hideProgress()
                    }

                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    progressHandler.hideProgress()
                    Timber.w(t, "loginWithSession")
                }
            })
    }

    private fun updateFirebaseToken() {
        //register firebase token on our server
        MdocManager.updateFirebaseToken(object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                MdocUtil.showToastShort(context, t.message)
            }
        })
    }

    private fun setLoginData(response: Response<LoginResponse>) {
        if (response.body()?.data?.accesses?.cases?.size!! > 0) {
            MdocAppHelper.getInstance()
                    .caseId =
                    response.body()
                            ?.data?.accesses?.cases?.get(0)
                            ?.caseId
            MdocAppHelper.getInstance()
                    .setIsPremiumPatient(
                            response.body()?.data?.accesses?.cases?.get(0)?.isPremium ?: false
                    )

            MdocAppHelper.getInstance()
                    .department =
                    response.body()
                            ?.data?.accesses?.cases?.get(0)
                            ?.department
            MdocAppHelper.getInstance()
                    .externalCaseId =
                    response.body()
                            ?.data?.accesses?.cases?.get(0)
                            ?.externalCaseId

            if(response.body()?.data?.accesses?.cases?.size!! > 1)
                loginCallback.openNextActivity(response.body()?.data?.accesses?.cases)
            else
                loginCallback.openNextActivity(null)
        }else{
            loginCallback.openNextActivity(null)
        }
    }

    private fun showEmailDialog(resp: Response<LoginResponse>) {
        userId = resp.body()?.data?.userId ?: ""
        enterEmailDialog = EnterEmailDialogFragment(dialogCallback = this, resp = resp)
        enterEmailDialog?.showNow((context as LogiActivty).supportFragmentManager, "")
    }

    private fun saveEmailApiCall(email: String, resp: Response<LoginResponse>, dialog: Dialog?) {
        MdocManager.saveEmail(SaveEmailRequest(email, userId), object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    dialog?.dismiss()
                    SaveMailDialogFragment(dialogCallback = this@LoginHelper, email = email, resp = resp).showNow(
                            (context as LogiActivty).supportFragmentManager, "")
                } else {
                    enterEmailDialog?.setErrorMessageForEmailInUse()
                }
                enterEmailDialog?.enableButtons()
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                dialog?.dismiss()
                setLoginData(resp)
                enterEmailDialog?.enableButtons()
            }
        })
    }

    private fun skipEmailApiCall(userId: String, resp: Response<LoginResponse>, dialog: Dialog?) {
        MdocManager.skipEmail(userId, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                dialog?.dismiss()
                setLoginData(resp)
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                dialog?.dismiss()
                setLoginData(resp)
            }
        })
    }

    override fun saveMail(mail: String?, resp: Response<LoginResponse>, dialog: Dialog?) {
        if (mail != null) {
            saveEmailApiCall(mail, resp, dialog)
        }
        else {
            progressHandler.showProgress()
            skipEmailApiCall(userId, resp, dialog)
        }
    }
}
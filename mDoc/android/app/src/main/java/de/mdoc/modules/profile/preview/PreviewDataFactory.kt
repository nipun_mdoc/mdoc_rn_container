package de.mdoc.modules.profile.preview

import androidx.annotation.StringRes
import de.mdoc.R
import de.mdoc.modules.profile.patient_details.PatientDetailsHelper
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.modules.profile.preview.data.Preview
import de.mdoc.network.response.CodingResponse
import de.mdoc.network.response.CoverageResponse
import de.mdoc.pojo.CodingItem
import de.mdoc.util.formatMdocDate

object PreviewDataFactory {

    fun getPersonalData(): List<Preview> {
        val datasource = mutableListOf<Preview>()

        // Firstname
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.firstName, R.string.first_name)


        // Surname
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.lastName, R.string.surname)


        // Single name
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.maidenName, R.string.single_name)

        // Gender
        datasource.tryAddItem(PatientDetailsHelper.getInstance().genderCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.genderCode, R.string.gender_star)

        // Date of birth
        PatientDetailsHelper.getInstance().patientDetailsData.publicUserDetails.born.let {
            datasource.add(Preview(R.string.date_of_birth_star, it.formatMdocDate()))
        }

        // Nationality
        datasource.tryAddItem(PatientDetailsHelper.getInstance().nationalityCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.nationality, R.string.nationality)

        // Hometown
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.homeTown, R.string.hometown)

        // Language
        datasource.tryAddItem(PatientDetailsHelper.getInstance().languageCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.language, R.string.language_of_correspondence)

        // Ahv
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.ssn, R.string.ahv_nr)

        // Civil status
        datasource.tryAddItem(PatientDetailsHelper.getInstance().civilStatusCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.maritalStatus, R.string.civil_status)

        // Religion
        datasource.tryAddItem(PatientDetailsHelper.getInstance().religionCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.religion, R.string.releigion)

        return datasource
    }


    fun getResidenceData(): List<Preview> {
        val datasource = mutableListOf<Preview>()

        // Street and house number
        datasource.tryAddItems(arrayListOf(
                PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.street,
                PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.houseNumber),
                R.string.street_and_housnumber)

        // Country
        datasource.tryAddItem(PatientDetailsHelper.getInstance().nationalityCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.countryCode, R.string.country)

        // Postal code
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.postalCode, R.string.post_code)

        // City
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.city, R.string.city)

        // Personal phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.homePhoneNumber, R.string.personal_phone)

        // Business phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.workPhoneNumber, R.string.businnes_phone)

        // Mobile phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.phone, R.string.meta_data_phone_mandatory)

        // Email
        datasource.tryAddItem(PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.email, R.string.email)

        return datasource
    }

    fun getEmployerData(): List<Preview> {
        val datasource = mutableListOf<Preview>()

        // Job
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.occupation, R.string.job)

        // Employer
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.name, R.string.employer)


        // Place
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.address?.city, R.string.place)

        // Postal code
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.address?.postalCode, R.string.postal_not_req)

        return datasource
    }

    fun getCaregiverData(): List<Preview> {
        val datasource = mutableListOf<Preview>()

        // Relationship
        datasource.tryAddItem(PatientDetailsHelper.getInstance().relationshipCodingResponse,
                PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                        ?.relationshipType, R.string.relationship)

        // First name
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.firstName, R.string.first_name_nr)

        // Last name
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.lastName, R.string.surname_nr)


        //street_and_housnumber_nr
        datasource.tryAddItems(arrayListOf(
                PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)?.address?.street,
                PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)?.address?.houseNumber),
                R.string.street_and_housnumber_nr)


        //city_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.city, R.string.city_nr)


        //postal_not_req
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.postalCode, R.string.postal_not_req)

        //country_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance().nationalityCodingResponse, PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)?.address?.countryCode, R.string.country_nr)


        //personal_phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.homePhoneNumber, R.string.personal_phone)

        //businnes_phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.workPhoneNumber, R.string.businnes_phone)


        //meta_data_phone
        datasource.tryAddItem(PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.phone, R.string.meta_data_phone)


        return datasource
    }

    fun getFamilyDoctorData(): List<Preview> {
        val datasource = mutableListOf<Preview>()

        //first_name_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.firstName, R.string.first_name_nr)

        //surname_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.lastName, R.string.surname_nr)

        // street_and_housnumber_nr
        datasource.tryAddItems(arrayListOf(
                PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.street,
                PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.houseNumber
        ), R.string.street_and_housnumber_nr)

        // city_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.city, R.string.city_nr)

        // postal_not_req
        datasource.tryAddItem(PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.postalCode, R.string.postal_not_req)


        // country_nr
        datasource.tryAddItem(PatientDetailsHelper.getInstance().nationalityCodingResponse, PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.countryCode, R.string.country_nr)

        // meta_data_phone_number
        datasource.tryAddItem(PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.phone, R.string.meta_data_phone_number)

        return datasource
    }

    fun getInsuranceData(coverage: CoverageResponse? = null,
                         healthInsurance: InsuranceDataKt? = null,
                         insuranceClassList: CodingItem? = null,
                         supplementaryInsurance: InsuranceDataKt? = null,
                         supplementaryInsuranceClass: CodingItem? = null,
                         accidentInsurance: InsuranceDataKt? = null,
                         accidentInsuranceClass: CodingItem? = null,
                         supplementaryAccidentInsurance: InsuranceDataKt? = null,
                         supplementaryAccidentInsuranceClass: CodingItem? = null,
                         disabilityInsurance: InsuranceDataKt? = null,
                         militaryInsurance: InsuranceDataKt? = null

    ): List<Preview> {
        val datasource = mutableListOf<Preview>()

        // meta_data_basic_health
        datasource.tryAddItem(healthInsurance?.name, R.string.meta_data_basic_health)

        //meta_data_insurance_card_number
        datasource.tryAddItem(coverage?.krankIdentifier, R.string.meta_data_insurance_card_number)

        //meta_data_insurance_class
        datasource.tryAddItem(insuranceClassList?.display, R.string.meta_data_insurance_class)

        //meta_data_supplementary_insurance
        datasource.tryAddItem(supplementaryInsurance?.name, R.string.meta_data_supplementary_insurance)

        //meta_data_supplementary_card_number
        datasource.tryAddItem(coverage?.suplementeryIdentifier, R.string.meta_data_supplementary_card_number)

        //meta_data_supplementary_insurance_class
        datasource.tryAddItem(supplementaryInsuranceClass?.display, R.string.meta_data_supplementary_insurance_class)

        //meta_data_accident_insurance
        datasource.tryAddItem(accidentInsurance?.name, R.string.meta_data_accident_insurance)

        //meta_data_accident_insurance_plan_id
        datasource.tryAddItem(coverage?.acidentIdentifier, R.string.meta_data_accident_insurance_plan_id)

        //meta_data_accident_insurance_class
        datasource.tryAddItem(accidentInsuranceClass?.display, R.string.meta_data_accident_insurance_class)


        //meta_data_supplementary_accident_insurance
        datasource.tryAddItem(supplementaryAccidentInsurance?.name, R.string.meta_data_supplementary_accident_insurance)

        //meta_data_supplementary_accident_ins_plan_id
        datasource.tryAddItem(coverage?.suplementeryAccidentIdentifier, R.string.meta_data_supplementary_accident_ins_plan_id)

        //meta_data_supplementary_accident_ins_class
        datasource.tryAddItem(supplementaryAccidentInsuranceClass?.display, R.string.meta_data_supplementary_accident_ins_class)

        // meta_data_disability_insurance
        datasource.tryAddItem(disabilityInsurance?.name, R.string.meta_data_disability_insurance)

        //meta_data_disability_insurance_number
        datasource.tryAddItem(coverage?.invalidIdentifier, R.string.meta_data_disability_insurance_number)

        //meta_data_military_insurance
        datasource.tryAddItem(militaryInsurance?.name, R.string.meta_data_military_insurance)
        return datasource
    }

    private fun MutableList<Preview>.tryAddItem(key: String?, @StringRes label: Int) {
        add(Preview(label, key ?: ""))
    }

    private fun MutableList<Preview>.tryAddItems(keys: ArrayList<String?>, @StringRes label: Int) {
        var result = ""
        keys.forEach { it ->
            it?.let {
                when (result.length) {
                    0 -> {
                        result = it
                    }
                    else -> {
                        result += " $it"
                    }
                }
            }
        }

        add(Preview(label, result))
    }

    private fun MutableList<Preview>.tryAddItem(coding: CodingResponse?, key: String?, @StringRes label: Int) {
        val item = coding?.findDisplayByCode(key)
        add(Preview(label, item ?: ""))
    }

    private fun CodingResponse.findDisplayByCode(code: String?): String? {
        return this.data?.list?.find { it.code == code }?.display
    }
}
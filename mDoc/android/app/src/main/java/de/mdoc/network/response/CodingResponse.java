package de.mdoc.network.response;

import de.mdoc.pojo.CodingData;

public class CodingResponse extends MdocResponse {

    private CodingData data;

    public CodingData getData() {
        return data;
    }

    public void setData(CodingData data) {
        this.data = data;
    }
}

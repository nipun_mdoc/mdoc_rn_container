package de.mdoc.modules.files.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.pojo.FileListItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class FilesTransitionViewModel(private val mDocService: IMdocService, val fileId: String) : ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    val proceed = MutableLiveData(false)


    var file: MutableLiveData<FileListItem> =
            MutableLiveData(FileListItem())

    fun getFileById() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            try {
                isLoading.value = true
                file.value = mDocService.getFileById(fileId).data
                isShowContent.value = true
                isLoading.value = false
                proceed.value = true
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowLoadingError.value = true
                isShowContent.value = false
            }
        }
    }
}
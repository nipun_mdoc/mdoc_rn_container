package de.mdoc.newlogin

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.login.LoginUtil
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.network.RestClient
import de.mdoc.newlogin.Interactor.ForgetPassword
import de.mdoc.newlogin.Interactor.LoginTan
import de.mdoc.newlogin.Interactor.RegistrationActivity
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.newlogin.Model.ImprintResponse
import de.mdoc.newlogin.Model.LoginResponseData
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.util.token_otp.Token
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LoginViewModel : ViewModel() {
    var progressHandler: ProgressHandler? = null
    var context: Context? = null
    var authListener: AuthListener? = null
    var userMutableLiveData: MutableLiveData<LoginUser>? = null

    val uesr_edit: MutableLiveData<String> = MutableLiveData()
    val password_edit: MutableLiveData<String> = MutableLiveData()
    val captcha_edit: MutableLiveData<String> = MutableLiveData()
    val mobilenumber: MutableLiveData<String> = MutableLiveData()
    val smsotp: MutableLiveData<String> = MutableLiveData()
    val user: MutableLiveData<LoginUser>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData()
            }
            return userMutableLiveData as MutableLiveData<LoginUser>
        }

    fun onLoginClick(view: View?) {
        view?.isClickable = false
        Handler().postDelayed(Runnable {
            view?.isClickable = true
        }, 3000)

        context = view!!.context
        progressHandler = ProgressHandler(this.context!!)
        var otp: String? = ""
        if (MdocAppHelper.getInstance().secretKey != null) {
            try {
                val token =
                        Token(MdocAppHelper.getInstance().secretKey)
                val code = token.generateCodes(System.currentTimeMillis())
                if (code == null)
                    Toast.makeText(context, context?.resources?.getString(R.string.err_time_token),
                            Toast.LENGTH_LONG)
                            .show()
                else
                    otp = code.currentCode1

                Log.e("code",code.getCurrentCode(System.currentTimeMillis()))
                Log.e("code1",code.currentCode1)
            } catch (e: Token.TokenUriInvalidException) {
                e.printStackTrace()
            }
        }
        val instanceId = FirebaseInstanceId.getInstance().id
        val deviceFriendly = "${android.os.Build.MODEL} # $instanceId"
        var loginUser = LoginUser()
        loginUser.client_id = this.context?.getString(R.string.keycloack_clinic_Id)
        loginUser.client_secret = this.context?.getString(R.string.client_secret)
        loginUser.friendlyName = deviceFriendly
        loginUser.password = if (password_edit.value != null) password_edit.value!!.trim() else ""
        loginUser.totp = otp
        loginUser.username = if (uesr_edit.value != null) uesr_edit.value!! else ""
        loginUser.captcha = if (captcha_edit.value != null) captcha_edit.value!!.trim() else ""
        loginUser.mobileNumber = if(mobilenumber.value !=null && mobilenumber.value?.length!! > 10) mobilenumber.value!!.trim().replace(" ","") else ""
        loginUser.smsotp = if(smsotp.value !=null) smsotp.value!!.trim() else ""
        userMutableLiveData!!.value = loginUser

    }

    fun createLlginRequest(context1: Context) {
        context = context1

        progressHandler = ProgressHandler(this.context!!)
        var otp: String? = ""
        if (MdocAppHelper.getInstance().secretKey != null) {
            try {
                val token =
                        Token(MdocAppHelper.getInstance().secretKey)
                val code = token.generateCodes(System.currentTimeMillis())
                if (code == null)
                    Toast.makeText(context, context1.resources.getString(R.string.err_time_token),
                            Toast.LENGTH_LONG)
                            .show()
                else
                    otp = code.getCurrentCode(System.currentTimeMillis())
            } catch (e: Token.TokenUriInvalidException) {
                e.printStackTrace()
            }
        }
        val instanceId = FirebaseInstanceId.getInstance().id
        val deviceFriendly = "${LoginUtil.DEVICE_NAME} # $instanceId"
        var loginUser = LoginUser()
        loginUser.client_id = this.context?.getString(R.string.keycloack_clinic_Id)
        loginUser.client_secret = this.context?.getString(R.string.client_secret)
        loginUser.friendlyName = deviceFriendly
        loginUser.password = if (password_edit.value != null) password_edit.value!!.trim() else ""
        loginUser.totp = otp
        loginUser.username = if (uesr_edit.value != null) uesr_edit.value!! else ""
        if (mobilenumber.value != null && mobilenumber.value?.length!! > 10){
        loginUser!!.mobileNumber = if (mobilenumber.value != null) mobilenumber.value!!.trim().replace(" ","") else ""
        }
        if (smsotp.value != null){
            loginUser!!.smsotp = if (smsotp.value != null) smsotp.value!!.trim() else ""
        }
        userMutableLiveData!!.value = loginUser
    }

    fun onForgetClick(view: View?) {
        var context: Context = view!!.context
        var intent = Intent(context, ForgetPassword::class.java)
        context.startActivity(intent)

    }

    fun onSinguClick(view: View?) {
        var context: Context = view!!.context
        var intent = Intent(context, RegistrationActivity::class.java)
        context.startActivity(intent)
    }


    fun onScanClick(view: View?) {
        authListener?.onScanClick()
    }

    fun onImpressumClick(view: View?) {
        loadImprintData("imprint", view!!.context)
    }

    fun onDatenschutzDieClick(view: View?) {
        loadImprintData("data", view!!.context)
    }

    fun onAGBClick(view: View?) {
        loadImprintData("privacy", view!!.context)
    }


    fun onTanLoginClick(view: View?) {
        var context: Context = view!!.context
        var intent = Intent(context, LoginTan::class.java)
        context.startActivity(intent)

    }

    fun onRefreshCaptchaClick(view: View?) {
        var context: Context = view!!.context
        loadCaptcha(context)
    }

    fun loadData(loginUser: LoginUser, context: Context) {
        MdocAppHelper.getInstance().accessToken = null
        MdocAppHelper.getInstance().refreshToken = null
        progressHandler?.showProgress()
        var call: Call<LoginResponseData>
        if(loginUser.captcha != null && loginUser.captcha!!.length > 0){
            call = RestClient.getAuthServiceLogin().authenticateUserCaptcha(loginUser.captcha, loginUser)
        }else{
            call = RestClient.getAuthServiceLogin().authenticateUser(loginUser)
        }
        call.enqueue(object : Callback<LoginResponseData?> {
            override fun onResponse(call: Call<LoginResponseData?>, response: Response<LoginResponseData?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    MdocAppHelper.getInstance().accessToken = response.body()!!.data.tokenType + " " + response.body()!!.data.accessToken
                    MdocAppHelper.getInstance().refreshToken = response.body()!!.data.tokenType + " " + response.body()!!.data.refreshToken
                    MdocAppHelper.getInstance().refreshTokenNoHeader = response.body()!!.data.refreshToken
                    if(response.body()!!.data.deviceSecret!=null && response.body()!!.data.deviceSecret?.trim().isNotEmpty())
                        MdocAppHelper.getInstance().secretKey = response.body()!!.data.deviceSecret
                    if (response.body()!!.code == "MDOC-IAM-020") {
                        authListener?.changePassword(response.body()!!.data.passwordResetToken)
                    } else {
                        authListener?.sucess()
                    }
                } else {
                    progressHandler?.hideProgress()
                    try {
                        val errorBody: ResponseBody? = response.errorBody()
                        var errorMessage: String = errorBody?.string()!!
                        if (errorMessage.contains("\"data\":\"\""))
                            errorMessage = errorMessage.replace(",\"data\":\"\"", "")
                        val errorResponse = Gson().fromJson(errorMessage, LoginResponseData::class.java)
                        if (errorResponse.code == "MDOC-IAM-022" || errorResponse.code == "MDOC-IAM-023") {
                            MdocUtil.showToastLong(context, if (errorResponse !== null && errorResponse.message != "") errorResponse.message else context.resources?.getString(R.string.login_failed))
                            loadCaptcha(context)
                        } else if (errorResponse.code == "MDOC-IAM-025" ) {
                            authListener?.showMobileNumberScreen()
                        } else if (errorResponse.code == "MDOC-IAM-026" || errorResponse.code == "MDOC-IAM-024") {
                            authListener?.showOTPScreen()
                        } else {
                            MdocUtil.showToastLong(context, if (errorResponse !== null && errorResponse.message != "") errorResponse.message else context.resources?.getString(R.string.login_failed))
                            if (authListener?.isCaptchaVisible()!!)
                                loadCaptcha(context)
                        }
                    } catch (ex: Exception) {
                        MdocUtil.showToastLong(context, context.resources?.getString(R.string.login_failed))
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponseData?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context.resources?.getString(R.string.login_failed))
            }
        })
    }

    fun loadCaptcha(context: Context) {
        authListener?.clearCaptchaField()
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getCaptcha()
        call!!.enqueue(object : Callback<CaptchaResponse?> {
            override fun onResponse(call: Call<CaptchaResponse?>, response: Response<CaptchaResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    authListener?.onLoadCaptcha(response.body())
                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if (errorResponse !== null && errorResponse.message != "") errorResponse.message else context.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<CaptchaResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context.resources?.getString(R.string.error_upload))
            }
        })
    }

    fun loadImprintData(requestType: String, context: Context) {
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getImprintData(Locale.getDefault().language)
        call!!.enqueue(object : Callback<ImprintResponse?> {
            override fun onResponse(call: Call<ImprintResponse?>, response: Response<ImprintResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    if (requestType.equals("imprint")) {
                        authListener?.onAGBClick(response.body()?.data?.get(0)?.display!!)
                    } else if (requestType.equals("data")) {
                        authListener?.onAGBClick(response.body()?.data?.get(1)?.display!!)
                    } else if (requestType.equals("privacy")) {
                        authListener?.onAGBClick(response.body()?.data?.get(2)?.display!!)
                    }

                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if (errorResponse !== null && errorResponse.message != "") errorResponse.message else context.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<ImprintResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context.resources?.getString(R.string.error_upload))
            }
        })
    }

}
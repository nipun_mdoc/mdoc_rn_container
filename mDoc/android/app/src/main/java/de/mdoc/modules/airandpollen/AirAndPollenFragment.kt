package de.mdoc.modules.airandpollen

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.airandpollen.data.AirAndPollenRepository
import de.mdoc.modules.airandpollen.items.AirAndPollenItemAdapter
import de.mdoc.modules.airandpollen.items.AirAndPollenTitle
import de.mdoc.modules.airandpollen.items.AirAndPollenTitleAdapter
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.bindVisibleInvisible
import de.mdoc.viewmodel.compositelist.CompositeListAdapter
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_air_and_pollen.*

class AirAndPollenFragment : MdocFragment(), TabLayout.OnTabSelectedListener,
    AdapterView.OnItemSelectedListener {

    private val viewModel by viewModel {
        AirAndPollenViewModel(
            mdocAppHelper = MdocAppHelper.getInstance(),
            repository = AirAndPollenRepository(RestClient.getService())
        )
    }

    private val regionAdapter = RegionAdapter()
    private val infoAdapter = CompositeListAdapter().apply {
        addAdapter(AirAndPollenItemAdapter())
        addAdapter(AirAndPollenTitleAdapter())
    }

    override fun init(savedInstanceState: Bundle?) {
        // do nothing
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_air_and_pollen
    }

    override val navigationItem: NavigationItem = NavigationItem.AirAndPollen

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindVisibleInvisible(progressBar, viewModel.isInProgress)
        bindVisibleInvisible(error, viewModel.isError)
        bindVisibleInvisible(content, viewModel.isContentVisible)
        bindVisibleGone(pleaseSelectRegion, viewModel.showNoRegionSelectedMessage)
        viewModel.dates.observe(this) { dates ->
            if (dates.isNotEmpty()) {
                dailyTabs.removeAllTabs()
                dates.forEach { dateItem ->
                    dailyTabs.addTab(
                            dailyTabs.newTab().setText(dateItem.date.getName(requireContext()))
                    )
                }

                dailyTabs.visibility = View.VISIBLE
            } else {
                dailyTabs.visibility = View.GONE
            }
        }
        viewModel.selectedDate.observe(this) {
            val dates = viewModel.dates.get()
            val index = dates.indexOf(it)
            dailyTabs.getTabAt(index)?.select()
        }
        dailyTabs.addOnTabSelectedListener(this)
        regionSelector.adapter = regionAdapter
        viewModel.regionsForSelection.observe(this) {
            regionAdapter.items = it
        }
        viewModel.selectionIndex.observe(this) {
            regionSelector.setSelection(it)
        }
        regionSelector.onItemSelectedListener = this
        infoList.adapter = infoAdapter
        infoList.layoutManager = LinearLayoutManager(context)
        viewModel.infoList.observe(this) {
            infoListContainer.visibility = if (it.isNotEmpty()) View.VISIBLE else View.GONE
            infoAdapter.setItems(mutableListOf<Any>().apply {
                add(AirAndPollenTitle)
                addAll(it.sortedBy { it.type.getName(context) })
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        regionSelector.adapter = null
        infoList.adapter = null
    }

    override fun onTabSelected(tab: TabLayout.Tab) {
        val index = tab.position
        viewModel.selectedDate.set(
            viewModel.dates.get()[index]
        )
    }

    override fun onTabUnselected(tab: TabLayout.Tab) {
        // ignore
    }

    override fun onTabReselected(tab: TabLayout.Tab) {
        // ignore
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        viewModel.selectedRegion.set(regionAdapter.items[position].getRegion())
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        viewModel.selectedRegion.set(null)
    }

}
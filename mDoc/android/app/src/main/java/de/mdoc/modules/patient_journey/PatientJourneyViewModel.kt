package de.mdoc.modules.patient_journey

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.patient_journey.data.CarePlan
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import org.joda.time.DateTime

class PatientJourneyViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    var activeCarePlans: MutableLiveData<List<CarePlan>> =
            MutableLiveData(emptyList())

    var historyCarePlans: MutableLiveData<List<CarePlan>> =
            MutableLiveData(emptyList())
    init {
        getPatientJourneys()
        getPatientJourneysHistory()
    }

    private fun getPatientJourneys() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            try {
                isLoading.value = true
                activeCarePlans.value = mDocService.getCarePlans(category = QUERY,
                        periodEndGTE = DateTime.now().millis.toString())
                    .data?.list
                isShowContent.value = true
                isLoading.value = false

                isShowNoData.value = activeCarePlans.value!!.isEmpty()

            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowLoadingError.value = true
                isShowContent.value = false
            }
        }
    }
    private fun getPatientJourneysHistory() {
        viewModelScope.launch {
            try {
                historyCarePlans.value = mDocService.getCarePlans(category = QUERY,
                        periodEndLTE = DateTime.now().millis.toString()
                                                                )
                    .data?.list
            } catch (e: java.lang.Exception) {


            }
        }
    }

    companion object {
        const val QUERY = "patientJourney"
    }
}
package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by ema on 1/16/17.
 */

public class Answer implements Serializable {

    private String pollId;
    private String pollAssignId;
    private String pollQuestionId;
    private String userId;
    private String answerData;
    private String pollVoterId;
    private String id;
    private String pollVotingActionId;
    private ArrayList<String> selection;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public ArrayList<String> getSelection() {
        return selection;
    }

    public void setSelection(ArrayList<String> selection) {
        this.selection = selection;
    }


    public String getPollVotingActionId() {
        return pollVotingActionId;
    }

    public void setPollVotingActionId(String pollVotingActionId) {
        this.pollVotingActionId = pollVotingActionId;
    }

    public Answer(String answerData, ArrayList<String> selection) {
        this.answerData = answerData;
        this.selection = selection;
    }

    public Answer(String answerData, ArrayList<String> selection, String pollQuestionId) {
        this.answerData = answerData;
        this.selection = selection;
        this.pollQuestionId = pollQuestionId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getPollQuestionId() {
        return pollQuestionId;
    }

    public void setPollQuestionId(String pollQuestionId) {
        this.pollQuestionId = pollQuestionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getAnswerData() {
        return answerData;
    }

    public void setAnswerData(String answerData) {
        this.answerData = answerData;
    }

    public String getPollVoterId() {
        return pollVoterId;
    }

    public void setPollVoterId(String pollVoterId) {
        this.pollVoterId = pollVoterId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, pollAssignId);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }else if (this == obj) {
            return true;
        } else if (obj instanceof Answer) {
            Answer q = (Answer) obj;
            return this.pollQuestionId.equals(q.getPollQuestionId()) && this.answerData.equals(q.getAnswerData());
        }
        return false;
    }
}

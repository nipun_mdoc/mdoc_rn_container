package de.mdoc.modules.entertainment.fragments.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.entertainment.adapters.filter.MagazinesFilterCatagoriesAdapter
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterSharedViewModel
import de.mdoc.modules.medications.create_plan.data.ListItem
import kotlinx.android.synthetic.main.fragment_entertainment_filter.*

class MagazinesFilterFragment : DialogFragment() {

    private lateinit var activity: MdocActivity
    private lateinit var sharedViewModel: MagazineFilterSharedViewModel

    private var categories: MutableList<ListItem> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MdocActivity
        setStyle(STYLE_NO_TITLE, android.R.style.Theme_Light_NoTitleBar_Fullscreen)

        sharedViewModel = activity.run {
            ViewModelProviders.of(this).get(MagazineFilterSharedViewModel::class.java)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(de.mdoc.R.layout.fragment_entertainment_filter, null)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel.applyTempData()

        reloadData()

        val json = Gson().toJson(sharedViewModel.categoriesTemp.value)
        val listType = object : TypeToken<MutableList<ListItem>>() {}.type
        categories = Gson().fromJson<MutableList<ListItem>>(json, listType)

        rvCategories.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            adapter = MagazinesFilterCatagoriesAdapter(categories, sharedViewModel)
        }

        sharedViewModel.languagesTemp.observe(viewLifecycleOwner, Observer {
            val filtered = sharedViewModel.languagesTemp.value?.filter { it?.activatedByUser == true }
            val display = filtered?.joinToString { it -> "${it?.display}" }
            btnLanguages.text = display
        })

        setOnClickListener()
    }

    private fun reloadData() {
        val json = Gson().toJson(sharedViewModel.categoriesTemp.value)
        val listType = object : TypeToken<MutableList<ListItem>>() {}.type
        categories = Gson().fromJson<MutableList<ListItem>>(json, listType)

        rvCategories.apply {
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            adapter = MagazinesFilterCatagoriesAdapter(categories, sharedViewModel)
        }
    }

    private fun setOnClickListener() {
        btnLanguages.setOnClickListener {
            findNavController().navigate(R.id.action_entertainmentFilter_to_entertainmentFilterLanguages)
        }

        btnCloseFilter.setOnClickListener {
            dismiss()
        }

        btnApplyFilter.setOnClickListener {
            sharedViewModel.applyFilter()
            dismiss()
        }

        btnClearAll.setOnClickListener {
            sharedViewModel.clearAll()
            reloadData()
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setWindowAnimations(R.style.FilterDialogAnimation)
    }
}
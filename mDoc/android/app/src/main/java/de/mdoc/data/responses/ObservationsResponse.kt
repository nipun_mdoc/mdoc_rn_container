package de.mdoc.data.responses

import de.mdoc.data.create_bt_device.ObservationResponseData

data class ObservationsResponse(
        val data: List<ObservationResponseData>
)
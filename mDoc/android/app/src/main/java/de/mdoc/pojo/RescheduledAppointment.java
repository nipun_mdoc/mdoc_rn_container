package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RescheduledAppointment implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("appointmentDetails")
    @Expose
    private AppointmentDetails appointmentDetails;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("metadataNeeded")
    @Expose
    private boolean metadataNeeded;
    @SerializedName("metadataPopulated")
    @Expose
    private boolean metadataPopulated;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("start")
    @Expose
    private long start;
    @SerializedName("end")
    @Expose
    private long end;
    @SerializedName("online")
    @Expose
    private boolean online;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isMetadataNeeded() {
        return metadataNeeded;
    }

    public void setMetadataNeeded(boolean metadataNeeded) {
        this.metadataNeeded = metadataNeeded;
    }

    public boolean isMetadataPopulated() {
        return metadataPopulated;
    }

    public void setMetadataPopulated(boolean metadataPopulated) {
        this.metadataPopulated = metadataPopulated;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}

package de.mdoc.fragments.kiosk;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.pojo.WeatherResponse;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 10/31/17.
 */

public class KioskWeatherFragment extends MdocBaseFragment{

    @BindView(R.id.firstDateTv)
    TextView firstDateTv;
    @BindView(R.id.secondDateTv)
    TextView secondDateTv;
    @BindView(R.id.thirdDateTv)
    TextView thirdDateTv;

    @BindView(R.id.firstImage)
    ImageView firstImage;
    @BindView(R.id.secondImage)
    ImageView secondImageTv;
    @BindView(R.id.thirdImage)
    ImageView thirdImage;

    @BindView(R.id.firstTextTv)
    TextView firstTextTv;
    @BindView(R.id.secondTextTv)
    TextView secondTextTv;
    @BindView(R.id.thirdTextTv)
    TextView thirdTextTv;

    @BindView(R.id.weatherTv)
    TextView weatherTv;

    LoginActivity activity;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_kiosk_weather;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getNewLoginActivity();

        MdocManager.getInstance().getWeather(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if(response.isSuccessful()){

                    Picasso.get().load(getResId(response.body().getList().get(0).getWeather().get(0).getIcon())).into(firstImage);
                    Picasso.get().load(getResId(response.body().getList().get(1).getWeather().get(0).getIcon())).into(secondImageTv);
                    Picasso.get().load(getResId(response.body().getList().get(2).getWeather().get(0).getIcon())).into(thirdImage);

                    firstDateTv.setText(response.body().getList().get(0).getDt_txt());
                    secondDateTv.setText(response.body().getList().get(1).getDt_txt());
                    thirdDateTv.setText(response.body().getList().get(2).getDt_txt());

                    firstTextTv.setText(getTemperature(response.body().getList().get(0).getMain().getTemp()));
                    secondTextTv.setText(getTemperature(response.body().getList().get(1).getMain().getTemp()));
                    thirdTextTv.setText(getTemperature(response.body().getList().get(2).getMain().getTemp()));

                    weatherTv.setText(response.body().getCity().getName());
                }else{
                    Timber.w("getWeather %s", APIErrorKt.getErrorDetails(response));
                    MdocUtil.showToastLong(activity, ErrorUtilsKt.getErrorMessage(response));
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                Timber.w(t, "getWeather");
                MdocUtil.showToastLong(activity, t.getMessage());
            }
        });
    }

    private String getTemperature(float temp){
        return Math.round(temp) + "\u00B0";
    }

    private int getResId(String name){
        switch (name){
            case "01d" :
                return R.drawable.a01d;
            case "02d" :
                return R.drawable.a02d;
            case "03d" :
                return R.drawable.a03d;
            case "04d" :
                return R.drawable.a04d;
            case "09d" :
                return R.drawable.a09d;
            case "10d" :
                return R.drawable.a10d;
            case "11d" :
                return R.drawable.a11d;
            case "13d" :
                return R.drawable.a13d;
            case "50d" :
                return R.drawable.a50d;
            case "01n" :
                return R.drawable.a01d;
            case "02n" :
                return R.drawable.a02d;
            case "03n" :
                return R.drawable.a03d;
            case "04n" :
                return R.drawable.a04d;
            case "09n" :
                return R.drawable.a09d;
            case "10n" :
                return R.drawable.a10d;
            case "11n" :
                return R.drawable.a11d;
            case "13n" :
                return R.drawable.a13d;
            case "50n" :
                return R.drawable.a50d;
            default:
                return R.drawable.a01d;

        }
    }
}

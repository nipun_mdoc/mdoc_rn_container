package de.mdoc.modules.common.export

import android.content.Context
import android.os.Environment
import androidx.lifecycle.ViewModel
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.viewmodel.liveData
import okhttp3.ResponseBody
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.util.*

abstract class BaseExportViewModel<T>(val callback: ExportCallback) : ViewModel() {

    val isLoading = liveData(false)

    interface ExportCallback {
        fun onDownload(path: File, type: ExportType)
    }

    abstract fun download(request: T, type: ExportType, context: Context?)

    protected fun writeFile(type: ExportType, context: Context?, responseBody: ResponseBody) {
        context?.let {
            val path = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS)
            val file = File(path, "${Date().time}.${type}")

            val stream = FileOutputStream(file)
            try {
                stream.write(responseBody.bytes())
            } catch (ex: Exception) {
                Timber.d(ex)
            } finally {
                stream.close()
            }

            callback.onDownload(file, type)
        }
    }
}
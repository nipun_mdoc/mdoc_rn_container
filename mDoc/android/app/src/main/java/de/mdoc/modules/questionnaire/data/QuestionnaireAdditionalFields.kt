package de.mdoc.modules.questionnaire.data

import android.os.Parcelable
import de.mdoc.pojo.DiaryList
import kotlinx.android.parcel.Parcelize

@Parcelize
data class QuestionnaireAdditionalFields(
        var pollId: String? = null,
        var pollAssignId: String? = null,
        var pollEndMessage: String? = null,
        var pollVotingId: String? = null,
        var showFreeText: Boolean = false,
        var allowSharing: Boolean = false,
        var autoShare: Boolean = false,
        var diaryConfigId: String? = null,
        var isDiary: Boolean = false,
        var answerDetailsTotal: Int = 0,
        var answerDetailsCompleted: Int = 0,
        var questionnaireName: String? = null,
        var questionnaireType: String? = null,
        var isSendToKis: Boolean = false,
        var resultScreenTypeCode: String? = null,
        var shouldAssignQuestionnaire:Boolean=false,
        var diaryList: DiaryList? = null
): Parcelable
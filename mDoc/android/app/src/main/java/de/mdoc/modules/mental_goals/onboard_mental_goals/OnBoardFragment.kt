package de.mdoc.modules.mental_goals.onboard_mental_goals

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.util.handleOnBackPressed
import kotlinx.android.synthetic.main.fragment_onboard_mental_goal.*

class OnBoardFragment: MdocBaseFragment() {

    override fun setResourceId(): Int {
        return R.layout.fragment_onboard_mental_goal
    }

    override fun init(savedInstanceState: Bundle?) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewPager = pager_onboard
        val adapter = OnBoardPagerAdapter(childFragmentManager, viewPager)
        viewPager.adapter = adapter
        tabDots.setupWithViewPager(viewPager)
        handleOnBackPressed {
            if (viewPager.currentItem > 0) {
                viewPager.currentItem = viewPager.currentItem - 1
            }
            else {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)

                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)

                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }
}
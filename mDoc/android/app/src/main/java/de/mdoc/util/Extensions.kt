package de.mdoc.util

import java.util.*

fun Locale.isImperial(): Boolean {
    return when (country.toUpperCase()) {
        "US", "LR", "MM" -> true
        else -> false
    }
}
package de.mdoc.interfaces;

/**
 * Created by ema on 9/22/17.
 */

public interface ItemTouchHelperViewHolder {
    void onItemSelected();
    void onItemClear();
}

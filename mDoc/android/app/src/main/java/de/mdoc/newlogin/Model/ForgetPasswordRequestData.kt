package de.mdoc.newlogin.Model

class ForgetPasswordRequestData {
    var client_id: String? = null
    var client_secret: String? = null
    var friendlyName: String? = null
    var password: String? = null
    var totp: String? = null
    var username: String? = null
}
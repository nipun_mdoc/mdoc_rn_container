package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.MealPlan;

/**
 * Created by ema on 10/31/17.
 */

public class PublicMealResponse extends MdocResponse {

    private ArrayList<MealPlan> data;

    public ArrayList<MealPlan> getData() {
        return data;
    }

    public void setData(ArrayList<MealPlan> data) {
        this.data = data;
    }
}

package de.mdoc.modules.questionnaire.widget

import android.view.View
import androidx.lifecycle.LifecycleOwner
import de.mdoc.R
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.compositelist.SimpleViewGroupAdapter
import kotlinx.android.synthetic.main.dashboard_widget_questionnaire.view.*

class QuestionnaireWidgetBinding(
    private val lifecycleOwner: LifecycleOwner,
    private val view: View,
    private val viewModel: QuestionnaireWidgetViewModel,
    private val callback: Callback
) : LifecycleOwner by lifecycleOwner {

    interface Callback {

        fun openAllQuestionnaires()

    }

    private val adapter =
        SimpleViewGroupAdapter(view.answerDataLv, QuestionnaireWidgetItemAdapter())

    init {
        viewModel.loadData()
        if (view.context.resources.getBoolean(R.bool.has_questionnaire_widget)) {
            view.visibility = View.VISIBLE
        }
        view.findViewById<View>(R.id.questionnaireLl).setOnClickListener {
            callback.openAllQuestionnaires()
        }
        bindVisibleGone(view.progressBar, viewModel.isLoading)
        bindVisibleGone(view.answerDataLv, viewModel.isShowContent)
        bindVisibleGone(view.noQuestionnaires, viewModel.isShowNoQuestionnairesMessage)
        bindVisibleGone(view.loadingError, viewModel.isShowLoadingError)
        viewModel.content.observe(this, adapter::setItems)
    }

}
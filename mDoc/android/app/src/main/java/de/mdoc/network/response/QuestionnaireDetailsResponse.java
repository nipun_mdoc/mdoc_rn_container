package de.mdoc.network.response;


import de.mdoc.pojo.QuestionnaireDetailsData;

/**
 * Created by ema on 12/28/16.
 */

public class QuestionnaireDetailsResponse extends MdocResponse {

    private QuestionnaireDetailsData data;

    public QuestionnaireDetailsData getData() {
        return data;
    }

    public void setData(QuestionnaireDetailsData data) {
        this.data = data;
    }
}

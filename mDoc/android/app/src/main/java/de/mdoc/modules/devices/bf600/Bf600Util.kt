package de.mdoc.modules.devices.bf600

import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import org.joda.time.IllegalFieldValueException
import org.joda.time.LocalDateTime

fun parseUserList(deviceData: ArrayList<Byte>): ArrayList<Bf600User> {
    val usersList = ArrayList<Bf600User>()
    val users = deviceData.chunked(12)
    var userIndex = ""
    var yearOne = ""
    var yearTwo = ""
    var monthHex = ""
    var dayOfBirthHex = ""
    var heightHex = ""
    var gender = ""

    for (user in users) {
        for ((index, byte) in user.withIndex()) {
            when (index) {
                1  -> userIndex = String.format("%02X", byte)
                5  -> yearOne = String.format("%02X", byte)
                6  -> yearTwo = String.format("%02X", byte)
                7  -> monthHex = String.format("%02X", byte)
                8  -> dayOfBirthHex = String.format("%02X", byte)
                9  -> heightHex = String.format("%02X", byte)
                10 -> gender = String.format("%02X", byte)

                11 -> {
                    val activityLevel = String.format("%02X", byte)
                    val hexYear = yearTwo + yearOne
                    val yearParsed = java.lang.Long.parseLong(hexYear, 16)
                        .toInt()
                    val day = java.lang.Long.parseLong(dayOfBirthHex, 16)
                        .toInt()
                    val height = java.lang.Long.parseLong(heightHex, 16)
                        .toDouble()
                    val month = java.lang.Long.parseLong(monthHex, 16)
                        .toInt()
                    val birthDay = try {
                        LocalDateTime(yearParsed, month, day, 0, 0, 0).toDateTime()
                            .millis
                    } catch (e: IllegalFieldValueException) {
                        LocalDateTime.now()
                            .toDateTime()
                            .millis
                    }

                    usersList.add(Bf600User(userIndex, birthDay, height, gender.toInt(),
                            activityLevel.toInt()))
                }
            }
        }
    }
    return usersList
}

fun connectionTimeoutAction(fragment: Fragment) {
    if (fragment.isAdded) {
        Toast.makeText(fragment.context, fragment.resources.getText(R.string.bluetooth_timeout), Toast.LENGTH_LONG)
            .show()

        val isBackToInit = fragment.findNavController().popBackStack(R.id.fragmentInitializeBf600, true)
        if (!isBackToInit) {
            fragment.findNavController().popBackStack()
        }
    }
}


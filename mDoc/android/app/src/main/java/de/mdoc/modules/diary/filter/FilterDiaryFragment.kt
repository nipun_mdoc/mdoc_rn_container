package de.mdoc.modules.diary.filter

import android.app.DatePickerDialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window.FEATURE_NO_TITLE
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.diary.DiaryDashboardFragment
import de.mdoc.modules.diary.data.DiaryFilter
import de.mdoc.util.serializableArgument
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.filter_diary_fragment.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class FilterDiaryFragment: DialogFragment() {

    val adapter:FilterDiaryAdapted? = null
    lateinit var activity: MdocActivity
    var filter by serializableArgument<DiaryFilter>("diary_filter")
    var adapterItems: List<DiaryEntryType>? = null
    private val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    private val viewModel by viewModel {
        FilterDiaryViewModel()
    }
    private var parentFragment: DiaryDashboardFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        activity = getActivity() as MdocActivity

        setStyle(STYLE_NORMAL, android.R.style.Theme_Light_NoTitleBar_Fullscreen)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        dialog!!.requestWindowFeature(FEATURE_NO_TITLE)

        return inflater.inflate(R.layout.filter_diary_fragment, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parentFragment = (targetFragment as? DiaryDashboardFragment)
        initPreviousFilter()

        viewModel.from.observe(viewLifecycleOwner, Observer { from ->
            txt_fromDate.text = if (from != null) formatter.print(from) else "-"

            if (from != null && viewModel.to.value != null && from.isAfter(viewModel.to.value)) {
                viewModel.to.value = from.withHourOfDay(23)
                    .withMinuteOfHour(59)
                    .withSecondOfMinute(59)
            }
        })
        viewModel.to.observe(viewLifecycleOwner, Observer { to ->
            txt_toDate.text = if (to != null) formatter.print(to) else "-"

            if (to != null && viewModel.from.value != null && to.isBefore(viewModel.from.value)) {
                viewModel.from.value = to.withHourOfDay(0)
                    .withMinuteOfHour(0)
                    .withSecondOfMinute(0)
            }
        })

        rv_placeholder?.layoutManager = LinearLayoutManager(activity)
        val adapter = FilterDiaryAdapted(context)
        rv_placeholder?.adapter = adapter
        adapter.items = adapterItems ?: listOf()

        btnCloseFilter?.setOnClickListener {
            if (parentFragment?.viewModel?.diaryItems?.isSet() == true && parentFragment?.viewModel?.diaryItems?.get()?.isEmpty() == true) {
                clearFilter()
            }
            dismiss()
        }
        btnClearAll?.setOnClickListener {
            adapter.clearCheckboxSelection()
            viewModel.from.value = null
            viewModel.to.value = null
        }

        txt_filter?.setOnClickListener {
            adapter.getAdapterData()
        }

        txt_fromDate?.setOnClickListener {
            showDatePicker(viewModel.from)
        }

        txt_toDate?.setOnClickListener {
            showDatePicker(viewModel.to, false)
        }

        txt_filter?.setOnClickListener {
            val filter = viewModel.getFilter(categoriesAdapter(adapterItems))
            parentFragment?.setFilter(filter)

            if (parentFragment?.viewModel?.diaryItems?.isSet() == true && parentFragment?.viewModel?.diaryItems?.get()?.isEmpty() == true) {
                Toast.makeText(context, getString(R.string.diary_no_results), Toast.LENGTH_SHORT)
                    .show()
                clearFilter()
            }
            else {
                dismiss()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setWindowAnimations(
                R.style.FilterDialogAnimation
                                           )
    }

    private fun initPreviousFilter() {
        viewModel.from.value = filter.from
        viewModel.to.value = filter.to
        adapterItems?.forEach {
            it.isSelected = filter.categories?.contains(it.diaryId) == true
        }
        adapter?.notifyDataSetChanged()
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window?.setLayout(width, height)

            dialog.window?.setBackgroundDrawable(
                    ColorDrawable(
                            ContextCompat.getColor(
                                    activity,
                                    R.color.default_background)))
        }
    }

    private fun showDatePicker(dateTime: MutableLiveData<DateTime?>?, isFrom: Boolean = true) {
        val initialDate: DateTime = dateTime?.value ?: DateTime.now()
        val mYear = initialDate.year
        val mMonth = initialDate.monthOfYear - 1
        val mDay = initialDate.dayOfMonth
        val datePickerDialog = DatePickerDialog(activity, { _, year, month, dayOfMonth ->
            dateTime?.value = if (isFrom) {
                DateTime(year, month + 1, dayOfMonth, 0, 0)
            }
            else {
                DateTime(year, month + 1, dayOfMonth, 23, 59)
            }
        }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.maxDate = DateTime.now()
            .millis
        datePickerDialog.show()
    }

    private fun categoriesAdapter(input: List<DiaryEntryType>?): ArrayList<String> {
        val result = arrayListOf<String>()
        input?.forEach { item ->
            if (item.isSelected) {
                result.add(item.diaryId ?: "")
            }
        }
        return result
    }

    private fun clearFilter() {
        parentFragment?.setFilter(DiaryFilter(
                categories = null,
                from = null,
                to = null))
    }
}

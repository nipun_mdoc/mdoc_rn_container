package de.mdoc.modules.appointments.video_conference

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.appointments.video_conference.data.SingleAppointmentResponse
import de.mdoc.modules.appointments.video_conference.data.TokBoxResponse
import de.mdoc.service.IMdocService
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class VideoConferenceViewModel(private val mDocService: IMdocService): ViewModel() {

    fun getSessionIdApi(appointmentId: String, onSuccess: (String?) -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                val call = mDocService.getAppointmentById(appointmentId)
                call.enqueue(object : Callback<SingleAppointmentResponse> {
                    override fun onFailure(call: Call<SingleAppointmentResponse>, t: Throwable) {
                        onError(t)
                    }

                    override fun onResponse(call: Call<SingleAppointmentResponse>, response: Response<SingleAppointmentResponse>) {
                        val sessionId= response.body()?.data?.appointmentDetails?.tokboxSessionId
                        onSuccess(sessionId)
                    }
                })
            } catch (e: java.lang.Exception) {
                onError(e)
            }
        }
    }

    fun getTokBoxTokenApi(appointmentId: String, onSuccess: (String?) -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                val call = mDocService.getTokboxToken(appointmentId)
                call.enqueue(object : Callback<TokBoxResponse> {
                    override fun onResponse(call: Call<TokBoxResponse>, response: Response<TokBoxResponse>) {
                        val tokBoxData = response.body()?.data
                        onSuccess(tokBoxData?.tokboxToken)
                    }

                    override fun onFailure(call: Call<TokBoxResponse>, t: Throwable) {
                        onError(t)
                    }
                })
            } catch (e: java.lang.Exception) {
                onError(e)
            }
        }
    }


     fun joinAppointment(appointmentId: String, onSuccess: () -> Unit, onError: (String?) -> Unit)  {
            try {
                val call = mDocService.joinCall(appointmentId)
                call.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onError(t?.message?:"")
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        if (response != null) {
                            if (response.isSuccessful) {
                                onSuccess()
                            } else {
                                val jObjError = JSONObject(response.errorBody()?.string()?:"")
                                onError(jObjError.getString("message")?: "")
                            }
                        }
                    }
                })
            } catch (error: java.lang.Exception) {
                Timber.d(error)
                onError("")
            }
    }

    fun leaveAppointmentCall(appointmentId: String) {
        viewModelScope.launch {
            withContext(NonCancellable) {
                try {
                    mDocService.leaveCall(appointmentId)
                } catch (e: java.lang.Exception) { }
            }
        }
    }
}
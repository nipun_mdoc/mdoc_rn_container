package de.mdoc.modules.medications

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window.FEATURE_NO_TITLE
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.databinding.FragmentItemOverviewBinding
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.modules.medications.data.MedicationAdministration
import de.mdoc.modules.medications.medication_widget.MedicationWidgetViewModel
import kotlinx.android.synthetic.main.fragment_item_overview.*

class ItemOverviewFragment: DialogFragment(), CommonDialogFragment.OnButtonClickListener, View.OnClickListener {

    lateinit var activity: MdocActivity
    var medicationAdministration = MedicationAdministration()
    var medicationRequestId: String? = ""
    var isOnDemandCase: Boolean = false
    var parentViewModel: MedicationsViewModel? = null
    var medicationWidgetViewModel: MedicationWidgetViewModel? = null
    private var dialogBuilder: CommonDialogFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = getActivity() as MdocActivity
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogMaterial)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val description = resources.getString(R.string.med_plan_delete_description)
        val positive = resources.getString(R.string.med_plan_delete_confirm)
        val negative = resources.getString(R.string.med_plan_delete_cancel)

        dialogBuilder = CommonDialogFragment.Builder()
            .description(description)
            .setPositiveButton(positive)
            .setNegativeButton(negative)
            .setOnClickListener(this)
            .build()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
            .also {
                it.window?.setLayout(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.MATCH_PARENT
                                    )
                it.requestWindowFeature(FEATURE_NO_TITLE)
            }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        val binding: FragmentItemOverviewBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_item_overview, container, false)

        parentViewModel?.medicationAdministration?.value = medicationAdministration
        binding.lifecycleOwner = this
        binding.itemOverviewViewModel = parentViewModel
        binding.item = medicationAdministration
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        img_delete.setOnClickListener(this)
        img_close.setOnClickListener(this)

        parentViewModel?.getMedicationImage(medicationAdministration.medicationId ?: "") ?: dismiss()

        parentViewModel?.isMedicationDeleted?.observe(viewLifecycleOwner, Observer {
            if (it) {
                dismiss()
                parentViewModel?.isMedicationDeleted?.value = false
                // Refresh data for previous screen
                if (isOnDemandCase) {
                    parentViewModel?.getOnDemandEvents()
                }
                else {
                    parentViewModel?.getMedicationAdministration()
                    medicationWidgetViewModel?.getUpcomingMedicationAdministration()
                }
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionBarColor()
    }

    private fun setActionBarColor() {
        dialog?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        dialog?.window?.statusBarColor = ContextCompat.getColor(activity, R.color.colorPrimary)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        if (button == ButtonClicked.POSITIVE) {
            parentViewModel?.deleteMedicationRequest(medicationRequestId)
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            img_delete -> {
                dialogBuilder?.showNow(activity.supportFragmentManager, "")
            }

            img_close  -> {
                dismiss()
            }
        }
    }
}
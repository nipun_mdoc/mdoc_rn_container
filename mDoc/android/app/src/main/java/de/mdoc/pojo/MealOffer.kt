package de.mdoc.pojo

import android.content.res.Resources
import android.view.View
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.meal_plan.view_model.MealHelperClick
import java.io.Serializable
import java.util.ArrayList

/**
 * Created by ema on 10/17/17.
 */

class MealOffer : Serializable {
    var calories: Int = 0
    var description: String? = null
    private var name: String? = null
    var options = ArrayList<String>()
    var dessert: Boolean = false
    var isPremium: Boolean = false
    var _id: String? = null
    var soup: Boolean = false

    var isSelected: Boolean = false
    var isRated: Boolean = false
    var info: ArrayList<String>? = null

    fun getCaloriesText() : String{
        return "Kcal: " + calories
    }

    fun isGlutenFree(): Boolean {
        return options.contains(MdocConstants.GLUTEN_FREE)
    }

    fun isVegeterian(): Boolean {
        return options.contains(MdocConstants.VEGETARIAN)
    }

    fun isPig(): Boolean {
        return options.contains(MdocConstants.PIG)
    }

    fun isChicken(): Boolean {
        return options.contains(MdocConstants.CHICKEN)
    }

    fun isCow(): Boolean {
        return options.contains(MdocConstants.COW)
    }

    fun isFish(): Boolean {
        return options.contains(MdocConstants.FISH)
    }

    fun isKosher(): Boolean {
        return options.contains(MdocConstants.KOSHER)
    }

    fun showInfo(): Boolean {
        return options.contains(MdocConstants.INFO)
    }

    fun onInfoClick(view: View){
        MealHelperClick().showAditivesDialog(view, info ?: arrayListOf())
    }

    fun isSoup(): Boolean {
        return soup
    }

    fun isDessert(): Boolean {
        return dessert
    }

    fun getName (resource: Resources?): String {
        name?.let {
            return when (it.contains("Menu")) {
                true -> "${resource?.getString(R.string.menu)} ${it.drop(4)}"
                false -> it
            }
        }
        return resource?.getString(R.string.menu)?:""
    }
}

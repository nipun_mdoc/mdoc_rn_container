package de.mdoc.modules.booking.clinic_selection

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.network.request.AllClinicsRequest
import de.mdoc.network.response.SpeciltyData
import de.mdoc.pojo.ClinicDetails
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch

class ClinicSelectionViewModel(private val mDocService: IMdocService, private val context: Context?): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(true)
    var content: MutableLiveData<ArrayList<ClinicDetails>> = MutableLiveData(ArrayList())
    var query: MutableLiveData<String> = MutableLiveData()
    var content1: MutableLiveData<ArrayList<SpeciltyData>> = MutableLiveData(ArrayList())

    init {
        searchClinic()
    }

    private fun searchClinic() {
        viewModelScope.launch {
            try {
                getSpecialities()

            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
            }
        }
    }
    private suspend fun getSpecialities(){
        content1.value = mDocService.getSpecialities("").data
        viewModelScope.launch {
            try {
                searchClinicAsync()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
            }
        }
    }
    private suspend fun searchClinicAsync() {
        isLoading.value = true
        isShowNoDataMessage.value = false
        isShowLoadingError.value = false

        content.value = mDocService.searchClinics(AllClinicsRequest(getSearchQuery(), true)).data


        var i=0;
        var special: ArrayList<String> = ArrayList();
        for (item in (content.value as java.util.ArrayList<ClinicDetails>?)!!) {
            special.clear()
            for(item1 in item.specialities!!){
                var s : String = content1.value?.filter { it.code == item1 }?.get(0)!!.specialty
                special.add(s)
            }
            (content.value as java.util.ArrayList<ClinicDetails>?)!![i]?.specialitiesNames?.addAll(special)
            i += 1;
        }
        isLoading.value = false

        if (content.value.isNullOrEmpty()) {
            isShowNoDataMessage.value = true
            isShowContent.value = false
        }
        else {
            isShowContent.value = true
            isShowNoDataMessage.value = false
        }
    }

    private fun getSearchQuery(): String{
        return if (context?.resources?.getBoolean(R.bool.has_appointment_multi_clinic) == true){
            ""
        } else {
            MdocAppHelper.getInstance().clinicId
        }
    }
}
package de.mdoc.modules.appointments;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.jibestream.jmapandroidsdk.components.ActiveVenue;
import com.jibestream.jmapandroidsdk.components.Destination;
import com.jibestream.jmapandroidsdk.components.Floor;
import com.jibestream.jmapandroidsdk.components.JCore;
import com.jibestream.jmapandroidsdk.components.Map;
import com.jibestream.jmapandroidsdk.components.Waypoint;
import com.jibestream.jmapandroidsdk.jcontroller.JController;
import com.jibestream.jmapandroidsdk.oauth.JAuth;
import com.jibestream.jmapandroidsdk.v3.astar.PathPerFloor;
import com.jibestream.jmapandroidsdk.v3.elements.Element;
import com.jibestream.jmapandroidsdk.v3.elements.ElementMap;
import com.jibestream.jmapandroidsdk.v3.elements.Unit;
import com.jibestream.jmapandroidsdk.v3.main.EngineView;
import com.jibestream.jmapandroidsdk.v3.shapes.Image;
import com.jibestream.jmapandroidsdk.v3.styles.Style;
import com.jibestream.jmapandroidsdk.v3.utils.Ninegrid;

import java.util.ArrayList;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.pojo.FloorExtension;
import de.mdoc.util.ProgressDialog;

/**
 * Created by ema on 6/14/17.
 */

public class AppointmentMapFragment extends MdocFragment {

    public static final int CUSTOMER_ID = 39;
    public static final String HOST = "https://demo-api.jibestream.com";
    public static final String CLIENT_ID = "c97831ab-9ad9-480f-8d9f-de7a8049a027";
    public static final String CLIENT_SECRET = "ng8TnFhcsM2HUBQ8UhQUkiKaLaOn0NIfaVGE/VsiAIQ=";

    @BindView(R.id.engineView)
    EngineView engineView;
    @BindView(R.id.floorsSpn)
    Spinner floorsSpn;

    JCore jCore;
    JController jController;
    MdocActivity activity;
    ProgressDialog progressDialog;
    Floor[] floors;
    ArrayAdapter<FloorExtension> adapter;
    Destination[] destinations;
    Destination start;
    Destination end;
    ElementMap elementMap = null;
    FloorExtension previousItemSelected = null;
    String room;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_map;
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Therapy;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        room = getArguments().getString(MdocConstants.ROOM);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setCancelable(false);
        progressDialog.show();

        elementMap = new ElementMap();
        Image image = new Image(drawableToBitmap(getResources().getDrawable(R.drawable.map_person)), 50, 50, new Ninegrid(Ninegrid.MiddleMiddle));
        elementMap.setShape(image);

        initJCore();

    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }


    private void initJCore(){
        JAuth authentication = new JAuth(CLIENT_ID, CLIENT_SECRET);
        jCore = new JCore(authentication, activity, CUSTOMER_ID , HOST );

// Calls server to retrieve venue list
        jCore.getVenues(new JCore.OnVenueCollectionReadyCallback() {
            @Override
            public void onSuccess() {

                // Calls server to populate venue. First venue in the list is selected by default.
                jCore.populateVenue(jCore.getVenueList().getAll()[0], new JCore.OnVenueReadyCallback() {
                    @Override
                    public void onSuccess(ActiveVenue activeVenue) {
                        // Calls server to populate the building within the active venue
                        jCore.populateBuildingInVenue(activeVenue, activeVenue.getBuildings().getDefault(), new JCore.OnBuildingReadyCallback() {
                            @Override
                            public void onSuccess(final ActiveVenue activeVenue) {
                                // Initialize JController once venue is fully populated
                                jController = new JController(activity, activeVenue);
                                jController.setStage(engineView);
                                floors = activeVenue.getBuildings().getDefault().getFloors().getAll();
                                final ArrayList<FloorExtension> floorExtensions = new ArrayList<FloorExtension>();

                                for(Floor f : floors){
                                    floorExtensions.add(new FloorExtension(f));
                                }
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        adapter = new ArrayAdapter<FloorExtension>(getActivity(),R.layout.simple_spinner_item, floorExtensions);
                                        floorsSpn.setAdapter(adapter);
                                    }
                                });
                                // Parse then show map based. Default building and default floor are selected.
                                jController.showMap(activeVenue.getBuildings().getDefault().getDefaultFloor().getMap(), new JController.OnMapShownCallback() {
                                    @Override
                                    public void onMapShown() {
                                        // Show icons and labels.
                                        jController.camera.addScale(0.5f);
                                        showMap(activeVenue);

                                        floorsSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                                                jController.showMap(((FloorExtension)floorsSpn.getSelectedItem()).getFloor().getMap(), new JController.OnMapShownCallback() {
                                                    @Override
                                                    public void onMapShown() {
                                                        ActiveVenue activeVenue = jController.getActiveVenue();
//                                                        if(previousItemSelected != null) {
//                                                            jController.removeComponent(elementMap, previousItemSelected.getFloor().getMap());
//                                                        }
//                                                        previousItemSelected = ((FloorExtension) floorsSpn.getSelectedItem());
                                                        showMap(activeVenue);
                                                        progressDialog.dismiss();
                                                    }

                                                    @Override
                                                    public void onError(String message) {

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> adapterView) {

                                            }
                                        });

                                        progressDialog.dismiss();
                                    }
                                    @Override
                                    public void onError(String message) {
                                        progressDialog.dismiss();
                                      //  MdocUtil.showToastShort(activity, message);
                                    }
                                });
                            }
                            @Override
                            public void onError(String message) {
                                progressDialog.dismiss();
                              //  MdocUtil.showToastShort(activity, message);
                            }
                        });
                    }
                    @Override
                    public void onError(String message) {
                        progressDialog.dismiss();
                       // MdocUtil.showToastShort(activity, message);
                    }
                });
            }
            @Override
            public void onError(String message) {
                progressDialog.dismiss();
//                MdocUtil.showToastShort(activity, message);
            }
        });
    }


    private void showMap(final ActiveVenue activeVenue){

        jController.showAllAmenities();
        jController.showAllPathTypes();
        jController.showAllTextMapLabels();
        jController.showAllImageMapLabels();


        jController.setOnElementClickListener(new JController.OnElementClickListener() {
            @Override
            public void onElementClick(Element element) {
                if(element instanceof Unit){
                    Unit unit = (Unit)element;

                    if(unit.getDestinations() != null && unit.getDestinations().length != 0) {
                        start = unit.getDestinations()[0];
                        setWaypath(activeVenue, start, end);
                    }
                }
            }
        });

        for (Map map : jController.getActiveVenue().getMaps().getAll()) {
            for (Unit unit : jController.getUnitsFromMap(map)) {
                for (Destination destination : unit.getDestinations()) {
                    if (destination.getDisplayMode() == 0) {
                        // do nothing
                        jController.hideUnitContents(unit);
                    } else if (destination.getDisplayMode() == 1) {
                        // display textObservation only
                        jController.addTextToUnit(destination.getName(), unit);
                    } else if (destination.getDisplayMode() == 2) {
                        // display image only
                        jController.addImageToUnit(destination.getName(), unit);
                    }
                }
            }
        }



        destinations = activeVenue.getDestinations().getByBuilding(activeVenue.getBuildings().getDefault());
        if(room == null){
            end = destinations[destinations.length-1];
            setWaypath(activeVenue, start != null ? start : destinations[0] , end);
            return;
        }
        switch (room){
            case "8797":
                end = destinations[destinations.length-1];
                break;
            case "8798":
                end = destinations[destinations.length-3];
                break;
            case "8799":
                end = destinations[destinations.length-5];
                break;
            default:
                end = destinations[destinations.length-1];
        }

        setWaypath(activeVenue, start != null ? start : destinations[0] , end);
    }

    private void setWaypath(ActiveVenue activeVenue, Destination start, Destination end){
        Waypoint startPoint = activeVenue.getMaps().getWaypointsByDestination(start)[0];
        Waypoint endPoint = activeVenue.getMaps().getWaypointsByDestination(end)[0];

        PathPerFloor[] pathPerFloorsObject =  jController.wayfindBetweenWaypoints(startPoint, endPoint, 100); //.getActiveVenue().wayfind(waypointCollection[10], waypointCollection[30]);

        Style style = new Style(Paint.Style.FILL);
        style.paintFill.setColor(Color.GREEN);
        jController.drawWayfindingPath(pathPerFloorsObject, style);

        PathPerFloor pathPerFloor = pathPerFloorsObject[0];
        int mapId = pathPerFloor.mapId;

        jController.addComponent(elementMap, activeVenue.getMaps().getById(mapId) , startPoint); //((FloorExtension)floorsSpn.getSelectedItem()).getFloor().getMap()

    }
}

package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.text.format.DateFormat
import de.mdoc.interfaces.BleResponseListener
import de.mdoc.util.MdocUtil
import de.mdoc.util.isImperial
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.abs


class As87BleWrapper(private val context: WeakReference<Context>, deviceAddress: String) : Handler.Callback {

    private var bluetoothDevice: BluetoothDevice? = null
    private val bleHandler: Handler
    private var myBleCallback: MyBleCallback? = null
    private var counter = 0

    private var characteristicWrite: BluetoothGattCharacteristic? = null
    private var acquiredMeasurementResponseArrayList: ArrayList<Byte>? = null
    private var mainCalendar: GregorianCalendar = GregorianCalendar()
    private var mainDate = Date()
    private var bleListener: WeakReference<BleResponseListener>? = null
    private var as87Counter = 1
    private var isFirstDescriptor: Boolean = false

    var commandCounter = 0

    fun setBleResponseListener(bleListener: WeakReference<BleResponseListener>) {
        this.bleListener = bleListener
    }

    fun removeBleResponseListener () {
        this.bleListener = null
    }

    init {
        val handlerThread = HandlerThread("BleThread")
        handlerThread.start()
        bleHandler = Handler(handlerThread.looper, this)
        myBleCallback = MyBleCallback(WeakReference<As87BleWrapper>(this))
        context.get()?.let {
            bluetoothDevice = getBluetoothDevice(it, deviceAddress)
        }
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {
        val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }

    fun connect(autoConnect: Boolean) {
        bleHandler.obtainMessage(MSG_CONNECT, autoConnect).sendToTarget()
    }

    fun disconnect() {
        bleHandler.obtainMessage(MSG_DISCONNECT, bluetoothDevice?.address).sendToTarget()

    }

    override fun handleMessage(message: Message): Boolean {
        when (message.what) {
            MSG_CONNECT -> doConnect(message.obj as Boolean)
            MSG_CONNECTED -> (message.obj as BluetoothGatt).discoverServices()
            MSG_DISCONNECT -> (message.obj as BluetoothGatt).disconnect()
            MSG_DISCONNECTED -> (message.obj as BluetoothGatt).close()
            MSG_SERVICES_DISCOVERED -> {
                writeIndicationDescriptor(message.obj as BluetoothGatt, BEURER_AS87_NOTIFY_1)
            }
            MSG_DESCRIPTOR_WRITE -> {

                if (isFirstDescriptor) {
                    writeIndicationDescriptor(message.obj as BluetoothGatt, BEURER_AS87_NOTIFY_2)
                    isFirstDescriptor = false
                } else {
                    if (acquiredMeasurementResponseArrayList == null) {
                        acquiredMeasurementResponseArrayList = ArrayList()
                    }

                    characteristicWrite = (message.obj as BluetoothGatt)
                            .getService(UUID.fromString(BEURER_AS87_UNKNOWN_SERVICE))
                            .getCharacteristic(UUID.fromString(BEURER_AS87_NOTIFY_1))

                    characteristicWrite?.value = as87CommandSetTime()
                    (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)
                    Timber.d("MSG_DESCRIPTOR_WRITE: Time Comand")
                }
            }

            MSG_CHARACTERISTIC_CHANGE -> {
                commandCounter++
                if (characteristicWrite != null) {

                    if (commandCounter == 1) {
                        characteristicWrite?.value = as87CommandSetTimeZone()
                        (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)
                        Timber.d("MSG_CHARACTERISTIC_CHANGE: Time Zone Comand")

                    } else if (commandCounter == 2) {
                        characteristicWrite?.value = as87Command(0)
                        (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)
                        Timber.d("MSG_CHARACTERISTIC_CHANGE: First Comand")

                    } else {
                        val command = as87Command(as87Counter)
                        as87Counter++

                        if (as87Counter < 16) {
                            characteristicWrite?.value = command
                            (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)
                        }
                    }
                }
            }
            MSG_LAST_PACKET -> {
                bleListener?.get()?.dataFromDeviceCallback(acquiredMeasurementResponseArrayList, (message.obj as BluetoothGatt))
                (message.obj as BluetoothGatt).disconnect()
            }
        }
        return true
    }

    private fun doConnect(autoConnect: Boolean) {
        context.get()?.let {
            bluetoothDevice?.connectGatt(it, autoConnect, myBleCallback)
        }
    }


    private inner class MyBleCallback(val wrapper: WeakReference<As87BleWrapper>) : BluetoothGattCallback() {
        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                when (newState) {
                    BluetoothGatt.STATE_CONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                        wrapper.get()?.bleHandler?.obtainMessage(MSG_CONNECTED, gatt)?.sendToTarget()

                    }
                    BluetoothGatt.STATE_DISCONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_DISCONNECTED $gatt $status")
                        wrapper.get()?.bleHandler?.obtainMessage(MSG_DISCONNECTED, gatt)?.sendToTarget()
                    }
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Timber.d("onServicesDiscovered: $gatt")
                wrapper.get()?.bleHandler?.obtainMessage(MSG_SERVICES_DISCOVERED, gatt)?.sendToTarget()
            }

        }

        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                wrapper.get()?.bleHandler?.obtainMessage(MSG_DESCRIPTOR_WRITE, gatt)?.sendToTarget()

            } else {
                Timber.d("onDescriptorWrite: FAILED $gatt")
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
            /*  get response of service in raw data, concentrate and show
                in human readable format.
             */
            super.onCharacteristicChanged(gatt, characteristic)
            Timber.d("onCharacteristicChanged: %s", Arrays.toString(characteristic.value))
            Timber.d("DeviceOutputOnCharacteristicChanged: %s", wrapper.get()?.acquiredMeasurementResponseArrayList)

            //device is AS87
            if (TIME_SUCCESS.contentEquals(characteristic.value)) {

                wrapper.get()?.bleHandler?.obtainMessage(MSG_CHARACTERISTIC_CHANGE, gatt)?.sendToTarget()
                Timber.d("onCharacteristicChanged: Time SUCC")

            } else if (ZONE_SUCCESS.contentEquals(characteristic.value)) {

                wrapper.get()?.bleHandler?.obtainMessage(MSG_CHARACTERISTIC_CHANGE, gatt)?.sendToTarget()
                Timber.d("onCharacteristicChanged: Time ZONE SUCC")

            } else if (AS87_LAST_COMMAND.contentEquals(characteristic.value)) {

                if (wrapper.get()?.as87Counter == 15) {
                    wrapper.get()?.bleListener?.get()?.dataFromDeviceCallback(wrapper.get()?.acquiredMeasurementResponseArrayList, gatt)
                    gatt.disconnect()
                }

                wrapper.get()?.counter = 0
                wrapper.get()?.bleHandler?.obtainMessage(MSG_CHARACTERISTIC_CHANGE, gatt)?.sendToTarget()

            } else if (AS87_NO_DATA.contentEquals(characteristic.value)) {
                wrapper.get()?.bleListener?.get()?.dataFromDeviceCallback(wrapper.get()?.acquiredMeasurementResponseArrayList, gatt)
                gatt.disconnect()

            } else {

                if (wrapper.get()?.counter == 0) {
                    for (i in characteristic.value.indices) {
                        if (i < 12) {
                            wrapper.get()?.acquiredMeasurementResponseArrayList!!.add(characteristic.value[i])
                        }
                    }
                }
                wrapper.get()?.let {
                    it.counter++
                }
            }

        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            Timber.i("onCharacteristicRead $characteristic")
            val value = characteristic?.value ?: byteArrayOf()
            val v = String(value)
            Timber.i("onCharacteristicRead Value: $v")
        }

    }

    companion object {
        private const val MSG_CONNECT = 10
        private const val MSG_CONNECTED = 20
        private const val MSG_DISCONNECT = 30
        private const val MSG_DISCONNECTED = 40
        private const val MSG_SERVICES_DISCOVERED = 50
        private const val MSG_DESCRIPTOR_WRITE = 60
        private const val MSG_CHARACTERISTIC_CHANGE = 70
        private const val MSG_LAST_PACKET = 80

        private const val BEURER_AS87_UNKNOWN_SERVICE = "D0A2FF00-2996-D38B-E214-86515DF5A1DF"
        private const val BEURER_AS87_CHARACTERISTIC_NOTIFICATION_DESC = "00002902-0000-1000-8000-00805f9b34fb"
        private const val BEURER_AS87_NOTIFY_1 = "7905ff01-b5ce-4e99-a40f-4b1e122d00d0"
        private const val BEURER_AS87_NOTIFY_2 = "7905ff02-b5ce-4e99-a40f-4b1e122d00d0"

        private val AS87_LAST_COMMAND = byteArrayOf(0xDE.toByte(), 0x02, 0x01, 0xED.toByte())
        private val AS87_NO_DATA = byteArrayOf(0xDE.toByte(), 0x02, 0x01, 0x06)
        private val TIME_SUCCESS = byteArrayOf(0xDE.toByte(), 0x01, 0x01, 0xED.toByte())
        private val ZONE_SUCCESS = byteArrayOf(0xDE.toByte(), 0x01, 0x02, 0xED.toByte())

    }

    private fun as87Command(int: Int): ByteArray {
        val as87Command = ByteArray(10)

        mainCalendar.time = mainDate

        if (int != 0) {
            mainCalendar.add(Calendar.DATE, -int)
        }

        val year = MdocUtil.getParsedDate("yyyy", mainCalendar).toInt()
        val month = MdocUtil.getParsedDate("MM", mainCalendar).toInt()
        val day = MdocUtil.getParsedDate("dd", mainCalendar).toInt()

        var hexYear = String.format("%02X", year)
        val hexMonth = String.format("%02X", month)
        val hexDay = String.format("%02X", day)

        while (hexYear.length < 4) {
            hexYear = """0$hexYear"""
        }
        val numOne = hexYear.substring(0, 2)
        val numTwo = hexYear.substring(2, 4)

        val byte1 = Integer.parseInt(numOne, 16)
        val byte2 = Integer.parseInt(numTwo, 16)
        val byte3 = Integer.parseInt(hexMonth, 16)
        val byte4 = Integer.parseInt(hexDay, 16)

        as87Command[0] = 0xBE.toByte()
        as87Command[1] = 0x02
        as87Command[2] = 0x01
        as87Command[3] = 0xFE.toByte()

        as87Command[4] = byte1.toByte()
        as87Command[5] = byte2.toByte()
        as87Command[6] = byte3.toByte()
        as87Command[7] = byte4.toByte()

        as87Command[8] = 0x00.toByte()
        as87Command[9] = 0x78.toByte()

        return as87Command
    }

    private fun writeIndicationDescriptor(gattObject: BluetoothGatt, characteristic: String) {
        isFirstDescriptor = true
        val characteristicNotificationOne = (gattObject).getService(UUID.fromString(BEURER_AS87_UNKNOWN_SERVICE))
                .getCharacteristic(UUID.fromString(characteristic))

        characteristicNotificationOne.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

        (gattObject).setCharacteristicNotification(characteristicNotificationOne, true)

        val descriptorOne = characteristicNotificationOne.getDescriptor(
                UUID.fromString(BEURER_AS87_CHARACTERISTIC_NOTIFICATION_DESC))
        descriptorOne.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        gattObject.writeDescriptor(descriptorOne)
    }

    private fun as87CommandSetTime(): ByteArray {
        val as87Command = ByteArray(16)

        mainCalendar.time = mainDate


        val year = MdocUtil.getParsedDate("yyyy", mainCalendar).toInt()
        val month = MdocUtil.getParsedDate("MM", mainCalendar).toInt()
        val day = MdocUtil.getParsedDate("dd", mainCalendar).toInt()

        var hexYear = String.format("%02X", year)
        val hexMonth = String.format("%02X", month)
        val hexDay = String.format("%02X", day)

        while (hexYear.length < 4) {
            hexYear = """0$hexYear"""
        }
        val numOne = hexYear.substring(0, 2)
        val numTwo = hexYear.substring(2, 4)

        val byte1 = Integer.parseInt(numOne, 16)
        val byte2 = Integer.parseInt(numTwo, 16)
        val byte3 = Integer.parseInt(hexMonth, 16)
        val byte4 = Integer.parseInt(hexDay, 16)

        as87Command[0] = 0xBE.toByte()
        as87Command[1] = 0x01
        as87Command[2] = 0x01
        as87Command[3] = 0xFE.toByte()

        if (Locale.getDefault().isImperial()) {
            as87Command[4] = 0x01
        } else {
            as87Command[4] = 0x00
        }

        if (DateFormat.is24HourFormat(context.get())) {
            as87Command[5] = 0x00
        } else {
            as87Command[5] = 0x01
        }

        val mTimeZone = mainCalendar.timeZone

        val mGMTOffset = mTimeZone.rawOffset

        val offset = TimeUnit.HOURS.convert(mGMTOffset.toLong(), TimeUnit.MILLISECONDS)

        val offsetCalc = abs(offset) * 2
        val hexOffset = String.format("%02X", offsetCalc)
        val byte6 = Integer.parseInt(hexOffset, 16)
        as87Command[6] = byte6.toByte() //home time zone
        as87Command[7] = byte6.toByte() //current time zone


        as87Command[8] = byte1.toByte()
        as87Command[9] = byte2.toByte()
        as87Command[10] = byte3.toByte()
        as87Command[11] = byte4.toByte()

        val dayOfWeek = mainCalendar.get(Calendar.DAY_OF_WEEK)
        val hour = MdocUtil.getParsedDate("HH", mainCalendar).toInt()
        val minute = MdocUtil.getParsedDate("mm", mainCalendar).toInt()
        val second = MdocUtil.getParsedDate("ss", mainCalendar).toInt()

        val hexDayOfWeek = String.format("%02X", dayOfWeek - 1)
        val hexHour = String.format("%02X", hour)
        val hexMinute = String.format("%02X", minute)
        val hexSecond = String.format("%02X", second)

        val byte12 = Integer.parseInt(hexDayOfWeek, 16)
        val byte13 = Integer.parseInt(hexHour, 16)
        val byte14 = Integer.parseInt(hexMinute, 16)
        val byte15 = Integer.parseInt(hexSecond, 16)

        as87Command[12] = byte12.toByte()
        as87Command[13] = byte13.toByte()
        as87Command[14] = byte14.toByte()
        as87Command[15] = byte15.toByte()


        return as87Command
    }

    private fun as87CommandSetTimeZone(): ByteArray {
        val as87Command = ByteArray(14)

        mainCalendar.time = mainDate

        val year = MdocUtil.getParsedDate("yyyy", mainCalendar).toInt()
        val month = MdocUtil.getParsedDate("MM", mainCalendar).toInt()
        val day = MdocUtil.getParsedDate("dd", mainCalendar).toInt()

        var hexYear = String.format("%02X", year)
        val hexMonth = String.format("%02X", month)
        val hexDay = String.format("%02X", day)

        while (hexYear.length < 4) {
            hexYear = """0$hexYear"""
        }
        val numOne = hexYear.substring(0, 2)
        val numTwo = hexYear.substring(2, 4)

        val byte1 = Integer.parseInt(numOne, 16)
        val byte2 = Integer.parseInt(numTwo, 16)
        val byte3 = Integer.parseInt(hexMonth, 16)
        val byte4 = Integer.parseInt(hexDay, 16)

        as87Command[0] = 0xBE.toByte()
        as87Command[1] = 0x01
        as87Command[2] = 0x02
        as87Command[3] = 0xFE.toByte()

        as87Command[4] = byte1.toByte()
        as87Command[5] = byte2.toByte()
        as87Command[6] = byte3.toByte()
        as87Command[7] = byte4.toByte()


        val dayOfWeek = mainCalendar.get(Calendar.DAY_OF_WEEK)
        val hexDayOfWeek = String.format("%02X", dayOfWeek - 1)
        val byte5 = Integer.parseInt(hexDayOfWeek, 16)

        as87Command[8] = byte5.toByte()


        val mTimeZone = mainCalendar.timeZone

        val mGMTOffset = mTimeZone.rawOffset

        val offset = TimeUnit.HOURS.convert(mGMTOffset.toLong(), TimeUnit.MILLISECONDS)

        val offsetCalc = abs(offset) * 2
        val hexOffset = String.format("%02X", offsetCalc)
        val byte6 = Integer.parseInt(hexOffset, 16)

        as87Command[9] = byte6.toByte() //current time zone

        val hour = MdocUtil.getParsedDate("HH", mainCalendar).toInt()
        val minute = MdocUtil.getParsedDate("mm", mainCalendar).toInt()
        val second = MdocUtil.getParsedDate("ss", mainCalendar).toInt()

        val hexHour = String.format("%02X", hour)
        val hexMinute = String.format("%02X", minute)
        val hexSecond = String.format("%02X", second)

        val byte9 = Integer.parseInt(hexHour, 16)
        val byte10 = Integer.parseInt(hexMinute, 16)
        val byte11 = Integer.parseInt(hexSecond, 16)

        as87Command[10] = byte9.toByte()
        as87Command[11] = byte10.toByte()
        as87Command[12] = byte11.toByte()

        if (DateFormat.is24HourFormat(context.get())) {
            as87Command[13] = 0x00
        } else {
            as87Command[13] = 0x01
        }
        return as87Command
    }
}
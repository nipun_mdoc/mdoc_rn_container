package de.mdoc.newlogin.Model

class UpdatePasswordResponseData(
    val code: String,
    val `data`: DataUpdatePassword,
    val message: String,
    val timestamp: Long
)

data class DataUpdatePassword(
    val email: String,
    val message: String,
    val passwordResetToken: String,
    val scope: String,
    val status: String
)
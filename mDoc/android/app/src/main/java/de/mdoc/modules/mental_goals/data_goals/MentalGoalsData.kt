package de.mdoc.modules.mental_goals.data_goals

data class MentalGoalsData(
        val description: String,
        val id: String,
        val ifThenPlans: ArrayList<IfThenPlan>,
        val items: ArrayList<ActionItem>,
        val title: String,
        var dueDate:Long,
        val status:String,
        val coachCreated:Boolean
)

package de.mdoc.modules.patient_journey

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentPatientJourneyBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel

class PatientJourneyFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.PatientJourney
    private val patientJourneyViewModel by viewModel {
        PatientJourneyViewModel(
                RestClient.getService())
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentPatientJourneyBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_patient_journey, container, false)

        binding.lifecycleOwner = this
        binding.patientJourneyViewModel = patientJourneyViewModel
        binding.handler = PatientJourneyHandler()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        patientJourneyViewModel.historyCarePlans.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            setHasOptionsMenu(it.isNotEmpty() && resources.getBoolean(R.bool.has_patient_journey_history))
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_patient_journey)
        val item = menu.findItem(R.id.journeyHistory)
        if (resources.getBoolean(R.bool.has_patient_journey_history)) {
            item.isVisible = true
        }
        super.onCreateOptionsMenu(menu, inflater)

    }
}
package de.mdoc.modules.mental_goals.goal_creation

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import kotlinx.android.synthetic.main.fragment_stop_and_think.*

class StopAndThinkFragment: MdocFragment() {
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_stop_and_think
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        progressBar?.progress = 4
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }

        txt_next?.setOnClickListener {
            val action = StopAndThinkFragmentDirections.actionStopAndThinkFragmentToIfThenFragment()
            findNavController().navigate(action)
        }
        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}
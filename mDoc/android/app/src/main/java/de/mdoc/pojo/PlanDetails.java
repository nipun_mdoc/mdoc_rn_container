package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/20/17.
 */

public class PlanDetails implements Serializable {

    private long checkIn;
    private long checkOut;

    public long getCheckIn() {
        return checkIn;
    }

    public void setCheckIn(long checkIn) {
        this.checkIn = checkIn;
    }

    public long getCheckOut() {
        return checkOut;
    }

    public void setCheckOut(long checkOut) {
        this.checkOut = checkOut;
    }
}

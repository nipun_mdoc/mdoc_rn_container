package de.mdoc.viewmodel

fun <T> liveData(): ValueLiveData<T> {
    return ValueLiveData<T>()
}

fun <T> liveData(initialValue: T): ValueLiveData<T> {
    return ValueLiveData<T>().apply {
        set(initialValue)
    }
}

fun ValueLiveData<Boolean>.toggle() {
    set(get().not())
}

fun <T, R> ValueLiveData<T>.map(function: (T) -> R): ValueLiveData<R> {
    val result = liveData<R>()
    this.observeForever {
        if (it != null) {
            result.set(function.invoke(it.value))
        }
    }
    return result
}

fun ValueLiveData<Boolean>.not(): ValueLiveData<Boolean> {
    return map { it.not() }
}

fun <T> ValueLiveData<List<T>>.isEmpty(): ValueLiveData<Boolean> {
    return map { it.isEmpty() }
}

fun <T> ValueLiveData<List<T>>.isNotEmpty(): ValueLiveData<Boolean> {
    return map { it.isNotEmpty() }
}

fun <A, B, R> combine(
    a: ValueLiveData<A>,
    b: ValueLiveData<B>,
    function: (A, B) -> R
): ValueLiveData<R> {
    val result = liveData<R>()
    observeAll(a, b) {
        val aValue = a.value
        val bValue = b.value
        if (aValue != null && bValue != null) {
            result.set(function.invoke(aValue.value, bValue.value))
        }
    }
    return result
}

fun <A, B, C, R> combine(
    a: ValueLiveData<A>,
    b: ValueLiveData<B>,
    c: ValueLiveData<C>,
    function: (A, B, C) -> R
): ValueLiveData<R> {
    val result = liveData<R>()
    observeAll(a, b, c) {
        val aValue = a.value
        val bValue = b.value
        val cValue = c.value
        if (aValue != null && bValue != null && cValue != null) {
            result.set(function.invoke(aValue.value, bValue.value, cValue.value))
        }
    }
    return result
}

private fun observeAll(vararg liveDataList: ValueLiveData<*>, callback: () -> Unit) {
    liveDataList.forEach { liveData ->
        liveData.observe {
            callback.invoke()
        }
    }
}

fun <T> ValueLiveData<out List<T>>.doWithItemAt(index: Int, action: (T) -> Unit) {
    val current = value
    if (current != null) {
        current.value.getOrNull(index)?.let(action)
    }
}

fun <T> ValueLiveData<T>.filter(filter: (T) -> Boolean): ValueLiveData<T> {
    return liveData<T>().also { result ->
        observe {
            if (filter(it)) {
                result.set(it)
            }
        }
    }
}

fun ValueLiveData<Boolean>.filterTrue(): ValueLiveData<Boolean> {
    return filter { it }
}

fun ValueLiveData<Boolean>.filterFalse(): ValueLiveData<Boolean> {
    return filter { it.not() }
}
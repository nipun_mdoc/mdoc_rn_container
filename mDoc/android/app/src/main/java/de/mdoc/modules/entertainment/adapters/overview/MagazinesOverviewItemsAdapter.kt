package de.mdoc.modules.entertainment.adapters.overview

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.entertainment.fragments.pager.MagazinesPagerFragment
import de.mdoc.pojo.entertainment.MediaResponseDto
import kotlinx.android.synthetic.main.entertainment_magazines_child_item.view.*
import java.text.SimpleDateFormat

class MagazinesOverviewItemsAdapter(private val children : Array<MediaResponseDto>?)
    : RecyclerView.Adapter<MagazinesOverviewItemsAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {

        val v =  LayoutInflater.from(parent.context)
                .inflate(R.layout.entertainment_magazines_child_item,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        if (children != null) {
            return children.size
        }
        return 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(children?.get(position))
    }

    private fun getFormattedDate (date: String?): String {
        return when (date) {
            null -> ""
            else -> {
                val formatter = SimpleDateFormat("yyyy-mm-dd")
                val parsedDate = formatter.parse(date)
                SimpleDateFormat("dd.mm.yyyy").format(parsedDate)
            }
        }
    }

    private fun getFormattedLanguage (language: String?): String {
        return when (language) {
            null -> ""
            else -> language.toUpperCase()
        }
    }


    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView), View.OnClickListener{
        private val textView : TextView = itemView.child_textView
        private val imgMagazineCover: ImageView = itemView.imgMagazineCover
        private val txtDate: TextView = itemView.txtMagazinesDate
        private val emptyView: View = itemView.magazinesEmptyView
        private val progress: ProgressBar = itemView.progress
        private val txtRead: TextView = itemView.txtRead

        private var item: MediaResponseDto? = null

        init {
            imgMagazineCover.setOnClickListener(this)
        }

        fun bind (item: MediaResponseDto?){
            this.item = item

            val placeholder =  ColorDrawable(ContextCompat.getColor(imgMagazineCover.context, R.color.colorGray97))
            val options= RequestOptions()
                    .placeholder(placeholder)

            emptyView.visibility = View.GONE
            progress.visibility = View.VISIBLE
            Glide.with(imgMagazineCover.context)
                    .setDefaultRequestOptions(options)
                    .load(item?.coverURL)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            emptyView.visibility = View.VISIBLE
                            progress.visibility = View.GONE
                            txtRead.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imgMagazineCover)

            // Set magazine name
            textView.text = item?.magazineName

            // Set date and language
            val date  = getFormattedDate(item?.publicationDate)
            var language = getFormattedLanguage(item?.language)
            txtDate.text = "$date - $language"

            // Read
            when (item?.readingHistory?.read == true) {
                true -> {txtRead.visibility = View.VISIBLE}
                else -> {txtRead.visibility = View.GONE}
            }
        }

        override fun onClick(view: View?) {
            val bundle = Bundle()
            bundle.putString(MdocConstants.MAGAZINE, Gson().toJson(item))
            bundle.putInt(MdocConstants.MAGAZINE_PAGE_NUMBER, item?.readingHistory?.pageNumber?:0)
            view?.findNavController()?.navigate(R.id.action_entertainmentOverview_to_entertainmentMagazine, bundle)
        }
    }
}
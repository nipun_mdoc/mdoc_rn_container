package de.mdoc.modules.booking.master_data

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.data.login_meta_data.Address
import de.mdoc.data.requests.MetaDataRequest
import de.mdoc.databinding.FragmentMasterDataBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.booking.BookAppointmentFragmentDirections
import de.mdoc.network.RestClient
import de.mdoc.pojo.CodingItem
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_master_data.*
import java.util.*

class MasterDataFragment: NewBaseFragment() {
    override val navigationItem: NavigationItem = NavigationItem.BookAppointment
    lateinit var activity: MdocActivity
    val calendar: Calendar = Calendar.getInstance()
    private var titleSelected: String = ""
    private var genderSelected: String = ""
    private var countrySelected: String = ""
    private var federalStateSelected: String = ""
    private var countryCode: String = ""

    private val args: MasterDataFragmentArgs by navArgs()

    private val masterDataViewModel by viewModel {
        MasterDataViewModel(RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity
        val binding: FragmentMasterDataBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_master_data, container, false)
        binding.masterDataViewModel = masterDataViewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupOnClickListeners()

        calendar.add(Calendar.DAY_OF_MONTH, -1)

        txt_birth_date?.setText(MdocUtil.getParsedDate(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar))

        masterDataViewModel.titleList.observe(viewLifecycleOwner, androidx.lifecycle.Observer { titleList ->
            initSpinner(titleList, spinner_title) { titleSelected = it }
        })
        masterDataViewModel.genderList.observe(viewLifecycleOwner, androidx.lifecycle.Observer { genderList ->
            initSpinner(genderList, spinner_gender) {
                genderSelected = it
            }
        })
        masterDataViewModel.countryList.observe(viewLifecycleOwner, androidx.lifecycle.Observer { countryList ->
            initSpinner(countryList, spinner_country) { countrySelected = it }
            for (country in countryList.indices) {
                if (countryList[country].code.equals("DE")) {
                    spinner_country.setSelection(country)
                    showFederalState()
                    break
                }
            }
        })
        masterDataViewModel.federalStateList.observe(viewLifecycleOwner,
                androidx.lifecycle.Observer { federalStateList ->
                    initSpinner(federalStateList, spinner_federal_state) { federalStateSelected = it }
                    for (selection in federalStateList.indices) {
                        if (federalStateList[selection].code.equals("NONE")) {
                            spinner_federal_state.setSelection(selection)
                            break
                        }
                    }
                })
    }

    private fun initSpinner(items: ArrayList<CodingItem>, spinner: AppCompatSpinner, onClick: (String) -> Unit) {
        if (items.isNotEmpty()) {
            if (items[0].display != "" && spinner != spinner_federal_state) {
                items.add(0, CodingItem("", "", getActivity()?.resources?.getString(R.string.no_selection)))
            }
            val countrySpinnerAdapter = de.mdoc.adapters.SpinnerAdapter(context!!, items)
            spinner.adapter = countrySpinnerAdapter

            spinner.onItemSelectedListener =
                    object: AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(p0: AdapterView<*>?) {
                        }

                        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                            if (spinner == spinner_gender) {
                                onClick(items.getOrNull(p2)?.code ?: "")
                            }
                            else {
                                onClick(items.getOrNull(p2)?.display ?: "")
                            }
                            if (spinner == spinner_country) {
                                if (!items.getOrNull(p2)?.code.equals("DE")) {
                                    hideFederalState()
                                    federalStateSelected = ""
                                }
                                else {
                                    showFederalState()
                                }
                                countryCode = items.getOrNull(p2)?.code.toString()
                            }
                        }
                    }
        }
    }

    private fun setupOnClickListeners() {
        txt_birth_date?.setOnClickListener {
            showDatePicker()
        }

        btn_send_data?.setOnClickListener {
            if (validateFields()) {
                masterDataViewModel.postMasterData(request = createRequestObject(), onSuccess = {
                    val action =  BookAppointmentFragmentDirections.actionConfirmBooking(args.appointment, args.bookingAdditionalFields, args.physicianName)
                    findNavController().navigate(action)

                }, onError = {
                })
            }
            else {
                MdocUtil.showToastShort(activity, resources.getString(R.string.fill_all_fields))
            }
        }
    }

    private fun validateFields(): Boolean {
        return !(txt_birth_date?.text.isNullOrEmpty()
                || genderSelected.isEmpty()
                || genderSelected == getActivity()?.resources?.getString(R.string.no_selection))
    }

    private fun createRequestObject(): MetaDataRequest {
        val request = MetaDataRequest()
        request.title = titleSelected
        request.born = calendar.timeInMillis
        request.gender = genderSelected
        request.federalState = federalStateSelected
        request.clinicId = MdocAppHelper.getInstance()
            .clinicId
        request.phone = et_phone?.text.toString()

        request.address = Address(
                city = et_city?.text.toString(),
                country = countrySelected,
                countryCode = countryCode,
                houseNumber = et_number.text.toString(),
                postalCode = et_postal.text.toString(),
                street = et_street.text.toString()
                                 )
        return request
    }

    private fun showDatePicker() {
        val currentCalendar: Calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(activity, { _, year, month, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            if (calendar.timeInMillis > currentCalendar.timeInMillis) {
                calendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR))
                calendar.set(Calendar.MONTH, currentCalendar.get(Calendar.MONTH))
                calendar.set(Calendar.DAY_OF_MONTH, currentCalendar.get(Calendar.DAY_OF_MONTH))
            }
            txt_birth_date?.setText(MdocUtil.getParsedDate(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar))
        }, mYear, mMonth, mDay)
        val now = Calendar.getInstance()
        now.add(Calendar.DAY_OF_MONTH, -1)
        datePickerDialog.datePicker.maxDate = now.timeInMillis
        datePickerDialog.show()
    }

    private fun showFederalState() {
        txt_federal_state.visibility = View.VISIBLE
        spinner_federal_state.visibility = View.VISIBLE
    }

    private fun hideFederalState() {
        txt_federal_state.visibility = View.GONE
        spinner_federal_state.visibility = View.GONE
    }
}



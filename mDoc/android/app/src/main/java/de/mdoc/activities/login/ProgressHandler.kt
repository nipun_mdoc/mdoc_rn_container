package de.mdoc.activities.login

import android.app.Dialog
import android.content.Context
import de.mdoc.util.ProgressDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.lang.Exception
import java.util.concurrent.TimeUnit

class ProgressHandler(
    private val context: Context
) {

    private val progressSubject = BehaviorSubject.createDefault(false)
    private val disposables = CompositeDisposable()
    private var progressDialog: Dialog? = null

    fun showProgress() {
        Timber.i("showProgress")
        progressSubject.onNext(true)
    }

    fun hideProgress() {
        Timber.i("hideProgress")
        progressSubject.onNext(false)
    }

    init {
        progressSubject.throttleLatest(1000, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                setProgressStatus(it)
            }.addTo(disposables)
    }

    private fun setProgressStatus(inProgress: Boolean) {
        Timber.i("Progress=$inProgress")
        try {
            if (inProgress) {
                progressDialog?.dismiss()
                progressDialog = ProgressDialog(context).apply {
                    setCancelable(false)
                    show()
                }
            } else {
                if(progressDialog?.isShowing!!)
                    progressDialog?.dismiss()
                progressDialog = null
            }
        }catch (e: Exception){
        }

    }

    fun recycle() {
        disposables.dispose()
        progressDialog?.dismiss()
        progressDialog = null
    }

}
package de.mdoc.util.views

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.textview.MaterialTextView
import de.mdoc.util.ColorUtil


class MdocClearTextView : MaterialTextView {

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    constructor(context: Context) : super(context) {
        init()
    }

    private fun init() {
        // Set text color
        this.setTextColor(ColorUtil.contrastColor(resources))
    }
}
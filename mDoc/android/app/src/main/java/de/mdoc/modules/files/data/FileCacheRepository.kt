package de.mdoc.modules.files.data

import androidx.collection.LruCache
import de.mdoc.pojo.CodingData

object FileCacheRepository {

    private val cache = LruCache<String, CodingData>(10)

    fun storeCategoryCoding(systemId: String, data: CodingData) {
        cache.put(systemId, data)
    }

    fun getCategoryCoding(systemId: String): CodingData? {
        return cache.get(systemId)
    }

}
package de.mdoc.modules.devices.available_devices

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import de.mdoc.R
import de.mdoc.data.configuration.ConfigurationDetailsMultiValue
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.devices.adapters.DynamicMedicalAvailableAdapter
import de.mdoc.storage.AppPersistence.deviceConfiguration
import de.mdoc.storage.AppPersistence.fhirDevicesData
import kotlinx.android.synthetic.main.fragment_medical_choose.*

class MedicalChooseFragment : MdocBaseFragment(){

    lateinit var configuration: ConfigurationDetailsMultiValue

    override fun setResourceId(): Int {
        return R.layout.fragment_medical_choose    }

    override fun init(savedInstanceState: Bundle?) {
        configuration = deviceConfiguration
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
    }

    private fun setupAdapter(){
        context?.let {
            val adapterItems=fhirDevicesData?: emptyList()
            rv_available_medical.layoutManager = GridLayoutManager(it, 2)
            rv_available_medical?.adapter= DynamicMedicalAvailableAdapter(it, adapterItems, configuration)
        }
    }
}
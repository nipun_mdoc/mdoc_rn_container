package de.mdoc.data.questionnaire

import com.google.gson.annotations.SerializedName

data class Option(
        val key: String,
        val ordinal: Int,
        val pollExternalOrder: Int,
        @SerializedName("value") val value: Map<String, String>
)
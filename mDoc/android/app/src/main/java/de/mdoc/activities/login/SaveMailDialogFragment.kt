package de.mdoc.activities.login

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import de.mdoc.R
import de.mdoc.network.response.LoginResponse
import kotlinx.android.synthetic.main.dialog_save_mail.*
import retrofit2.Response

class SaveMailDialogFragment(private val dialogCallback: DialogSaveCallback,
                             private var email: String? = null,
                             private val resp: Response<LoginResponse>): DialogFragment() {

    interface DialogSaveCallback {
        fun saveMail(mail: String?, resp: Response<LoginResponse>, dialog : Dialog?)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        return inflater.inflate(R.layout.dialog_save_mail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)
        emailStatus?.text = if (email.isNullOrEmpty()) {
            resources.getString(R.string.login_email_skip)
        }
        else {
            resources.getString(R.string.login_email_saved, email.toString())
        }
        emailStatus.text
        okBtn?.setOnClickListener {
            dismiss()
            //Email is set to null because we don't need in this case api call for saving email anymore
            email = null
            dialogCallback.saveMail(email, resp, null)
        }
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)

        super.onResume()
    }
}

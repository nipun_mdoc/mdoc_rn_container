package de.mdoc.modules.entertainment.data

import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaReadingHistoryResponseDto_
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaStatisticResponseDto_

data class OverviewAndBookmarks(val overview: ResponseSearchResultsMediaStatisticResponseDto_? = null, val bookmarks: ResponseSearchResultsMediaReadingHistoryResponseDto_? = null)

fun OverviewAndBookmarks.isCategoriesEmpty(): Boolean {
    return overview?.data?.list.isNullOrEmpty()
}

fun OverviewAndBookmarks.isBookmarksEmpty (): Boolean {
    return bookmarks?.data?.list.isNullOrEmpty()
}
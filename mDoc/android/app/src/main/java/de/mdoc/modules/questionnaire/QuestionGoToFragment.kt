package de.mdoc.modules.questionnaire

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.util.intArgument
import de.mdoc.util.stringArgument
import kotlinx.android.synthetic.main.fragment_question_go_to.*

class QuestionGoToFragment: BottomSheetDialogFragment(), View.OnClickListener {

    private var questionIndexInSection by intArgument("questionIndexInSection",0)
    private var questionIndexInSectionLastSeen by intArgument("questionIndexInSectionLastSeen",0)
    private var firstUnfilledSectionName by stringArgument("sectionNameFirstUnfilled", "")
    private var sectionNameLastSeen by stringArgument("sectionNameLastSeen", "")
    private var firstUnfilledQuestionIndex by intArgument("firstUnfilledQuestionIndex", 0)
    private var lastSeenIndex by intArgument("lastSeenIndex", 0)

    companion object {
        fun <T> newInstance(questionIndexInSection: Int, firstUnfilledSectionName : String, questionIndexInSectionLastSeen: Int, sectionNameLastSeen : String,firstUnfilledQuestionIndex : Int, lastSeenIndex : Int, target: T): QuestionGoToFragment
                where T : Fragment {
            return QuestionGoToFragment().also {
                it.setTargetFragment(target, 0)
                it.questionIndexInSection = questionIndexInSection
                it.questionIndexInSectionLastSeen = questionIndexInSectionLastSeen
                it.firstUnfilledSectionName = firstUnfilledSectionName
                it.sectionNameLastSeen = sectionNameLastSeen
                it.firstUnfilledQuestionIndex = firstUnfilledQuestionIndex
                it.lastSeenIndex = lastSeenIndex
            }
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_question_go_to, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        isCancelable = false
        btnGoToBeginning.setOnClickListener(this)
        btnGoToFirstOpen.setOnClickListener(this)
        btnGoToLastSession.setOnClickListener(this)

        prepareUI()
    }

    private fun prepareUI () {
        val firstOpenText = getString(R.string.questionnaire_go_to_first_open_question, questionIndexInSection + 1, firstUnfilledSectionName)
        btnGoToFirstOpen.text = firstOpenText

        // For explanation check -> ArrayListExtendedQuestionExt.getIndexForLastSeen()
        if (questionIndexInSectionLastSeen == -1) {
            btnGoToLastSession.visibility = View.GONE

            val goToDescriptionText = getString(R.string.questionnaire_go_to_description, questionIndexInSection + 1, firstUnfilledSectionName)
            txtGoToDescription.text = goToDescriptionText

        } else {
            val lastSessionText = getString(R.string.questionnaire_go_to_last_session_question, questionIndexInSectionLastSeen + 1, sectionNameLastSeen)
            btnGoToLastSession.text = lastSessionText

            val goToDescriptionText = getString(R.string.questionnaire_go_to_description, questionIndexInSectionLastSeen + 1, sectionNameLastSeen)
            txtGoToDescription.text = goToDescriptionText
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            btnGoToBeginning -> {
                (targetFragment as QuestionFragment).setQuestionUi(0)
                dismiss()
            }

            btnGoToFirstOpen -> {
                (targetFragment as QuestionFragment).setQuestionUi(firstUnfilledQuestionIndex)
                dismiss()
            }

            btnGoToLastSession -> {
                (targetFragment as QuestionFragment).setQuestionUi(lastSeenIndex)
                dismiss()
            }
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        (targetFragment as QuestionFragment).activity.onBackPressed()
    }
}
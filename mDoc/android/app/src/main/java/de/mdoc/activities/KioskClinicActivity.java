package de.mdoc.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.adapters.AutocompleteClinicAdapter;
import de.mdoc.components.AutocompleteText;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.SearchClinicResponse;
import de.mdoc.pojo.ClinicDetails;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class KioskClinicActivity extends MdocBaseActivity {

    @BindView(R.id.clinicEdt)
    AutocompleteText clinicEdt;
    @BindView(R.id.clinicErrorTv)
    TextView clinicErrorTv;

    ArrayList<ClinicDetails> clinicDetails = new ArrayList<>();
    ClinicDetails cd = new ClinicDetails();

    AutocompleteClinicAdapter adapter;

    @Override
    public int getResourceId() {
        return R.layout.activity_kiosk_clinic;
    }

    @Override
    public void init(Bundle savedInstanceState) {

        adapter = new AutocompleteClinicAdapter(this, R.layout.autocomplete_clinic_item, clinicDetails);
        clinicEdt.setAdapter(adapter);
        searchClinic("");
    }

    private void searchClinic(String query) {

        MdocManager.searchClinicKiosk(query, new Callback<SearchClinicResponse>() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onResponse(Call<SearchClinicResponse> call, Response<SearchClinicResponse> response) {
                if (response.isSuccessful()) {
                    clinicDetails.clear();
                    clinicDetails.addAll(response.body().getData());
                    adapter.notifyDataSetChanged();
                    clinicEdt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            cd = adapter.getItem(position);
                            if (cd != null) {
                                cd.getAddress();
                            }
                        }
                    });
                    clinicEdt.setOnTouchListener((arg0, arg1) -> {
                        clinicEdt.showDropDown();
                        return false;
                    });
                } else {
                    MdocUtil.showToastShort(KioskClinicActivity.this, ErrorUtilsKt.getErrorMessage(response));
                    Timber.w("searchClinic %s", APIErrorKt.getErrorDetails(response));
                }
            }

            @Override
            public void onFailure(retrofit2.Call<SearchClinicResponse> call, Throwable throwable) {
                Timber.w(throwable, "searchClinic");
                MdocUtil.showToastShort(KioskClinicActivity.this, throwable.getMessage());
            }
        });
    }

    private void navigateToLogin() {
        Intent loginIntent = new Intent(KioskClinicActivity.this, LoginActivity.class);
        KioskClinicActivity.this.startActivity(loginIntent);
        KioskClinicActivity.this.finish();
    }

    @OnClick(R.id.nextBtn)
    public void onNextBtnClick() {
        if (!isError()) {
            MdocAppHelper.getInstance().setClinicId(cd.getClinicId());
            MdocAppHelper.getInstance().setClinicLatitude(cd.getLatitude());
            MdocAppHelper.getInstance().setClinicLongitude(cd.getLongitude());
            navigateToLogin();
        }
    }

    private boolean isError() {
        boolean isError = false;
        removeError(clinicEdt, clinicErrorTv);
        if (clinicEdt.getText().toString().isEmpty()) {
            setError(clinicEdt, clinicErrorTv, getString(R.string.clinic_required));
            isError = true;

        } else if (cd == null) {
            setError(clinicEdt, clinicErrorTv, getString(R.string.clinic_select));
            isError = true;

        } else if (cd.getName() == null || !cd.getName().equals(clinicEdt.getText().toString())) {
            setError(clinicEdt, clinicErrorTv, getString(R.string.clinic_select));
            isError = true;
        }
        return isError;
    }

    protected void removeError(EditText edt, TextView errorTv) {
        edt.setBackground(getResources().getDrawable(R.drawable.login_edt_background));
        if (errorTv != null) {
            errorTv.setVisibility(View.INVISIBLE);
        } else {
            edt.setTextColor(getResources().getColor(R.color.text_color));
        }
    }

    protected void setError(EditText edt, TextView errorTv, String message) {
        edt.setBackground(getResources().getDrawable(R.drawable.lust_rectangle));
        if (errorTv != null) {
            errorTv.setVisibility(View.VISIBLE);
            errorTv.setText(message);
        } else {
            edt.setTextColor(getResources().getColor(R.color.lust));
        }
    }
}

package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ImedOneData {

    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("departments")
    @Expose
    private ArrayList<ImedOneDepartment> departments = null;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ImedOneDepartment> getDepartments() {
        return departments;
    }

    public void setDepartments(ArrayList<ImedOneDepartment> departments) {
        this.departments = departments;
    }
}

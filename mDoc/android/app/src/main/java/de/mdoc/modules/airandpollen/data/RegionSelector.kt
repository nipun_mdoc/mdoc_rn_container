package de.mdoc.modules.airandpollen.data

import android.content.Context
import de.mdoc.R

sealed class RegionSelector {

    abstract fun getName(context: Context): String

    abstract fun getRegion(): Region?

    class NoSelection : RegionSelector() {
        override fun getName(context: Context): String {
            return context.getString(R.string.region_please_select)
        }

        override fun getRegion(): Region? {
            return null
        }
    }

    class RegionSelection(
        private val region: Region
    ) : RegionSelector() {
        override fun getName(context: Context): String {
            return region.name
        }

        override fun getRegion(): Region? {
            return region
        }
    }

}
package de.mdoc.modules.diary

import de.mdoc.pojo.DiaryList

interface DiaryAdapterCallback {

    fun onDeleteFileRequest(item: DiaryList)

    fun onEditFileRequest(item: DiaryList)

    fun onViewClickListner(item: DiaryList,boolean: Boolean)
}
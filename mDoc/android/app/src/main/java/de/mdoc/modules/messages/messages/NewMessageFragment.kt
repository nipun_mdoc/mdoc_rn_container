package de.mdoc.modules.messages.messages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.databinding.FragmentNewMessageBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.util.showSnackbar
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_new_message.*

class NewMessageFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Messages
    private val messagesViewModel by viewModel {
        MessagesViewModel(
                RestClient.getService()
                         )
    }
    lateinit var activity: MdocActivity
    val args: MessagesFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentNewMessageBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_new_message, container, false)
        binding.lifecycleOwner = this
        binding.messagesHandler = MessagesHandler()
        binding.messagesViewModel = messagesViewModel
        activity = getActivity() as MdocActivity
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleButtonState()

        messagesViewModel.letterbox.value = args.additionalFields.letterbox
        messagesViewModel.letterboxTitle.value = args.additionalFields.letterboxTitle
        messagesViewModel.recipient.value = args.additionalFields.recipient

        messagesViewModel.getCodingReason()
        messagesViewModel.reasonList.observe(viewLifecycleOwner, Observer {
            if (!it.isNullOrEmpty()) {
                val spinnerAdapter = context?.let { context -> SpinnerAdapter(context, it) }
                reasonSpinner?.adapter = spinnerAdapter

                reasonSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        messagesViewModel.reason.value = it.getOrNull(p2)?.code
                        messagesViewModel.reasonTitle.value = it.getOrNull(p2)?.display
                        handleButtonState()
                    }
                }
            }
        })

        messagesViewModel.description.observe(viewLifecycleOwner, Observer {
            handleButtonState()
        })

        messagesViewModel.hasError.observe(viewLifecycleOwner, Observer {
            if (it) {
                showSnackbar(getString(R.string.messages_send_message_error), view)
            }
        })

        messagesViewModel.messageSent.observe(viewLifecycleOwner, Observer {
            if (it) {
                showSnackbar(getString(R.string.messages_message_sent), view)
                findNavController().navigate(R.id.navigation_messages)
            }
        })
    }

    private fun handleButtonState() {
        btn_send_message?.isEnabled =
                (messagesViewModel.reason.value?.isNotEmpty() == true && messagesViewModel.description.value?.isNotEmpty() == true)
    }
}
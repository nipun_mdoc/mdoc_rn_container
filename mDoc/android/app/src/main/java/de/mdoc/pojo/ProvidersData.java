package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AdisMulabdic on 9/7/17.
 */

public class ProvidersData {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("data")
    @Expose
    private DataProvider data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DataProvider getData() {
        return data;
    }

    public void setData(DataProvider data) {
        this.data = data;
    }
}


package de.mdoc.modules.dashboard.welcome_widget

import de.mdoc.util.MdocAppHelper

class WelcomeWidgetHandler {
    fun onDismissClicked(viewModel: WelcomeWidgetViewModel) {
        viewModel.shouldShowContent.value = false
        MdocAppHelper.getInstance()
            .isClosed = true
    }
}
package de.mdoc.pojo.digitalsignature

/**
 * Created by Trenser on 30/12/20.
 */


data class SignatureSignResponse(
    val code: String,
    val `data`: String,
    val message: String,
    val timestamp: Long
)
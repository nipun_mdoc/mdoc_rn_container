package de.mdoc.modules.questionnaire.answer_validation

abstract class AbstractValidator {

    open fun isValidAnswer(): Boolean {
        return true
    }
}
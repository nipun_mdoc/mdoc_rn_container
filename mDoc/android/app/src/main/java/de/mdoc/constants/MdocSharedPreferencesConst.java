package de.mdoc.constants;

import de.mdoc.util.MdocAppHelper;

/**
 * Created by ema on 4/6/17.
 */

public class MdocSharedPreferencesConst {


    public static final String SHARED_PREFS_KEY = MdocAppHelper.class.getCanonicalName();
    public static final String USER_AUTH = "USER_AUTH";
    public static final String PASSWORD = "password";
    public static final String LOGIN_TIME = "login_time";
    public static final String EMAIL = "email";
    public static final String PATIENT_ID = "patient_id";
    public static final String USER_ID = "user_id";
    public static final String USER_LAST_NAME = "user_last_name";
    public static final String IS_CLOSED_WELCOME_MESSAGE = "is_closed";
    public static final String USER_TYPE = "user_type";
    public static final String LANGUAGE = "language";
    public static final String CLINIC_ID = "clinic_id";
    public static final String CLINIC_NAME = "clinic_name";
    public static final String DEPARTMENT = "department";
    public static final String CASE_ID = "case_id";
    public static final String EXTERNAL_CASE_ID = "external_case_id";

    public static final String USERNAME = "username";

    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String REFRESH_TOKEN_NO_HEADER = "refresh_token_no_header";

    public static final String CHECKLIST_ORDER = "checklist_order";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String IS_PREMIUM_PATIENT = "is_premium_patient";
    public static final String CHILD_ACCOUNT = "child_account";

    public static final String USER_FIRST_NAME = "user_first_name";
    public static final String FIRST_TIME = "is_first_time";

    public static final String SELECTED_POLLEN_REGION_ID = "selected_pollen_region_id";
    public static final String SHOW_GOALS_ONBORDING = "show_goals_onbording";
    public static final String BIOMETRIC_PROMPT = "biometric_prompt";
    public static final String IS_OPT_IN = "is_opt_in";
    public static final String DIARY_DISCLAIMER = "diary_disclaimer";
    public static final String VITALS_DISCLAIMER = "vitals_disclaimer";
    public static final String QUESTIONNAIRE_DISCLAIMER = "questionnaire_disclaimer";
    public static final String CHECKLIST_DISCLAIMER = "checklist_disclaimer";
    public static final String EMERGENCY_INFO = "EMERGENCY_INFO";
    public static final String PATIENT_FILE_UPLOAD = "patient_file_upload";
    public static final String PRINT_THERAPY_INFO = "PRINT_THERAPY_INFO";
}

package de.mdoc.modules.my_stay.widget

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.joda.time.format.DateTimeFormat

class MyStayWidgetViewModel(
        private val mDocService: IMdocService

) : ViewModel() {
    private val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy")

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    var isShowWidgetInModule = MutableLiveData(false)
    var clinicTitle = MutableLiveData("")
    var subTitle = MutableLiveData("")
    var caseId = MutableLiveData("")
    var checkIn = MutableLiveData("")
    var checkOut = MutableLiveData("")
    var clinic = MutableLiveData("")
    var room = MutableLiveData("")
    var responsibleDoctor = MutableLiveData("")
    var doctorImage = MutableLiveData("")

    init {
        viewModelScope.launch {
            try {
                networkCallsAsync()
            } catch (e: Exception) {
                isLoading.value = false
                isShowLoadingError.value = true
            }
        }
    }

    private suspend fun networkCallsAsync() = coroutineScope {
        isLoading.value = true

        val clinicResponse =
                mDocService.coroutineGetClinicByCase(MdocAppHelper.getInstance().caseId).data

        MdocAppHelper.getInstance().checkInDate = clinicResponse.checkIn
        if(clinicResponse.isCgmPatient()){
            MdocAppHelper.getInstance().checkOutDate = clinicResponse.checkOutActual ?: 0
        }else {
            MdocAppHelper.getInstance().checkOutDate = clinicResponse.checkOutActual
                    ?: clinicResponse.checkOut
        }

        clinicTitle.value = clinicResponse.clinicName
        subTitle.value = clinicResponse.speciality
        caseId.value = clinicResponse.caseId

        if(MdocAppHelper.getInstance().checkInDate == 0L){
            checkIn.value = ""
        }else{
            checkIn.value = dateFormat.print(MdocAppHelper.getInstance().checkInDate)
        }

        if(MdocAppHelper.getInstance().checkOutDate == 0L){
            checkOut.value = ""
        }else{
            checkOut.value = dateFormat.print(MdocAppHelper.getInstance().checkOutDate)
        }
        clinic.value = clinicResponse.department
        room.value = clinicResponse.room
        responsibleDoctor.value = clinicResponse.physician
        isLoading.value = false
    }

    fun onCardClick(view: View) {
        if (!isShowWidgetInModule.value!!) {
            view.findNavController().navigate(R.id.navigation_my_stay)
        }
    }
}
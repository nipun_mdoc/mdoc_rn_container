package de.mdoc.adapters;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.exercise.ExercisesFragment;
import de.mdoc.network.interceptors.HeaderInterceptor;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.pojo.CarePlanDetailsResponses;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;

public class ExerciseAdapterNew extends RecyclerView.Adapter<ExerciesViewHolderNew> implements Filterable {

    private ArrayList<CarePlanDetailsResponses> exercises;
    private ArrayList<CarePlanDetailsResponses> filtered;

    MdocActivity activity;
    Picasso picasso;
    private String carePlanId;
    private ProgressDialog progressDialog;
    ExercisesFragment fragment;

    public String getCarePlanId() {
        return carePlanId;
    }

    public void setCarePlanId(String carePlanId) {
        this.carePlanId = carePlanId;
    }

    public ExerciseAdapterNew(MdocActivity activity, ArrayList<CarePlanDetailsResponses> exercises, ExercisesFragment fragment) {
        this.exercises = exercises;
        this.filtered = exercises;
        this.activity = activity;
        this.fragment = fragment;

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor()).build();

        picasso = new Picasso.Builder(activity)
                .downloader(new OkHttp3Downloader(okHttpClient))
                .build();
    }

    @Override
    public ExerciesViewHolderNew onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exercises_item_layout, parent, false);
        return new ExerciesViewHolderNew(view);
    }

    @Override
    public void onBindViewHolder(ExerciesViewHolderNew holder, final int position) {
        final CarePlanDetailsResponses item = filtered.get(position);
        holder.updateUI(item, activity, position, picasso);
        if(item.getStatus() != null) {
            holder.exerciesLy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openExerciesDescriptionDialog(activity, item, position);
                }
            });
        }
    }

    @SuppressLint("SetTextI18n")
    private void openExerciesDescriptionDialog(final Context context, final CarePlanDetailsResponses exercies, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.exercies_desc_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        ImageView closeIv = view.findViewById(R.id.closeIv);
        LinearLayout markCompletedLv = view.findViewById(R.id.markCompletedLv);
        ImageView exercDescIv = view.findViewById(R.id.exercDescIv);
        TextView timeExercTv = view.findViewById(R.id.timeExercTv);
        TextView repExercTv = view.findViewById(R.id.repExercTv);
        TextView exercDescTv = view.findViewById(R.id.exercDescTv);
        TextView exercDescTitleTv = view.findViewById(R.id.exercDescTitleTv);

        timeExercTv.setText(exercies.getDailyAmount().getValue().getValue() + " " + context.getResources().getString(R.string.min));
        repExercTv.setText(exercies.getQuantity().getValue().getValue() + " " + context.getResources().getString(R.string.rep));
        exercDescTv.setText(exercies.getDescription());
        exercDescTitleTv.setText(exercies.getName());

        if (exercies.getRelatedArtifact().size() > 0) {
            String imageUrl = context.getString(R.string.base_url) + "v2/common/files/download/" + exercies.getRelatedArtifact().get(0).getResource().getReference();
            if (imageUrl != null && !imageUrl.isEmpty())
                picasso.load(imageUrl).into(exercDescIv);
        }

        if(exercies.getStatus() != null) {
            if (exercies.getStatus().getValue().equalsIgnoreCase(MdocConstants.STATUS_COMPLETED)) {
                markCompletedLv.setVisibility(View.INVISIBLE);
            }else{
                markCompletedLv.setVisibility(View.VISIBLE);
            }
        }

        markCompletedLv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JsonObject body = new JsonObject();
                body.addProperty(MdocConstants.STATUS, MdocConstants.STATUS_COMPLETED);
                body.addProperty(MdocConstants.ACTIVITY_ID, exercies.getDefinition().getReference());
                body.addProperty(MdocConstants.CARE_PLAN_ID, carePlanId);
                progressDialog.show();
                MdocManager.changeActivityStatus(body, new Callback<MdocResponse>() {
                    @Override
                    public void onResponse(Call<MdocResponse> call, retrofit2.Response<MdocResponse> response) {
                        if(response.isSuccessful()){
                            dialog.dismiss();
                            fragment.refreshData();
                        }else{
                            MdocUtil.showToastLong(context, activity.getString(R.string.something_went_wrong));
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure(Call<MdocResponse> call, Throwable t) {
                        MdocUtil.showToastLong(context, activity.getString(R.string.something_went_wrong));
                        progressDialog.dismiss();
                    }
                });

            }
        });

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public int getItemCount() {
        return filtered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                    String charString = charSequence.toString();
                    if (charString.isEmpty()) {
                        filtered = exercises;
                    } else {
                        ArrayList<CarePlanDetailsResponses> filteredList = new ArrayList<>();
                        for (CarePlanDetailsResponses row : exercises) {

                            // name match condition. this might differ depending on your requirement
                            // here we are looking for name or phone number match
                            if (row.getName().toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))) {
                                filteredList.add(row);
                            }
                        }

                        filtered = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = filtered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filtered = (ArrayList<CarePlanDetailsResponses>) filterResults.values;

                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }
}

class ExerciesViewHolderNew extends RecyclerView.ViewHolder {

    public LinearLayout exerciesLy, completedLv;
    private TextView completedTv, exerciesTitleTv, durationTv, repTv;
    private ImageView exerciesIv;

    public ExerciesViewHolderNew(View itemView) {
        super(itemView);
        this.exerciesLy = itemView.findViewById(R.id.exerciesLy);
        this.completedLv = itemView.findViewById(R.id.completedLv);
        this.completedTv = itemView.findViewById(R.id.completedTv);
        this.exerciesTitleTv = itemView.findViewById(R.id.exerciesTitleTv);
        this.durationTv = itemView.findViewById(R.id.durationTv);
        this.repTv = itemView.findViewById(R.id.repTv);
        this.exerciesIv = itemView.findViewById(R.id.exerciesIv);
    }

    @SuppressLint("SetTextI18n")
    void updateUI(CarePlanDetailsResponses data, Context context, int index, Picasso picasso) {

        exerciesTitleTv.setText(data.getName());
        if(data.getStatus() != null) {
            if (data.getStatus().getValue().equalsIgnoreCase(MdocConstants.STATUS_COMPLETED)) {
                completedTv.setText(context.getResources().getString(R.string.completed_ex));
                completedLv.setBackground(ContextCompat.getDrawable(context, R.drawable.meal_btn_rr));
            } else {
                completedTv.setText(context.getResources().getString(R.string.pending));
                completedLv.setBackground(ContextCompat.getDrawable(context, R.drawable.yellow_rounded_rec));
            }
            durationTv.setText(data.getDailyAmount().getValue().getValue() + " " + context.getResources().getString(R.string.min));
            repTv.setText(data.getQuantity().getValue().getValue() + " " + context.getResources().getString(R.string.rep));
        }
        if (data.getRelatedArtifact() != null && data.getRelatedArtifact().size() > 0) {
            String imageUrl = context.getString(R.string.base_url) + "v2/common/files/download/" + data.getRelatedArtifact().get(0).getResource().getReference();
            if (imageUrl != null && !imageUrl.isEmpty())
                picasso.load(imageUrl).into(exerciesIv);
        }
    }
}

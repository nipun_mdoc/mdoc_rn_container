package de.mdoc.modules.devices.connected_devices

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.devices.adapters.VitalConnectedAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.pojo.MyDevices
import de.mdoc.pojo.ProvidersData
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.fragment_vital_choose.*
import org.joda.time.LocalDateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class VitalConnectedFragment : MdocBaseFragment() {

    override fun setResourceId(): Int {
        return R.layout.fragment_vital_choose
    }

    override fun init(savedInstanceState: Bundle?) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getConnectedProviders()

        fab_add_vital?.setOnClickListener {
            findNavController().navigate(R.id.fragmentAvailableDevices)
        }
    }

    private fun getConnectedProviders() {
        val userId = MdocAppHelper.getInstance().userId
        progressBar?.visibility = View.VISIBLE

        MdocManager.getConnectedDevices(userId, object : Callback<ProvidersData> {
            override fun onResponse(call: Call<ProvidersData>, response: Response<ProvidersData>) {
                if (response.body()?.data?.providers != null) {


                    val deviceList: ArrayList<MyDevices> = ArrayList()
                    response.body()!!.data.providers.forEach {
                        if (it.linked) {
                            val time: String = if (it.synced != null) {
                                it.synced.toString()
                            } else {
                                LocalDateTime.now().toString()
                            }
                            deviceList.add(MyDevices(it.device, it.device, it.linkUrl, time))
                        }
                    }

                    progressBar?.visibility = View.GONE

                    if (deviceList.isEmpty()) {
                        rv_connected?.visibility = View.GONE
                        no_devices?.visibility = View.VISIBLE

                    } else {
                        rv_connected?.visibility = View.VISIBLE
                        no_devices?.visibility = View.GONE
                        setupAdapter(deviceList)
                    }

                }else{
                    progressBar?.visibility = View.GONE
                    rv_connected?.visibility = View.GONE
                    no_devices?.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<ProvidersData>, t: Throwable) {
                Timber.v("failed")
                progressBar?.visibility = View.GONE
                rv_connected?.visibility = View.GONE
                no_devices?.visibility = View.VISIBLE
            }
        })
    }

    private fun setupAdapter(items: ArrayList<MyDevices>) {
        context?.let {
            rv_connected.layoutManager = LinearLayoutManager(it)
            rv_connected?.adapter = VitalConnectedAdapter(it, items)
        }
    }
}
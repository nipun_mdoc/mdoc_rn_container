package de.mdoc.pojo;

import android.graphics.Bitmap;
import java.io.Serializable;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by ema on 12/28/16.
 */

public class QuestionnaireDetailsData implements Serializable {

    private String pollType;
    private String clinicId;
    private Map<String, String> name;
    private Map<String, String> introduction;
    private Long created;
    private Map<String, String> closingMessage;
    private boolean premium;
    private String pollId;
    private String pollAssignId;
    private ArrayList<Diagnose> diagnosisIds;
    private String id;
    private AnswearDetails answerDetails;
    private ArrayList<Section> sections;
    private String lastSeenPollQuestionId;
    private Boolean isCopyright;
    private QuestionnaireImageCoding copyrightInfo;
    private Boolean sendToKIS = false;
    private String resultScreenTypeCode;

    private String defaultLanguage;
    private ArrayList<String> supportedLanguages;


    public ArrayList<String> getSupportedLanguages() {
        return supportedLanguages;
    }

    public void setDefaultLanguage (String language) {
        this.defaultLanguage = language;
    }

    public String getDefaultLanguage () {return this.defaultLanguage;}

    public String getResultScreenTypeCode() {
        return resultScreenTypeCode;
    }

    public void setResultScreenTypeCode(String resultScreenTypeCode) {
        this.resultScreenTypeCode = resultScreenTypeCode;
    }

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public Boolean getSendToKIS() {
        return sendToKIS;
    }

    public void setSendToKIS(Boolean sendToKIS) {
        this.sendToKIS = sendToKIS;
    }

    public String getPollType() {
        return pollType;
    }

    public void setPollType(String pollType) {
        this.pollType = pollType;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getName() {
        return name.get(defaultLanguage);
    }

    public String getNameLang(String language) {
        return name.get(language);
    }

    public String getIntroduction() {
        return introduction.get(defaultLanguage);
    }

    public String getIntroductionLang(String language) {
        return introduction.get(language)==null?"":introduction.get(language);
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getClosingMessage() {
        return closingMessage.get(defaultLanguage);
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public ArrayList<Diagnose> getDiagnosisIds() {
        return diagnosisIds;
    }

    public void setDiagnosisIds(ArrayList<Diagnose> diagnosisIds) {
        this.diagnosisIds = diagnosisIds;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Section> getSections() {
        return sections;
    }

    public void setSections(ArrayList<Section> sections) {
        this.sections = sections;
    }

    public AnswearDetails getAnswerDetails() {
        return answerDetails;
    }

    public void setAnswerDetails(AnswearDetails answerDetails) {
        this.answerDetails = answerDetails;
    }

    public String getLastSeenPollQuestionId() {
        return lastSeenPollQuestionId;
    }

    public void setLastSeenPollQuestionId(String lastSeenPollQuestionId) {
        this.lastSeenPollQuestionId = lastSeenPollQuestionId;
    }

    public Boolean isCopyright () {
        return isCopyright != null && isCopyright;
    }

    public Bitmap getCopyrightImage () {
        if (copyrightInfo != null) {
            return copyrightInfo.getImage();
        }
        return null;
    }

    public String getCopyrightDescription () {
        if (copyrightInfo != null) {
            return copyrightInfo.getDisplay();
        }
        return "";
    }
}

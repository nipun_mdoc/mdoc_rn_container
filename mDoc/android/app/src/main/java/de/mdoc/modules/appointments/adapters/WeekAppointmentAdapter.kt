package de.mdoc.modules.appointments.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseAppointmentViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.week.AllDayInWeekViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.week.AppointmentWeekViewHolder
import de.mdoc.modules.appointments.adapters.viewholders.week.MetaDataWeekViewHolder
import de.mdoc.pojo.AppointmentDetails
import java.util.*

class WeekAppointmentAdapter(val context: Context, private val appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback) : RecyclerView.Adapter<BaseAppointmentViewHolder>() {

    private val items: ArrayList<AppointmentDetails> = arrayListOf()

    fun setItems(list: List<AppointmentDetails>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun removeItems() {
        items.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAppointmentViewHolder {
        return when (viewType) {
            ALL_DAY_ITEM -> {
                AllDayInWeekViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_all_day_in_week, parent, false))
            }
            META_DATA -> {
                MetaDataWeekViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_appointment_week, parent, false))
            }
            else -> {
                AppointmentWeekViewHolder(LayoutInflater.from(context).inflate(R.layout.placeholder_appointment_week, parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: BaseAppointmentViewHolder, position: Int) {
        holder.bind(items[position], context, appointmentCallback)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when {
            item.isAllDay -> ALL_DAY_ITEM
            item.isMetaDataNeeded || item.isMetaDataPopulated || item.isCanceled -> META_DATA
            else -> DEFAULT_ITEM
        }
    }

    companion object {
        const val ALL_DAY_ITEM = 0
        const val DEFAULT_ITEM = 1
        const val META_DATA = 2
    }
}



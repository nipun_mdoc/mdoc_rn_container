package de.mdoc.data.create_bt_device

data class Component(
        val code: Code,
        val valueQuantity: ValueQuantity
)
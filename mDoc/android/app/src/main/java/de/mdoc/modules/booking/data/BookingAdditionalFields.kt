package de.mdoc.modules.booking.data

import android.os.Parcelable
import android.view.Display
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingAdditionalFields(
        var clinicId: String? = null,
        var departmentDescription: String? = null,
        var appointmentType: String? = null,
        var appointmentTypeDisplay: String? = null,
        var appointmentId: String? = null,
        var specialtyProvider: String? = null,
        var specialityName: String? = null,
        var specialtyDisplay: String? = null,
        var onlineType: String? = null,
        var specialities: ArrayList<String>? = null,
        var specialtyCode: String? = null): Parcelable {

    fun isIntegration(): Boolean {
        return (!specialtyProvider.isNullOrEmpty() && specialtyProvider != "mdoc")
    }
}
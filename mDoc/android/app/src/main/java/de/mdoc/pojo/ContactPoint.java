package de.mdoc.pojo;

public class ContactPoint {
    private String value;
    private String use;

    public ContactPoint(String value, String use) {
        this.value = value;
        this.use = use;
    }

    public ContactPoint() {
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUse() {
        return use;
    }

    public void setUse(String use) {
        this.use = use;
    }
}

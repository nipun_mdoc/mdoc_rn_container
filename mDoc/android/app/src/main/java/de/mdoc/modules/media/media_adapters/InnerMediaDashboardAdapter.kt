package de.mdoc.modules.media.media_adapters

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.Cursor
import android.os.Bundle
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Environment
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.folioreader.Config
import com.folioreader.FolioReader
import com.folioreader.util.AppUtil
import com.squareup.picasso.Picasso
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.activities.navigation.isPhone
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.media.data.MediaLightItem
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MediaResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.util.MdocUtil
import de.mdoc.util.getErrorMessage
import de.mdoc.util.scaleBitmapImage
import kotlinx.android.synthetic.main.placeholder_media_dashboard_articles.view.*
import pub.devrel.easypermissions.EasyPermissions
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.File
import java.util.*

class InnerMediaDashboardAdapter(var context: Context, var category: List<MediaLightItem>):
        RecyclerView.Adapter<InnerMediaDashboardViewHolder>() {

    var downloadId: Long = 0
    val activity = context as MdocActivity
    val progressHandler = ProgressHandler(activity)
    val STORAGE_PERMISSION_CODE: Int = 124

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InnerMediaDashboardViewHolder {
        context = parent.context
        val view: View = if (viewType == 1) {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_media_dashboard_first_article, parent, false)
        }
        else {
            LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_media_dashboard_articles, parent, false)
        }
        return InnerMediaDashboardViewHolder(view)
    }

    override fun onBindViewHolder(holder: InnerMediaDashboardViewHolder, position: Int) {
        holder.setIsRecyclable(true)
        val article = category[position]
        val articleName = if (category.isNotEmpty()) article.name ?: "" else ""
        val articleDescription =
                if (category.isNotEmpty()) article.thumbails?.get(0)?.meta?.getDisplayByCode(CODE_DESCRIPTION) else ""

        holder.txtArticleName.text = articleName
        holder.txtArticleDescription.text = articleDescription

        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        if(activity.isPhone()){
            val imageWidth: Int = (displayMetrics.widthPixels - (displayMetrics.density * activity.resources.getInteger(R.integer.inner_media_padding_adapter_phone))).toInt() // 32 => sum of padding's and margins from the parent views
            holder.imgThumbnail.layoutParams.width = imageWidth
        } else {
            val extraPadding =  if(activity.isOpenMenu) activity.resources.getInteger(R.integer.inner_media_padding_adapter_tab_menu_open) else activity.resources.getInteger(R.integer.inner_media_padding_adapter_tab_menu_close)
            val imageWidth = (displayMetrics.widthPixels - (displayMetrics.density * extraPadding)).toInt() // extraPadding => sum of padding's and margins from the parent views
            holder.imgThumbnail.layoutParams.width = imageWidth
        }

        handleVideoWaterMark(article, holder.imgVideoWatermark)
        handleImageLoad(activity, category, article, holder.imgThumbnail, holder.itemViewType)

        holder.clPlaceholder.setOnClickListener {
            if (article.type == MdocConstants.VIDEO) {
                MdocManager.getMediaById(article.id, object : Callback<MediaResponse> {
                    override fun onResponse(call: Call<MediaResponse>, response: Response<MediaResponse>) {
                        if (response.isSuccessful) {
                            val action = MainNavDirections.globalActionToVideo(response.body()!!.data.publicUrl)
                            activity.findNavController(R.id.navigation_host_fragment)
                                    .navigate(action)
                        } else {
                            Timber.w("getMediaById ${response.getErrorDetails()}")
                            MdocUtil.showToastLong(activity, response.getErrorMessage())
                        }
                    }

                    override fun onFailure(call: Call<MediaResponse>, t: Throwable) {
                        Timber.w(t, "getMediaById")
                        MdocUtil.showToastLong(activity, t.message)
                    }
                })
            } else if (article.type == MdocConstants.EPUB){
                progressHandler.showProgress()
                if (EasyPermissions.hasPermissions(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                MdocManager.getMediaById(article.id, object : Callback<MediaResponse> {
                    override fun onResponse(call: Call<MediaResponse>, response: Response<MediaResponse>) {
                        if (response.isSuccessful) {
                            downloadEpub(response.body()!!.data.publicUrl, article)
                        } else {
                            Timber.w("getMediaById ${response.getErrorDetails()}")
                            MdocUtil.showToastLong(activity, response.getErrorMessage())
                            progressHandler.hideProgress()
                        }
                    }

                    override fun onFailure(call: Call<MediaResponse>, t: Throwable) {
                        Timber.w(t, "getMediaById")
                        MdocUtil.showToastLong(activity, t.message)
                        progressHandler.hideProgress()
                    }
                }) } else {
                    EasyPermissions.requestPermissions(activity, activity.resources.getString(R.string.request_storage_permission), STORAGE_PERMISSION_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    progressHandler.hideProgress()
                }
            } else {
                val url=context.getString(R.string.base_url) + "v2/media/Media/" + article.id
                val action= MainNavDirections.globalActionToPdf(url = url, isComingFromMediaModule = true)
                activity.findNavController(R.id.navigation_host_fragment)
                    .navigate(action)
            }
        }
    }

    private fun downloadEpub(url: String?, file: MediaLightItem) {
        if (!fileExist(file.name)) {
            val request = DownloadManager.Request(Uri.parse(url))
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            request.allowScanningByMediaScanner()
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN)
            request.setVisibleInDownloadsUi(false)
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, file.name + ".epub")

            val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
            downloadId = manager.enqueue(request)

            val f = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath, file.name + ".epub")

            val br = object : BroadcastReceiver() {
                override fun onReceive(context: Context?, intent: Intent?) {
                    val action = intent?.action
                    if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action) {
                        downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
                        val query = DownloadManager.Query()
                        query.setFilterById(downloadId)
                        val c: Cursor = manager.query(query)
                        if (c.moveToFirst()) {
                            val columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS)
                            if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                                openEpub(f.absolutePath)
                            }
                        }
                    }
                }
            }
            context.registerReceiver(br, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
        } else {
            val f = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath, file.name + ".epub")
            openEpub(f.absolutePath)
        }
    }


    private fun openEpub(path: String){
        progressHandler.hideProgress()
        val folioReader: FolioReader = FolioReader.get()
        var config: Config? = AppUtil.getSavedConfig(context)
        if (config == null) config = Config()
        config.allowedDirection = Config.AllowedDirection.VERTICAL_AND_HORIZONTAL
        config.isShowTts = true
        config.setThemeColorInt(context.resources.getColor(R.color.colorPrimary))
        folioReader.setConfig(config, true).openBook(path)
    }

    private fun fileExist(name: String?): Boolean {
        val f = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath, "$name.epub")
        val file: File = f
        return file.exists()
    }

    override fun getItemCount(): Int {
        return category.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) 1
        else 2
    }

    private fun handleImageLoad(context: Context, category: List<MediaLightItem>, article: MediaLightItem, imgThumbnail: ImageView, viewType: Int) =
            if (category.isNotEmpty()) {
                val imageUrl = article.thumbails?.getOrNull(0)?.publicUrl
                Glide.with(context).asBitmap()
                        .load(imageUrl)
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(object : CustomTarget<Bitmap?>() {
                            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap?>?) {
                                val scaledImage: Bitmap = scaleBitmapImage(resource, imgThumbnail.measuredWidth, imgThumbnail.measuredHeight)
                                if (viewType != 1) {
                                    imgThumbnail.scaleType = ImageView.ScaleType.CENTER_CROP
                                }
                                imgThumbnail.setImageBitmap(scaledImage)
                            }

                            override fun onLoadCleared(placeholder: Drawable?) {}
                        })

            }
            else {
                Picasso.get()
                    .load(R.drawable.circle_white)
                    .into(imgThumbnail)
            }

    private fun handleVideoWaterMark(article: MediaLightItem, imgVideoWatermark: ImageView) {
        if (article.type == MdocConstants.VIDEO) {
            imgVideoWatermark.visibility = View.VISIBLE
        }
        else {
            imgVideoWatermark.visibility = View.GONE
        }
    }

    companion object {
        const val CODE_DESCRIPTION = "description"
    }
}

class InnerMediaDashboardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val clPlaceholder: ConstraintLayout = itemView.cl_placeholder as ConstraintLayout
    val txtArticleName: TextView = itemView.txt_article_name
    val txtArticleDescription: TextView = itemView.txt_article_description
    val imgThumbnail: ImageView = itemView.img_article_item
    val imgVideoWatermark: ImageView = itemView.img_video_watermark
}
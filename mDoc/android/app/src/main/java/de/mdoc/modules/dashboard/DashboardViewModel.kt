package de.mdoc.modules.dashboard

import android.app.Application
import android.content.res.Resources
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R

import de.mdoc.activities.navigation.permission.LowPermission
import de.mdoc.databinding.FragmentDashboardBinding
import de.mdoc.modules.covid19.Covid19ViewModel
import de.mdoc.modules.medications.create_plan.data.ListItems
import de.mdoc.network.request.CodingRequest
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.Resource
import de.mdoc.service.IMdocService
import de.mdoc.storage.AppPersistence
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class DashboardViewModel(val app: Application,
                         private val mDocService: IMdocService,
                         val binding: FragmentDashboardBinding,
                         val resource: Resources?): AndroidViewModel(app) {

    init {
        getStaticApplicationData()
    }

    private fun getStaticApplicationData() {
        viewModelScope.launch {
            try {
                coroutineScope {
                    val timeUnitResponse = async { checkDefaultTimeCoding() }
                    val medicalUnitResponse = async { getMedicationUnitCodingApi() }
                    val deviceConfigurationResponse = async { getDeviceConfigurationApi() }
                    val fhirConfigurationResponse = async { getFhirConfigurationApi() }
                    val covidStatusCodingResponse = async { getCovidStatusCodingApi() }
                    val relationshipCodingResponse = async { getRelationshipCodingApi() }
                    val appointmentTypeCodingResponse = async { getAppointmentTypeCodingApi() }


                    awaitAll(timeUnitResponse, medicalUnitResponse, deviceConfigurationResponse,
                            fhirConfigurationResponse,
                            covidStatusCodingResponse, relationshipCodingResponse,
                            appointmentTypeCodingResponse)
                }
            } catch (e: Exception) {
            }

            val hasPermission = LowPermission(resource).hasPermissionToDisplay()
            if (hasPermission) {
                DashboardBindingConfiguration().bindConfiguration(binding, app.applicationContext)
            }

            DashboardBindingConfiguration().bindLowPermissionWidgets(binding, app.applicationContext)
        }
    }

    private suspend fun checkDefaultTimeCoding() {
        if (hasMedicationPlanModule()) {
            try {
                val timeOfDayCoding = mDocService.getImageCodingLight(DEFAULT_TIME_CODING, SORT_FIELD)
                    .data?.list

                AppPersistence.timeCoding = timeOfDayCoding?.map {
                    ListItems(
                            text = it?.display as String,
                            code = it.code,
                            imageBase64 = it.imageBase64)
                }
            } catch (e: Exception) {
            }
        }
    }

    private suspend fun getMedicationUnitCodingApi() {
        if (hasMedicationWidget()) {
            try {
                val unitCodingResponse = mDocService.getCodingLight(UNIT_CODING)
                    .data?.list

                AppPersistence.medicationUnitsCoding = unitCodingResponse?.map {
                    CodingItem(
                            display = it?.display,
                            code = it?.code
                              )
                }
            } catch (e: Exception) {
            }
        }
    }

    private suspend fun getDeviceConfigurationApi() {
        if (hasVitalsModule()) {
            AppPersistence.deviceConfiguration = mDocService.coroutineCheckDeviceSettings()
                .data.configurationDetailsMultiValue
        }
    }

    private suspend fun getFhirConfigurationApi() {
        if (hasVitalsModule()) {
            AppPersistence.fhirConfigurationData = mDocService.coroutineFhirConfiguration()
                .manualDevice()
                .sortedBy { it.imageCoding?.display }
        }
    }

    private suspend fun getCovidStatusCodingApi() {
        if (hasCovidModule()) {
            AppPersistence.covidStatusCoding = mDocService.getCodingKt(CodingRequest(
                    Covid19ViewModel.STATUS_CODING)).data.list.sortedBy { it.code }

            AppPersistence.covidIllnesCoding = mDocService.getCodingKt(CodingRequest(
                    Covid19ViewModel.ILLNES_CODING)).data.list
        }
    }

    private suspend fun getRelationshipCodingApi() {
        AppPersistence.relationshipCoding = mDocService.getCodingKt(CodingRequest(RELATIONSHIP_CODING)).data.list
    }

    private suspend fun getAppointmentTypeCodingApi() {
        if (hasBookingModule()) {
            AppPersistence.appointmentTypeCoding = mDocService.getCodingKt(CodingRequest(
                    APPOINTMENT_TYPE_CODING)).data.list
        }
    }

    private fun hasVitalsModule() = app.applicationContext.resources.getBoolean(R.bool.has_vitals)
    private fun hasMedicationWidget() = app.applicationContext.resources.getBoolean(R.bool.has_medication_widget)
    private fun hasMedicationPlanModule() = app.applicationContext.resources.getBoolean(R.bool.has_medications_plan)
    private fun hasCovidModule() = app.applicationContext.resources.getBoolean(R.bool.has_covid19_module)
    private fun hasBookingModule() = app.applicationContext.resources.getBoolean(R.bool.has_booking)

    companion object {
        const val DEFAULT_TIME_CODING = "https://mdoc.one/coding/default_time"
        const val UNIT_CODING = "http://unitsofmeasure.org/medication-intake"
        const val RELATIONSHIP_CODING = "https://www.hl7.org/fhir/v2/0063"
        const val APPOINTMENT_TYPE_CODING = "https://mdoc.one/coding/appointmentType"
        const val SORT_FIELD = "order"
    }
}
package de.mdoc.util.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.RectF
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.modules.files.Config
import kotlin.math.abs

class GaugeView : View {

    private lateinit var paint: Paint
    private lateinit var rect: RectF

    private var trackWidth = 0f

    private var startAngle = 0
    private var sweepAngle = 0

    private var endValue = 0

    private var value: Float = 0.0f

    private var pointAngle = 0.0
    private var point = 0

    // Colors
    private var startColor = 0
    private var endColor = 0
    private var trackBackgroundColor = 0

    // Animation
    private var animationStartTime: Long = 0
    private lateinit var interpolator: Interpolator
    private val animationDuration = Config.STORAGE_ANIMATION_DURATION

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.GaugeView, 0, 0)

        // Track
        trackWidth = attributes.getDimension(R.styleable.GaugeView_trackWidth, 10f)
        trackBackgroundColor = attributes.getColor(R.styleable.GaugeView_trackBackgroundColor, ContextCompat.getColor(context, R.color.gray_e6e6e6))

        // Angle start and sweep (opposite direction 0, 270, 180, 90)
        startAngle = attributes.getInt(R.styleable.GaugeView_startAngle, 0)
        sweepAngle = attributes.getInt(R.styleable.GaugeView_sweepAngle, 360)

        // Scale (from mStartValue to mEndValue)
        setEndValue(attributes.getInt(R.styleable.GaugeView_max, 1000))

        // Pointer colors
        startColor = attributes.getColor(R.styleable.GaugeView_startColor, ContextCompat.getColor(context, R.color.colorPrimary))
        endColor = attributes.getColor(R.styleable.GaugeView_endColor, ContextCompat.getColor(context, R.color.red_f53948))

        // Calculating one point sweep
        pointAngle = Math.abs(sweepAngle).toDouble() / (endValue)
        attributes.recycle()

        init()
    }

    private fun init() {
        // Paint
        paint = Paint()
        paint.color = trackBackgroundColor
        paint.strokeWidth = trackWidth
        paint.isAntiAlias = true
        paint.strokeCap = Paint.Cap.ROUND
        paint.style = Paint.Style.STROKE

        // Rect
        rect = RectF()
        point = startAngle

        // Interpolator
        interpolator = AccelerateDecelerateInterpolator()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        if (animationStartTime == 0L) {
            animationStartTime = System.currentTimeMillis()
        }

        val padding = trackWidth
        val size = if (width < height) width.toFloat() else height.toFloat()
        val width = size - 2 * padding
        val height = size - 2 * padding
        val radius = if (width < height) width / 2 else height / 2

        val rectLeft = (getWidth() - 2 * padding) / 2 - radius + padding
        val rectTop = (getHeight() - 2 * padding) / 2 - radius + padding
        val rectRight = (getWidth() - 2 * padding) / 2 - radius + padding + width
        val rectBottom = (getHeight() - 2 * padding) / 2 - radius + padding + height

        rect.set(rectLeft, rectTop, rectRight, rectBottom)
        paint.color = trackBackgroundColor
        paint.shader = null

        // Draw base layer
        canvas.drawArc(rect, startAngle.toFloat(), sweepAngle.toFloat(), false, paint)

        // Change point color based on value
        if (value >=  Config.PERCENTAGE_RED_STATE) {
            paint.color = endColor
        } else {
            paint.color = startColor
        }

        canvas.drawArc(rect, startAngle.toFloat(), currentFrameAngle, false, paint)

        if (currentFrameAngle < drawAngle) {
            invalidate()
        }
    }

    private val drawAngle: Int get() = point - startAngle

    fun setValue(value: Float) {
        this.value = value
        point = (startAngle + (this.value) * pointAngle).toInt()
        invalidate()
    }

    private fun setEndValue(endValue: Int) {
        this.endValue = endValue
        pointAngle = abs(sweepAngle).toDouble() / (this.endValue)
        invalidate()
    }

    private val currentFrameAngle: Float
        get() {
            val now = System.currentTimeMillis()
            val pathGone = (now - animationStartTime).toFloat() / animationDuration
            val interpolatedPathGone = interpolator.getInterpolation(pathGone)

            return if (pathGone < 1.0f) {
                drawAngle * interpolatedPathGone
            } else {
                drawAngle.toFloat()
            }
        }
}
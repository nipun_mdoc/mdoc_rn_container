package de.mdoc.network.response;


import de.mdoc.pojo.CurrentClinicData;

/**
 * Created by ema on 1/20/17.
 */

public class CurrentClinicResponse extends MdocResponse {

    private CurrentClinicData data;

    public CurrentClinicData getData() {
        return data;
    }

    public void setData(CurrentClinicData data) {
        this.data = data;
    }
}

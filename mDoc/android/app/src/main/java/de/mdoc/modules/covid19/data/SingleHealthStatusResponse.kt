package de.mdoc.modules.covid19.data

data class SingleHealthStatusResponse(
        val data: HealthStatus? = HealthStatus(),
        val timestamp: Long? = 0
                               )

package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 7/14/17.
 */

public class QuestionnaireProgress implements Serializable{

    private String name;
    private int count;
    private int started;
    private String displayName;

    public QuestionnaireProgress(String name, int count, int started,String displayName) {
        this.name = name;
        this.count = count;
        this.started = started;
        this.displayName=displayName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getStarted() {
        return started;
    }

    public void setStarted(int started) {
        this.started = started;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}

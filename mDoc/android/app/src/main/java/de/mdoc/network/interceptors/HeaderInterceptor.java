package de.mdoc.network.interceptors;

import java.io.IOException;
import java.util.Locale;

import de.mdoc.BuildConfig;
import de.mdoc.constants.MdocConstants;
import de.mdoc.util.MdocAppHelper;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by ema on 4/6/17.
 */

public class HeaderInterceptor implements Interceptor {

    private boolean shouldAdd = true;

    public HeaderInterceptor(boolean shouldAdd) {
        this.shouldAdd =  shouldAdd;
    }

    public HeaderInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Request.Builder requestBuilder = request.newBuilder();
        String userAuth;
        String caseId = MdocAppHelper.getInstance().getCaseId();
        String clinicId = MdocAppHelper.getInstance().getClinicId();

        if(BuildConfig.FLAV.equals("139")) {
            userAuth =  MdocAppHelper.getInstance().getUserAuth();
        }else {
            userAuth = MdocAppHelper.getInstance().getAccessToken();
        }

        // Default header params
        requestBuilder.addHeader(MdocConstants.CONTENT_TYPE, MdocConstants.APPLICATION_JSON)
                .addHeader(MdocConstants.USER_SOURCE, MdocConstants.ANDROID)
                .addHeader(MdocConstants.ACCEPT_LANGUAGE, Locale.getDefault().getLanguage())
                .addHeader(MdocConstants.USER_AGENT,"m-doc");


        // Additional header params only if the user has been authorized
        if (userAuth!= null) {
            requestBuilder.addHeader(MdocConstants.AUTHORIZATION, userAuth);

            if (caseId != null) {
                requestBuilder.addHeader(MdocConstants.DELEGATED_CASE, caseId);
            }

            if (clinicId != null && shouldAdd) {
                requestBuilder.addHeader(MdocConstants.DELEGATED_CLINIC, clinicId);
            }
        }

        return chain.proceed(requestBuilder.build());
    }
}

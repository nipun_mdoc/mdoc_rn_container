package de.mdoc.newlogin.Interactor

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import de.mdoc.R
import de.mdoc.databinding.ActivityLoginTanBinding
import de.mdoc.newlogin.AuthListener
import de.mdoc.newlogin.LoginTanViewModel
import de.mdoc.newlogin.LoginUser
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.activity_logi.*
import java.util.*
import androidx.lifecycle.Observer
import de.mdoc.BuildConfig
import de.mdoc.activities.CaseActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.login.BiometricPromptDialogFragment
import de.mdoc.activities.login.BiometricPromptDialogFragmentCallback
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.activities.login.TermsPrivacyDialogHandler
import de.mdoc.activities.login.clinicselection.ClinicSelectionActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.pojo.Case
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import kotlinx.android.synthetic.main.activity_forget_password.*

class LoginTan : AppCompatActivity(), LoginHelper.LoginCallback, TermsPrivacyDialogHandler.TermsPrivacyCallback, BiometricPromptDialogFragmentCallback, CommonDialogFragment.OnButtonClickListener, AuthListener {

    private lateinit var encryptionService: EncryptionServices
    private lateinit var systemServices: SystemServices
    private val progressHandler = ProgressHandler(this)
    private var refreshTokenCode = 200
    private val loginHelper by lazy {
        LoginHelper(this, this, progressHandler)
    }
    private var _cases: ArrayList<Case>? = null
    private val termsPrivacyDialogHandler by lazy {
        TermsPrivacyDialogHandler(this, this, progressHandler)
    }
    private var loginTanViewModel: LoginTanViewModel? = null
    private var binding: ActivityLoginTanBinding? = null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loginTanViewModel = ViewModelProviders.of(this).get(LoginTanViewModel::class.java)
        binding = DataBindingUtil.setContentView(this@LoginTan, R.layout.activity_login_tan)
        binding?.setLifecycleOwner(this)
        binding?.setLoginTanViewModel(loginTanViewModel)

        loginTanViewModel?.authListener = this

        encryptionService = EncryptionServices(this)
        systemServices = SystemServices(this)

        if (encryptionService.getMasterKey() == null) {
            encryptionService.createMasterKey()
        }

        var shakeAnimation: Animation? = AnimationUtils.loadAnimation(this, R.anim.shake)
        loginTanViewModel!!.user.observe(this, Observer<LoginUser> { userRequestModel ->

            if (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.username)) {
                txtLoginLayout?.setError(" ")
                txtLoginLayout.errorIconDrawable = null
                txtLoginLayout?.requestFocus();

                txtPasswordLayout?.startAnimation(shakeAnimation)
            }else if (linearCaptchaMain.visibility == View.VISIBLE && (TextUtils.isEmpty(Objects.requireNonNull(userRequestModel)!!.captcha))) {
                txtCaptchaLayout?.setError(" ")
                txtCaptchaLayout.errorIconDrawable = null
                txtCaptchaLayout?.requestFocus();
                txtCaptchaLayout?.startAnimation(shakeAnimation)
            }  else {
                loginTanViewModel?.loadData(userRequestModel!!, this)
            }
        })

    }

    override fun sucess() {
        if (MdocAppHelper.getInstance().secretKey == null && MdocConstants.KEY_MISSING) {
            MdocConstants.KEY_MISSING = false
            loginHelper.fetchSecretKey();
        } else {
            loginHelper.loginWithSession()
        }
    }

    override fun onLoadCaptcha(response: CaptchaResponse?) {
        linearCaptchaMain.visibility = View.VISIBLE
        val base64String = response?.data?.image?.replace("data:image/jpeg;base64,", "")
        val decodedString: ByteArray = android.util.Base64.decode(base64String, android.util.Base64.DEFAULT)
        val decodedByte: Bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imageViewCaptcha.setImageBitmap(decodedByte)
    }

    override fun isCaptchaVisible(): Boolean? {
        return linearCaptchaMain?.isVisible
    }

    override fun clearCaptchaField() {
        txtCaptchaLayout?.editText?.setText("")
    }
    override fun changePassword(token: String) {
        TODO("Not yet implemented")
    }

    override fun showMobileNumberScreen() {
        TODO("Not yet implemented")
    }

    override fun failer() {
        TODO("Not yet implemented")
    }
    override fun showOTPScreen(){
        TODO("Not yet implemented")
    }

    override fun onImpressumClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)    }
    override fun onDatenschutzDieClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)  }
    override fun onAGBClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)   }

    override fun onCloseClick() {
        finish()
    }

    override fun onScanClick() {
        TODO("Not yet implemented")
    }

    override fun openTermsAndConditions() {
        termsPrivacyDialogHandler.openTermsFromCoding()
    }

    override fun openPrivacyPolicy() {
        termsPrivacyDialogHandler.openPrivacyFromCoding()
    }

    override fun openNextActivity(cases: ArrayList<Case>?) {
        _cases = cases

        if (MdocAppHelper.getInstance()
                        .isOptIn && MdocConstants.IS_UPDAE_PASSWORD) {
            MdocConstants.IS_UPDAE_PASSWORD = false
            SystemServices(this).authenticateFingerprint(successAction = {
                systemServices.optIn(MdocConstants.NEW_PASSWORD)
                decideFlowBasedOnCases()
            }, cancelAction = {
                this.runOnUiThread {
                    MdocAppHelper.getInstance().isOptIn = false
                    decideFlowBasedOnCases()
                }
            }, context1 = this@LoginTan)

        } else {
            if (systemServices.canOptIn()) {
                BiometricPromptDialogFragment(this).also { it.isCancelable = false }
                        .showNow(supportFragmentManager, "BiometricPromptDialogFragment")
            } else {
                decideFlowBasedOnCases()
            }
        }


    }

    override fun onPrivacyPolicyAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onTermsAndConditionsAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onPrivacyPolicyDeclined() {
//        loadInitialPage()
    }

    override fun onTermsAndConditionsDeclined() {
//        loadInitialPage()
    }

    override fun openClinics() {
        val intent = Intent(this, ClinicSelectionActivity::class.java)
        startActivityForResult(intent, ClinicSelectionActivity.CLINIC_SELECTION_RESULT)
    }

    var isShowing = false;
    override fun onMissingSecret() {
        if (isShowing) return
        isShowing = true
        CommonDialogFragment.Builder()
                .title(resources.getString(R.string.login_not_possible))
                .description(resources.getString(R.string.login_not_possible_description))
                .setPositiveButton(resources.getString(R.string.button_ok))
                .setOnClickListener(this)
                .build().showNow(supportFragmentManager, CommonDialogFragment::class.simpleName)
    }


    private fun decideFlowBasedOnCases() {
        if (_cases == null) {
            navigateToDashboard()
        } else {
            openCases(_cases)
        }
    }

    private fun navigateToDashboard() {
        val intent = Intent(this, MdocActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

        if (BuildConfig.FLAV != "kiosk") {
            finish()
        }
    }

    private fun openCases(cases: ArrayList<Case>?) {
        val intent = Intent(applicationContext, CaseActivity::class.java)
        intent.putExtra(MdocConstants.CASES, cases)
        startActivity(intent)
        finish()
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        isShowing = false
    }

    override fun onBiometricPromptDialogButtonClick(isPositiveButton: Boolean) {
        MdocAppHelper.getInstance()
                .isBiometricPrompted = true
        if (isPositiveButton) {
            optInBiometricAuthentication()
        }
        else {
            decideFlowBasedOnCases()
            MdocAppHelper.getInstance()
                    .userPassword = null
        }
    }

    private fun optInBiometricAuthentication() {
        systemServices.authenticateFingerprint(successAction = {
            Toast.makeText(applicationContext,
                    resources.getString(R.string.biometric_auth_success), Toast.LENGTH_SHORT)
                    .show()
            val token = MdocAppHelper.getInstance()
                    .secretKey

            if (encryptionService.getFingerprintKey() == null) {
                encryptionService.createFingerprintKey()
            }
            val password = MdocAppHelper.getInstance()
                    .userPassword

            MdocAppHelper.getInstance()
                    .isOptIn = true
            MdocAppHelper.getInstance()
                    .userPassword = password

            MdocAppHelper.getInstance()
                    .secretKey = token


            decideFlowBasedOnCases()
        }, cancelAction = {
            this.runOnUiThread {
                decideFlowBasedOnCases()
            }
        }, context1 = this@LoginTan)
    }


}
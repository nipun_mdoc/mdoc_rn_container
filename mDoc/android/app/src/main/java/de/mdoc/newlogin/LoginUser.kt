package de.mdoc.newlogin



class LoginUser {
     var client_id: String? = null
     var client_secret: String? = null
     var friendlyName: String? = null
     var password: String? = null
     var totp: String? = null
     var username: String? = null
     var captcha: String? = null
     var mobileNumber: String? = null
     var smsotp: String? = null
}
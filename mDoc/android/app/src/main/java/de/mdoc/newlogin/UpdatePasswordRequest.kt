package de.mdoc.newlogin



class UpdatePasswordRequest {
     var client_id: String? = null
     var client_secret: String? = null
     var confirmPassword: String? = null
     var password: String? = null
     var token: String? = null

}
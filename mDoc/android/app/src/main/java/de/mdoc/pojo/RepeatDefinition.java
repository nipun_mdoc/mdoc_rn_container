package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RepeatDefinition implements Serializable {

    @SerializedName("info")
    @Expose
    private AppointmentsInfo info;

    public AppointmentsInfo getInfo() {
        return info;
    }

    public void setInfo(AppointmentsInfo info) {
        this.info = info;
    }
}

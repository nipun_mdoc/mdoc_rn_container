package de.mdoc.modules.messages.news_widget

import android.view.View
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.modules.messages.data.AdditionalMessagesFields
import de.mdoc.modules.messages.data.ListItem

class NewsWidgetHandler {

    fun onSeeAllClicked(view: View) {
        view.findNavController()
            .navigate(R.id.navigation_messages)
    }

    fun onSeeMoreClick(view: View, item: ListItem) {
        val additionalFields = AdditionalMessagesFields(isClosedConversation = false,
                conversationId = item.id,
                letterboxTitle = item.getSenderInformation(),
                reasonTitle = item.getReasonText(),
                isNewMessage = item.isNotSeen(),
                isPublic = item.isPublic,
                replyAllowed = item.replyAllowed)
        val action = MainNavDirections.globalActionToMessage(additionalFields)
        view.findNavController()
            .navigate(action)
    }
}
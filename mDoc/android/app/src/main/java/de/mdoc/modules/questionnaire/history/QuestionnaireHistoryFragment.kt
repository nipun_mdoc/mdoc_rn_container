package de.mdoc.modules.questionnaire.history

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.data.questionnaire.Questionnaire
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.questionnaire.questionnaire_adapter.QuestionnaireHistoryAdapter
import de.mdoc.modules.questionnaire.questionnaire_adapter.SpinnerDateAdapter
import de.mdoc.modules.questionnaire.questionnaire_adapter.SpinnerTitleAdapter
import de.mdoc.viewmodel.doWithItemAt
import kotlinx.android.synthetic.main.fragment_questionnaire_history.*
import kotlinx.android.synthetic.main.fragment_status_screen.*

class QuestionnaireHistoryFragment: MdocFragment(), QuestionnaireHistoryAdapter.Listener {

    private val viewModel by lazy {
        ViewModelProviders.of(this)
            .get(QuestionnaireHistoryViewModel::class.java)
    }
    lateinit var activity: MdocActivity
    private var adapter = QuestionnaireHistoryAdapter(this)

    override fun setResourceId(): Int {
        return R.layout.fragment_questionnaire_history
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireHistory

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rv_questionnaire_history?.layoutManager = LinearLayoutManager(activity)
        rv_questionnaire_history?.adapter = adapter
        viewModel.inProgress.observe(this) {
            progressBar.visibility = if (it) {
                View.VISIBLE
            }
            else View.GONE
        }

        viewModel.isNoHistory.observe(this) {
            if (it) {
                status_history?.visibility = View.VISIBLE
                img_status?.setImageResource(R.drawable.ic_no_history)
                tv_title?.text = resources.getString(R.string.no_history)
                tv_message?.text = resources.getString(R.string.questionnaire_no_history)
            }
            else {
                status_history?.visibility = View.GONE
            }
        }

        viewModel.isDataPresent.observe(this) {
            if (it) {
                ll_spinner_holder?.visibility = View.VISIBLE
                rv_questionnaire_history?.visibility = View.VISIBLE
            }
            else {
                ll_spinner_holder?.visibility = View.GONE
                rv_questionnaire_history?.visibility = View.GONE
            }
        }

        viewModel.listOfItems.observe(this) {
            adapter.items = it
        }

        viewModel.names.observe(this) {
            val titleSpinnerAdapter = SpinnerTitleAdapter(context, it)
            title_spinner?.adapter = titleSpinnerAdapter
        }

        viewModel.selectedType.observe(this) {
            val position = viewModel.names.get()
                .indexOf(it)
            title_spinner.setSelection(position)
        }

        title_spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?, view: View?, position: Int, id: Long
                                       ) {
                viewModel.names.doWithItemAt(position) {
                    viewModel.selectedType.set(it)
                }
            }
        }

        viewModel.dates.observe(this) {
            val dateSpinnerAdapter = SpinnerDateAdapter(context, it)
            date_spinner?.adapter = dateSpinnerAdapter
        }

        viewModel.selectedDate.observe(this) {
            val position = viewModel.dates.get()
                .indexOf(it)
            date_spinner.setSelection(position)
        }

        date_spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                    parent: AdapterView<*>?, view: View?, position: Int, id: Long
                                       ) {
                viewModel.dates.doWithItemAt(position) {
                    viewModel.selectedDate.set(it)
                }
            }
        }
    }

    override fun onQuestionnaireSelected(questionnaire: Questionnaire) {
        val action =
                QuestionnaireHistoryFragmentDirections.actionQuestionnaireHistoryFragmentToQuestionnaireAnswersFragment(
                        questionnaire.pollVotingActions[0].pollVotingActionId)
        findNavController().navigate(action)
    }
}

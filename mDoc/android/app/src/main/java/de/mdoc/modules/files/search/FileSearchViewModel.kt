package de.mdoc.modules.files.search

import de.mdoc.modules.common.viewmodel.BaseSearchViewModel
import de.mdoc.modules.files.data.FilesData
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.files.data.listSize
import de.mdoc.network.request.SearchFileRequest
import de.mdoc.pojo.DiaryList

class FileSearchViewModel(private val repository: FilesRepository) : BaseSearchViewModel<FilesData>() {

    override fun requestSearch(query: String) {
        super.requestSearch(query)
        if (query.isNotEmpty()) {
            val request = SearchFileRequest(0, 0, query)
            repository.getFilesData(request, ::onDataLoaded, ::onError)
        }
    }

    override fun requestDelete(query: DiaryList) {
    }

    override fun requestUpdate(query: DiaryList) {
    }

    fun onDataLoaded(data: FilesData?) {
        onDataLoaded(data, data?.listSize()?:0)
    }
}
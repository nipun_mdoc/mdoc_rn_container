package de.mdoc.modules.questionnaire.questions

import de.mdoc.R

internal class TextQuestionType : AbstractTextQuestionType() {

    override val layoutResId: Int = R.layout.long_text_layout

}
package de.mdoc.modules.mental_goals.mental_goal_adapters

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_action_item.view.*
import androidx.recyclerview.widget.LinearLayoutManager


class ActionItemsAdapter(var context: Context, var items:ArrayList<ActionItem>,val isEditing:Boolean) : RecyclerView.Adapter<ActionItemViewHolder>() {

    var mRecyclerView: RecyclerView? = null
    var layoutManager: LinearLayoutManager? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ActionItemViewHolder {
        context = parent.context
        val view:View =
            LayoutInflater.from(parent.context).inflate(R.layout.placeholder_action_item, parent, false)
         layoutManager = mRecyclerView?.layoutManager as LinearLayoutManager


        return ActionItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ActionItemViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item=items[position]
        val planNumber = String.format("%02d. ", position+1)

        holder.txtNumber.text =planNumber
        holder.txtAction.setText(item.item)

        if(holder.txtAction.text.isEmpty()){
            holder.txtAction.requestFocus()
        }

        holder.txtAction.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                item.item = holder.txtAction.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        if(isEditing){
            holder.txtAction.imeOptions = EditorInfo.IME_ACTION_NEXT
            holder.txtAction.setRawInputType(InputType.TYPE_CLASS_TEXT)

        }else {
            holder.txtAction.imeOptions = EditorInfo.IME_ACTION_DONE
            holder.txtAction.setRawInputType(InputType.TYPE_CLASS_TEXT)

            holder.txtAction.setOnEditorActionListener { _, actionId, _ ->
                if(actionId==EditorInfo.IME_ACTION_DONE){
                    if(holder.txtAction.text.isEmpty()) {
                        MdocUtil.showToastShort(context,
                                context.resources.getString(R.string.this_field_can_not_be_empty))
                    }
                    else {
                        items.add(ActionItem(null, null))
                        notifyDataSetChanged()
                    }
                    MentalGoalsUtil.hideKeyboard(context as Activity)
                }
                true
            }
            holder.txtAction.setOnKeyListener { _, actionId, event ->
                if(position!=0&&holder.txtAction.text.isEmpty()){

                    if(event.action==KeyEvent.ACTION_DOWN&&
                            actionId==KeyEvent.KEYCODE_DEL){
                        items.removeAt(position)
                        notifyDataSetChanged()
                        MentalGoalsUtil.hideKeyboard(context as Activity)
                    }
                }else{

                }
                false
            }
        }

    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        mRecyclerView = recyclerView
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class ActionItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtAction:EditText=itemView.txt_action
    var txtNumber:TextView =itemView.txt_number
}
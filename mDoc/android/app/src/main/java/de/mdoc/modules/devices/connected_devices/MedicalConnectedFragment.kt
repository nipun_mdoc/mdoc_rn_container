package de.mdoc.modules.devices.connected_devices

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.data.configuration.ConfigurationDetailsMultiValue
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.devices.DevicesUtil.medicalDevicesList
import de.mdoc.modules.devices.adapters.MedicalConnectedAdapter
import de.mdoc.storage.AppPersistence.deviceConfiguration
import kotlinx.android.synthetic.main.fragment_medical_connected.*

class MedicalConnectedFragment: MdocBaseFragment() {

    lateinit var configuration: ConfigurationDetailsMultiValue

    override fun setResourceId(): Int {
        return R.layout.fragment_medical_connected
    }

    override fun init(savedInstanceState: Bundle?) {
        configuration = deviceConfiguration
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        fab_add_device?.setOnClickListener {
            val action = ConnectedDevicesFragmentDirections.actionToFragmentAvailableDevices(isFromMedical = true)
            findNavController().navigate(action)
        }
    }

    private fun setupAdapter() {
        context?.let {
            rv_connected_devices?.layoutManager = LinearLayoutManager(it)
            rv_connected_devices?.adapter = MedicalConnectedAdapter(it, medicalDevicesList)
        }
    }
}
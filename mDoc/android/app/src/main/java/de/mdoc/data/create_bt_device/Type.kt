package de.mdoc.data.create_bt_device

data class Type(
        val coding: List<Coding>
)
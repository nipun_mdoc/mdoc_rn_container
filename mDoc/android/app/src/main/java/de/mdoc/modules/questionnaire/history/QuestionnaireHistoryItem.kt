package de.mdoc.modules.questionnaire.history

import de.mdoc.constants.MdocConstants
import de.mdoc.data.questionnaire.Questionnaire
import de.mdoc.util.MdocUtil

class QuestionnaireHistoryItem(
    val questionnaire: Questionnaire
) {
    val name = questionnaire.pollTitle
    val date = MdocUtil.getDateFromMS(
        MdocConstants.DAY_MONTH_DOT_FORMAT,
        questionnaire.pollVotingActions[0].finished
    )
}
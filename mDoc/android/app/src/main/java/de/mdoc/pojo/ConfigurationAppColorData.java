package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Pramod on 12/31/20.
 */

public class ConfigurationAppColorData {

    @SerializedName("colorPrimary")
    @Expose
    private String colorPrimary;
    @SerializedName("colorAccent")
    @Expose
    private String colorAccent;
    @SerializedName("colorTertiary")
    @Expose
    private String colorTertiary;

    public ConfigurationAppColorData(String colorPrimary, String colorAccent, String colorTertiary) {
        this.colorPrimary = colorPrimary;
        this.colorAccent = colorAccent;
        this.colorTertiary = colorTertiary;
    }

    public void setColorPrimary(String colorPrimary){
        this.colorPrimary = colorPrimary;
    }
    public String getColorPrimary(){
        return this.colorPrimary;
    }
    public void setColorAccent(String colorAccent){
        this.colorAccent = colorAccent;
    }
    public String getColorAccent(){
        return this.colorAccent;
    }
    public void setColorTertiary(String colorTertiary){
        this.colorTertiary = colorTertiary;
    }
    public String getColorTertiary(){
        return this.colorTertiary;
    }

}

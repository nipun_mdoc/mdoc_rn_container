package de.mdoc.newlogin.Model


data class ImprintResponse(
    val code: String,
    val `data`: List<Data1>,
    val message: String,
    val timestamp: Long
)

data class Data1(
    val code: String,
    val display: String,
    val system: String
)

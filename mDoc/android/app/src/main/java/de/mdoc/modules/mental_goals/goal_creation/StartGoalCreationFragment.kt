package de.mdoc.modules.mental_goals.goal_creation

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import kotlinx.android.synthetic.main.fragment_start_goal_creation.*

class StartGoalCreationFragment: MdocFragment() {

    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_start_goal_creation
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        handleOnBackPressed {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
        progressBar?.progress = 1

        et_goal_title?.setText(MentalGoalsUtil.goalTitle)
        et_goal_title?.imeOptions = EditorInfo.IME_ACTION_DONE
        et_goal_title?.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_goal_title?.background?.mutate()
            ?.colorFilter= PorterDuffColorFilter(ContextCompat.getColor(activity, R.color.black),
                PorterDuff.Mode.SRC_ATOP)

        et_goal_title?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                et_goal_description?.requestFocus()
            }
            true
        }

        et_goal_description?.setText(MentalGoalsUtil.goalDescription)
        et_goal_description?.imeOptions = EditorInfo.IME_ACTION_DONE
        et_goal_description?.setRawInputType(InputType.TYPE_CLASS_TEXT)
        et_goal_description?.background?.mutate()?.colorFilter= PorterDuffColorFilter(ContextCompat.getColor(activity, R.color.black),
        PorterDuff.Mode.SRC_ATOP)

        et_goal_description?.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                MentalGoalsUtil.hideKeyboard(activity)
            }
            true
        }
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }

        txt_next?.setOnClickListener {
            if (bothFieldsAreFilled()) {
                MentalGoalsUtil.goalTitle = et_goal_title.text.toString()
                MentalGoalsUtil.goalDescription = et_goal_description.text.toString()
                val action = StartGoalCreationFragmentDirections.actionStartGoalCreationFragmentToGoalPeriodFragment()
                findNavController().navigate(action)
            }
            else {
                MdocUtil.showToastShort(activity, resources.getString(R.string.fill_all_fields))
            }
        }
    }

    private fun bothFieldsAreFilled(): Boolean {
        return et_goal_title?.text.toString()
            .isNotEmpty() &&
                et_goal_description?.text.toString()
                    .isNotEmpty()
    }
}
package de.mdoc.modules.mental_goals.data_goals

data class MentalGoalsResponse(
        val data:ArrayList<MentalGoalsData>)

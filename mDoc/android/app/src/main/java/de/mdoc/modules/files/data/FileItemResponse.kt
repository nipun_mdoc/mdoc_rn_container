package de.mdoc.modules.files.data

import de.mdoc.pojo.FileListItem


data class FileItemResponse(val code: String = "",
                            val data: FileListItem,
                            val message: String = "",
                            val timestamp: Long = 0)
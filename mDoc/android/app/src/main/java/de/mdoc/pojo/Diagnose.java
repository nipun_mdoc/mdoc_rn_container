package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 2/13/17.
 */

public class Diagnose implements Serializable {

    private String id;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

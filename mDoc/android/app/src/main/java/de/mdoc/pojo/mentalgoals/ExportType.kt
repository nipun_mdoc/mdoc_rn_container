package de.mdoc.pojo.mentalgoals

enum class ExportType(val value: String) {
    PDF("pdf"),
    XLSX("excel")
}
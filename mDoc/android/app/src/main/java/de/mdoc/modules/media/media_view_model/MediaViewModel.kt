package de.mdoc.modules.media.media_view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.media.data.MediaLightItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class MediaViewModel(private val mDocService: IMdocService
                    ): ViewModel() {

    var isLoading = MutableLiveData(false)

    fun getMediaCategory(category: String, onSuccess: (List<MediaLightItem>) -> Unit) {
        viewModelScope.launch {
            isLoading.value = true

            try {
                val response = mDocService.searchMedia(category,PUBLIC_URLS)
                    .data.list.filter { !it.category.isNullOrEmpty() }
                onSuccess(response)
            } catch (e: java.lang.Exception) {
                isLoading.value = false
            }
            isLoading.value = false
        }
    }
    companion object{
        const val PUBLIC_URLS = "*publicUrls*"
    }
}
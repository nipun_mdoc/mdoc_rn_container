package de.mdoc.modules.profile.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact(var firstName: String? = null,
                   var lastName: String? = null,
                   var name: String? = null,
                   var phone: String? = null,
                   var email: String? = null,
                   var relation: String? = null):Parcelable
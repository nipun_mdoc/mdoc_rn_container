package de.mdoc.modules.files.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.files.adapters.viewholders.FileItemViewHolder
import de.mdoc.modules.files.adapters.viewholders.FileItemViewHolderCallback
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FileListItem
import java.util.*

/**
 * Created by ema on 4/2/18.
 */

class FilesListAdapter(
        private val callback: FilesAdapterCallback,
        private var items: MutableList<FileListItem>? = arrayListOf(),
        private var categories: List<CodingItem>? = ArrayList()
) : RecyclerView.Adapter<FileItemViewHolder>(), FileItemViewHolderCallback {

    fun setItems(items: List<FileListItem>) {
        this.items = items.toMutableList()
        notifyDataSetChanged()
    }

    fun setCategories(categories: List<CodingItem>) {
        this.categories = categories
        notifyDataSetChanged()
    }

    fun removeItem (id: String) {
        val itemForDelete = items?.find { it -> it.id == id }
        items?.remove(itemForDelete)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.file_list_item, parent, false)
        return FileItemViewHolder(view, this, parent.context)
    }

    override fun onBindViewHolder(holder: FileItemViewHolder, position: Int) {
        val item = items?.get(position)
        holder.update(item!!, categories!!)
    }

    override fun onClickMore(view: View, item: FileListItem, position: Int, holder: FileItemViewHolder) {
        holder.showPopup(view, item, callback)
    }

    override fun getItemCount(): Int {
        return items?.size?:0
    }
}


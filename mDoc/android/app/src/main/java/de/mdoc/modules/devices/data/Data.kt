package de.mdoc.modules.devices.data

data class Data(
        val avg: Double,
        val max: Double,
        val metricCode: String,
        val min: Double,
        val totalCount: Int,
        val data: DataObject)
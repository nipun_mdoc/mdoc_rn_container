package de.mdoc.modules.media.media_adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.media.MediaStatisticsData
import de.mdoc.modules.media.MediaFragmentDirections
import de.mdoc.modules.media.data.MediaLightItem
import kotlinx.android.synthetic.main.placeholder_media_dashboard_item.view.*

class MediaDashboardAdapter(var context: Context, var items: List<MediaStatisticsData>):
        RecyclerView.Adapter<MediaDashboardViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaDashboardViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_media_dashboard_item, parent, false)

        context = parent.context

        return MediaDashboardViewHolder(view)
    }

    override fun onBindViewHolder(holder: MediaDashboardViewHolder, position: Int) {
        val activity = context as MdocActivity
        val mediaItem = items[position]

        holder.txtCategoryLabel.text = mediaItem.category
        val categoryContent = contentToMediaLight(mediaItem)

        holder.rvMediaItems.layoutManager = LinearLayoutManager(context)
        holder.rvMediaItems.adapter = InnerMediaDashboardAdapter(context, categoryContent)
        //        isViewMoreVisible(holder.txtMoreFromCategory, categoryContent.size)
        holder.txtMoreFromCategory.setOnClickListener {
            val action=MediaFragmentDirections.actionMediaFragmentToMediaCategoryFragment(mediaItem.category)
            activity.findNavController(R.id.navigation_host_fragment).navigate(action)
        }
    }

    private fun contentToMediaLight(mediaItem: MediaStatisticsData): List<MediaLightItem> {
       return mediaItem.content.map {
            MediaLightItem(
                    id = it.id,
                    category = it.category,
                    type = it.mediaType,
                    name = it.magazineName,
                    publicUrl = it.coverURL,
                    content = MediaLightItem.Content(title = it.magazineName),
                    thumbails = listOf(
                            MediaLightItem.Thumbail(publicUrl = it.coverURL, meta = MediaLightItem.Thumbail.Meta(tag =
                            listOf(MediaLightItem.Thumbail.Meta.Tag(
                                    code = InnerMediaDashboardAdapter.CODE_DESCRIPTION
                                    , display = it.description))))
                                      ))
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
    //    private fun isViewMoreVisible(view: View, childItemSize: Int) {
    //        view.visibility = if (childItemSize > 1) {
    //            View.VISIBLE
    //        }
    //        else {
    //            View.GONE
    //        }
    //    }

}

class MediaDashboardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val txtCategoryLabel: TextView = itemView.txt_category_label
    val rvMediaItems: RecyclerView = itemView.rv_media_items
    val txtMoreFromCategory: TextView = itemView.txt_category_more
}


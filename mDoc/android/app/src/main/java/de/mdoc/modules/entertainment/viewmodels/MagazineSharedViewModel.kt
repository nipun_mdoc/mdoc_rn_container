package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MagazineSharedViewModel: ViewModel() {

    var shouldEnableScrolling = MutableLiveData(true)

    fun enableMagazineScrolling () {
        shouldEnableScrolling.value = true
    }

    fun disableMagazineScrolling () {
        shouldEnableScrolling.value = false
    }
}
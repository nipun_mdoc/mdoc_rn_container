package de.mdoc.activities.navigation

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.squareup.picasso.Picasso
import de.mdoc.R
import de.mdoc.activities.navigation.permission.shouldDisplay
import de.mdoc.constants.MdocConstants
import de.mdoc.pojo.ConfigurationListItem
import de.mdoc.pojo.SubConfigurationData
import de.mdoc.util.MdocAppHelper
import java.util.ArrayList

class NavigationHandler(
    private val context: Context,
    private val viewModel: NavigationViewModel,
    private val lifecycleOwner: LifecycleOwner) {

    val defaultTextColor = ContextCompat.getColor(context, R.color.charocal_gray)
    val defaultIconColor = ContextCompat.getColor(context, R.color.battleship_gray)
    val selectedColor = ContextCompat.getColor(context, R.color.colorPrimary)

    private inner class MenuItemView(
        val menuItem: MenuItem,
        val container: View?,
        val text: TextView?,
        val icon: ImageView?,
        val shortContainer: View?,
        val shortIcon: ImageView?
    ) {

        private var isSelected: Boolean? = null

        fun applyVisibility(context: Context): Boolean {
            val configs = MdocAppHelper.getInstance().configurationData.list.filter { it.id== MdocAppHelper.getInstance().getPairedIdForMoreMenu(context.getResources().getResourceEntryName(menuItem.menuContainerId)) } as ArrayList<ConfigurationListItem>
            var isVisible = false;
            var label = "";
            var imageUrl = "";
            if(configs != null && configs.size > 0) {
                val con = configs[0];
                if (con.isEnabled) {
                    if(con.subConfig != null) {
                        var subConfigs = con.subConfig.filter { it.id == MdocConstants.history } as ArrayList<SubConfigurationData>
                        if (subConfigs != null && subConfigs.size > 0) {
                            if (subConfigs[0].isEnabled) {
                                isVisible = true;
                                label = con.featureTitle;
//                    imageUrl = con.icon
                            }
                        }
                    }
                }
            }

            val visibility = if (isVisible) View.VISIBLE else View.GONE
            container?.visibility = visibility
            shortContainer?.visibility = visibility
//            if(isVisible){
//                if(label.isNotEmpty()) {
//                    text?.text = label;
//                }
//                if(imageUrl.isNotEmpty()) {
//                    Picasso.get()
//                            .load(imageUrl)
//                            .error(R.drawable.circle_white)
//                            .into(object : com.squareup.picasso.Target {
//                                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
////                                val mBitmapDrawable = BitmapDrawable(context.getResources(), bitmap)
//                                    icon?.setImageBitmap(bitmap)
//                                }
//
//                                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
//
//                                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {}
//                            })
//                }
//            }
            return isVisible
        }

        fun setSelected(isSelected: Boolean) {
            if (this.isSelected != isSelected) {
                this.isSelected = isSelected
                text?.setTextColor(if (isSelected) selectedColor else defaultTextColor)
                icon?.setColorFilter(if (isSelected) selectedColor else defaultIconColor)
                shortIcon?.setColorFilter(if (isSelected) selectedColor else defaultIconColor)
            }
        }

    }

    fun setupMenu(menuView: View) {
        val items = MenuItem.values().map {
            MenuItemView(
                menuItem = it,
                container = menuView.findViewById(it.menuContainerId),
                text = menuView.findViewById(it.menuTextId),
                icon = menuView.findViewById(it.menuIconId),
                shortIcon = menuView.findViewById(it.menuShortIconId),
                shortContainer = menuView.findViewById(it.menuShortContainerId)
            )
        }.filter {
            it.applyVisibility(menuView.context)
        }
        items.forEach { menuItemView ->
            menuItemView.setSelected(false)
            menuItemView.container?.setOnClickListener {
                menuItemView.setSelected(true)
                viewModel.onMenuItemClick(menuItemView.menuItem)
            }
            menuItemView.shortContainer?.setOnClickListener {
                menuItemView.setSelected(true)
                viewModel.onMenuItemClick(menuItemView.menuItem)
            }
        }
        viewModel.currentMenuItem.observe(lifecycleOwner, Observer { selectedMenuItem ->
            items.forEach { item ->
                item.setSelected(selectedMenuItem == item.menuItem)
            }
        })
    }
}

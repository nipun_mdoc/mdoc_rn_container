package de.mdoc.modules.medications.create_plan.data

data class CodingResponseKt(
        val data: Data? = null)

data class Data(val list: List<ListItem?>? = null,
        val moreDataAvailable: Boolean? = null,
        val totalCount: Int? = null
               ){

    fun getItemForCode(code : String) : ListItem?{
        list?.forEach {
            if(it?.code == code){
                return it
            }
        }
        return null
    }
}

data class ListItem(
        var activatedByUser: Boolean = false,
        val active: Boolean? = null,
        val code: String? = null,
        val cts: Long? = null,
        val display: String? = null,
        val id: String? = null,
        val system: String? = null,
        val translations: Translations? = null,
        val uts: Long? = null,
        val imageBase64: String?=null
                   )

data class Translations(
        val de: String? = null,
        val en: String? = null
                       )



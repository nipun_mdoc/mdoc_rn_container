package de.mdoc.modules.medications.search.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class MedicineDetail(
        val data: MedicineDetailData)

@Parcelize
data class MedicineDetailData(
        var code: String = "",
        val cts: Long = 0,
        val description: String = "",
        var form: String = "",
        val id: String = "",
        val image: String = "",
        val imageContentType: String = "",
        val manualEntry: Boolean = false,
        val manufacturer: String = "",
        val manufacturerCode: String = "",
        val compounds: ArrayList<Compound>? = null,
        val prescriptionReasons: ArrayList<String>? = null,
        val size: String = "",
        val unit: String = "",
        val uts: Long = 0): Parcelable {

    fun pznInfo(): String {
        return "PZN - $code"
    }

    fun getCompound(): String {
        return compounds?.getOrNull(0)?.name ?: ""
    }

    fun dosageInfo(): String {
        return when {
            form.isNotEmpty() -> "$form - $size $unit"
            size.isNotEmpty() -> "$size $unit"
            else              -> unit
        }
    }

    fun getPrescriptionReason(): String {
        return prescriptionReasons?.getOrNull(0) ?: ""
    }
}
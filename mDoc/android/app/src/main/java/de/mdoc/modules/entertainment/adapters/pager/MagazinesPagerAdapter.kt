package de.mdoc.modules.entertainment.adapters.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.modules.entertainment.fragments.pager.MagazinePageViewFragment

class MagazinesPagerAdapter(fragmentManager: FragmentManager, private val pdfId: String?, private val magazineId: String?, private val numberOfPages: Int, private val targetFragment: Fragment) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return MagazinePageViewFragment.newInstance(position.toString(), pdfId, magazineId, targetFragment)
    }

    override fun getCount(): Int {
        return numberOfPages
    }
}
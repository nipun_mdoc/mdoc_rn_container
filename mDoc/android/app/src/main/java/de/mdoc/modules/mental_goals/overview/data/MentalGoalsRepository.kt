package de.mdoc.modules.mental_goals.overview.data

import de.mdoc.modules.mental_goals.data_goals.GamificationStatus
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsData
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.service.IMdocService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class MentalGoalsRepository(
    private val service: IMdocService
) {

    private val disposables = CompositeDisposable()

    fun getGamificationData(onSuccess: (GamificationData) -> Unit, onError: (Throwable) -> Unit) {
        val request = MentalGoalsRequest()
        service.searchGamificationMentalGoals(request)
            .map {
                val data: Map<String, Int> = it.data
                GamificationData(
                    countNew = data[GamificationStatus.NEW_GOAL.toString()] ?: 0,
                    countAlmostReached = data[GamificationStatus.NEARLY_FINISHED.toString()] ?: 0,
                    countCompleted = data[GamificationStatus.ACHIEVED.toString()] ?: 0
                )
            }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .addTo(disposables)
    }

    fun getOpenGoals(onSuccess: (List<MentalGoalsData>) -> Unit, onError: (Throwable) -> Unit) {
        val request = MentalGoalsRequest()
        request.status = "OPEN"
        getGoals(request, onSuccess, onError)
    }

    fun getClosedGoals(onSuccess: (List<MentalGoalsData>) -> Unit, onError: (Throwable) -> Unit) {
        val request = MentalGoalsRequest()
        request.notStatus = "OPEN"
        getGoals(request, onSuccess, onError)
    }

    private fun getGoals(
        request: MentalGoalsRequest,
        onSuccess: (List<MentalGoalsData>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        service.searchMentalGoals(request).map {
            it.data
        }.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .addTo(disposables)
    }

    fun recycle() {
        disposables.dispose()
    }

}
package de.mdoc.util

import android.widget.AutoCompleteTextView
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt

class AutoCompleteValidator(private val inputList: ArrayList<InsuranceDataKt>): AutoCompleteTextView.Validator {
    override fun isValid(p0: CharSequence?): Boolean {
        inputList.forEach {
            if (it.name.toString() == p0.toString()) {
                return true
            }
        }
        return false
    }

    override fun fixText(p0: CharSequence?): CharSequence {
        return ""
    }
}
package de.mdoc.pojo;

public class deleteDiaryPjo{
    public long timestamp;
    public String message;
    public String code;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DiaryList getData() {
        return data;
    }

    public void setData(DiaryList data) {
        this.data = data;
    }

    public DiaryList data;
}

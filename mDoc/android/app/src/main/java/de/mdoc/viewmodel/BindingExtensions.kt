package de.mdoc.viewmodel

import android.view.View
import android.widget.TextView
import androidx.lifecycle.LifecycleOwner

fun LifecycleOwner.bindVisibleGone(target: View?, source: ValueLiveData<Boolean>) {
    source.observe(this) {
        target?.visibility = if (it) View.VISIBLE else View.GONE
    }
}

fun LifecycleOwner.bindVisibleInvisible(target: View?, source: ValueLiveData<Boolean>) {
    source.observe(this) {
        target?.visibility = if (it) View.VISIBLE else View.INVISIBLE
    }
}


fun LifecycleOwner.bindEnableDisable(target: View?, source: ValueLiveData<Boolean>) {
    source.observe(this) {
        target?.isEnabled = it
    }
}


fun LifecycleOwner.bindText(target: TextView?, source: ValueLiveData<String>) {
    source.observe(this) {
        target?.text = it
    }
}

/**
 * This function invokes [action] only once when [source] property is true. Later changes of
 * [source] have no effect.
 */
fun LifecycleOwner.onceTrue(source: ValueLiveData<Boolean>, action: () -> Unit) {
    var isCalled = false
    source.observe(this) {
        if (!isCalled && it) {
            action.invoke()
            isCalled = true
        }
    }
}
package de.mdoc.modules.devices.bf600

import android.app.Dialog
import android.bluetooth.BluetoothGatt
import android.content.res.ColorStateList
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.interfaces.DeviceDataListener
import de.mdoc.modules.devices.data.BleMessage
import de.mdoc.modules.devices.data.ScaleUser
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.showCancelableProgressDialog
import kotlinx.android.synthetic.main.fragment_bf600_enter_pin.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class EnterPinBf600: MdocFragment(), DeviceDataListener {

    lateinit var activity: MdocActivity
    var scaleUser: ScaleUser? = null
    private val scaleTimeoutHandler = Handler()
    private var shouldCancelConnection = true
    private var pinProgressDialog: Dialog? = null

    private val args: EnterPinBf600Args by navArgs()

    override fun setResourceId() = R.layout.fragment_bf600_enter_pin

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        args.bf600Wrapper?.setBleResponseListener(this@EnterPinBf600)
    }

    override val navigationItem: NavigationItem = NavigationItem.Devices

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed {
            shouldCancelConnection = false
            findNavController().popBackStack()
        }

        shouldCancelConnection = true

        if (args.userIndex == -1) {
            txt_pin_message?.visibility = View.VISIBLE
        }

        RxTextView.textChanges(et_pin)
            .debounce(
                    100,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread()
                     )
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                if (it.toString().length == 4) {
                    bottom_toolbar.isClickable = true
                    et_pin.setLineColor(ResourcesCompat.getColor(resources, R.color.colorPrimary, null))

                    bottom_toolbar.backgroundTintList = ColorStateList.valueOf(
                            ContextCompat.getColor(activity, R.color.colorPrimary))
                }
                else {
                    bottom_toolbar.isClickable = false
                    et_pin.setLineColor(ResourcesCompat.getColor(resources, R.color.gray_ccccc, null))

                    bottom_toolbar.backgroundTintList = ColorStateList.valueOf(
                            ContextCompat.getColor(activity, R.color.gray_ccccc))
                }
            }

        bottom_toolbar?.setOnClickListener {
            pinProgressDialog = showCancelableProgressDialog()

            if (args.userIndex == -1) {
                args.bf600Wrapper?.createUser(et_pin.text.toString())
            }
            else {
                args.bf600Wrapper?.userLogin(args.userIndex, et_pin.text.toString())
            }
        }
    }

    override fun genericDeviceResponse(list: ArrayList<Byte>?,
                                       gatt: BluetoothGatt?,
                                       message: BleMessage?) {
        activity.runOnUiThread {
            when (message) {
                BleMessage.LOG_IN_SUCCESS     -> {
                    if (args.userIndex == -1) {
                        args.bf600Wrapper?.updateUserDetails(scaleUser)
                    }
                    else {
                        args.bf600Wrapper?.takeMeasurement()
                    }

                    pinProgressDialog?.dismiss()
                    scaleTimeoutHandler.removeCallbacksAndMessages(null)
                    shouldCancelConnection = false
                    val action = EnterPinBf600Directions.actionToFragmentLoadingDevice(
                            deviceData = args.deviceData,
                            deviceId = args.deviceId,
                            bf600Wrapper = args.bf600Wrapper)

                    findNavController().navigate(action)
                }

                BleMessage.LOG_IN_FAIL        -> {
                    scaleTimeoutHandler.removeCallbacksAndMessages(null)
                    pinProgressDialog?.dismiss()
                    et_pin?.setText("")
                    MdocUtil.showToastLong(activity, activity.resources.getString(R.string.wrong_pin))
                }

                BleMessage.CONNECTION_TIMEOUT -> {
                    connectionTimeoutAction(this)
                }

                else                          -> {
                    //do nothing
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        pinProgressDialog?.dismiss()
        if (shouldCancelConnection) {
            args.bf600Wrapper?.disconnect()
        }
    }
}
package de.mdoc.modules.booking.specialities

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.constants.MdocConstants
import de.mdoc.network.request.CodingRequest
import de.mdoc.network.response.SpeciltyData
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class SpecialitiesViewModel(private val mDocService: IMdocService,val specialtyProvider:String?): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(true)
    var content: MutableLiveData<ArrayList<SpeciltyData>> = MutableLiveData(ArrayList())
    var query: MutableLiveData<String> = MutableLiveData()

    init {
        getSpecialities()
    }

    private fun getSpecialities() {
        viewModelScope.launch {
            try {
                getSpecialitiesAsync()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
            }
        }
    }

    private suspend fun getSpecialitiesAsync() {
        isLoading.value = true
        isShowNoDataMessage.value = false
        isShowLoadingError.value = false

        content.value = mDocService.getSpecialities(specialtyProvider?:"").data

        isLoading.value = false

        if (content.value.isNullOrEmpty()) {
            isShowNoDataMessage.value = true
            isShowContent.value = false
        }
        else {
            isShowContent.value = true
            isShowNoDataMessage.value = false
        }
    }
}
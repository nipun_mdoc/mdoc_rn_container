package de.mdoc.modules.vitals.charts

import android.app.Activity
import android.content.Context
import android.view.View
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import de.mdoc.R
import de.mdoc.modules.devices.data.ObservationPeriodType
import de.mdoc.modules.vitals.FhirConfigurationUtil.componentMetricLabel
import de.mdoc.util.ChartFormatter.DayFormatter
import de.mdoc.util.ChartFormatter.MonthFormatter
import de.mdoc.util.ChartFormatter.WeekFormatter
import de.mdoc.util.ChartFormatter.YearFormatter
import java.util.*

fun drawCompositeLineChart(context: Context,
                           periodType: ObservationPeriodType,
                           inputData: ArrayList<ArrayList<Entry>>,
                           metricCodes:List<String>,
                           lastDayOfMonth:Int) {

    val activity:Activity=context as Activity
    val graphColors = context.resources.getIntArray(R.array.graph)

    var minValueInCollection=0F
    var maxValueInCollection=0F
    val yAxisLimitTop:Float
    val yAxisLimitBottom:Float

    val lineChart = activity.findViewById<LineChart>(R.id.chart_pulse)

    if(lineChart!=null) {
        lineChart.visibility = View.VISIBLE

        val lineDataSets = arrayListOf<ILineDataSet>()
        for((index,observationList) in inputData.withIndex()){
            val graphColor=graphColors[index]
            val dataSet=LineDataSet(
                    observationList,
                    componentMetricLabel(metricCodes.getOrNull(index) ?: "")
            )

            dataSet.lineWidth = 3F
            dataSet.setCircleColor(graphColor)
            dataSet.setCircleColorHole(graphColor)
            dataSet.color = graphColor
            lineDataSets.add(dataSet)
            maxValueInCollection= if(observationList.maxBy {it.y }?.y?:200F>=maxValueInCollection){
                observationList.maxBy {it.y }?.y?:200F
            }else{
                maxValueInCollection
            }

            minValueInCollection=if(observationList.minBy {it.y }?.y?:0F<=minValueInCollection){
                observationList.minBy {it.y }?.y?:0F
            }else{
                minValueInCollection }
        }

        yAxisLimitTop=maxValueInCollection+(maxValueInCollection*(10F/100F))
        yAxisLimitBottom=minValueInCollection-(minValueInCollection*(10F/100F))

        lineChart.setNoDataText("")
        lineChart.setScaleEnabled(false)
        lineChart.axisLeft.setDrawGridLines(false)
        lineChart.axisRight.isEnabled = false
        lineChart.description.isEnabled = false

        //x axis
        lineChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        lineChart.xAxis.setDrawAxisLine(true)
        lineChart.xAxis.setDrawGridLines(false)

        when (periodType) {
            ObservationPeriodType.DAY -> {
                val dayFormatter = DayFormatter()
                lineChart.xAxis.setLabelCount(5, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 24F
                lineChart.xAxis.valueFormatter = dayFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.WEEK -> {
                val weekFormatter = WeekFormatter(context)
                lineChart.xAxis.setLabelCount(7, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 6F
                lineChart.xAxis.valueFormatter = weekFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum =yAxisLimitTop
            }
            ObservationPeriodType.MONTH -> {
                val monthFormatter = MonthFormatter()
                lineChart.xAxis.setLabelCount(7, true)
                lineChart.xAxis.axisMinimum = 01F
                lineChart.xAxis.axisMaximum = lastDayOfMonth.toFloat()
                lineChart.xAxis.valueFormatter = monthFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.YEAR -> {
                val yearFormatter = YearFormatter()
                lineChart.xAxis.setLabelCount(12, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 11F
                lineChart.xAxis.valueFormatter = yearFormatter


                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            else -> {
            }
        }

        if (inputData.isEmpty()) {
            inputData.add(arrayListOf(Entry(0.toFloat(), 0.toFloat())))
        }

        val lineData = LineData(lineDataSets)

        lineChart.data = lineData
        lineChart.invalidate()
    }
}
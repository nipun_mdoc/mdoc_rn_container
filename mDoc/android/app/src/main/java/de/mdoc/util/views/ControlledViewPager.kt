package de.mdoc.util.views

import android.content.Context
import android.view.MotionEvent
import android.util.AttributeSet
import androidx.viewpager.widget.ViewPager


class ControlledViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var isSwipeEnabled: Boolean = false

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return when (isSwipeEnabled) {
            true -> super.onTouchEvent(event)
            else -> false
        }
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return when (isSwipeEnabled) {
            true ->  super.onInterceptTouchEvent(event)
            else -> false
        }
    }

    fun setPagingEnabled(enabled: Boolean) {
        this.isSwipeEnabled = enabled
    }
}
package de.mdoc.util

import android.R
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.graphics.drawable.InsetDrawable
import android.view.View
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.data.create_bt_device.ObservationResponseData
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.vitals.adapters.UnitPickerAdapter
import de.mdoc.modules.vitals.adapters.VitalsAdapter
import de.mdoc.modules.vitals.adapters.VitalsDashboardAdapter
import de.mdoc.modules.vitals.unit_picker.UnitPickerViewModel
import de.mdoc.pojo.AppointmentDetails


@BindingAdapter(value = ["adapterItems", "adapterLayout", "adapterHandler", "adapterDivider"], requireAll = false)
fun setAppointmentItems(view: RecyclerView, items: List<Any>?, layout: Int, handler: Any? = null, hasDivider: Boolean = false) {
    if (!items.isNullOrEmpty()) {

        if (view.layoutManager == null) {
            view.layoutManager = LinearLayoutManager(view.context)
        }

        // Divider
        if (hasDivider) {
            val orientation = (view.layoutManager as LinearLayoutManager).orientation
            view.addItemDecoration(
                    object : DividerItemDecoration(view.context, orientation) {
                        override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
                            val position = parent.getChildAdapterPosition(view)
                            // Hide the divider for the last item
                            if (position == state.itemCount - 1) {
                                outRect.setEmpty()
                            } else {
                                super.getItemOffsets(outRect, view, parent, state)
                            }
                            super.getItemOffsets(outRect, view, parent, state)
                        }
                    }
            )
        }

        val genericAdapter = GenericRecyclerAdapter(items, layout, handler)
        view.adapter = genericAdapter
    }
}

@BindingAdapter("vitalItems")
fun setVitalItems(view: RecyclerView, items: List<ObservationResponseData>?) {
    if (view.layoutManager == null) {
        view.layoutManager = LinearLayoutManager(view.context)
    }

    val vitalsAdapter = VitalsAdapter(view.context)
    view.adapter = vitalsAdapter
    vitalsAdapter.setData(items?: emptyList())
    vitalsAdapter.notifyDataSetChanged()
}
@BindingAdapter("vitalWidgetItems")
fun setVitalWidgetAdapter(view: RecyclerView, items: List<ObservationResponseData>?) {
    if (view.layoutManager == null) {
        view.layoutManager = LinearLayoutManager(view.context)
        view.isScrollContainer=false
    }

    val vitalsAdapter = VitalsDashboardAdapter(view.context)
    view.adapter = vitalsAdapter
    vitalsAdapter.setData(items?: emptyList())
    vitalsAdapter.notifyDataSetChanged()
}

@BindingAdapter("appointmentItems", "appointmentItemCallback")
fun setAppointmentItems(view: RecyclerView, items: List<AppointmentDetails>?,  appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?) {
    if (!items.isNullOrEmpty()) {

        if (view.layoutManager == null) {
            view.layoutManager = LinearLayoutManager(view.context)
        }
        var adapter = view.context?.let { DayAppointmentAdapter(it, appointmentCallback, true) }
        view?.adapter = adapter
        adapter?.setItems(items)
    }
}

@BindingAdapter("unitViewModel")
fun setUnitPicker(view: RecyclerView, viewModel: UnitPickerViewModel) {
    if (view.layoutManager == null) {
        view.layoutManager = LinearLayoutManager(view.context)
    }

    val unitPickerAdapter = UnitPickerAdapter(view.context,viewModel)
    view.adapter = unitPickerAdapter
    unitPickerAdapter.notifyDataSetChanged()

}
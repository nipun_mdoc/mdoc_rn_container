package de.mdoc.modules.devices.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.network.managers.MdocManager
import de.mdoc.pojo.MyDevices
import de.mdoc.util.MdocAppHelper
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class VitalConnectedAdapter(var context: Context, var items:ArrayList<MyDevices>) :
        RecyclerView.Adapter<VitalConnectedViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VitalConnectedViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.placeholder_vitals_connected,
                parent, false)
        context = parent.context

        return VitalConnectedViewHolder(view)
    }

    override fun onBindViewHolder(holder: VitalConnectedViewHolder, position: Int) {

        val item = items[position]
        holder.updateDevices(item)

        holder.layout.setOnClickListener {
            val deviceName = items[position].deviceName
            val bundle = Bundle()
            bundle.putString(MdocConstants.DEVICE_NAME, deviceName)
            it.findNavController().navigate(R.id.fragmentDevicesResults, bundle)
        }

        holder.removeDevice.setOnClickListener {
            val activity = context as MdocActivity

            val alert = AlertDialog.Builder(
                    context)
            alert.setTitle("Alert!!")
            alert.setMessage("Are you sure to delete record")
            alert.setPositiveButton(context.getString(R.string.yes)) { dialog, _ ->
                disconnectDevice()
                items.removeAt(holder.adapterPosition)
                notifyDataSetChanged()
                dialog.dismiss()
                activity.findNavController(R.id.navigation_host_fragment).popBackStack(R.id.fragmentDevices, false)

            }
            alert.setNegativeButton(context.getString(R.string.no)) { dialog, _ -> dialog.dismiss() }
            alert.show()
        }
    }

    private fun disconnectDevice() {
        val userId = MdocAppHelper.getInstance().userId

        MdocManager.disconnectDevice(userId, object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    Timber.v("success")
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.v("Fail")
            }
        })
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class VitalConnectedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var imageView = itemView.findViewById<View>(R.id.imageMyDevice) as ImageView
    var deviceName = itemView.findViewById<View>(R.id.txtMyDevice) as TextView
    val transferTime=itemView.findViewById<View>(R.id.txtTransferTime) as TextView
    var layout = itemView.findViewById<View>(R.id.myDeviceRl) as ConstraintLayout
    var removeDevice= itemView.findViewById<View>(R.id.ic_popup_remove) as ImageView

    fun updateDevices(devices: MyDevices) {
        val uri = devices.imgUri
        val resources = imageView.resources.getIdentifier(uri, "drawable", imageView.context.packageName)
        imageView.setImageResource(resources)
        deviceName.text = devices.deviceName
        transferTime.text=devices.time
    }
}
package de.mdoc.data.diary

data class Data(
    val autoShare: Boolean,
    val coding: Coding,
    val cts: Long,
    val freeTextEntryAllowed: Boolean,
    val id: String,
    val pollId: String?,
    val pollType: String,
    val shareAllowed: Boolean,
    val uts: Long,
    val title: String
)
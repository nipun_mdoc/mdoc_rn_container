package de.mdoc.modules.my_stay.search

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.my_stay.adapters.SearchClinicAdapter
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.network.RestClient
import de.mdoc.pojo.ClinicDetails
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.bindVisibleInvisible
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_search_clinic.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit


class SearchClinicFragment : MdocFragment() {

    internal var activity: MdocActivity?=null
    internal var adapter: SearchClinicAdapter?=null
    private var oldQuery : String? = null

    private val viewModel by viewModel {
        SearchClinicViewModel(
                repository = RxStayRepository(RestClient.getService())
        )
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_search_clinic
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity

    }
    override val navigationItem: NavigationItem = NavigationItem.SearchClinic

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        bindVisibleInvisible(progressBar, viewModel.isLoading)
        bindVisibleGone(error, viewModel.isShowLoadingError)
        bindVisibleGone(clinicLv, viewModel.isShowContent)
        bindVisibleGone(noDataRl, viewModel.isNoContent)

        viewModel.searchResults.observe(this){

            adapter = SearchClinicAdapter(it, activity!!.applicationContext)
            clinicLv!!.adapter = adapter
            adapter!!.notifyDataSetChanged()

        }

        clinicLv?.setOnItemClickListener { _, _, position, _ ->

            val item = adapter!!.getItem(position) as ClinicDetails
            val bundle = Bundle()
            bundle.putSerializable(MdocConstants.CLINIC_DETAILS, item)
            bundle.putSerializable(MdocConstants.CURRENT_CLINIC_DATA, CurrentClinicData(item))

            val direction = SearchClinicFragmentDirections.actionToFragmentMyStay(CurrentClinicData(item))
            findNavController().navigate(direction)
        }

        RxTextView.textChanges(searchEdt)
                .debounce (
                        MdocConstants.SEARCH_WAIT_TIME.toLong(),
                        TimeUnit.MILLISECONDS,
                        AndroidSchedulers.mainThread()
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if(oldQuery != it.toString()) {
                        viewModel.requestSearch(it.toString())
                    }
                    oldQuery = it.toString()
                }

    }
}

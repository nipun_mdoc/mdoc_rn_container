package de.mdoc.network.certificate

import android.content.Context
import okhttp3.CertificatePinner
import timber.log.Timber

fun buildCertificatePinner(context: Context): CertificatePinner? {
    val array = context.resources.getStringArray(de.mdoc.R.array.certificate_pins).map {
        "sha256/$it"
    }.toTypedArray()
    return if (array.isNotEmpty()) {
        Timber.i("Certificate pins initialized")
        CertificatePinner.Builder()
            .add("sitdemo2.mdoc.one", *array)
            .build()
    } else null
}
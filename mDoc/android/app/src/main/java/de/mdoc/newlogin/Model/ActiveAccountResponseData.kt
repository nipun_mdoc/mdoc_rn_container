package de.mdoc.newlogin.Model

class ActiveAccountResponseData(
    val code: String,
    val `data`: Data,
    val message: String,
    val timestamp: Long
)


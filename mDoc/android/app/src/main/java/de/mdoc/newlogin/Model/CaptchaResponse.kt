package de.mdoc.newlogin.Model


data class CaptchaResponse(
    val code: String,
    val `data`: ImageObject,
    val message: String,
    val timestamp: Long
)

data class ImageObject(
    val image: String
)

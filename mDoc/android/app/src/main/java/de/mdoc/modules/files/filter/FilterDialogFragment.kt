package de.mdoc.modules.files.filter

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.network.RestClient
import de.mdoc.util.FullScreenDialogFragment
import de.mdoc.util.serializableArgument
import de.mdoc.viewmodel.ValueLiveData
import de.mdoc.viewmodel.bindEnableDisable
import de.mdoc.viewmodel.compositelist.CompositeListAdapter
import kotlinx.android.synthetic.main.fragment_files_filter.*
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat
import java.util.*


class FilterDialogFragment : FullScreenDialogFragment(), FilterCategoryListener {

    private var filter by serializableArgument<FilesFilter>("filter")

    companion object {
        fun <T> newInstance(filter: FilesFilter, target: T): FilterDialogFragment
                where T : Fragment, T : FileFilterChangedListener {
            return FilterDialogFragment().also {
                it.filter = filter
                it.setTargetFragment(target, 0)
            }
        }
    }

    private lateinit var viewModel: FilterViewModel

    private val format = DateTimeFormat.forPattern("dd.MM.yyyy")

    private val adapter = CompositeListAdapter().also {
        it.addAdapter(FilterCategoryAdapter(this))
        it.setHasStableIds(true)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = activity?.run {
            ViewModelProviders.of(this,
                    FilterViewModelFactory(FilterRepository(filesRepository = FilesRepository( service = RestClient.getService(), cache = FileCacheRepository)))).get(FilterViewModel::class.java)

        }?: throw Exception("Invalid Activity")
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(de.mdoc.R.layout.fragment_files_filter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryFiltersList.adapter = adapter
        categoryFiltersList.layoutManager = LinearLayoutManager(view.context)

        bindEnableDisable(btnClearAll, viewModel.isClearFilterButtonEnabled)
        viewModel.categoryFilterItems.observe(this) {
            adapter.setItems(it)
        }
        viewModel.from.observe(this) {
            fromDate.text = if (it != null) format.print(it) else "-"
        }
        viewModel.to.observe(this) {
            toDate.text = if (it != null) format.print(it) else "-"
        }

        btnCloseFilter.setOnClickListener {
            dismiss()
        }

        btnClearAll.setOnClickListener {
            viewModel.onDeleteAllFiltersClicked()
        }
        btnApplyFilter.setOnClickListener {
            dismiss()
            (targetFragment as? FileFilterChangedListener)?.onFileFilterChanged(
                    viewModel.getFilter()
            )
        }
        fromDate.setOnClickListener {
            chooseDateFrom()
        }
        toDate.setOnClickListener {
            chooseDateTo()
        }
    }

    override fun onCategoryChecked(item: FilterItem) {
        viewModel.onCategoryClicked(item.category)
    }

    private fun chooseDateFrom() {
        chooseDate(viewModel.from, LocalDate.now().minusMonths(1))
    }

    private fun chooseDateTo() {
        chooseDate(viewModel.to, LocalDate.now())
    }

    private fun chooseDate(target: ValueLiveData<LocalDate?>, defaultValue: LocalDate) {
        val initialValue = (target.get() ?: defaultValue)
                .toDateTimeAtStartOfDay().toCalendar(Locale.getDefault())
        DatePickerDialog(
                context!!,
                DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                    val calendar = Calendar.getInstance().apply {
                        set(Calendar.YEAR, year)
                        set(Calendar.MONTH, month)
                        set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    }
                    target.set(
                            LocalDate.fromCalendarFields(calendar)
                    )
                },
                initialValue.get(Calendar.YEAR),
                initialValue.get(Calendar.MONTH),
                initialValue.get(Calendar.DAY_OF_MONTH)
        ).show()
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.setWindowAnimations(de.mdoc.R.style.FilterDialogAnimation)
    }
}
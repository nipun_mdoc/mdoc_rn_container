package de.mdoc.modules.meal_plan;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.Optional;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.interfaces.RecyclerViewClickListener;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapter;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapterTabletFuture;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapterTabletPast;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.VoteRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.VoteResponse;
import de.mdoc.pojo.MealChoice;
import de.mdoc.pojo.MealOffer;
import de.mdoc.pojo.MealPlan;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 1/26/18.
 */

public class MealPlanCantChooseTablet extends MealUtil{

    private static String ITEMS = "items";
    private static String DAY_INDEX = "day_index";
    private static String TODAY_INDEX = "today_index";

    @BindView(R.id.monthAndDateTv)
    TextView monthAndDateTv;
    @BindView(R.id.container)
    LinearLayout container;

    @BindView(R.id.dayOneText)
    TextView dayOneText;
    @BindView(R.id.dayTwoText)
    TextView dayTwoText;
    @BindView(R.id.dayThreeText)
    TextView dayThreeText;
    @Nullable
    @BindView(R.id.dayFourText)
    TextView dayFourText;
    @Nullable
    @BindView(R.id.dayFiveText)
    TextView dayFiveText;

    @BindView(R.id.dayOneImage)
    ImageView dayOneImage;
    @BindView(R.id.dayTwoImage)
    ImageView dayTwoImage;
    @BindView(R.id.dayThreeImage)
    ImageView dayThreeImage;
    @Nullable
    @BindView(R.id.dayFourImage)
    ImageView dayFourImage;
    @Nullable
    @BindView(R.id.dayFiveImage)
    ImageView dayFiveImage;
    @BindView(R.id.txtHelloName)
    TextView txtHelloName;


    MealOfferAdapter mealOfferAdapter;
    MealOfferAdapterTabletFuture adapter;
    private int dayIndex = 0;
    private int todayIndex = -1;
    private int totalIndex = 0;
    private int firstDayClick, secondDayClick, thirdDayClick, fourthDayClick, fiveDayClick;
    private long checkTimestamp;
    private MdocActivity activity;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_cant_choose_meal_tablet;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        items.clear();
        mealChoices.clear();
        progressDialog = activity.getParentProgressDialog();
        if (progressDialog != null) {
            progressDialog.setCancelable(false);
        }

        String patientName =  MdocAppHelper.getInstance().getUserFirstName() + " " + MdocAppHelper.getInstance().getUserLastName();
        txtHelloName.setText(getResources().getString(R.string.hello) + " " + patientName + ",");

        if (!getResources().getBoolean(R.bool.phone)) {
            if (savedInstanceState != null) {
                dayIndex = savedInstanceState.getInt(DAY_INDEX);
                todayIndex = savedInstanceState.getInt(TODAY_INDEX);
                items.clear();
                items.addAll((ArrayList<MealPlan>)savedInstanceState.getSerializable(ITEMS));
                setData();
                setMonthAndDateTv(dayIndex);
            } else {
                getMealInfo();

            }
        }
    }

    @Override
    void setData() {

        for (int i = 0; i < items.size(); i++) {
            if (MdocUtil.isToday(items.get(i).getDateNumber())) {
                todayIndex = i;
                dayIndex = i;
                setMonthAndDateTv(i);
            }
        }
        setLayout();
    }

    private void setMonthAndDateTv(int i) {
        resetTextViewColors();

        if (checkTimestamp == items.get(items.size() - 1).getDateNumber()) {
            return;
        }
        dayOneText.setText(getDate(items.get(i).getDateNumber()));
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        setImageSelected(i, dayOneImage);
        firstDayClick = i;

        i++;
        dayTwoText.setText(getDate(items.get(i).getDateNumber()));
        setImageSelected(i, dayTwoImage);
        secondDayClick = i;

        i++;
        dayThreeText.setText(getDate(items.get(i).getDateNumber()));
        setImageSelected(i, dayThreeImage);
        checkTimestamp = items.get(i).getDateNumber();
        totalIndex = i;
        thirdDayClick = i;

        if (dayFourText != null) {
            i++;
            dayFourText.setText(getDate(items.get(i).getDateNumber()));
            setImageSelected(i, dayFourImage);
            fourthDayClick = i;
        }
        if (dayFiveText != null) {
            i++;
            dayFiveText.setText(getDate(items.get(i).getDateNumber()));
            setImageSelected(i, dayFiveImage);
            checkTimestamp = items.get(i).getDateNumber();
            totalIndex = i;
            fiveDayClick = i;
        }
        setLayout();
    }

    private void setImageSelected(int i, ImageView imageView) {
        if (checkSelected(i))
            imageView.setVisibility(View.VISIBLE);
        else
            imageView.setVisibility(View.INVISIBLE);
    }

    private boolean checkSelected(int index) {
        MealPlan item = items.get(index);
        MealOffer selected = getSelectedOffer(item.getOffers());

        return selected.isSelected();
    }

    private void setLayout() {

        container.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(activity);
        View today = inflater.inflate(R.layout.layout_cant_choose_today, null, false);
        View future = inflater.inflate(R.layout.layout_meal_future, null, false);
        View past = inflater.inflate(R.layout.layout_cant_choose_past, null, false);
        if(items.size() > 0 && dayIndex != -1) {
            MealPlan item = items.get(dayIndex);
        if (MdocUtil.isPast(items.get(dayIndex).getDateNumber())) {
            container.addView(past);
            setPastLayout(item);
        } else if (MdocUtil.isToday(items.get(dayIndex).getDateNumber())) {
            if(item.isMealPlanRatingRule() && getResources().getBoolean(R.bool.rate_meal_after_13)){
                container.addView(past);
                setPastLayout(item);
            }else{
                container.addView(today);
                setTodayLayout(item);
            }
        } else {
            container.addView(future);
            setFutureLayout(dayIndex);
        }
        }

    }

    private void resetTextViewColors() {
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        dayTwoText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        dayThreeText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        if (dayFourText != null)
            dayFourText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        if (dayFiveText != null)
            dayFiveText.setTextColor(ContextCompat.getColor(activity, R.color.black));
    }

    @OnClick(R.id.leftDayIv)
    public void onLeftMonthIvClick() {
        if (dayIndex > 0) {
            Timber.i("onLeftMonthIvClick");
            checkTimestamp = 0;
            dayIndex--;
            setMonthAndDateTv(dayIndex);
        }
    }

    @OnClick(R.id.rightDayIv)
    public void onRightMonthIvClick() {

        if (totalIndex < items.size() - 1) {
            dayIndex++;
            setMonthAndDateTv(dayIndex);
        }
    }

    private String getDate(long timeStamp) {

        try {
            DateFormat sdf = new SimpleDateFormat("MMM dd", Locale.getDefault());
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    private MealOffer getSelectedOffer(ArrayList<MealOffer> offers) {
        for (MealOffer mo : offers) {
            if (mo.isSelected()) {
                return mo;
            }
        }
        return offers.get(0);
    }


    MealOfferAdapterTabletPast pastAdapter;
    RecyclerViewClickListener listener;
    private void setPastLayout(final MealPlan item) {
        final LinearLayout yesterdayLl = container.findViewById(R.id.yesterdayLl);
        final RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager

        listener = listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, final int position) {
                if(item.getOffers().get(position).isDessert() || item.getOffers().get(position).isSoup()){
                    return;
                }

                mealOfferRv.setVisibility(View.GONE);
                yesterdayLl.setVisibility(View.VISIBLE);

                TextView voteMealTv = container.findViewById(R.id.voteMealTv);
                Button confirmBtn = container.findViewById(R.id.confirmBtn);

                absoluteIv = container.findViewById(R.id.absoluteIv);
                yesIv = container.findViewById(R.id.yesIv);
                slightlyYesIv = container.findViewById(R.id.slightlyYesIv);
                mediumIv = container.findViewById(R.id.mediumIv);
                noIv = container.findViewById(R.id.noIv);
                voteEdt = container.findViewById(R.id.voteEdt);

                absoluteIv.setTag(MdocConstants.NOT_SELECTED);
                yesIv.setTag(MdocConstants.NOT_SELECTED);
                slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
                mediumIv.setTag(MdocConstants.NOT_SELECTED);
                noIv.setTag(MdocConstants.NOT_SELECTED);

                voteMealTv.setText(item.getOffers().get(position).getDescription());

                absoluteIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onAbsoluteIvClick();
                        }
                    }
                });

                yesIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onYesIvClick();
                        }
                    }
                });

                slightlyYesIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onSlightlYesIvClick();
                        }
                    }
                });

                mediumIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onMediumIvClick();
                        }
                    }
                });

                noIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onNoIvClick();
                        }
                    }
                });

                confirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onConfirmBtnClick(item, item.getOffers().get(position), mealOfferRv, yesterdayLl);
                        }
                        Timber.i("On clicked %s", new Gson().toJson(item));
                    }
                });

                if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                    voteEdt.setEnabled(true);
                    voteEdt.setFocusable(true);
                    voteEdt.setFocusableInTouchMode(true);
                }else{
                    voteEdt.setEnabled(false);
                    voteEdt.setFocusable(false);
                    voteEdt.setFocusableInTouchMode(false);
                }


                setRated(item, item.getOffers().get(position).get_id());

            }
        };

        pastAdapter = new MealOfferAdapterTabletPast(item.getOffers(), activity, item.getMealPlanId(), item.getMealChoice() != null ? item.getMealChoice().getMealChoiceId() : "", listener, item.getMealChoice());

        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));


        mealOfferRv.setAdapter(pastAdapter);

        mealOfferRv.setVisibility(View.VISIBLE);
        yesterdayLl.setVisibility(View.GONE);
    }


    private void setTodayLayout(MealPlan item) {
        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers();

        adapter = null;
        adapter = new MealOfferAdapterTabletFuture(tommorow, activity, items.get(dayIndex).getMealPlanId(), items.get(dayIndex).getMealChoice() != null ? items.get(dayIndex).getMealChoice().getMealChoiceId() : "");

        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        mealOfferRv.setAdapter(adapter);
    }

    private void setFutureLayout(int dayIndex) {
        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers(); //getTomorrowsMealOffer(thisWeek.getMealPlanOld().getWeeklyMealOffer(), nextWeek.getMealPlanOld().getWeeklyMealOffer());

        MealOfferAdapter.previousItem = null;
        mealOfferAdapter = null;
        mealOfferAdapter = new MealOfferAdapter(tommorow, activity, items.get(dayIndex).getMealPlanId(), items.get(dayIndex).getMealChoice() != null ? items.get(dayIndex).getMealChoice().getMealChoiceId() : "");
        mealOfferRv.setAdapter(mealOfferAdapter);
    }

    public void onConfirmBtnClick(final MealPlan mealPlan, MealOffer offer, RecyclerView mealOfferRv, LinearLayout yesterdayLl) {

        if (getReview() != null) {

            final VoteRequest request = new VoteRequest();
            request.setCaseId(MdocAppHelper.getInstance().getCaseId());
            request.setDescription(voteEdt.getText().toString());
            request.setMealId(offer.get_id());
            request.setMealPlanId(mealPlan.getMealPlanId());
            request.setReview(getReview());

            if(mealPlan.getMealChoice() == null || mealPlan.getMealChoice().getMealChoiceId() == null || mealPlan.getMealChoice().getMealChoiceId().isEmpty()) {
                MdocManager.vote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if(isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));
                                MealChoice choice = new MealChoice();
                                choice.setMealChoiceId(response.body().getData().getMealChoiceId());
                                choice.setReview(getReview());
                                choice.setDescription(voteEdt.getText().toString());
                                choice.setMealId(response.body().getData().getMealId());

                                if (mealPlan.getMealChoice() == null) {
                                    mealPlan.setMealChoice(choice);
                                }

                                mealOfferRv.setVisibility(View.VISIBLE);
                                yesterdayLl.setVisibility(View.GONE);
                                voteEdt.setText("");
                                pastAdapter = new MealOfferAdapterTabletPast(mealPlan.getOffers(), activity, mealPlan.getMealPlanId(), mealPlan.getMealChoice() != null ? mealPlan.getMealChoice().getMealChoiceId() : "", listener, mealPlan.getMealChoice());
                                mealOfferRv.setAdapter(pastAdapter);
                            } else {
                                Timber.w("vote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                        Timber.w(throwable, "vote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), throwable.getMessage());
                        }
                    }
                });
            } else {
                request.setMealChoiceId(mealPlan.getMealChoice().getMealChoiceId());
                MdocManager.updateVote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if(isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));
                                mealPlan.getMealChoice().setMealChoiceId(response.body().getData().getMealChoiceId());
                                mealPlan.getMealChoice().setReview(getReview());
                                mealPlan.getMealChoice().setDescription(voteEdt.getText().toString());
                                mealPlan.getMealChoice().setMealId(response.body().getData().getMealId());
                                mealOfferRv.setVisibility(View.VISIBLE);
                                yesterdayLl.setVisibility(View.GONE);
                                voteEdt.setText("");
                                pastAdapter = new MealOfferAdapterTabletPast(mealPlan.getOffers(), activity, mealPlan.getMealPlanId(), mealPlan.getMealChoice() != null ? mealPlan.getMealChoice().getMealChoiceId() : "", listener, mealPlan.getMealChoice());
                                mealOfferRv.setAdapter(pastAdapter);
                            } else {
                                Timber.w("updateVote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                        Timber.w(throwable, "updateVote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), throwable.getMessage());
                        }
                    }
                });
            }
        }
    }

    private void setRated(MealPlan item, String mealId) {
        if(item.getMealChoice() != null && item.getMealChoice().getMealId().equals(mealId)){
            if (item.getMealChoice() != null && item.getMealChoice().getReview() != null && !item.getMealChoice().getReview().isEmpty()) {
                switch (item.getMealChoice().getReview()) {
                    case MdocConstants.POOR:
                        mediumIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.NORMAL:
                        slightlyYesIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.EXCELLENT:
                        yesIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.VERY_POOR:
                        noIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.ABSOLUTE:
                        absoluteIv.setTag(MdocConstants.SELECTED);
                        break;
                }

                if (!item.getMealChoice().getDescription().isEmpty()) {
                    voteEdt.setText(item.getMealChoice().getDescription());
                }
            }
        }
        setSelection();
    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable(ITEMS, items);
        savedInstanceState.putInt(DAY_INDEX, dayIndex);
        savedInstanceState.putInt(TODAY_INDEX, todayIndex);
    }

    private void setLayoutFromClick(int indexClick){

        container.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(activity);
        View today= inflater.inflate(R.layout.layout_cant_choose_today, null, false);
        View future= inflater.inflate(R.layout.layout_meal_future, null, false);
        View past= inflater.inflate(R.layout.layout_cant_choose_past, null, false);

        MealPlan item = items.get(indexClick);

        if(MdocUtil.isPast(items.get(indexClick).getDateNumber())){
            container.addView(past);
            setPastLayout(item);
        }else if(MdocUtil.isToday(items.get(indexClick).getDateNumber())){
            if(item.isMealPlanRatingRule() && getResources().getBoolean(R.bool.rate_meal_after_13)){
                container.addView(past);
                setPastLayout(item);
            }else{
                container.addView(today);
                setTodayLayout(item);
            }
        }else {
            container.addView(future);
            setFutureLayout(indexClick);
        }
    }

    @OnClick(R.id.linerDayOne)
    public void onDayOneClick() {
        resetTextViewColors();
        setLayoutFromClick(firstDayClick);
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    @OnClick(R.id.linerDayTwo)
    public void onDayTwoClick() {
        resetTextViewColors();
        setLayoutFromClick(secondDayClick);
        dayTwoText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    @OnClick(R.id.linerDayThree)
    public void onDayThreeClick() {
        resetTextViewColors();
        setLayoutFromClick(thirdDayClick);
        dayThreeText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    @Optional
    @OnClick(R.id.linerDayFour)
    public void onDayFourClick() {
        resetTextViewColors();
        setLayoutFromClick(fourthDayClick);
        dayFourText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

    @Optional
    @OnClick(R.id.linerDayFive)
    public void onDayFiveClick() {
        resetTextViewColors();
        setLayoutFromClick(fiveDayClick);
        dayFiveText.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
    }

}

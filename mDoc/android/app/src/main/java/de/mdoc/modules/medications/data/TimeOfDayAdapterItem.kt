package de.mdoc.modules.medications.data

data class TimeOfDayAdapterItem(var dayPeriod: String, var amount: String, var time: String)
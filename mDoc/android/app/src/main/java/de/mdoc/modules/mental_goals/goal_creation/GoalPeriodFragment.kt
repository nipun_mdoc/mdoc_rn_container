package de.mdoc.modules.mental_goals.goal_creation

import android.app.DatePickerDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_goal_period.*
import java.util.*

class GoalPeriodFragment: MdocFragment() {

    lateinit var activity: MdocActivity
    private var customPeriod: Long = 0
    override fun setResourceId(): Int {
        return R.layout.fragment_goal_period
    }

    private val args: GoalPeriodFragmentArgs by navArgs()
    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        prepareUi()
        setupListeners()
        setupRadioGroupListeners()
    }

    private fun prepareUi() {
        hasPreviousSelection()
        if (args.isEditing) {
            ll_toolbar?.visibility = View.GONE
            txt_title?.visibility = View.GONE
            txt_label?.visibility = View.GONE
            txt_save?.visibility = View.VISIBLE
        }
        else {
            progressBar?.progress = 2
        }
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            if (args.isEditing) {
                findNavController().popBackStack()
            }
            else {
                val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
                if (!hasGoalOverview) {
                    val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                    if (!hasMoreFragment) {
                        val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                        if (!hasDashboardFragment) {
                            findNavController().popBackStack()
                        }
                    }
                }
            }
        }

        txt_next?.setOnClickListener {
            if (itemIsSelected()) {
                findNavController().navigate(R.id.action_goalPeriodFragment_to_actionItemsFragment)
            }
            else {
                MdocUtil.showToastShort(activity, resources.getString(R.string.make_selection))
            }
        }
        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }

        txt_save?.setOnClickListener {
            if (itemIsSelected()) {
                findNavController().navigate(R.id.action_goalPeriodFragment2_to_editGoalFragment)
            }
            else {
                MdocUtil.showToastShort(activity, resources.getString(R.string.make_selection))
            }
        }
    }

    private fun setupRadioGroupListeners() {
        val itemCount = radio_group.childCount
        for (position in 0..itemCount) {
            if (radio_group.getChildAt(position) != null) {
                val textView = radio_group.getChildAt(position) as TextView
                textView.setOnClickListener {
                    if (textView == txt_chose_date) {
                        clearButtonSelection()
                        showDatePicker()
                    }
                    else {
                        handleUi(textView)
                    }
                }
            }
        }
    }

    private fun handleUi(textView: View) {
        clearButtonSelection()

        if (textView.backgroundTintList ==
            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary8
                                                         ))) {
            textView.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, R.color.colorSecondary32
                                          ))
        }
        else {
            textView.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, R.color.colorSecondary8
                                          ))
        }
    }

    private fun clearButtonSelection() {
        for (index in 0..radio_group.childCount) {
            val textView = radio_group.getChildAt(index)
            textView?.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, R.color.colorSecondary8))
        }
    }

    private fun showDatePicker() {
        val currentCalendar: Calendar = Calendar.getInstance()
        val calendar: Calendar = Calendar.getInstance()
        val mYear = calendar.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(activity, { _, year, month, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            if (calendar.timeInMillis < currentCalendar.timeInMillis) {
                calendar.set(Calendar.YEAR, currentCalendar.get(Calendar.YEAR))
                calendar.set(Calendar.MONTH, currentCalendar.get(Calendar.MONTH))
                calendar.set(Calendar.DAY_OF_MONTH, currentCalendar.get(Calendar.DAY_OF_MONTH))
            }
            txt_chose_date?.text = MdocUtil.getParsedDateDefault(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar)
            txt_chose_date?.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, R.color.colorSecondary32))

            customPeriod = calendar.timeInMillis
        }, mYear, mMonth, mDay)
        datePickerDialog.datePicker.minDate = Calendar.getInstance().timeInMillis
        datePickerDialog.show()
    }

    private fun itemIsSelected(): Boolean {
        val itemCount = radio_group.childCount
        for (position in 0..itemCount) {
            if (radio_group.getChildAt(position) != null) {
                val textView = radio_group.getChildAt(position) as TextView
                if (textView.backgroundTintList == ColorStateList.valueOf(
                            ContextCompat.getColor(activity, R.color.colorSecondary32))) {
                    parseValueInMs(textView)
                    return true
                }
            }
        }
        return false
    }

    private fun parseValueInMs(textView: TextView) {
        val calendar: Calendar = Calendar.getInstance()
        val timePeriod: Long

        when (textView) {
            txt_day        -> {
                timePeriod = calendar.timeInMillis + 86400000
                MentalGoalsUtil.timePeriod = timePeriod
                MentalGoalsUtil.customDateSelected = false
            }

            txt_week       -> {
                timePeriod = calendar.timeInMillis + 604800000
                MentalGoalsUtil.timePeriod = timePeriod
                MentalGoalsUtil.customDateSelected = false
            }

            txt_month      -> {
                timePeriod = calendar.timeInMillis + 2592000000
                MentalGoalsUtil.timePeriod = timePeriod
                MentalGoalsUtil.customDateSelected = false
            }

            txt_year       -> {
                timePeriod = calendar.timeInMillis + 31536000000
                MentalGoalsUtil.timePeriod = timePeriod
                MentalGoalsUtil.customDateSelected = false
            }

            txt_chose_date -> {
                if (customPeriod != 0L) {
                    timePeriod = customPeriod
                    MentalGoalsUtil.timePeriod = timePeriod
                    MentalGoalsUtil.customDateSelected = true
                }
            }
        }
    }

    private fun hasPreviousSelection() {
        clearButtonSelection()
        if (MentalGoalsUtil.timePeriod != 0L) {
            val calendar: Calendar = Calendar.getInstance()
            val selectedTime = MentalGoalsUtil.timePeriod

            if (MentalGoalsUtil.customDateSelected) {
                txt_chose_date.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(activity, R.color.colorSecondary32))
                txt_chose_date?.text = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, selectedTime)
            }
            else {
                when (selectedTime - calendar.timeInMillis) {
                    in 1..86400000             -> {
                        txt_day.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary32))
                    }

                    in 86400001..604800000     -> {
                        txt_week.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary32))
                    }

                    in 604800001..2592000000   -> {
                        txt_month.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary32))
                    }

                    in 2592000001..31536000000 -> {
                        txt_year.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary32))
                    }

                    else                       -> {
                        txt_chose_date.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary32))
                        txt_chose_date?.text = MdocUtil.getDateFromMS(
                                MdocConstants.D_IN_WEEK_DD_MM_YY_FORMAT, selectedTime)
                    }
                }
            }
        }
    }
}
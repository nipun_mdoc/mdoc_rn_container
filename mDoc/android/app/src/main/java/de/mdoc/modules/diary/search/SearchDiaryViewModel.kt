package de.mdoc.modules.diary.search

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import de.mdoc.modules.common.viewmodel.BaseSearchViewModel
import de.mdoc.modules.diary.data.DiarySearchRequest
import de.mdoc.modules.diary.data.RxDiaryRepository
import de.mdoc.pojo.DiaryList
import de.mdoc.pojo.DiaryListUpdate
import de.mdoc.pojo.deleteDiaryPjo

class SearchDiaryViewModel(private val repository: RxDiaryRepository) : BaseSearchViewModel<List<DiaryList>>() {
    var searchText: String = ""
    var shouldShowDeleteMsg = MutableLiveData(false)

    override fun requestSearch(query: String) {
        super.requestSearch(query)
        if (query.isNotEmpty()) {
            searchText = query
            val request = DiarySearchRequest(query)
            repository.getAllDiaries(request = request, resultListener = ::onDataLoaded, errorListener = ::onError)
        }
    }

    fun onDataLoaded(data: List<DiaryList>?) {
        onDataLoaded(data, data?.size ?: 0)
    }

    fun onDataRefresh(data: List<deleteDiaryPjo>?) {
        if (searchText.isNotEmpty()) {
            val request = DiarySearchRequest(searchText)
            repository.getAllDiaries(request = request, resultListener = ::onDataLoaded, errorListener = ::onError)
        }
        shouldShowDeleteMsg.value = true
        Handler().postDelayed({
            shouldShowDeleteMsg.value = false
        }, 6000)
    }


    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }

    override fun requestDelete(query: DiaryList) {
        repository.getDiariesDeleteRequest(id = query.id , resultListener = ::onDataRefresh, errorListener = ::onError)

    }

    override fun requestUpdate(query: DiaryList) {
        var request = DiaryListUpdate()
        request.caseId=query.id
        request.clinicId=query.clinicId
        request.description=query.description
        request.diaryConfig=query.diaryConfig.toString()
        request.showToDoctor= query.showToDoctor
        request.start=query.start
        request.startTime=0
        request.title=query.title
        repository.getDiariesUpdateRequest(id = query.id,request = request, resultListener = ::onDataRefresh, errorListener = ::onError)
    }
}
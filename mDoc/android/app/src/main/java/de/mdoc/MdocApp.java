package de.mdoc;

import android.app.Application;
import android.content.Context;
import androidx.multidex.MultiDex;
import de.mdoc.network.RestClient;
import de.mdoc.modules.profile.patient_details.PatientDetailsHelper;
import de.mdoc.util.MdocAppHelper;
import rx.android.BuildConfig;
import timber.log.Timber;
//import io.fabric.sdk.android.Fabric;
//import com.crashlytics.android.Crashlytics;


/**
 * Created by ema on 4/7/17.
 */

public class MdocApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.uprootAll();
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
//      if (!BuildConfig.DEBUG) {
//          Fabric.with(this, new Crashlytics());
//      }

        PatientDetailsHelper.getInstance().init(this);
        MdocAppHelper.getInstance().init(this);
        RestClient.getInstance().init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
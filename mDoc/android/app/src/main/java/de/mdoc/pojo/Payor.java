package de.mdoc.pojo;

public class Payor {

    private String reference;

    public Payor(String reference) {
        this.reference = reference;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

}

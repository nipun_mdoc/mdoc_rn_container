package de.mdoc.modules.booking.data

import de.mdoc.constants.SHORT_TIME_FORMAT
import de.mdoc.util.MdocUtil
import org.joda.time.DateTime
import java.util.*

class AppointmentData(val totalCount: Int,
                      val list: List<ListItem>,
                      val moreDataAvailable: Boolean)

data class ListItem(
        val appointmentDetails: AppointmentDetails,
        val cts: Long,
        val deleted: Boolean,
        val id: String,
        val uts: Long
                   ) {

    fun isActiveHour(): Boolean {
        val shortFromTime = MdocUtil.getTimeFromMs(appointmentDetails.start, SHORT_TIME_FORMAT);
        val dt = DateTime()
        val hour = dt.hourOfDay
        val time = Integer.parseInt(shortFromTime)

        return hour == time
    }

    fun isAppointmentUpdated(): Boolean {
        val currentTime = Calendar.getInstance()
            .timeInMillis
        //      1800000 ms =30 min
        val minuteRange: Long = 1800000
        return uts > cts + 1000 && currentTime - uts < minuteRange
    }

    fun hasComment() = !appointmentDetails.comment.isNullOrEmpty()
}

data class AppointmentDetails(
        val clinicId: String,
        val comment: String?,
        val end: Long,
        val identifiers: List<Any>,
        val location: String,
        val participants: List<Participant>,
        val resourceType: String,
        val start: Long,
        val timeVisible: Boolean,
        val title: String,
        val visible: Boolean
                             )

data class Participant(
        val firstName: String,
        val lastName: String,
        val status: String,
        val title: String,
        val type: String,
        val username: String
                      )
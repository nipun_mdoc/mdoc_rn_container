package de.mdoc.modules.questionnaire.widget

import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.QuestionnaireResponse
import de.mdoc.pojo.QuestionnaireData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionnaireWidgetRepository {

    private val disposables = CompositeDisposable()

    fun getData(onResult: (List<SectionData>) -> Unit, onError: (Throwable) -> Unit) {
        MdocManager.getQuestionnaries(object : Callback<QuestionnaireResponse> {
            override fun onResponse(
                call: Call<QuestionnaireResponse>,
                response: Response<QuestionnaireResponse>
            ) {
                if (response.isSuccessful && response.body() != null) {
                    response.body()?.data?.let {
                        convert(it, onResult, onError)
                    } ?: run {
                        onError.invoke(Exception("Data is null"))
                    }
                } else {
                    onError.invoke(Exception(response.message()))
                }
            }

            override fun onFailure(call: Call<QuestionnaireResponse>, t: Throwable) {
                onError.invoke(t)
            }
        })
    }

    private fun convert(
        data: List<QuestionnaireData>,
        onResult: (List<SectionData>) -> Unit,
        onError: (Throwable) -> Unit
    ) {
        Single.fromCallable {
            convertContent(data)
        }.subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onResult, onError)
            .addTo(disposables)
    }

    private fun convertContent(data: List<QuestionnaireData>): List<SectionData> {
        return Section.values().map { section ->
            val listForSection = data.filter { questionnaireData ->
                section.typeKeys.contains(questionnaireData.pollType)

            }
            val started = listForSection.count {
                it.answerDetails.isStarted
            }
            val total = listForSection.size
            SectionData(
                type = section,
                started = started,
                total = total,
                displayName =listForSection.getOrNull(0)?.pollTypeDisplayValue?:""
            )

        }.filter {
            it.total > 0
        }
    }

    fun recycle() {
        disposables.dispose()
    }

}
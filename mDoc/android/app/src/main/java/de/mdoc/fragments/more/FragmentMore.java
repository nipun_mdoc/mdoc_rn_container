package de.mdoc.fragments.more;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationHandler;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.activities.navigation.NavigationViewModel;
import de.mdoc.databinding.FragmentMoreBinding;
import de.mdoc.fragments.NewBaseFragment;
import de.mdoc.newlogin.Interactor.LogiActivty;
import de.mdoc.util.MdocAppHelper;

/**
 * Created by ema on 6/22/17.
 */

public class FragmentMore extends NewBaseFragment implements View.OnClickListener {

    MdocActivity activity;

    private NavigationViewModel navigationViewModel;
    private NavigationHandler navigationHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        navigationViewModel = ((MdocActivity)getActivity()).getNavigationViewModel();
        navigationHandler = new NavigationHandler(getContext(), navigationViewModel, this);
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.More;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMoreBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_more, container, false);
        binding.setLifecycleOwner(this);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.signoutRl).setOnClickListener(this);
        activity = (MdocActivity) getActivity();
        navigationHandler.setupMenu(view.findViewById(R.id.mainMenuLayout));


        MdocAppHelper.getInstance().getNotificationIndex().observe(getViewLifecycleOwner(), new Observer<Integer>() {
            @Override
            public void onChanged(Integer count) {
                if (count != null && count > 0) {
                    view.findViewById(R.id.notifyIconMore).setVisibility(View.VISIBLE);
                    ((TextView)view.findViewById(R.id.badgeMoreTv)).setText(count.toString());

                } else {
                    view.findViewById(R.id.notifyIconMore).setVisibility(View.GONE);
                    ((TextView)view.findViewById(R.id.badgeMoreTv)).setText("");
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signoutRl) {
            if (MdocAppHelper.getInstance().isOptIn()) {
                Intent loginActivity = new Intent(activity, LogiActivty.class);
                loginActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                loginActivity.putExtra(LogiActivty.FORCED_LOGOUT, true);
                startActivity(loginActivity);
                activity.finish();
            } else {
                activity.onSignoutRlClick();
            }
        }
    }
}

package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import de.mdoc.R
import de.mdoc.modules.questionnaire.questionnaire_adapter.DropdownQuestionAdapter
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Option
import kotlinx.android.synthetic.main.question_dropdown_layout.view.*
import timber.log.Timber

class DropdownQuestionType: QuestionType() {


    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.question_dropdown_layout, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value = false
        val options = arrayListOf(Option())
        options.addAll(question.questionData.options)
        val questionSpinner = view.question_spinner
        val answer = question.answer
        val spinnerAdapter = DropdownQuestionAdapter(parent.context, options, question)
        questionSpinner?.adapter = spinnerAdapter

        try {
            val index: Int? =
                    options.indexOf(Option(answer.selection.getOrNull(0), answer.answerData, question.selectedLanguage))
            if (index != null) {
                questionSpinner?.setSelection(index)
            }
        } catch (e: Exception) {
            Timber.d("Can not map answer")
        }

        questionSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2 == 0) {
                    questionIsAnswered.value = false
                }
                else {
                    questionIsAnswered.value = true
                    val ans = options.getOrNull(p2)?.getValue(question.selectedLanguage) ?: ""
                    val key: String? = options.getOrNull(p2)?.key ?: ""
                    isNewAnswer = if (key.isNullOrEmpty()) {
                        false
                    }
                    else {
                        answers.clear()
                        answers.add(ans)
                        selection.clear()
                        selection.add(key)
                        true
                    }
                }
            }
        }

        mandatoryCheck(mandatory, null, question)

        return view
    }
}
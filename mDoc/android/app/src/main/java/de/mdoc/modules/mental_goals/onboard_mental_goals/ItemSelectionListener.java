package de.mdoc.modules.mental_goals.onboard_mental_goals;

public interface ItemSelectionListener {
    void pagerIndex(Integer index);

}

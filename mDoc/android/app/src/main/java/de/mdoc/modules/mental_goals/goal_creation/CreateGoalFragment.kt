package de.mdoc.modules.mental_goals.goal_creation

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.MentalGoalStatus
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.modules.mental_goals.data_goals.SingleMentalGoalResponse
import de.mdoc.modules.mental_goals.mental_goal_adapters.DashboardActionItemsAdapter
import de.mdoc.modules.mental_goals.mental_goal_adapters.DashboardWhenThenAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_create_goal.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CreateGoalFragment: MdocFragment() {
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_create_goal
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        fillUiWithPersistedData()
        txt_cancel?.visibility = View.VISIBLE
        progressBar?.progress = 6
    }

    private fun setupListeners() {
        txt_goal_create?.setOnClickListener {
            createMentalGoalApi()
        }

        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }
        txt_cancel?.setOnClickListener {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
    }

    private fun fillUiWithPersistedData() {
        txt_goal_title?.text = MentalGoalsUtil.goalTitle
        txt_goal_description?.text = MentalGoalsUtil.goalDescription
        txt_date_placeholder?.text = MdocUtil.getDateFromMS(
                MdocConstants.D_IN_WEEK_DD_MM_YY_FORMAT, MentalGoalsUtil.timePeriod)


        initActionsAdapter()
        initWhenThenAdapter()
    }

    private fun initWhenThenAdapter() {
        val adapterItems = MentalGoalsUtil.ifThenItems
        if (!adapterItems.isNullOrEmpty()) {
            val whenThenDashboardAdapter = DashboardWhenThenAdapter(activity, adapterItems)
            rv_when_then_dashboard?.layoutManager = LinearLayoutManager(
                    activity,
                    RecyclerView.VERTICAL,
                    false
                                                                       )
            rv_when_then_dashboard?.adapter = whenThenDashboardAdapter
        }
    }

    private fun initActionsAdapter() {
        val actionItems = MentalGoalsUtil.actionItems
        if (!actionItems.isNullOrEmpty()) {
            val actionsDashboardAdapter = DashboardActionItemsAdapter(activity, actionItems)
            rv_action_items_dashboard?.layoutManager = LinearLayoutManager(
                    activity,
                    RecyclerView.VERTICAL,
                    false
                                                                          )
            rv_action_items_dashboard?.adapter = actionsDashboardAdapter
        }
    }

    private fun createMentalGoalApi() {
        val request = MentalGoalsRequest()

        with(request) {
            title = MentalGoalsUtil.goalTitle
            description = MentalGoalsUtil.goalDescription
            status = MentalGoalStatus.OPEN.toString()
            ifThenPlans = MentalGoalsUtil.ifThenItems
            dueDate = MentalGoalsUtil.timePeriod
            items = MentalGoalsUtil.actionItems
        }

        MdocManager.postMentalGoals(request, object: Callback<SingleMentalGoalResponse> {
            override fun onResponse(call: Call<SingleMentalGoalResponse>,
                                    response: Response<SingleMentalGoalResponse>) {
                if (response.isSuccessful) {
                    MentalGoalsUtil.showCreatedMessage = true
                    findNavController().navigate(R.id.navigation_mental_goal)
                }
                else {
                    MdocUtil.showToastLong(activity, response.message())
                }
            }

            override fun onFailure(call: Call<SingleMentalGoalResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
            }
        })
    }
}
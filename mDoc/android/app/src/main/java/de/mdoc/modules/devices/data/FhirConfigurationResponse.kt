package de.mdoc.modules.devices.data

data class FhirConfigurationResponse(
        val data: List<FhirConfigurationData>
)

{
    fun manualDevice():List<Measurement>{
        for(item in data){
            if(item.deviceCode == "MANUAL"){
                return item.measurements?: arrayListOf()
            }
        }
        return emptyList()
    }
    fun physicalDevices():List<FhirConfigurationData>{
        val listOfDevices= ArrayList<FhirConfigurationData>()
        for(item in data){
            if(item.deviceCode != "MANUAL"){
                listOfDevices.add(item)
            }
        }
        return listOfDevices
    }
}
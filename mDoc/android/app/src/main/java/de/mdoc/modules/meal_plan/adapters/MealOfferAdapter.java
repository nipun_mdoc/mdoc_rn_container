package de.mdoc.modules.meal_plan.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.VoteRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.network.response.VoteResponse;
import de.mdoc.pojo.MealOffer;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 4/19/17.
 */

public class MealOfferAdapter extends RecyclerView.Adapter<MealOfferAdapter.ViewHolder> {

    ArrayList<MealOffer> items;
    private LayoutInflater inflater;
    Context context;
    ProgressDialog progressDialog;
    String mealChoiceId;
    public static MealOffer previousItem = null;
    private String mealPlanId;

    public MealOfferAdapter(ArrayList<MealOffer> items, Context context, String mealPlanId, String mealChoiceId) {
        this.items = items;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mealChoiceId = mealChoiceId;
        this.mealPlanId = mealPlanId;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.meal_offer_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MealOffer item = items.get(position);

        if (item.isSoup()) {
            holder.mealNameTv.setText(context.getString(R.string.soup));
        } else {
            holder.mealNameTv.setText(item.getName(context.getResources()));
        }
        holder.mealDescriptionTv.setText(item.getDescription());

        if (item.getCalories() > 0) {
            holder.kcalTv.setVisibility(View.VISIBLE);
            holder.kcalTv.setText("Kcal: " + item.getCalories());
        } else {
            holder.kcalTv.setVisibility(View.GONE);
        }

        if (context.getResources().getBoolean(R.bool.can_user_choose_meal)) {
            holder.selectBtn.setVisibility(View.VISIBLE);
            if (item.isSelected()) {
                previousItem = item;
                holder.rootRl.setBackground(ContextCompat.getDrawable(context, R.drawable.gray_yellow_storke_rr));
                holder.selectedIv.setVisibility(View.VISIBLE);

                holder.selectBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.button_deselect));
                holder.selectBtn.setTextColor(ContextCompat.getColor(context, R.color.cool_blue));
                holder.selectBtn.setText(context.getString(R.string.deselect_menu));
            } else {
                holder.rootRl.setBackground(ContextCompat.getDrawable(context, R.drawable.platinum_meal_rr));
                holder.selectedIv.setVisibility(View.GONE);
                holder.selectBtn.setText(context.getString(R.string.select_menu));
                holder.selectBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.button_select));
                holder.selectBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
            }
            if (item.isDessert() || item.isSoup()) {
                holder.selectBtn.setVisibility(View.GONE);
            } else {
                holder.selectBtn.setVisibility(View.VISIBLE);
            }
        } else {
            holder.selectBtn.setVisibility(View.GONE);
        }

        if (item.getOptions().contains(MdocConstants.GLUTEN_FREE)) {
            holder.gfIv.setVisibility(View.VISIBLE);
        } else {
            holder.gfIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.VEGETARIAN)) {
            holder.veganIv.setVisibility(View.VISIBLE);
        } else {
            holder.veganIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.PIG)) {
            holder.pigIv.setVisibility(View.VISIBLE);
        } else {
            holder.pigIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.CHICKEN)) {
            holder.chickenIv.setVisibility(View.VISIBLE);
        } else {
            holder.chickenIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.COW)) {
            holder.cowIv.setVisibility(View.VISIBLE);
        } else {
            holder.cowIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.FISH)) {
            holder.fishIv.setVisibility(View.VISIBLE);
        } else {
            holder.fishIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.INFO)) {
            holder.infoIv.setVisibility(View.VISIBLE);
        } else {
            holder.infoIv.setVisibility(View.GONE);
        }
        if(item.isKosher()){
            holder.kosherIv.setVisibility(View.VISIBLE);
        }else{
            holder.kosherIv.setVisibility(View.GONE);
        }

        holder.selectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {

                progressDialog.show();

                if (holder.selectBtn.getText().equals(context.getString(R.string.deselect_menu))) {
                    MdocManager.deselectMeal(mealChoiceId, new Callback<MdocResponse>() {
                        @Override
                        public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                            if (response.isSuccessful()) {
                                mealChoiceId = null;
                                item.setSelected(false);
                                notifyDataSetChanged();
                                holder.selectBtn.setText(context.getString(R.string.select_menu));
                                holder.selectBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.button_select));
                                holder.selectBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
                                MdocUtil.showToastShort(context, context.getString(R.string.meal_choice_updated));
                            } else {
                                Timber.w("deselectMeal %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(context, ErrorUtilsKt.getErrorMessage(response));
                            }
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onFailure(Call<MdocResponse> call, Throwable throwable) {
                            Timber.w(throwable, "deselectMeal");
                            progressDialog.dismiss();
                            MdocUtil.showToastShort(context, throwable.getMessage());
                        }
                    });
                } else {

                    final VoteRequest request = new VoteRequest();
                    request.setCaseId(MdocAppHelper.getInstance().getCaseId());
                    request.setMealId(item.get_id());
                    request.setMealPlanId(mealPlanId);

                    if (mealChoiceId == null || mealChoiceId.equals("")) {

                        MdocManager.vote(request, new Callback<VoteResponse>() {
                            @Override
                            public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    mealChoiceId = response.body().getData().getMealChoiceId();
                                    progressDialog.dismiss();

                                    if (previousItem != null) {
                                        previousItem.setSelected(false);
                                    }
                                    item.setSelected(true);
                                    notifyDataSetChanged();
                                    holder.selectBtn.setText(context.getString(R.string.select_menu));
                                    holder.selectBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.button_select));
                                    holder.selectBtn.setTextColor(ContextCompat.getColor(context, R.color.white));
                                    MdocUtil.showToastShort(context, context.getString(R.string.meal_choice_updated));
                                } else {
                                    Timber.w("vote %s", APIErrorKt.getErrorDetails(response));
                                    MdocUtil.showToastShort(context, ErrorUtilsKt.getErrorMessage(response));
                                }
                            }

                            @Override
                            public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                                Timber.w(throwable, "vote");
                                progressDialog.dismiss();
                                MdocUtil.showToastShort(context, throwable.getMessage());
                            }
                        });
                    } else {
                        request.setMealChoiceId(mealChoiceId);
                        MdocManager.updateVote(request, new Callback<VoteResponse>() {
                            @Override
                            public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                                progressDialog.dismiss();
                                if (response.isSuccessful()) {
                                    mealChoiceId = response.body().getData().getMealChoiceId();

                                    if (previousItem != null) {
                                        previousItem.setSelected(false);
                                    }
                                    item.setSelected(true);
                                    notifyDataSetChanged();
                                    holder.selectBtn.setText(context.getString(R.string.deselect_menu));
                                    holder.selectBtn.setBackground(ContextCompat.getDrawable(context, R.drawable.button_deselect));
                                    holder.selectBtn.setTextColor(ContextCompat.getColor(context, R.color.cool_blue));
                                    MdocUtil.showToastShort(context, context.getString(R.string.meal_choice_updated));
                                } else {
                                    Timber.w("updateVote %s", APIErrorKt.getErrorDetails(response));
                                    MdocUtil.showToastShort(context, ErrorUtilsKt.getErrorMessage(response));
                                }
                            }

                            @Override
                            public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                                Timber.w(throwable, "updateVote");
                                progressDialog.dismiss();
                                MdocUtil.showToastShort(context, throwable.getMessage());
                            }
                        });
                    }
                }
            }
        });

        holder.infoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAditivesDialog(context, item.getInfo());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mealNameTv)
        TextView mealNameTv;
        @BindView(R.id.mealDescriptionTv)
        TextView mealDescriptionTv;
        @BindView(R.id.rateBtn)
        TextView selectBtn;
        @BindView(R.id.kcalTv)
        TextView kcalTv;

        @BindView(R.id.infoIv)
        ImageView infoIv;
        @BindView(R.id.gfIv)
        ImageView gfIv;
        @BindView(R.id.veganIv)
        ImageView veganIv;
        @BindView(R.id.pigIv)
        ImageView pigIv;
        @BindView(R.id.chickenIv)
        ImageView chickenIv;
        @BindView(R.id.cowIv)
        ImageView cowIv;
        @BindView(R.id.fishIv)
        ImageView fishIv;
        @BindView(R.id.rootRl)
        LinearLayout rootRl;
        @BindView(R.id.selectedIv)
        ImageView selectedIv;
        @BindView(R.id.kosherIv)
        ImageView kosherIv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }

    private void showAditivesDialog(Context context, ArrayList<String> infoList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.aditives_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        ImageView closeIv = view.findViewById(R.id.closeIv);
        ListView aditivesLv = view.findViewById(R.id.aditivesLv);
        HashMap<String, String> hashMapAllergens = MdocAppHelper.getInstance().getAllergens();
        HashMap<String, String> hashMapAdditives = MdocAppHelper.getInstance().getAdditives();

        ArrayList<String> showAllergens = new ArrayList<>();

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAdditives.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAllergens.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        ArrayAdapter<String> additivesAdapter =
                new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, showAllergens);

        aditivesLv.setAdapter(additivesAdapter);

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}

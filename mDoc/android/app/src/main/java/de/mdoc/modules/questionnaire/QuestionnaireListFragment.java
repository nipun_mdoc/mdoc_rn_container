package de.mdoc.modules.questionnaire;

import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.adapters.QuestionnaireExpandableAdapter;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.modules.common.dialog.ButtonClicked;
import de.mdoc.modules.common.dialog.CommonDialogFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.QuestionnaireResponse;
import de.mdoc.pojo.QuestionnaireData;
import de.mdoc.pojo.QuestionnaireProgress;
import de.mdoc.util.FragmentExtensionsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 4/13/17.
 */

public class QuestionnaireListFragment extends MdocFragment implements CommonDialogFragment.OnButtonClickListener, CommonDialogFragment.CancelListener {

    @BindView(R.id.expandableList)
    ExpandableListView listView;


    @BindView(R.id.status_view)
    ConstraintLayout statusLayout;

    @BindView(R.id.tv_title)
    TextView txtMessageTitle;


    @BindView(R.id.tv_message)
    TextView txtMessage;

    @BindView(R.id.img_status)
    ImageView imgStatus;


    MdocActivity activity;
    public ProgressDialog progressDialog;
    ArrayList<QuestionnaireData> anamnesis = new ArrayList<>();
    ArrayList<QuestionnaireData> feedback = new ArrayList<>();
    ArrayList<QuestionnaireData> scoring = new ArrayList<>();
    ArrayList<QuestionnaireProgress> types = new ArrayList<>();
    HashMap<QuestionnaireProgress, ArrayList<QuestionnaireData>> data = new HashMap<>();
    QuestionnaireExpandableAdapter adapter;

    @Override
    protected int setResourceId() {
        return R.layout.fragmnet_questionaire_list;
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Questionnaire;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_QUESTIONNAIRES, MdocConstants.HAS_HISTORY)) {
//        if (getResources().getBoolean(R.bool.has_questionnaire_history)) {
            setHasOptionsMenu(true);
        }
        activity = getMdocActivity();
        shouldShowDisclaimer();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);

        adapter = new QuestionnaireExpandableAdapter(activity, types, data);
        listView.setAdapter(adapter);

        listView.setOnChildClickListener((expandableListView, view, i, i1, l) -> {

            startQuestionnaire(i, i1);

            return false;
        });

        //TODO Handle tablet rotation by implementig live data
        if (!getResources().getBoolean(R.bool.phone)) {
            getQuestionnaires();
//            if(savedInstanceState != null){
//                listData.clear();
//                listData.addAll((ArrayList<QuestionnaireData>)savedInstanceState.getSerializable(DATA));
//                for(QuestionnaireData d : listData){
//                    if(!d.getPollType().equals(MdocConstants.CHECKLIST)){ // && d.isInSameDepartment() d.isActive() &&
//
//                        if(d.getPollType().equalsIgnoreCase(MdocConstants.ANAMNESIS) || d.getPollType().equalsIgnoreCase(MdocConstants.ANAMNESE)){
//                            anamnesis.add(d);
//                        }else if (d.getPollType().equalsIgnoreCase(MdocConstants.SCORING)){
//                            scoring.add(d);
//                        }else if (d.getPollType().equalsIgnoreCase(MdocConstants.FEEDBACK)){
//                            feedback.add(d);
//                        }
//                    }
//                }
//                setUpList();
//            }else{
//                getQuestionnaires();
//            }
        } else {
            getQuestionnaires();
        }

    }

    private void getQuestionnaires() {
        progressDialog.show();
        MdocManager.getQuestionnaries(new Callback<QuestionnaireResponse>() {
            @Override
            public void onResponse(Call<QuestionnaireResponse> call, Response<QuestionnaireResponse> response) {
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        anamnesis.clear();
                        scoring.clear();
                        feedback.clear();
                        if (response.body().getData().size() > 0) {
                            ArrayList<QuestionnaireData> filteredListData = filteredListData(response.body().getData());

                            if (filteredListData != null && filteredListData.size() > 0) {
                                for (QuestionnaireData d : filteredListData) {
                                    if (!d.getPollType().equals(MdocConstants.CHECKLIST)) { // && d.isInSameDepartment() d.isActive() &&

                                        if (d.getPollType().equalsIgnoreCase(MdocConstants.ANAMNESIS) || d.getPollType().equalsIgnoreCase(MdocConstants.ANAMNESE)) {
                                            anamnesis.add(d);
                                        } else if (d.getPollType().equalsIgnoreCase(MdocConstants.SCORING)) {
                                            scoring.add(d);
                                        } else if (d.getPollType().equalsIgnoreCase(MdocConstants.FEEDBACK)) {
                                            feedback.add(d);
                                        }
                                    }
                                }
                                setUpList();
                            } else {
                                setUiNoQuestionnaires();
                            }
                        } else {
                            setUiNoQuestionnaires();

                        }
                    } else {
                        Timber.w("getQuestionnaires %s", APIErrorKt.getErrorDetails(response));
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<QuestionnaireResponse> call, Throwable t) {
                Timber.w(t, "getQuestionnaires");
                progressDialog.dismiss();
            }
        });
    }

    private ArrayList<QuestionnaireData> filteredListData(ArrayList<QuestionnaireData> listData) {
        ArrayList<QuestionnaireData> outputList = new ArrayList<>();
        for (QuestionnaireData d : listData) {
            if (!d.getPollType().equals(MdocConstants.CHECKLIST)) {
                outputList.add(d);
            }
        }
        return outputList;
    }

    private void setUpList() {
        new Handler().post(() -> {
            types.clear();
            if (anamnesis.size() > 0) {
                QuestionnaireProgress first = new QuestionnaireProgress(
                        getString(R.string.anamnesis),
                        anamnesis.size(),
                        getStarted(anamnesis),
                        anamnesis.get(0).getPollTypeDisplayValue());

                types.add(first);
                data.put(first, anamnesis);
            }
            if (feedback.size() > 0) {
                QuestionnaireProgress second = new QuestionnaireProgress(
                        getString(R.string.feedback),
                        feedback.size(),
                        getStarted(feedback),
                        feedback.get(0).getPollTypeDisplayValue());

                types.add(second);
                data.put(second, feedback);
            }
            if (scoring.size() > 0) {
                QuestionnaireProgress third = new QuestionnaireProgress(
                        getString(R.string.scoring),
                        scoring.size(),
                        getStarted(scoring),
                        scoring.get(0).getPollTypeDisplayValue());

                types.add(third);
                data.put(third, scoring);
            }

            adapter.notifyDataSetChanged();
        });
    }

    private int getStarted(ArrayList<QuestionnaireData> questionnaire) {
        int counter = 0;
        for (QuestionnaireData d : questionnaire) {
            if (d.getAnswearsDetails().isStarted()) {
                counter++;
            }
        }
        return counter;
    }

    private void startQuestionnaire(int groupPosition, int childPosition) {
        QuestionnaireData item = (QuestionnaireData) adapter.getChild(groupPosition, childPosition);
        NavDirections actions = QuestionnaireListFragmentDirections.Companion.actionQuestionnaireListFragmentToQuestionnaireTransitionFragment(item.getPollId(),item.getPollAssignId(),false);
        Navigation.findNavController(activity, R.id.navigation_host_fragment).navigate(actions);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    private void setUiNoQuestionnaires() {
        if (listView != null && statusLayout != null) {
            listView.setVisibility(View.GONE);
            statusLayout.setVisibility(View.VISIBLE);
            imgStatus.setImageDrawable(getResources().getDrawable(R.drawable.ic_questionnaire_history));
            txtMessageTitle.setText(getResources().getString(R.string.no_questionnaire, getResources().getString(R.string.questionnaire_lower_case)));
            txtMessage.setText(getResources().getString(R.string.no_questionnaire_message, getResources().getString(R.string.questionnaire_lower_case)));

        }
    }

    private void shouldShowDisclaimer() {
        String moduleName = getResources().getString(R.string.questionnaire);
        List disclaimerModules = Arrays.asList(getResources().getStringArray(R.array.disclaimer_modules));
        if (disclaimerModules.contains(moduleName) && (!MdocAppHelper.getInstance().getDisclaimerSeenQuestionnaire())) {
            showDisclaimerDialog();
        }
    }

    private void showDisclaimerDialog() {
        new CommonDialogFragment.Builder()
                .title(getResources().getString(R.string.disclaimer))
                .description(getResources().getString(R.string.disclaimer_body))
                .setOnCancelListener(this)
                .setOnClickListener(this)
                .setPositiveButton(getResources().getString(R.string.disclaimer_accept))
                .navigateBackOnCancel(true)
                .build()
                .showNow(getChildFragmentManager(), "");
    }

    @Override
    public void onDialogButtonClick(ButtonClicked button) {
        if (button == ButtonClicked.POSITIVE) {
            MdocAppHelper.getInstance().setDisclaimerSeenQuestionnaire(true);
        }

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        FragmentExtensionsKt.onCreateOptionsMenu(this, menu, inflater, R.menu.menu_questionnaire);
        MenuItem item = menu.findItem(R.id.questionnaireHistory);
        if(getResources().getBoolean(R.bool.has_questionnaire_history)){
           item.setVisible(true);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.questionnaireHistory) {
            Navigation.findNavController(activity, R.id.navigation_host_fragment).navigate(R.id.action_questionnaireListFragment_to_questionnaireHistoryFragment);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCancelDialog() {
        getParentFragmentManager().popBackStack();
    }
}

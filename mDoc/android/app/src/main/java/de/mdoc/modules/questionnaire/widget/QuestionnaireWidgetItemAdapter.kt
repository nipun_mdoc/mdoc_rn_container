package de.mdoc.modules.questionnaire.widget

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.mdoc.R
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder
import kotlinx.android.synthetic.main.questionnaire_list_item.view.*

class QuestionnaireWidgetItemAdapter :
    ListItemTypeAdapter<SectionData, QuestionnaireWidgetItemAdapter.Holder> {

    class Holder(view: View) : ListItemViewHolder(view) {

        val sectionName = view.pollTypeTv
        val answers = view.answearsTv
        val progressBar = view.progressSb
        val progressPercentage = view.progressTv

    }

    override fun onCreateViewHolder(parent: ViewGroup): Holder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.questionnaire_list_item, parent, false
        );
        return Holder(view)
    }

    override fun onBind(position: Int, item: SectionData, holder: Holder) {
        super.onBind(position, item, holder)
        holder.sectionName.text = item.displayName
        holder.answers.text =
            "${holder.itemView.context.getString(R.string.started)} ${item.getStarted()}"
        holder.progressBar.max = item.total
        holder.progressBar.progress = item.started
        holder.progressPercentage.text = item.getPercentage()
    }

}
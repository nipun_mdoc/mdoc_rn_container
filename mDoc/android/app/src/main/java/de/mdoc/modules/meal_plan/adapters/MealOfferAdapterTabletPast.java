package de.mdoc.modules.meal_plan.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.interfaces.RecyclerViewClickListener;
import de.mdoc.pojo.MealChoice;
import de.mdoc.pojo.MealOffer;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.ProgressDialog;

/**
 * Created by ema on 12/25/17.
 */

public class MealOfferAdapterTabletPast extends RecyclerView.Adapter<MealOfferAdapterTabletPast.ViewHolder> {

    ArrayList<MealOffer> items;
    private LayoutInflater inflater;
    Context context;
    String dessert;
    ProgressDialog progressDialog;
    String mealChoiceId;
    public static MealOffer previousItem = null;
    private String mealPlanId;
    RecyclerViewClickListener listener;
    MealChoice selectedChoice;

    public MealOfferAdapterTabletPast(ArrayList<MealOffer> items, Context context, String mealPlanId, String mealChoiceId, RecyclerViewClickListener listener, MealChoice selectedChoice) {
        this.items = items;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mealChoiceId = mealChoiceId;
        this.mealPlanId = mealPlanId;
        this.listener = listener;
        this.selectedChoice = selectedChoice;

        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.meal_offer_item, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MealOffer item = items.get(position);

        if (item.isSoup()) {
            holder.mealNameTv.setText(context.getString(R.string.soup));
        } else {
            holder.mealNameTv.setText(item.getName(context.getResources()));
        }
        holder.mealDescriptionTv.setText(item.getDescription());
        if (item.getCalories() > 0) {
            holder.kcalTv.setVisibility(View.VISIBLE);
            holder.kcalTv.setText("Kcal: " + item.getCalories());
        } else {
            holder.kcalTv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.GLUTEN_FREE)) {
            holder.gfIv.setVisibility(View.VISIBLE);
        } else {
            holder.gfIv.setVisibility(View.GONE);
        }

        if (item.getOptions().contains(MdocConstants.VEGETARIAN)) {
            holder.veganIv.setVisibility(View.VISIBLE);
        } else {
            holder.veganIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.PIG)) {
            holder.pigIv.setVisibility(View.VISIBLE);
        } else {
            holder.pigIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.CHICKEN)) {
            holder.chickenIv.setVisibility(View.VISIBLE);
        } else {
            holder.chickenIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.COW)) {
            holder.cowIv.setVisibility(View.VISIBLE);
        } else {
            holder.cowIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.FISH)) {
            holder.fishIv.setVisibility(View.VISIBLE);
        } else {
            holder.fishIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.INFO)) {
            holder.infoIv.setVisibility(View.VISIBLE);
        } else {
            holder.infoIv.setVisibility(View.GONE);
        }
        if(item.isKosher()){
            holder.kosherIv.setVisibility(View.VISIBLE);
        }else{
            holder.kosherIv.setVisibility(View.GONE);
        }


        if (selectedChoice != null && !context.getResources().getBoolean(R.bool.can_user_choose_meal) && item.get_id().equals(selectedChoice.getMealId())) {
            holder.selectedIv.setVisibility(View.VISIBLE);
            holder.mealDescriptionTv.setTextColor(context.getResources().getColor(R.color.cool_blue));
            holder.rateBtn.setBackgroundResource(R.drawable.gray_rr_100);
            holder.rateBtn.setText(context.getString(R.string.rated));
        } else {
            holder.selectedIv.setVisibility(View.GONE);
            holder.mealDescriptionTv.setTextColor(context.getResources().getColor(R.color.charocal_gray));
            holder.rateBtn.setBackgroundResource(R.drawable.meal_button);
            holder.rateBtn.setText(context.getString(R.string.rate));
        }

        if (selectedChoice != null && item.get_id().equals(selectedChoice.getMealId())) {
            if (item.isDessert() || item.isSoup()) {
                holder.rateBtn.setVisibility(View.GONE);
            } else {
                holder.rateBtn.setVisibility(View.VISIBLE);
            }

        } else {
            if (selectedChoice != null) {
                holder.rateBtn.setVisibility(View.GONE);
            } else {
                if (item.isDessert() || item.isSoup()) {
                    holder.rateBtn.setVisibility(View.GONE);
                } else {
                    holder.rateBtn.setVisibility(View.VISIBLE);
                }

            }
        }


        holder.infoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAditivesDialog(context, item.getInfo());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.mealNameTv)
        TextView mealNameTv;
        @BindView(R.id.mealDescriptionTv)
        TextView mealDescriptionTv;
        @BindView(R.id.kcalTv)
        TextView kcalTv;

        @BindView(R.id.infoIv)
        ImageView infoIv;
        @BindView(R.id.gfIv)
        ImageView gfIv;
        @BindView(R.id.veganIv)
        ImageView veganIv;
        @BindView(R.id.pigIv)
        ImageView pigIv;
        @BindView(R.id.chickenIv)
        ImageView chickenIv;
        @BindView(R.id.cowIv)
        ImageView cowIv;
        @BindView(R.id.fishIv)
        ImageView fishIv;
        @BindView(R.id.rootRl)
        LinearLayout rootRl;
        @BindView(R.id.selectedIv)
        ImageView selectedIv;
        @BindView(R.id.rateBtn)
        TextView rateBtn;
        @BindView(R.id.kosherIv)
        ImageView kosherIv;

        private RecyclerViewClickListener mListener;

        public ViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mListener = listener;
            rateBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v, getAdapterPosition());
        }
    }

    private void showAditivesDialog(Context context, ArrayList<String> infoList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.aditives_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        ImageView closeIv = view.findViewById(R.id.closeIv);
        ListView aditivesLv = view.findViewById(R.id.aditivesLv);
        HashMap<String, String> hashMapAllergens = MdocAppHelper.getInstance().getAllergens();
        HashMap<String, String> hashMapAdditives = MdocAppHelper.getInstance().getAdditives();

        ArrayList<String> showAllergens = new ArrayList<>();

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAdditives.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAllergens.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        ArrayAdapter<String> additivesAdapter =
                new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, showAllergens);

        aditivesLv.setAdapter(additivesAdapter);

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}


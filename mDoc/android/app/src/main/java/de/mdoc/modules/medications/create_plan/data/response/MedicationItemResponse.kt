package de.mdoc.modules.medications.create_plan.data.response

import de.mdoc.modules.medications.data.Medication

data class MedicationItemResponse(
        val code: String? = null,
        val `data`: Medication? = null,
        val message: String? = null,
        val timestamp: Long? = null
)


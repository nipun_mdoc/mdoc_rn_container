package de.mdoc.modules.files

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.*
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.EXTERNAL_WRITE_PERMISSION_GRANT
import de.mdoc.databinding.FragmentMyUploadsBinding
import de.mdoc.modules.files.adapters.FilesListAdapter
import de.mdoc.modules.files.base.FilesBaseFragment
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.files.filter.*
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.onCreateOptionsMenuWithBadge
import de.mdoc.util.updateBadge
import de.mdoc.viewmodel.bindVisibleGone
import kotlinx.android.synthetic.main.common_search_view.*
import kotlinx.android.synthetic.main.fragment_my_uploads.*

class FilesOverviewFragment : FilesBaseFragment(), FileFilterChangedListener {

    override val navigationItem: NavigationItem = NavigationItem.FilesList
    private lateinit var filterViewModel: FilterViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentMyUploadsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_uploads, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        filterViewModel = activity?.run {
            ViewModelProviders.of(this,
                    FilterViewModelFactory(FilterRepository(filesRepository = FilesRepository( service = RestClient.getService(), cache = FileCacheRepository)))).get(FilterViewModel::class.java)
        }?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        requestStoragePermissions()
        onFileFilterChanged(filterViewModel.getFilter())

        if (!MdocAppHelper.getInstance().patientFileUpload) {
            fab?.visibility = View.INVISIBLE
        }

        fab?.setOnClickListener {
            val direction = FilesOverviewFragmentDirections.actionToFragmentAddNewFile()
            findNavController().navigate(direction)
        }

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                findNavController().navigate(R.id.fragmentFilesSearch)
            }
        }

        val adapter = FilesListAdapter(this)
        filesLv.adapter = adapter
        filesLv.layoutManager = LinearLayoutManager(view.context)
        viewModel.files.observe(this) {
            adapter.setItems(it)
        }
        viewModel.categories.observe(this) {
            adapter.setCategories(it.list)
        }
        bindVisibleGone(hasDataContainer, viewModel.isHaveData)
        bindVisibleGone(noDataContainer, viewModel.isNoData)
        viewModel.reloadData()
    }

    override fun onFileFilterChanged(filesFilter: FilesFilter) {
        viewModel.filter.set(filesFilter)
        activity?.invalidateOptionsMenu()
    }

    private fun requestStoragePermissions() {
        if (ContextCompat.checkSelfPermission((activity as MdocActivity), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    EXTERNAL_WRITE_PERMISSION_GRANT
            )
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == EXTERNAL_WRITE_PERMISSION_GRANT) {
            if (grantResults.isNotEmpty() && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                findNavController().popBackStack()
            }
        }
    }

    override fun reloadData() {
        viewModel.reloadData()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        onCreateOptionsMenuWithBadge(menu, inflater, R.menu.menu_files, R.id.menuFilesFilter)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        updateBadge(menu, viewModel.filter.get().getActiveFiltersCount(), R.id.menuFilesFilter)
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuFilesFilter) {
            FilterDialogFragment.newInstance(viewModel.filter.get(), this
            ).show(requireFragmentManager(), "TAG_FILTER_DIALOG")
        }
        return super.onOptionsItemSelected(item)
    }
}
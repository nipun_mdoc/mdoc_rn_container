package de.mdoc.modules.my_stay

import android.os.Handler
import android.view.View
import android.widget.ImageView
import androidx.lifecycle.LifecycleOwner
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.mdoc.R
import de.mdoc.modules.my_stay.adapters.AndroidImageAdapter
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.pojo.Image
import de.mdoc.util.MdocUtil
import de.mdoc.util.StringUtils
import de.mdoc.util.openLink
import kotlinx.android.synthetic.main.fragment_my_stay_contact_info.view.*
import kotlinx.android.synthetic.main.layout_my_stay_detail.view.*
import kotlinx.android.synthetic.main.social_media_layout.view.*


class MyStayDetailsBinding(
        private val lifecycleOwner: LifecycleOwner,
        private val view: View,
        viewModel: MyStayViewModel,
        private val callback: Callback
) : LifecycleOwner by lifecycleOwner {
    private var adapterItems= arrayListOf<String>()

    private val pagerAdapter: PagerAdapter by lazy {
        AndroidImageAdapter(view.context, adapterItems)
    }
    val handler: Handler = Handler()
    private var page = 0

    interface Callback {

        fun openMap()
        fun makeACall()

    }
    private var runnable: Runnable = object : Runnable {
        val viewpager=view.viewPagerClinic

        override fun run() {
            if (pagerAdapter.count - 1 == page) {
                page = 0
            } else {
                page++
            }
            viewpager.setCurrentItem(page, true)
            handler.postDelayed(this, DELAY)
        }
    }

    init {

        viewModel.clinicData.observe(this){
            fillMyStayDetails(it)
        }

        view.findViewById<View>(R.id.txt_show_on_map).setOnClickListener {
            callback.openMap()
        }
        view.findViewById<View>(R.id.cl_call_clinic).setOnClickListener {
            callback.makeACall()
        }
    }

    fun removeHandlerCallback(){
        handler.removeCallbacks(runnable)
    }

    fun addHandlerCallback(){
        handler.postDelayed(runnable, DELAY)
    }

    private fun fillMyStayDetails(input: CurrentClinicData?) {
        val clinicDetails = input?.clinicDetails
        val street = clinicDetails?.address?.street ?: ""
        val houseNumber = clinicDetails?.address?.houseNumber ?: ""
        val postalCode = clinicDetails?.address?.postalCode ?: ""
        val city = clinicDetails?.address?.city ?: ""

        val addressFirstLine = "${street} ${houseNumber}"
        val addressSecondLine = "${postalCode} ${city}"

        view.txt_my_clinic.text=clinicDetails?.name
        view.txt_head_label?.text=clinicDetails?.headOfClinic?.description
        view.txt_head_of_clinic?.text = clinicDetails?.headOfClinic?.name
        view.txt_address_first_line?.text = addressFirstLine
        view.txt_address_second_line?.text = addressSecondLine
        view.txt_telephone?.text = clinicDetails?.contactDetails?.phone
        view.txt_email?.text = clinicDetails?.contactDetails?.email
        if(clinicDetails?.contactDetails?.email.isNullOrEmpty()){
            view.email_bg.visibility = View.GONE
        }

        val lstValues: List<String>? = clinicDetails?.description?.split("<span style=\"text-decoration: underline;\">")?.map { it -> it.trim() }
        val finalString = StringBuilder()
        lstValues?.forEachIndexed { index, it ->
            val str = it.toString()
            if(index == 0){
                finalString.append(str)
            }else{
                val ii = StringUtils.matchDetails(str, "</span>", 0)
                finalString.append(" <span style=\"text-decoration: underline;\"><u>").append(str.substring(0, ii)).append("</u>").append(str.substring(ii, str.length));
            }
        }
        view.txt_expertise.text = StringUtils.fromHtml(finalString.toString())
        view.txt_specification?.text=StringUtils.fromHtml(clinicDetails?.specifications?.replace("<p class=\"MsoNormal\" style=\"margin-bottom: 0cm; line-height: normal;\"><span lang=\"DE\" style=\"mso-ansi-language: DE;\">&nbsp;</span></p>", ""))

        setHeadOfClinicImage(clinicDetails?.headOfClinic?.image)

        val images:ArrayList<String>?=getImageUrlsFromCollection(clinicDetails?.images)
        setClinicImage(images)



        if(clinicDetails?.socialMediaDetails != null && clinicDetails.isSocialMediaEnabled()) {

            view.socialMediaLayout?.visibility = View.VISIBLE
            input.clinicDetails?.let{
                if(it.showFacebook()) view.facebookIv?.visibility = View.VISIBLE
                if(it.showInstagram()) view.instagramIv?.visibility = View.VISIBLE
                if(it.showTwitter()) view.twitterIv?.visibility = View.VISIBLE
            }
        }else{
            view.socialMediaLayout?.visibility = View.GONE
        }

        view.facebookIv?.setOnClickListener {
            openLink(clinicDetails?.getFacebookLink(), it.context)
        }

        view.instagramIv?.setOnClickListener {
            openLink(clinicDetails?.getInstagramLink(), it.context)
        }

        view.twitterIv?.setOnClickListener {
            openLink(clinicDetails?.getTwitterLink(), it.context)
        }
    }



    private fun getImageUrlsFromCollection(data: ArrayList<Image>?): ArrayList<String>? {
        val images= arrayListOf<String>()
        if(!data.isNullOrEmpty()){
            for(item in data){
                if(!item.image.isNullOrEmpty()){
                    images.add(item.image)
                }
            }
        }
        return images
    }

    private fun setClinicImage(images: ArrayList<String>?) {

            if(images!=null && images.size>0) {
                view.no_clinic_image_ll?.visibility = View.GONE
                view.viewPagerClinic?.visibility = View.VISIBLE
                val viewpager = view.viewPagerClinic
                adapterItems = images
                pagerAdapter.notifyDataSetChanged()
                viewpager.adapter = pagerAdapter

                if (images.size > 1) {
                    view.tabDots?.setupWithViewPager(viewpager, true)
                    view.tabDots?.visibility = View.VISIBLE
                }
            }else{
                view.viewPagerClinic?.visibility = View.GONE
                view.tabDots?.visibility = View.GONE
                view.no_clinic_image_ll?.visibility = View.VISIBLE
            }

    }

    private fun setHeadOfClinicImage(imageUrl: String?) {
        val headOfClinic=view.findViewById<ImageView>(R.id.img_head_of_clinic)

        if(headOfClinic!=null){
            Glide.with(view.context)
                    .load(imageUrl)
                    .placeholder(R.drawable.ic_no_doctor_placeholder)
                    .error(R.drawable.ic_no_doctor_placeholder)
                    .apply(RequestOptions().override(150, 150).fitCenter())
                    .into(headOfClinic)
        }
    }

    companion object{
        private const val DELAY = 10000L
    }
}
package de.mdoc.network.response;

import java.util.ArrayList;

public class SpecialtiesResponse extends MdocResponse {

    private ArrayList<SpeciltyData> data;

    public ArrayList<SpeciltyData> getData() {
        return data;
    }

    public void setData(ArrayList<SpeciltyData> data) {
        this.data = data;
    }

    public SpeciltyData getSpecialtyDataByServiceId(String serviceId) {
        SpeciltyData result = null;
        if (serviceId != null && data != null) {
            for (SpeciltyData speciltyData : data) {
                result = speciltyData.findSpecialtyByServiceId(serviceId);
                if (result != null) {
                    break;
                }
            }
        }
        return result;
    }
}

/**
* Api Documentation
* Api Documentation
*
* OpenAPI spec version: 1.0
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/
package de.mdoc.pojo.entertainment



/**
 * 
 * @param contentType 
 * @param contentTypeWellKnown 
 * @param creation 
 * @param &#x60;data&#x60; 
 * @param dataOld 
 * @param hash 
 * @param language 
 * @param size 
 * @param title 
 * @param url 
 */
data class Attachment (
    val contentType: kotlin.String? = null,
    val contentTypeWellKnown: kotlin.String? = null,
    val language: kotlin.String? = null,
    val size: kotlin.String? = null,
    val title: kotlin.String? = null,
    val url: kotlin.String? = null
)


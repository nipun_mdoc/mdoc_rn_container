package de.mdoc.modules.messages.data

import de.mdoc.util.MimeTypes
import de.mdoc.util.overrideNullText

data class FileListItem(val owner: String = "",
                        val extension: String = "",
                        val cts: Long = 0,
                        val sharingList: List<SharingListItem>?,
                        val description: String = "",
                        val mediaType: String = "",
                        val path: String = "",
                        val size: Int = 0,
                        val uts: Long = 0,
                        val name: String = "",
                        val id: String = "",
                        val category: String = ""){
    fun getFullFileName(): String{
        return "${overrideNullText(name)}.${overrideNullText(MimeTypes.getDefaultExt(mediaType))}"
    }
}
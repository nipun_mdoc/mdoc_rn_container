package de.mdoc.pojo;

/**
 * Created by ema on 1/13/17.
 */

public class ExtendedOption extends Option {

    private String scaleBeginningText;
    private String scaleEndText;
    private String number;
    private Answer answer;

    public ExtendedOption(Option o, String scaleBeginningText, String scaleEndText, String number, Answer a) {
        this.scaleBeginningText = scaleBeginningText;
        this.scaleEndText = scaleEndText;
        this.number = number;
        this.key = o.key;
        this.value = o.value;
        this.answer = a;
    }

    public String getScaleBeginningText() {
        return scaleBeginningText;
    }

    public void setScaleBeginningText(String scaleBeginningText) {
        this.scaleBeginningText = scaleBeginningText;
    }

    public String getScaleEndText() {
        return scaleEndText;
    }

    public void setScaleEndText(String scaleEndText) {
        this.scaleEndText = scaleEndText;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }
}

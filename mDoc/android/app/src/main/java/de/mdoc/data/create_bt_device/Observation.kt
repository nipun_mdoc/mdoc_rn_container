package de.mdoc.data.create_bt_device


class Observation{

  lateinit var category: List<Category>

    lateinit var code: Code
    lateinit var device: Device
    lateinit var effectiveDateTime: String
    lateinit var performer: List<Performer>
    lateinit var resourceType: String
    lateinit var status: String
    lateinit var subject: Subject
    var text: Text?=null
    var note:String?=null
    var valueQuantity: ValueQuantity?=null
    lateinit var bodySite: Code
    lateinit var component: ArrayList<Component>
    lateinit var id: String
    lateinit var model : String

    constructor(category: List<Category>, code: Code, device: Device, effectiveDateTime: String, performer: List<Performer>, resourceType: String, status: String, subject: Subject, text: Text, valueQuantity: ValueQuantity, model : String) {
        this.category = category
        this.code = code
        this.device = device
        this.effectiveDateTime = effectiveDateTime
        this.performer = performer
        this.resourceType = resourceType
        this.status = status
        this.subject = subject
        this.text = text
        this.valueQuantity = valueQuantity
        this.model = model
    }
    constructor(category: List<Category>, code: Code, device: Device, effectiveDateTime: String, performer: List<Performer>, resourceType: String, status: String, subject: Subject, valueQuantity: ValueQuantity, model : String) {
        this.category = category
        this.code = code
        this.device = device
        this.effectiveDateTime = effectiveDateTime
        this.performer = performer
        this.resourceType = resourceType
        this.status = status
        this.subject = subject
        this.valueQuantity = valueQuantity
        this.model = model
    }

    constructor(code: Code, device: Device, effectiveDateTime: String, performer: List<Performer>, resourceType: String, status: String, subject: Subject, text: Text?, valueQuantity: ValueQuantity, model: String) {
        this.code = code
        this.device = device
        this.effectiveDateTime = effectiveDateTime
        this.performer = performer
        this.resourceType = resourceType
        this.status = status
        this.subject = subject
        this.text = text
        this.valueQuantity = valueQuantity
        this.model = model
    }

    constructor()

    constructor(category: List<Category>, code: Code, device: Device, effectiveDateTime: String, resourceType: String, status: String, bodySite: Code, component: ArrayList<Component>, id: String, model: String) {
        this.category = category
        this.code = code
        this.device = device
        this.effectiveDateTime = effectiveDateTime
        this.resourceType = resourceType
        this.status = status
        this.bodySite = bodySite
        this.component = component
        this.id = id
        this.model = model
    }
}
data class ValueQuantity(
        val code: String,
        val system: String,
        val unit: String,
        val value: Double
)
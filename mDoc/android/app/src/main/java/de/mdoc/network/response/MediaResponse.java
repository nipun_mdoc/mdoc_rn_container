package de.mdoc.network.response;

import de.mdoc.pojo.Magazine;

public class MediaResponse {
    Magazine data;

    public Magazine getData() {
        return data;
    }

    public void setData(Magazine data) {
        this.data = data;
    }
}

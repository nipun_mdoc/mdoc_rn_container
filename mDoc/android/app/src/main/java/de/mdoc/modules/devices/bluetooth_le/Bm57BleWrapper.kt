package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import de.mdoc.interfaces.BleResponseListener
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class Bm57BleWrapper( context: Context, deviceAddress: String) : BluetoothGattCallback() {

    private var bluetoothDevice: BluetoothDevice?=null

    private var acquiredMeasurementResponseArrayList: ArrayList<Byte>? = null
    private var bleListener: BleResponseListener? = null

    fun setBleResponseListener(bleListener: BleResponseListener) {
        this.bleListener = bleListener
    }

    init {
        Timber.d(": initCalled")
        if(bluetoothDevice==null) {
            bluetoothDevice = getBluetoothDevice(context, deviceAddress)
        }
        bluetoothDevice!!.connectGatt(context,false,this)
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {

        val bluetoothManager = context
                .getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        super.onConnectionStateChange(gatt, status, newState)

        Timber.d("onConnectionStateChange %s",status)

        if (status == BluetoothGatt.GATT_SUCCESS) {
            when (newState) {
                BluetoothGatt.STATE_CONNECTED -> {
                    Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                    gatt.discoverServices()


                }
                BluetoothGatt.STATE_DISCONNECTED -> {
                    Timber.d("onConnectionStateChange_STATE_DISCONNECTED_RETURN_VALUES")
                    gatt.close()

                    bleListener!!.dataFromDeviceCallback(acquiredMeasurementResponseArrayList, gatt)

                }
            }
        }
        else if(status==19){
            Timber.d("onConnectionStateChange_STATUS_19_RETURN_VALUES")
            gatt.close()
            Timber.d("onConnectionStateChange: STATE_DISCONNECTED $gatt $status")
            bleListener!!.dataFromDeviceCallback(acquiredMeasurementResponseArrayList, gatt)
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt,
                                      status: Int) {
        Timber.d("onServicesDiscovered %s",status)

        super.onServicesDiscovered(gatt, status)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Timber.d("onServicesDiscovered: $gatt")

            val characteristicNotification = gatt.getService(UUID.fromString(BLOOD_PRESSURE_SERVICE))
                    .getCharacteristic(UUID.fromString(BLOOD_PRESSURE_MEASUREMENT_CHARACTERISTIC))
            characteristicNotification.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

            gatt.setCharacteristicNotification(characteristicNotification, true)


            val descriptorOne = characteristicNotification.getDescriptor(
                    UUID.fromString(NOTIFICATION_CHARACTERISTIC))
            descriptorOne.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE

            Timber.d("onServicesDiscovered_delay")
            Timber.d("onServicesDiscovered_BefireWrite")
            Thread.sleep(1000)
            gatt.writeDescriptor(descriptorOne)
            Timber.d("onServicesDiscovered_AfterWrite")
        }

    }

    override fun onDescriptorWrite(gatt: BluetoothGatt,
                                   descriptor: BluetoothGattDescriptor,
                                   status: Int) {
        super.onDescriptorWrite(gatt, descriptor, status)
        Timber.d("onDescriptorWrite %s",status)

        if (status == BluetoothGatt.GATT_SUCCESS) {
            if (acquiredMeasurementResponseArrayList == null) {
                acquiredMeasurementResponseArrayList = ArrayList()
            }

        } else {
            Timber.d("onDescriptorWrite: FAILED $gatt")
        }
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt,
                                         characteristic: BluetoothGattCharacteristic) {
        super.onCharacteristicChanged(gatt, characteristic)
        Timber.d("onCharacteristicChanged")

        Timber.d("onCharacteristicChanged: %s", Arrays.toString(characteristic.value))
        characteristic.value.forEach {
            acquiredMeasurementResponseArrayList?.add(it)
        }
    }

    override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                       characteristic: BluetoothGattCharacteristic?,
                                       status: Int) {
        super.onCharacteristicWrite(gatt, characteristic, status)
        Timber.d("onCharacteristicWrite: %s", Arrays.toString(characteristic?.value))

        if (status == BluetoothGatt.GATT_SUCCESS) {
            Timber.d("onCharacteristicWrite: %s", Arrays.toString(characteristic?.value))

        }
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                      characteristic: BluetoothGattCharacteristic?,
                                      status: Int) {
        super.onCharacteristicRead(gatt, characteristic, status)
        Timber.i("onCharacteristicRead $characteristic")
        val value = characteristic?.value ?: byteArrayOf()
        val v = String(value)
        Timber.i("onCharacteristicRead Value: $v")
    }


    companion object {

        private const val NOTIFICATION_CHARACTERISTIC = "00002902-0000-1000-8000-00805f9b34fb"

        private const val BLOOD_PRESSURE_SERVICE = "00001810-0000-1000-8000-00805f9b34fb"

        private const val BLOOD_PRESSURE_MEASUREMENT_CHARACTERISTIC = "00002A35-0000-1000-8000-00805f9b34fb"
    }
}

package de.mdoc.modules.diary.data

data class DiarySearchRequest(
    var query:String?="",
    var skip:Int?=null,
    var limit: Int? = null,
    var diaryConfigId: ArrayList<String>? = null,
    var from: Long? = null,
    var to: Long? = null
)
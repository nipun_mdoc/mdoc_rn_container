package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import org.joda.time.LocalDateTime

class Gl50Device(val gatt: BluetoothGatt, val data: ArrayList<Byte>): BluetoothDevice {

    override fun getDeviceObservations(list: ArrayList<Byte>,
                                       deviceId: String,
                                       updateFrom: Long?): ArrayList<Observation> {
        val userId = MdocAppHelper.getInstance()
            .userId
        val codingForCategory = Coding("vitals", "Fitness Data", "http://hl7.org/fhir/observation-category")
        val codingForCategoryList = listOf(codingForCategory)
        val category = Category(codingForCategoryList)
        val categoryList = listOf(category)
        ///////////////////
        val performer = listOf(Performer("user/$userId"))
        val subject = Subject("user/$userId")
        //////////////
        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var yearOne = ""
        var yearTwo = ""
        var month = ""
        var day = ""
        var hour = ""
        var minute = ""
        var glucoseOne = ""
        var glucoseTwo = ""
        val readsByGroup = list.chunked(17)

        for (chunk in readsByGroup) {
            if (chunk.size == 17) {
                for ((index, byte) in chunk.withIndex()) {
                    when (index) {
                        3  -> {
                            yearOne = String.format("%02X", byte)
                        }

                        4  -> {
                            yearTwo = String.format("%02X", byte)
                        }

                        5  -> {
                            month = String.format("%02X", byte)
                        }

                        6  -> {
                            day = String.format("%02X", byte)
                        }

                        7  -> {
                            hour = String.format("%02X", byte)
                        }

                        8  -> {
                            minute = String.format("%02X", byte)
                        }

                        9  -> {
                            glucoseOne = String.format("%02X", byte)
                        }

                        10 -> {
                            glucoseTwo = String.format("%02X", byte)
                        }

                        16 -> {
                            val marker = String.format("%02X", byte)
                            val hexYear = yearTwo + yearOne
                            val yearParsed = java.lang.Long.parseLong(hexYear, 16)
                                .toInt()
                            val monthParsed = java.lang.Long.parseLong((month), 16)
                                .toInt()
                            val dayParsed = java.lang.Long.parseLong(day, 16)
                                .toInt()
                            val hourParsed = java.lang.Long.parseLong(hour, 16)
                                .toInt()
                            val minuteParsed = java.lang.Long.parseLong(minute, 16)
                                .toInt()
                            val time = LocalDateTime(yearParsed, monthParsed, dayParsed, hourParsed, minuteParsed)
                            val effectiveTime = time.toDateTime()
                                .millis.toString()
                            val hexGlucose = glucoseOne + glucoseTwo
                            val rawValue: Long = java.lang.Long.parseLong(hexGlucose, 16)

                            val glucose = getValueUnit(rawValue).first
                            val unit = getValueUnit(rawValue).second
                            val observedItem = Observation(categoryList
                                    , generateCode(marker)
                                    , Device("device/$deviceId")
                                    , effectiveTime
                                    , performer, "Observation"
                                    , "FINAL"
                                    , subject
                                    , ValueQuantity(unit,
                                    "http://unitsofmeasure.org",
                                    unit,
                                    glucose),
                                    "GL50")
                            if (updateFrom == null) {
                                observations.add(observedItem)
                            }
                            else {
                                if (time.toDateTime().millis > updateFrom) {
                                    observations.add(observedItem)
                                }
                            }
                        }
                    }
                }
            }
        }
        return observations
    }

    private fun generateCode(marker: String): Code {
        val loincCode = when (marker) {
            "01" -> "87421-4"
            "02" -> "14760-3"
            else -> "41653-7"
        }
        return Code(listOf(Coding(loincCode, null, "http://loinc.org")))
    }

    private fun getValueUnit(rawValue: Long): Pair<Double, String> {
        return if (AppPersistence.latestConnectedGL50Device == 8) {
            val glucose: Double = rawValue / 10.0
            Pair(glucose, "mmol/l")
        }
        else {
            Pair(rawValue.toDouble(), "mg/dl")
        }
    }

    override fun deviceAddress(): String {
        return gatt.device.address
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data
    }

    override fun deviceName(): String {
        return gatt.device.name
    }
}

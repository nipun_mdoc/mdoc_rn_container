package de.mdoc.fragments.kiosk;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import de.mdoc.BuildConfig;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.LoginActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.fragments.login.CaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.KeycloackLoginRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.KeycloackLoginResponse;
import de.mdoc.network.response.LoginResponse;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 10/23/17.
 */

public class KioskLoginFragment extends MdocBaseFragment {

    private static final int MY_CAMERA_REQUEST_CODE = 100;
    public static final String USERNAME = "username";
    public static final String OLD_PASSWORD = "oldPassword";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String OTP = "otp";

    @BindView(R.id.patientIdEdt)
    EditText patientIdEdt;
    @BindView(R.id.passwordEdt)
    EditText passwordEdt;
    @BindView(R.id.passwordErrorTv)
    TextView passwordErrorTv;
    @BindView(R.id.patientIdErrorTv)
    TextView patientIdErrorTv;
    @BindView(R.id.backDrawbaleIv)
    ImageView backDrawbaleIv;

    @BindView(R.id.signInBtn)
    Button signInBtn;

    @BindView(R.id.usernamePassLl)
    LinearLayout usernamePassLl;
    @BindView(R.id.otpLl)
    LinearLayout otpLl;

    @BindView(R.id.otpErrorTv)
    TextView otpErrorTv;
    @BindView(R.id.otpEdt)
    EditText otpEdt;

    LoginActivity activity;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_first;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getNewLoginActivity();
    }

    private void clearErrors(){
        removeError(patientIdEdt, patientIdErrorTv);
        removeError(passwordEdt, passwordErrorTv);
    }

    private boolean isError(){
        boolean isError=false;
        clearErrors();

        if(!MdocUtil.isNotNull(patientIdEdt.getText().toString())){
            setError(patientIdEdt, patientIdErrorTv, getString(R.string.patient_id_required));
            isError = true;
        }

        if(!MdocUtil.isNotNull(passwordEdt.getText().toString())){
            setError(passwordEdt, passwordErrorTv, getString(R.string.password_required));
            isError = true;
        }

        return isError;
    }

    @OnClick(R.id.signInBtn)
    public void onSignInBtnClick(){
        if(!isError()){
            keycloackLogin();
        }
    }

    private void keycloackLogin(){


//        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;

        KeycloackLoginRequest request = new KeycloackLoginRequest();
        request.setUsername(patientIdEdt.getText().toString());
        request.setPassword(passwordEdt.getText().toString());
        request.setClient_id(getString(R.string.keycloack_clinic_Id));
        request.setClient_secret(getString(R.string.client_secret));
        request.setGrant_type("password");
        if(otpLl.getVisibility() == View.VISIBLE) {
            request.setEmotp(otpEdt.getText().toString());
//            request.setDeviceName(deviceName);
        }

        MdocManager.loginWithKeycloackEmotp(request, new Callback<KeycloackLoginResponse>() {
            @Override
            public void onResponse(Call<KeycloackLoginResponse> call, Response<KeycloackLoginResponse> response) {
                if(response.isSuccessful()){
                    MdocAppHelper.getInstance().setAccessToken(response.body().getToken_type() + " " + response.body().getAccess_token());
                    MdocAppHelper.getInstance().setRefreshToken(response.body().getToken_type() + " " + response.body().getRefresh_token());
                    MdocAppHelper.getInstance().setRefreshTokenNoHeader(response.body().getRefresh_token());
                    loginWithSession();
                }else{
                    String errorMsg = MdocAppHelper.getInstance().getErrorMsg();
                    if (errorMsg != null && errorMsg.equals(MdocConstants.ERROR_MESSAGE)) {
                        changePasswordDialog(patientIdEdt.getText().toString(), passwordEdt.getText().toString());
                    } else if(errorMsg != null && errorMsg.contains(MdocConstants.OTP_REQ_MESSAGE)){
//                        usernamePassLl.setVisibility(View.GONE);
//                        otpLl.setVisibility(View.VISIBLE);
                        sendRequestForEmail();
                    }else{
                        MdocUtil.showToastLong(activity, activity.getResources().getString(R.string.login_failed));
                        signInBtn.setEnabled(true);
                    }
                }
            }

            @Override
            public void onFailure(Call<KeycloackLoginResponse> call, Throwable t) {
                MdocUtil.showToastLong(getContext(), "Error " + t.getMessage());
                signInBtn.setEnabled(true);
                usernamePassLl.setVisibility(View.GONE);
                otpLl.setVisibility(View.VISIBLE);
            }
        });
    }

    public void sendRequestForEmail(){
        KeycloackLoginRequest request = new KeycloackLoginRequest();
        request.setUsername(patientIdEdt.getText().toString());
        request.setPassword(passwordEdt.getText().toString());
        request.setClient_id(getString(R.string.keycloack_clinic_Id));
        request.setClient_secret(getString(R.string.client_secret));
        request.setGrant_type("password");
        request.setEmotp(MdocConstants.OTP_CHALLENGE);

        MdocManager.sendRequestForEmail(request, new Callback<KeycloackLoginResponse>() {
            @Override
            public void onResponse(Call<KeycloackLoginResponse> call, Response<KeycloackLoginResponse> response) {
                if(response.isSuccessful()){
                    usernamePassLl.setVisibility(View.GONE);
                    otpLl.setVisibility(View.VISIBLE);
                }else{
                    MdocUtil.showToastLong(activity, activity.getResources().getString(R.string.login_failed));
                    signInBtn.setEnabled(true);
                }
            }

            @Override
            public void onFailure(Call<KeycloackLoginResponse> call, Throwable t) {
                MdocUtil.showToastLong(activity, activity.getResources().getString(R.string.login_failed));
                signInBtn.setEnabled(true);
            }
        });
    }

    private void loginWithSession(){
        MdocManager.loginWithSession(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                if(response.isSuccessful()){
                    MdocAppHelper.getInstance().setPublicUserDetailses(response.body().getData().getPublicUserDetails());
                    MdocAppHelper.getInstance().setUserLastName(response.body().getData().getPublicUserDetails().getLastName());
                    MdocAppHelper.getInstance().setUserFirstName(response.body().getData().getPublicUserDetails().getFirstName());
                    MdocAppHelper.getInstance().setUsername(response.body().getData().getPublicUserDetails().getUsername());
                    MdocAppHelper.getInstance().setUserType(response.body().getData().getUserType());
//                    MKAppHelper.getInstance().setDepartment(response.body().getData().getDepartment());
                    MdocAppHelper.getInstance().setUserId(response.body().getData().getUserId());

                    if (response.body().getData().getAccesses().getCases().size() == 1) {
                        MdocAppHelper.getInstance().setCaseId(response.body().getData().getAccesses().getCases().get(0).getCaseId());
                        MdocAppHelper.getInstance().setIsPremiumPatient(response.body().getData().getAccesses().getCases().get(0).isPremium());
                        MdocAppHelper.getInstance().setClinicId(response.body().getData().getAccesses().getCases().get(0).getClinicId());
                        MdocAppHelper.getInstance().setDepartment(response.body().getData().getAccesses().getCases().get(0).getDepartment());
                        MdocAppHelper.getInstance().setExternalCaseId(response.body().getData().getAccesses().getCases().get(0).getExternalCaseId());
                        navigateToDashboard();
                    } else {
                        Bundle b = new Bundle();
                        b.putSerializable(MdocConstants.CASES, response.body().getData().getAccesses().getCases());
                        navigateToCaseFragment(b);
                    }
                }else{
                    Timber.w("loginWithSession %s", APIErrorKt.getErrorDetails(response));
                }
                signInBtn.setEnabled(true);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Timber.w(t, "loginWithSession");
                signInBtn.setEnabled(true);
            }
        });
    }

    private void changePasswordDialog(final String username, final String oldPassword) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.password_change, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        Button btnConfirm = view.findViewById(R.id.btnConfirmChange);
        Button btnCancel = view.findViewById(R.id.btnCancelChange);
        final EditText newPassword = view.findViewById(R.id.newPasswordEdit);
        final EditText confirmPassword = view.findViewById(R.id.confirmPassEdit);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (newPassword.getText().toString().equals(confirmPassword.getText().toString())) {
                    if (newPassword.getText().length() < 8 || confirmPassword.getText().length() < 0) {
                        Toast.makeText(activity, activity.getResources().getString(R.string.pass_min_lenght), Toast.LENGTH_LONG).show();
                    } else {
                        String newPass = newPassword.getText().toString();
                        changePassword(newPass, oldPassword, username, dialog);
                    }

                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.password_mismatch), Toast.LENGTH_LONG).show();
                }
//                dialog.dismiss();
            }
        });
    }

    private void changePassword(String newPassword, String oldPassword, String username, final AlertDialog dialog) {
        JsonObject body = new JsonObject();
        body.addProperty(USERNAME, username );
        body.addProperty(OLD_PASSWORD, oldPassword);
        body.addProperty(NEW_PASSWORD, newPassword);
        MdocManager.changeFirstTimePassword(body, new Callback<MdocResponse>() {
            @Override
            public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(activity, activity.getResources().getString(R.string.password_change_success), Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                } else {
                    Toast.makeText(activity, activity.getResources().getString(R.string.pass_failed), Toast.LENGTH_LONG).show();
                    Timber.w("changeFirstTimePassword %s", APIErrorKt.getErrorDetails(response));
                }
            }

            @Override
            public void onFailure(Call<MdocResponse> call, Throwable t) {
                //Toast.makeText(activity, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                Timber.w(t, "changeFirstTimePassword");
            }
        });
    }

    public void navigateToDashboard() {
        Intent intent = new Intent(getNewLoginActivity(), MdocActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        if(!BuildConfig.FLAV.equals("kiosk")){
            getActivity().finish();
        }
    }

    private void navigateToCaseFragment(Bundle b) {
        activity.showNewFragmentAddToBackStack(new CaseFragment(), R.id.login_container, b);
    }

    @OnClick(R.id.backDrawbaleIv)
    public void onBackPressed(){
        otpLl.setVisibility(View.GONE);
        usernamePassLl.setVisibility(View.VISIBLE);
    }

    @OnEditorAction(R.id.passwordEdt)
    boolean onEditorAction(TextView v, int actionId, KeyEvent event){

        if (actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
            onSignInBtnClick();
        }
        return false;
    }

}

package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Participants implements Serializable {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("displayDataInfo")
    @Expose
    private String displayDataInfo;
    @SerializedName("username")
    @Expose
    private String username;
    private String title;
    @SerializedName("participant")
    @Expose
    private Participant participant;

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    public String getDisplayDataInfo() {
        return displayDataInfo;
    }

    public void setDisplayDataInfo(String displayDataInfo) {
        this.displayDataInfo = displayDataInfo;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Participant getParticipant() {
        return participant;
    }
}

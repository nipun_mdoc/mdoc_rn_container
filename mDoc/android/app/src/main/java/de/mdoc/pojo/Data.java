package de.mdoc.pojo;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AdisMulabdic on 9/5/17.
 */

public class Data {

    @SerializedName("activity")
    @Expose
    private Activity activity;

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }
}

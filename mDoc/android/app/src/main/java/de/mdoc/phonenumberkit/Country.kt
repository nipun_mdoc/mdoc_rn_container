package de.mdoc.phonenumberkit
data class Country(
    val iso2: String,
    val name: String,
    val countryCode: Int
)

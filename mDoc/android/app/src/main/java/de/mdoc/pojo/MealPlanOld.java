package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by Admir on 12/4/16.
 */

public class MealPlanOld implements Serializable{

    private String mealPlanType;
    private String description;
    private String name;
    private WeeklyMealOffer weeklyMealOffer;
    private String clinicId;
    private String mealPlanId;

    public String getMealPlanType() {
        return mealPlanType;
    }

    public void setMealPlanType(String mealPlanType) {
        this.mealPlanType = mealPlanType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeeklyMealOffer getWeeklyMealOffer() {
        return weeklyMealOffer;
    }

    public void setWeeklyMealOffer(WeeklyMealOffer weeklyMealOffer) {
        this.weeklyMealOffer = weeklyMealOffer;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

}

package de.mdoc.modules.mental_goals

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers

class MentalGoalFragment: MdocFragment() {
    lateinit var activity: MdocActivity
    private var disposables: CompositeDisposable = CompositeDisposable()

    override fun setResourceId(): Int {
        return R.layout.fragment_blank
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoals

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        MentalGoalsUtil.clearData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disposables = CompositeDisposable()
        searchGamificationApi()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposables.dispose()
    }

    private fun searchGamificationApi() {
        val request = MentalGoalsRequest()

        RestClient.getService()
            .searchGamificationMentalGoals(request)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val data = it.data
                decideFlow(!data.isNullOrEmpty())
            }, {
                MdocUtil.showToastLong(
                        activity,
                        resources.getString(R.string.err_request_failed)
                                      )
            })
            .addTo(disposables)
    }

    private fun decideFlow(hasGoals: Boolean) {
        if (hasGoals) {
            val action = MainNavDirections.globalActionToMentalGoalOverview()
            findNavController().navigate(action)
        }
        else {
            if (MdocAppHelper.getInstance().isShowGoalsOnboarding) {
                val action = MentalGoalFragmentDirections.actionMentalGoalFragmentToOnboardFragment()
                findNavController().navigate(action)
            }
            else {
                val action = MainNavDirections.globalActionToMentalGoalOverview()
                findNavController().navigate(action)
            }
        }
    }
}
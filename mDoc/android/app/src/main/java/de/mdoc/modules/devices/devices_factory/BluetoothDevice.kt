package de.mdoc.modules.devices.devices_factory

import de.mdoc.data.create_bt_device.Observation

interface BluetoothDevice {

    fun deviceName():String
    fun deviceAddress():String
    fun deviceData(): ArrayList<Byte>
    fun getDeviceObservations(list:ArrayList<Byte>,deviceId:String,updateFrom:Long?): ArrayList<Observation>
}
package de.mdoc.data.create_bt_device

data class Div(
        val clazz: List<Any>,
        val content: List<Any>,
        val dir: String
)
package de.mdoc.pojo;

/**
 * Created by ema on 12/5/17.
 */

public class VoteData {
    private String caseId;
    private String mealId;
    private String mealPlanId;
    private String review;
    private String mealChoiceId;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getMealChoiceId() {
        return mealChoiceId;
    }

    public void setMealChoiceId(String mealChoiceId) {
        this.mealChoiceId = mealChoiceId;
    }
}

package de.mdoc.fragments.kiosk;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.adapters.KioskMealOfferAdapter;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.MealPlanRequest;
import de.mdoc.network.response.PublicMealResponse;
import de.mdoc.pojo.MealOffer;
import de.mdoc.pojo.MealPlan;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 10/23/17.
 */
public class KioskMealFragment extends MdocBaseFragment {

    static final long ONE_WEEK = 7 * 24 * 60 * 60 * 1000;


    @BindView(R.id.mealOfferRv)
    RecyclerView mealOfferRv;

    @BindView(R.id.dayOneText)
    TextView dayOneText;
    @BindView(R.id.dayTwoText)
    TextView dayTwoText;
    @BindView(R.id.dayThreeText)
    TextView dayThreeText;

    @BindView(R.id.dayOneImage)
    ImageView dayOneImage;
    @BindView(R.id.dayTwoImage)
    ImageView dayTwoImage;
    @BindView(R.id.dayThreeImage)
    ImageView dayThreeImage;
    @BindView(R.id.holderLl)
    LinearLayout holderLl;

    KioskMealOfferAdapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    LoginActivity activity;
    ArrayList<MealOffer> offers = new ArrayList<>();
    ArrayList<MealPlan> items = new ArrayList<>();

    private int dayIndex = 0;
    private int todayIndex = -1;
    private int totalIndex = 0;
    private int firstDayClick, secondDayClick, thirdDayClick;
    private long checkTimestamp;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_kiosk_meal;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getNewLoginActivity();

        layoutManager = new LinearLayoutManager(activity.getApplicationContext());
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));

        adapter = new KioskMealOfferAdapter(activity, offers);
        mealOfferRv.setAdapter(adapter);

        getMealPlan();
    }

    private void getMealPlan() {

        final MealPlanRequest request = new MealPlanRequest();
        request.setClinicId(MdocAppHelper.getInstance().getClinicId());
        request.setDaysAfter(20);
        request.setStandardOffersOnly(!MdocAppHelper.getInstance().isPremiumPatient());

        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(new Date());
        int today = c.get(Calendar.DAY_OF_WEEK);
        c.add(Calendar.DAY_OF_WEEK, -today + Calendar.MONDAY);
        request.setFromDate(c.getTimeInMillis() - ONE_WEEK);

        MdocManager.getPublicMealRepsonse(request, new Callback<PublicMealResponse>() {
            @Override
            public void onResponse(Call<PublicMealResponse> call, Response<PublicMealResponse> response) {
                if (response.isSuccessful()) {
                    items.clear();
                    if (response.body().getData().size() >= 0) {
                        items.addAll(response.body().getData());
                        Collections.sort(items, new Comparator<MealPlan>() {
                            @Override
                            public int compare(MealPlan o1, MealPlan o2) {
                                return Long.valueOf(o1.getDateNumber()).compareTo(Long.valueOf(o2.getDateNumber()));
                            }
                        });
                        if(items.size()>0) {
                            fillList();
                        }else{
                            holderLl.setVisibility(View.GONE);
                        }
                    }
                }else{
                    holderLl.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<PublicMealResponse> call, Throwable throwable) {
                holderLl.setVisibility(View.GONE);
            }
        });

    }

    private void fillList() {
        boolean hasToday = false;

        for (int i = 0; i < items.size(); i++) {
            if (MdocUtil.isToday(items.get(i).getDateNumber())) {
                todayIndex = i;
                dayIndex = i;
                setMonthAndDateTv(i);
                hasToday = true;
            }
        }
        if(!hasToday){
            if(items.size()>0){
                todayIndex = 0;
                dayIndex = 0;
                setMonthAndDateTv(0);
            }
        }
        setLayout();
    }

    private void setMonthAndDateTv(int i) {
        resetTextViewColors();

        if (checkTimestamp == items.get(items.size() - 1).getDateNumber()) {
            return;
        }
        dayOneText.setText(getDate(items.get(i).getDateNumber()));
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.blue_gray));
        firstDayClick = i;

        i++;
        dayTwoText.setText(getDate(items.get(i).getDateNumber()));
        secondDayClick = i;

        i++;
        dayThreeText.setText(getDate(items.get(i).getDateNumber()));
        checkTimestamp = items.get(i).getDateNumber();
        totalIndex = i;
        thirdDayClick = i;


        setLayout();
    }

    private String getDate(long timeStamp) {

        try {
            DateFormat sdf = new SimpleDateFormat("MMM dd", Locale.getDefault());
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        } catch (Exception ex) {
            return "xx";
        }
    }

    private void setLayout() {
        setFutureLayout(dayIndex);
    }

    private void setFutureLayout(int dayIndex) {
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers(); //getTomorrowsMealOffer(thisWeek.getMealPlanOld().getWeeklyMealOffer(), nextWeek.getMealPlanOld().getWeeklyMealOffer());
        adapter = null;
        adapter = new KioskMealOfferAdapter(activity, items.get(dayIndex).getOffers());
        mealOfferRv.setAdapter(adapter);
    }

    private void resetTextViewColors() {
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        dayTwoText.setTextColor(ContextCompat.getColor(activity, R.color.black));
        dayThreeText.setTextColor(ContextCompat.getColor(activity, R.color.black));
    }

    @OnClick(R.id.linerDayOne)
    public void onDayOneClick() {
        resetTextViewColors();
        setFutureLayout(firstDayClick);
        dayOneText.setTextColor(ContextCompat.getColor(activity, R.color.blue_gray));
    }

    @OnClick(R.id.linerDayTwo)
    public void onDayTwoClick() {
        resetTextViewColors();
        setFutureLayout(secondDayClick);
        dayTwoText.setTextColor(ContextCompat.getColor(activity, R.color.blue_gray));
    }

    @OnClick(R.id.linerDayThree)
    public void onDayThreeClick() {
        resetTextViewColors();
        setFutureLayout(thirdDayClick);
        dayThreeText.setTextColor(ContextCompat.getColor(activity, R.color.blue_gray));
    }

    @OnClick(R.id.leftDayIv)
    public void onLeftMonthIvClick() {
        if (dayIndex > 0) {
            Timber.d("onLeftMonthIvClick");
            checkTimestamp = 0;
            dayIndex--;
            setMonthAndDateTv(dayIndex);
        }
    }

    @OnClick(R.id.rightDayIv)
    public void onRightMonthIvClick() {

        if (totalIndex < items.size() - 1) {
            dayIndex++;
            setMonthAndDateTv(dayIndex);
        }
    }
}


package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.FileListItem;
import de.mdoc.pojo.SharingList;

public class FileUpdate {

    private String id;
    private String name;
    private String owner;
    private String path;
    private String extension;
    private int size;
    private String description;
    private String category;
    private ArrayList<SharingList> sharingList = null;

    public FileUpdate() {
    }

    public FileUpdate(FileListItem item) {
        this.id = item.getId();
        this.name = item.getName();
        this.owner = item.getOwner();
        this.path = item.getPath();
        this.extension = item.getExtension();
        this.size = item.getSize();
        this.description = item.getDescription();
        this.category = item.getCategory();
        this.sharingList = item.getSharingList();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<SharingList> getSharingList() {
        return sharingList;
    }

    public void setSharingList(ArrayList<SharingList> sharingList) {
        this.sharingList = sharingList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}

package de.mdoc.modules.diary

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.diary.search.SearchDiaryFragmentDirections
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import de.mdoc.pojo.DiaryList
import kotlinx.android.synthetic.main.fragment_dialog_edit.*
import kotlinx.android.synthetic.main.fragment_dialog_media.img_close

class DiaryBottomEditDialogFragment : BottomSheetDialogFragment(){


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_dialog_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var item = arguments?.getSerializable(MdocConstants.CATEGORY_ITEM) as DiaryList
        var isComingFromSearch = arguments?.getSerializable(MdocConstants.DIARY_IS_SEARCH) as Boolean

        diaryLl?.setOnClickListener {
            val pollId = if(item.diaryConfig?.pollId != null) item.diaryConfig?.pollId else ""
            val questionnaireAdditionalFields = QuestionnaireAdditionalFields(
                    showFreeText = item.diaryConfig?.isFreeTextEntryAllowed!!,
                    allowSharing = item.showToDoctor,
                    autoShare = item.diaryConfig?.isAutoShare!!,
                    isDiary = true,
                    shouldAssignQuestionnaire = false,
                    diaryConfigId = item.diaryConfig?.id,
                    pollId = pollId,
                    pollAssignId = pollId,
                    diaryList = item)
            val action = if (isComingFromSearch) {
                SearchDiaryFragmentDirections.actionSearchDiaryFragmentToDiaryEntryFragment(questionnaireAdditionalFields)
            } else {
                DiaryDashboardFragmentDirections.actionDiaryDashboardFragmentToDiaryEntryFragment(questionnaireAdditionalFields)
            }
            activity?.findNavController(R.id.navigation_host_fragment)?.navigate(action)
            dismiss()
        }

        questionnaireLl?.setOnClickListener {
            val pollId = if(item.diaryConfig?.pollId != null) item.diaryConfig?.pollId else ""
            val questionnaireAdditionalFields = QuestionnaireAdditionalFields(
                    showFreeText = item.diaryConfig?.isFreeTextEntryAllowed!!,
                    allowSharing = item.showToDoctor,
                    autoShare = item.diaryConfig?.isAutoShare!!,
                    isDiary = true,
                    shouldAssignQuestionnaire = false,
                    diaryConfigId = item.diaryConfig?.id,
                    pollId = pollId,
                    pollAssignId = null,
                    pollVotingId = item.pollVotingActionId,
                    diaryList = item)
            val action = MainNavDirections.globalActionToQuestion(questionnaireAdditionalFields)
            findNavController().navigate(action)
            dismiss()
        }

        img_close?.setOnClickListener {
            dismiss()
        }

    }

    companion object {
        fun newInstance(item: DiaryList?, isComingFromSearch: Boolean): DiaryBottomEditDialogFragment {
            val args = Bundle().also {
                it.putSerializable(MdocConstants.CATEGORY_ITEM, item!!)
                it.putSerializable(MdocConstants.DIARY_IS_SEARCH, isComingFromSearch!!)
            }
            val fragment = DiaryBottomEditDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }


}
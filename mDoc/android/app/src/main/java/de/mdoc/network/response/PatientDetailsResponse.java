package de.mdoc.network.response;

import de.mdoc.pojo.PatientDetailsData;

public class PatientDetailsResponse extends MdocResponse {

    private PatientDetailsData data;

    public PatientDetailsData getData() {
        return data;
    }

    public void setData(PatientDetailsData data) {
        this.data = data;
    }
}

package de.mdoc.modules.files.adapters.viewholders

import android.view.View
import android.widget.TextView
import de.mdoc.pojo.FileDetailsItemType
import kotlinx.android.synthetic.main.file_details_simple_item.view.*

class FileDetailsListItemViewHolder(itemView: View): BaseFileItemViewHolder(itemView) {
    private val txtLabel : TextView = itemView.txtLabel
    private val txtValue : TextView = itemView.txtValue

    fun bind (item: Map.Entry<FileDetailsItemType, Any>?) {
        txtLabel.text = txtLabel.context.getText(item?.key?.itemName?:-1)
        txtValue.text = item?.value.toString()
    }
}
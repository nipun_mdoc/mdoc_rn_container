package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import de.mdoc.R
import de.mdoc.databinding.ActivityPatientDetailsBinding
import de.mdoc.modules.profile.patient_details.adapter.AutoCompleteInsuranceAdapter
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.util.AutoCompleteValidator
import de.mdoc.util.ColorUtil
import kotlinx.android.synthetic.main.activity_patient_details.*

class PatientDetailsActivity: AppCompatActivity() {

    companion object {
        val APPOINTMENT_ID = "APPOINTMENT_ID"
    }

    lateinit var patientDetailsViewModel: PatientDetailsViewModel
    lateinit var appointmentId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parseIntent()

        patientDetailsViewModel = ViewModelProviders.of(this)
            .get(PatientDetailsViewModel()::class.java)
        val binding: ActivityPatientDetailsBinding =
                DataBindingUtil.setContentView(this, R.layout.activity_patient_details)
        binding.patientDetailsViewModel = patientDetailsViewModel
        binding.lifecycleOwner = this
        titleTv.setTextColor(ColorUtil.contrastColor(resources))
        patientDetailsViewModel.error.observe(this, Observer {
            if (it.hasError) {
                showSnackBar(getString(R.string.meta_data_error_message), container)
            }
        })
        patientDetailsViewModel.isFirstFragmentReady.observe(this, Observer {
            if (it) {
                showNewFragment(
                        PersonalIDataFragment(), R.id.container)
            }
        })
        patientDetailsViewModel.currentNavigationItem.observe(this, Observer {
            handleNavigationBar(it)
        })
    }

    private fun parseIntent () {
        appointmentId = intent.getStringExtra(APPOINTMENT_ID)
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .commit()
    }

    private fun showSnackBar(message: String, view: View) {
        val snackBar = Snackbar.make(
                view,
                message, Snackbar.LENGTH_INDEFINITE
                                    )
            .setAction(resources
                .getString(R.string.meta_data_hide)) {
            }
        snackBar.show()
    }

    private fun handleNavigationBar(navigationPosition: Int) {
        val holderList = listOf(firstRl, secondRl, thirdRl, fourthRl, fifthRl, sixtrhRl)
        val textList = listOf(firstTv, secondTv, thirdTv, fourthTv, fifthTv, sixthTv)

        holderList.forEachIndexed { index, relativeLayout ->
            when {
                navigationPosition == index -> {
                    relativeLayout.setBackgroundResource(R.drawable.circle_mac_and_cheese_border)
                    textList.getOrNull(index)
                        ?.text = (index + 1).toString()
                }

                navigationPosition > index  -> {
                    relativeLayout.setBackgroundResource(R.drawable.circle_mac_cheese)
                    textList.getOrNull(index)
                        ?.text = getString(R.string.checkmark_string)
                }

                else                        -> {
                    relativeLayout.setBackgroundResource(R.drawable.circle_light_gray_e2e2e2)
                    textList.getOrNull(index)
                        ?.text = (index + 1).toString()
                }
            }
        }
    }

    fun generateAutoComplete(view: AutoCompleteTextView,
                             items: ArrayList<InsuranceDataKt>,
                             hasDefaultSelection: Boolean = true,
                             callback: (InsuranceDataKt) -> Unit) {
        val insuranceList: ArrayList<InsuranceDataKt> = arrayListOf()
        if (hasDefaultSelection) {
            insuranceList.add(InsuranceDataKt("", getString(R.string.none)))
        }
        insuranceList.addAll(items)
        val adapter = AutoCompleteInsuranceAdapter(this, insuranceList)
        view.setAdapter(adapter)
        view.validator = AutoCompleteValidator(items)
        view.threshold = 0
        view.onItemClickListener =
                AdapterView.OnItemClickListener { parent, _, position, _ ->
                    callback(parent.getItemAtPosition(position) as InsuranceDataKt)
                }

        view.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                view.showDropDown()
            }
            else {
                if (!view.validator.isValid(view.text)) {
                    view.validator.fixText("")
                    callback(InsuranceDataKt("none", null))
                }
            }
        }
        view.setOnClickListener {
            view.showDropDown()
        }

        view.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                callback(parent?.getItemAtPosition(position) as InsuranceDataKt)
            }
        }
    }

    fun getPositionOfItem(inputList: ArrayList<InsuranceDataKt>?,
                          coverage: String?): Int {
        var index = -1
        inputList?.forEachIndexed { position, insuranceDataKt ->
            if (insuranceDataKt.id == coverage) {
                index = position
            }
        }
        return index
    }

    fun setNavigationItem(navigation: Int) {
        patientDetailsViewModel.currentNavigationItem.value = navigation
    }

    fun getNavigationItem(): Int {
        return patientDetailsViewModel.currentNavigationItem.value ?: 0
    }
}
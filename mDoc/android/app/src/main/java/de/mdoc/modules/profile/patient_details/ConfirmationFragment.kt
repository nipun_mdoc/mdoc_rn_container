package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.MetaDataPostRequest
import de.mdoc.network.response.MdocResponse
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.activity_patient_details.*
import kotlinx.android.synthetic.main.fragment_confirmation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.annotation.Nullable

class ConfirmationFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_confirmation, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity?.progress_layout?.visibility = View.GONE

        cancelBtn.setOnClickListener { activity?.finish() }

        sendDataBtn.setOnClickListener {
            val request = MetaDataPostRequest()
            request.coverages = PatientDetailsHelper.getInstance()
                .coverages
            request.professional = PatientDetailsHelper.getInstance()
                .familyDoctorData
            request.user = PatientDetailsHelper.getInstance()
                .patientDetailsData
            request.appointmentId = (activity as PatientDetailsActivity).appointmentId

            MdocManager.postMetadata(request, object: Callback<MdocResponse> {
                override fun onResponse(call: Call<MdocResponse>,
                                        response: Response<MdocResponse>) {
                    if (response.isSuccessful) {
                        showNewFragment(
                                SuccessFragment(), R.id.container)
                    }
                    else {
                        MdocUtil.showToastLong(context, getString(R.string.something_went_wrong))
                    }
                }

                override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                    MdocUtil.showToastLong(context, getString(R.string.something_went_wrong))
                }
            })
        }
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity!!.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }
}

package de.mdoc.data.questionnaire

data class Data(
        val caseId: String,
        val questionnaires: ArrayList<Questionnaire>
)
package de.mdoc.modules.messages.data

data class AdditionalSharingInfo(val sourceId: String = "",
                                 val sourceClassName: String = "",
                                 val active: Boolean = false,
                                 val sourceTitle: String = "")
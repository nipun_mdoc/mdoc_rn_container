package de.mdoc.modules.mental_goals.mental_goal_adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import kotlinx.android.synthetic.main.placeholder_dashboard_if_then.view.*


class DashboardWhenThenAdapter(var context: Context, var items: List<IfThenPlan>) :
    RecyclerView.Adapter<DashboardWhenThenViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DashboardWhenThenViewHolder {
        context = parent.context
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_dashboard_if_then, parent, false)

        return DashboardWhenThenViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboardWhenThenViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item = items[position]
        val planNumber = String.format("%02d", position + 1)

        holder.txtPlanLabel.text = context.resources.getString(R.string.when_then_plan, planNumber)


        val ifThenText =
            "${context.resources.getString(R.string.`when`)} ${item.ifValue}, ${context.resources.getString(
                R.string.then
            )} ${item.thenValue}"
        holder.txtWhenThen.text = ifThenText
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class DashboardWhenThenViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtPlanLabel: TextView = itemView.txt_plan_label
    var txtWhenThen: TextView = itemView.txt_when_then
}
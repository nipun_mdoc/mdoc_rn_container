package de.mdoc.modules.mental_goals.overview

import androidx.lifecycle.ViewModel
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsData
import de.mdoc.modules.mental_goals.overview.data.MentalGoalsRepository
import de.mdoc.viewmodel.combine
import de.mdoc.viewmodel.liveData
import de.mdoc.viewmodel.map
import timber.log.Timber

enum class GoalOverviewTab {
    Open,
    Closed
}

class GoalOverviewViewModel(
    private val repository: MentalGoalsRepository
) : ViewModel() {

    val countNewGoals = liveData<Int>(0)
    val countAlmostReached = liveData<Int>(0)
    val countCompletedGoals = liveData<Int>(0)

    val selectedTab = liveData(GoalOverviewTab.Open)

    private val openGoals = liveData<List<MentalGoalsData>?>(null)
    private val closedGoals = liveData<List<MentalGoalsData>?>(null)

    val listOfGoals = combine(selectedTab, openGoals, closedGoals) { tab, open, closed ->
        when (tab) {
            GoalOverviewTab.Open -> open
            GoalOverviewTab.Closed -> closed
        }
    }

    val isShowNoNewGoals = combine(selectedTab, openGoals, closedGoals) { tab, openList, closeList ->
        tab == GoalOverviewTab.Open && openList != null && openList.isEmpty() && closeList != null && closeList.isNotEmpty()
    }
    val isShowNoClosedGoals = combine(selectedTab, openGoals, closedGoals) { tab, openList, closeList ->
        tab == GoalOverviewTab.Closed && openList != null && openList.isNotEmpty() && closeList != null && closeList.isEmpty()
    }

    val isShowNoOpenOrClosedGoals = combine(openGoals, closedGoals) { openList, closedList ->
        openList != null && openList.isEmpty() && closedList != null && closedList.isEmpty()
    }

    val isInProgress = listOfGoals.map {
        it == null
    }

    val error = liveData<Unit>()

    init {
        repository.getGamificationData({
            countNewGoals.set(it.countNew)
            countAlmostReached.set(it.countAlmostReached)
            countCompletedGoals.set(it.countCompleted)
        }, ::onError)

        getOpenGoals()

        getClosedGoals()

    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        error.set(Unit)
    }

     fun getOpenGoals(){
        repository.getOpenGoals({
            openGoals.set(it)
        }, {
            if (openGoals.get() == null) {
                openGoals.set(emptyList())
            }
            onError(it)
        })
    }

     fun getClosedGoals(){
        repository.getClosedGoals({
            closedGoals.set(it)
        }, {
            if (closedGoals.get() == null) {
                closedGoals.set(emptyList())
            }
            onError(it)
        })
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }

}
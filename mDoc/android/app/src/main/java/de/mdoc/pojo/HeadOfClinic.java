package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/20/17.
 */

public class HeadOfClinic implements Serializable{

    private String image;
    private String description;
    private String name;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

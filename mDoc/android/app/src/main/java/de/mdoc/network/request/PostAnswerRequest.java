package de.mdoc.network.request;

import java.util.ArrayList;

/**
 * Created by ema on 7/10/17.
 */

public class PostAnswerRequest {

    private String pollId;
    private String pollAssignId;
    private String pollQuestionId;
    private String answerData;
    private String pollVotingActionId;
    private ArrayList<String> selection;
    private String language;

    public PostAnswerRequest(String pollId, String pollAssignId, String pollQuestionId, String answerData, String pollVotingActionId, ArrayList<String> selection) {
        this.pollId = pollId;
        this.pollAssignId = pollAssignId;
        this.pollQuestionId = pollQuestionId;
        this.answerData = answerData;
        this.pollVotingActionId = pollVotingActionId;
        this.selection = selection;
    }

    public PostAnswerRequest(String pollId, String pollAssignId, String pollQuestionId, String answerData, String pollVotingActionId, ArrayList<String> selection, String language) {
        this.pollId = pollId;
        this.pollAssignId = pollAssignId;
        this.pollQuestionId = pollQuestionId;
        this.answerData = answerData;
        this.pollVotingActionId = pollVotingActionId;
        this.selection = selection;
        this.language = language;
    }

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getPollQuestionId() {
        return pollQuestionId;
    }

    public void setPollQuestionId(String pollQuestionId) {
        this.pollQuestionId = pollQuestionId;
    }

    public String getAnswerData() {
        return answerData;
    }

    public void setAnswerData(String answerData) {
        this.answerData = answerData;
    }

    public String getPollVotingActionId() {
        return pollVotingActionId;
    }

    public void setPollVotingActionId(String pollVotingActionId) {
        this.pollVotingActionId = pollVotingActionId;
    }
}

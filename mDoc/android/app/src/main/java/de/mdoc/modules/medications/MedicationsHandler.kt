package de.mdoc.modules.medications

import android.content.Context
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import de.mdoc.R
import de.mdoc.modules.medications.create_plan.data.MedicationAdapterData
import de.mdoc.modules.medications.data.MedicationAdministration
import de.mdoc.modules.medications.data.getDosageInfo
import de.mdoc.modules.medications.medication_widget.MedicationWidgetViewModel
import de.mdoc.util.MdocUtil
import org.joda.time.format.DateTimeFormat

class MedicationsHandler(private val medicationsViewModel: MedicationsViewModel,
                         private val medicationWidgetViewModel: MedicationWidgetViewModel?=null) {

    // Main RecyclerView
    fun getTime(item: MedicationAdapterData): String {
        val hour = String.format("%02d", item.timeOfDay?.hour)
        val minute = String.format("%02d", item.timeOfDay?.minute)
        return "$hour:$minute"
    }

    fun isTimeHeaderVisible(item: MedicationAdapterData): Boolean {
        return item.fullTimeOfDay != null
    }

    fun setTimeHeader(context: Context, item: MedicationAdapterData): String {
        val dateFormatter = DateTimeFormat.forPattern("dd.MM.")
        item.fullTimeOfDay?.let {
            return if (MdocUtil.isToday(it.millis)) {
                context.getString(R.string.med_overview_date_today, dateFormatter.print(it))
            } else {
                context.getString(R.string.med_overview_date_tomorrow, dateFormatter.print(it))
            }
        }
        return ""
    }

    // Inner RecyclerView
    fun getName(item: MedicationAdministration): String {
        return item.medication?.description ?: ""
    }

    fun getDosageInfo(context: Context, item: MedicationAdministration): String {
        return context.resources.getString(R.string.med_overview_take_dosage, item.getDosageInfo())
    }

    fun onInnerItemClick(context: Context, item: MedicationAdministration) {
        ItemOverviewFragment()
                .also {
                    it.medicationAdministration = item
                    it.medicationRequestId = item.medicationRequestId
                    it.parentViewModel = medicationsViewModel
                    it.medicationWidgetViewModel = medicationWidgetViewModel
                }
                .show((context as AppCompatActivity).supportFragmentManager, "itemOverview")
    }

    fun goToMedicationSearch(view: View) {
        view.findNavController().navigate(R.id.navigation_medications_search)
    }

    fun goToMedicationOverview (view: View) {
        view.findNavController().navigate(R.id.navigation_medications)
    }
}
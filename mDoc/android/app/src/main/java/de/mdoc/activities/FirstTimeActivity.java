package de.mdoc.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;

import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import timber.log.Timber;

public class FirstTimeActivity extends MdocBaseActivity {

    private static final int MY_CAMERA_REQUEST_CODE = 100;

    @Override
    public int getResourceId() {
        return R.layout.activity_first_time;
    }

    @Override
    public void init(Bundle savedInstanceState) {

    }

    @OnClick(R.id.btnOhoneQrCode)
    public void onOhneQrCode() {
        MdocAppHelper.getInstance().setIsFirstTime(true);
        navigateToLogin();
    }

    @OnClick(R.id.btnMitQrCode)
    public void onMitQrCode() {
        MdocAppHelper.getInstance().setIsFirstTime(true);
        callScanActivity();
    }

    private void callScanActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_REQUEST_CODE);
            } else {
                startActivity(new Intent(this, ScanActivity.class));
            }
        } else {
            startActivity(new Intent(this, ScanActivity.class));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == MY_CAMERA_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(new Intent(this, ScanActivity.class));
            } else {
                MdocUtil.showToastLong(this, "No Permission");
                Timber.w("Permissions are not granted");
            }

        }
    }

    private void navigateToLogin(){
        startActivity(new Intent(this, LoginActivity.class));
    }
}

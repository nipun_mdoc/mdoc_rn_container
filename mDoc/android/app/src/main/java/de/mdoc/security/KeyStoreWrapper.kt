package de.mdoc.security

import android.annotation.TargetApi
import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.security.keystore.StrongBoxUnavailableException
import androidx.annotation.RequiresApi
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.security.KeyStore
import java.security.UnrecoverableKeyException
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

/**
 * This class wraps [KeyStore] class apis with some additional possibilities.
 */
class KeyStoreWrapper(private val context: Context, defaultKeyStoreName: String) {

    private val keyStore: KeyStore = createAndroidKeyStore()
    private val defaultKeyStoreFile = File(context.filesDir, defaultKeyStoreName)
    private val defaultKeyStore = createDefaultKeyStore()

    /**
     * @return symmetric key from Android Key Store or null if any key with given alias exists
     */
    fun getAndroidKeyStoreSymmetricKey(alias: String): SecretKey? = keyStore.getKey(alias, null) as SecretKey?

    /**
     * @return symmetric key from Default Key Store or null if any key with given alias exists
     */
    fun getDefaultKeyStoreSymmetricKey(alias: String, keyPassword: String): SecretKey? {
        return try {
            defaultKeyStore.getKey(alias, keyPassword.toCharArray()) as SecretKey
        } catch (e: UnrecoverableKeyException) {
            null
        }
    }

    /**
     * Remove key with given alias from Android Key Store
     */
    fun removeAndroidKeyStoreKey(alias: String) = keyStore.deleteEntry(alias)

    fun createDefaultKeyStoreSymmetricKey(alias: String, password: String) {
        val key = generateDefaultSymmetricKey()
        val keyEntry = KeyStore.SecretKeyEntry(key)

        defaultKeyStore.setEntry(alias, keyEntry, KeyStore.PasswordProtection(password.toCharArray()))
        defaultKeyStore.store(FileOutputStream(defaultKeyStoreFile), password.toCharArray())
    }

    /**
     * Generates symmetric [KeyProperties.KEY_ALGORITHM_AES] key with default [KeyProperties.BLOCK_MODE_CBC] and
     * [KeyProperties.ENCRYPTION_PADDING_PKCS7] using default provider.
     */
    fun generateDefaultSymmetricKey(): SecretKey {
        val keyGenerator = KeyGenerator.getInstance("AES")
        return keyGenerator.generateKey()
    }

    /**
     * Creates symmetric [KeyProperties.KEY_ALGORITHM_AES] key with default [KeyProperties.BLOCK_MODE_CBC] and
     * [KeyProperties.ENCRYPTION_PADDING_PKCS7] and saves it to Android Key Store.
     */
    @TargetApi(Build.VERSION_CODES.M)
    fun createAndroidKeyStoreSymmetricKey(
            alias: String,
            userAuthenticationRequired: Boolean = false,
            invalidatedByBiometricEnrollment: Boolean = true,
            userAuthenticationValidityDurationSeconds: Int = -1,
            userAuthenticationValidWhileOnBody: Boolean = true){
        val keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore")
        if (SystemServices.hasPie()) {
            try {
                initGeneratorWithStrongbox(keyGenerator, alias, userAuthenticationRequired,
                        invalidatedByBiometricEnrollment, userAuthenticationValidityDurationSeconds,
                        userAuthenticationValidWhileOnBody)
            } catch (e: StrongBoxUnavailableException) {
                initGenerator(keyGenerator, alias, userAuthenticationRequired, invalidatedByBiometricEnrollment,
                        userAuthenticationValidityDurationSeconds, userAuthenticationValidWhileOnBody)
            }
        }else{
            initGenerator(keyGenerator, alias, userAuthenticationRequired, invalidatedByBiometricEnrollment,
                    userAuthenticationValidityDurationSeconds, userAuthenticationValidWhileOnBody)
        }
    }

    @RequiresApi(Build.VERSION_CODES.P)
    private fun initGeneratorWithStrongbox(generator: KeyGenerator,
                                           alias: String,
                                           userAuthenticationRequired: Boolean = false,
                                           invalidatedByBiometricEnrollment: Boolean = true,
                                           userAuthenticationValidityDurationSeconds: Int = -1,
                                           userAuthenticationValidWhileOnBody: Boolean = true) {
        val builder = KeyGenParameterSpec.Builder(alias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            .setUserAuthenticationRequired(userAuthenticationRequired)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
            .setUserAuthenticationValidityDurationSeconds(userAuthenticationValidityDurationSeconds)
            .setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)
            .setUserAuthenticationValidWhileOnBody(userAuthenticationValidWhileOnBody)
            .setIsStrongBoxBacked(true)

        generator.init(builder.build())
        generator.generateKey()
    }

    private fun initGenerator(generator: KeyGenerator, alias: String, userAuthenticationRequired: Boolean = false,
                              invalidatedByBiometricEnrollment: Boolean = true,
                              userAuthenticationValidityDurationSeconds: Int = -1,
                              userAuthenticationValidWhileOnBody: Boolean = true) {
        val builder = KeyGenParameterSpec.Builder(alias, KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
            .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
            .setUserAuthenticationRequired(userAuthenticationRequired)
            .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
            .setUserAuthenticationValidityDurationSeconds(userAuthenticationValidityDurationSeconds)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            builder.setInvalidatedByBiometricEnrollment(invalidatedByBiometricEnrollment)
            builder.setUserAuthenticationValidWhileOnBody(userAuthenticationValidWhileOnBody)
        }
        generator.init(builder.build())
        generator.generateKey()
    }

    private fun createAndroidKeyStore(): KeyStore {
        val keyStore = KeyStore.getInstance("AndroidKeyStore")
        keyStore.load(null)
        return keyStore
    }

    private fun createDefaultKeyStore(): KeyStore {
        val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())

        if (!defaultKeyStoreFile.exists()) {
            keyStore.load(null)
        }
        else {
            keyStore.load(FileInputStream(defaultKeyStoreFile), null)
        }
        return keyStore
    }
}


package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import de.mdoc.interfaces.BleResponseListener
import timber.log.Timber
import java.lang.ref.WeakReference
import java.util.*
import kotlin.collections.ArrayList


class Po60BleWrapper(private val context: WeakReference<Context>, deviceAddress: String) : Handler.Callback {

    private var bluetoothDevice: BluetoothDevice? = null
    private val bleHandler: Handler
    private var myBleCallback: MyBleCallback? = null

    private var counter = 0

    private var characteristicWrite: BluetoothGattCharacteristic? = null
    private var acquiredMeasurementResponseArrayList: ArrayList<Byte>? = null
    private var bleListener: WeakReference<BleResponseListener>? = null

    fun setBleResponseListener(bleListener: WeakReference<BleResponseListener>) {
        this.bleListener = bleListener
    }

    init {
        val handlerThread = HandlerThread("BleThread")
        handlerThread.start()

        bleHandler = Handler(handlerThread.looper, this)
        myBleCallback = MyBleCallback(WeakReference<Po60BleWrapper>(this))

        context.get()?.let {
            bluetoothDevice = getBluetoothDevice(it, deviceAddress)
        }
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {

        val bluetoothManager = context
                .getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }

    fun connect(autoConnect: Boolean) {
        bleHandler.obtainMessage(MSG_CONNECT, autoConnect).sendToTarget()
    }

    fun disconnect() {
        bleHandler.obtainMessage(MSG_DISCONNECT, bluetoothDevice?.address).sendToTarget()

    }

    override fun handleMessage(message: Message): Boolean {
        when (message.what) {
            MSG_CONNECT -> doConnect(message.obj as Boolean)
            MSG_CONNECTED -> (message.obj as BluetoothGatt).discoverServices()
            MSG_DISCONNECT -> (message.obj as BluetoothGatt).disconnect()
            MSG_DISCONNECTED -> (message.obj as BluetoothGatt).close()
            MSG_SERVICES_DISCOVERED -> {
                val characteristicNotification = (message.obj as BluetoothGatt).getService(UUID.fromString(BEURER_PO60_CUSTOM_SERVICE))
                        .getCharacteristic(UUID.fromString(BEURER_PO60_CHARACTERISTIC_NOTIFICATION))

                (message.obj as BluetoothGatt).setCharacteristicNotification(characteristicNotification, true)

                val descriptor = characteristicNotification.getDescriptor(
                        UUID.fromString(BEURER_PO60_CHARACTERISTIC_NOTIFICATION_DESC))

                descriptor.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                (message.obj as BluetoothGatt).writeDescriptor(descriptor)
            }
            MSG_DESCRIPTOR_WRITE -> {

                if (acquiredMeasurementResponseArrayList == null) {
                    acquiredMeasurementResponseArrayList = ArrayList()
                }

                characteristicWrite = (message.obj as BluetoothGatt)
                        .getService(UUID.fromString(BEURER_PO60_CUSTOM_SERVICE))
                        .getCharacteristic(UUID.fromString(BEURER_PO60_CHARACTERISTIC_WRITE))

                characteristicWrite!!.value = ACQUIRE_MEASUREMENT_DATA_COMMAND
                acquiredMeasurementResponseArrayList!!.clear()
                (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)

            }

            MSG_CHARACTERISTIC_CHANGE -> {
                // Send command to get next 10 reads
                characteristicWrite!!.value = ACQUIRE_NEXT_MEASUREMENT_DATA_COMMAND
                (message.obj as BluetoothGatt).writeCharacteristic(characteristicWrite)
            }
            MSG_LAST_PACKET -> {
                bleListener?.get()?.dataFromDeviceCallback(acquiredMeasurementResponseArrayList, (message.obj as BluetoothGatt))
                (message.obj as BluetoothGatt).disconnect()
            }
        }
        return true
    }

    private fun doConnect(autoConnect: Boolean) {
        context.get()?.let {
            bluetoothDevice?.connectGatt(it, autoConnect, myBleCallback)
        }
    }


    private inner class MyBleCallback(val weakReference: WeakReference<Po60BleWrapper>) : BluetoothGattCallback() {

        override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
            super.onConnectionStateChange(gatt, status, newState)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                when (newState) {
                    BluetoothGatt.STATE_CONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                        weakReference.get()?.bleHandler?.obtainMessage(MSG_CONNECTED, gatt)?.sendToTarget()

                    }
                    BluetoothGatt.STATE_DISCONNECTED -> {
                        Timber.d("onConnectionStateChange: STATE_DISCONNECTED $gatt $status")
                        weakReference.get()?.bleHandler?.obtainMessage(MSG_DISCONNECTED, gatt)?.sendToTarget()
                    }
                }
            }
        }

        override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
            super.onServicesDiscovered(gatt, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Timber.d("onServicesDiscovered: $gatt")
                weakReference.get()?.bleHandler?.obtainMessage(MSG_SERVICES_DISCOVERED, gatt)?.sendToTarget()
            }
        }

        override fun onDescriptorWrite(gatt: BluetoothGatt, descriptor: BluetoothGattDescriptor, status: Int) {
            super.onDescriptorWrite(gatt, descriptor, status)
            if (status == BluetoothGatt.GATT_SUCCESS) {
                weakReference.get()?.bleHandler?.obtainMessage(MSG_DESCRIPTOR_WRITE, gatt)?.sendToTarget()

            } else {
                Timber.d("onDescriptorWrite: FAILED $gatt")
            }
        }

        override fun onCharacteristicChanged(gatt: BluetoothGatt, characteristic: BluetoothGattCharacteristic) {
            //  Get response of service in raw data, concentrate and show in human readable format.
            super.onCharacteristicChanged(gatt, characteristic)

            weakReference.get()?.let {
                it.counter++
            }

            for (item in characteristic.value) {
                weakReference.get()?.acquiredMeasurementResponseArrayList?.add(item)
            }

            // Debug info
            val measurementResponse = weakReference.get()?.acquiredMeasurementResponseArrayList
            Timber.d("onCharacteristicChanged: %s", Arrays.toString(characteristic.value))
            Timber.d("DeviceOutputOnCharacteristicChanged: %s", measurementResponse?.toString())
            Timber.d("heartBeat: %s", measurementResponse?.getOrNull(measurementResponse.size ?: 0 - 2))
            Timber.d("SPo2: %s", measurementResponse?.getOrNull(measurementResponse.size ?: 0 - 5))

            if (weakReference.get()?.counter == 12) {
                weakReference.get()?.bleHandler?.obtainMessage(MSG_CHARACTERISTIC_CHANGE, gatt)?.sendToTarget()
                weakReference.get()?.counter = 0
            }

            if (weakReference.get()?.bleListener != null && measurementResponse != null && (measurementResponse.size % 24 == 0)) {
                if (measurementResponse[measurementResponse.size - 23] > 9) {
                    weakReference.get()?.bleListener?.get()?.dataFromDeviceCallback(measurementResponse, gatt)
                    gatt.disconnect()
                }
            }
        }

        override fun onCharacteristicRead(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
            super.onCharacteristicRead(gatt, characteristic, status)
            Timber.i("onCharacteristicRead $characteristic")
            val value = characteristic?.value ?: byteArrayOf()
            val v = String(value)
            Timber.i("onCharacteristicRead Value: $v")
        }
    }

    companion object {
        private const val MSG_CONNECT = 10
        private const val MSG_CONNECTED = 20
        private const val MSG_DISCONNECT = 30
        private const val MSG_DISCONNECTED = 40
        private const val MSG_SERVICES_DISCOVERED = 50
        private const val MSG_DESCRIPTOR_WRITE = 60
        private const val MSG_CHARACTERISTIC_CHANGE = 70
        private const val MSG_LAST_PACKET = 80
        private const val BEURER_PO60_CUSTOM_SERVICE = "0000ff12-0000-1000-8000-00805f9b34fb"
        private const val BEURER_PO60_CHARACTERISTIC_WRITE = "0000ff01-0000-1000-8000-00805f9b34fb"
        private const val BEURER_PO60_CHARACTERISTIC_NOTIFICATION = "0000ff02-0000-1000-8000-00805f9b34fb"
        private const val BEURER_PO60_CHARACTERISTIC_NOTIFICATION_DESC = "00002902-0000-1000-8000-00805f9b34fb"

        private val ACQUIRE_MEASUREMENT_DATA_COMMAND = byteArrayOf(0x99.toByte(), 0x00, 0x19)
        private val ACQUIRE_NEXT_MEASUREMENT_DATA_COMMAND = byteArrayOf(0x99.toByte(), 0x01, 0x1A)
    }
}
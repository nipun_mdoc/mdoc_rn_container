package de.mdoc.modules.vitals.unit_picker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentUnitsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class UnitPickerFragment: NewBaseFragment() {
    override val navigationItem: NavigationItem
        get() = NavigationItem.Units

    private val args: UnitPickerFragmentArgs by navArgs()

    private val unitPickerViewModel by viewModel {
        UnitPickerViewModel(
                RestClient.getService(), args.unitItems ?: arrayOf())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentUnitsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_units, container, false)

        binding.lifecycleOwner = this
        binding.unitPickerViewModel = unitPickerViewModel
        return binding.root
    }
}
package de.mdoc.modules.profile.patient_details

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.mdoc.R
import kotlinx.android.synthetic.main.fragment_success.*
import javax.annotation.Nullable


class SuccessFragment: Fragment() {

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backBtn?.setOnClickListener {
            activity?.setResult(Activity.RESULT_OK, Intent())
            activity?.finish()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_success, container, false)
    }
}

package de.mdoc.modules.messages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.viewpager.widget.ViewPager
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.ACTIVE_INBOX
import de.mdoc.databinding.FragmentConversationsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.messages.adapters.MessagesPagerAdapter
import de.mdoc.modules.messages.conversations.ConversationsHandler
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.debounce
import de.mdoc.util.handleOnBackPressed
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_conversations.*

class ConversationsFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Messages
    private val conversationsViewModel by viewModel {
        ConversationsViewModel(RestClient.getService())
    }

    private val configViewModel by viewModel {
        MdocAppHelper.getInstance().getConfigurationData()
    }

    var oldQuery = ""

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentConversationsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_conversations, container, false)

        binding.configHandler = configViewModel

        binding.lifecycleOwner = this
        binding.handler = ConversationsHandler()
        binding.conversationsViewModel = conversationsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleOnBackPressed {
            val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)

            if (!hasMoreFragment) {
                val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)

                if (!hasDashboardFragment) {
                    findNavController().popBackStack()
                }
            }
        }
        setupPagerAdapter()

        conversationsViewModel.query.debounce(500)
            .observe(viewLifecycleOwner, Observer { query ->
                if (query.isNotEmpty()) {
                    et_layout?.endIconDrawable = resources.getDrawable(R.drawable.ic_cancel_search_messages)
                    et_layout?.setEndIconOnClickListener {
                        conversationsViewModel.query.value = ""
                    }
                }
                else {
                    et_layout?.endIconDrawable = null
                }
                if (query != oldQuery) {
                    if (pager_conversations?.currentItem == ACTIVE_INBOX) {
                        conversationsViewModel.getActiveConversations(!query.isEmpty())
                    }
                    else {
                        conversationsViewModel.getClosedConversations(!query.isEmpty())
                    }
                    oldQuery = query
                }
            })
        val pageChangeListener = object: ViewPager.SimpleOnPageChangeListener() {
            override fun onPageSelected(position: Int) {
                if (pager_conversations?.currentItem == ACTIVE_INBOX) {
                    conversationsViewModel.getActiveConversations(!oldQuery.isEmpty())
                }
                else {
                    conversationsViewModel.getClosedConversations(!oldQuery.isEmpty())
                }
            }
        }
        pager_conversations?.addOnPageChangeListener(pageChangeListener)
    }

    private fun setupPagerAdapter() {
        pager_conversations?.adapter = MessagesPagerAdapter(childFragmentManager, conversationsViewModel, context)
        tab_layout?.setupWithViewPager(pager_conversations)
    }
}
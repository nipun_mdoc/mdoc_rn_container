package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.pojo.Address
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.newCodingItem
import kotlinx.android.synthetic.main.activity_patient_details.*
import kotlinx.android.synthetic.main.fragment_family_doctor.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.annotation.Nullable

class FamilyDoctorFragment: Fragment() {

    var countryList: ArrayList<CodingItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_family_doctor, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PatientDetailsActivity).setNavigationItem(4)

        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.let {
            fdFirstNameEdt.setText(it.firstName)
            fdLastNameEdt.setText(it.lastName)
        }
        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.address?.street?.let {
            fdStreenHauseNumberEdt.setText(it)
        }
        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.address?.houseNumber?.let {
            fdStreenHauseNumberEdt.append(it)
        }
        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.address?.city?.let { fdCityEdt.setText(it) }
        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.address?.postalCode?.let {
            fdPostcodeEdt.setText(it)
        }
        PatientDetailsHelper.getInstance()
            .familyDoctorData?.publicUserDetails?.phone?.let { fdPhoneEdt.setText(it) }


        previous4Btn.setOnClickListener{
                activity?.onBackPressed()
        }

        continue4Btn.setOnClickListener(object: View.OnClickListener {
            override fun onClick(v: View?) {
                val text = fdStreenHauseNumberEdt.text.toString()
                val index = text.lastIndexOf(" ")
                if (index != -1) {
                    val street = text.substring(0, index)
                    val number = text.substring(index, text.length)
                    PatientDetailsHelper.getInstance()
                        .familyDoctorData?.publicUserDetails?.address?.street = street
                    PatientDetailsHelper.getInstance()
                        .familyDoctorData?.publicUserDetails?.address?.houseNumber = number
                }
                else {
                    PatientDetailsHelper.getInstance()
                        .familyDoctorData?.publicUserDetails?.address?.street =
                            fdStreenHauseNumberEdt.text.toString()
                }

                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.firstName = fdFirstNameEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.lastName = fdLastNameEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.address?.city = fdCityEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.address?.postalCode =
                        fdPostcodeEdt.text.toString()

                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.phone = fdPhoneEdt.text.toString()

                if (PatientDetailsHelper.getInstance().familyDoctorData?.extendedUserDetails?.addressList == null || PatientDetailsHelper.getInstance().familyDoctorData?.extendedUserDetails?.addressList?.size == 0) {
                    PatientDetailsHelper.getInstance()
                        .familyDoctorData?.extendedUserDetails?.addressList = ArrayList<Address>()
                    PatientDetailsHelper.getInstance()
                        .familyDoctorData?.extendedUserDetails?.addressList?.add(
                            PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address)
                }
                else {
                    if (PatientDetailsHelper.getInstance().familyDoctorData?.extendedUserDetails?.addressList?.size!! > 0) {
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.city = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.city
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.countryCode = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.countryCode
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.country = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.country
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.street = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.street
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.houseNumber = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.houseNumber
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.state = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.state
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.description = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.description
                        PatientDetailsHelper.getInstance()
                            .familyDoctorData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.postalCode = PatientDetailsHelper.getInstance()
                            .familyDoctorData?.publicUserDetails?.address?.postalCode
                    }
                }

                showNewFragment(
                        InsuranceFragment(), R.id.container)
            }
        })
        //country list
        MdocManager.getCoding("https://mdoc.one/hl7/fhir/countryList",
                object: Callback<CodingResponse> {
                    override fun onResponse(call: Call<CodingResponse>,
                                            response: Response<CodingResponse>) {
                        if (response.isSuccessful) {
                            if (isAdded) {
                                countryList = response.body()!!.data?.list
                                countryList!!.add(0, newCodingItem("", ""))
                                var spinnerAdapter: SpinnerAdapter =
                                        SpinnerAdapter(context!!, countryList!!)
                                famDocSpinner?.adapter = spinnerAdapter
                                val index: Int? =
                                        countryList?.indexOf(newCodingItem(
                                                PatientDetailsHelper.getInstance().familyDoctorData?.publicUserDetails?.address?.countryCode))
                                if (index != null) {
                                    famDocSpinner?.setSelection(index)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CodingResponse>, t: Throwable) {
                    }
                })

        famDocSpinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.address?.countryCode =
                        countryList?.getOrNull(p2)
                            ?.code
                PatientDetailsHelper.getInstance()
                    .familyDoctorData?.publicUserDetails?.address?.country =
                        countryList?.getOrNull(p2)
                            ?.display
            }
        }
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity!!.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }
}

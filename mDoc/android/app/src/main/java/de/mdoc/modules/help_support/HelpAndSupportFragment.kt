package de.mdoc.modules.help_support

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentHelpAndSupportBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.help_support.data.HelpSupportData
import de.mdoc.modules.tutorial.TutorialDialogFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_help_and_support.*

class HelpAndSupportFragment: NewBaseFragment() {

    lateinit var activity: MdocActivity
    override val navigationItem: NavigationItem = NavigationItem.HelpSupport
    private val helpAndSupportViewModel by viewModel {
        HelpAndSupportViewModel(
                RestClient.getService()
                               )
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentHelpAndSupportBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_help_and_support, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = helpAndSupportViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val webSettings = webView?.settings
        webSettings?.defaultTextEncodingName = "utf-8"
        handleWebViewLinkClick()

        helpAndSupportViewModel.impressum.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val convertedHtml: String = it.replace("<a href=\"www", "<a href=\"http://www", true)
                webView.loadDataWithBaseURL(null, convertedHtml, "text/html; charset=utf-8", "UTF-8", null)
            }
        })
        setupClickListeners()
    }

    private fun openHelpSupportDialog() {
        val action = HelpAndSupportFragmentDirections.actionHelpAndSupportFragmentToHelpSupportDialog(
                getHelpSupportData())
        findNavController().navigate(action)
    }

    private fun openHelpSupportDialogBalgrist() {
        val action = HelpAndSupportFragmentDirections.actionHelpAndSupportFragmentToHelpSupportDialogDetails(
                getHelpSupportData())
        findNavController().navigate(action)
    }

    private fun setupClickListeners() {
        privacyPolicyRl?.setOnClickListener {
            showDialog(helpAndSupportViewModel.privacy.value ?: "")
        }

        termsConditionRl?.setOnClickListener {
            showDialog(helpAndSupportViewModel.terms.value ?: "")
        }

        helpSupportRl.setOnClickListener {
            if (context?.resources?.getBoolean(R.bool.show_help_and_support_balgrist) == true){
                openHelpSupportDialogBalgrist()
            } else {
                openHelpSupportDialog()
            }
        }

        tutorialRl?.setOnClickListener { showTutorial() }
    }

    private fun showDialog(htmlText: String) {
        val action = HelpAndSupportFragmentDirections.actionHelpAndSupportFragmentToWebViewDialogFragment(htmlText)
        findNavController().navigate(action)
    }

    private fun handleWebViewLinkClick() {
        webView?.webViewClient = object: WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                var newUrl = request?.url.toString()

                if (newUrl.contains("file:///android_asset/html/")) {
                    newUrl = newUrl.substring(27, newUrl.length)
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://$newUrl"))
                    activity.startActivity(browserIntent)
                }else{
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(newUrl))
                    activity.startActivity(browserIntent)
                }
                return true
            }
        }
    }

    private fun showTutorial() {
        val dialogFragment = TutorialDialogFragment()
        dialogFragment.show(childFragmentManager, "Tutorial Fragment")
    }

    private fun getHelpSupportData(): HelpSupportData {
        val branchAndCommit = when (BuildConfig.LastCommit) {
            null   -> BuildConfig.BranchName
            "null" -> BuildConfig.BranchName
            else   -> "${BuildConfig.BranchName}/${BuildConfig.LastCommit}"
        }

        return HelpSupportData(
                name = getString(R.string.app_name),
                app = "Android v${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})",
                branch = branchAndCommit,
                api = getString(R.string.base_url),
                iam = getString(R.string.keycloack_base_url)
                              )
    }
}
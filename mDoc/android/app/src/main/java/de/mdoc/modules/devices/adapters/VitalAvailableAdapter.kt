package de.mdoc.modules.devices.adapters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.pojo.MyDevices

class VitalAvailableAdapter(var context: Context, var items: ArrayList<MyDevices>) :
        RecyclerView.Adapter<VitalAvailableViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VitalAvailableViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.placeholder_vitals_available,
                parent, false)
        context = parent.context

        return VitalAvailableViewHolder(view)
    }

    override fun onBindViewHolder(holder: VitalAvailableViewHolder, position: Int) {

        val item = items[position]
        holder.updateDevices(item)

        holder.relativeLayout.setOnClickListener {
            val linkUrl = items[position].linkUrl
            val bundle = Bundle()
            bundle.putString(MdocConstants.REGISTER_URL, linkUrl)
            it.findNavController().navigate(R.id.fragmentRegisterDevice, bundle)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class VitalAvailableViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var imageView = itemView.findViewById<View>(R.id.imageMyDevice) as ImageView
    var deviceName = itemView.findViewById<View>(R.id.txtMyDevice) as TextView
    var relativeLayout = itemView.findViewById<View>(R.id.myDeviceRl) as LinearLayout

    fun updateDevices(devices: MyDevices) {
        val uri = devices.imgUri
        val resources = imageView.resources.getIdentifier(uri, "drawable", imageView.context.packageName)
        imageView.setImageResource(resources)
        deviceName.text = devices.deviceName
    }
}
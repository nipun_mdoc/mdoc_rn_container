package de.mdoc.network.response;

import de.mdoc.pojo.VoteData;

/**
 * Created by ema on 10/19/17.
 */

public class VoteResponse extends MdocResponse {

    VoteData data;

    public VoteData getData() {
        return data;
    }

    public void setData(VoteData data) {
        this.data = data;
    }
}

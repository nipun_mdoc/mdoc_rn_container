package de.mdoc.modules.digital_signature

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.pojo.digitalsignature.FormMetadataDTO
import de.mdoc.pojo.digitalsignature.SignatureDocumentInfoData
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.InputStream

/**
 * Created by Trenser on 30/12/20.
 */

class DocumentReviewViewModel(private val mDocService: IMdocService, docID: String?) : ViewModel() {

    var isLoading = MutableLiveData(false);
    var signatureDocumentInfoData = MutableLiveData<SignatureDocumentInfoData>()
    var formMetadataDTOList: MutableLiveData<List<FormMetadataDTO>> =
        MutableLiveData(listOf())

    fun setLoading( loading : Boolean){
        isLoading.value = loading
    }

    fun setFormMetaDataById ( id : String) {
        formMetadataDTOList.value?.find { it.id == id }?.checked = !formMetadataDTOList.value?.find { it.id == id }?.checked!!
    }

    fun getFormMetadataDTO(): MutableLiveData<List<FormMetadataDTO>> {
        return formMetadataDTOList
    }

    fun getDocumentData(id: String, onSuccess: (InputStream?) -> Unit, onError: (Throwable?) -> Unit ) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val call = mDocService.getSignaturePatientDocument(id)
                call.enqueue(object : Callback<ResponseBody> {
                    override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
                        onError(t)
                        isLoading.value = false
                    }

                    override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                        onSuccess(response?.body()?.byteStream())
                        isLoading.value = false
                    }
                })
            }catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun getDocumentMetaData(id: String,onSuccess: () -> Unit, onError: (Throwable?) -> Unit ){
        viewModelScope.launch {
            try {
                isLoading.value = true
                signatureDocumentInfoData.value = mDocService.getSignaturePatientDocumentInfo(id).data
                formMetadataDTOList.value = signatureDocumentInfoData.value?.formMetadataDTOList
                onSuccess()
                isLoading.value = false
            }catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun postSignInApi(onSuccess: (String?) -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                signatureDocumentInfoData.value?.formMetadataDTOList = formMetadataDTOList.value!!
                val data = mDocService.postSignaturePatientSign(signatureDocumentInfoData.value!!).data
                onSuccess(data)
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }
}
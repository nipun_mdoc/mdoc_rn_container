package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by AdisMulabdic on 10/6/17.
 */

public class Chat {

    @SerializedName("answer")
    @Expose
    private String answer;

    @SerializedName("plain_answer")
    @Expose
    private String plainAnswer;

    @SerializedName("options")
    @Expose
    private ArrayList<ChatOption> options = null;

    @SerializedName("reply_suggestions")
    @Expose
    private String replySuggestions;

    @SerializedName("reply_options")
    @Expose
    private String replyOptions;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("action")
    @Expose
    private String action;

    private boolean isMine = false;

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getPlainAnswer() {
        return plainAnswer;
    }

    public void setPlainAnswer(String plainAnswer) {
        this.plainAnswer = plainAnswer;
    }

    public ArrayList<ChatOption> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<ChatOption> options) {
        this.options = options;
    }

    public String getReplySuggestions() {
        return replySuggestions;
    }

    public void setReplySuggestions(String replySuggestions) {
        this.replySuggestions = replySuggestions;
    }

    public String getReplyOptions() {
        return replyOptions;
    }

    public void setReplyOptions(String replyOptions) {
        this.replyOptions = replyOptions;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isMine() {
        return isMine;
    }

    public void setMine(boolean mine) {
        isMine = mine;
    }
}

class ChatOption {

    @SerializedName("number")
    @Expose
    private int number;
    @SerializedName("desc")
    @Expose
    private String desc;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}

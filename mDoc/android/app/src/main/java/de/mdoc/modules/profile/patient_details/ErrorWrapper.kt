package de.mdoc.modules.profile.patient_details

data class ErrorWrapper(var hasError: Boolean=false, var message:String?="", var code:String?="") {

}

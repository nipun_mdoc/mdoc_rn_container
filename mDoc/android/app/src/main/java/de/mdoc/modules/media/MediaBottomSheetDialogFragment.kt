package de.mdoc.modules.media

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.media.MediaStatisticsData
import de.mdoc.modules.media.media_adapters.MediaCategoriesAdapter
import de.mdoc.modules.media.media_view_model.MediaStatisticsViewModel
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_dialog_media.*

class MediaBottomSheetDialogFragment: BottomSheetDialogFragment() {

    private lateinit var categoryAdapter: MediaCategoriesAdapter
    var category: ArrayList<MediaStatisticsData> = arrayListOf()
    private val mediaStatisticsViewModel by viewModel {
        MediaStatisticsViewModel(
                RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_dialog_media, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity: MdocActivity = activity as MdocActivity

        categoryAdapter = MediaCategoriesAdapter(activity, category)
        rv_category?.layoutManager = LinearLayoutManager(activity)
        rv_category?.adapter = categoryAdapter
        img_close.setOnClickListener {
            dismiss()
        }

        mediaStatisticsViewModel.mediaStatistics.observe(viewLifecycleOwner, Observer { mediaStatResponse ->
            if (mediaStatResponse != null) {
                category.clear()
                category.addAll(mediaStatResponse)
                categoryAdapter.notifyDataSetChanged()
            }
        })
    }

    companion object {
        fun newInstance(): MediaBottomSheetDialogFragment {
            return MediaBottomSheetDialogFragment()
        }
    }
}
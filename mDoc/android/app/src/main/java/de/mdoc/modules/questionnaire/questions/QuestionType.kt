package de.mdoc.modules.questionnaire.questions

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.questionnaire.answer_validation.AbstractValidator
import de.mdoc.modules.questionnaire.answer_validation.MandatoryValidator
import de.mdoc.modules.questionnaire.answer_validation.TextAnswerValidator
import de.mdoc.modules.questionnaire.ext.isMandatory
import de.mdoc.pojo.ExtendedQuestion

abstract class QuestionType {

    var isNewAnswer = false
    val answers = arrayListOf<String>()
    val questionIsAnswered: MutableLiveData<Boolean> = MutableLiveData(false)
    var selection = arrayListOf<String>()
    var validator: AbstractValidator? = null
    var mandatoryValidator: AbstractValidator? = null

    fun fillQuestion(parent: ViewGroup, question: ExtendedQuestion) {
        val view = onCreateQuestionView(parent, question)
        parent.addView(view)
        val title = view.findViewById<TextView>(R.id.questionTv)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)

        if (question.isMandatory() && (question.pollQuestionType == MdocConstants.INPUT || question.pollQuestionType == MdocConstants.TEXT)) {
            mandatory.visibility = View.VISIBLE
            mandatoryValidator = MandatoryValidator(view, question.pollQuestionExternalType)
            if (question.pollQuestionType == MdocConstants.INPUT) {
                validator = TextAnswerValidator(view, question.pollQuestionExternalType)
            }
        } else {
            if (question.pollQuestionType == MdocConstants.INPUT) {
                validator = TextAnswerValidator(view, question.pollQuestionExternalType)
            }
        }

        if (title != null) {
            title.text = "${question.ordinal} ${question.question}"
        }
        val description = view.findViewById<TextView>(R.id.questDesc)

        if (question.description.isNullOrEmpty()) {
            description?.visibility = View.GONE
        }
        else {
            description?.visibility = View.VISIBLE
            description?.text = question.description
        }
    }

    abstract fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View

    open fun onSaveQuestion(questionView: View, question: ExtendedQuestion) {
        // implement by subclasses if needed
    }

    fun mandatoryCheck(mandatory: View?, warning: View?, question: ExtendedQuestion){
        if (question.required != null && question.required){
            mandatory?.visibility = View.VISIBLE
            warning?.visibility = View.VISIBLE
        }
    }

    fun showWarning(view: View, ans: ArrayList<String>){
        if (ans.isEmpty()){
            view.visibility = View.VISIBLE
        } else {
            view.visibility = View.GONE
        }
    }
}
package de.mdoc.modules.files.utils

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Environment
import android.webkit.MimeTypeMap
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.pojo.FileListItem
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import java.io.File
import java.util.*

class FileDownloadManager(private val context: Context?) {

    private var downloadID: Long = 0

    fun downloadViaManager(url: String, item: FileListItem) {
        val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString() + File.separator + item.name.replace(
                        "\\s".toRegex(),
                        ""
                ).replace(":", "")
        )

        val request = DownloadManager.Request(Uri.parse(url))
                .setTitle(item.name)// Title of the Download Notification
                .setDescription("Downloading")// Description of the Download Notification
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)// Visibility of the download Notification
                .setDestinationUri(Uri.fromFile(file)) // Uri of the destination file
                .setVisibleInDownloadsUi(true)
                .setMimeType(getMimeFromFileName(item.name))
                .addRequestHeader(MdocConstants.AUTHORIZATION, MdocAppHelper.getInstance().accessToken)
                .addRequestHeader(MdocConstants.DELEGATED_CASE, MdocAppHelper.getInstance().caseId)
                .addRequestHeader(MdocConstants.ACCEPT_LANGUAGE, Locale.getDefault().language)
                .setAllowedOverMetered(true)// Set if download is allowed on Mobile network
                .setAllowedOverRoaming(true)// Set if download is allowed on roaming network

        val downloadManager = context?.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        downloadID = downloadManager.enqueue(request)// enqueue puts the download request in the queue.
    }

    private fun getMimeFromFileName(fileName: String): String? {
        val map = MimeTypeMap.getSingleton()
        val ext = MimeTypeMap.getFileExtensionFromUrl(
                fileName.replace("\\s".toRegex(), "").replace(
                        ":",
                        ""
                )
        )
        return map.getMimeTypeFromExtension(ext)
    }

    private val onDownloadCompleted = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            if (context != null) {
                //Fetching the download id received with the broadcast
                val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)

                //Checking if the received broadcast is for our enqueued download by matching download id
                if (downloadID == id) {
                    MdocUtil.showToastLong(context, context.getString(R.string.file_dowloaded_succ))
                }
            }
        }
    }

    fun unregisterReceiver() {
        if (context != null) {
            LocalBroadcastManager.getInstance(context).unregisterReceiver(onDownloadCompleted)
        }
    }

    fun registerReceiver() {
        context?.registerReceiver(onDownloadCompleted, IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE))
    }
}
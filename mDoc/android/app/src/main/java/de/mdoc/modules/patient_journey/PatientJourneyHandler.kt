package de.mdoc.modules.patient_journey

import android.content.Context
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.patient_journey.data.CarePlan
import de.mdoc.modules.patient_journey.data.Period
import org.joda.time.format.DateTimeFormat
import java.util.*

class PatientJourneyHandler {

    fun setDate(context: Context, time: Period?): String {
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
        val start = context.resources.getString(R.string.pat_journey_from) + " " + formatter.print(time?.start ?: 0)
        val end = context.resources.getString(R.string.pat_journey_to) + " " + formatter.print(time?.end ?: 0)
        return "$start $end"
    }

    fun setDateWithHour(context: Context, time: Long?): String {
        val formatter = DateTimeFormat.forPattern("dd. MMMM yyyy - HH:mm")
        val suffix = context.resources.getString(R.string.pat_journey_hour)

        return "${time?.let {
            formatter.print(it)
                .toUpperCase(Locale.getDefault())
        }} $suffix"
    }

    fun openJourneyDetails(context: Context, item: CarePlan?, fromDashboard: Boolean) {
        item?.let {
            val action =
                    MainNavDirections.globalActionToJourneyDetails(item.id, item.description, item.title, fromDashboard)
            (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                .navigate(action)
        } ?: run {
            (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                .navigate(R.id.navigation_patient_journey)
        }
    }

    fun setSpecialty(item: CarePlan?): String {
        return item?.codingDisplayBySystem(QUERY) ?: ""
    }

    companion object {
        const val QUERY = "http://hl7.org/fhir/ValueSet/appointment-specialty"
    }
}
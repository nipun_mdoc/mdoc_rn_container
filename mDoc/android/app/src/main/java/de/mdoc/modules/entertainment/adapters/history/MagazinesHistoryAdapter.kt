package de.mdoc.modules.entertainment.adapters.history

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaReadingHistoryResponseDto_
import de.mdoc.pojo.entertainment.*
import kotlinx.android.synthetic.main.entertainment_magazines_child_item.view.imgMagazineCover
import kotlinx.android.synthetic.main.entertainment_magazines_child_item.view.magazinesEmptyView
import kotlinx.android.synthetic.main.entertainment_magazines_child_item.view.progress
import kotlinx.android.synthetic.main.entertainment_magazines_empty_view.view.*
import kotlinx.android.synthetic.main.entertainment_magazines_history_item.view.*

class MagazinesHistoryAdapter(private val data: ResponseSearchResultsMediaReadingHistoryResponseDto_?)
    : RecyclerView.Adapter<MagazinesHistoryAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.entertainment_magazines_history_item, parent, false))
    }

    override fun getItemCount(): Int {
        return when (data) {
            null -> 0
            else -> data.data?.list?.size?:0
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data?.data?.list?.get(position))
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val imgMagazineCover: ImageView = itemView.imgMagazineCover
        private val txtMagazinePage: TextView = itemView.txtMagazinePage
        private val emptyView: View = itemView.magazinesEmptyView
        private val emptyViewTitle: View = itemView.magazinesEmptyView.txtContentNotAvailable
        private val progress: ProgressBar = itemView.progress

        private var item: MediaReadingHistoryResponseDto? = null

        init {
            emptyViewTitle.visibility = View.GONE
            imgMagazineCover.setOnClickListener(this)
        }

        fun bind(item: MediaReadingHistoryResponseDto?) {
            this.item = item
            val placeholder = ColorDrawable(ContextCompat.getColor(imgMagazineCover.context, R.color.colorGray97))
            val options = RequestOptions().placeholder(placeholder)

            var url:String? = null
            if (!item?.media?.thumbails.isNullOrEmpty()) {
                url = item?.media?.thumbails?.get(0)?.content?.url
            }

            emptyView.visibility = View.GONE
            progress.visibility = View.VISIBLE

            Glide.with(imgMagazineCover.context)
                    .setDefaultRequestOptions(options)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            emptyView.visibility = View.VISIBLE
                            progress.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imgMagazineCover)


            val pageNumber = item?.pageNumber?.plus(1)
            val pageRes = txtMagazinePage.context.getString(R.string.entertainment_magazines_page)
            txtMagazinePage.text ="$pageRes $pageNumber"
        }

        override fun onClick(view: View?) {
            val pageNumber = item?.pageNumber?:0
            val bundle = Bundle()
            bundle.putString(MdocConstants.MAGAZINE, Gson().toJson(item?.getMediaResponseDto()))
            bundle.putInt(MdocConstants.MAGAZINE_PAGE_NUMBER, pageNumber)
            view?.findNavController()?.navigate(R.id.action_entertainmentOverview_to_entertainmentMagazine, bundle)
        }
    }
}
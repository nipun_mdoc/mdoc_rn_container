package de.mdoc.modules.external_services.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.external_services.data.ExternalServiceItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class ExternalServicesViewModel(private val mDocService: IMdocService): ViewModel() {

    var isLoading = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val externalServices: MutableLiveData<List<ExternalServiceItem>> = MutableLiveData(emptyList())

    init {
        getExternalServices()
    }

    private fun getExternalServices() {
        viewModelScope.launch {
            try {
                isShowNoData.value = false
                isShowContent.value = false
                isLoading.value = true
                isShowLoadingError.value = false

                externalServices.value = mDocService.getExternalServices().data?.list

                isLoading.value = false
                if (externalServices.value?.size == 0) {
                    isShowContent.value = false
                }
                else {
                    isShowContent.value = true
                    isShowNoData.value = false
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowNoData.value = false
                isShowLoadingError.value = true
            }
        }
    }
}
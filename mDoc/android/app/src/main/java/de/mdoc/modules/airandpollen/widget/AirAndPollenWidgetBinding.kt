package de.mdoc.modules.airandpollen.widget

import android.content.res.ColorStateList
import android.view.View
import android.widget.ImageView
import androidx.core.widget.ImageViewCompat
import androidx.lifecycle.LifecycleOwner
import de.mdoc.R
import de.mdoc.modules.airandpollen.data.PollenLevel
import de.mdoc.viewmodel.bindText
import de.mdoc.viewmodel.bindVisibleGone
import kotlinx.android.synthetic.main.dashboard_widget_air_and_pollen.view.*
import kotlinx.android.synthetic.main.dashboard_widget_air_and_pollen_item.view.*
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

class AirAndPollenWidgetBinding(
    private val lifecycleOwner: LifecycleOwner,
    private val view: View,
    private val viewModel: AirAndPollenWidgetViewModel,
    private val callback: Callback
) : LifecycleOwner by lifecycleOwner {

    interface Callback {

        fun onOpenPollenModule()

    }

    private val FORMAT = DateTimeFormat.forPattern("dd.MM.YYYY")
    private val pollenInfoViews = arrayOf(view.pollenInfo1, view.pollenInfo2, view.pollenInfo3)

    init {
        viewModel.loadData()
        val date = FORMAT.print(LocalDate.now())
        view.todayDate.text = "${view.context.getString(R.string.today_date)}, $date"
        bindVisibleGone(view.pollenLoadProgress, viewModel.isInProgress)
        bindText(view.regionName, viewModel.selectedRegionName)
        bindVisibleGone(view.pollenRegion, viewModel.isSelectedRegionVisible)
        bindVisibleGone(view.pollenNoRegion, viewModel.isNoRegionSelectedMessageVisible)
        bindVisibleGone(view.pollenInfo, viewModel.isPollenInfoVisible)
        bindVisibleGone(view.pollenNoData, viewModel.isLoadingErrorVisible)
        bindVisibleGone(view.pollenRegionDivider, viewModel.isBottomSectionVisible)
        bindVisibleGone(view.pollenContent, viewModel.isBottomSectionVisible)
        view.txtPollenWidget.isSelected = true
        viewModel.pollenData.observe(this, ::fillPollenData)
        view.setOnClickListener {
            callback.onOpenPollenModule()
        }
    }

    private fun fillPollenData(data: PollenWidgetData?) {
        val grayColor = PollenLevel.None.getColor(view.context)
        if (data != null) {
            val viewCount = pollenInfoViews.size
            val list = data.getItems(view.context, viewCount)
            list.forEachIndexed { index, item ->
                val itemView = pollenInfoViews[index]
                itemView.pollenWidgetTypeName.text = item.type.getName(view.context)
                val fillColor = item.level.getColor(view.context)
                (0 until itemView.levels.childCount).forEach {
                    val image = itemView.levels.getChildAt(it) as ImageView
                    val isFill = it + 1 <= item.amount
                    val color = if (isFill) fillColor else grayColor
                    ImageViewCompat.setImageTintList(image, ColorStateList.valueOf(color))
                }
                itemView.pollenWidgetTypeLevelName.text = item.level.getName(view.context)
            }
        }
    }

}
package de.mdoc.modules.files.adapters.viewholders

import android.content.Context
import android.view.Gravity
import android.view.Menu
import android.view.View
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import butterknife.ButterKnife
import de.mdoc.R
import de.mdoc.constants.FILE_PROCESSING_STATE
import de.mdoc.constants.FILE_READY_STATE
import de.mdoc.constants.FILE_REJECTED_STATE
import de.mdoc.constants.FILE_UPLOADED_STATE
import de.mdoc.modules.files.FilesOverviewFragmentDirections
import de.mdoc.modules.files.adapters.FilesAdapterCallback
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FileListItem
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.file_list_item.view.*

interface FileItemViewHolderCallback {
    fun onClickMore(view: View, item: FileListItem, position: Int, holder: FileItemViewHolder)
}

class FileItemViewHolder(
        itemView: View,
        private val callback: FileItemViewHolderCallback,
        var context: Context
) : BaseFileItemViewHolder(itemView) {

    private val iconIv = itemView.iconIv
    private val fileNameTv = itemView.fileNameTv
    private val dateTv = itemView.dateTv
    private val moreIv = itemView.moreIv

    private val fileTitleTv = itemView.fileTitleTv
    private val categoryTv = itemView.categoryTv
    private val rootRl = itemView.rootRl
    private val updateTime = itemView.tvUpdate
    private val lastUpdatedLl = itemView.lastUpdatedLl
    private val pendingLl = itemView.pendingLl
    private val rejectedLl = itemView.rejectedLl

    private val tvPending = itemView.tvPending
    private val tvRejected = itemView.tvRejected
    private val cardView = itemView.cardView
    private val fileItemDivider = itemView.fileItemDivider
    private val tvRejectedLabel = itemView.tvRejectedLabel
    private val rejectedDetailsLl = itemView.rejectedDetailsLl
    private val infoDescription = itemView.info
    private val receivedInfo = itemView.receiveTxt
    private val clinicInfo = itemView.clinicNameText
    private val infoIcon = itemView.infoIcon
    private val sharedTv = itemView.sharedTv

    init {
        ButterKnife.bind(this, itemView)
    }

    fun update(item: FileListItem, categories: List<CodingItem>) {
        fillData(item, categories)


        when(item.state){
            FILE_REJECTED_STATE -> setRejectedLayout(item, categories)
            FILE_READY_STATE -> setLastUpdatedLayout(item, categories)
            FILE_UPLOADED_STATE -> setPendingLayout(item)
            FILE_PROCESSING_STATE -> setPendingLayout(item)
        }
    }


    fun updateRejected(item: FileListItem, categories: List<CodingItem>) {

        fillData(item, categories)

        rejectedDetailsLl.visibility = View.VISIBLE
        fileItemDivider.visibility = View.INVISIBLE
        cardView.setCardBackgroundColor(ContextCompat.getColor(context, R.color.color_fbc4ca))
        tvRejectedLabel.text = context.resources.getString(R.string.rejected_file_header)
        moreIv.visibility = View.INVISIBLE

    }

    private fun fillData(item: FileListItem, categories: List<CodingItem>) {
        fileNameTv.text = item.name
        fileTitleTv.text = item.description

        categoryTv.text = categories.firstOrNull {
            item.category == it.code
        }?.display ?: "-"

        dateTv.text = MdocUtil.getTimeFromMs(
                item.cts,
                "dd.MM.yyyy"
        )

        iconIv.setImageResource(item.type.imageRes)
        clinicInfo?.text = context.resources.getString(R.string.clinic_name, MdocAppHelper.getInstance().clinicName)
        if(!item.userInfoList.isNullOrEmpty() && item.userInfoList.size > 0){
            sharedTv.visibility = View.VISIBLE
            var allSharedBy = "";
            for (el in item.userInfoList){
                    allSharedBy = allSharedBy + el.sharedBy.firstName
                }
            sharedTv.text = context.resources.getString(R.string.shared_by) + item.userInfoList.get(0).sharedBy.firstName
        }

        if(item.shareByVisibility){
            sharedTv.visibility = View.VISIBLE
        }else{
            sharedTv.visibility = View.GONE
        }

        when {
            item.sharedWithClinic -> {
                infoDescription.visibility = View.VISIBLE
                showUserShare()
            }
            item.sharedWithPatient -> {
                infoDescription.visibility = View.VISIBLE
                showClinicShare()
            }
            item.sharedByClinic -> {
                infoDescription.visibility = View.VISIBLE
                showClinicShare()
            }
            else -> {
                infoDescription.visibility = View.GONE
            }
        }

        moreIv.setOnClickListener {
            callback.onClickMore(moreIv, item, adapterPosition, this)
        }
    }

    fun setRejectedLayout(item: FileListItem, categories: List<CodingItem>){
        pendingLl.visibility = View.GONE
        lastUpdatedLl.visibility = View.GONE
        rejectedLl.visibility = View.VISIBLE

        tvRejected.text = context.resources.getString(R.string.rejected_file)
        moreIv.visibility = View.INVISIBLE
        rootRl.setOnClickListener {
            val action = FilesOverviewFragmentDirections.globalToFragmentFilesDetails(categories.toTypedArray(), item.id)
            it.findNavController().navigate(action)
        }
    }

    fun setLastUpdatedLayout(item: FileListItem, categories: List<CodingItem>){
        rejectedLl.visibility = View.GONE
        pendingLl.visibility = View.GONE
        lastUpdatedLl.visibility = View.VISIBLE

        moreIv.visibility = View.VISIBLE
        updateTime.text = context.resources.getString(R.string.last_updated_at, MdocUtil.getTimeFromMs(item.uts, "dd.MM.yyyy HH:mm"))

        rootRl.setOnClickListener {
            val action = FilesOverviewFragmentDirections.globalToFragmentFilesDetails(categories.toTypedArray(), item.id)
            it.findNavController().navigate(action)
        }
    }

    fun setPendingLayout(item: FileListItem){
        rejectedLl.visibility = View.GONE
        lastUpdatedLl.visibility = View.GONE
        pendingLl.visibility = View.VISIBLE

        tvPending.text = context.resources.getString(R.string.processing_file)
        moreIv.visibility = View.INVISIBLE
        rootRl.setOnClickListener(null)
    }

    fun showPopup(v: View, fileItem: FileListItem, callback: FilesAdapterCallback) {
        val popup = PopupMenu(v.context, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.actions, popup.menu)
        checkFileItems(popup.menu, fileItem)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.share -> callback.onShareFileRequest(fileItem)
                R.id.edit -> callback.onEditFileRequest(fileItem)
                R.id.dowload -> callback.onDownloadFileRequest(fileItem)
                R.id.delete -> callback.onDeleteFileRequest(fileItem)
            }
            false
        }

        val menuHelper = MenuPopupHelper(v.context, popup.menu as MenuBuilder, v)
        menuHelper.setForceShowIcon(true)
        menuHelper.gravity = Gravity.START
        menuHelper.show()
    }

    private fun checkFileItems(menuItem: Menu, file: FileListItem) {
        menuItem.findItem(R.id.share).isVisible = file.sharable
        menuItem.findItem(R.id.edit).isVisible = file.updatable
        menuItem.findItem(R.id.dowload).isVisible = true
        menuItem.findItem(R.id.delete).isVisible = file.deletable
    }

    private fun showUserShare(){
        receivedInfo.text = context.resources.getString(R.string.sent_to)
        infoIcon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_sent_to))
        receivedInfo.setTextColor(context.resources.getColor(R.color.sent_to_color))
    }

    private fun showClinicShare(){
        receivedInfo.text = context.resources.getString(R.string.received_from)
        infoIcon.setImageDrawable(context.resources.getDrawable(R.drawable.ic_received))
        receivedInfo.setTextColor(context.resources.getColor(R.color.received_color))
    }
}

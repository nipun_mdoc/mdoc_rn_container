package de.mdoc.interfaces;

import android.view.View;

/**
 * Created by ema on 12/25/17.
 */

public interface RecyclerViewClickListener {
    void onClick(View view, int position);
}

package de.mdoc.pojo;

/**
 * Created by AdisMulabdic on 8/31/17.
 */

public class MyDevices {

    private String deviceName, imgUri, linkUrl;
    private String time;

    public String getDeviceName() {
        return deviceName;
    }

    public String getImgUri() {
        return  imgUri;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public MyDevices(String deviceName, String imgUri, String linkUrl) {
        this.deviceName = deviceName;
        this.imgUri = imgUri;
        this.linkUrl = linkUrl;
    }
    public MyDevices(String deviceName, String imgUri, String linkUrl,String time) {
        this.deviceName = deviceName;
        this.imgUri = imgUri;
        this.linkUrl = linkUrl;
        this.time=time;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof MyDevices))
            return false;
        if (obj == this)
            return true;
        return this.getDeviceName() == ((MyDevices) obj).getDeviceName();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}

package de.mdoc.modules.patient_journey.data

data class CarePlanDetailsResponse(
        val code: String? = "",
        val data: CarePlan? = CarePlan(),
        val message: String? = "",
        val timestamp: Long? = 0
                                  )
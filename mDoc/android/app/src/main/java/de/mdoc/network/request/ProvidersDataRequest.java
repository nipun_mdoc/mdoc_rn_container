package de.mdoc.network.request;

public class ProvidersDataRequest {

    private String gender;
    private String born;

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }
}

package de.mdoc.network.request;

public class CodingRequest {

    private String systemId;

    public CodingRequest(String systemId) {
        this.systemId = systemId;
    }

    public String getSystemId() {
        return systemId;
    }

    public void setSystemId(String systemId) {
        this.systemId = systemId;
    }
}

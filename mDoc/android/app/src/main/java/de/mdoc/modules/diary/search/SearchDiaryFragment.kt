package de.mdoc.modules.diary.search

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding.widget.RxSearchView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentCommonSearchBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.diary.DiaryAdapter
import de.mdoc.modules.diary.DiaryAdapterCallback
import de.mdoc.modules.diary.DiaryBottomEditDialogFragment
import de.mdoc.modules.diary.data.RxDiaryRepository
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import de.mdoc.network.RestClient
import de.mdoc.pojo.DiaryList
import de.mdoc.util.hideActionBar
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_common_search.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SearchDiaryFragment : NewBaseFragment(), View.OnClickListener, DiaryAdapterCallback {

    override val navigationItem: NavigationItem get() = NavigationItem.SearchClinic

    private val viewModel by viewModel {
        SearchDiaryViewModel(repository = RxDiaryRepository(RestClient.getService()))
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentCommonSearchBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_common_search, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.searchInputHint = getString(R.string.search_entries)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideActionBar()
        btnCloseSearch.setOnClickListener(this)
        rvSearchItems.visibility = View.VISIBLE
        searchEdt.isIconified = false

        viewModel.searchResponse.observe(viewLifecycleOwner, Observer {
            rvSearchItems.apply {
                if (it != null) {
                    layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    adapter = DiaryAdapter(it, context as MdocActivity, true, this@SearchDiaryFragment)
                }
            }
        })

        viewModel.shouldShowDeleteMsg.observe(viewLifecycleOwner){
            linerDeleteMsg.visibility = if(it == true )View.VISIBLE else View.GONE
        }

        RxSearchView.queryTextChanges(searchEdt)
                .debounce(
                        MdocConstants.SEARCH_WAIT_TIME.toLong(),
                        TimeUnit.MILLISECONDS,
                        AndroidSchedulers.mainThread()
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewModel.requestSearch(it.toString())
                }
    }

    override fun onClick(button: View?) {
        when (button) {
            btnCloseSearch -> {
                findNavController().popBackStack()
            }
        }
    }

    override fun onDeleteFileRequest(item: DiaryList) {
        deleteDialog(item)
    }

    override fun onEditFileRequest(item: DiaryList) {
        val pollId = if(item.diaryConfig?.pollId != null) item.diaryConfig?.pollId else ""
        val questionnaireAdditionalFields = QuestionnaireAdditionalFields(
                showFreeText = item.diaryConfig?.isFreeTextEntryAllowed!!,
                allowSharing = item.showToDoctor,
                autoShare = item.diaryConfig?.isAutoShare!!,
                isDiary = true,
                shouldAssignQuestionnaire = false,
                diaryConfigId = item.diaryConfig?.id,
                pollId = pollId,
                pollAssignId = pollId,
                diaryList = item)

        if(!item?.pollVotingActionId.isNullOrEmpty()) {
            val bottomSheetDialogFragment = DiaryBottomEditDialogFragment.newInstance(item, true)
            bottomSheetDialogFragment.show((activity as MdocActivity).supportFragmentManager, bottomSheetDialogFragment.tag)
        }else{
            val action = SearchDiaryFragmentDirections.actionSearchDiaryFragmentToDiaryEntryFragment(questionnaireAdditionalFields)
            activity?.findNavController(R.id.navigation_host_fragment)?.navigate(action)
        }
    }

    override fun onViewClickListner(list: DiaryList, isComingFromSearch: Boolean) {

    }

    fun deleteDialog(item: DiaryList) {

        var dialog = Dialog(context as MdocActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_popup_dilog)


        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var done = dialog.findViewById<TextView>(R.id.done)

        done!!.setOnClickListener {
            viewModel.requestDelete(item)
            dialog.dismiss()
        }

        cancel!!.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }
}

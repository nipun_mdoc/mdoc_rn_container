package de.mdoc.network.request;

import java.util.ArrayList;

public class NotificationSeenRequest {

    private boolean seen;
    private ArrayList<String> notificationIds;

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public ArrayList<String> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(ArrayList<String> notificationIds) {
        this.notificationIds = notificationIds;
    }
}

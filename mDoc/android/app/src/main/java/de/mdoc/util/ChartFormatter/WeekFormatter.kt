package de.mdoc.util.ChartFormatter

import android.content.Context

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import de.mdoc.R

class WeekFormatter(val context: Context) : IAxisValueFormatter {

    override fun getFormattedValue(value: Float, axis: AxisBase): String {
        val resultInt = Math.round(value)
        val days = arrayOf(context.getString(R.string.mon),
                context.getString(R.string.tue),
                context.getString(R.string.wed),
                context.getString(R.string.thu),
                context.getString(R.string.fri),
                context.getString(R.string.sat),
                context.getString(R.string.sun))
        return if (resultInt < days.size && resultInt > -1) {
            days[resultInt]
        } else {
            resultInt.toString() + ""
        }
    }
}
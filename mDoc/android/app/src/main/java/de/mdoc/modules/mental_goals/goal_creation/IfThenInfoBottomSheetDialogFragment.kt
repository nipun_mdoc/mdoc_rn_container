package de.mdoc.modules.mental_goals.goal_creation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import kotlinx.android.synthetic.main.fragment_if_then_bottom_sheet.*

class IfThenInfoBottomSheetDialogFragment: BottomSheetDialogFragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_if_then_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        img_close.setOnClickListener {
            dismiss()
        }
    }

    companion object {
        fun newInstance(): IfThenInfoBottomSheetDialogFragment {
            return IfThenInfoBottomSheetDialogFragment()
        }
    }
}
package de.mdoc.modules.vitals.data

data class UnitPersistenceRequest(var username: String?=null,
                                  var firstName: String?=null,
                                  var lastName: String?=null,
                                  var email:String?=null,
                                  var lastSeenUnitByCode: Map<String, String>?=null)
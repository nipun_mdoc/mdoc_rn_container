package de.mdoc.modules.questionnaire.finish

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.activities.navigation.setCurrentNavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.FinishQuestionnaireResponse
import de.mdoc.util.setActionTitle
import kotlinx.android.synthetic.main.send_questionnaire_fragment.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SendQuestionnaireFragment: MdocFragment() {

    var activity: MdocActivity? = null
    private var consent: String = ""
    val args: SendQuestionnaireFragmentArgs by navArgs()
    lateinit var questionnaireAdditionalFields: QuestionnaireAdditionalFields
    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireTitle

    override fun init(savedInstanceState: Bundle?) {}

    override fun setResourceId(): Int = R.layout.send_questionnaire_fragment

    @SuppressLint("StringFormatMatches")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionnaireAdditionalFields = args.questionnaireAdditionalFields ?: QuestionnaireAdditionalFields()

        activity = mdocActivity

        if (questionnaireAdditionalFields.isSendToKis) {
            view.questionnaireCheck.visibility = View.VISIBLE
            setActionTitle(requireActivity().resources.getString(R.string.questionnaire_prompt_title_kis_enabled))
        }
        else {
            view.questionnaireCheck.visibility = View.INVISIBLE
            setActionTitle(requireActivity().resources.getString(R.string.questionnaire_prompt_title_kis_disabled))
            view.tvTitleOne.text = activity?.resources?.getString(R.string.questionnaire_prompt_title_3_kis_disabled)
        }

        scoringViewSetup(view)

        view.tvQuestionnaireQuestion?.text = requireActivity().resources.getString(
                R.string.questionnaire_prompt_title_1,
                questionnaireAdditionalFields.answerDetailsCompleted,
                questionnaireAdditionalFields.answerDetailsTotal,
                questionnaireAdditionalFields.questionnaireName)

        view.questionnaireYes.setOnClickListener {
            disableButtonsForRequest()
            checkBox(view)
            finishQuestionnaire()
        }

        view.questionnaireCheck.setOnClickListener {
            view.questionnaireYes.isEnabled = view.questionnaireCheck.isChecked
        }

        view.questionnaireBack.setOnClickListener {
            findNavController().popBackStack()
        }

        if (resources.getBoolean(R.bool.has_questionnaire_history))
            view.tvTitleTwo.visibility = View.VISIBLE
        else
            view.tvTitleTwo.visibility = View.GONE
    }

    private fun disableButtonsForRequest() {
        view?.questionnaireBack?.isEnabled = false
        view?.questionnaireYes?.isEnabled = false
    }

    private fun scoringViewSetup(view: View) {
        if (questionnaireAdditionalFields.questionnaireType.equals(MdocConstants.SCORING)) {
            view.tvTitleOne.text = requireActivity().resources.getString(
                    R.string.questionnaire_score_title_kis_enabled_and_disabled)
            view.questionnaireYes.text = requireActivity().resources.getString(R.string.questionnaire_yes_get_results)
        }
    }

    private fun checkBox(view: View) {
        consent = if (view.questionnaireCheck.isChecked) {
            "true"
        }
        else {
            "false"
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        view?.invalidate()
    }

    private fun finishQuestionnaire() {
        MdocManager.finishQuestionnaires(questionnaireAdditionalFields.pollId, consent,
                questionnaireAdditionalFields.pollVotingId.toString(), questionnaireAdditionalFields.pollAssignId,
                object:
                        Callback<FinishQuestionnaireResponse?> {
                    override fun onResponse(call: Call<FinishQuestionnaireResponse?>,
                                            response: Response<FinishQuestionnaireResponse?>) {
                        if (isAdded) {
                            if (questionnaireAdditionalFields.isDiary) {
                                val action =
                                        SendQuestionnaireFragmentDirections.actionSendQuestionnaireFragmentToDiaryEntryFragment2(
                                                questionnaireAdditionalFields)
                                findNavController().navigate(action)
                            }
                            else {
                                response.body()
                                    ?.let {
                                        val isScoringType = MdocConstants.SCORING == it.getType()
                                        val endMessage = it.getClosingMessage(it.data.pollVotingActionDetails.language)
                                        val action =
                                                SendQuestionnaireFragmentDirections.actionSendQuestionnaireFragmentToScoringResultFragment(
                                                        questionnaireAdditionalFields.pollVotingId.toString(),
                                                        questionnaireAdditionalFields.pollAssignId.toString(),
                                                        endMessage ?: "",
                                                        isScoringType,
                                                        questionnaireAdditionalFields.resultScreenTypeCode.toString())
                                        findNavController().navigate(action)
                                    }
                            }
                        }
                    }

                    override fun onFailure(call: Call<FinishQuestionnaireResponse?>, t: Throwable) {
                        if (isAdded) {
                            if (questionnaireAdditionalFields.isDiary) {
                                val action =
                                        SendQuestionnaireFragmentDirections.actionSendQuestionnaireFragmentToDiaryEntryFragment2(
                                                questionnaireAdditionalFields)
                                findNavController().navigate(action)
                            }
                            else {
                                val action =
                                        SendQuestionnaireFragmentDirections.actionSendQuestionnaireFragmentToScoringResultFragment(
                                                null, null, null, false, null)
                                findNavController().navigate(action)
                            }
                        }
                    }
                })
    }
}

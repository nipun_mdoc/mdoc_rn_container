package de.mdoc.modules.common.dialog

enum class ButtonClicked {
    POSITIVE, NEGATIVE
}
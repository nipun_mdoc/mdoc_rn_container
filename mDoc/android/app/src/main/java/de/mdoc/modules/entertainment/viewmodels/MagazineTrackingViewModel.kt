package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.mdoc.modules.entertainment.repository.MagazinesTrackingRepository
import de.mdoc.pojo.entertainment.MediaResponseDto
import de.mdoc.service.IMdocService

class MagazineTrackingViewModel (private val repository: MagazinesTrackingRepository, private val mDocService: IMdocService): ViewModel() {

    var trackPageOpen = MutableLiveData(-1)
    var isSessionOpen = MutableLiveData(false)

    fun trackSessionOpen (media: MediaResponseDto?){
        repository.trackSessionOpen(media, ::onSessionOpen)
    }

    fun trackSessionClosed (media: MediaResponseDto?) {
        repository.trackSessionClosed(media)
    }

    fun trackPageOpen (media: MediaResponseDto?, page: String) {
        repository.trackPageOpen(media, page)
    }

    fun trackPageClose (media: MediaResponseDto?, pageToClose: String, pageToOpen: Int) {
        repository.trackPageClose(media, pageToClose, pageToOpen, ::onClose)
    }

    private fun onClose (page: Int) {
        trackPageOpen.value = page
    }

    private fun onSessionOpen () {
        isSessionOpen.value = true
    }
}
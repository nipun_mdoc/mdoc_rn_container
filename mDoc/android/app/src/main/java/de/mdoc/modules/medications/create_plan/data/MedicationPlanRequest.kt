package de.mdoc.modules.medications.create_plan.data

import org.joda.time.DateTime

data class MedicationPlanRequest(
        var authoredOn: String? = null,
        var dispenseRequest: DispenseRequest? = null,
        var dosageInstruction: List<DosageInstruction?>? = null,
        var medicationId: String? = null,
        var requester: String? = null,
        var showReminder: Boolean? = null,
        var subject: String? = null
                                )

data class DispenseRequest(
        var infinite: Boolean? = true,
        var validityPeriod: ValidityPeriod? = ValidityPeriod()
                          )

data class ValidityPeriod(
        var end: Any? = DateTime.now().plusMonths(1),
        var start: Any? = DateTime.now()
                         )

data class DosageInstruction(
        val doseQuantity: Double? = null,
        val doseQuantityCode: String? = null,
        val patientInstruction: String? = null,
        val sequence: Int? = 1,
        val text: String? = null,
        val timing: Timing? = null
                            )

data class Timing(
        val event: List<String?>? = null,
        val repeat: Repeat? = null
                 )

data class Repeat(
        val dayOfWeek: List<String?>? = null,
        var period: Int? = 1,
        var periodUnit: String? = "D",
        val timeOfDay: List<TimeOfDay?>? = null
                 )

data class TimeOfDay(
        val hour: Int? = null,
        val minute: Int? = null,
        val dayOfMonth:Int?=null,
        val monthOfYear:Int?=null,
        val year:Int?=null
                    )


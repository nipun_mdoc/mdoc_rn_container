package de.mdoc.modules.files.filter

interface FileFilterChangedListener {

    fun onFileFilterChanged(filesFilter: FilesFilter)

}
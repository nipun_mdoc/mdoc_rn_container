package de.mdoc.modules.my_stay.map

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_clinic_map.*

class MyStayMapFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.MyClinic
    private var googleMap: GoogleMap? = null
    lateinit var activity: MdocActivity

    private val args: MyStayMapFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity
        return inflater.inflate(R.layout.fragment_clinic_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var street = ""
        var houseNumber = ""
        var postalCode = ""
        var city = ""

        args.clinicDetails?.address?.apply {
            street = this.street?:""
            houseNumber = this.houseNumber?:""
            postalCode = this.postalCode?:""
            city = this.city?:""
        }

        val address = if (houseNumber.isNotEmpty()) {
            "$street $houseNumber, $postalCode $city"
        }
        else {
            "$street $postalCode $city"
        }

        addressTv?.text = address


        mapIcon.setOnClickListener {
            onIconClick()
        }

        setupMap(savedInstanceState)
    }

    private fun setupMap(savedInstanceState: Bundle?) {
        mapView?.onCreate(savedInstanceState)

        mapView?.onResume()
        try {
            MapsInitializer.initialize(getActivity()?.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mapView?.getMapAsync { mMap ->
            googleMap = mMap
            // For dropping a marker at a point on the Map
            val loc = LatLng(args.clinicDetails?.latitude?.toDouble() ?: MdocConstants.LAT,
                    args.clinicDetails?.longitude?.toDouble() ?: MdocConstants.LON)
            googleMap?.addMarker(MarkerOptions().position(loc).icon(
                    BitmapDescriptorFactory.fromResource(R.drawable.location)))
            val cameraPosition = CameraPosition.Builder()
                .target(loc)
                .zoom(12f)
                .build()
            googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
        }
    }

    override fun onResume() {
        super.onResume()
        mapView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView?.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }

    private fun onIconClick() {
        val latitude = args.clinicDetails?.latitude ?: MdocConstants.LAT
        val longitude = args.clinicDetails?.longitude ?: MdocConstants.LON
        val uri = "http://maps.google.com/maps?daddr=$latitude,$longitude"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))
        intent.setPackage("com.google.android.apps.maps")
        if (intent.resolveActivity(activity.packageManager) != null) {
            startActivity(intent)
        }
        else {
            MdocUtil.showToastLong(activity, getString(R.string.google_maps_disabled))
        }
    }
}
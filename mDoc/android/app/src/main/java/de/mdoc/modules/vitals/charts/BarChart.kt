package de.mdoc.modules.vitals.charts

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import de.mdoc.R
import de.mdoc.modules.devices.data.Data
import de.mdoc.modules.devices.data.Observation
import de.mdoc.modules.devices.data.ObservationPeriodType
import de.mdoc.modules.vitals.FhirConfigurationUtil.metricLabel
import de.mdoc.util.ChartFormatter.DayFormatter
import de.mdoc.util.ChartFormatter.MonthFormatter
import de.mdoc.util.ChartFormatter.WeekFormatter
import de.mdoc.util.ChartFormatter.YearFormatter
import java.util.*
import kotlin.math.roundToInt

fun drawBarChart(context: Context,
                 periodType: ObservationPeriodType,
                 inputData: ArrayList<BarEntry>,
                 lastDayOfMonth:Int,
                 unit:String) {
    val activity=context as Activity

    val barChart = activity.findViewById<BarChart>(R.id.bar_chart)
    barChart.visibility= View.VISIBLE
    val barDataSet = arrayListOf<IBarDataSet>()

    val set1 = BarDataSet(inputData,unit)

    set1.setDrawValues(false)
    barChart.setNoDataText("")
    val maxValueInCollection=inputData.maxBy {it.y }?.y?:1000F
    val maxY=maxValueInCollection+(maxValueInCollection*(10F/100F))
    val yAxisLimitTop=((maxY / 10.0).roundToInt() *10).toFloat()

    barChart.axisRight.isEnabled = false

    barChart.description.isEnabled = false

    barChart.axisLeft.axisMinimum = 0F

    //x axis
    barChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
    barChart.xAxis.setDrawAxisLine(true)
    barChart.xAxis.setDrawGridLines(false)
    barChart.setScaleEnabled(false)
    when (periodType) {
        ObservationPeriodType.DAY -> {
            val myValueFormatter = DayFormatter()
            barChart.xAxis.setLabelCount(5, true)
            barChart.xAxis.axisMinimum = 0F
            barChart.xAxis.axisMaximum = 24F
            barChart.xAxis.valueFormatter = myValueFormatter

            barChart.axisLeft.setLabelCount(5, true)
            barChart.axisLeft.axisMaximum = yAxisLimitTop
        }
        ObservationPeriodType.WEEK -> {
            val weekFormatter = WeekFormatter(context)

            barChart.xAxis.setLabelCount(7, true)
            barChart.xAxis.axisMinimum = 0F
            barChart.xAxis.axisMaximum = 6F
            barChart.xAxis.valueFormatter = weekFormatter

            barChart.axisLeft.setLabelCount(5, true)
            barChart.axisLeft.axisMaximum = yAxisLimitTop
        }
        ObservationPeriodType.MONTH -> {
            val monthFormatter = MonthFormatter()
            barChart.xAxis.setLabelCount(7, true)
            barChart.xAxis.axisMinimum = 01F
            barChart.xAxis.axisMaximum = lastDayOfMonth.toFloat()
            barChart.xAxis.valueFormatter = monthFormatter

            barChart.axisLeft.setLabelCount(5, true)
            barChart.axisLeft.axisMaximum = yAxisLimitTop
        }
        ObservationPeriodType.YEAR -> {
            val yearFormatter = YearFormatter()
            barChart.xAxis.setLabelCount(12, true)
            barChart.xAxis.axisMinimum = 0F
            barChart.xAxis.axisMaximum = 11F
            barChart.xAxis.valueFormatter = yearFormatter

            barChart.axisLeft.setLabelCount(5, true)
            barChart.axisLeft.axisMaximum = yAxisLimitTop
        }
        else -> {}
    }

    set1.color = ContextCompat.getColor(context, R.color.blue_chart)

    barDataSet.add(set1)
    val barData = BarData(barDataSet)
    if(inputData.isEmpty()){
        inputData.add(BarEntry(0.toFloat(), 0.toFloat()))
        set1.color=ContextCompat.getColor(activity, R.color.white_transparent)
        set1.setDrawValues(false)
    }
    barChart.data = barData
    barChart.invalidate()

    if (inputData.isNullOrEmpty()) {
        inputData.clear()
        barChart.invalidate()
        barChart.clear()
    }
}

fun parseDataForBarGraph(input: List<Data>?, periodType: ObservationPeriodType): ArrayList<BarEntry> {
    var inputData= arrayListOf<Observation>()
    val output: ArrayList<BarEntry> = arrayListOf()
    val time: Calendar = Calendar.getInstance()
    time.firstDayOfWeek = Calendar.MONDAY

    if(!input.isNullOrEmpty()){
        inputData= input[0].data.list
        time.timeInMillis= inputData[0].period
    }

    var xAxisValue = 0

    for (item in inputData) {
        time.timeInMillis = item.period

        when (periodType) {

            ObservationPeriodType.DAY -> {
                xAxisValue = time.get(Calendar.HOUR_OF_DAY)
            }
            ObservationPeriodType.WEEK -> {
                xAxisValue = time.get(Calendar.DAY_OF_WEEK) - 2
                if (xAxisValue == -1) xAxisValue = 6
            }
            ObservationPeriodType.MONTH -> {
                xAxisValue = time.get(Calendar.DAY_OF_MONTH)
            }
            ObservationPeriodType.YEAR -> {
                xAxisValue = time.get(Calendar.MONTH)
            }
            else -> {
            }
        }

        val entryElement = BarEntry(xAxisValue.toFloat(), item.avg.toFloat())
        output.add(entryElement)
    }
    return output
}

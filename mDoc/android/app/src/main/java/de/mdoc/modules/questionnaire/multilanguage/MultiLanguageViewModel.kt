package de.mdoc.modules.questionnaire.multilanguage

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.network.request.CodingRequest
import de.mdoc.pojo.QuestionariesWelcomeResponse
import de.mdoc.service.IMdocService
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

class MultiLanguageViewModel(private val mDocService: IMdocService): ViewModel() {

    val response: MutableLiveData<QuestionariesWelcomeResponse> = MutableLiveData()

    fun getData (id: String) {
        viewModelScope.launch {
            try {
                coroutineScope {
                    val responseCoding = async { mDocService.getCodingKt(CodingRequest(LANGUAGES_CODING)) }
                    val responseQuestionnaire = async {mDocService.getQuestionnaireByIdSuspend(id)}
                    val responseAnswers = async {mDocService.getQuestionnaireAnswers(id)}

                    response.value = QuestionariesWelcomeResponse(responseCoding.await(), responseQuestionnaire.await(), responseAnswers.await())
                }

            } catch (e: java.lang.Exception) {
                Timber.d(e)
            }
        }
    }

    companion object {
        private const val LANGUAGES_CODING = "https://mdoc.one/hl7/fhir/questionnaire/languages"
    }
}
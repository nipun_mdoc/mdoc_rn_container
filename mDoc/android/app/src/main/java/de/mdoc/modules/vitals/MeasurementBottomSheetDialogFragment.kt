package de.mdoc.modules.vitals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.vitals.adapters.MeasurementsAdapter
import de.mdoc.storage.AppPersistence.fhirConfigurationData
import kotlinx.android.synthetic.main.fragment_add_measurement.*

class MeasurementBottomSheetDialogFragment : BottomSheetDialogFragment() {
    private lateinit var measurementsAdapter: MeasurementsAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_add_measurement, container, false)    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val activity: MdocActivity = activity as MdocActivity

        setupAdapter(activity)

        img_close.setOnClickListener {
            dismiss()
        }
    }

    private fun setupAdapter(activity: MdocActivity){
        measurementsAdapter= MeasurementsAdapter(activity,fhirConfigurationData,this)
        rv_measurements?.layoutManager = LinearLayoutManager(activity)
        rv_measurements?.adapter = measurementsAdapter
    }
}
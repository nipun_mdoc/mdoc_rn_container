package de.mdoc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.ClinicDetails;

/**
 * Created by ema on 2/27/17.
 */

public class AutocompleteClinicAdapter extends ArrayAdapter<ClinicDetails> implements Filterable {

    ArrayList<ClinicDetails> list;
    Context context;
    private int viewResourceId;

    public AutocompleteClinicAdapter(Context context, int resource, ArrayList<ClinicDetails> list) {
        super(context, resource, new ArrayList<ClinicDetails>());
        this.list = list;
        this.context = context;
        this.viewResourceId = resource;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView =  LayoutInflater.from(context).inflate(viewResourceId, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        ClinicDetails cd = getItem(position);
        holder.clinicNameTv.setText(cd.getClinicId() + "/" +cd.getName());
        return convertView;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private Filter mFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((ClinicDetails)resultValue).getName();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<ClinicDetails> suggestions = new ArrayList<ClinicDetails>();
                for (ClinicDetails customer : list) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
                    if (customer.getName().toLowerCase(Locale.getDefault()).contains(constraint.toString().toLowerCase(Locale.getDefault()))) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<ClinicDetails>) results.values);
            }

            notifyDataSetChanged();
        }
    };


    static class ViewHolder {
        @BindView(R.id.clinicNameTv)
        TextView clinicNameTv;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}

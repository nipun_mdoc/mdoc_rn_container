package de.mdoc.viewmodel.compositelist

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

interface ListItemTypeAdapter<T, Holder : ListItemViewHolder> {

    fun onCreateViewHolder(parent: ViewGroup): Holder

    fun onBind(position: Int, item: T, holder: Holder) {
        // implement if needed
    }

    fun getId(position: Int, item: T): Long {
        return RecyclerView.NO_ID
    }
}
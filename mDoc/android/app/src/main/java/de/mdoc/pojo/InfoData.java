package de.mdoc.pojo;

import java.util.HashMap;

/**
 * Created by ema on 3/9/18.
 */

public class InfoData {
    private String id;
    private String configurationType;
    private String applicationEvent;
    private ConfigurationDetailsMultiValue configurationDetailsMultiValue;

    private HashMap<String, String> configurationDetails;

    public HashMap<String, String> getConfigurationDetails() {
        return configurationDetails;
    }

    public void setConfigurationDetails(HashMap<String, String> configurationDetails) {
        this.configurationDetails = configurationDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getConfigurationType() {
        return configurationType;
    }

    public void setConfigurationType(String configurationType) {
        this.configurationType = configurationType;
    }

    public String getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(String applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public ConfigurationDetailsMultiValue getConfigurationDetailsMultiValue() {
        return configurationDetailsMultiValue;
    }

    public void setConfigurationDetailsMultiValue(ConfigurationDetailsMultiValue configurationDetailsMultiValue) {
        this.configurationDetailsMultiValue = configurationDetailsMultiValue;
    }
}

package de.mdoc.modules.devices.bf600

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.BF600
import de.mdoc.fragments.MdocFragment
import de.mdoc.interfaces.DeviceDataListener
import de.mdoc.modules.devices.bluetooth_le.BF600Wrapper
import de.mdoc.modules.devices.data.BleMessage
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.pojo.CodingItem
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.parcelableArgument
import de.mdoc.util.stringArgument
import kotlinx.android.synthetic.main.fragment_init_bf600.*
import timber.log.Timber
import java.lang.ref.WeakReference

class InitializeBf600Fragment: MdocFragment(), DeviceDataListener {
    override val navigationItem: NavigationItem = NavigationItem.Devices
    lateinit var activity: MdocActivity
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothLeScanner: BluetoothLeScanner? = null
    private var callOnce = true
    var bf600Wrapper: BF600Wrapper? = null
    val scaleScanTimeoutHandler = Handler()
    var adapterItems: ArrayList<CodingItem> = ArrayList()
    var shouldCancelConnection = true

    private val args: InitializeBf600FragmentArgs by navArgs()

    private var scanCallBack: ScanCallback? = null

    override fun setResourceId() = R.layout.fragment_init_bf600

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        val manager = activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = manager.adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed{
            bf600Wrapper?.disconnect()
            findNavController().popBackStack()
        }

        shouldCancelConnection = true
        if (bf600Wrapper != null) {
            bf600Wrapper?.setBleResponseListener(this@InitializeBf600Fragment)
            bf600Wrapper?.getUsers()
        }
        else {
            requestPermission()
        }
        user_spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, index: Int, p3: Long) {
                if (index != 0) {
                    shouldCancelConnection = false
                    scaleScanTimeoutHandler.removeCallbacksAndMessages(null)

                    val action = InitializeBf600FragmentDirections.actionToFragmentEnterPinBf600(
                            bf600Wrapper = bf600Wrapper,
                            deviceData = args.deviceData,
                            deviceId = args.deviceId,
                            userIndex = adapterItems.getOrNull(index)?.code?.toInt()?:-1)

                    findNavController().navigate(action)
                }
            }
        }

        txt_create_user?.setOnClickListener {
            shouldCancelConnection = false
            scaleScanTimeoutHandler.removeCallbacksAndMessages(null)

            val action = InitializeBf600FragmentDirections.actionToFragmentCreateNewBf600User(
                    bf600Wrapper, args.deviceData, args.deviceId
            )

            findNavController().navigate(action)

        }
        txt_cancel?.setOnClickListener {
            scaleScanTimeoutHandler.removeCallbacksAndMessages(null)
            bf600Wrapper?.disconnect()
            findNavController().popBackStack()
        }
    }

    private fun requestPermission() {
        val locationPermission = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION
                           ), ENABLE_LOCATIONS_REQUEST_ID)
        }
        else {
            checkIfBluetoothIsEnabled()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ENABLE_LOCATIONS_REQUEST_ID) {
            if (permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkIfBluetoothIsEnabled()
            }
            else {
                findNavController().popBackStack()
            }
        }
    }

    private fun checkIfBluetoothIsEnabled() {
        if (bluetoothAdapter == null || !bluetoothAdapter!!.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_ID)
        }
        else if (bluetoothAdapter!!.isEnabled) {
            startScan()
        }
        if (!activity.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(getActivity(), "BLE is not supported", Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ENABLE_BT_REQUEST_ID) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    startScan()
                } catch (e: Exception) {
                }
            }
            else {
              findNavController().popBackStack()
            }
        }
    }

    private fun startScan() {
        bluetoothLeScanner = bluetoothAdapter!!.bluetoothLeScanner

        initScanCallback(WeakReference<InitializeBf600Fragment>(this), WeakReference<DeviceDataListener>(this))

        val settings = ScanSettings.Builder()
            .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
            .build()
        bluetoothLeScanner!!.startScan(null, settings, scanCallBack)
        scaleScanTimeoutHandler.postDelayed({
            cancelScan()
        }, args.deviceData.getSyncTimeoutMs())
    }

    private fun initScanCallback (fragment: WeakReference<InitializeBf600Fragment>, bleResponseListener: WeakReference<DeviceDataListener>){
        scanCallBack = object: ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                Timber.d("onScanResultDeviceName %s", result.device.name)

                if (result.device.name != null
                        && (result.device.name.contains(fragment.get()?.args?.deviceData?.deviceModel.toString()))) {
                    fragment.get()?.scaleScanTimeoutHandler?.removeCallbacksAndMessages(null)
                    fragment.get()?.bluetoothLeScanner?.stopScan(this)

                    if (result.device == null) {
                        fragment.get()?.deviceAlreadyAddedAction()

                    } else if ((result.device != null) && (result.device.name.contains(BF600)) && (fragment.get()?.callOnce == true)) {
                        fragment.get()?.callOnce = false
                        Timber.d("onScanResultAS87Found %s", result.device.address)

                        val weakContext = fragment.get()?.activity
                        val weakListener = bleResponseListener.get()
                        val weakDeviceData = fragment.get()?.args?.deviceData

                        if (weakContext != null && weakDeviceData != null && weakListener != null) {
                            fragment.get()?.bf600Wrapper = BF600Wrapper(weakContext, result.device.address, weakDeviceData)
                            fragment.get()?.bf600Wrapper?.setBleResponseListener(weakListener)
                        }
                    }
                }
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                fragment.get()?.context?.let {
                    MdocUtil.showToastLong(it, errorCode.toString())
                }
            }
        }
    }

    private fun cancelScan() {
        activity.runOnUiThread {
            if (isAdded) {
                bluetoothLeScanner?.stopScan(scanCallBack)
                Toast.makeText(context, resources.getText(R.string.bluetooth_timeout), Toast.LENGTH_LONG)
                    .show()
                findNavController().popBackStack()
            }
        }
    }

    override fun genericDeviceResponse(deviceData: ArrayList<Byte>,
                                       gatt: BluetoothGatt,
                                       message: BleMessage) {
        activity.runOnUiThread {
            if (isAdded) {
                when (message) {
                    BleMessage.USER_LIST          -> {
                        hideProgress()
                        val items = parseUserList(deviceData)
                        setupAdapter(items)
                    }
                    BleMessage.NO_USERS           -> {
                        hideProgress()
                        MdocUtil.showToastLong(activity, "NO USERS ON DEVICE")
                    }
                    BleMessage.CONNECTION_TIMEOUT -> {
                        connectionTimeoutAction(this)
                    }

                    else                          -> {
                        //do nothing
                    }
                }
            }
        }
    }

    private fun setupAdapter(items: ArrayList<Bf600User>) {
        if (isAdded) {
            adapterItems.clear()
            val stringSelect = resources.getString(R.string.select_user)
            adapterItems.add(CodingItem("", stringSelect, stringSelect))

            items.forEach {
                val index = it.userIndex
                adapterItems.add(
                        CodingItem("", index,
                                resources.getString(R.string.user) + " " + index))
            }
            val spinnerAdapter = SpinnerAdapter(activity, adapterItems)
            user_spinner?.adapter = spinnerAdapter
        }
    }

    private fun deviceAlreadyAddedAction() {
        activity.runOnUiThread {
            if (isAdded) {
                MdocUtil.showToastLong(activity,
                        activity.resources.getString(R.string.err_device_already_added))
               findNavController().popBackStack()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        bluetoothLeScanner?.stopScan(scanCallBack)
        scaleScanTimeoutHandler.removeCallbacksAndMessages(null)
        if (shouldCancelConnection) {
            callOnce=true
            bf600Wrapper?.disconnect()
            bf600Wrapper=null
        }
    }

    companion object {
        private const val ENABLE_BT_REQUEST_ID = 1
        private const val ENABLE_LOCATIONS_REQUEST_ID = 2
    }

    private fun hideProgress () {
        if (isAdded) {
            progress.visibility = View.GONE
            scroll.visibility = View.VISIBLE
        }
    }
}
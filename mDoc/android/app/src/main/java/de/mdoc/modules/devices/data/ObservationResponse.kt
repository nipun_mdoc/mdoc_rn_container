package de.mdoc.modules.devices.data

import de.mdoc.constants.MdocConstants

class ObservationResponse{

    lateinit var data: ArrayList<Data>

     fun getSystolicData() : Data?{
        for(item in data){
            if(item.metricCode == MdocConstants.METRIC_CODE_BLOOD_PRESSURE_SYS){
                return item
            }
        }
        return null
    }

     fun getDiastolicData() : Data?{
        for(item in data){
            if(item.metricCode == MdocConstants.METRIC_CODE_BLOOD_PRESSURE_DIA){
                return item
            }
        }
        return null
    }
    fun isComposite():Boolean?{

        return data.size>1
    }
}
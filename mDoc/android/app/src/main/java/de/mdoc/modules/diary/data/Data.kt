package de.mdoc.modules.diary.data

data class Data(
    val id: String,
    val pollDetails: PollDetails
)
package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.ServiceIds;

public class SpecialtyServices {

    private ArrayList<ServiceIds> serviceIds;
    private String title;
    private String description;

    public ArrayList<ServiceIds> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(ArrayList<ServiceIds> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    boolean containsSpecificServiceId(String serviceId){
        boolean contains = false;
        for(ServiceIds id : serviceIds){
            if (id.getServiceId().equalsIgnoreCase(serviceId)) {
                contains = true;
                break;
            }
        }
        return contains;
    }
    public String getServiceId(String serviceProvider){
        String serviceId="";
        for(ServiceIds id : serviceIds){
            if(id.getProvider().equalsIgnoreCase(serviceProvider)){
                serviceId =id.getServiceId();
            }
        }
        return serviceId;
    }

}

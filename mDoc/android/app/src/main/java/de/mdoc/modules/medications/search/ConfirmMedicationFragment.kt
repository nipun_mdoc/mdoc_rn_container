package de.mdoc.modules.medications.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentConfirmMedicationBinding
import de.mdoc.fragments.NewBaseFragment
import kotlinx.android.synthetic.main.fragment_confirm_medication.*

class ConfirmMedicationFragment: NewBaseFragment() {

    private val args: ConfirmMedicationFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentConfirmMedicationBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_confirm_medication, container, false)

        binding.lifecycleOwner = this

        if(args.medication.manualEntry){
            args.medication.code=resources.getString(R.string.med_plan_not_available)
        }
        binding.medicationItem = args.medication
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_confirm_medication?.setOnClickListener {
            val direction = ConfirmMedicationFragmentDirections.actionToFragmentMedicationPlan(args.medication, args.medication.form, args.type)
            findNavController().navigate(direction)
        }
    }

    override val navigationItem: NavigationItem = NavigationItem.Medication
}
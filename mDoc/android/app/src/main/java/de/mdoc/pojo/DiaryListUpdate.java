package de.mdoc.pojo;

import java.io.Serializable;

import javax.annotation.Nullable;

public class DiaryListUpdate implements Serializable {


    private String caseId;
    private String clinicId;
    private String title;
    private String description;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getDiaryConfig() {
        return diaryConfig;
    }

    public void setDiaryConfig(String diaryConfig) {
        this.diaryConfig = diaryConfig;
    }

    private Boolean showToDoctor;
    private long start;
    private long startTime;

    private String diaryConfig;



    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getShowToDoctor() {
        return showToDoctor;
    }

    public void setShowToDoctor(Boolean showToDoctor) {
        this.showToDoctor = showToDoctor;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }


}

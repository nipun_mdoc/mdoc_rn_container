package de.mdoc.pojo

import de.mdoc.network.response.PollVotingActionDetails

data class QuestionnairesAnswersResponse(
        val code: String,
        val timestamp: Long,
        val description: String,
        val message: String,
        var data: QuestionnairesAnswersResponseData
)

data class QuestionnairesAnswersResponseData(
        val cts: Long,
        val uts: Long,
        val id: String,
        val pollVotingActionDetails: PollVotingActionDetails
)
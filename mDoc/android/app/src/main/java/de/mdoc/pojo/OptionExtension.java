package de.mdoc.pojo;

/**
 * Created by ema on 5/19/17.
 */

public class OptionExtension extends Option {

    private String answer;
    private String selection;
    private String language;

    public OptionExtension(Option o) {
        this.answer = null;
        this.key = o.getKey();
        this.value = o.value;
    }

    public OptionExtension(String selection, String answer, Option o, String language) {
        this.selection = selection;
        this.answer = answer;
        this.key = o.getKey();
        this.value = o.value;
        this.language = language;
    }

    public OptionExtension(String answer) {
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getSelection() {
        return selection;
    }

    public void setSelection(String selection) {
        this.selection = selection;
    }

    public String getLanguage() {
        return language;
    }
}

package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NewAppointmentSchedule implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("appointmentDetails")
    @Expose
    private AppointmentDetails appointmentDetails;
    @SerializedName("integrationType")
    @Expose
    private String integrationType;
    @SerializedName("deleted")
    @Expose
    private boolean deleted;
    @SerializedName("confirmationRequired")
    @Expose
    private boolean confirmationRequired;
    @SerializedName("newVersionReferenceId")
    @Expose
    private String newVersionReferenceId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("newVersionDocument")
    @Expose
    private RescheduledAppointment newVersionDocument;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    public void setConfirmationRequired(boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    public String getNewVersionReferenceId() {
        return newVersionReferenceId;
    }

    public void setNewVersionReferenceId(String newVersionReferenceId) {
        this.newVersionReferenceId = newVersionReferenceId;
    }

    public RescheduledAppointment getNewVersionDocument() {
        return newVersionDocument;
    }

    public void setNewVersionDocument(RescheduledAppointment newVersionDocument) {
        this.newVersionDocument = newVersionDocument;
    }
}

package de.mdoc.modules.external_services

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentExternalServicesBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.external_services.view_model.ExternalServicesViewModel
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class ExternalServicesFragment : NewBaseFragment() {

    private val extServicesViewModel by viewModel {
        ExternalServicesViewModel(RestClient.getService())
    }

    override val navigationItem: NavigationItem = NavigationItem.ExternalServices

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentExternalServicesBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_external_services, container, false)

        binding.lifecycleOwner = this
        binding.handler = ExternalServicesHandler()
        binding.extServicesViewModel = extServicesViewModel
        return binding.root
    }
}
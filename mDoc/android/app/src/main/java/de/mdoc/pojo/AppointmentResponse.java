package de.mdoc.pojo;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by AdisMulabdic on 2/12/18.
 */

public class AppointmentResponse extends MdocResponse {
    private AppointmentData data;

    public AppointmentData getData() {
        return data;
    }

    public void setData(AppointmentData data) {
        this.data = data;
    }
}


package de.mdoc.data.questionnaire

data class AnswersRequest(var from: String? = null,
                          var to: String? = null,
                          var pollVotingActionId: String? = null,
                          var pollId: String? = null,
                          var includeAnswers: Boolean = false)
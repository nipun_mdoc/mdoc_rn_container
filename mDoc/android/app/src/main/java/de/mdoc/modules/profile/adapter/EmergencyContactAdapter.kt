package de.mdoc.modules.profile.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.profile.MyProfileFragmentDirections
import de.mdoc.modules.profile.data.Contact
import de.mdoc.pojo.CodingItem
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.placeholder_contact_in_profile.view.*
import java.util.*

class EmergencyContactAdapter(val items: ArrayList<Contact>, val context: Context):
        RecyclerView.Adapter<InnerSecondaryEventViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): InnerSecondaryEventViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_contact_in_profile, parent, false)

        return InnerSecondaryEventViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(holder: InnerSecondaryEventViewHolder, position: Int) {
        val item = items[position]
        holder.txtContactHeader.text = context.resources.getString(R.string.profile_contact_header, (position + 1))
        holder.txtName.text = item.name
        val relationshipList = arrayListOf(CodingItem())
        relationshipList.addAll(AppPersistence.relationshipCoding)
        val index = relationshipList.indexOfFirst { it.code == item.relation }
        holder.txtRelation.text = relationshipList.getOrElse(index) { CodingItem(display = "") }.display
        holder.txtPhone.text = item.phone

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MYPROFILE, MdocConstants.HAS_EMAIL_IN_EMERGENCY_CONTACT)) {
            holder.txtEmail.visibility = View.VISIBLE
            holder.txtEmailLabel.visibility = View.VISIBLE
            holder.txtEmail.text = item.email
        }
        else {
            holder.txtEmail.visibility = View.GONE
            holder.txtEmailLabel.visibility = View.GONE
        }
        holder.txtEditContact.setOnClickListener {
            val action = MyProfileFragmentDirections.actionMyProfileFragmentToEditContactFragment(
                    items.toTypedArray(), position, false)
            (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                .navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class InnerSecondaryEventViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val txtContactHeader: TextView = itemView.txtContactHeader
    val txtName: TextView = itemView.txtName
    val txtRelation: TextView = itemView.txtRelation
    val txtEmailLabel: TextView = itemView.txtEmailLabel
    val txtEmail: TextView = itemView.txtEmail
    val txtPhone: TextView = itemView.txtPhone
    val txtEditContact: TextView = itemView.txtEditContact
}
package de.mdoc.modules.questionnaire.history

import androidx.lifecycle.ViewModel
import de.mdoc.data.questionnaire.AnswersRequest
import de.mdoc.data.questionnaire.Questionnaire
import de.mdoc.data.responses.QuestionnaireAnswersResponse
import de.mdoc.network.managers.MdocManager
import de.mdoc.viewmodel.combine
import de.mdoc.viewmodel.liveData
import de.mdoc.viewmodel.map
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class QuestionnaireHistoryViewModel : ViewModel(), Callback<QuestionnaireAnswersResponse> {

    private val allQuestionnaires = liveData<List<QuestionnaireHistoryItem>>(emptyList())

    val inProgress = liveData(false)

    val names = allQuestionnaires.map { list ->
        list.map {
            it.name
        }.distinct()
    }

    val selectedType = liveData<String>("")

    val dates = combine(allQuestionnaires, selectedType) { list, type ->
        list.filter {
            it.name == type
        }.map {
            it.date
        }.distinct()
    }
    val selectedDate = liveData<String>("")

    val listOfItems =
        combine(allQuestionnaires, selectedType, selectedDate) { all, selectedType, selectedDate ->
            all.filter {
                it.name == selectedType && it.date == selectedDate
            }
        }

    val isNoHistory = liveData(false)
    val isDataPresent = liveData(false)

    init {
        names.observe {
            selectedType.set(it.getOrNull(0) ?: "")
        }
        dates.observe {
            val current = selectedDate.get()
            if (!it.contains(current)) {
                selectedDate.set(it.getOrNull(0) ?: "")
            }
        }
        getAnswers()
    }

    private fun getAnswers() {
        val request = AnswersRequest()
        val from = request.from
        val to = request.to
        val pollVotingActionId = request.pollVotingActionId
        val pollId = request.pollId
        val includeAnswers = request.includeAnswers
        inProgress.set(true)
        MdocManager.getAnsweredQuestionnaires(
            from,
            to,
            pollVotingActionId,
            pollId,
            includeAnswers,
            this
        )

    }

    override fun onResponse(
        call: Call<QuestionnaireAnswersResponse>,
        response: Response<QuestionnaireAnswersResponse>
    ) {
        inProgress.set(false)
        if (response.isSuccessful) {
            val list: List<Questionnaire> =
                response.body()?.data?.get(0)?.questionnaires ?: emptyList()
            allQuestionnaires.set(list.map(::QuestionnaireHistoryItem))
            isNoHistory.set(list.isEmpty())
            isDataPresent.set(list.isNotEmpty())
        }
    }

    override fun onFailure(call: Call<QuestionnaireAnswersResponse>, t: Throwable) {
        inProgress.set(false)
    }

}
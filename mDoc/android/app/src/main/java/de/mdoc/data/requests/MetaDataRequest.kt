package de.mdoc.data.requests

import de.mdoc.data.login_meta_data.Address


data class MetaDataRequest(var clinicId:String="",
                           var department:String="",
                           var title:String="",
                           var phone:String="",
                           var gender:String="",
                           var born:Long=0,
                           var countryCode: String="",
                           var federalState: String="",
                           var address: Address=Address()
)
package de.mdoc.data.configuration

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ConfigurationDetailsMultiValue(
        val medicals: Boolean,
        val vitals: Boolean
): Parcelable
package de.mdoc.modules.covid19.emergency_contact

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentEmergencyContactsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel

class EmergencyContactsFragment: NewBaseFragment() {
    lateinit var activity: MdocActivity
    override val navigationItem: NavigationItem = NavigationItem.EmergencyContacts
    private val emergencyContactsViewModel by viewModel {
        EmergencyContactsViewModel(
                RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentEmergencyContactsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_emergency_contacts, container, false)


        binding.lifecycleOwner = this
        binding.handler = EmergencyContactsHandler()
        binding.emergencyContactsViewModel = emergencyContactsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_emergency_contacts)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.openContacts) {
            findNavController().navigate(R.id.navigation_profile)
        }
        return super.onOptionsItemSelected(item)
    }
}

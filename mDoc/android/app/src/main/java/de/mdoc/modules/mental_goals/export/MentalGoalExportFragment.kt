package de.mdoc.modules.mental_goals.export

import android.os.Bundle
import android.view.View
import de.mdoc.modules.common.export.BaseExportFragment
import de.mdoc.network.RestClient
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_goal_export_options.*

class MentalGoalExportFragment : BaseExportFragment() {

    private val viewModel by viewModel {
        MentalGoalExportViewModel(RestClient.getService(), this)
    }

    lateinit var goalId: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindVisibleGone(downloadExportProgress, viewModel.isLoading)
    }

    override fun downloadPdf() {
        viewModel.download(goalId, ExportType.PDF, context)
    }

    override fun downloadXml() {
        viewModel.download(goalId, ExportType.XLSX, context)
    }
}
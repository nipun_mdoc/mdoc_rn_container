package de.mdoc.pojo;

import de.mdoc.network.response.MdocResponse;

public class FreeSlotsResponse extends MdocResponse {

    private FreeSlotsData data;

    public FreeSlotsData getData() {
        return data;
    }

    public void setData(FreeSlotsData data) {
        this.data = data;
    }
}

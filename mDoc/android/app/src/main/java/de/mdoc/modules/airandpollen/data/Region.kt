package de.mdoc.modules.airandpollen.data

data class Region(
    val id: Int,
    val name: String,
    val today: List<AirAndPollenInfo>,
    val tomorrow: List<AirAndPollenInfo>,
    val dayAfterTomorrow: List<AirAndPollenInfo>
)
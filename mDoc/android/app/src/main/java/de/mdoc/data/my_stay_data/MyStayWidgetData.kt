package de.mdoc.data.my_stay_data

import de.mdoc.constants.CGM_PUSH
import java.io.Serializable
import java.util.*


data class MyStayWidgetData(
        val caseId: String,
        val checkIn: Long,
        val checkOut: Long,
        val checkOutActual: Long? = null,
        val doctorImage: String,
        val room: String,
        val speciality: String,
        val department:String,
        val doctorName:String,
        val clinicName:String,
        val integrationType: String? = null,
        val physician:String = ""
): Serializable{
    fun isCgmPatient(): Boolean{
        return !integrationType.isNullOrEmpty() && integrationType == CGM_PUSH
    }

    fun canPatientAccessEntertainment (): Boolean {
        val checkInDate = getCheckInDate()
        var checkOutDate = getCheckOutDate()

        // If dates are null we'll not let user to read magazines
        if (checkInDate  == null || checkOutDate == null) {
            return false
        }

        // Check if current date is not before check-in and after check-out date
        return !(isBeforeCheckIn() || isAfterCheckOut())
    }

    fun isBeforeCheckIn (): Boolean {
        val checkInDate = getCheckInDate()
        return Date().before(checkInDate)
    }

    fun isAfterCheckOut (): Boolean {
        val checkOutDate = getCheckOutDate()
        return Date().after(checkOutDate)
    }

    fun getCheckOutDate (): Date? {
        if (checkOutActual != null){
             return Date(checkOutActual)
        }

        return  Date(checkOut)
    }

    fun getCheckInDate (): Date? {
        return Date(checkIn)
    }
}
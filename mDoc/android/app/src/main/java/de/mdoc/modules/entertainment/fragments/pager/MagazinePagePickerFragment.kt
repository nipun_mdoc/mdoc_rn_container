package de.mdoc.modules.entertainment.fragments.pager

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.entertainment.adapters.pager.MagazinePagePickerAdapter
import de.mdoc.modules.entertainment.viewmodels.MagazineBookmarkViewModel
import de.mdoc.network.RestClient
import de.mdoc.pojo.entertainment.MediaResponseDto
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_entertainment_magazine_page_picker.*


class MagazinePagePickerFragment : DialogFragment(), MagazinePagePickerAdapter.MagazinePagePickerAdapterDelegate {

    companion object {
        const val SELECTED_PAGE = "SELECTED_PAGE"
    }

    lateinit var magazine: MediaResponseDto

    private val viewModelBookmark by viewModel {
        MagazineBookmarkViewModel(
                mDocService = RestClient.getService(),
                mediaId = magazine.id
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                STYLE_NO_TITLE,
                de.mdoc.R.style.FullScreenDialogTranslucentStatus
        )

        val bundle = arguments
        if (bundle != null) {
            magazine = Gson().fromJson(bundle.getString(MdocConstants.MAGAZINE), MediaResponseDto::class.java)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {

        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(de.mdoc.R.layout.fragment_entertainment_magazine_page_picker, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rvPages.apply {
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

            val maxPages = (magazine.pageMax?.toInt() ?: 0)
            val rangeList: IntArray = (1..maxPages).toList().toIntArray()
            adapter = MagazinePagePickerAdapter(items = rangeList, delegate = this@MagazinePagePickerFragment)
            viewModelBookmark.bookmark.observe(viewLifecycleOwner, Observer {
                adapter = MagazinePagePickerAdapter(items = rangeList, bookmarks = it.bookMarkedPages, delegate = this@MagazinePagePickerFragment)
            })
        }


        btnClose.setOnClickListener {
            dismiss()
        }
    }

    override fun onSelected(position: Int) {
        findNavController().previousBackStackEntry?.savedStateHandle?.set(SELECTED_PAGE, position)
        dismiss()
    }
}
package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.util.MdocAppHelper
import org.joda.time.LocalDateTime

class Bf600Device(val gatt: BluetoothGatt?, val data: ArrayList<Byte>?): BluetoothDevice {


    override fun getDeviceObservations(list: ArrayList<Byte>,
                                       deviceId: String,
                                       updateFrom: Long?): ArrayList<Observation> {

        val userId = MdocAppHelper.getInstance().userId
        val codingForCategory =
                Coding("fitness", "Fitness Data", "http://hl7.org/fhir/observation-category")
        val codingForCategoryList = listOf(codingForCategory)
        val category = Category(codingForCategoryList)
        val categoryList = listOf(category)
        val weightCoding = Coding("29463-7", "Body weight", "http://loinc.org")
        val weightCodingList = listOf(weightCoding)
        val weightCode = Code(weightCodingList)
        val performer = Performer("user/$userId")
        val performerList = listOf(performer)
        val subject = Subject("user/$userId")
        val text = Text("<div>[name] : [value] [hunits] @ [date]</div>", "GENERATED")
        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var observedItem: Observation
        var weightHexOne = ""
        var weightHexTwo = ""
        var yearOne = ""
        var yearTwo = ""
        var monthHex = ""
        var dayHex = ""
        var hourHex = ""
        var minuteHex = ""
        var secondHex = ""


        list.forEachIndexed { index, byte ->
            when (index) {
                1  -> weightHexOne = String.format("%02X", byte)
                2  -> weightHexTwo = String.format("%02X", byte)
                3  -> yearOne = String.format("%02X", byte)
                4  -> yearTwo = String.format("%02X", byte)
                5  -> monthHex = String.format("%02X", byte)
                6  -> dayHex = String.format("%02X", byte)
                7  -> hourHex = String.format("%02X", byte)
                8  -> minuteHex = String.format("%02X", byte)
                9  -> secondHex = String.format("%02X", byte)

                14 -> {
                    val hexYear = yearTwo + yearOne
                    val yearParsed = java.lang.Long.parseLong(hexYear, 16)
                        .toInt()
                    val month = java.lang.Long.parseLong(monthHex, 16).toInt()
                    val day = java.lang.Long.parseLong(dayHex, 16)
                        .toInt()
                    val hour = java.lang.Long.parseLong(hourHex, 16)
                        .toInt()
                    val minute = java.lang.Long.parseLong(minuteHex, 16)
                        .toInt()
                    val second = java.lang.Long.parseLong(secondHex, 16)
                        .toInt()
                    val hexWeight = weightHexTwo + weightHexOne
                    val weightParsed = java.lang.Long.parseLong(hexWeight, 16).toDouble()

                    val weightConverted:Double=weightParsed/200
                    val measurementTime =
                            LocalDateTime(yearParsed, month, day, hour, minute,
                                    second).toDateTime()
                                .millis


                    observedItem = Observation(categoryList
                            , weightCode
                            , Device("device/$deviceId")
                            , measurementTime.toString()
                            , performerList, "Observation"
                            , "FINAL"
                            , subject
                            , text
                            , ValueQuantity("kg", "http://unitsofmeasure.org", "kilograms",
                            weightConverted), "BF600")

                    if (updateFrom == null) {
                        observations.add(observedItem)
                    }
                    else {
                        if (measurementTime > updateFrom) {
                            observations.add(observedItem)
                        }
                    }
                }
            }
        }
        return observations
    }

    override fun deviceAddress(): String {
        return gatt?.device?.address ?: ""
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data ?: arrayListOf()
    }

    override fun deviceName(): String {
        return gatt?.device?.name ?: ""
    }
}

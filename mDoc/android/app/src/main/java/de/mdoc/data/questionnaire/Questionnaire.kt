package de.mdoc.data.questionnaire

data class Questionnaire(
        val pollId: String,
        val pollTitle: String,
        val pollVotingActions: List<PollVotingAction>
)
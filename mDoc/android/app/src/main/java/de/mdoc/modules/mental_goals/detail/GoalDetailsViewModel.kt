package de.mdoc.modules.mental_goals.detail

import androidx.lifecycle.ViewModel
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.liveData

class GoalDetailsViewModel(
     repository: GoalDetailsRepository
) : ViewModel() {

    var isEditButtonVisible = false
    val isChangeStatusButtonVisible = liveData<Boolean>()

    val title = liveData<String>()
    val description = liveData<String>()
    val achievementPeriod = liveData<String>()
    val actionItems = liveData<List<ActionItem>>()
    val ifThenPlans = liveData<List<IfThenPlan>>()

    init {
        isEditButtonVisible=repository.isGoalEditable()
        isChangeStatusButtonVisible.set(isEditButtonVisible)
        title.set(repository.getTitle())
        description.set(repository.getDescription())
        val date = MdocUtil.getDateFromMS(
            MdocConstants.D_IN_WEEK_DD_MM_YY_FORMAT,
            repository.getTimePeriod()
        )
        achievementPeriod.set(date)
        actionItems.set(repository.getActionItems())
        ifThenPlans.set(repository.getIfThenPlans())
    }

}
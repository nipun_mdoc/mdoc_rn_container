package de.mdoc.modules.files.filter

import android.content.Context
import de.mdoc.R
import de.mdoc.pojo.CodingItem

sealed class FilterCategory {

    object AllCategories : FilterCategory() {

        override fun getName(context: Context): String {
            return context.getString(R.string.files_filter_categories_all)
        }

        override fun getId(): Long {
            return 0
        }

    }

    data class SingleCategory(
        val data: CodingItem,
        private val id: Long
    ) : FilterCategory() {

        override fun getName(context: Context): String {
            return data.display ?: ""
        }

        override fun getId(): Long {
            return id
        }

    }

    abstract fun getName(context: Context): String
    abstract fun getId(): Long

}
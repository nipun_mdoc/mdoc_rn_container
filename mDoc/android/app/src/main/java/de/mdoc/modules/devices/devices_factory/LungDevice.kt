package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.util.MdocAppHelper
import java.util.*

class LungDevice(val gatt: BluetoothGatt, val data: ArrayList<Byte>) : BluetoothDevice {

    override fun getDeviceObservations(list: ArrayList<Byte>, deviceId: String, updateFrom: Long?): ArrayList<Observation> {
        val userId = MdocAppHelper.getInstance().userId

        val codingForCategory = Coding("vitals", "Fitness Data", "http://hl7.org/fhir/observation-category")
        val codingForCategoryList = listOf(codingForCategory)
        val category = Category(codingForCategoryList)
        val categoryList = listOf(category)

        ///////////////////

        val stepsCoding = Coding("19935-6", null, "http://loinc.org")
        val stepsCodingList = listOf(stepsCoding)
        val spoCode = Code(stepsCodingList)

        /////////////

        val performer = Performer("user/$userId")
        val performerList = listOf(performer)
        val subject = Subject("user/$userId")
        val text = Text("<div>[name] : [value] [hunits] @ [date]</div>", "GENERATED")

        //////////////

        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var observedItem: Observation

        var dataList = byteArrayOf(data[15], data[16], data[17])
        val stringData = String(dataList)

        observedItem = Observation(
                categoryList
                , spoCode
                , Device("device/$deviceId") //
                , Calendar.getInstance().timeInMillis.toString()
                , performerList, "Observation"
                , "FINAL"
                , subject
                , text
                , ValueQuantity("l/min", "http://unitsofmeasure.org", "1/min", stringData.toDouble())
                , "TIO") //stringData.toInt()
        if (updateFrom == null) {
            observations.add(observedItem)
        }
//        else {
//            if (dateInMS > updateFrom) {
//                observations.add(observedItem)
//            }
//        }

        return observations

    }


    override fun deviceAddress(): String {
        return gatt.device.address
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data
    }

    override fun deviceName(): String {
        return gatt.device.name
    }
}
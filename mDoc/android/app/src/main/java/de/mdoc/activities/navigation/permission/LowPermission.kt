package de.mdoc.activities.navigation.permission

import android.content.res.Resources
import de.mdoc.R
import de.mdoc.activities.navigation.MenuItem
import de.mdoc.constants.MdocConstants
import de.mdoc.util.MdocAppHelper

/*
* User story: https://mdocplatform.atlassian.net/browse/MDPD-10767
* */
class LowPermission(val resources: Resources?) {

    fun hasPermissionToDisplay (): Boolean {
        val isPairingMandatory = (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_PAIR, MdocConstants.MANDATROY_PAIRING))
        if (!isPairingMandatory) {
            return true
        }

        val isPaired = MdocAppHelper.getInstance().hasSecretKey()
        return isPairingMandatory && isPaired
    }
}


private val LOW_PERMISSION_MENU_ITEMS: List<MenuItem> = listOf(
        MenuItem.Dashboard,
        MenuItem.MyClinic,
        MenuItem.MyProfile,
        MenuItem.HelpSupport)


// Menu
fun MenuItem.shouldDisplay(resources: Resources?): Boolean {
    val hasPermissionToDisplay = LowPermission(resources).hasPermissionToDisplay()

    if (!hasPermissionToDisplay) {
        val isLowPermissionModule = LOW_PERMISSION_MENU_ITEMS.contains(this)

        if (!isLowPermissionModule) {
            return false
        }
    }

    return configEnabledResIds.all { resources?.getBoolean(it) == true }
}
package de.mdoc.security

import android.content.Context
import android.security.keystore.KeyInfo
import android.security.keystore.UserNotAuthenticatedException
import java.security.InvalidKeyException
import java.security.spec.InvalidKeySpecException
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory

class EncryptionServices(val context: Context) {

    companion object {

        val DEFAULT_KEY_STORE_NAME = "default_keystore"
        val MASTER_KEY = "MASTER_KEY"
        val FINGERPRINT_KEY = "FINGERPRINT_KEY"
    }

    private val keyStoreWrapper = KeyStoreWrapper(context, DEFAULT_KEY_STORE_NAME)

    fun createMasterKey() {
        keyStoreWrapper.createAndroidKeyStoreSymmetricKey(MASTER_KEY)
    }

    fun getMasterKey(): SecretKey? {
        return keyStoreWrapper.getAndroidKeyStoreSymmetricKey(MASTER_KEY)
    }

    fun removeMasterKey() {
        keyStoreWrapper.removeAndroidKeyStoreKey(MASTER_KEY)
    }

    fun createFingerprintKey() {
        keyStoreWrapper.createAndroidKeyStoreSymmetricKey(FINGERPRINT_KEY,
                userAuthenticationRequired = true,
                invalidatedByBiometricEnrollment = true,
                userAuthenticationValidWhileOnBody = false,
                userAuthenticationValidityDurationSeconds = 10)
    }

    fun getFingerprintKey(): SecretKey? {
        return keyStoreWrapper.getAndroidKeyStoreSymmetricKey(FINGERPRINT_KEY)
    }

    fun removeFingerprintKey() {
        keyStoreWrapper.removeAndroidKeyStoreKey(FINGERPRINT_KEY)
    }

    fun encrypt(data: String): String {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).encrypt(data, masterKey, true)
    }

    fun decrypt(data: String): String {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).decrypt(data, masterKey, true)
    }

    @Throws(UserNotAuthenticatedException::class, InvalidKeyException::class)
    fun encryptWithFingerprint(data: String): String? {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(FINGERPRINT_KEY)
        val result: String?
        try {
            result = CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).encrypt(data, masterKey, true)
        } catch (e: UserNotAuthenticatedException) {
            throw UserNotAuthenticatedException()
        } catch (e: InvalidKeyException) {
            throw InvalidKeyException()
        }
        return result
    }

    @Throws(UserNotAuthenticatedException::class, InvalidKeyException::class)
    fun decryptWithFingerprint(data: String): String? {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreSymmetricKey(FINGERPRINT_KEY)
        val result: String?
        try {
            result = CipherWrapper(CipherWrapper.TRANSFORMATION_SYMMETRIC).decrypt(data, masterKey, true)
        } catch (e: UserNotAuthenticatedException) {
            throw UserNotAuthenticatedException()
        } catch (e: InvalidKeyException) {
            throw InvalidKeyException()
        }
        return result
    }
}
package de.mdoc.pojo;

import java.util.ArrayList;

public class Type {

    private ArrayList<Coding> coding;

    public ArrayList<Coding> getCoding() {
        return coding;
    }

    public void setCoding(ArrayList<Coding> coding) {
        this.coding = coding;
    }

    public boolean isKrank(){
        for (Coding c : coding){
            if(c.getCode().equals("krank")){
                return true;
            }
        }
        return false;
    }

    public boolean isInvalid(){
        for (Coding c : coding){
            if(c.getCode().equals("invalid")){
                return true;
            }
        }
        return false;
    }

    public boolean isMilitar(){
        for (Coding c : coding){
            if(c.getCode().equals("militar")){
                return true;
            }
        }
        return false;
    }

    public boolean isUnfal(){
        for (Coding c : coding){
            if(c.getCode().equals("unfall")){
                return true;
            }
        }
        return false;
    }

    public boolean isSuplementary(){
        for (Coding c : coding){
            if(c.getCode().equals("zusatz")){
                return true;
            }
        }
        return false;
    }

    public boolean isSuplementaryAccident(){
        for (Coding c : coding){
            if(c.getCode().equals("unfall_zusatz")){
                return true;
            }
        }
        return false;
    }


    public String getKrankClassCode(){
        for (Coding c : coding){
            if(c.getSystem().contains("insuranceTypes")){
                return c.getCode();
            }
        }
        return "";
    }

    public String getAccidentClassCode(){
        for (Coding c : coding){
            if(c.getSystem().contains("insuranceTypes")){
                return c.getCode();
            }
        }
        return "";
    }

    public String getSuplementeryClassCode(){
        for (Coding c : coding){
            if(c.getSystem().contains("supplementaryInsuranceClass")){
                return c.getCode();
            }
        }
        return "";
    }
}

package de.mdoc.modules.diary

import android.app.Dialog
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.data.diary.Data
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.modules.diary.data.DiaryFilter
import de.mdoc.modules.diary.data.RxDiaryRepository
import de.mdoc.modules.diary.export.DiaryExportFragment
import de.mdoc.modules.diary.filter.DiaryEntryType
import de.mdoc.modules.diary.filter.FilterDiaryFragment
import de.mdoc.modules.mental_goals.export.MentalGoalExportFragment
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import de.mdoc.network.RestClient
import de.mdoc.pojo.DiaryList
import de.mdoc.storage.AppPersistence.diaryConfig
import de.mdoc.util.*
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.bindVisibleInvisible
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.common_search_view.*
import kotlinx.android.synthetic.main.fragment_diary_dashboard.*
import java.util.*

class DiaryDashboardFragment: MdocFragment(), CommonDialogFragment.CancelListener,
        CommonDialogFragment.OnButtonClickListener,DiaryAdapterCallback {

    var category: List<Data>? = null
    val viewModel by viewModel {
        RxDiaryViewModel(repository = RxDiaryRepository(RestClient.getService()))
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_diary_dashboard
    }

    override val navigationItem: NavigationItem = NavigationItem.Diary

    override fun init(savedInstanceState: Bundle?) {
        // do nothing
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed {
            if (hasFilter()) {
                viewModel.filter.set(DiaryFilter(
                        categories = null,
                        from = null,
                        to = null))
            }
            else {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    findNavController().popBackStack()
                }
            }
        }

        setHasOptionsMenu(true)
        shouldShowDisclaimer()
        bindVisibleInvisible(progressBar, viewModel.isLoading)
        bindVisibleGone(error, viewModel.isShowLoadingError)
        bindVisibleGone(noDataLl, viewModel.isShowEmptyMessage)
        bindVisibleGone(hasDataRl, viewModel.isShowContent)

//        bindVisibleGone(linerDeleteMsg, viewModel.shouldShowDeleteMsg)

        startUsingDiaryBtn?.setOnClickListener {
            openBottomSheet()
        }

        newEntryBtn?.setOnClickListener {
            openBottomSheet()
        }
        searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                findNavController().navigate(R.id.action_diaryDashboardFragment_to_searchDiaryFragment)
            }
        }

        viewModel.shouldShowDeleteMsg.observe(viewLifecycleOwner){
            linerDeleteMsg.visibility = if(it == true )View.VISIBLE else View.GONE
        }

        viewModel.diaryItems.observe(this) {
            diariesRv?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            val adapter = DiaryAdapter(it, activity as MdocActivity,false,DiaryDashboardFragment@this)
            diariesRv?.adapter = adapter
            diariesRv?.adapter?.notifyDataSetChanged()
        }

        viewModel.diaryConfig.observe(this) {
            category = it
            val entries = mutableListOf<DiaryEntryType>()
            it.forEach { data ->
                entries.add(
                        DiaryEntryType(
                                diaryId = data.id,
                                isSelected = false,
                                base64Image = data.coding.imageBase64,
                                //                                name = data.title
                                name = if (data.title == null) {
                                    data.coding.display
                                }
                                else {
                                    data.title
                                }
                                      )
                           )
            }

            diaryConfig = entries
        }
    }

    private fun shouldShowDisclaimer() {
        val moduleName = resources.getString(R.string.medical_diary_lower)
        val list = resources.getStringArray(R.array.disclaimer_modules)
        if (list.contains(moduleName) && (MdocAppHelper.getInstance().disclaimerSeenDiary == false)) {
            showDisclaimerDialog()
        }
    }

    private fun showDisclaimerDialog() {
        CommonDialogFragment.Builder()
            .title(resources.getString(R.string.disclaimer))
            .description(resources.getString(R.string.disclaimer_body))
            .setOnCancelListener(this)
            .setOnClickListener(this)
            .setPositiveButton(resources.getString(R.string.disclaimer_accept))
            .navigateBackOnCancel(true)
            .build()
            .showNow(childFragmentManager, "")
    }

    private fun openBottomSheet() {
        when (category?.size) {
            null -> {
                MdocUtil.showToastLong(activity, resources.getString(R.string.no_configuration))
            }

            1    -> {
                val categoryItem = category!![0]
                val questionnaireAdditionalFields =
                        QuestionnaireAdditionalFields(
                                showFreeText = categoryItem.freeTextEntryAllowed,
                                allowSharing = categoryItem.shareAllowed,
                                autoShare = categoryItem.autoShare,
                                isDiary = true,
                                shouldAssignQuestionnaire = true,
                                diaryConfigId = categoryItem.id,
                                pollId = categoryItem.pollId)
                val action = if (categoryItem.pollId != null) {
                    MainNavDirections.globalActionToQuestion(
                            questionnaireAdditionalFields)
                }
                else {
                    DiaryDashboardFragmentDirections.actionDiaryDashboardFragmentToDiaryEntryFragment(
                            questionnaireAdditionalFields)
                }
                findNavController().navigate(action)
            }

            else -> {
                val bottomSheetDialogFragment = DiaryBottomSheetDialogFragment.newInstance(
                        category ?: listOf()
                                                                                          )
                bottomSheetDialogFragment.show(
                        (activity as MdocActivity).supportFragmentManager,
                        bottomSheetDialogFragment.tag
                                              )
            }
        }
    }

    fun setFilter(diaryFilter: DiaryFilter) {
        viewModel.filter.set(diaryFilter)
    }

    private fun hasFilter() = viewModel.filter.get()
        .hasFilter()


    override fun onCancelDialog() {
        findNavController().popBackStack()
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        if (button == ButtonClicked.POSITIVE) {
            MdocAppHelper.getInstance().disclaimerSeenDiary = true
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenuWithBadge(menu, inflater, R.menu.menu_diary_dashboard, R.id.diaryFilter)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        updateBadge(menu, viewModel.filterCounter.value, R.id.diaryFilter)
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.diaryFilter) {
            openFilter()
        }
        else if (item.itemId == R.id.diaryExport) {
            openExport()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openFilter() {
        val filterDiaryFragment = FilterDiaryFragment().also {
            it.adapterItems = diaryConfig
            it.filter = viewModel.filter.get()
            it.setTargetFragment(this, 0)
        }
        filterDiaryFragment.show(parentFragmentManager, filterDiaryFragment.tag)
    }

    private fun openExport() {
        DiaryExportFragment().also {
            it.filter = viewModel.filter.get()
        }
            .show(parentFragmentManager, MentalGoalExportFragment::class.simpleName)
    }

    override fun onDeleteFileRequest(item: DiaryList) {
        deleteDialog(item)
    }

    override fun onEditFileRequest(item: DiaryList) {
        val pollId = if(item.diaryConfig?.pollId != null) item.diaryConfig?.pollId else ""
        val questionnaireAdditionalFields = QuestionnaireAdditionalFields(
                showFreeText = item.diaryConfig?.isFreeTextEntryAllowed!!,
                allowSharing = item.showToDoctor,
                autoShare = item.diaryConfig?.isAutoShare!!,
                isDiary = true,
                shouldAssignQuestionnaire = false,
                diaryConfigId = item.diaryConfig?.id,
                pollId = pollId,
                pollAssignId = null,
                pollVotingId = item.pollVotingActionId,
                diaryList = item)

        if(!item?.pollVotingActionId.isNullOrEmpty()) {
            val bottomSheetDialogFragment = DiaryBottomEditDialogFragment.newInstance(item, false)
            bottomSheetDialogFragment.show((activity as MdocActivity).supportFragmentManager, bottomSheetDialogFragment.tag)
        }else{
            val action = DiaryDashboardFragmentDirections.actionDiaryDashboardFragmentToDiaryEntryFragment(questionnaireAdditionalFields)
            activity?.findNavController(R.id.navigation_host_fragment)?.navigate(action)
        }

    }

    override fun onViewClickListner(item: DiaryList, isComingFromSearch: Boolean) {
    }

    fun deleteDialog(item: DiaryList) {

        var dialog = Dialog(context as MdocActivity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.add_popup_dilog)


        var cancel = dialog.findViewById<TextView>(R.id.cancel)
        var done = dialog.findViewById<TextView>(R.id.done)

        done!!.setOnClickListener {
        viewModel.requestDelete(item)
            dialog.dismiss()
        }

        cancel!!.setOnClickListener { dialog.dismiss() }
        dialog.show()

    }

}

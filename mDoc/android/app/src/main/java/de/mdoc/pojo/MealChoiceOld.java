package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 12/9/16.
 */

public class MealChoiceOld implements Serializable{
    private boolean completed;
    private String description;
    private String mealChoiceId;
    private String mealPlanId;
    private String userId;
    private WeeklyMealChoice weeklyMealChoice;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMealChoiceId() {
        return mealChoiceId;
    }

    public void setMealChoiceId(String mealChoiceId) {
        this.mealChoiceId = mealChoiceId;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public WeeklyMealChoice getWeeklyMealChoice() {
        return weeklyMealChoice;
    }

    public void setWeeklyMealChoice(WeeklyMealChoice weeklyMealChoice) {
        this.weeklyMealChoice = weeklyMealChoice;
    }
}

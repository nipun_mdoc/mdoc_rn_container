package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.modules.devices.data.FhirCodes
import de.mdoc.util.MdocUtil
import org.joda.time.LocalDate
import org.joda.time.LocalDateTime
import java.time.LocalTime

class Bm57Device(val gatt: BluetoothGatt, val data: ArrayList<Byte>) : BluetoothDevice {

    override fun getDeviceObservations(list: ArrayList<Byte>, deviceId: String,updateFrom: Long?): ArrayList<Observation> {

        val codingListOne = listOf(
                Coding("vital-signs",
                        "Vital Signs",
                        "http://terminology.hl7.org/CodeSystem/observation-category"
                      ))
        val codingListTwo = listOf(
                Coding(FhirCodes.BLOOD_PRESSURE.code,
                        "Blood pressure panel",
                        "http://loinc.org")
                                  )
        val categoryList = listOf(Category(codingListOne))

        val spoCode = Code(codingListTwo)

        val prCodingListTwo = listOf(
                Coding("368209003",
                        "Right arm",
                        "http://snomed.info/sct"
                      ))

        val bodySite = Code(prCodingListTwo)

        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var observedItem: Observation

        val readsByGroup = list.chunked(19)

        for (chunk in readsByGroup) {

            if (chunk.isNotEmpty()){
                val s1 = String.format("%02X", chunk[1])
                val s2 = String.format("%02X", chunk[2])
                val systolic = s2 + s1
                val sys = java.lang.Long.parseLong(systolic, 16)

                val d1 = String.format("%02X", chunk[3])
                val d2 = String.format("%02X", chunk[4])
                val diastolic = d2 + d1
                val dia = java.lang.Long.parseLong(diastolic, 16)

                val y1 = String.format("%02X", chunk[7])
                val y2 = String.format("%02X", chunk[8])
                val yy = y2+y1
                val year = java.lang.Long.parseLong(yy, 16)

                val mm = String.format("%02X", chunk[9])
                val month = java.lang.Long.parseLong(mm, 16)

                val dd = String.format("%02X", chunk[10])
                val day = java.lang.Long.parseLong(dd, 16)

                val hh = String.format("%02X", chunk[11])
                val hour = java.lang.Long.parseLong(hh, 16)

                val min = String.format("%02X", chunk[12])
                val minutes = java.lang.Long.parseLong(min, 16)

                val sec = String.format("%02X", chunk[13])
                val seconds = java.lang.Long.parseLong(sec, 16)

                val dateMilliseconds = MdocUtil.getTimeInMS(
                        year.toInt(),
                        month.toInt()-1,
                        day.toInt(),
                        hour.toInt(),
                        minutes.toInt(),
                        seconds.toInt()
                                                           )

                val innerCodingListOne = listOf(
                        Coding(FhirCodes.SYSTOLIC.code, "Systolic blood pressure", "http://loinc.org"),
                        Coding("271649006", "Systolic blood pressure", "http://snomed.info/sct"),
                        Coding("bp-s", "Systolic Blood pressure", "http://acme.org/devices/clinical-codes")
                                               )
                val code = Code(innerCodingListOne)
                val valueQuantity = ValueQuantity("mm[Hg]", "http://unitsofmeasure.org", "mmHg", sys.toDouble())
                val coding11 = Coding(FhirCodes.DIASTOLIC.code, "Diastolic blood pressure", "http://loinc.org")
                val codingList3 = listOf(coding11)
                val code2 = Code(codingList3)
                val valueQuantity2 = ValueQuantity(
                        "mm[Hg]",
                        "http://unitsofmeasure.org",
                        "mmHg",
                        dia.toDouble()
                                                  )
                val componentList = arrayListOf(
                        Component(code, valueQuantity),
                        Component(code2, valueQuantity2)
                                               )
                observedItem = Observation(categoryList,
                        spoCode,
                        Device("device/$deviceId"),
                        dateMilliseconds.toString(),
                        "Observation",
                        "FINAL",
                        bodySite,
                        componentList,
                        "blood-pressure",
                        "BM57"
                                          )
                val currentTime=LocalDateTime.now()
                currentTime.plusHours(3)

                observations.add(observedItem)

            }

        }
        return observations
    }

    override fun deviceAddress(): String {
        return gatt.device.address
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data
    }

    override fun deviceName(): String {
        return gatt.device.name
    }
}
package de.mdoc.data.create_bt_device

data class Identifier(
        val use: String,
        val value: String
)
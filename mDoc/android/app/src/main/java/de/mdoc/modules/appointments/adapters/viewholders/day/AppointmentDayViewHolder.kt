package de.mdoc.modules.appointments.adapters.viewholders.day

import android.content.Context
import android.view.View
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseDayAppointmentViewHolder
import de.mdoc.pojo.AppointmentDetails

class AppointmentDayViewHolder(itemView: View, private val isFromWidget: Boolean) : BaseDayAppointmentViewHolder(itemView), View.OnClickListener {

    init {
        _dayItemPlaceHolder.setOnClickListener(this)
    }

    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        super.bind(item, context, appointmentCallback, hasHeader)

        _txtMetaDataStatus.visibility = View.GONE
        _txtUpdateMessage.visibility = View.VISIBLE

        prepareUI(item, context, isFromWidget)
    }

    override fun onClick(view: View?) {
        when (view) {
            _dayItemPlaceHolder -> {
                showAppointmentDetailsWithTabletCheck(view)
            }
        }
    }
}
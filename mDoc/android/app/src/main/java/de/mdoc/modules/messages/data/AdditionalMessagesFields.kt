package de.mdoc.modules.messages.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AdditionalMessagesFields(val isClosedConversation: Boolean = false,
                                    val conversationId: String? = null,
                                    val letterboxTitle: String? = "",
                                    val reasonTitle: String? = "",
                                    val isNewMessage: Boolean = false,
                                    val isPublic: Boolean = false,
                                    val replyAllowed: Boolean = false,
                                    val recipient: String? = "",
                                    val letterbox: String? = ""): Parcelable

package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.mdoc.modules.medications.create_plan.data.ListItem
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_
import de.mdoc.pojo.entertainment.MediaLightDTO

class MagazineFilterSharedViewModel : ViewModel() {

    // Original data from API
    var categories: List<ListItem?>? = emptyList()
    var languages: List<ListItem?>? = emptyList()

    // Working copy of API data
    var categoriesTemp = MutableLiveData<List<ListItem?>?>(emptyList())
    var languagesTemp = MutableLiveData<List<ListItem?>?>(emptyList())

    // Counter of selected filter options
    var filterCounter = MutableLiveData(0)

    fun applyFilter() {
        categories = categoriesTemp.value
        languages = languagesTemp.value
        filterCounter.value = getActiveFilterCount()
    }

    fun clearAll() {
        categoriesTemp.value?.forEach { it -> it?.activatedByUser = false }
        languagesTemp.value?.forEach { it -> it?.activatedByUser = false }
        languagesTemp.value = languagesTemp.value
    }

    private fun getActiveFilterCount(): Int {
        val counterLang = languages?.filter { it?.activatedByUser == true }?.size ?: 0
        val counterCategories = categories?.filter { it?.activatedByUser == true }?.size ?: 0
        return counterLang.plus(counterCategories)
    }

    fun applyTempData() {
        val jsonLang = Gson().toJson(languages)
        val listTypeLang = object : TypeToken<MutableList<ListItem>>() { }.type
        languagesTemp.value =  Gson().fromJson<MutableList<ListItem>>(jsonLang, listTypeLang)
        
        val jsonCategories = Gson().toJson(categories)
        val listTypeCategories = object : TypeToken<MutableList<ListItem>>() { }.type
        categoriesTemp.value =  Gson().fromJson<MutableList<ListItem>>(jsonCategories, listTypeCategories)

    }

    private fun getActiveLanguages(): List<String> {
        return languages?.filter { it -> it?.activatedByUser == true && it.code != null }?.map { it?.code.toString() }!!
    }

    fun getActiveCategoriesQuery(): String {
        val list  = categories?.filter { it -> it?.activatedByUser == true && it.code != null }?.map { it?.code.toString() }!!
        return list.joinToString(separator = ",")
    }

    fun getActiveLanguagesQuery(): String {
        val list = languages?.filter { it -> it?.activatedByUser == true && it.code != null }?.map { it?.code.toString() }!!
        return list.joinToString(separator = ",")
    }

    fun hasActiveLanguages(): Boolean {
        val size = getActiveLanguages()
        return when (size.size) {
            0 -> false
            else -> true
        }
    }

    fun filterLanguages(input: ResponseSearchResultsMediaLightDTO_?): Array<MediaLightDTO>? {
        val lang = getActiveLanguages()
        val filtered = input?.data?.list?.filter { it -> lang.contains(it.language) }
        return filtered?.toTypedArray()
    }
}
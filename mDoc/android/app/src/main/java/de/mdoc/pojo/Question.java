package de.mdoc.pojo;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ema on 12/29/16.
 */

public class Question implements Serializable {
    protected int index;
    protected String uuid;
    protected String pollQuestionType;
    protected String pollQuestionExternalType;
    protected String ordinal;
    protected Boolean showContent;
    protected String userId;
    protected String pollSectionId;
    protected String pollId;
    protected String pollAssignId;
    protected String id;
    protected QuestionData questionData;
    protected ArrayList<String> childQuestionsIds;
    protected String parentQuestionId;
    protected Object showIf;
    protected String pollQuestionTextType;
    protected Boolean required;
    protected String selectedLanguage;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public void setPollQuestionExternalType(String pollQuestionExternalType) {
        this.pollQuestionExternalType = pollQuestionExternalType;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public ArrayList<String> getChildQuestionsIds() {
        return childQuestionsIds;
    }

    public void setChildQuestionsIds(ArrayList<String> childQuestionsIds) {
        this.childQuestionsIds = childQuestionsIds;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getPollQuestionType() {
        return pollQuestionType;
    }

    public void setPollQuestionType(String pollQuestionType) {
        this.pollQuestionType = pollQuestionType;
    }

    public String getPollQuestionExternalType() {
        return pollQuestionExternalType;
    }

    public String getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(String ordinal) {
        this.ordinal = ordinal;
    }

    public Boolean getShowContent() {
        return showContent;
    }

    public void setShowContent(Boolean showContent) {
        this.showContent = showContent;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPollSectionId() {
        return pollSectionId;
    }

    public void setPollSectionId(String pollSectionId) {
        this.pollSectionId = pollSectionId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public QuestionData getQuestionData() {
        return questionData;
    }

    public void setQuestionData(QuestionData questionData) {
        this.questionData = questionData;
    }

    public String getParentQuestionId() {
        return parentQuestionId;
    }

    public void setParentQuestionId(String parentQuestionId) {
        this.parentQuestionId = parentQuestionId;
    }

    public Object getShowIf() {
        return showIf;
    }

    public void setShowIf(Object showIf) {
        this.showIf = showIf;
    }

    public String getPollQuestionTextType() {
        return pollQuestionTextType;
    }

    public void setPollQuestionTextType(String pollQuestionTextType) {
        this.pollQuestionTextType = pollQuestionTextType;
    }

    public String getDescription () {
        return questionData.getDescription(selectedLanguage);
    }

    public String getQuestion () {
        return questionData.getQuestion(selectedLanguage);
    }

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public String getScaleBeginningText () {
        return questionData.getScaleBeginningText(selectedLanguage);
    }

    public String getScaleEndText () {
        return questionData.getScaleEndText(selectedLanguage);
    }

    public ArrayList<String> parseShowIf () throws JSONException, NullPointerException {
        ArrayList<String> parsedData = new ArrayList<>();

        if (getShowIf() instanceof String) {
            parsedData.add(getShowIf().toString());
        } else {
            JSONArray jsonArray = new JSONArray((ArrayList) getShowIf());
            for (int i = 0; i < jsonArray.length(); i++) {
                parsedData.add(jsonArray.get(i).toString());
            }
        }
        return parsedData;
    }
}

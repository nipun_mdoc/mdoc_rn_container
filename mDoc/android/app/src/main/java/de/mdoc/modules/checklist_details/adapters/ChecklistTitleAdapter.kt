package de.mdoc.modules.checklist_details.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.blogc.android.views.ExpandableTextView
import com.mikhaellopez.circularprogressbar.CircularProgressBar
import de.mdoc.R
import de.mdoc.interfaces.ChecklistListener
import de.mdoc.modules.checklist_details.adapters.ChecklistTitleAdapter.Companion.defaultLanguageTemp
import de.mdoc.pojo.ShortQuestion
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_checklist_title_item.view.*


class ChecklistTitleAdapter (val items: ArrayList<ShortQuestion>, val optionsRv : RecyclerView, val descriptionTxt : ExpandableTextView , val pollVotingActionId : String , val defaultLanguage : String):
        RecyclerView.Adapter<TitleViewHolder>(), ChecklistListener {

    var tempPos = 0
    var holder : TitleViewHolder? = null
    var context : Context? = null


    companion object{
        var defaultLanguageTemp : String? = null
        var pollVotingId : String? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): TitleViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_checklist_title_item, parent, false)
        context = parent.context

        updateDefaultLanguage(defaultLanguage)

        return TitleViewHolder(view)
    }

    override fun onBindViewHolder(holder: TitleViewHolder, position: Int) {
        val item = items[position]

        holder.updateCategoryTitles(item)
        updatePollVotingId(pollVotingActionId)

        if (holder.adapterPosition == tempPos) {
            holder.titleCheckDetailsRl.background = context?.let { ContextCompat.getDrawable(it, R.drawable.checklist_rounded_stroke) }
            optionsRv.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            val choicesAdapter = ChecklistChoicesAdapterKt(item.options, item, pollVotingId ?: "", defaultLanguage)
            choicesAdapter.setChecklistListener(this)
            this.holder = holder
            optionsRv.adapter = choicesAdapter
            descriptionTxt.text = item.getDescription(defaultLanguage)

        } else {
            holder.titleCheckDetailsRl.background = context?.let { ContextCompat.getDrawable(it, R.drawable.checklist_rounded) }
        }

        holder.titleCheckDetailsRl.setOnClickListener {
            tempPos = holder.adapterPosition
            notifyDataSetChanged()
        }

        descriptionTxt.setOnClickListener(View.OnClickListener {
            if (descriptionTxt.isExpanded) {
                descriptionTxt.collapse()
            } else {
                descriptionTxt.expand()
            }
        })

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun updateList(id: String?) {
        for (i in items.indices) {
            if (items[i].id == id) {
                holder?.updateCategoryTitles(items[i])
            }
        }
    }

    override fun updatePollVotingId(id: String?) {
        pollVotingId = id
    }

    fun updateDefaultLanguage(lang: String?) {
        defaultLanguageTemp = lang
    }

}

class TitleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val title: TextView = itemView.txtCategoryTitle
    val total: TextView = itemView.totalTxt
    val completedTv: TextView = itemView.completedTxt
    val circularProgressBar: CircularProgressBar = itemView.circularView
    var titleCheckDetailsRl: RelativeLayout = itemView.titleCheckDetailsRl
    val imageCheckComplete: ImageView = itemView.imageCheckComplete

    fun updateCategoryTitles(item: ShortQuestion) {

        var comp = 0

        for (oe in item.options) {
            if (oe.answer != null) {
                comp++
            }
        }

        if (comp == item.options.size) {
            imageCheckComplete.visibility = View.VISIBLE
            circularProgressBar.visibility = View.GONE
        } else {
            imageCheckComplete.visibility = View.GONE
            circularProgressBar.visibility = View.VISIBLE
        }

        title.text = item.getQuestion(defaultLanguageTemp)
        total.text = item.options.size.toString() + ""
        completedTv.text = comp.toString() + ""
        circularProgressBar.progress = MdocUtil.calculateProgress(comp, item.options.size).toFloat()
    }
}
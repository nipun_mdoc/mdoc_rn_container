package de.mdoc.modules.covid19.history

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.modules.profile.data.Contact
import de.mdoc.pojo.PublicUserDetails
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class TestHistoryViewModel: ViewModel() {
    var allHealthStatus: MutableLiveData<List<HealthStatus>> = MutableLiveData(listOf())
}
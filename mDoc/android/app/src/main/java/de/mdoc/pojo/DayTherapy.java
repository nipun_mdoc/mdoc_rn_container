package de.mdoc.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ema on 11/15/16.
 */

public class DayTherapy implements Serializable{

    private int day;
    private boolean changed;
    private boolean newTherapy;
    private List<Appointment> appointments;


    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public boolean isNewTherapy() {
        return newTherapy;
    }

    public void setNewTherapy(boolean newTherapy) {
        this.newTherapy = newTherapy;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }
}

package de.mdoc.modules.medications.create_plan

import android.annotation.SuppressLint
import android.app.TimePickerDialog
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputEditText
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentWhatTimeBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.interfaces.TimePeriodListener
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import de.mdoc.modules.medications.create_plan.data.ListItems
import de.mdoc.storage.AppPersistence
import kotlinx.android.synthetic.main.fragment_what_time.*
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat

class WhatTimeFragment internal constructor(val viewModel: CreatePlanViewModel): NewBaseFragment(),
        TimePeriodListener {

    private var selectedDateTime = LocalDateTime.now()
    private val timeFormatter = DateTimeFormat.forPattern("HH:mm")
    lateinit var activity: MdocActivity
    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentWhatTimeBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_what_time, container, false)
        val frag = this.parentFragment as MedicationPlanFragment?
        frag?.setTimeListener(this@WhatTimeFragment)
        activity = context as MdocActivity
        binding.lifecycleOwner = this
        binding.createPlanViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        add_another_time?.setOnClickListener {
            val hour = String.format("%02d", LocalDateTime.now().hourOfDay)
            val minute = String.format("%02d", LocalDateTime.now().minuteOfHour)
            val itemId = viewModel.additionalDailyPeriod.value?.size ?: 0
            viewModel.additionalDailyPeriod.value?.add(
                    ListItems(text = itemId.toString(),
                            code = "$hour:$minute"))
            val listId = viewModel.additionalDailyPeriod.value?.size?.minus(1) ?: -1

            addMeasurementPlaceholder(
                    ListItems(text = listId.toString(),
                            code = "$hour:$minute"))
        }

        viewModel.isShowReminder.observe(viewLifecycleOwner, Observer {
            if (it) {
                sw_reminder?.text = resources.getString(R.string.med_plan_active)
                sw_reminder?.setTextColor(ContextCompat.getColor(activity, R.color.black_222222))
                selectedClock()
            }
            else {
                sw_reminder?.text = resources.getString(R.string.med_plan_deactivated)
                sw_reminder?.setTextColor(ContextCompat.getColor(activity, R.color.new_text_grey))
                deselectedClock()
            }
        })
    }

    override fun timePeriodSelected() {
        removeElements()
        AppPersistence.timeCoding?.forEach {
            if (it.selected) {
                addMeasurementPlaceholder(it)
            }
        }
        viewModel.additionalDailyPeriod.value?.forEach {
            addMeasurementPlaceholder(it)
        }
    }

    @SuppressLint("InflateParams")
    private fun addMeasurementPlaceholder(item: ListItems) {
        val childView =
                LayoutInflater.from(context)
                    .inflate(R.layout.placeholder_intake_time, null)
        val tietTime = childView.findViewById<TextInputEditText>(R.id.tiet_time)
        tietTime?.setText(item.code)
        time_placeholder?.addView(childView)
        tietTime?.setOnClickListener {
            showTimePicker(tietTime, item)
        }
    }

    private fun removeElements() {
        time_placeholder?.removeAllViews()
    }

    private fun showTimePicker(tietTime: TextInputEditText, item: ListItems) {
        val hourFormat = DateTimeFormat.forPattern("HH:mm")
        val currentSelection = hourFormat.parseLocalTime(item.code)
        val timePickerDialog = TimePickerDialog(
                activity, TimePickerDialog.OnTimeSetListener
        { _, hourOfDay, minute ->
            selectedDateTime = selectedDateTime.withHourOfDay(hourOfDay)
                .withMinuteOfHour(minute)

            tietTime.setText(timeFormatter.print(selectedDateTime))

            viewModel.hourEdited(item, selectedDateTime)
        },
                currentSelection.hourOfDay,
                currentSelection.minuteOfHour,
                true
                                               )
        timePickerDialog.show()
    }

    private fun selectedClock() {
        sw_reminder?.compoundDrawables?.forEach {
            it?.let {
                it.colorFilter =
                        PorterDuffColorFilter(ContextCompat.getColor(sw_reminder.context, R.color.black_222222),
                                PorterDuff.Mode.SRC_IN)
            }
        }
    }

    private fun deselectedClock() {
        sw_reminder?.compoundDrawables?.forEach {
            it?.let {
                it.colorFilter =
                        PorterDuffColorFilter(ContextCompat.getColor(sw_reminder.context, R.color.new_text_grey),
                                PorterDuff.Mode.SRC_IN)
            }
        }
    }

    companion object {
        val TAG = WhatTimeFragment::class.java.canonicalName
    }
}

package de.mdoc.modules.diary

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.mdoc.data.diary.Data
import de.mdoc.modules.diary.data.DiaryFilter
import de.mdoc.modules.diary.data.DiarySearchRequest
import de.mdoc.modules.diary.data.RxDiaryRepository
import de.mdoc.pojo.DiaryList
import de.mdoc.pojo.DiaryListUpdate
import de.mdoc.pojo.deleteDiaryPjo
import de.mdoc.viewmodel.combine
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class RxDiaryViewModel(private val repository: RxDiaryRepository?=null): ViewModel() {

    val filter = liveData(
            DiaryFilter(
                    categories = null,
                    from = null,
                    to = null
                       )
                                      )
    val isLoading = liveData(false)
    val isShowLoadingError = liveData(false)
    private val allDiaries = liveData<List<DiaryList>>()
    val isShowContent = liveData(false)
    val isShowEmptyMessage = liveData(false)
    val diaryConfig = liveData<List<Data>>()
    val diaryItems = combine(allDiaries, filter) { list, diaryFilter ->
        list.filter(diaryFilter::isMatch)
    }

    // Counter of selected filter options
    var filterCounter = MutableLiveData(0)
    var shouldShowDeleteMsg = MutableLiveData(false)

    init {
        isLoading.set(true)
        val request = DiarySearchRequest(skip = 0, limit = 9999)
        repository?.getAllDiaries(request, ::onDataLoaded, ::onError)
        repository?.diaryConfiguration(::onConfigLoaded, ::onError)
    }

     fun onDataLoaded(result: List<DiaryList>) {
        isLoading.set(false)
        if (result.isNotEmpty()) {
            allDiaries.set(result)
            isShowContent.set(true)
            isShowEmptyMessage.set(false)
        }
        else {
            isShowEmptyMessage.set(true)
            isShowContent.set(false)
        }
    }

    fun onDataRefresh(data: List<deleteDiaryPjo>?) {
        val request = DiarySearchRequest(skip = 0, limit = 9999)
        repository?.getAllDiaries(request, ::onDataLoaded, ::onError)
        repository?.diaryConfiguration(::onConfigLoaded, ::onError)
        shouldShowDeleteMsg.value = true
        Handler().postDelayed({
            shouldShowDeleteMsg.value = false
        }, 6000)
    }


    private fun onConfigLoaded(result: List<Data>) {
        if (result.isNotEmpty()) {
            diaryConfig.set(result)
        }
    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        isLoading.set(false)
        isShowLoadingError.set(true)
    }

     fun requestDelete(query: DiaryList) {
        repository?.getDiariesDeleteRequest(id = query.id , resultListener = ::onDataRefresh, errorListener = ::onError)
    }

     fun requestUpdate(query: DiaryList) {
        var request = DiaryListUpdate()
        request.caseId=query.id
        request.clinicId=query.clinicId
        request.description=query.description
        request.diaryConfig=query.diaryConfig.toString()
        request.showToDoctor= query.showToDoctor
        request.start=query.start
        request.startTime=0
        request.title=query.title

        repository?.getDiariesUpdateRequest(id = query.id,request = request, resultListener = ::onDataRefresh, errorListener = ::onError)
    }

    override fun onCleared() {
        super.onCleared()
        repository?.recycle()
    }
}
package de.mdoc.modules.devices.available_devices

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.R

class DevicePagerAdapter(fm: FragmentManager?,val context: Context?,val fragments:List<Fragment>) :
        FragmentStatePagerAdapter(fm!!){

    override fun getItem(position: Int): Fragment {

        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (fragments[position] is AvailableVitalFragment){
            context?.resources?.getString(R.string.vital)
        }else{
            context?.resources?.getString(R.string.medical
            )
        }
    }
}

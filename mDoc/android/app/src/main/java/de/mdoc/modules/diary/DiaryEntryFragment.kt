package de.mdoc.modules.diary

import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.gson.JsonObject
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.DiaryResponse
import de.mdoc.pojo.DiaryList
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.diary_entry_fragment.*
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

class DiaryEntryFragment: MdocFragment() {

    var diaryList: DiaryList? = null
    var pollVotingAction = ""
    var showFreeText = true
    var autoShare = true
    var allowSharing = true
    var diaryConfigId = ""
    var isTitleChanged = false
    var isDescriptionChanged = false
    var isDateChanged = false
    var isTimeChanged = false
    var isShareChanged = false
    var selectedDate = ""
    var selectedTime = ""
    private var selectedDateTime = LocalDateTime.now()
    private val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    private val timeFormatter = DateTimeFormat.forPattern("HH:mm")
    private val args: DiaryEntryFragmentArgs by navArgs()
    override fun setResourceId(): Int {
        return R.layout.diary_entry_fragment
    }

    override val navigationItem: NavigationItem = NavigationItem.Diary

    override fun init(savedInstanceState: Bundle?) {
        // do nothing
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dateEntryTv?.text = dateFormatter.print(selectedDateTime)
        timeTv?.text = timeFormatter.print(selectedDateTime)

        setFieldsFromBundle()
        if(diaryList != null){
            btnSaveEntry.isEnabled = false
        }

        checkCoachShare()

        titleEt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isTitleChanged = diaryList != null && !((diaryList?.title?.trim()).equals(s.toString().trim())!!)
                checkDataChange()
            }
        })
        commentEdt.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                isDescriptionChanged = diaryList != null && !((diaryList?.description?.trim()).equals(s.toString().trim())!!)
                checkDataChange()
            }
        })

        shareCb.setOnCheckedChangeListener { buttonView, isChecked ->
            isShareChanged = diaryList != null && !((diaryList?.showToDoctor) == isChecked!!)
            checkDataChange()
        }

        btnSaveEntry?.setOnClickListener {
            if (showFreeText && pollVotingAction.isEmpty() && commentEdt.text.trim().isEmpty() && titleEt.text.trim().isEmpty()) {
                MdocUtil.showToastLong(activity, getString(R.string.comment_field_must_not_be_empty))
            }
            else {
                var body = JsonObject()

                if(diaryList != null){
                    body.addProperty("clinicId", diaryList?.clinicId!!)
                    body.addProperty("showToDoctor", shareCb?.isChecked)
                    body.addProperty("description", commentEdt?.text.toString().trim())
                    body.addProperty("title", titleEt?.text.toString().trim())
                    body.addProperty("start", selectedDateTime.toDateTime().millis)
                    body.addProperty("caseId", MdocAppHelper.getInstance().caseId)
                    body.addProperty("diaryConfigId", diaryList?.diaryConfig!!.id)
                    body.addProperty("pollVotingActionId", if(diaryList?.pollVotingActionId != null) diaryList?.pollVotingActionId else "" )
                    body.addProperty("ownerUsername", diaryList?.ownerUsername!!)
                    updateDiary(body,diaryList?.id!!)
                }else{
                    body.addProperty("clinicId", MdocAppHelper.getInstance().clinicId)
                    body.addProperty("showToDoctor", shareCb?.isChecked)
                    body.addProperty("description", commentEdt?.text.toString().trim())
                    body.addProperty("title", titleEt?.text.toString().trim())
                    body.addProperty("start", selectedDateTime.toDateTime().millis)
                    body.addProperty("caseId", MdocAppHelper.getInstance().caseId)
                    body.addProperty("diaryConfigId", diaryConfigId)
                    body.addProperty("pollVotingActionId", pollVotingAction)
                    body.addProperty("ownerUsername", MdocAppHelper.getInstance().username)
                    createDiary(body)
                }
            }
        }
        var datePickerDialog = DatePickerDialog.newInstance(
                { _, year, monthOfYear, dayOfMonth ->
                    selectedDateTime = selectedDateTime.withDate(year, monthOfYear + 1, dayOfMonth)
                    dateEntryTv?.text = dateFormatter.print(selectedDateTime)
                    isDateChanged = diaryList != null && !(selectedDate.equals(dateFormatter.print(selectedDateTime)))
                    checkDataChange()
                },
                selectedDateTime.year,
                selectedDateTime.monthOfYear - 1,
                selectedDateTime.dayOfMonth
                                                           )
        datePickerDialog.version = DatePickerDialog.Version.VERSION_2
        datePickerDialog.maxDate = Calendar.getInstance()

        dateEntryTv.setOnClickListener {
            datePickerDialog.show(parentFragmentManager, "Datepickerdialog")
        }

        timeTv?.setOnClickListener {
            showTimePicker()
        }

    }

    private fun showTimePicker() {
        val timePickerDialog = TimePickerDialog(
                activity, TimePickerDialog.OnTimeSetListener
        { _, hourOfDay, minute ->
            selectedDateTime = selectedDateTime.withHourOfDay(hourOfDay)
                    .withMinuteOfHour(minute)

            timeTv?.text = timeFormatter.print(selectedDateTime)
            isTimeChanged = diaryList != null && !(selectedTime.equals(timeFormatter.print(selectedDateTime)))
            checkDataChange()
        },
                selectedDateTime.hourOfDay,
                selectedDateTime.minuteOfHour,
                true

        )

        timePickerDialog.show()
    }

    private fun setFieldsFromBundle() {

        diaryList = args.questionnaireAdditionalFields?.diaryList ?: null

        if(diaryList != null){
            titleEt.setText(diaryList?.title)
            commentEdt.setText(diaryList?.description)
            dateEntryTv.setText("" + diaryList?.start)
            shareCb?.isChecked = diaryList?.showToDoctor!!
            shareCb?.isEnabled = !(diaryList?.showToDoctor!!)

            var timestamp = Timestamp(diaryList?.start!!)
            var date = Date(timestamp.getTime())
            val simpleDateFormat = SimpleDateFormat("dd.MM.yyyy")
            val simpleTimeFormat = SimpleDateFormat("HH:mm")
            selectedDate = ""+simpleDateFormat.format(date)
            selectedTime = ""+simpleTimeFormat.format(timestamp)
            dateEntryTv?.setText(""+simpleDateFormat.format(date))
            timeTv?.setText(""+simpleTimeFormat.format(timestamp))
        }else{
            setLayoutBasedOnConfig()
            showFreeText = args.questionnaireAdditionalFields?.showFreeText ?: false
            autoShare = args.questionnaireAdditionalFields?.autoShare ?: false
            allowSharing = args.questionnaireAdditionalFields?.allowSharing ?: false
            diaryConfigId = args.questionnaireAdditionalFields?.diaryConfigId ?: ""
            pollVotingAction = args.questionnaireAdditionalFields?.pollVotingId ?: ""
        }
    }

    private fun  checkDataChange(){
        if(diaryList != null){
            btnSaveEntry.isEnabled = isTitleChanged || isDescriptionChanged || isDateChanged || isTimeChanged || isShareChanged
        }else{
            btnSaveEntry.isEnabled = true
        }
    }

    private fun setLayoutBasedOnConfig() {
        if (!showFreeText) {
            layoutAddComment.visibility = View.GONE
        }

        if (!autoShare && !allowSharing) {
            shareCb?.visibility = View.INVISIBLE
        }

        if (autoShare) {
            shareCb?.isChecked = true
        }
        if (!allowSharing) {
            shareCb?.isEnabled = false
        }
    }

    private fun createDiary(body: JsonObject) {
        MdocManager.createDiary(body, object : Callback<DiaryResponse> {
            override fun onResponse(call: Call<DiaryResponse>, response: Response<DiaryResponse>) {
                findNavController().navigate(R.id.navigation_diary)
            }

            override fun onFailure(call: Call<DiaryResponse>, t: Throwable) {
                findNavController().navigate(R.id.navigation_diary)
            }
        })
    }

    private fun updateDiary(body: JsonObject,id :String) {
        MdocManager.updateDiary(id,body, object : Callback<DiaryResponse> {
            override fun onResponse(call: Call<DiaryResponse>, response: Response<DiaryResponse>) {
                findNavController().navigate(R.id.navigation_diary)
            }

            override fun onFailure(call: Call<DiaryResponse>, t: Throwable) {
                findNavController().navigate(R.id.navigation_diary)
            }
        })
    }

    private fun checkCoachShare(){
        if (resources.getBoolean(R.bool.hide_coach_share)){
            shareCb?.visibility = View.GONE
        } else {
            shareCb?.visibility = View.VISIBLE
        }
    }
}
package de.mdoc.data.media

import java.util.*

data class MediaStatisticsData(
        val category: String,
        val content: ArrayList<Content>,
        val countByType: TreeMap<String, Int>,
        val cts: Long,
        val id: String,
        val initial: Boolean,
        val uts: Long
)
package de.mdoc.util

import android.content.res.Resources
import androidx.core.graphics.ColorUtils
import de.mdoc.R

class ColorUtil {
    companion object {

        fun contrastColor(resources: Resources, baseColor: Int = R.color.colorPrimary): Int {
            val result = ColorUtils.calculateLuminance(resources.getColor(baseColor))
            return if (result < 0.5) {
                resources.getColor(R.color.white)
            } else {
                resources.getColor(R.color.black_222222)
            }
        }
    }
}
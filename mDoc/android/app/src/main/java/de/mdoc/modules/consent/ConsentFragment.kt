package de.mdoc.modules.consent

import android.annotation.SuppressLint
import android.net.http.SslError
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.SslErrorHandler
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.NewBaseFragment
import kotlinx.android.synthetic.main.webview_fragment.*

class ConsentFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Consent

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.webview_fragment, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.javaScriptCanOpenWindowsAutomatically = true
        webView?.settings?.defaultTextEncodingName = TEXT_ENCODING
        webView?.requestFocus(View.FOCUS_DOWN)
        webView?.loadUrl(CONSENT_URL)
        //TODO commented for appStore, uncomment later
//        webView?.webViewClient = object: WebViewClient() {
//            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
//                view.loadUrl(request.toString())
//                return true
//            }
//
//            //App rejected from fore because of this, need to add it for cerner
//            override fun onReceivedSslError(view: WebView,
//                                            handler: SslErrorHandler,
//                                            error: SslError) {
//                if (BuildConfig.FLAV == "cerner") {
//                    handler.proceed() // Ignore SSL certificate errors
//                }
//            }
//        }
    }

    companion object {
        const val CONSENT_URL = "https://demo.thieme-compliance.de/ecpm/workflow/patient"
        const val TEXT_ENCODING = "utf-8"
    }
}
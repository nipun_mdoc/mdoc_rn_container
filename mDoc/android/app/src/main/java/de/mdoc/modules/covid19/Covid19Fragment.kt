package de.mdoc.modules.covid19

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentCovid19Binding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment.Companion.NOT_TESTED
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_covid19.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class Covid19Fragment: NewBaseFragment(), CovidStatusChangeBottomSheetFragment.StatusChangeCallback {
    lateinit var activity: MdocActivity
    override val navigationItem: NavigationItem = NavigationItem.HealthStatus
    private val covid19ViewModel by viewModel {
        Covid19ViewModel(
                RestClient.getService())
    }
    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private var covidStatusFragment = CovidStatusChangeBottomSheetFragment()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentCovid19Binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_covid19, container, false)
        binding.lifecycleOwner = this
        binding.handler = Covid19Handler()
        binding.covid19ViewModel = covid19ViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        llChangeStatus?.setOnClickListener {
            openBottomDialogChangeStatus()
        }

        txtTestResults?.setOnClickListener {
            if (covid19ViewModel.healthStatus.value?.healthStatus?.equals(NOT_TESTED) == true) {
                openBottomDialogChangeStatus(true)
            }
        }
        setRadioListener()
    }

    private fun setRadioListener() {
        radioIll.setOnClickListener {
            val request = covid19ViewModel.healthStatus.value.also {
                it?.ill = true
                it?.lastUpdate = formatter.print(DateTime.now())
            } ?: HealthStatus()
            covid19ViewModel.postHealthStatus(request)
        }
        radioNotIll.setOnClickListener {
            val request = covid19ViewModel.healthStatus.value.also {
                it?.ill = false
                it?.lastUpdate = formatter.print(DateTime.now())
            } ?: HealthStatus()
            covid19ViewModel.postHealthStatus(request)
        }
        radioYes.setOnClickListener {
            val request = covid19ViewModel.healthStatus.value.also {
                it?.recovered = true
                it?.lastUpdate = formatter.print(DateTime.now())
            } ?: HealthStatus()
            covid19ViewModel.postHealthStatus(request)
        }

        radioNo.setOnClickListener {
            val request = covid19ViewModel.healthStatus.value.also {
                it?.recovered = false
                it?.lastUpdate = formatter.print(DateTime.now())
            } ?: HealthStatus()
            covid19ViewModel.postHealthStatus(request)
        }
    }

    private fun openBottomDialogChangeStatus(isUpdate: Boolean = false) {
        covidStatusFragment.healthStatusId = if (isUpdate) {
            covid19ViewModel.healthStatus.value?.id
        }
        else {
            null
        }
        covidStatusFragment.setStatusChangeListener(this@Covid19Fragment)
        covidStatusFragment.showNow(activity.supportFragmentManager, "")
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_health_status)
        val changeTestItem = menu.findItem(R.id.changeTest)
        if (!MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_COVID19, MdocConstants.CAN_ADD_TEST)) {
            changeTestItem.isVisible = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.changeTest) {
            openBottomDialogChangeStatus()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStatusChangeClick(healthStatus: HealthStatus) {
        covid19ViewModel.postHealthStatus(healthStatus)
    }
}
package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;


public class FreeSlotsAppointmentDetails implements Serializable {
    @SerializedName("resourceType")
    @Expose
    private String resourceType;
    @SerializedName("specialtyName")
    @Expose
    private String specialityName;
    @SerializedName("specialty")
    @Expose
    private String specialty;
    @SerializedName("identifiers")
    @Expose
    private ArrayList<Object> identifiers = null;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("start")
    @Expose
    private long start;
    @SerializedName("end")
    @Expose
    private long end;
    @SerializedName("visible")
    @Expose
    private Boolean visible;
    @SerializedName("timeVisible")
    @Expose
    private Boolean timeVisible;
    @SerializedName("participants")
    @Expose
    private ArrayList<FreeSlotParticipants> participants;
    @SerializedName("clinicId")
    @Expose
    private String clinicId;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("showEndTime")
    @Expose
    private boolean isShowEndTime;
    @SerializedName("online")
    private boolean online;

    public String getSpecialityName() {
        return specialityName;
    }

    public void setSpecialityName(String specialityName) {
        this.specialityName = specialityName;
    }

    public boolean isShowEndTime() {
        return isShowEndTime;
    }

    public void setShowEndTime(boolean showEndTime) {
        isShowEndTime = showEndTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public ArrayList<Object> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(ArrayList<Object> identifiers) {
        this.identifiers = identifiers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getTimeVisible() {
        return timeVisible;
    }

    public void setTimeVisible(Boolean timeVisible) {
        this.timeVisible = timeVisible;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(Boolean online) {
        this.online = online;
    }

    public ArrayList<FreeSlotParticipants> getParticipants() {
        return participants;
    }

    public void addParticipant(FreeSlotParticipants freeSlotParticipants) {
        participants.add(freeSlotParticipants);
    }

    public boolean hasGuest() {
        if (participants != null && participants.size() > 0) {
            for (int i = 0; i < participants.size(); i++) {
                if (participants.get(i).getType().contains("GUEST")) {
                    return true;
                }
            }
        }

        return false;
    }

    public void setParticipants(ArrayList<FreeSlotParticipants> participants) {
        this.participants = participants;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }
}

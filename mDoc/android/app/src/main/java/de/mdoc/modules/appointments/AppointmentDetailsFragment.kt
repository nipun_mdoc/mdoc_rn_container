package de.mdoc.modules.appointments

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.PopupMenu
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MainActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.constants.MdocConstants.RESCHEDULING_CONFIRMATION
import de.mdoc.databinding.FragmentTherapyDetailsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.appointments.video_conference.VideoConferenceFragment
import de.mdoc.modules.booking.confirm_booking.OnlineTooltipBottomSheetFragment
import de.mdoc.modules.profile.patient_details.PatientDetailsActivity
import de.mdoc.modules.profile.preview.PreviewMetadataActivity
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.network.response.MdocResponse
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.pojo.NewAppointmentSchedule
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.util.ProgressDialog
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.therapy_details_layout.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AppointmentDetailsFragment: NewBaseFragment(), View.OnClickListener {


    lateinit var activity: MdocActivity
    internal var progressDialog: ProgressDialog? = null
    override val navigationItem: NavigationItem = NavigationItem.Therapy

    private var refreshHandler: Handler? = null
    private var rescheduledAppointment = NewAppointmentSchedule()
    private var appointmentDescription : String = ""

    private val args: AppointmentDetailsFragmentArgs by navArgs()

    private val appointmentDetailsViewModel by viewModel {
        AppointmentDetailsViewModel(
                RestClient.getService(), args.bookingAdditionalFields, args.appointmentDetails)
    }
    private var onlineTooltipDialog = OnlineTooltipBottomSheetFragment()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentTherapyDetailsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_therapy_details, container, false)
        binding.appointmentDetails = args.appointmentDetails
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnMetaData.setOnClickListener(this)
        isVideoConferenceVisibleRecursive()

        progressDialog = ProgressDialog(activity)
        progressDialog?.setCancelable(false)

        if (args.appointmentDetails.state == RESCHEDULING_CONFIRMATION) {
            updatedAppointment()
        }

        setAppointmentInfo(args.appointmentDetails)
        setUpdateTime()

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.APPOINTMENT_EDIT_ICON_ENABLED)) {
            if (args.appointmentDetails.appointmentType != null && args.appointmentDetails.appointmentType == "IN_PERSON") {
                fab_edit?.setOnClickListener {
                    getSpecialty()
                    showPopupMenu(it)
                }
                fab_edit?.show()
            }
        }
        pinIv?.setOnClickListener { onRoomTvClick() }
        btn_appointment_tooltip.setOnClickListener {
            onlineTooltipDialog.showNow(activity.supportFragmentManager, "")
        }

        btnConfirm?.setOnClickListener {appointmentReschedule()}
    }

    private fun isVideoConferenceVisibleRecursive() {
        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.HAS_VIRTUAL_PRACTICE) && args.appointmentDetails.isOnline) {
            btnVideoConference?.visibility = View.VISIBLE
            btnVideoConference?.setOnClickListener {
                navigateToVideoConference()
            }
            shouldEnableVideoConferenceButton()
            refreshHandler()
        }
        else {
            btnVideoConference?.visibility = View.INVISIBLE
        }
    }

    private fun navigateToVideoConference() {
        appointmentDetailsViewModel.getSpecialities(onSuccess = { specialtyData, description, specialtyCode, appointmentType ->
            args.bookingAdditionalFields.specialtyDisplay = specialtyData
            args.bookingAdditionalFields.departmentDescription = description
            args.bookingAdditionalFields.specialtyCode = specialtyCode
            args.bookingAdditionalFields.appointmentType = appointmentType
            openVideoConference()
        }, onError = {
            openVideoConference()
        })
    }

    private fun updatedAppointment() {
        appointmentDetailsViewModel.getUpdatedAppointment(onSuccess = {
            setupAppointmentUpdate(it)
        }, onError = {
            MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
        })
    }

    private fun setupAppointmentUpdate(appointment: NewAppointmentSchedule){
        btnConfirm.visibility = View.VISIBLE
        appointmentUpdateDescription.visibility = View.VISIBLE
        appointmentTimeTv.visibility = View.VISIBLE
        roomTv.visibility = View.VISIBLE
        appointmentTimeTv.paintFlags = appointmentTimeTv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        roomTv.paintFlags = roomTv.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        appointmentTimeTvUpdate.text = "${MdocUtil.getTimeFromMs(appointment.newVersionDocument.appointmentDetails.start, TIME_FORMAT)} - ${MdocUtil.getTimeFromMs(appointment.newVersionDocument.appointmentDetails.end, TIME_FORMAT)}"
        roomTvUpdate.text = appointment.newVersionDocument.appointmentDetails.location
        appointmentUpdateTxt.visibility = View.VISIBLE
        appointmentRoomUpdateTxt.visibility = View.VISIBLE
        appointmentTimeTvUpdate.visibility = View.VISIBLE
        roomTvUpdate.visibility = View.VISIBLE
    }

    private fun appointmentReschedule(){
        appointmentDetailsViewModel.rescheduleAppointment(rescheduledAppointment, onSuccess = {
            findNavController().popBackStack()
        }, onError = {
            MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
        })
    }

    private fun refreshHandler() {
        if (refreshHandler == null) {
            refreshHandler = Handler(Looper.getMainLooper())
        }
        refreshHandler?.postDelayed({
            isVideoConferenceVisibleRecursive()
        }, REFRESH_TIMEOUT)
    }

    private fun shouldEnableVideoConferenceButton() {
        val from = args.appointmentDetails.start - (MINUTE_MS * 10)
        val to = args.appointmentDetails.end + (MINUTE_MS * 5)
        btnVideoConference.isEnabled = System.currentTimeMillis() in from..to
    }

    private fun openVideoConference() {
        activity.showNewFragmentAdd(VideoConferenceFragment().also {
                        it.appointmentDetails = args.appointmentDetails
                        it.bookingAdditionalFields = args.bookingAdditionalFields
                    }, R.id.videoContainer)

    }

    private fun setUpdateTime() {
        val hour = MdocUtil.getTimeFromMs(args.appointmentDetails.uts, MdocConstants.HOUR_AND_MINUTES)
        val date = MdocUtil.getTimeFromMs(args.appointmentDetails.uts, MdocConstants.DAY_MONTH_DOT_FORMAT)
        txt_last_update_details?.text = resources.getString(R.string.last_updated_date_at_hour, date, hour)
    }

    private fun setAppointmentInfo(item: AppointmentDetails) {
        //Set therapist title based on clinic type
        if (item.isAcuteClinic) {
            txt_doctor_label?.setText(R.string.appointments_details_physician)
        }
        else {
            txt_doctor_label?.setText(R.string.appointments_details_therapist)
        }
        val timeFrom = MdocUtil.getTimeFromMs(item.start, TIME_FORMAT)
        if (item.isAllDay) {
            appointmentTimeTv?.text = getString(R.string.appointments_all_day_event)
        }
        else if (item.isShowEndTime && item.end!=null && item.end.toString().trim()!="" && item.end != 0.toLong()) {
            val endTime = MdocUtil.getTimeFromMs(item.end, TIME_FORMAT)
            val formattedTime = "$timeFrom - $endTime"
            appointmentTimeTv?.text = formattedTime
        }
        else {
            appointmentTimeTv?.text = timeFrom
        }
        appointmentDateTv?.text = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, item.start)

        if (item.isShowStatus && !item.appointmentStatus.isNullOrEmpty()){
            status_view.visibility = View.VISIBLE
            tv_status_description.text = item.appointmentStatus
        }

        if (item.isShowTherapistName) {
            item.participants.forEach {
                if (it.type == "ORGANISER") {
                    if (it.displayDataInfo.isNullOrEmpty() ||
                            (it.username != null && it.username.contains("admin")) ||
                            (it.participant != null && it.participant.superAdmin)) {

                        ll_therapist_placeholder.visibility = View.GONE
                    }
                    else {
                        drNameTv?.text = it.displayDataInfo
                    }
                }
            }
        }
        else {
            ll_therapist_placeholder.visibility = View.GONE
        }
        if (resources.getBoolean(R.bool.show_therapist_name_and_room)) {
            item.participants.forEach {
                if (it.type == "ORGANISER") {
                    var roomNumberName = "";
                    if (!item.additionalLocationInfo.isNullOrEmpty() && !item.additionalLocationInfo.trim().isNullOrEmpty()) {
                        if(it.displayDataInfo.isNullOrEmpty() || it.displayDataInfo.trim().isNullOrEmpty()){
                           it.displayDataInfo  = item.additionalLocationInfo
                            roomNumberName = " / " +item.additionalLocationInfo
                        } else {
                            roomNumberName = "${it.displayDataInfo} / ${item.additionalLocationInfo}"
                            it.displayDataInfo =  "${it.displayDataInfo} / ${item.additionalLocationInfo}"
                        }
                    }else{
                        if(!it.displayDataInfo.isNullOrEmpty() || !it.displayDataInfo.trim().isNullOrEmpty()) {
                            roomNumberName = it.displayDataInfo + " / "
                        }
                    }

                    if(it.displayDataInfo.isNullOrEmpty() || it.displayDataInfo.trim().isNullOrEmpty()){
                        ll_therapist_room_placeholder.visibility = View.GONE
                        txt_doctor_room_label.visibility = View.GONE
                    }else{
                        if(resources.getBoolean(R.bool.has_specialityRoom_custom_text)) {
                            drNameRoomTv?.text = roomNumberName
                        }else{
                            drNameRoomTv?.text = it.displayDataInfo
                        }
                    }
                }
            }
        }

        therapyNameTv?.text = item.title
        if (item.comment.isNullOrEmpty()) {
            noteSectionLl?.visibility = View.GONE
        }
        else {
            descriptionTv.text = item.comment
            noteSectionLl?.visibility = View.VISIBLE
        }

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.HAS_VIRTUAL_PRACTICE) && item.isOnline) {
            llRoomPlaceholder?.visibility = View.GONE
        }
        else {
            if (item.location.isNullOrEmpty()) {
                roomTv?.text = getString(R.string.appointment_details_no_info_availabe)
            }
            else {
                roomTv?.text = item.location
            }
        }


        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.HAS_METADATA)) {
            when {
                item.isCanceled          -> {
                    layoutMetaData.visibility = View.VISIBLE
                    btnMetaData.isEnabled = false
                }

                item.isMetaDataPopulated -> {
                    layoutMetaData.visibility = View.VISIBLE
                    txtMetaDataDesc.setText(R.string.appointment_metadata_populated_description)
                    btnMetaData.setText(R.string.appointment_meta_data_show_form)
                }

                item.isMetaDataNeeded -> {
                    appointmentDetailsViewModel.getAppointmentDescriptionFromCoding(onSuccess = {
                        for (item in it?.data?.list!!) {
                            if(item?.code == "DESCRIPTION"){
                                appointmentDescription = item?.display.toString()
                                break
                            }
                        }

                        layoutMetaData.visibility = View.VISIBLE
                        txtMetaDataDesc.text = appointmentDescription?.replace("<appointmentname>",item.title)
                        if(txtMetaDataDesc?.text.isNullOrEmpty()) {
                            txtMetaDataDesc.text =
                                    String.format(getString(R.string.appointment_metadata_needed_description), item.title)
                        }
                        btnMetaData.setText(R.string.appointment_metadata_fill_out_form)
                    }, onError = {
                        MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
                    })

                }
            }
        }

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_BOOKING, MdocConstants.HAS_COMMENT_FIELDS)) {
            if (item.personalInfo.isNullOrEmpty()) {
                commentSectionLl.visibility = View.GONE
            }
            else {
                commentTv.text = item.personalInfo
            }
        }
        else {
            commentSectionLl.visibility = View.GONE
        }
    }

    private fun showPopupMenu(view: View) {
        val popup = PopupMenu(context, view)
        try {
            val fields = popup.javaClass.declaredFields
            for (field in fields) {
                if ("mPopup" == field.name) {
                    field.isAccessible = true
                    val menuPopupHelper = field.get(popup)
                    val classPopupHelper = Class.forName(menuPopupHelper!!.javaClass.name)
                    val setForceIcons = classPopupHelper.getMethod("setForceShowIcon", Boolean::class.javaPrimitiveType)
                    setForceIcons.invoke(menuPopupHelper, true)
                    break
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        popup.menuInflater.inflate(R.menu.popup_appointment_menu, popup.menu)

        if (args.appointmentDetails.specialty.isNullOrEmpty() || context?.resources?.getBoolean(R.bool.change_appointment_enabled)==false){
            popup.menu.findItem(R.id.changeAppointment).isVisible = false
        }

        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.changeAppointment -> {
                    changeAppointmentDialog()
                    true
                }

                R.id.cancelAppointment -> {
                    cancelAppointmentDialog()
                    false
                }

                else ->
                    false
            }
        }
        popup.show()
    }

    private fun getSpecialty() {
        appointmentDetailsViewModel.getSpecialities(onSuccess = {specialtyData, description, specialtyCode, appointmentType ->
            args.bookingAdditionalFields.specialtyDisplay = specialtyData
            args.bookingAdditionalFields.departmentDescription = description
            args.bookingAdditionalFields.specialtyCode = specialtyCode
            args.bookingAdditionalFields.appointmentType = appointmentType
        }, onError = {})
    }

    private fun changeAppointmentDialog() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_appointment_change, null)
        builder.setView(view)
        val dialog = builder.show()
        dialog.setCanceledOnTouchOutside(false)
        val dateTv = view.findViewById<TextView>(R.id.bookDateConfirmTv)
        val timeTv = view.findViewById<TextView>(R.id.bookTimeConfirmTv)
        val appointmentType = view.findViewById<TextView>(R.id.bookTypeConfirmTv)
        val specialtyDisplay = view.findViewById<TextView>(R.id.bookSpecConfirmTv)
        val yesBtn = view.findViewById<Button>(R.id.yesBtn)
        val noBtn = view.findViewById<Button>(R.id.noBtn)

        if (args.appointmentDetails.isShowEndTime && args.appointmentDetails.end!=null && args.appointmentDetails.end.toString().trim()!="" && args.appointmentDetails.end != 0.toLong()) {
            val endTimeText = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, args.appointmentDetails.start) +
                    " - " + MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, args.appointmentDetails.end)
            timeTv.text = endTimeText
        }
        else {
            timeTv.text = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, args.appointmentDetails.start)
        }

        dateTv.text = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, args.appointmentDetails.start)
        specialtyDisplay.text = args.bookingAdditionalFields.specialtyDisplay
        appointmentType.text = args.bookingAdditionalFields.appointmentTypeDisplay

        yesBtn.setOnClickListener {
            val action = AppointmentDetailsFragmentDirections.actionBookAppointment(args.bookingAdditionalFields)
            findNavController().navigate(action)
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
    }

    private fun cancelAppointmentDialog() {
        val dateAppointment = MdocUtil.getDateFromMS("dd.MM.yyyy", args.appointmentDetails.start)
        val time = MdocUtil.getTimeFromMs(args.appointmentDetails.start, "HH:mm")
        val message = resources.getString(
                R.string.cancel_appointment_first) + " " + args.appointmentDetails.title + " " + resources.getString(
                R.string.cancel_appointment_second) + " " + dateAppointment + " " + resources.getString(
                R.string.cancel_appointment_third) + " " + time + " " + resources.getString(
                R.string.cancel_appointment_fourth)
        val date = args.appointmentDetails.start
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.book_appointment_cancel, null)
        builder.setView(view)
        val dialog = builder.show()
        val cancelTitleTv = view.findViewById<TextView>(R.id.cancelTitleTv)
        cancelTitleTv?.text = message
        val yesBtn = view.findViewById<Button>(R.id.cancelYesBtn)
        val cancelBtn = view.findViewById<Button>(R.id.cancelNoBtn)


        yesBtn?.setOnClickListener {
            cancelAppointmentAPI(date)
            dialog.dismiss()
        }
        cancelBtn?.setOnClickListener { dialog.dismiss() }
    }

    private fun cancelAppointmentAPI(date: Long) {
        progressDialog?.show()
        MdocManager.cancelAppointment(args.bookingAdditionalFields.appointmentId, object: Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    findNavController().popBackStack()
                }

                progressDialog?.dismiss()
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                progressDialog?.dismiss()
            }
        })
    }

    private fun onRoomTvClick() {
        findNavController().navigate(R.id.indoorNavigationFragment)
    }

    override fun onDestroy() {
        super.onDestroy()
        progressDialog?.dismiss()
    }

    override fun onPause() {
        super.onPause()
        refreshHandler?.removeCallbacksAndMessages(null)
        Log.e("DEBUG", "onPause of AppointmentDetailsFragment")
    }

    override fun onResume() {
        super.onResume()
        Log.e("DEBUG", "onResume of AppointmentDetailsFragment")
        (getActivity() as MainActivity?)?.showBottomAndTopBar()
    }

    override fun onClick(view: View?) {
       when (view) {
           btnMetaData -> {
               if (args.appointmentDetails.isMetaDataPopulated) {
                   val previewIntent = Intent(activity, PreviewMetadataActivity::class.java)
                   previewIntent.putExtra(PatientDetailsActivity.APPOINTMENT_ID, args.bookingAdditionalFields.appointmentId)
                   previewIntent.putExtra(MdocConstants.APPOINTMENT_UTS, args.appointmentDetails.uts)
                   activity.startActivity(previewIntent)

               } else if (args.appointmentDetails.isMetaDataNeeded){
                   val editIntent = Intent(activity, PatientDetailsActivity::class.java)
                   editIntent.putExtra(PatientDetailsActivity.APPOINTMENT_ID, args.bookingAdditionalFields.appointmentId)
                   activity.startActivityForResult(editIntent, PATIENT_FORM_REQUEST)
               }
           }
       }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PATIENT_FORM_REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                layoutMetaData.visibility = View.GONE
            }
        }
    }

    companion object {
        private const val TIME_FORMAT = "HH:mm"
        private const val MINUTE_MS = 60_000L
        private const val REFRESH_TIMEOUT = 10_000L
        private const val PATIENT_FORM_REQUEST = 10
    }
}
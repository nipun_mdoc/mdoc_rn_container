package de.mdoc.fragments.kiosk;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.PublicClinicResponse;
import de.mdoc.util.MdocAppHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ema on 10/24/17.
 */

public class KioskClinicFragment extends MdocBaseFragment{

    @BindView(R.id.clinicNameTv)
    TextView clinicNameTv;
    @BindView(R.id.descriptionTv)
    TextView descriptionTv;
    @BindView(R.id.myStayIv)
    ImageView clinicIv;

    LoginActivity activity;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_kiosk_clinic;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getNewLoginActivity();

        MdocManager.getPublicClinicDetails(MdocAppHelper.getInstance().getClinicId(), new Callback<PublicClinicResponse>() {
            @Override
            public void onResponse(Call<PublicClinicResponse> call, Response<PublicClinicResponse> response) {
                if(response.isSuccessful()){
                    clinicNameTv.setText(response.body().getData().getName());
                    descriptionTv.setText(Html.fromHtml(response.body().getData().getDescription()));
                    if(!response.body().getData().getImages().get(0).getImage().isEmpty()) {
                        Picasso.get().load(response.body().getData().getImages().get(0).getImage()).into(clinicIv);
                    }
                }
            }

            @Override
            public void onFailure(Call<PublicClinicResponse> call, Throwable throwable) {

            }
        });
    }
}

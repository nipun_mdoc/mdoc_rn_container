package de.mdoc.data.create_bt_device

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CodingObservation(
        val active: Boolean,
        val code: String,
        val display: String,
        val system: String
):Parcelable

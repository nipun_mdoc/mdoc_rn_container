package de.mdoc.modules.checklist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import de.mdoc.pojo.ChecklistItem
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.extractFirstIntFromString
import kotlinx.coroutines.launch
import java.util.*

class ChecklistViewModel (private val mDocService: IMdocService) : ViewModel(){

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    private val checkList = ArrayList<ChecklistItem>()
    private val tempList = ArrayList<ChecklistItem>()

    var items: MutableLiveData<ArrayList<ChecklistItem>> =
            MutableLiveData(arrayListOf())
    

    fun getQuestionnaires(){

        viewModelScope.launch {
            try {
                isLoading.value = true
                isShowContent.value = false

                val data = mDocService.getQuestionariesSuspend(true).data

                checkList.clear()
                tempList.clear()
                for (item in data) {
                    if (item.pollType == ChecklistFragment.POLL_TYPE) {
                        tempList.add(ChecklistItem(item))
                    }
                }
                if (MdocAppHelper.getInstance().checklistOrder != null) {
                    val list = Gson().fromJson<ArrayList<String>>(MdocAppHelper.getInstance().checklistOrder, object : TypeToken<ArrayList<String>>() {

                    }.type)
                    for (s in list) {
                        for (item in tempList) {
                            if (item.pollId == s) {
                                checkList.add(item)
                            }
                        }
                    }
                } else {
                    val sortedList = tempList.sortedWith(Comparator { it1, it2 -> extractFirstIntFromString(it1.title ?: "") - extractFirstIntFromString(it2.title  ?: "") })
                    checkList.addAll(sortedList)
                }

                items.value = checkList
                isLoading.value = false

                if(items.value?.size == 0){
                    isShowNoData.value = true
                }else{
                    isShowContent.value = true
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/16/17.
 */

public class AnswearDetails implements Serializable {

    private int completed;

    private int total;

    private String pollVotingActionId;

    public String getPollVotingActionId() {
        return pollVotingActionId;
    }

    public void setPollVotingActionId(String pollVotingActionId) {
        this.pollVotingActionId = pollVotingActionId;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public boolean isFinished(){
        return completed == total;
    }

    public boolean isStarted(){
        return completed != 0;
    }


}

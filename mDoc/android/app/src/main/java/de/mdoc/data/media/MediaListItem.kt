package de.mdoc.data.media

data class MediaListItem(
        val list: ArrayList<MediaStatisticsData>
)
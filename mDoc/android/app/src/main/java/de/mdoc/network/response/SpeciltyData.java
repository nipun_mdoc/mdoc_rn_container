package de.mdoc.network.response;

import java.util.ArrayList;

public class SpeciltyData {

    private String id;
    private Double cts;
    private Double uts;
    private String language;
    private String specialty;
    private String code;
    private ArrayList<SpecialtyServices> specialtyServices;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Double getCts() {
        return cts;
    }

    public void setCts(Double cts) {
        this.cts = cts;
    }

    public Double getUts() {
        return uts;
    }

    public void setUts(Double uts) {
        this.uts = uts;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public ArrayList<SpecialtyServices> getSpecialtyServices() {
        return specialtyServices;
    }

    public void setSpecialtyServices(ArrayList<SpecialtyServices> specialtyServices) {
        this.specialtyServices = specialtyServices;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String findDescriptionByServiceId(String serviceId) {
        String result = null;
        if (serviceId != null && specialtyServices != null) {
            for (SpecialtyServices specialtyServices : specialtyServices) {
                if (specialtyServices.containsSpecificServiceId(serviceId)) {
                    result = specialtyServices.getDescription();
                }
            }
        }
        return result;
    }

    SpeciltyData findSpecialtyByServiceId(String serviceId) {
        SpeciltyData result = null;
        if (serviceId != null) {
            for (SpecialtyServices specialtyServices : specialtyServices) {
                if (specialtyServices.containsSpecificServiceId(serviceId)) {
                    result = this;
                }
            }
        }
        return result;
    }
}

package de.mdoc.network.request;

/**
 * Created by ema on 10/18/17.
 */

public class VoteRequest {

    private String caseId;
    private String description;
    private String mealId;
    private String mealPlanId;
    private String review;
    private String mealChoiceId;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public String getMealChoiceId() {
        return mealChoiceId;
    }

    public void setMealChoiceId(String mealChoiceId) {
        this.mealChoiceId = mealChoiceId;
    }
}

package de.mdoc.modules.diary.data

import de.mdoc.pojo.DiaryList
import org.joda.time.DateTime
import java.io.Serializable

data class DiaryFilter(
        val categories: List<String>?,
        val from: DateTime?,
        val to: DateTime?
                      ): Serializable {


    fun isMatch(item: DiaryList): Boolean {
        return ((categories == null || categories.contains(item.diaryConfig?.id))
                && (from?.millis == null || item.start >= from.millis)
                && (to?.millis == null || item.start < to.millis))
    }

    fun hasFilter(): Boolean {
        return categories != null || from != null || to != null
    }
}
package de.mdoc.modules.appointments.adapters.viewholders.base

import android.content.Context
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.pojo.AppointmentDetails

interface BaseAppointmentInterface {

    fun bind(item: AppointmentDetails,
             context: Context,
             appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean = false)
}
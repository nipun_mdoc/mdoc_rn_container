package de.mdoc.modules.booking

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentBookAppointmentBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.network.request.FreeSlotsRequest
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FreeSlotsList
import de.mdoc.pojo.newCodingItem
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_book_appointment.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.util.*

class BookAppointmentFragment: NewBaseFragment(), BookAppointmentHandler.BookAppointmentCallback {

    private val args: BookAppointmentFragmentArgs by navArgs()

    lateinit var activity: MdocActivity
    private var from: String? = null
    private var to: String? = null
    private var calendarFrom: Calendar = Calendar.getInstance()
    private var calendarTo: Calendar = Calendar.getInstance()
    override val navigationItem: NavigationItem = NavigationItem.BookAppointment
    private val bookAppointmentViewModel by viewModel {
        BookAppointmentViewModel(
                RestClient.getService(), args.bookingAdditionalFields)
    }
    var filteredDoctors: List<CodingItem> = listOf()
    var doctorId: String? = null
    private var oldPosition = 0
    private val formatter = DateTimeFormat.forPattern("dd MMMM")
    private var defaultClinicId: String? = null
    var appointmentTypeCode: String? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentBookAppointmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_book_appointment, container, false)
        binding.bookAppointmentViewModel = bookAppointmentViewModel
        binding.lifecycleOwner = this
        val appointmentHandler = BookAppointmentHandler(bookAppointmentViewModel.isAppointmentChanging() == true)
        appointmentHandler.setBleResponseListener(this@BookAppointmentFragment)
        binding.handler = appointmentHandler

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializeData()

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_BOOKING, MdocConstants.CAN_FILTER_BOOKABLE_APPOINTMENTS)) {
            observeListOfDoctors()
        }
    }

    private fun initializeData() {
        val calendar = Calendar.getInstance()
        from = calendar.timeInMillis.toString()
        to = MdocUtil.getDayOfTheWeek(calendar.time, 7, false)
            .toString()

        bookAppointmentViewModel.getFreeSlots(slotsRequest(doctorId), onSuccess = {
            hasAppointmentTypeCoding()
        }, onError = {})
        bookAppointmentViewModel.getDoctorList()

        chooseAppFromTv?.text = formatter.print(DateTime.now())
        val lastDayInMonth = Date(MdocUtil.getDayOfTheWeek(Date(), 7, false))
        calendarTo.time = lastDayInMonth

        chooseAppToTv?.text = formatter.print(calendarTo.timeInMillis)

        chooseAppFromLy?.setOnClickListener {
            showDateTimePicker(true, calendarFrom, chooseAppFromTv)
        }

        chooseAppToLy?.setOnClickListener {
            showDateTimePicker(false, calendarTo, chooseAppToTv)
        }

        btn_search_slots?.setOnClickListener {
            bookAppointmentViewModel.getFreeSlots(slotsRequest(doctorId), onSuccess = {
                bookAppointmentViewModel.filterAppointmentsByCode(appointmentTypeCode)
            }, onError = {})
        }
        if(args.bookingAdditionalFields.appointmentId == null)
            appointmentTitleTv?.text = args.bookingAdditionalFields.appointmentTypeDisplay
        else
            appointmentTitleTv?.text = args.bookingAdditionalFields.specialtyDisplay

    }

    private fun observeListOfDoctors() {
        bookAppointmentViewModel.doctors.observe(viewLifecycleOwner, Observer { listOfDoctors ->
            filteredDoctors = listOfDoctors.filter { doctor ->
                doctor.resourcePurposes?.contains(args.bookingAdditionalFields.appointmentType) == true
            }
                .map {
                    CodingItem(code = it.id, display = it.name)
                }
                .distinct()
            if (filteredDoctors.isNotEmpty()) {
                spn_physician.visibility = View.VISIBLE
                generateSpinner(spn_physician, filteredDoctors)
            }
            else {
                spn_physician?.visibility = View.GONE
            }
        })
    }

    private fun generateSpinner(view: AppCompatSpinner?,
                                codingList: List<CodingItem>) {
        val adapterItems = ArrayList(codingList)
        adapterItems.add(0,
                newCodingItem(null, getString(R.string.appointments_book_filter_default))
                        )
        val spinnerAdapter = SpinnerAdapter(context!!, adapterItems)
        view?.adapter = spinnerAdapter
        view?.onItemSelectedListener =
                object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                        doctorId = if (position == 0) {
                            null
                        }
                        else {
                            filteredDoctors.getOrNull(position - 1)
                                ?.code
                        }
                        if (oldPosition != position) {
                            oldPosition = position
                            bookAppointmentViewModel.getFreeSlots(slotsRequest(doctorId), onSuccess = {}, onError = {})
                        }
                    }
                }
    }

    private fun slotsRequest(doctorId: String?): FreeSlotsRequest {
        return FreeSlotsRequest().also {
            it.appointmentType = args.bookingAdditionalFields.appointmentType
            it.caseId = MdocAppHelper.getInstance().caseId
            it.clinicIds = arrayListOf(args.bookingAdditionalFields.clinicId)
            it.from = from
            it.to = to
            it.timeZone = TimeZone.getDefault().id
            it.limit = 10000
            it.skip = 0
            it.query = ""
            it.participant = doctorId
            it.specialtyCode = args.bookingAdditionalFields.specialtyCode
        }
    }

    private fun showDateTimePicker(isFromSelected: Boolean,
                                   calendar: Calendar?,
                                   textView: TextView?) {
        val minDate: Long
        var maxDate: Long = 0

        if (isFromSelected) {
            minDate = Calendar.getInstance()
                .timeInMillis
            maxDate = calendarTo.timeInMillis
        }
        else {
            minDate = calendarFrom.timeInMillis
        }
        val mYear = calendar!!.get(Calendar.YEAR)
        val mMonth = calendar.get(Calendar.MONTH)
        val mDay = calendar.get(Calendar.DAY_OF_MONTH)
        val finalMaxDate = maxDate
        val datePickerDialog = DatePickerDialog(activity, { view, year, month, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.MINUTE, 0)
            calendar.set(Calendar.SECOND, 0)
            calendar.set(Calendar.MILLISECOND, 0)
            view.minDate = minDate

            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            textView?.text = formatter.print(calendar.timeInMillis)

            if (isFromSelected) {
                if (MdocUtil.isToday(calendar.timeInMillis)) {
                    val temp = Calendar.getInstance()
                    calendar.set(Calendar.HOUR_OF_DAY, temp.get(Calendar.HOUR_OF_DAY))
                }
                else {
                    calendar.set(Calendar.HOUR_OF_DAY, 0)
                }
                from = calendar.timeInMillis.toString()
                view.maxDate = finalMaxDate
            }
            else {
                calendar.set(Calendar.HOUR_OF_DAY, 23)
                to = calendar.timeInMillis.toString()
            }
        }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.minDate = minDate
        datePickerDialog.show()
    }

    override fun onFreeSlotClick(appointment: FreeSlotsList) {
        if (appointment.integrationType != null) {
            defaultClinicId = MdocAppHelper.getInstance().clinicId
            MdocAppHelper.getInstance().clinicId = args.bookingAdditionalFields.clinicId
            bookAppointmentViewModel.getEncountersApi(onSuccess = { onEncounterSuccess(it, appointment) }, onError = { onEncounterError() })
        } else {
            openConfirmBookingFragment(appointment)
        }
    }

    private fun onEncounterSuccess(shouldShowMasterData: Boolean, appointment: FreeSlotsList) {
        MdocAppHelper.getInstance().clinicId = defaultClinicId
        if (shouldShowMasterData) {
            val action = BookAppointmentFragmentDirections.actionBookAppointmentFragmentToFragmentMasterData(appointment, args.bookingAdditionalFields, getDoctorNameById(doctorId) ?: "")
            findNavController().navigate(action)
        }
        else {
            openConfirmBookingFragment(appointment)
        }
    }

    private fun onEncounterError() {
        MdocAppHelper.getInstance().clinicId = defaultClinicId

        activity.runOnUiThread {
            if (isAdded) {
                MdocUtil.showToastLong(context, activity.getString(R.string.something_went_wrong))
            }
        }
    }

    private fun openConfirmBookingFragment(appointment: FreeSlotsList) {
        val action =  BookAppointmentFragmentDirections.actionConfirmBooking(appointment, args.bookingAdditionalFields, getDoctorNameById(doctorId) ?: "")
        findNavController().navigate(action)
    }

    private fun hasAppointmentTypeCoding() {
        if (!AppPersistence.appointmentTypeCoding.isNullOrEmpty() && resources.getBoolean(R.bool.has_online_appointments)) {
            appointmentTypePlaceholder?.visibility = View.VISIBLE
                setupAppointmentTypeDropdown(AppPersistence.appointmentTypeCoding)
        }
        else {
            appointmentTypePlaceholder?.visibility = View.GONE
            bookAppointmentViewModel.filterAppointmentsByCode(appointmentTypeCode)
        }
    }

    private fun setupAppointmentTypeDropdown(items: List<CodingItem>) {
        val adapterAppointmentType = SpinnerAdapter(context!!, items)
        appointmentTypeDropDown?.adapter = adapterAppointmentType
        if (context?.resources?.getBoolean(R.bool.virtual_practice) == true){
            appointmentTypeDropDown.setSelection(1)
        }
        appointmentTypeDropDown?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                appointmentTypeCode = items.getOrNull(position)?.code
                bookAppointmentViewModel.filterAppointmentsByCode(appointmentTypeCode)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    private fun getDoctorNameById(doctorId: String?): String? {
        return filteredDoctors.firstOrNull { it.code == doctorId }?.display
    }
}
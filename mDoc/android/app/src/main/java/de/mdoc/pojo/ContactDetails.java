package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 6/19/17.
 */

public class ContactDetails implements Serializable{

    private String email;
    private String fax;
    private String phone;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}

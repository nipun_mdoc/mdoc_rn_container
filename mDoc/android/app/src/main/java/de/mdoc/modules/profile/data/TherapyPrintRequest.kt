package de.mdoc.modules.profile.data

data class TherapyPrintRequest(val printPatientPlan: String)
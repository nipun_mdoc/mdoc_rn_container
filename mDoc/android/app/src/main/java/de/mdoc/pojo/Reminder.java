package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Reminder implements Serializable {

    @SerializedName("time")
    @Expose
    private long time;
    @SerializedName("notifications")
    @Expose
    private List<Object> notifications = null;

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public List<Object> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Object> notifications) {
        this.notifications = notifications;
    }

}

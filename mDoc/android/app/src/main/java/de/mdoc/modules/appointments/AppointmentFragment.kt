package de.mdoc.modules.appointments

import android.graphics.RectF
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alamkanak.weekview.DateTimeInterpreter
import com.alamkanak.weekview.MonthLoader.MonthChangeListener
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewEvent
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.activities.navigation.isTablet
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentAppointmentBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.WeekAppointmentAdapter
import de.mdoc.modules.appointments.adapters.YearAppointmentAdapter
import de.mdoc.modules.appointments.video_conference.VideoConferenceFragment
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.modules.profile.patient_details.PatientDetailsHelper
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.CodingRequest
import de.mdoc.network.response.CodingResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.pojo.*
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_appointment.*
import kotlinx.android.synthetic.main.fragment_caregiver.*
import kotlinx.android.synthetic.main.therapy_details_layout.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AppointmentFragment: NewBaseFragment(), MonthChangeListener, DayAppointmentAdapter.AppointmentDayCallback,
        WeekView.EventClickListener {

    private val appointmentViewModel by viewModel {
        AppointmentViewModel()
    }
    lateinit var activity: MdocActivity
    private var appointmentData: AppointmentData? = null
    private var appointmentsAdapter: DayAppointmentAdapter? = null
    private var weekAppointmentsAdapter: WeekAppointmentAdapter? = null
    private var yearAppointmentAdapter: YearAppointmentAdapter? = null
    private val detailsWeek = ArrayList<AppointmentDetails>()
    private var detailsDay = ArrayList<AppointmentDetails>()
    private val childListUserName = ArrayList<String>()
    private var nameForAppointment = ""
    private var isDay = true
    private var isMonth = false
    private var isWeek = false
    private var isYear = false
    private var startOfWeekMillisecond: Long = MdocUtil.getFirstDayOfWeek()
    private var startOfTheMonth: Long = MdocUtil.getFirstDayInMonth()
    private var endOfTheMonth: Long = MdocUtil.getLastDayInMonth()
    private var startOfTheYear: Long = MdocUtil.getFirstMonthInYear()
    private var endOfTheYear: Long = MdocUtil.getLastMonthInYear()
    private var endWeekMs: Long = 0
    var medium: Typeface? = null
    private var thin: Typeface? = null
    private var dateFromBooking: Long = 0
    internal var mainCalendar: Calendar = GregorianCalendar()
    internal var mainDate = Date()
    override val navigationItem: NavigationItem get() = NavigationItem.Therapy
    private var scrollToPosition = 0
    private val eventsList: ArrayList<WeekViewEvent> = arrayListOf()
    private var leftArrow: ImageView? = null
    private var rightArrow: ImageView? = null
    private var lastUpdateTime: TextView? = null
    private var todayRl: RelativeLayout? = null
    private var weekRl: RelativeLayout? = null
    private var monthRl: RelativeLayout? = null
    private var yearRl: RelativeLayout? = null
    private var todayWeekTabletTv: TextView? = null
    private var weekTabletTv: TextView? = null
    private var dayRecyclerView: RecyclerView? = null
    private var monthView: WeekView? = null
    private var tabletDetails: LinearLayout? = null
    private var todayDayLabel: TextView? = null
    private var childSpinner: com.jaredrummler.materialspinner.MaterialSpinner? = null
    private var appointmentIdForTablet = ""
    private var toast: Toast? = null
    val now = System.currentTimeMillis()

    //Appointment details tablet
    private var appointmentTime: TextView? = null
    private var monthData: List<ListData>? = null

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentAppointmentBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_appointment, container, false)
        binding.lifecycleOwner = this
        binding.appointmentViewModel = appointmentViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        leftArrow = view.findViewById(R.id.arrowLeftIv)
        rightArrow = view.findViewById(R.id.arrowRightIv)
        todayRl = view.findViewById(R.id.todayRl)
        weekRl = view.findViewById(R.id.weekRl)
        monthRl = view.findViewById(R.id.monthRl)
        yearRl = view.findViewById(R.id.yearRl)
        dayRecyclerView = view.findViewById(R.id.appointmentRv)
        weekTabletTv = view.findViewById(R.id.weekTabletTv)
        todayWeekTabletTv = view.findViewById(R.id.todayWeekTabletTv)
        appointmentsAdapter = DayAppointmentAdapter(activity, this)
        weekAppointmentsAdapter = WeekAppointmentAdapter(activity, this)
        yearAppointmentAdapter = YearAppointmentAdapter(activity, this)
        lastUpdateTime = view.findViewById(R.id.txt_last_updated)
        childSpinner = view.findViewById(R.id.childSpinner)
        //TABLET SPECIFIC
        if (activity.isTablet()) {
            appointmentTime = view.findViewById(R.id.appointmentTimeTv)
            todayDayLabel = view.findViewById(R.id.todayAppointmentTv)
            tabletDetails = view.findViewById(R.id.detailLy)
            monthView = view.findViewById(R.id.monthView)
            monthView?.monthChangeListener = this
            monthView?.setOnEventClickListener(this)
            showCommentIfNeeded(null)
            setupDateTimeInterpreter()
            val simpleDateFormat = SimpleDateFormat("EEE", Locale.getDefault())
            val day = simpleDateFormat.format(Date())
            todayDayLabel?.text = day
        }

        dayRecyclerView?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        BandwidthCheck.checkBandwidth(resources.getString(R.string.err_bandwidth), view)

        medium = context?.let { ResourcesCompat.getFont(it, R.font.hk_grotesk_medium) }
        thin =  context?.let { ResourcesCompat.getFont(it, R.font.hk_grotesk_regular) }

        nameForAppointment = MdocAppHelper.getInstance()
            .username


        if (resources.getBoolean(R.bool.has_family_acc)) {
            getChildsForUser()
        }
        else {
            childSpinner?.visibility = View.GONE
        }

        initStartDate()
        setOnClickListeners()
    }

    private fun initStartDate() {
        mainCalendar.time = mainDate
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)

        getPassedDateFromAppointment()
        if (dateFromBooking > 0) {
            setDateFromBooking(dateFromBooking)
        }
        setStartDateTextView()
    }

    private fun getPassedDateFromAppointment() {
        val b = this.arguments
        if (b != null) {
            dateFromBooking = if (b.containsKey("DATE_OF_CALENDAR")) b.getLong("DATE_OF_CALENDAR") else 0
        }
    }

    private fun setOnClickListeners() {
        leftArrow?.setOnClickListener { onLeftArrowClick() }
        rightArrow?.setOnClickListener { onRightArrowClick() }
        todayRl?.setOnClickListener { onTodayClick() }
        weekRl?.setOnClickListener { onWeekClick() }
        monthRl?.setOnClickListener { onMonthClick() }
        yearRl?.setOnClickListener { onYearClick() }

        swipeToRefreshL?.setOnRefreshListener {
            if (!activity.isTablet()) {
                if (!isDay && isWeek) {
                    getAppointmentsForWeek(false)
                } else if (!isDay && isMonth){
                    getAppointmentForMonthMobile(false)
                } else if (!isDay && isYear){
                    getAppointmentForYearMobile(false)
                }
                else {
                    getAppointmentsForDay(false)
                }
            }
            else {
                if (isDay) {
                    getAppointmentsForDay(false)
                }
            }
        }

        refreshIv?.setOnClickListener {
            if (activity.isTablet() && !isDay) {
                getAppointmentsForMonth(false)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isDay && isWeek) {
            onWeekClick()
        } else if (!isDay && isMonth){
            onMonthClick()
        } else if (!isDay && isYear){
            onYearClick()
        }
        else {
            onTodayClick()
        }
    }

    private fun setStartDateTextView() {
        val parsedDate = DateTimeFormat.forPattern("dd.MMMM yyyy")
            .print(mainCalendar.timeInMillis)
        appointmentViewModel.dateRange.value = parsedDate
    }

    private fun getChildsForUser() {
        swipeToRefreshL?.isRefreshing = true

        MdocManager.getChildAccounts(object: Callback<ChildResponse> {
            override fun onResponse(call: Call<ChildResponse>, response: Response<ChildResponse>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        try{
                            val childs: ArrayList<Child>
                            if (response.body()!!.data.size >= 0) {
                                childs = response.body()!!.data
                                if (childs.size > 0) {
                                    childSpinner?.visibility = View.VISIBLE
                                    val childListName = ArrayList<String>()
                                    childListName.add(
                                            MdocAppHelper.getInstance().userFirstName + " " + MdocAppHelper.getInstance().userLastName)
                                    childListUserName.add(MdocAppHelper.getInstance().username)
                                    for (child in childs) {
                                        childListName.add(
                                                child.publicUserDetails.firstName + " " + child.publicUserDetails.lastName)
                                        childListUserName.add(child.publicUserDetails.username)
                                    }
                                    if (childs.size > 0) {
                                        childSpinner?.setItems(childListName)
                                        childSpinner?.setOnItemSelectedListener { _, position, _, _ ->
                                            mainCalendar.time = mainDate
                                            mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
                                            mainCalendar.set(Calendar.MINUTE, 0)
                                            mainCalendar.set(Calendar.SECOND, 0)
                                            mainCalendar.set(Calendar.MILLISECOND, 0)
                                            nameForAppointment = childListUserName[position]
                                            if (isDay) {
                                                detailsDay.clear()
                                                appointmentsAdapter?.notifyDataSetChanged()
                                                getAppointmentsForDay(true)
                                            }
                                            else {
                                                detailsWeek.clear()
                                                weekAppointmentsAdapter?.notifyDataSetChanged()
                                                yearAppointmentAdapter?.notifyDataSetChanged()
                                                getAppointmentsForWeek(true)
                                                getAppointmentForMonthMobile(true)
                                                getAppointmentForYearMobile(true)
                                            }
                                        }
                                    }
                                    else {
                                        childSpinner?.visibility = View.INVISIBLE
                                    }
                                }
                                else {
                                    childSpinner?.visibility = View.GONE
                                }
                            }
                        }catch (e: Exception){

                        }
                    }
                }
                swipeToRefreshL?.isRefreshing = false
            }

            override fun onFailure(call: Call<ChildResponse>, t: Throwable) {
                swipeToRefreshL?.isRefreshing = false
            }
        })
    }

    private fun getAppointmentsForDay(shouldShowProgress: Boolean) {
        monthView?.visibility = View.GONE
        if (shouldShowProgress) {
            swipeToRefreshL?.isRefreshing = true
        }
        val from = mainCalendar.timeInMillis
        val to = DateTime(mainCalendar.timeInMillis).plusDays(1)
            .withHourOfDay(0)
            .withMinuteOfHour(0)
            .withSecondOfMinute(0)
            .millis
        val parsedDate = MdocUtil.getParsedDate("dd", mainCalendar) + ". " + MdocUtil.getParsedDate("MMMM",
                mainCalendar) + " " + MdocUtil.getParsedDate("yyyy", mainCalendar)

        appointmentViewModel.dateRange.value = parsedDate

        MdocManager.searchAppointments(requestBody(from, to), object: Callback<AppointmentResponse> {
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        try {
                            if(response.body()?.data?.list?.isNotEmpty()!!) {
                                response.body()?.data?.list?.get(0)?.isFirstItem = true
                            }
                            var adapterItems = response.body()
                                    ?.data?.list?.map {
                                        it.appointmentDetails.addAdditionalData(it.id, it.uts, it.cts, it.integrationType,
                                                it.encounter, it.metadataNeeded, it.metadataPopulated, it.isConfirmationRequired, it.state, it.newVersionReferenceId, it.isFirstItem, false)
                                    }
                                    ?.sortedWith(compareBy({ !it.isAllDay }, { it.start })) ?: listOf()

                            if (adapterItems.getOrNull(0)?.isAllDay == true) {
                                adapterItems[0].isFirst = true
                            }


                            if (!adapterItems.isNullOrEmpty()) {
                                if (!shouldShowProgress) {
                                    toast = MdocUtil.getCustomToastShort(activity,
                                            getString(R.string.appointment_calendar_is_up_to_date))
                                            .also { it.show() }
                                }
                            }

                            run breaker@{
                                adapterItems.forEach {
                                    if (it.isConfirmationRequired && it.start >= now) {
                                        it.isDark = true
                                        return@breaker
                                    }
                                }
                            }
                            adapterItems = adapterItems.filter { DateTime(mainCalendar.timeInMillis).withHourOfDay(0)
                                    .withMinuteOfHour(0)
                                    .withSecondOfMinute(0).withMillisOfSecond(0).equals(DateTime(it.start).withHourOfDay(0)
                                            .withMinuteOfHour(0)
                                            .withSecondOfMinute(0).withMillisOfSecond(0)) }
                            dayRecyclerView?.adapter = appointmentsAdapter
                            appointmentsAdapter?.setItems(adapterItems)

                            if (activity.isTablet() && adapterItems.isNotEmpty()) {
                                updateAppointmentDetails(adapterItems[0], adapterItems[0].appointmentId)
                            }
                            lastUpdateTime?.visibility = View.VISIBLE
                            val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, response.body()!!.timestamp)
                            lastUpdateTime?.text = resources.getString(R.string.last_updated_at, time)
                            handleEmptyViewWithSwipe(adapterItems)
                        }catch (e: Exception){}

                    }
                    else {
                        Timber.w("searchAppointments %s", response.getErrorDetails())
                        appointmentsAdapter?.setItems(listOf())
                        handleEmptyViewWithSwipe(listOf())
                    }

                    setNoAppointmentMessage(response)
                    swipeToRefreshL?.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                if (isAdded) {
                    Timber.w(t, "searchAppointments")
                    swipeToRefreshL?.isRefreshing = false
                    handleEmptyViewWithSwipe(listOf())
                    showNoDataView()
                }
            }
        })
    }

    private fun handleEmptyViewWithSwipe(items: List<AppointmentDetails>) {
        if (items.isNotEmpty()) {
            appointmentRv?.visibility = View.VISIBLE
            emptyView?.visibility = View.GONE
        }
        else {
            appointmentRv?.visibility = View.GONE
            emptyView?.visibility = View.VISIBLE
        }
    }

    private fun requestBody(from: Long, to: Long): JsonObject {
        val body = JsonObject()
        body.addProperty(MdocConstants.INCLUDE_ENCOUNTER, true)
        body.addProperty(MdocConstants.SKIP, 0)
        body.addProperty(MdocConstants.LIMIT, 9999)
        body.addProperty(MdocConstants.QUERY, "")
        body.addProperty(MdocConstants.FROM, from)
        body.addProperty(MdocConstants.TO, to)
        body.addProperty(MdocConstants.PARTICIPANT, nameForAppointment)
        body.addProperty(MdocConstants.LAST_SEEN_UPDATE, true)
        val array = JsonArray()
        val id = MdocAppHelper.getInstance()
            .clinicId
        array.add(id)
        body.add("clinicIds", array)
        return body
    }

    private fun getAppointmentsForWeek(shouldShowProgress: Boolean) {
        if (shouldShowProgress) {
            swipeToRefreshL?.isRefreshing = true
        }
        val date = Date(startOfWeekMillisecond)
        val calendarRequest = GregorianCalendar()
        calendarRequest.time = date
        val to = MdocUtil.getDayOfTheWeek(calendarRequest.time, 7, true)
        endWeekMs = to

        MdocManager.searchAppointments(requestBody(startOfWeekMillisecond, to), object: Callback<AppointmentResponse> {
            override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                if (response.isSuccessful) {
                    try{
                        lastUpdateTime?.visibility = View.VISIBLE
                        val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, response.body()?.timestamp ?: 0)
                        lastUpdateTime?.text = resources.getString(R.string.last_updated_at, time)

                        appointmentData = response.body()
                                ?.data
                        if(response.body()?.data?.list?.isNotEmpty()!!)response.body()?.data?.list?.get(0)?.isFirstItem = true
                        var adapterItems = response.body()?.data?.list?.map {
                            it.appointmentDetails.addAdditionalData(it.id, it.uts, it.cts, it.integrationType,
                                    it.encounter,
                                    it.metadataNeeded, it.metadataPopulated, it.isConfirmationRequired, it.state, it.newVersionReferenceId, it.isFirstItem, false)
                        }
                                ?.sortedBy { it.start } ?: listOf()

                        if (!adapterItems.isNullOrEmpty()) {
                            if (!shouldShowProgress) {
                                toast = MdocUtil.getCustomToastShort(activity,
                                        getString(R.string.appointment_calendar_is_up_to_date))
                                        .also { it.show() }
                            }
                        }

                        run breaker@{
                            sortedWeekItems(adapterItems).forEach {
                                if (it.isConfirmationRequired && it.start >= now) {
                                    it.isDark = true
                                    return@breaker
                                }
                            }
                        }
                        adapterItems = adapterItems.filter { it.start >= startOfWeekMillisecond && it.start<=endWeekMs  }
                        appointmentRv?.adapter = weekAppointmentsAdapter

                        weekAppointmentsAdapter?.setItems(sortedWeekItems(adapterItems))
                        appointmentRv?.scrollToPosition(scrollToPosition)
                        handleEmptyViewWithSwipe(adapterItems)
                    }catch (e: Exception){}

                }
                else {
                    Timber.w("searchAppointments %s", response.getErrorDetails())
                    handleEmptyViewWithSwipe(listOf())
                }

                setNoAppointmentMessage(response)

                swipeToRefreshL?.isRefreshing = false
            }

            override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                Timber.w(t, "searchAppointments")
                showNoDataView()
                swipeToRefreshL?.isRefreshing = false
                handleEmptyViewWithSwipe(listOf())
            }
        })
    }

    private fun sortedWeekItems(input: List<AppointmentDetails>): ArrayList<AppointmentDetails> {
        val sortedList = arrayListOf<AppointmentDetails>()

        for (dayInWeek in 0..6) {
            val dateTime = DateTime(startOfWeekMillisecond).plusDays(dayInWeek)
            val dayAppointments = arrayListOf<AppointmentDetails>()

            input.forEachIndexed { index, appointment ->
                val appointmentDay = DateTime(appointment.start)
                if (appointmentDay.dayOfWeek() == dateTime.dayOfWeek()) {
                    if (MdocUtil.isToday(appointment.start)) {
                        scrollToPosition = index
                    }
                    dayAppointments.add(appointment)
                }
            }
            dayAppointments.find { it.isAllDay }
                ?.isFirst = true
            dayAppointments.find { !it.isAllDay }
                ?.isFirst = true
            val sorted = dayAppointments.sortedWith(
                    compareBy<AppointmentDetails> { !it.isAllDay }.thenBy { it.start })

            sorted.getOrNull(0)
                ?.isFirstItemInDay = true
            sortedList.addAll(sorted)
        }
        return sortedList
    }

    private fun getAppointmentForMonthMobile(shouldShowProgress: Boolean) {
        swipeToRefreshL?.isRefreshing = true
        val startDate = Date(startOfTheMonth)
        val endDate = Date(endOfTheMonth)
        val startCal = GregorianCalendar()
        startCal.time = startDate
        val endCal = GregorianCalendar()
        endCal.time = endDate
        val month = MdocUtil.getMonth(startDate)
        val year = MdocUtil.getYear(startDate)
        appointmentViewModel.dateRange.value = (month.toString() + " " + year.toString())

        MdocManager.searchAppointments(requestBody(startOfTheMonth, endOfTheMonth),
                object: Callback<AppointmentResponse> {
                    override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                        if (response.isSuccessful) {
                            try{
                                val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES,
                                        response.body()!!.timestamp)
                                lastUpdateTime?.text = resources.getString(R.string.last_updated_at, time)
                                appointmentData = response.body()?.data

                                if(response.body()?.data?.list?.isNotEmpty()!!)response.body()?.data?.list?.get(0)?.isFirstItem = true

                                var previousDate = ""
                                val adapterItems = response.body()?.data?.list?.map {

                                    var isFirstTime = true;
                                    if(MdocUtil.getTimeFromMs(it.appointmentDetails.start, "dd").equals(previousDate)){
                                        isFirstTime = false
                                    }else{
                                        isFirstTime = true
                                    }
                                    previousDate = MdocUtil.getTimeFromMs(it.appointmentDetails.start, "dd")
                                    it.appointmentDetails.addAdditionalData(it.id, it.uts, it.cts, it.integrationType,
                                            it.encounter,
                                            it.metadataNeeded, it.metadataPopulated, it.isConfirmationRequired, it.state, it.newVersionReferenceId, isFirstTime, true)
                                }
                                        ?.sortedBy { it.start } ?: listOf()

                                if (!adapterItems.isNullOrEmpty()) {
                                    if (!shouldShowProgress) {
                                        toast = MdocUtil.getCustomToastShort(activity,
                                                getString(R.string.appointment_calendar_is_up_to_date))
                                                .also { it.show() }
                                    }
                                }

                                run breaker@{
                                    sortedWeekItems(adapterItems).forEach {
                                        if (it.isConfirmationRequired && it.start >= now) {
                                            it.isDark = true
                                            return@breaker
                                        }
                                    }
                                }

                                appointmentRv?.adapter = weekAppointmentsAdapter


                                adapterItems.forEachIndexed { index, appointment ->
                                    if (MdocUtil.isToday(appointment.start)) {
                                        scrollToPosition = index
                                    }
                                }

                                weekAppointmentsAdapter?.setItems(adapterItems)
                                appointmentRv?.scrollToPosition(scrollToPosition)
                                handleEmptyViewWithSwipe(adapterItems)
                            }catch (e: Exception){

                            }
                        }
                        else {
                            Timber.w(response.message(), "searchAppointments")
                        }
                        setNoAppointmentMessage(response)

                        swipeToRefreshL?.isRefreshing = false
                    }

                    override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                        Timber.w(t, "searchAppointments")
                        swipeToRefreshL?.isRefreshing = false
                        showNoDataView()
                    }
                })
    }

    private fun getAppointmentForYearMobile(shouldShowProgress: Boolean){
        swipeToRefreshL?.isRefreshing = true
        val startDate = Date(startOfTheYear)
        val endDate = Date(endOfTheYear)
        val startCal = GregorianCalendar()
        startCal.time = startDate
        val endCal = GregorianCalendar()
        endCal.time = endDate
        val year = MdocUtil.getYear(startDate)
        appointmentViewModel.dateRange.value = year

        MdocManager.searchAppointments(requestBody(startOfTheYear, endOfTheYear),
                object: Callback<AppointmentResponse> {
                    override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                        if (response.isSuccessful) {
                            try{
                                val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES,
                                        response.body()!!.timestamp)
                                lastUpdateTime?.text = resources.getString(R.string.last_updated_at, time)
                                appointmentData = response.body()?.data

                                if(response.body()?.data?.list?.isNotEmpty()!!)response.body()?.data?.list?.get(0)?.isFirstItem = true
                                var previousDate = ""
                                var previousMonth = ""
                                val adapterItems = response.body()?.data?.list?.map {

                                    var isFirstTime = true;
                                    if(MdocUtil.getTimeFromMs(it.appointmentDetails.start, "MMM").equals(previousMonth) && MdocUtil.getTimeFromMs(it.appointmentDetails.start, "dd").equals(previousDate)){
                                        isFirstTime = false
                                    }else{
                                        isFirstTime = true
                                    }
                                    previousDate = MdocUtil.getTimeFromMs(it.appointmentDetails.start, "dd")
                                    previousMonth = MdocUtil.getTimeFromMs(it.appointmentDetails.start, "MMM")
                                    it.appointmentDetails.addAdditionalData(it.id, it.uts, it.cts, it.integrationType,
                                            it.encounter,
                                            it.metadataNeeded, it.metadataPopulated, it.isConfirmationRequired, it.state, it.newVersionReferenceId, isFirstTime, false)
                                }
                                        ?.sortedBy { it.start } ?: listOf()


                                if (!adapterItems.isNullOrEmpty()) {
                                    if (!shouldShowProgress) {
                                        toast = MdocUtil.getCustomToastShort(activity,
                                                getString(R.string.appointment_calendar_is_up_to_date))
                                                .also { it.show() }
                                    }
                                }

                                run breaker@{
                                    sortedWeekItems(adapterItems).forEach {
                                        if (it.isConfirmationRequired && it.start >= now) {
                                            it.isDark = true
                                            return@breaker
                                        }
                                    }
                                }

                                appointmentRv?.adapter = yearAppointmentAdapter

                                yearAppointmentAdapter?.setItems(adapterItems)
                                appointmentRv?.scrollToPosition(scrollToPosition)
                                handleEmptyViewWithSwipe(adapterItems)
                            }catch (e: Exception){}

                        }
                        else {
                            Timber.w(response.message(), "searchAppointments")
                        }
                        setNoAppointmentMessage(response)

                        swipeToRefreshL?.isRefreshing = false
                    }

                    override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                        Timber.w(t, "searchAppointments")
                        swipeToRefreshL?.isRefreshing = false
                        showNoDataView()
                    }
                })
    }

    private fun getAppointmentsForMonth(shoudShowProgress: Boolean) {
        swipeToRefreshL?.isRefreshing = true
        val startDate = Date(MdocUtil.getFirstDayInMonth())
        val endDate = Date(MdocUtil.getLastDayInMonth())
        val startCal = GregorianCalendar()
        startCal.time = startDate
        val endCal = GregorianCalendar()
        endCal.time = endDate
        val startDateString = MdocUtil.getParsedDate("dd", startCal) + ". " + MdocUtil.getParsedDate("MMMM",
                startCal) + " " + MdocUtil.getParsedDate("yyyy", startCal)
        val endDateString = MdocUtil.getParsedDateUTC("dd", endCal) + ". " + MdocUtil.getParsedDate("MMMM",
                endCal) + " " + MdocUtil.getParsedDate("yyyy", endCal)
        appointmentViewModel.dateRange.value = "$startDateString - $endDateString"

        MdocManager.searchAppointments(requestBody(MdocUtil.getFirstDayInMonth(), MdocUtil.getLastDayInMonth()),
                object: Callback<AppointmentResponse> {
                    override fun onResponse(call: Call<AppointmentResponse>, response: Response<AppointmentResponse>) {
                        if (response.isSuccessful) {
                            try{
                                eventsList.clear()
                                val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES,
                                        response.body()!!.timestamp)
                                lastUpdateTime?.text = resources.getString(R.string.last_updated_at, time)
                                monthData = response.body()
                                        ?.data?.list

                                if (!monthData.isNullOrEmpty()) {
                                    if (!shoudShowProgress) {
                                        toast = MdocUtil.getCustomToastShort(activity,
                                                getString(R.string.appointment_calendar_is_up_to_date))
                                                .also { it.show() }
                                    }
                                }

                                setupMonthView(monthData ?: listOf())
                            }catch (e: Exception){}

                        }
                        else {
                            Timber.w(response.message(), "searchAppointments")
                        }
                        setNoAppointmentMessage(response)

                        swipeToRefreshL?.isRefreshing = false
                    }

                    override fun onFailure(call: Call<AppointmentResponse>, t: Throwable) {
                        Timber.w(t, "searchAppointments")
                        swipeToRefreshL?.isRefreshing = false
                        showNoDataView()
                    }
                })
    }

    private fun setupMonthView(appointments: List<ListData>) {
        val horizontalDate = Date()
        var position = 0
        val formatter = SimpleDateFormat("HH");


        for ((index) in appointments.withIndex()) {
            val data = appointments[index].appointmentDetails
            val start = Calendar.getInstance()
            val end = Calendar.getInstance()

            start.timeInMillis = data.start
            end.timeInMillis = data.start + (60 * 30 * 1000);
            if (position == 0) {
                position = formatter.format(data.start)
                    .toInt()
            }
            val fromTime = MdocUtil.getTimeFromMs(data.start, TIME_FORMAT);

            var event: WeekViewEvent?
            event = if (appointments[index].appointmentDetails.isShowEndTime && appointments[index].appointmentDetails.end!=null && appointments[index].appointmentDetails.end.toString().trim()!="" && appointments[index].appointmentDetails.end != 0.toLong()) {
                val endTime = MdocUtil.getTimeFromMs(data.end, TIME_FORMAT)
                WeekViewEvent(index.toLong(), fromTime + " - " + endTime + "\n" + data.title, start, end)
            }
            else {
                WeekViewEvent(index.toLong(), fromTime + "\n" + data.title, start, end)
            }

            if (MdocUtil.isToday(start.timeInMillis)) {
                horizontalDate.time = start.timeInMillis
                event.color = ContextCompat.getColor(activity, R.color.colorSecondary32)
            }
            else {
                event.color = ContextCompat.getColor(activity, R.color.lavander_mist)
            }

            eventsList.add(event)
        }

        monthView?.notifyDatasetChanged()
        monthView?.goToToday()
        monthView?.goToHour(position.toDouble())
    }

    private fun updateDateLabel() {
        val newDate = Date(startOfWeekMillisecond)
        val calendar = GregorianCalendar()
        calendar.time = newDate
        val startWeek = GregorianCalendar()
        startWeek.time = newDate
        val to = MdocUtil.getDayOfTheWeek(calendar.time, 6, false)
        endWeekMs = to
        val endWeekDate = Date(endWeekMs)
        val endWeekCalendar = GregorianCalendar()
        endWeekCalendar.time = endWeekDate
        val startDateString = MdocUtil.getParsedDate("dd", startWeek) + ". " + MdocUtil.getParsedDate("MMMM",
                startWeek) + " " + MdocUtil.getParsedDate("yyyy", startWeek)
        val endDateString = MdocUtil.getParsedDateUTC("dd", endWeekCalendar) + ". " + MdocUtil.getParsedDate("MMMM",
                endWeekCalendar) + " " + MdocUtil.getParsedDate("yyyy", endWeekCalendar)
        appointmentViewModel.dateRange.value = "$startDateString - $endDateString"
    }

    private fun updateMonthLabel(){
        val startDate = Date(startOfTheMonth)
        val month = MdocUtil.getMonth(startDate)
        val year = MdocUtil.getYear(startDate)
        appointmentViewModel.dateRange.value = (month.toString() + " " + year.toString())
    }

    private fun updateYearLabel(){
        val startDate = Date(startOfTheYear)
        val year = MdocUtil.getYear(startDate)
        appointmentViewModel.dateRange.value = year
    }

    private fun setTodaySelected() {
        if (activity.isTablet()) {
            leftArrow?.visibility = View.VISIBLE
            rightArrow?.visibility = View.VISIBLE
            todayWeekTabletTv?.typeface = medium
            todayWeekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        }
        todayV?.visibility = View.VISIBLE
        todayTv?.typeface = medium
    }

    private fun setWeekSelected() {
        if (activity.isTablet()) {
            monthView?.visibility = View.VISIBLE
            dayRecyclerView?.visibility = View.GONE
            tabletDetails?.visibility = View.GONE
            leftArrow?.visibility = View.INVISIBLE
            rightArrow?.visibility = View.INVISIBLE
            weekTabletTv?.typeface = medium
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        }
        weekV?.visibility = View.VISIBLE
        weekTv?.typeface = medium
    }

    private fun setMonthSelected(){
        if (activity.isTablet()){
            monthView?.visibility = View.VISIBLE
            dayRecyclerView?.visibility = View.GONE
            tabletDetails?.visibility = View.GONE
            leftArrow?.visibility = View.INVISIBLE
            rightArrow?.visibility = View.INVISIBLE
            weekTabletTv?.typeface = medium
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        } else {
            monthV?.visibility = View.VISIBLE
            monthTv?.typeface = medium
        }
    }

    private fun setYearSelected(){
        if (activity.isTablet()){
            monthView?.visibility = View.VISIBLE
            dayRecyclerView?.visibility = View.GONE
            tabletDetails?.visibility = View.GONE
            leftArrow?.visibility = View.INVISIBLE
            rightArrow?.visibility = View.INVISIBLE
            weekTabletTv?.typeface = medium
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.colorPrimary))
        } else {
            yearV?.visibility = View.VISIBLE
            yearTv?.typeface = medium
        }
    }

    private fun setYearDeselected(){
        yearAppointmentAdapter?.removeItems()
        yearV?.visibility = View.INVISIBLE
        yearTv?.typeface = thin
        if (activity.isTablet()) {
            monthView?.visibility = View.GONE
            dayRecyclerView?.visibility = View.VISIBLE
            tabletDetails?.visibility = View.VISIBLE
            monthView?.visibility = View.GONE
            weekTabletTv?.typeface = thin
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.charocal_gray))
        }
    }

    private fun setMonthDeselected(){
        yearAppointmentAdapter?.removeItems()
        monthV?.visibility = View.INVISIBLE
        monthTv?.typeface = thin
        if (activity.isTablet()) {
            monthView?.visibility = View.GONE
            dayRecyclerView?.visibility = View.VISIBLE
            tabletDetails?.visibility = View.VISIBLE
            monthView?.visibility = View.GONE
            weekTabletTv?.typeface = thin
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.charocal_gray))
        }
    }

    private fun setTodayDeselected() {
        appointmentsAdapter?.removeItems()
        todayV?.visibility = View.INVISIBLE
        todayTv?.typeface = thin

        todayWeekTabletTv?.typeface = thin
        todayWeekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.charocal_gray))
    }

    private fun setWeekDeselected() {
        weekAppointmentsAdapter?.removeItems()
        weekV?.visibility = View.INVISIBLE
        weekTv?.typeface = thin
        if (activity.isTablet()) {
            monthView?.visibility = View.GONE
            dayRecyclerView?.visibility = View.VISIBLE
            tabletDetails?.visibility = View.VISIBLE
            monthView?.visibility = View.GONE
            weekTabletTv?.typeface = thin
            weekTabletTv?.setTextColor(ContextCompat.getColor(activity, R.color.charocal_gray))
        }
    }

    private fun onTodayClick() {
        toast?.cancel()
        isDay = true
        isMonth = false
        isWeek = false
        isYear = false
        if (dateFromBooking > 0) {
            setDateFromBooking(dateFromBooking)
            dateFromBooking = 0
        }
        else {
            mainCalendar.time = mainDate
            mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
            mainCalendar.set(Calendar.MINUTE, 0)
            mainCalendar.set(Calendar.SECOND, 0)
            mainCalendar.set(Calendar.MILLISECOND, 0)
        }
        setTodaySelected()
        setWeekDeselected()
        setMonthDeselected()
        setYearDeselected()
        detailsWeek.clear()
        weekAppointmentsAdapter?.notifyDataSetChanged()
        seperator?.visibility = View.VISIBLE

        if (activity.isTablet()) {
            refreshIv?.visibility = View.GONE
            swipeToRefreshL?.visibility = View.VISIBLE
            todayDayLabel?.visibility = View.VISIBLE
        }

        getAppointmentsForDay(true)
    }

    private fun onMonthClick(){
        toast?.cancel()
        isDay = false
        isMonth = true
        isWeek = false
        isYear = false

        setMonthSelected()
        setTodayDeselected()
        setWeekDeselected()
        setYearDeselected()
        detailsDay.clear()
        appointmentsAdapter?.notifyDataSetChanged()
        seperator?.visibility = View.VISIBLE

        if (activity.isTablet()) {
            refreshIv?.visibility = View.VISIBLE
            swipeToRefreshL?.visibility = View.INVISIBLE
        }
        else {
            swipeToRefreshL?.visibility = View.VISIBLE
        }

        setCurrentMonth()
    }

    private fun onYearClick(){
        toast?.cancel()
        isDay = false
        isMonth = false
        isWeek = false
        isYear = true

        setYearSelected()
        setTodayDeselected()
        setWeekDeselected()
        setMonthDeselected()
        detailsDay.clear()
        appointmentsAdapter?.notifyDataSetChanged()
        seperator?.visibility = View.VISIBLE

        if (activity.isTablet()) {
            refreshIv?.visibility = View.VISIBLE
            swipeToRefreshL?.visibility = View.INVISIBLE
        }
        else {
            swipeToRefreshL?.visibility = View.VISIBLE
        }

        setCurrentYear()
    }

    private fun onWeekClick() {
        toast?.cancel()
        isDay = false
        isMonth = false
        isWeek = true
        isYear = false

        setWeekSelected()
        setMonthDeselected()
        setTodayDeselected()
        setYearDeselected()
        detailsDay.clear()
        appointmentsAdapter?.notifyDataSetChanged()
        seperator?.visibility = View.GONE

        if (activity.isTablet()) {
            refreshIv?.visibility = View.VISIBLE
            swipeToRefreshL?.visibility = View.INVISIBLE
            todayDayLabel?.visibility = View.GONE
        }
        else {
            swipeToRefreshL?.visibility = View.VISIBLE
        }
        setCurrentWeek()
    }

    private fun onRightArrowClick() {
        toast?.cancel()
        if (isDay) {
            appointmentsAdapter?.removeItems()
            mainCalendar.add(Calendar.DATE, 1)
            mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
            mainCalendar.set(Calendar.MINUTE, 0)
            mainCalendar.set(Calendar.SECOND, 0)
            mainCalendar.set(Calendar.MILLISECOND, 0)
            mainDate = mainCalendar.time
            if (activity.isTablet()) {
                val simpleDateFormat = SimpleDateFormat("EEE", Locale.getDefault())
                val day = simpleDateFormat.format(mainDate)
                todayDayLabel?.text = day
            }
            getAppointmentsForDay(true)
        }
        else {
            weekAppointmentsAdapter?.removeItems()
            if (!activity.isTablet()) {
                if (isWeek) {
                    getNextWeek()
                    if (activity.isTablet()) {
                        todayDayLabel?.visibility = View.GONE
                    }
                } else if (isMonth) {
                    getNextMonth()
                } else if (isYear){
                    getNextYear()
                }
            }
        }
    }

    private fun onLeftArrowClick() {
        toast?.cancel()
        if (isDay) {
            appointmentsAdapter?.removeItems()
            mainCalendar.add(Calendar.DATE, -1)
            mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
            mainCalendar.set(Calendar.MINUTE, 0)
            mainCalendar.set(Calendar.SECOND, 0)
            mainCalendar.set(Calendar.MILLISECOND, 0)
            mainDate = mainCalendar.time
            if (activity.isTablet()) {
                val simpleDateFormat = SimpleDateFormat("EEE", Locale.getDefault())
                val day = simpleDateFormat.format(mainDate)
                todayDayLabel?.text = day
            }
            getAppointmentsForDay(true)
        }
        else {
            weekAppointmentsAdapter?.removeItems()
            if (!activity.isTablet()) {
                if (isWeek) {
                    getPastWeek()
                    if (activity.isTablet()) {
                        todayDayLabel?.visibility = View.GONE
                    }
                } else if (isMonth) {
                    getPastMonth()
                } else if (isYear){
                    getPastYear()
                }
            }
        }
    }

    private fun setCurrentYear(){
        yearAppointmentAdapter?.notifyDataSetChanged()

        updateYearLabel()
        if (activity.isTablet()) {
            getAppointmentsForMonth(true)
        }
        else {
            getAppointmentForYearMobile(true)
        }
    }

    private fun setCurrentMonth(){
        weekAppointmentsAdapter?.notifyDataSetChanged()

        updateMonthLabel()
        if (activity.isTablet()) {
            getAppointmentsForMonth(true)
        }
        else {
            getAppointmentForMonthMobile(true)
        }
    }

    private fun setCurrentWeek() {
        detailsWeek.clear()
        weekAppointmentsAdapter?.notifyDataSetChanged()

        updateDateLabel()
        if (activity.isTablet()) {
            getAppointmentsForMonth(true)
        }
        else {
            getAppointmentsForWeek(true)
        }
    }

    private fun getNextYear(){
        weekAppointmentsAdapter?.notifyDataSetChanged()
        val dateStart = Date(startOfTheYear)
        val dateEnd = Date(endOfTheYear)
        startOfTheYear = MdocUtil.getYear(dateStart, 1)
        endOfTheYear = MdocUtil.getYear(dateEnd, 1)
        updateYearLabel()
        getAppointmentForYearMobile(true)
    }

    private fun getPastYear(){
        yearAppointmentAdapter?.notifyDataSetChanged()
        val dateStart = Date(startOfTheYear)
        val dateEnd = Date(endOfTheYear)
        startOfTheYear = MdocUtil.getYear(dateStart, -1)
        endOfTheYear = MdocUtil.getYear(dateEnd, -1)
        updateYearLabel()
        getAppointmentForYearMobile(true)
    }

    private fun getNextMonth(){
        weekAppointmentsAdapter?.notifyDataSetChanged()
        val dateStart = Date(startOfTheMonth)
        val dateEnd = Date(endOfTheMonth)
        startOfTheMonth = MdocUtil.getMonth(dateStart, 1, false)
        endOfTheMonth = MdocUtil.getMonth(dateEnd, 1, true)
        updateMonthLabel()
        getAppointmentForMonthMobile(true)
    }

    private fun getPastMonth(){
        weekAppointmentsAdapter?.notifyDataSetChanged()
        val dateStart = Date(startOfTheMonth)
        val dateEnd = Date(endOfTheMonth)
        startOfTheMonth = MdocUtil.getMonth(dateStart, -1, false)
        endOfTheMonth = MdocUtil.getMonth(dateEnd, -1, true)
        updateMonthLabel()
        getAppointmentForMonthMobile(true)
    }

    private fun getNextWeek() {
        detailsWeek.clear()
        weekAppointmentsAdapter?.notifyDataSetChanged()
        val date = Date(startOfWeekMillisecond)
        startOfWeekMillisecond = MdocUtil.getDayOfTheWeek(date, 7, true)
        updateDateLabel()
        getAppointmentsForWeek(true)
    }

    private fun getPastWeek() {
        detailsWeek.clear()
        weekAppointmentsAdapter?.notifyDataSetChanged()
        val date = Date(startOfWeekMillisecond)
        startOfWeekMillisecond = MdocUtil.getDayOfTheWeek(date, -7, true)
        updateDateLabel()
        getAppointmentsForWeek(true)
    }

    private fun setNoAppointmentMessage(response: Response<AppointmentResponse>) {
        when (response.isSuccessful) {
            true -> {

                when (response.body()?.data?.list?.isNullOrEmpty()) {
                    false -> {
                        hideNoDataView()
                    }
                    else -> {
                        showNoDataView()
                    }
                }
            }

            else -> {
                showNoDataView()
            }
        }
    }

    private fun showNoDataView () {
        noDataTherapyTv?.visibility = View.VISIBLE
    }

    private fun hideNoDataView () {
        noDataTherapyTv?.visibility = View.GONE
    }

    private fun setDateFromBooking(dateFromBooking: Long) {
        mainCalendar.timeInMillis = dateFromBooking
        mainCalendar.set(Calendar.HOUR_OF_DAY, 0)
        mainCalendar.set(Calendar.MINUTE, 0)
        mainCalendar.set(Calendar.SECOND, 0)
        mainCalendar.set(Calendar.MILLISECOND, 0)
    }

    override fun onDestroy() {
        super.onDestroy()
        swipeToRefreshL?.isRefreshing = false
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): MutableList<out WeekViewEvent> {
        val matchedEvents: MutableList<WeekViewEvent> = java.util.ArrayList()
        for (event in eventsList) {
            if (eventMatches(event, newYear, newMonth)) {
                matchedEvents.add(event)
            }
        }
        return matchedEvents
    }

    private fun eventMatches(event: WeekViewEvent, year: Int, month: Int): Boolean {
        return event.startTime[Calendar.YEAR] == year && event.startTime[Calendar.MONTH] == month - 1 || event.endTime[Calendar.YEAR] == year && event.endTime[Calendar.MONTH] == month - 1
    }

    companion object {
        const val TIME_FORMAT = "HH:mm"
    }

    //TABLET ONLY
    override fun onDayItemClicked(item: AppointmentDetails, view: View?) {
        updateAppointmentDetails(item, item.appointmentId)
    }

    private fun setupDateTimeInterpreter() {
        monthView?.dateTimeInterpreter = object: DateTimeInterpreter {
            override fun interpretDate(date: Calendar?): String {
                val weekdayNameFormat = SimpleDateFormat("EEE", Locale.getDefault())
                val weekday = weekdayNameFormat.format(date?.time ?: Date())
                val format = SimpleDateFormat(" d", Locale.getDefault())
                return weekday.toUpperCase(Locale.getDefault()) + format.format(date?.time ?: Date())
            }

            override fun interpretTime(hour: Int): String {
                return hour.toString()
            }
        }
    }

    private fun updateAppointmentDetails(item: AppointmentDetails, appointmentId: String) {
        //Set therapist title based on clinic type
        isVideoConferenceVisible(item)

        if (item.isAcuteClinic) {
            txt_doctor_label?.setText(R.string.appointments_details_physician)
        }
        else {
            txt_doctor_label?.setText(R.string.appointments_details_therapist)
        }
        val hour = MdocUtil.getTimeFromMs(item.uts, MdocConstants.HOUR_AND_MINUTES)
        val date = MdocUtil.getTimeFromMs(item.uts, MdocConstants.DAY_MONTH_DOT_FORMAT)
        txt_last_update_details?.text = resources.getString(R.string.last_updated_date_at_hour, date, hour)
        val timeFrom = MdocUtil.getTimeFromMs(item.start, TIME_FORMAT)
        when {
            item.isAllDay      -> {
                appointmentTimeTv?.text = getString(R.string.appointments_all_day_event)
            }

            item.isShowEndTime && item.end!=null && item.end.toString().trim()!="" && item.end != 0.toLong() -> {
                val endTime = MdocUtil.getTimeFromMs(item.end, TIME_FORMAT)
                val formattedTime = "$timeFrom - $endTime"
                appointmentTimeTv?.text = formattedTime
            }

            else               -> {
                appointmentTimeTv?.text = timeFrom
            }
        }
        appointmentDateTv?.text = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, item.start)

        therapyNameTv.text = item.title
        if (item.comment.isNullOrEmpty()) {
            noteSectionLl?.visibility = View.GONE
        }
        else {
            descriptionTv.text = item.comment
            noteSectionLl?.visibility = View.VISIBLE
        }

        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.HAS_VIRTUAL_PRACTICE)) {
            if (item.location == null || item.location.isEmpty()) {
                roomTv.text = getString(R.string.appointment_details_no_info_availabe)
            }
            else {
                roomTv.text = item.location
            }
        }
        else {
            if (item.location.isNullOrEmpty()) {
                roomTv?.text = getString(R.string.appointment_details_no_info_availabe)
            }
            else {
                roomTv?.text = item.location
            }
        }

        if (item.isShowTherapistName) {
            item.participants.forEach {
                if (it.type == "ORGANISER") {
                    if (it.displayDataInfo.isNullOrEmpty() || (it.username != null && it.username.contains("admin"))) {
                        ll_therapist_placeholder.visibility = View.GONE
                    }
                    else {
                        drNameTv?.text = it.displayDataInfo
                    }
                }
            }
        }
        else {
            ll_therapist_placeholder.visibility = View.GONE
        }

        if (resources.getBoolean(R.bool.show_therapist_name_and_room)) {
            item.participants.forEach {
                if (it.type == "ORGANISER") {
                    var roomNumberName = "";
                    var nameOfDoctor  = "${it.title?:""} ${it.firstName?:""} ${it.lastName?:""}"
                    roomNumberName = nameOfDoctor
                    if (!item.additionalLocationInfo.trim().isNullOrEmpty()) {
                        nameOfDoctor = if (nameOfDoctor.trim()
                                .isEmpty()) {
                            if(resources.getBoolean(R.bool.has_specialityRoom_custom_text)){
                                " / " + item.additionalLocationInfo
                            }else{
                                item.additionalLocationInfo
                            }
                        }
                        else {
                            roomNumberName = "$nameOfDoctor / ${item.additionalLocationInfo}"
                            "$nameOfDoctor / ${item.additionalLocationInfo}"
                        }
                    }else{
                        roomNumberName   = roomNumberName + " / "
                    }

                    if (nameOfDoctor.trim()
                            .isEmpty()) {
                        ll_therapist_room_placeholder.visibility = View.GONE
                        txt_doctor_room_label.visibility = View.GONE
                    }
                    else {
                        if(resources.getBoolean(R.bool.has_specialityRoom_custom_text)) {
                            drNameRoomTv?.text = roomNumberName
                        }else{
                            drNameRoomTv?.text = nameOfDoctor
                        }
                    }
                }
            }
        }
        showCommentIfNeeded(item.personalInfo)

        appointmentIdForTablet = appointmentId

    }

    private fun isVideoConferenceVisible(appointmentDetails: AppointmentDetails) {
        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.HAS_VIRTUAL_PRACTICE) && appointmentDetails.isOnline) {
            btnVideoConference?.visibility = View.VISIBLE
            btnVideoConference?.setOnClickListener { openVideoConference(appointmentDetails) }
        }
        else {
            btnVideoConference?.visibility = View.INVISIBLE
        }
    }

    private fun openVideoConference(appointmentDetails: AppointmentDetails) {
        activity.showNewFragmentAdd(VideoConferenceFragment().also {
            it.appointmentDetails = appointmentDetails
        }, R.id.videoContainer)
    }

    private fun showCommentIfNeeded(personalInfo: String?) {
        if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_BOOKING, MdocConstants.HAS_COMMENT_FIELDS)) {
            if (personalInfo.isNullOrEmpty()) {
                commentSectionLl?.visibility = View.GONE
            }
            else {
                commentTv?.text = personalInfo
            }
        }
        else {
            commentSectionLl?.visibility = View.GONE
        }
    }

    override fun onPause() {
        super.onPause()
        toast?.cancel()
    }

    override fun onEventClick(event: WeekViewEvent?, eventRect: RectF?) {
        val index = event?.id?.toInt() ?: 0
        val details = monthData?.getOrNull(index)
            ?.appointmentDetails
        if (details != null) {
            tabletDetails?.visibility = View.VISIBLE
            updateAppointmentDetails(details, monthData?.get(index)!!.id)
        }
    }

    override fun showAppointmentDetails(item: AppointmentDetails, view: View?) {
        val booking = BookingAdditionalFields(
                specialtyProvider = item.specialtyProvider,
                appointmentId = item.appointmentId,
                clinicId = item.clinicId,
                specialityName = item.specialty,
                appointmentType = item.title)

        val action = AppointmentFragmentDirections.actionTab2ToNavigationTherapyPlan(item, booking)
        findNavController().navigate(action)
    }
}
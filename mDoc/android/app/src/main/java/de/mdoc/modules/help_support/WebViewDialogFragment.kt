package de.mdoc.modules.help_support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.util.stringArgument
import kotlinx.android.synthetic.main.privacy_layout.*

class WebViewDialogFragment: DialogFragment() {
    private val args:WebViewDialogFragmentArgs by navArgs()
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        return inflater.inflate(R.layout.privacy_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnConfirmPrivacy?.text = getString(R.string.back)
        btnConfirmPrivacy?.setOnClickListener { dismiss() }

        btnCancelPrivacy?.visibility = View.GONE
        checkPrivacy?.visibility = View.GONE

        webView?.settings?.defaultTextEncodingName = "utf-8"

        if(args.htmlContent.isNotEmpty()){
            //if link doesn't have http shouldOverrideUrlLoading won't be triggered and user will end up with with blank screen
            val convertedHtml: String = args.htmlContent.replace("<a href=\"www", "<a href=\"http://www", true)
            webView.loadDataWithBaseURL(null, convertedHtml, "text/html; charset=utf-8", "UTF-8", null)
        }

        webView?.webViewClient=object : WebViewClient(){
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                //this will disable clicks in webview
                return true
            }
        }


    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)
        dialog?.setCanceledOnTouchOutside(true)

        super.onResume()
    }
}

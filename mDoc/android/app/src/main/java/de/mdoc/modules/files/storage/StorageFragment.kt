package de.mdoc.modules.files.storage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentFileStorageBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_file_storage.*

class StorageFragment: NewBaseFragment () {

    val viewModel by viewModel {
        StorageViewModel(RestClient.getService())
    }

    override val navigationItem: NavigationItem
        get() = NavigationItem.Files

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentFileStorageBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_file_storage, container, false)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.storage.observe(viewLifecycleOwner, Observer {
            storageView.setValue(it)
            storageFileCounterView.setValue(it)
        })
    }
}
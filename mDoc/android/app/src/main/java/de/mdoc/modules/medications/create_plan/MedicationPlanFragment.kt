package de.mdoc.modules.medications.create_plan

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMedicationPlanBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.interfaces.TimePeriodListener
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import de.mdoc.modules.medications.create_plan.data.ListItemTypes
import de.mdoc.modules.medications.create_plan.data.ListItems
import de.mdoc.network.RestClient
import de.mdoc.util.DaysInWeek
import de.mdoc.util.handleOnBackPressed
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_medication_plan.*

class MedicationPlanFragment: NewBaseFragment() {

    lateinit var activity: MdocActivity
    private var fragments = arrayListOf<Fragment>()
    private var dailySelected = arrayListOf<Fragment>()
    private var weeklySelected = arrayListOf<Fragment>()
    private var certainPeriodSelected = arrayListOf<Fragment>()

    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar

    private val args: MedicationPlanFragmentArgs by navArgs()

    private val createPlanViewModel by viewModel {
        CreatePlanViewModel(
                RestClient.getService(), args.medication)
    }
    private var timeListener: TimePeriodListener? = null

    fun setTimeListener(timeListener: TimePeriodListener) {
        this.timeListener = timeListener
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMedicationPlanBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_medication_plan, container, false)

        binding.lifecycleOwner = this

        activity = context as MdocActivity
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleOnBackPressed {
            navigateBack()
        }
        createPlanViewModel.medicationId.value = args.medication.id
        createPlanViewModel.form.value = args.form
        createPlanViewModel.medicationName.value = args.medication.description

        fillWeekly()
        observeNetworkResults()
        setupFragments()
        setupViewPager()
        setupClickListeners()
    }

    private fun setupClickListeners() {
        txt_next?.setOnClickListener {
            nextButtonHandler()
        }

        txt_back?.setOnClickListener {
            progress_bar.max = fragments.size

            when (fragments[pager_medication.currentItem]::class.java.canonicalName) {
                DosageFragment.TAG -> {
                    timeListener?.timePeriodSelected()
                    navigateBack()
                }

                else               -> {
                    navigateBack()
                }
            }
        }

        txt_cancel?.setOnClickListener {
           findNavController().popBackStack()
        }
    }

    private fun setupViewPager() {
        pager_medication.setSwipeable(false)
        val adapter =
                CreateMedicationPagerAdapter(fragments, childFragmentManager, createPlanViewModel)
        pager_medication.adapter = adapter
        val currentItem = pager_medication.currentItem

        createPlanViewModel.selectedPeriod.observe(viewLifecycleOwner, Observer {
            when (it) {
                ListItemTypes.DAILY -> {
                    while (fragments.size > 2) {
                        fragments.removeAt(fragments.size - 1)
                    }
                    setStep(currentItem + 1)

                    fragments.addAll(ArrayList<Fragment>(dailySelected))
                    adapter.notifyDataSetChanged()
                }

                ListItemTypes.WEEKLY -> {
                    while (fragments.size > 2) {
                        fragments.removeAt(fragments.size - 1)
                    }
                    setStep(currentItem + 1)

                    fragments.addAll(ArrayList<Fragment>(weeklySelected))

                    adapter.notifyDataSetChanged()
                }

                ListItemTypes.EVERY_CERTAIN_PERIOD -> {
                    while (fragments.size > 2) {
                        fragments.removeAt(fragments.size - 1)
                    }

                    setStep(currentItem + 1)

                    fragments.addAll(ArrayList<Fragment>(certainPeriodSelected))
                    adapter.notifyDataSetChanged()
                }

                ListItemTypes.ON_DEMAND -> {
                    setStep(currentItem)
                }
            }
        })
        setStep(currentItem)

        progress_bar.progress = currentItem + 1
        progress_bar.max = fragments.size
    }

    private fun setupFragments() {
          when (args.type) {
            ListItemTypes.ON_DEMAND -> {
                fragments = arrayListOf(IndicatePeriodFragment(createPlanViewModel), IntakeInformationFragment(createPlanViewModel))
            }
            else -> {
                fragments = arrayListOf(IndicatePeriodFragment(createPlanViewModel),
                        HowOftenFragment(createPlanViewModel))

                dailySelected =
                        arrayListOf(DailyIntakeFragment(createPlanViewModel),
                                WhatTimeFragment(createPlanViewModel),
                                DosageFragment(createPlanViewModel),
                                IntakeInformationFragment(createPlanViewModel))
                weeklySelected =
                        arrayListOf(WeeklyIntakeFragment(createPlanViewModel),
                                DailyIntakeFragment(createPlanViewModel),
                                WhatTimeFragment(createPlanViewModel),
                                DosageFragment(createPlanViewModel),
                                IntakeInformationFragment(createPlanViewModel))
                certainPeriodSelected =
                        arrayListOf(EveryCertainPeriodFragment(createPlanViewModel),
                                DailyIntakeFragment(createPlanViewModel),
                                WhatTimeFragment(createPlanViewModel),
                                DosageFragment(createPlanViewModel),
                                IntakeInformationFragment(createPlanViewModel))

            }
        }
    }

    private fun setStep(currentItem: Int) {
        txt_step?.text = resources.getString(R.string.med_plan_step_of, String.format("%02d", currentItem + 1))

        // Change button title based on pager position
        if (pager_medication.currentItem == fragments.size - 1) {
            txt_next?.text = resources.getString(R.string.med_plan_create_plan)
        }  else {
            txt_next?.text = resources.getString(R.string.next)
        }
    }

    private fun showDialog(message: String) {
        MaterialAlertDialogBuilder(activity).setMessage(message)
            .setPositiveButton(R.string.mdtp_ok
                              ) { _, _ -> }
            .show()
    }
    private fun isHowOftenSelected() = createPlanViewModel.isHowOftenSelected()

    private fun isWhatTimeSelected() = createPlanViewModel.isWhatTimeSelected()

    private fun createMedicationPlan() {
        createPlanViewModel.createPlanRequest()
    }

    private fun createMedicationPlanOnDemand() {
        createPlanViewModel.createPlanRequestOnDemand()
    }

    private fun observeNetworkResults() {
        createPlanViewModel.isPlanCreated.observe(viewLifecycleOwner, Observer {
            if (it) {
                hideKeyboard()
                findNavController().popBackStack(R.id.fragmentSearchMedications, true)
            }
        })
    }

    private fun hideKeyboard (){
        val view = this.view
        view?.let { v ->
            val imm = this.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.hideSoftInputFromWindow(v.windowToken, 0)
        }
    }

    private fun fillWeekly() {
        DaysInWeek.values()
            .forEach {
                createPlanViewModel.weeklyPeriod.value?.add(
                        ListItems(it.getName(activity) as String, code = it.jsonName))
            }
    }

    private fun nextButtonHandler() {
        progress_bar.max = fragments.size

        when (fragments[pager_medication.currentItem]::class.java.canonicalName) {
            HowOftenFragment.TAG          -> {
                if (isHowOftenSelected()) {
                    navigateNext()
                }else{
                    showDialog(getString(R.string.med_plan_intake_frequency_feedback))

                }
            }

            WeeklyIntakeFragment.TAG      -> {
                navigateNext()
                timeListener?.timePeriodSelected()
            }

            DailyIntakeFragment.TAG       -> {
                navigateNext()
                timeListener?.timePeriodSelected()
            }

            WhatTimeFragment.TAG          -> {
                if (isWhatTimeSelected()) {
                    navigateNext()
                }
                else {
                    showDialog(getString(R.string.med_plan_add_atleast_one_time))
                }
            }

            IntakeInformationFragment.TAG -> {
                when (createPlanViewModel.selectedPeriod.value) {
                    ListItemTypes.ON_DEMAND -> {
                        createMedicationPlanOnDemand()
                    } else -> {
                        createMedicationPlan()
                    }
                }
            }

            else -> {
                navigateNext()
            }
        }
    }

     fun navigateNext() {
        pager_medication.currentItem = pager_medication.currentItem + 1
        setStep(pager_medication.currentItem)
        progress_bar.progress = pager_medication.currentItem + 1
    }

    private fun navigateBack() {
        if (pager_medication.currentItem > 0) {
            txt_next?.text = resources.getString(R.string.next)

            pager_medication.currentItem = pager_medication.currentItem - 1
            setStep(pager_medication.currentItem)
            progress_bar.progress = pager_medication.currentItem + 1
        }
        else {
           findNavController().popBackStack()
        }
    }
}
package de.mdoc.modules.medications.export

import android.os.Bundle
import android.view.View
import de.mdoc.modules.common.export.BaseExportFragment
import de.mdoc.network.RestClient
import de.mdoc.network.request.export.MedicationExportRequest
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_goal_export_options.*

class MedicationExportFragment : BaseExportFragment() {

    private val viewModel by viewModel {
        MedicationExportViewModel(RestClient.getService(), this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindVisibleGone(downloadExportProgress, viewModel.isLoading)
    }

    override fun downloadPdf() {
        viewModel.download(MedicationExportRequest(), ExportType.PDF, context)
    }

    override fun downloadXml() {
        viewModel.download(MedicationExportRequest(), ExportType.XLSX, context)
    }
}
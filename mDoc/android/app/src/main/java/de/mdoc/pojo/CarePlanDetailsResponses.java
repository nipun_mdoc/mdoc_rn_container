package de.mdoc.pojo;

import java.util.ArrayList;

public class CarePlanDetailsResponses {

    private String description;
    private String levelType;
    private String name;
    private UnitValue dailyAmount;
    private UnitValue quantity;
    private Value status;
    private ArrayList<RelatedArtifact> relatedArtifact;
    private Resource definition;

    public Resource getDefinition() {
        return definition;
    }

    public void setDefinition(Resource definition) {
        this.definition = definition;
    }

    public ArrayList<RelatedArtifact> getRelatedArtifact() {
        return relatedArtifact;
    }

    public void setRelatedArtifact(ArrayList<RelatedArtifact> relatedArtifact) {
        this.relatedArtifact = relatedArtifact;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UnitValue getDailyAmount() {
        return dailyAmount;
    }

    public void setDailyAmount(UnitValue dailyAmount) {
        this.dailyAmount = dailyAmount;
    }

    public UnitValue getQuantity() {
        return quantity;
    }

    public void setQuantity(UnitValue quantity) {
        this.quantity = quantity;
    }

    public Value getStatus() {
        return status;
    }

    public void setStatus(Value status) {
        this.status = status;
    }
}

package de.mdoc.modules.entertainment.fragments.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.jakewharton.rxbinding.widget.RxSearchView
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentCommonSearchBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.entertainment.adapters.all.MagazinesSeeAllAdapter
import de.mdoc.modules.entertainment.viewmodels.MagazinesSearchViewModel
import de.mdoc.network.RestClient
import de.mdoc.util.hideActionBar
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_common_search.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class MagazinesSearchFragment : NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.EntertainmentSearchMagazines

    private val viewModel by viewModel {
        MagazinesSearchViewModel(RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentCommonSearchBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_common_search, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.searchInputHint = getString(R.string.entertainment_search_magazines)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideActionBar()

        searchEdt.isIconified = false
        viewModel.searchResponse.observe(viewLifecycleOwner, Observer {
            gvSearchItems.visibility = View.VISIBLE
            gvSearchItems.apply {
                adapter = MagazinesSeeAllAdapter(it?.data?.list, context)
            }
        })

        var lastValue = ""
        RxSearchView.queryTextChanges(searchEdt)
                .filter { it.toString() != lastValue }
                .debounce(
                        MdocConstants.SEARCH_WAIT_TIME.toLong(),
                        TimeUnit.MILLISECONDS,
                        AndroidSchedulers.mainThread()
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    lastValue = it.toString()
                    viewModel.requestSearch(it.toString())
                }

        btnCloseSearch.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}

package de.mdoc.util

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Base64
import de.mdoc.R

fun String.toBitmap(): Bitmap? {
    if (this.isNotEmpty()) {
        return try {
            val base64Image = if (this.split(",").size > 1) {
                this.split(",")[1]
            }
            else {
                this.split(",")[0]
            }

            val decodedString = Base64.decode(base64Image, Base64.DEFAULT)
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        } catch (e: IllegalArgumentException) {
            null
        }
    }

    return null
}

fun openLink(link: String?, context: Context) {
    try {
        var tempLink = link
        if (!link!!.startsWith("http://") && !link.startsWith("https://"))
            tempLink = "http://$link";
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(tempLink))
        context.startActivity(browserIntent)
    } catch (e: Exception) {
        MdocUtil.showToastLong(context, context.getString(R.string.my_stay_invalid_link))
    }
}

fun scaleBitmapImage(image: Bitmap, maxWidth: Int, maxHeight: Int): Bitmap {
    var image = image
    return if (maxHeight > 0 && maxWidth > 0) {
        val width = image.width
        val height = image.height
        val ratioBitmap = width.toFloat() / height.toFloat()
        var finalWidth = maxWidth
        var finalHeight: Int = (maxWidth.toFloat() / ratioBitmap).toInt()
        image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true)
        image
    } else {
        image
    }
}

fun extractFirstIntFromString(text: String) : Int {
    val sArray = text.split(" ")
    val num = sArray.find { it.toIntOrNull() is Int }
    // return 0 if no digits found
    return if (num.isNullOrEmpty()) 0 else Integer.parseInt(num)
}
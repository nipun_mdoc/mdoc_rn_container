package de.mdoc.modules.mental_goals.mental_goal_edit

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.MentalGoalStatus
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_goal_status_change.*

class ChangeGoalStatusFragment: MdocFragment() {

    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_goal_status_change
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        hasPreviousSelection()
        setupListeners()
        setupRadioGroupListeners()
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            MentalGoalsUtil.clearGoalStatus()
            findNavController().popBackStack()
        }
        txt_next?.setOnClickListener {
            if (itemIsSelected()) {
                val action = ChangeGoalStatusFragmentDirections.actionChangeGoalStatusFragmentToNoteGoalStatusFragment()
                findNavController().navigate(action)
            }
            else {
                MdocUtil.showToastShort(activity, resources.getString(R.string.msg_goal_status_selection))
            }
        }
        txt_back?.setOnClickListener {
            MentalGoalsUtil.clearGoalStatus()
            findNavController().popBackStack()
        }
    }

    private fun setupRadioGroupListeners() {
        val itemCount = radio_group.childCount
        for (position in 0..itemCount) {
            if (radio_group.getChildAt(position) != null) {
                val constraintLayout = radio_group.getChildAt(position) as ConstraintLayout
                constraintLayout.setOnClickListener {
                    handleUi(constraintLayout)
                }
            }
        }
    }

    private fun handleUi(view: View) {
        clearButtonSelection()

        if (view.backgroundTintList == ColorStateList.valueOf(
                    ContextCompat.getColor(activity, R.color.colorSecondary8))) {
            view.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
        }
        else {
            view.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary8))
        }
    }

    private fun clearButtonSelection() {
        for (index in 0..radio_group.childCount) {
            val view = radio_group.getChildAt(index)
            view?.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary8))
        }
    }

    private fun itemIsSelected(): Boolean {
        val itemCount = radio_group.childCount
        for (position in 0..itemCount) {
            if (radio_group.getChildAt(position) != null) {
                val view = radio_group.getChildAt(position) as ConstraintLayout
                if (view.backgroundTintList == ColorStateList.valueOf(
                            ContextCompat.getColor(activity, R.color.colorSecondary32))) {
                    parseSelection(view)
                    return true
                }
            }
        }
        return false
    }

    private fun parseSelection(view: ConstraintLayout) {
        when (view) {
            cl_fully_achieved     -> {
                MentalGoalsUtil.statusChange = MentalGoalStatus.ACHIEVED.toString()
            }

            cl_partially_achieved -> {
                MentalGoalsUtil.statusChange = MentalGoalStatus.NEARLY_ACHIEVED.toString()
            }

            cl_not_achieved       -> {
                MentalGoalsUtil.statusChange = MentalGoalStatus.NOT_ACHIEVED.toString()
            }

            cl_rejected           -> {
                MentalGoalsUtil.statusChange = MentalGoalStatus.CANCELLED.toString()
            }

            cl_modified           -> {
                MentalGoalsUtil.statusChange = MentalGoalStatus.MODIFIED.toString()
            }
        }
    }

    private fun hasPreviousSelection() {
        clearButtonSelection()
        if (MentalGoalsUtil.statusChange.isNotEmpty()) {
            when (MentalGoalsUtil.statusChange) {
                MentalGoalStatus.ACHIEVED.toString()        -> {
                    cl_fully_achieved.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
                }

                MentalGoalStatus.NEARLY_ACHIEVED.toString() -> {
                    cl_partially_achieved.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
                }

                MentalGoalStatus.NOT_ACHIEVED.toString()    -> {
                    cl_not_achieved.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
                }

                MentalGoalStatus.CANCELLED.toString()       -> {
                    cl_rejected.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
                }

                MentalGoalStatus.MODIFIED.toString()        -> {
                    cl_modified.backgroundTintList =
                            ColorStateList.valueOf(ContextCompat.getColor(activity, R.color.colorSecondary32))
                }
            }
        }
    }
}


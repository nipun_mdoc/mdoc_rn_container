package de.mdoc.modules.files.base

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.NavigationFilesDirections
import de.mdoc.R
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.files.FilesViewModel
import de.mdoc.modules.files.adapters.FilesAdapterCallback
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.files.delete.DeleteFileDialogFragment
import de.mdoc.modules.files.delete.FileDeleteConfirmedListener
import de.mdoc.modules.files.share.ShareFileDialogFragment
import de.mdoc.modules.files.share.ShareFileListener
import de.mdoc.modules.files.utils.FileDownloadManager
import de.mdoc.network.RestClient
import de.mdoc.pojo.FileListItem
import de.mdoc.util.MdocUtil
import de.mdoc.util.showProgressDialog
import de.mdoc.viewmodel.viewModel

abstract class FilesBaseFragment: NewBaseFragment(), FilesAdapterCallback, FileDeleteConfirmedListener, ShareFileListener, FilesBaseFragmentAdditionalFunctions {

    val viewModel by viewModel {
        FilesViewModel(FilesRepository(RestClient.getService(), FileCacheRepository))
    }

    private var downloadManager: FileDownloadManager? = null
    private var fileDeleteProgressDialog: Dialog? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        downloadManager = FileDownloadManager(context)
        downloadManager?.registerReceiver()

        viewModel.isFileDeleteInProgress.observe(viewLifecycleOwner) {
            fileDeleteProgressDialog?.dismiss()
            if (it) {
                fileDeleteProgressDialog = showProgressDialog()
            }
        }

        viewModel.onFileDeleteFailedEvent.observe(viewLifecycleOwner) {
            MdocUtil.showToastShort(context, getString(R.string.cant_delete_file))
        }

        viewModel.onFileDeleted.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                onFileDeleted(it)
            }
        }
    }


    override fun onDeleteFileRequest(item: FileListItem) {
        val deleteFragment = DeleteFileDialogFragment().newInstance(item, this)
        deleteFragment.show(requireFragmentManager(), "Delete fragment")
    }

    override fun onEditFileRequest(item: FileListItem) {
        findNavController().navigate(NavigationFilesDirections.globalToFragmentEdit(item))
    }

    override fun onShareFileRequest(item: FileListItem) {
        val shareFragment = ShareFileDialogFragment().newInstance(item, this)
        shareFragment.show(requireFragmentManager(), "ShareFileDialogFragment")
    }

    override fun onDownloadFileRequest(item: FileListItem) {
        val url = activity?.getString(R.string.base_url) + "v2/common/files/download/" + item.id
        downloadManager?.downloadViaManager(url, item)
    }

    override fun onDeleteFileConfirmed(id: String) {
        viewModel.deleteFile(id)
    }

    override fun onShareFileCompleted() {
        reloadData()
    }


    override fun reloadData() {}

    override fun onFileDeleted(id: String) {}

    override fun onDestroy() {
        super.onDestroy()
        downloadManager?.unregisterReceiver()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        fileDeleteProgressDialog?.dismiss()
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 12/9/16.
 */

public class WeeklyMealChoice implements Serializable {

    private Choice fridayChoice;
    private Choice mondayChoice;
    private Choice saturdayChoice;
    private Choice sundayChoice;
    private Choice thursdayChoice;
    private Choice tuesdayChoice;
    private Choice wednesdayChoice;

    public Choice getFridayChoice() {
        return fridayChoice;
    }

    public void setFridayChoice(Choice fridayChoice) {
        this.fridayChoice = fridayChoice;
    }

    public Choice getMondayChoice() {
        return mondayChoice;
    }

    public void setMondayChoice(Choice mondayChoice) {
        this.mondayChoice = mondayChoice;
    }

    public Choice getSaturdayChoice() {
        return saturdayChoice;
    }

    public void setSaturdayChoice(Choice saturdayChoice) {
        this.saturdayChoice = saturdayChoice;
    }

    public Choice getSundayChoice() {
        return sundayChoice;
    }

    public void setSundayChoice(Choice sundayChoice) {
        this.sundayChoice = sundayChoice;
    }

    public Choice getThursdayChoice() {
        return thursdayChoice;
    }

    public void setThursdayChoice(Choice thursdayChoice) {
        this.thursdayChoice = thursdayChoice;
    }

    public Choice getTuesdayChoice() {
        return tuesdayChoice;
    }

    public void setTuesdayChoice(Choice tuesdayChoice) {
        this.tuesdayChoice = tuesdayChoice;
    }

    public Choice getWednesdayChoice() {
        return wednesdayChoice;
    }

    public void setWednesdayChoice(Choice wednesdayChoice) {
        this.wednesdayChoice = wednesdayChoice;
    }
}

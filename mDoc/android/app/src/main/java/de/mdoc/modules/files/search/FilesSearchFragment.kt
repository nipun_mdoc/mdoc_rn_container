package de.mdoc.modules.files.search

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding.widget.RxSearchView
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentCommonSearchBinding
import de.mdoc.modules.files.adapters.FilesListAdapter
import de.mdoc.modules.files.base.FilesBaseFragment
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.network.RestClient
import de.mdoc.util.hideActionBar
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_common_search.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class FilesSearchFragment : FilesBaseFragment(), View.OnClickListener {

    override val navigationItem: NavigationItem = NavigationItem.FilesSearch

    private val searchViewModel by viewModel {
        FileSearchViewModel(
                FilesRepository(
                        RestClient.getService(),
                        FileCacheRepository
                )
        )
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentCommonSearchBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_common_search, container, false)

        binding.lifecycleOwner = this
        binding.viewModel = searchViewModel
        binding.searchInputHint = getString(R.string.files_search)
        return binding.root
    }

    private var savedRecyclerLayoutState: Parcelable? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideActionBar()
        rvSearchItems.visibility = View.VISIBLE
        searchEdt.isIconified = false
        btnCloseSearch.setOnClickListener(this)
        registerDataObserver()
        registerSearchObserver()
    }

    private fun registerDataObserver() {
        searchViewModel.searchResponse.observe(viewLifecycleOwner, Observer {
            rvSearchItems.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = FilesListAdapter(this@FilesSearchFragment, it?.files?.list, it?.categoryCoding?.list)
                if (savedRecyclerLayoutState != null) {
                    rvSearchItems.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                }
            }
        })
    }

    private fun registerSearchObserver() {
        var lastValue = ""
        RxSearchView.queryTextChanges(searchEdt)
                .filter { it.toString() != lastValue }
                .debounce(
                        MdocConstants.SEARCH_WAIT_TIME.toLong(),
                        TimeUnit.MILLISECONDS,
                        AndroidSchedulers.mainThread()
                )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    lastValue = it.toString()
                    searchViewModel.requestSearch(it.toString())
                }
    }

    override fun reloadData() {
        searchViewModel.requestSearch(searchEdt.query.toString())
    }

    override fun onFileDeleted(id: String) {
        val adapter = rvSearchItems.adapter
        if (adapter is FilesListAdapter) {
            adapter.removeItem(id)
        }
    }

    override fun onStop() {
        super.onStop()
        savedRecyclerLayoutState = rvSearchItems.layoutManager?.onSaveInstanceState()
    }

    override fun onClick(button: View?) {
        when (button) {
            btnCloseSearch -> {
               findNavController().popBackStack()
            }
        }
    }
}

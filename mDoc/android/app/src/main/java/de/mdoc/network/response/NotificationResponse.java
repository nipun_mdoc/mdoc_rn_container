package de.mdoc.network.response;

import de.mdoc.pojo.NotificationData;

/**
 * Created by ema on 7/14/17.
 */

public class NotificationResponse extends MdocResponse{


    private NotificationData data;

    public NotificationData getData() {
        return data;
    }

    public void setData(NotificationData data) {
        this.data = data;
    }
}

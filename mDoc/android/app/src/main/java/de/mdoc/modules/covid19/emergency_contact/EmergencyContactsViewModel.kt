package de.mdoc.modules.covid19.emergency_contact

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.profile.data.Contact
import de.mdoc.pojo.PublicUserDetails
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class EmergencyContactsViewModel(private val mDocService: IMdocService): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    var publicUserDetails: MutableLiveData<PublicUserDetails> = MutableLiveData(PublicUserDetails())
    var emergencyContacts: MutableLiveData<List<Contact>> = MutableLiveData(listOf())
    var emergencyInfo: MutableLiveData<String> = MutableLiveData("")

    init {
        getPublicUserDetails()
    }

    private fun getPublicUserDetails() {
        viewModelScope.launch {
            try {
                val response = mDocService.getPatientDetails()
                    .data
                publicUserDetails.value = response.publicUserDetails ?: PublicUserDetails()
                emergencyContacts.value =
                        publicUserDetails.value?.emergencyContacts?.filter { !it.phone.isNullOrEmpty() } ?: listOf()
                emergencyInfo.value = response.extendedUserDetails.emergencyInfo
            } catch (e: java.lang.Exception) {
                Timber.e(e)
            }
        }
    }
}
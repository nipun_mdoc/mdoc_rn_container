package de.mdoc.modules.appointments.adapters.viewholders.day

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseDayAppointmentViewHolder
import de.mdoc.pojo.AppointmentDetails

class MetaDataDayViewHolder(itemView: View, private val isFromWidget: Boolean) : BaseDayAppointmentViewHolder(itemView), View.OnClickListener {

    init {
        _dayItemPlaceHolder.setOnClickListener(this)
    }

    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        super.bind(item, context, appointmentCallback, hasHeader)

        _txtMetaDataStatus.visibility = View.VISIBLE
        _txtUpdateMessage.visibility = View.GONE

        prepareUI(item, context, isFromWidget)

        when {
            item.isCanceled -> {
                _txtMetaDataStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.custom_bg))
                _txtMetaDataStatus.setText(R.string.appointment_canceled)
                _txtMetaDataStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error_48_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.color_404040))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.color_7a7a7a))

            }
            item.isMetaDataPopulated -> {
                _txtMetaDataStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSecondary32))
                _txtMetaDataStatus.setText(R.string.appointment_metadata_populated)
                _txtMetaDataStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle_24_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))

            }
            item.isMetaDataNeeded -> {
                _txtMetaDataStatus.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSecondary32))
                _txtMetaDataStatus.text = MdocConstants.appointment_title
//                _txtMetaDataStatus.setText(R.string.appointment_metadata_needed)
                _txtMetaDataStatus.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_report_problem_24_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
            }
        }
    }

    override fun onClick(view: View?) {
        when (view) {
            _dayItemPlaceHolder -> {
                showAppointmentDetailsWithTabletCheck(view)
            }
        }
    }
}
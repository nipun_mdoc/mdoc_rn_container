package de.mdoc.modules.external_services

import android.content.Context
import de.mdoc.modules.external_services.data.ExternalServiceItem
import de.mdoc.util.openLink

class ExternalServicesHandler {

    fun onOpenLinkClick(link: String, context: Context) {
        openLink(link, context)
    }

    fun hasPartnerLogo(item: ExternalServiceItem):Boolean{
        return item.partner!=null
    }
}
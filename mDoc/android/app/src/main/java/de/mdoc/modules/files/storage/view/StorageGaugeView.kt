package de.mdoc.modules.files.storage.view

import android.animation.ValueAnimator
import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import de.mdoc.R
import de.mdoc.modules.files.Config
import de.mdoc.network.response.files.ResponseSearchResultsFileStorageDocumentImpl_
import kotlinx.android.synthetic.main.gauge_view.view.*


class StorageGaugeView(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.gauge_view, this)
    }

    fun setValue(response: ResponseSearchResultsFileStorageDocumentImpl_) {

        val storage = response.data?.list?.last()
        val taken = storage?.takenStorageSize ?: 0
        val max = storage?.storageSize ?: 0

        // Progress calculation
        val calculation: Float = (taken.toFloat() / (max.toInt()))
        val progress = calculation * 100

        // Percentage of storage usage
        storageGaugeView.setValue(progress)

        // Current storage usage
        val currentGb = convertBytesToGb(taken.toFloat())
        txtStorageCurrent.text = formatGB(currentGb)

        // Animate
        startCountAnimation(currentGb)

        // Max storage space
        val maxGb = convertBytesToGb(max.toFloat())
        txtStorageMax.text = formatGB(maxGb)
    }


    private fun convertBytesToGb(value: Float?): Float {
        return when (value) {
            null -> return 0.0f
            else -> value.div(1024).div(1024).div(1024)
        }
    }

    private fun startCountAnimation(maxValue: Float) {
        val animator = ValueAnimator.ofFloat(0.0f, maxValue)
        animator.duration = Config.STORAGE_ANIMATION_DURATION
        animator.addUpdateListener { animation ->
            txtStorageUsedCenter.text = formatGB(animation.animatedValue as Float)
        }
        animator.start()
    }

    private fun formatGB(value: Float?): String {
        return String.format("%.2f", value) + " GB"
    }
}
package de.mdoc.modules.vitals.charts

import com.github.mikephil.charting.data.BarEntry
import de.mdoc.modules.devices.data.Data
import de.mdoc.modules.devices.data.ObservationPeriodType
import java.util.*

fun compositeObservationToBarEntry(input: ArrayList<Data>?, periodType: ObservationPeriodType): ArrayList<ArrayList<BarEntry>> {
    val time: Calendar = Calendar.getInstance()
    val mainOutput: ArrayList<ArrayList<BarEntry>> = arrayListOf()

    input?.forEach{mainItem ->
        val compositObjectList= mainItem.data.list
        val output: ArrayList<BarEntry> = arrayListOf()

        for (item in compositObjectList) {
            time.timeInMillis = item.period
            var xAxisValue = 0

            when (periodType) {
                ObservationPeriodType.DAY -> {
                    xAxisValue = time.get(Calendar.HOUR_OF_DAY)
                }
                ObservationPeriodType.WEEK -> {
                    xAxisValue = time.get(Calendar.DAY_OF_WEEK) - 2

                    if (xAxisValue == -1) xAxisValue = 6
                }
                ObservationPeriodType.MONTH -> {
                    xAxisValue = time.get(Calendar.DAY_OF_MONTH)

                }
                ObservationPeriodType.YEAR -> {
                    xAxisValue = time.get(Calendar.MONTH)
                }
                else -> {
                }
            }

            val entryElement = BarEntry(xAxisValue.toFloat(), item.avg.toFloat())
            output.add(entryElement)
        }
        val sortedOutput: ArrayList<BarEntry> = output
        sortedOutput.sortBy { it.x }

        mainOutput.add(sortedOutput)
    }

    return mainOutput
}
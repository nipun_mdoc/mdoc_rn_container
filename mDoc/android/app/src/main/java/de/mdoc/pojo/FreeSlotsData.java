package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FreeSlotsData {

    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("list")
    @Expose
    private ArrayList<FreeSlotsList> list = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<FreeSlotsList> getList() {
        return list;
    }

    public void setList(ArrayList<FreeSlotsList> list) {
        this.list = list;
    }
}

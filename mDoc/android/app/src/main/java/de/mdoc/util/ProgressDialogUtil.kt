package de.mdoc.util

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import de.mdoc.R


class ProgressDialog(context: Context) : Dialog(context) {
    init {
        window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
        window?.setDimAmount(0f)
        setContentView(R.layout.progress_dialog)
    }
}
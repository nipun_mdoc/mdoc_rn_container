package de.mdoc.modules.mental_goals.mental_goal_edit

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.modules.mental_goals.data_goals.SingleMentalGoalResponse
import de.mdoc.modules.mental_goals.mental_goal_adapters.ActionItemsAdapter
import de.mdoc.modules.mental_goals.mental_goal_adapters.IfThenAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.onCreateOptionsMenu
import kotlinx.android.synthetic.main.fragment_edit_goal.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EditGoalFragment: MdocBaseFragment() {

    lateinit var activity: MdocActivity
    private var adapterItems = arrayListOf<IfThenPlan>()
    private var actionsAdapterItems = arrayListOf<ActionItem>()
    private val ifThenItems: ArrayList<IfThenPlan> = arrayListOf()
    private val actionItems: ArrayList<ActionItem> = arrayListOf()

    override fun setResourceId(): Int {
        return R.layout.fragment_edit_goal
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupListeners()
        fillUiWithPersistedData()
        handleOnBackPressed {
            findNavController().popBackStack(R.id.goalDetailFragment, false)
        }
    }

    private fun setupListeners() {
        cl_change_date?.setOnClickListener {
            val action = EditGoalFragmentDirections.actionEditGoalFragmentToGoalPeriodFragment2(true)
            findNavController().navigate(action)
        }
    }

    private fun updateMentalGoalApi() {
        val request = MentalGoalsRequest()
        val goalId = MentalGoalsUtil.goalId

        with(request) {
            title = et_goal_title?.text.toString()
            description = et_goal_detail?.text.toString()
            dueDate = MentalGoalsUtil.timePeriod
            ifThenPlans = ifThenItems
            items = actionItems
        }

        MdocManager.updateMentalGoal(goalId, request, object: Callback<SingleMentalGoalResponse> {
            override fun onResponse(
                    call: Call<SingleMentalGoalResponse>,
                    response: Response<SingleMentalGoalResponse>
                                   ) {
                if (response.isSuccessful) {
                    MentalGoalsUtil.clearGoalStatus()
                    findNavController().navigate(R.id.navigation_mental_goal)
                }
                else {
                    MdocUtil.showToastLong(activity, response.message())
                }
            }

            override fun onFailure(call: Call<SingleMentalGoalResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
            }
        })
    }

    private fun fillUiWithPersistedData() {
        et_goal_title?.setText(MentalGoalsUtil.goalTitle)
        et_goal_detail?.setText(MentalGoalsUtil.goalDescription)
        txt_due_date?.text = MdocUtil.getDateFromMS(
                MdocConstants.D_IN_WEEK_DD_MM_YY_FORMAT,
                MentalGoalsUtil.timePeriod
                                                   )

        initActionsAdapter()
        initIfThenAdapter()
    }

    private fun initIfThenAdapter() {
        adapterItems = MentalGoalsUtil.ifThenItems ?: arrayListOf()

        ifThenItems.clear()
        ifThenItems.addAll(adapterItems.map { it.copy() })

        if (!adapterItems.isNullOrEmpty()) {
            val ifThenAdapter = IfThenAdapter(activity, ifThenItems, true)
            rv_adapter?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            rv_adapter?.adapter = ifThenAdapter
        }
    }

    private fun initActionsAdapter() {
        actionsAdapterItems = MentalGoalsUtil.actionItems ?: arrayListOf()
        actionItems.clear()
        actionItems.addAll(actionsAdapterItems.map { it.copy() })

        if (!actionsAdapterItems.isNullOrEmpty()) {
            val actionItemAdapter = ActionItemsAdapter(activity, actionItems, true)
            rv_action_adapter?.layoutManager =
                    LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            rv_action_adapter?.adapter = actionItemAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_mental_goals_edit)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.save) {
            if (MentalGoalsUtil.hasEmptyIfThenFields(ifThenItems) || MentalGoalsUtil.hasEmptyActionItems(
                        actionItems
                                                                                                        )
                || et_goal_title.text.isEmpty()
                || et_goal_detail.text.isEmpty()
            ) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.fill_all_fields))
            }
            else {
                updateMentalGoalApi()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
package de.mdoc.activities.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.mdoc.util.FullScreenDialogFragment
import kotlinx.android.synthetic.main.fragment_biometric_prompt_dialog.*

interface BiometricPromptDialogFragmentCallback {
    fun onBiometricPromptDialogButtonClick(isPositiveButton: Boolean = false)
}

    class BiometricPromptDialogFragment(private val listener: BiometricPromptDialogFragmentCallback):
        FullScreenDialogFragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        return inflater.inflate(de.mdoc.R.layout.fragment_biometric_prompt_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setOnClickListeners()
    }

    private fun setOnClickListeners() {
        txtEnableBiometric.setOnClickListener {
            listener.onBiometricPromptDialogButtonClick(true)
            dismiss()
        }
        txtNotNow.setOnClickListener {
            listener.onBiometricPromptDialogButtonClick()
            dismiss()
        }
    }
}
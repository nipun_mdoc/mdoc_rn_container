package de.mdoc.data.responses

import de.mdoc.data.configuration.ConfigurationData

data class ConfigurationResponse(
        val data:ConfigurationData
)
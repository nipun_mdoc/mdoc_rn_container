package de.mdoc.modules.patient_journey.journey_details

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentJourneyDetailsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.patient_journey.adapters.EventAdapter
import de.mdoc.modules.patient_journey.data.ChildCarePlan
import de.mdoc.network.RestClient
import de.mdoc.util.setActionTitle
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_journey_details.*
import org.joda.time.DateTime

class JourneyDetailsFragment: NewBaseFragment() {
    override val navigationItem: NavigationItem = NavigationItem.PatientJourney
    val args: JourneyDetailsFragmentArgs by navArgs()
    var adapterItems: List<ChildCarePlan> = listOf()
    private var smoothScroller: LinearSmoothScroller? = null
    private var eventAdapter: EventAdapter? = null
    private val journeyDetailsViewModel by viewModel {
        JourneyDetailsViewModel(
                RestClient.getService(), args.carePlanId ?: "")
    }
    var phasePosition = 0
    var popPosition = -1

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentJourneyDetailsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_journey_details, container, false)

        binding.lifecycleOwner = this
        binding.journeyDetailsViewModel = journeyDetailsViewModel
        binding.description = args.description
        binding.isFromDashboard = args.isFromDashboard

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setActionTitle(args.parentTitle)
        rv_event?.layoutManager = LinearLayoutManager(activity)
        eventAdapter = EventAdapter(activity as Context)
        rv_event?.adapter = eventAdapter

        ViewCompat.setNestedScrollingEnabled(rv_event, false)

        journeyDetailsViewModel.carePlan.observe(viewLifecycleOwner, Observer { carePlan ->
            if (carePlan.childCarePlanList != null && carePlan.childCarePlanList.isNotEmpty()) {
                if (carePlan.childCarePlanList.getOrNull(0)?.title != "manual") {
                    carePlan.childCarePlanList.add(0, ChildCarePlan(title = "manual", description = args.description))
                }
                if (carePlan.childCarePlanList.getOrNull(1)
                        ?.codingDisplayBySystem(TIMING) == PRE_MAIN_CODE) {
                    carePlan.childCarePlanList[1].preOrPost = "pre"
                }
                carePlan.childCarePlanList.forEachIndexed { index, child ->
                    if (carePlan.childCarePlanList[index].isExpanded) {
                        popPosition = index
                    }

                    if (child.codingDisplayBySystem(TIMING) == MAIN_CODE) {
                        carePlan.childCarePlanList.getOrNull(index + 1)
                            ?.preOrPost = "post"
                    }
                    val startTime = DateTime(child.period?.start ?: 0)
                    val endTime = DateTime(child.period?.end)

                    if (startTime.withHourOfDay(0)
                                    .withMinuteOfHour(0)
                                    .withSecondOfMinute(0).isBeforeNow && endTime.withHourOfDay(23)
                                    .withMinuteOfHour(59)
                                    .withSecondOfMinute(59).isAfterNow) {
                        phasePosition = if (phasePosition == 0 || DateUtils.isToday(child.period?.start)) {
                            child.isExpanded = true
                            index
                        } else phasePosition
                    }
                }
                adapterItems = carePlan.childCarePlanList
                eventAdapter?.setItems(adapterItems)
                smoothScrollToPosition()

                if (popPosition > 0) {
                    popScroll(popPosition)
                }
            }
        })
        btn_scroll_to?.setOnClickListener {
            smoothScrollToPosition()
        }

        rv_event.addOnScrollListener(object: RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val firstItem = (rv_event?.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
                val lastItem = (rv_event?.layoutManager as LinearLayoutManager).findLastVisibleItemPosition()

                if (phasePosition in firstItem..lastItem) {
                    if (btn_scroll_to.isShown) {
                        btn_scroll_to?.visibility = View.GONE
                    }
                }
                else {
                    if (!btn_scroll_to.isShown) {
                        btn_scroll_to?.visibility = View.VISIBLE
                    }
                }
            }
        })
    }

    private fun smoothScrollToPosition() {
        if (smoothScroller == null) {
            smoothScroller = object: LinearSmoothScroller(context) {
                override fun getVerticalSnapPreference(): Int {
                    return SNAP_TO_START
                }
            }
        }
        smoothScroller?.targetPosition = phasePosition
        rv_event?.layoutManager?.startSmoothScroll(smoothScroller)
    }

    private fun popScroll(position: Int) {
        if (smoothScroller == null) {
            smoothScroller = object: LinearSmoothScroller(context) {
                override fun getVerticalSnapPreference(): Int {
                    return SNAP_TO_START
                }
            }
        }
        Handler().postDelayed({
            smoothScroller?.targetPosition = position
            rv_event?.layoutManager?.startSmoothScroll(smoothScroller)
        }, 200)
    }

    companion object {
        const val TIMING = "http://hl7.org/fhir/ValueSet/timing"
        const val MAIN_CODE = "main_event"
        const val PRE_MAIN_CODE = "before_main_event"
    }
}
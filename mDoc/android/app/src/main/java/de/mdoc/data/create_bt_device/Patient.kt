package de.mdoc.data.create_bt_device

data class Patient(
        val identifier: Identifier,
        val reference: String
)
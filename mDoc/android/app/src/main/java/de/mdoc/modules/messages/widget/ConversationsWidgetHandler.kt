package de.mdoc.modules.messages.widget

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.View
import androidx.core.text.HtmlCompat
import androidx.navigation.findNavController
import de.mdoc.R

class ConversationsWidgetHandler {
    fun openMessagesModule(view: View) {
        view.findNavController().navigate(R.id.navigation_messages)
    }

    fun getNewMessagesCountText(context: Context, newMessageCount : Any) : Any =
        when(newMessageCount) {
            0 -> context.getString(R.string.messages_show_all_messages)
            1,2,3,4,5,6,7,8,9,10 -> if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(String.format(context.getString(R.string.messages_new_messages), newMessageCount), Html.FROM_HTML_MODE_COMPACT)
            } else {
                HtmlCompat.fromHtml(String.format(context.getString(R.string.messages_new_messages), newMessageCount), HtmlCompat.FROM_HTML_MODE_LEGACY)
            }
            else -> context.getString(R.string.messages_ten_plus_new_messages)
        }

}
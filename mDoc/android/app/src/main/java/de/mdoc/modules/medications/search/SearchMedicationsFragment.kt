package de.mdoc.modules.medications.search

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentSearchMedicationsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.search.data.MedicationData
import de.mdoc.network.RestClient
import de.mdoc.util.debounce
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_search_medications.*

class SearchMedicationsFragment: NewBaseFragment() {

    private val args: SearchMedicationsFragmentArgs by navArgs()

    private val searchMedicationsViewModel by viewModel {
        SearchMedicationsViewModel(RestClient.getService())
    }
    var oldQuery = ""
    lateinit var activity: MdocActivity
    override val navigationItem: NavigationItem = NavigationItem.Medication

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentSearchMedicationsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_search_medications, container, false)
        activity = context as MdocActivity
        binding.lifecycleOwner = this
        binding.searchMedicationsViewModel = searchMedicationsViewModel
        binding.handler = MedicationSearchHandler(args.type)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observeLoading()
        observeSearchParameters()
        observeTimeoutRequest()

        add_manually?.setOnClickListener {
            if (searchMedicationsViewModel.query.value.isNullOrEmpty()) {
                Toast.makeText(
                        context,
                        resources.getString(R.string.med_plan_medication_name_message),
                        Toast.LENGTH_LONG).show()
            }
            else {
                openManualEntry()
            }
        }
        et_search.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                searchMedicationsViewModel.searchRequest()
                return@OnKeyListener true
            }
            false
        })

    }

    private fun observeSearchParameters() {
        handleSearchBox()
//        debounceSearchQuery()
    }

    private fun handleSearchBox() {
        searchMedicationsViewModel.query.observe(viewLifecycleOwner, Observer {
            when {
                it.isEmpty()  -> {
                    searchMedicationsViewModel.clearContent()
                    et_layout?.endIconDrawable = null
                    oldQuery = ""
                }

                it.length > 1 -> {
                    et_layout?.endIconDrawable = ContextCompat.getDrawable(activity, R.drawable.ic_close_24dp)
                    et_layout?.setEndIconOnClickListener {
                        searchMedicationsViewModel.query.value = ""
                        oldQuery = ""
                        searchMedicationsViewModel.clearContent()
                    }
                }

                else          -> {
                    et_layout?.endIconDrawable = null
                }
            }
        })
    }

    private fun debounceSearchQuery() {
        searchMedicationsViewModel.query
            .observe(viewLifecycleOwner, Observer { query ->
                if (query.length > 1) {
                    if (oldQuery != query) {
                        oldQuery = query
                    }
                }
            })
    }

    private fun observeTimeoutRequest() {
        searchMedicationsViewModel.requestTimeout.observe(viewLifecycleOwner, Observer
        {
            if (it) {
                Toast.makeText(activity, getString(R.string.med_plan_request_timeout), Toast.LENGTH_LONG)
                    .show()
                searchMedicationsViewModel.requestTimeout.value = false
                Toast.LENGTH_LONG
            }
        })
    }

    private fun observeLoading() {
        searchMedicationsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            if (it) {
                findNavController().popBackStack(R.id.fragmentSearchMedications, false)
                findNavController().navigate(R.id.fragmentSearchBlocker)
            } else {
                findNavController().popBackStack(R.id.fragmentSearchMedications, false)
            }
        })
    }

    private fun openManualEntry() {
        val data =  MedicationData(
                description = searchMedicationsViewModel.query.value ?: "manual",
                manualEntry = true,
                unit = getString(R.string.med_plan_no_description_available),
                code = getString(R.string.med_plan_not_available))

        val direction = SearchMedicationsFragmentDirections.actionToFragmentConfirmMedication(data, args.type)
        findNavController().navigate(direction)
    }
}
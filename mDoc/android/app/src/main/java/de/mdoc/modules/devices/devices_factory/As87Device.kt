package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil

class As87Device(val gatt: BluetoothGatt, val data: ArrayList<Byte>) : BluetoothDevice {


    override fun getDeviceObservations(list: ArrayList<Byte>, deviceId: String, updateFrom: Long?): ArrayList<Observation> {
        val userId = MdocAppHelper.getInstance().userId

        val codingForCategory = Coding("fitness", "Fitness Data", "http://hl7.org/fhir/observation-category")
        val codingForCategoryList = listOf(codingForCategory)
        val category = Category(codingForCategoryList)
        val categoryList = listOf(category)

        val stepsCoding = Coding("55423-8", "Step Count", "http://loinc.org")
        val stepsCodingList = listOf(stepsCoding)
        val spoCode = Code(stepsCodingList)

        val performer = Performer("user/$userId")
        val performerList = listOf(performer)
        val subject = Subject("user/$userId")
        val text = Text("<div>[name] : [value] [hunits] @ [date]</div>", "GENERATED")

        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var observedItem: Observation

        var year1 = ""
        var year2 = ""
        var mm = ""
        var step1 = ""
        var step2 = ""
        var step3 = ""
        var dateInMS: Long = 0

        val readsByGroup = list.chunked(12)

        for (chunk in readsByGroup) {

            for ((index, byte) in chunk.withIndex()) {

                when (index) {
                    4 -> {
                        year1 = String.format("%02X", byte)
                    }
                    5 -> {
                        year2 = String.format("%02X", byte)
                    }
                    6 -> {
                        mm = String.format("%02X", byte)
                    }
                    7 -> {
                        val dd = String.format("%02X", byte)

                        val hexYear = year1 + year2

                        val year = java.lang.Long.parseLong(hexYear, 16)
                        val month = java.lang.Long.parseLong((mm), 16)
                        val day = java.lang.Long.parseLong(dd, 16)

                        dateInMS = MdocUtil.getTimeInDays(year.toInt(), month.toInt()-1, day.toInt())
                    }
                    8 -> {
                        step1 = String.format("%02X", byte)
                    }
                    9 -> {
                        step2 = String.format("%02X", byte)
                    }
                    10 -> {
                        step3 = String.format("%02X", byte)
                    }
                    11 -> {
                       val step4 = String.format("%02X", byte)

                        val hexSteps = step1 + step2 + step3 + step4

                        val steps = java.lang.Long.parseLong(hexSteps, 16)

                        observedItem = Observation(categoryList
                                , spoCode
                                , Device("device/$deviceId")
                                , dateInMS.toString()
                                , performerList, "Observation"
                                , "FINAL"
                                , subject
                                , text
                                , ValueQuantity("-", "http://unitsofmeasure.org", "count", steps.toDouble()), "AS87")

                        if (updateFrom == null) {
                            observations.add(observedItem)
                        } else {
                            if (dateInMS > updateFrom) {
                                observations.add(observedItem)
                            }
                        }
                    }
                }
            }
        }
        return observations    }


    override fun deviceAddress(): String {
        return gatt.device.address
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data
    }

    override fun deviceName(): String {
        return gatt.device.name
    }
}

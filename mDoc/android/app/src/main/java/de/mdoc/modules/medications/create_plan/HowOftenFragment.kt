package de.mdoc.modules.medications.create_plan

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentHowOftenBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import kotlinx.android.synthetic.main.fragment_how_often.*



class HowOftenFragment internal constructor(val viewModel: CreatePlanViewModel): NewBaseFragment() {
    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar
    lateinit var activity: MdocActivity

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentHowOftenBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_how_often, container, false)

        activity = context as MdocActivity
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupHowOften()
    }

    private fun setupHowOften() {
        how_often_group?.removeAllViews()
        viewModel.howOften.value?.forEachIndexed { index, listItem ->
            val childView =
                    LayoutInflater.from(context)
                        .inflate(R.layout.placeholder_checkbox_item, null)
            val placeholder: ConstraintLayout = childView.findViewById(R.id.placeholder)
            placeholder.visibility=View.VISIBLE
            val label: TextView = childView.findViewById(R.id.label)

            placeholder.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, listItem.color))

            label.text = resources.getText(listItem.text as Int)

            how_often_group.addView(childView)
            placeholder.setOnClickListener {
                viewModel.deleteFutureSelection()


                viewModel.selectedPeriod.value = listItem.type

                when (index) {
                    0 -> viewModel.periodUnit.value = "D"
                    1 -> viewModel.periodUnit.value = "WK"
                }
                deselectAll()

                viewModel.howOftenSelected(listItem)

                placeholder.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(activity, listItem.color))
                navigateNext()
            }
        }
    }

    private fun deselectAll() {
        deselectHowOften()
        for (index in 0..how_often_group.childCount) {
            if (how_often_group.getChildAt(index) is LinearLayout) {
                val linearLayout: LinearLayout = how_often_group.getChildAt(index) as LinearLayout
                for (ind in 0..linearLayout.childCount) {
                    if (linearLayout.getChildAt(ind) is ConstraintLayout) {
                        val constraintLayout: ConstraintLayout =
                                linearLayout.getChildAt(ind) as ConstraintLayout
                        constraintLayout.backgroundTintList = ColorStateList.valueOf(
                                ContextCompat.getColor(activity, R.color.colorSecondary8))
                    }
                }
            }
        }
    }

    private fun deselectHowOften() {
        viewModel.howOften.value?.forEach {
            it.selected = false
            it.color = R.color.colorSecondary8
        }
    }
    private fun navigateNext(){
        val parent = parentFragment as MedicationPlanFragment?
        parent?.navigateNext()
    }

    companion object {
        val TAG = HowOftenFragment::class.java.canonicalName
    }
}

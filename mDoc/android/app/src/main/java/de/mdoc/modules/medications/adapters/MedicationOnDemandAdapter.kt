package de.mdoc.modules.medications.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.medications.MedicationsViewModel
import de.mdoc.modules.medications.adapters.viewholders.MedicationOnDemandViewHolder
import de.mdoc.modules.medications.data.MedicationAdministrationResponse

class MedicationOnDemandAdapter(private val data: MedicationAdministrationResponse, private val viewModel: MedicationsViewModel) :
        RecyclerView.Adapter<MedicationOnDemandViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MedicationOnDemandViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_medication_dashboard, parent, false)
        return MedicationOnDemandViewHolder(view)
    }

    override fun onBindViewHolder(holder: MedicationOnDemandViewHolder, position: Int) {
        val item = data.data?.list?.get(position)
        item?.let { holder.bind(it, viewModel) }
    }

    override fun getItemCount(): Int {
        return data.data?.list?.size ?: 0
    }
}



package de.mdoc.modules.files.data

import de.mdoc.network.request.CodingRequest
import de.mdoc.network.request.SearchFileRequest
import de.mdoc.network.response.SearchFilesResponse
import de.mdoc.pojo.CodingData
import de.mdoc.service.IMdocService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

private const val SYSTEM_ID = "https://mdoc.one/hl7/fhir/fileCategories"

class FilesRepository(
    private val service: IMdocService,
    private val cache: FileCacheRepository
) {

    private val disposables = CompositeDisposable()

    private fun getCategoryCodingFromServer(): Single<CodingData> {
        return service.rxGetCoding(CodingRequest(SYSTEM_ID))
            .map {
                it.data
            }.subscribeOn(Schedulers.io())
            .doOnSuccess {
                cache.storeCategoryCoding(SYSTEM_ID, it)
            }.subscribeOn(AndroidSchedulers.mainThread())
    }

    fun getCategoryCoding(onSuccess: (CodingData) -> Unit, onError: (Throwable) -> Unit) {
        val cached = cache.getCategoryCoding(SYSTEM_ID)
        if (cached != null) {
            onSuccess(cached)
        }
        getCategoryCodingFromServer().subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, {
                if (cached == null) {
                    onError(it)
                } else {
                    Timber.w(it)
                }
            })
            .addTo(disposables)
    }

    fun getFilesData(query: SearchFileRequest, onSuccess: (FilesData) -> Unit, onError: (Throwable) -> Unit) {
        Single.zip(
            service.searchFiles(query),
            getCategoryCodingFromServer(),
            BiFunction { files: SearchFilesResponse, codingData: CodingData ->
                FilesData(files.data, codingData)
            }
        ).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(onSuccess, onError)
            .addTo(disposables)
    }

    fun deleteFile(id: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        service.deleteFile(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                onSuccess.invoke()
            }, onError)
            .addTo(disposables)
    }

    fun recycle() {
        disposables.dispose()
    }

}
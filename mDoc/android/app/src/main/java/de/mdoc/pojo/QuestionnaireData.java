package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

import de.mdoc.util.MdocAppHelper;

/**
 * Created by ema on 12/23/16.
 */

public class QuestionnaireData implements Serializable {

    private String clinicId;
    private Map<String, String> closingMessage; //need
    private Long created;
    private String department;
    private String diagnosis;
    private Map<String, String> introduction;
    private ArrayList<Diagnose> diagnosisIds;
    private Map<String, String> name;
    private String pollId; //need
    private String pollAssignId;
    private String pollType; //need
    private String premium;
    private String checklistType;
    private int version;
    private AnswearDetails answerDetails; // need
    private boolean active;
    private Long lastUpdate;
    private String pollTypeDisplayValue;
    private ArrayList<String> supportedLanguages;
    private String defaultLanguage;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public String getChecklistType() {
        return checklistType;
    }

    public void setChecklistType(String checklistType) {
        this.checklistType = checklistType;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getClosingMessage() {
        return closingMessage.get(defaultLanguage);
    }

    public Long getCreated() {
        return created;
    }

    public void setCreated(Long created) {
        this.created = created;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getIntroduction() {
        return introduction.get(defaultLanguage);
    }

    public String getName() {
        return name.get(defaultLanguage);
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getPollType() {
        return pollType;
    }

    public void setPollType(String pollType) {
        this.pollType = pollType;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public ArrayList<Diagnose> getDiagnosisIds() {
        return diagnosisIds;
    }

    public void setDiagnosisIds(ArrayList<Diagnose> diagnosisIds) {
        this.diagnosisIds = diagnosisIds;
    }

    public AnswearDetails getAnswearsDetails() {
        return answerDetails;
    }

    public void setAnswearsDetails(AnswearDetails answearsDetails) {
        this.answerDetails = answearsDetails;
    }

    public AnswearDetails getAnswerDetails() {
        return answerDetails;
    }

    public void setAnswerDetails(AnswearDetails answerDetails) {
        this.answerDetails = answerDetails;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isInSameDepartment(){
        return MdocAppHelper.getInstance().getDepartment().equals(department);
    }

    public String getPollTypeDisplayValue() {
        return pollTypeDisplayValue;
    }

    public void setPollTypeDisplayValue(String pollTypeDisplayValue) {
        this.pollTypeDisplayValue = pollTypeDisplayValue;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
}

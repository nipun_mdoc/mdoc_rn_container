package de.mdoc.util.views

import android.content.Context
import android.content.res.ColorStateList
import android.util.AttributeSet
import androidx.core.content.ContextCompat
import com.google.android.material.button.MaterialButton
import de.mdoc.R
import de.mdoc.util.ColorUtil

class MdocButton (context: Context, attrs: AttributeSet): MaterialButton(context, attrs) {

    init {

        // Colors
        val contrastColor = ColorUtil.contrastColor(resources)
        val primaryColor = ContextCompat.getColor(context, R.color.colorPrimary)
        val buttonDisabledColor = ContextCompat.getColor(context, R.color.light_gray_b2b2b2)

        // Text color
        this.setTextColor(contrastColor)

        // Compounds drawable tint
        this.compoundDrawableTintList = ColorStateList.valueOf(contrastColor)

        // Button states
        val states = arrayOf(intArrayOf(android.R.attr.state_enabled), intArrayOf(-android.R.attr.state_enabled))
        val colors = intArrayOf(primaryColor, buttonDisabledColor)
        backgroundTintList = ColorStateList(states, colors)
    }
}
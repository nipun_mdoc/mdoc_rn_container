package de.mdoc.modules.devices.available_devices

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.storage.AppPersistence.deviceConfiguration
import de.mdoc.util.handleOnBackPressed
import kotlinx.android.synthetic.main.fragment_available_devices.*


class AvailableDevicesFragment : MdocBaseFragment() {

    private val args: AvailableDevicesFragmentArgs by navArgs()

    override fun setResourceId(): Int {
        return R.layout.fragment_available_devices
    }

    override fun init(savedInstanceState: Bundle?) {

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleOnBackPressed {
           findNavController().popBackStack(R.id.fragmentDevices, true)
        }
        setupPagerAdapter()
    }

    private fun setupPagerAdapter(){
        pager_devices?.adapter= DevicePagerAdapter(childFragmentManager, context, tabFragments())
        tab_layout?.setupWithViewPager(pager_devices)

        if(args.isFromMedical) {
            tab_layout?.tabCount?.minus(1)
                ?.let { tab_layout?.getTabAt(it)?.select() }
        }
    }

    private fun tabFragments():ArrayList<Fragment>{
        val fragments: ArrayList<Fragment> = ArrayList()
        val configuration = deviceConfiguration

        if(configuration.vitals){
            fragments.add(AvailableVitalFragment())
        }
        if (configuration.medicals){
            fragments.add(MedicalChooseFragment())
        }
        return fragments
    }
}
package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FhirConfigurationData(
        val cts: Long?=null,
        val deviceCode: String?=null,
        var deviceModel: String?=null,
        val id: String?=null,
        val measurements: List<Measurement>?=null,
        val uts: Long?=null,
        val deviceImage:DeviceImage?=null,
        val manufacturer:Manufacturer?=null,
        private val connectTimeoutInSeconds: Long = 0,
        private val syncTimeoutInSeconds: Long = 0
):Parcelable {

    fun getConnectionTimeoutMs ():Long {
        return connectTimeoutInSeconds * 1000
    }

    fun getSyncTimeoutMs(): Long {
        return syncTimeoutInSeconds * 1000
    }
}
package de.mdoc.pojo.digitalsignature

/**
 * Created by Trenser on 30/12/20.
 */

data class SignatureDocumentInfoResponse(
    val code: String,
    val `data`: SignatureDocumentInfoData,
    val message: String,
    val timestamp: Long
)

data class SignatureDocumentInfoData(
        val docID: String,
        val externalID: String,
        val fileName: String,
        var formMetadataDTOList: List<FormMetadataDTO>,
        val notificationID: String,
        val owner: String,
        val title: String?,
        val type: String,
        val version: String
)

data class FormMetadataDTO(
        var checked: Boolean,
        val formType: String,
        val height: Double,
        val id: String,
        val pageNumber: Int,
        val width: Double,
        val x: Double,
        val y: Double
)
package de.mdoc.modules.medications.overview

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.navigation.findNavController
import androidx.viewpager.widget.ViewPager
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMedicationPlanOverviewBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.MedicationsFragment
import de.mdoc.modules.medications.create_plan.data.ListItemTypes
import de.mdoc.modules.medications.demand.OnDemandEventsFragment
import de.mdoc.modules.medications.export.MedicationExportFragment
import de.mdoc.modules.mental_goals.export.MentalGoalExportFragment
import de.mdoc.util.onCreateOptionsMenu
import kotlinx.android.synthetic.main.fragment_medication_plan_overview.*

class MedicationPlanOverview : NewBaseFragment() {

    override val navigationItem: NavigationItem
        get() = NavigationItem.Medication

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMedicationPlanOverviewBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_medication_plan_overview, container, false)
        binding.lifecycleOwner = this
        binding.fragment = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setupViewPager(viewPagerMedications)
        tabs.setupWithViewPager(viewPagerMedications)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = Adapter(childFragmentManager)
        adapter.addFragment(MedicationsFragment(), getString(R.string.med_plans))
        adapter.addFragment(OnDemandEventsFragment(), getString(R.string.med_plan_on_demand))
        viewPager.adapter = adapter
    }

    internal class Adapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        private val mFragments: MutableList<Fragment> = ArrayList()
        private val mFragmentTitles: MutableList<String> = ArrayList()
        fun addFragment(fragment: Fragment, title: String) {
            mFragments.add(fragment)
            mFragmentTitles.add(title)
        }

        override fun getItem(position: Int): Fragment {
            return mFragments[position]
        }

        override fun getCount(): Int {
            return mFragments.size
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitles[position]
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_medications)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItemExport) {
            MedicationExportFragment().show(parentFragmentManager, MentalGoalExportFragment::class.simpleName)
        }
        return super.onOptionsItemSelected(item)
    }

    fun onAddMedicationClick(view: View) {
        when (viewPagerMedications.currentItem) {
            0 -> {
                val direction = MainNavDirections.globalActionToMedicationSearch(ListItemTypes.PERMENANT)
                 view.findNavController().navigate(direction)
            }

            1 -> {
                val direction = MainNavDirections.globalActionToMedicationSearch(ListItemTypes.ON_DEMAND)
                view.findNavController().navigate(direction)
            }
        }
    }
}
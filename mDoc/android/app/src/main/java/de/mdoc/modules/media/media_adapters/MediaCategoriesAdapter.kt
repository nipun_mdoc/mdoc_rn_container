package de.mdoc.modules.media.media_adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.data.media.MediaStatisticsData
import de.mdoc.modules.media.MediaBottomSheetDialogFragmentDirections
import kotlinx.android.synthetic.main.placeholder_media_categories.view.*

class MediaCategoriesAdapter(var context: MdocActivity,
                             var category: List<MediaStatisticsData>):
        RecyclerView.Adapter<MediaCategoriesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaCategoriesViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_media_categories, parent, false)
        return MediaCategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: MediaCategoriesViewHolder, position: Int) {
        val categoryItem = category[position]
        val categoryName = if (category.isNotEmpty()) categoryItem.category else ""

        holder.categoryLabel.text = categoryName

        holder.statisticsLabel.text = mapToString(categoryItem.countByType)

        holder.clItemPlaceholder.setOnClickListener {
            val action = MediaBottomSheetDialogFragmentDirections.actionMediaBottomSheetDialogFragmentToMediaCategoryFragment(categoryName)
            context.findNavController(R.id.navigation_host_fragment)
                .navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return category.size
    }

    private fun mapToString(countByType: Map<String, Int>): String {
        var result = ""

        for (item in countByType) {
            result += item.value.toString() + " "
            result += when (item.key) {
                MdocConstants.VIDEO -> {
                    if (item.value == 1) {
                        "Video "
                    }
                    else {
                        "Videos "
                    }
                }
                MdocConstants.PHOTO -> {
                    "Artikel "
                }
                else                -> item.key + " "
            }
        }
        return result
    }
}

class MediaCategoriesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val clItemPlaceholder: ConstraintLayout = itemView.cl_item_placeholder as ConstraintLayout
    val categoryLabel: TextView = itemView.txt_category_label
    val statisticsLabel: TextView = itemView.txt_statistic
}

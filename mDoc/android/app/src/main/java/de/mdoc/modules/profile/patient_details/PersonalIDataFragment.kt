package de.mdoc.modules.profile.patient_details

import android.app.DatePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.ExtendedUserDetails
import de.mdoc.pojo.RelatedPerson
import de.mdoc.pojo.newCodingItem
import kotlinx.android.synthetic.main.activity_patient_details.*
import kotlinx.android.synthetic.main.fragment_personal_data.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import javax.annotation.Nullable
import kotlin.collections.ArrayList

class PersonalIDataFragment : Fragment(){

    enum class ContactPointUse {
       HOME, WORK, TEMP, OLD, MOBILE
   }

    var genderList : ArrayList<CodingItem>? = null
    var nationalityList : ArrayList<CodingItem>? = null
    var languageList : ArrayList<CodingItem>? = null
    var civilList : ArrayList<CodingItem>? = null
    var religionList : ArrayList<CodingItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_personal_data, container, false)

    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PatientDetailsActivity).setNavigationItem(0)


        gender_spinner.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long){

                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.genderCode = genderList?.getOrNull(p2)?.code

            }

        }

        nationalitySpn.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long){
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.nationality = nationalityList?.getOrNull(p2)?.code
            }

        }

        languageSpn.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long){
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.language = languageList?.getOrNull(p2)?.code
            }

        }

        civilStatusSpn.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long){
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.maritalStatus = civilList?.getOrNull(p2)?.code
            }

        }

        religionSpn.onItemSelectedListener =  object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long){
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.religion = religionList?.getOrNull(p2)?.code
            }

        }

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)
        val myFormat = "dd.MM.yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)

        val dpd = DatePickerDialog(
                activity!!, DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
            // mention the format you need
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            dateOfBirthTv.text = sdf.format(c.time)
        }, year, month, day)

        dateOfBirthTv.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                dpd.show()
            }
        })

        fun hasError() : Boolean{
            var isError = false
            if(firstNameEdt.text.isEmpty()){
                firstNameEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
                firstNameRfTv.visibility = View.VISIBLE
                firstNameLabelTv.setTextColor(resources.getColor(R.color.lust))
                isError = true
            }else{
                firstNameRfTv.visibility = View.GONE
                firstNameLabelTv.setTextColor(resources.getColor(R.color.taupe))

            }

            if(lastNameEdt.text.isEmpty()){
                lastNameEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
                lastNameRfTv.visibility = View.VISIBLE
                lastNameLabelTv.setTextColor(resources.getColor(R.color.lust))
                isError = true
            }else{
                lastNameRfTv.visibility = View.GONE
                lastNameLabelTv.setTextColor(resources.getColor(R.color.taupe))
            }

            if(dateOfBirthTv.text.isEmpty()){
                dateOfBirthTv.background = resources.getDrawable(R.drawable.red_rr_radius_3)
                dateRfTv.visibility = View.VISIBLE
            }else{
                dateRfTv.visibility = View.GONE
            }

            return isError
        }

        firstNameEdt.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                firstNameEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
                firstNameRfTv.visibility = View.GONE
                firstNameLabelTv.setTextColor(resources.getColor(R.color.taupe))

            }

            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })

        lastNameEdt.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                lastNameEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
                lastNameRfTv.visibility = View.GONE
                lastNameLabelTv.setTextColor(resources.getColor(R.color.taupe))
            }

            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })



        continueBtn?.setOnClickListener {
            if(!hasError()) {
                if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails == null) {
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.extendedUserDetails = ExtendedUserDetails()
                }
                if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo == null) {
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo = ArrayList<RelatedPerson>()
                }

                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.firstName = firstNameEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.lastName = lastNameEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.maidenName = singleNameEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.born = c.timeInMillis
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.homeTown = hometownEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.ssn = ahvNoEdt.text.toString()

                showNewFragment(
                        ResidenceFragment(), R.id.container)
            }else{
                scrollView.smoothScrollTo(0,0)
            }
        }


        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.firstName?.let { firstNameEdt.setText(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.lastName?.let { lastNameEdt.setText(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.maidenName?.let { singleNameEdt.setText(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.born?.let { c.timeInMillis = it }
        dateOfBirthTv.text = sdf.format(c.time)
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.homeTown?.let { hometownEdt.setText(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.ssn?.let { ahvNoEdt.setText(it) }


        //gender
        MdocManager.getCoding("https://www.hl7.org/fhir/v2/0001", object : Callback<CodingResponse> {
            override fun onResponse(call: Call<CodingResponse>, response: Response<CodingResponse>) {
                if(response.isSuccessful){
                    if(isAdded) {
                        genderList = response.body()!!.data?.list
                        genderList!!.add(0, newCodingItem("", ""))
                        val spinnerAdapter = SpinnerAdapter(context!!, genderList!!)
                        gender_spinner?.adapter = spinnerAdapter

                        val index: Int? =
                            genderList?.indexOf(newCodingItem(
                                    PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.genderCode))
                        if (index != null) {
                            gender_spinner?.setSelection(index)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<CodingResponse>, t: Throwable) {

            }
        })

        //nationality
        MdocManager.getCoding("https://mdoc.one/hl7/fhir/countryList", object : Callback<CodingResponse> {
            override fun onResponse(call: Call<CodingResponse>, response: Response<CodingResponse>) {
                if(response.isSuccessful){
                    if(isAdded) {
                        nationalityList = response.body()!!.data?.list
                        nationalityList!!.add(0, newCodingItem("", ""))
                        val spinnerAdapter = SpinnerAdapter(context!!, nationalityList!!)
                        nationalitySpn?.adapter = spinnerAdapter

                        val index: Int? =
                            nationalityList?.indexOf(newCodingItem(
                                    PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.nationality))
                        if (index != null) {
                            nationalitySpn?.setSelection(index)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<CodingResponse>, t: Throwable) {

            }
        })

        //language
        MdocManager.getCoding("https://mdoc.one/hl7/fhir/ukb-usz/languages", object : Callback<CodingResponse> {
            override fun onResponse(call: Call<CodingResponse>, response: Response<CodingResponse>) {
                if(response.isSuccessful){
                    if(isAdded) {
                        languageList = response.body()!!.data?.list
                        languageList!!.add(0, newCodingItem("", ""))
                        val spinnerAdapter = SpinnerAdapter(context!!, languageList!!)
                        languageSpn?.adapter = spinnerAdapter

                        val index: Int? =
                            languageList?.indexOf(newCodingItem(
                                    PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.language))
                        if (index != null) {
                            languageSpn?.setSelection(index)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<CodingResponse>, t: Throwable) {

            }
        })

        //material status
        MdocManager.getCoding("https://www.hl7.org/fhir/v2/0002", object : Callback<CodingResponse> {
            override fun onResponse(call: Call<CodingResponse>, response: Response<CodingResponse>) {
                if(response.isSuccessful){
                    if(isAdded) {
                        civilList = response.body()!!.data?.list
                        civilList!!.add(0, newCodingItem("", ""))
                        val spinnerAdapter = SpinnerAdapter(context!!, civilList!!)
                        civilStatusSpn?.adapter = spinnerAdapter

                        val index: Int? =
                            civilList?.indexOf(newCodingItem(
                                    PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.maritalStatus))
                        if (index != null) {
                            civilStatusSpn?.setSelection(index)
                        }
                    }

                }
            }

            override fun onFailure(call: Call<CodingResponse>, t: Throwable) {

            }
        })


        //religion
        MdocManager.getCoding("https://www.hl7.org/fhir/v2/0006", object : Callback<CodingResponse> {
            override fun onResponse(call: Call<CodingResponse>, response: Response<CodingResponse>) {
                if(response.isSuccessful){
                    if(isAdded) {
                        religionList = response.body()!!.data?.list
                        religionList!!.add(0, newCodingItem("", ""))
                        val spinnerAdapter = SpinnerAdapter(context!!, religionList!!)
                        religionSpn?.adapter = spinnerAdapter

                        val index: Int? =
                            religionList?.indexOf(newCodingItem(
                                    PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.religion))
                        if (index != null) {
                            religionSpn?.setSelection(index)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<CodingResponse>, t: Throwable) {

            }
        })

    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity!!.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction().replace(container, frag).addToBackStack(null).commit()
    }

}

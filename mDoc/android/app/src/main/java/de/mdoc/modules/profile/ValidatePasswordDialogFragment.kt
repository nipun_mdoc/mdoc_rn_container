package de.mdoc.modules.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import de.mdoc.R
import de.mdoc.modules.common.dialog.ButtonClicked
import kotlinx.android.synthetic.main.dialog_common.*
import kotlinx.android.synthetic.main.dialog_common.negativeButton
import kotlinx.android.synthetic.main.dialog_common.positiveButton
import kotlinx.android.synthetic.main.dialog_common.txtDescription
import kotlinx.android.synthetic.main.dialog_common.txtTitle
import kotlinx.android.synthetic.main.dialog_validate_password.*

open class ValidatePasswordDialogFragment private constructor(val title: String?,
                                                              val description: String?,
                                                              val listener: PasswordValidatorListener?
                                                            ): DialogFragment() {

    interface PasswordValidatorListener {
        fun onDialogButtonClick(password: String)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        return inflater.inflate(R.layout.dialog_validate_password, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)

        positiveButton?.setOnClickListener {
            listener?.onDialogButtonClick(password.text.toString())
            dismiss()
        }

            negativeButton?.setOnClickListener {
                dismiss()
            }

    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.WRAP_CONTENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)

        super.onResume()
    }

    data class Builder(var title: String? = null,
                       var description: String? = null,
                       var positiveText: String? = null,
                       var negativeText: String? = null) {

        var listener: PasswordValidatorListener? = null

        fun title(title: String) = apply { this.title = title }
        fun description(description: String) = apply { this.description = description }
        fun setPositiveButton(positiveText: String) = apply { this.positiveText = positiveText }

        fun setOnClickListener(listener: PasswordValidatorListener): Builder {
            this.listener = listener
            return this
        }

        fun build() = ValidatePasswordDialogFragment(title, description,
                listener)
    }
}
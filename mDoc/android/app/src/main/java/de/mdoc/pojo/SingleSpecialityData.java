package de.mdoc.pojo;

import java.util.ArrayList;

public class SingleSpecialityData {
    private ArrayList<ServiceIds> serviceIds;
    private String title;
    private String description;

    public ArrayList<ServiceIds> getServiceIds() {
        return serviceIds;
    }

    public void setServiceIds(ArrayList<ServiceIds> serviceIds) {
        this.serviceIds = serviceIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

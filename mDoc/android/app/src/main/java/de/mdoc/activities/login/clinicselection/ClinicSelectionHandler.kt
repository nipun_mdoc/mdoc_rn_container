package de.mdoc.activities.login.clinicselection

import android.view.View
import android.widget.TextView
import at.blogc.android.views.ExpandableTextView
import de.mdoc.R

class ClinicSelectionHandler(private val viewModel: ClinicSelectionViewModel) {
    fun generateCase(clinicId: String?) {
        viewModel.generateCase(clinicId)
    }

    fun expandView(view: View) {
        view.rootView.findViewById<ExpandableTextView>(R.id.txtExpand).toggle()
        if(view.rootView.findViewById<ExpandableTextView>(R.id.txtExpand).isExpanded) {
            (view.rootView.findViewById<TextView>(R.id.btnMore)).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_dark, 0);
        }else{
            (view.rootView.findViewById<TextView>(R.id.btnMore)).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_dark, 0);
        }

    }
}
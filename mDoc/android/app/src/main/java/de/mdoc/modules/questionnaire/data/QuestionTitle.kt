package de.mdoc.modules.questionnaire.data

data class QuestionTitle(
        var poolId: String? = null,
        var title: String? = null
)
package de.mdoc.network.request;

/**
 * Created by ema on 10/16/17.
 */

public class MealPlanRequest {

    private String clinicId;
    private int daysAfter;
    private long fromDate;
    private boolean standardOffersOnly;
    private String timeZone;

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public boolean isStandardOffersOnly() {
        return standardOffersOnly;
    }

    public void setStandardOffersOnly(boolean standardOffersOnly) {
        this.standardOffersOnly = standardOffersOnly;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public int getDaysAfter() {
        return daysAfter;
    }

    public void setDaysAfter(int daysAfter) {
        this.daysAfter = daysAfter;
    }

    public long getFromDate() {
        return fromDate;
    }

    public void setFromDate(long fromDate) {
        this.fromDate = fromDate;
    }
}


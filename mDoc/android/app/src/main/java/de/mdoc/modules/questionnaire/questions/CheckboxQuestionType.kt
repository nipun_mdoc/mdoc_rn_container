package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import de.mdoc.R
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import de.mdoc.util.MdocUtil
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class CheckboxQuestionType: QuestionType() {

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.checkbox_layout, parent, false)
        val checkBoxList = view.findViewById(R.id.checkboxHolder) as LinearLayout
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value = false
        val uAnswered = ArrayList<String>()
        val answer = question.answer

        if (answer != null) {
            //Convert stringify array to  list
            try {
                val array = JSONArray(answer.answerData)
                for (i in 0 until array.length()) {
                    uAnswered.add(array.get(i)
                            .toString())
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        answers.clear()
        selection.clear()

        question.questionData.options.forEachIndexed { index, option ->
            val optionValue = question.getOptionValue(option)
            val checkBox = inflater.inflate(R.layout.checkbox_template, checkBoxList, false) as CheckBox
            checkBoxList.addView(checkBox)
            checkBox.text = optionValue
            checkBox.id = index
            checkBox.tag = option.key

            if (MdocUtil.arrayHasString(uAnswered, optionValue)) {
                checkBox.isChecked = true
                answers.add(optionValue)
                selection.add(option.key)
            }

            checkBox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    questionIsAnswered.value = true
                    answers.add(optionValue)
                    selection.add(option.key)
                    isNewAnswer = true
                }
                else {
                    answers.remove(optionValue)
                    selection.remove(option.key)
                    if (answers.size == 0) {
                        questionIsAnswered.value = false
                    }
                }
            }

            mandatoryCheck(mandatory, null, question)
        }

        return view
    }
}
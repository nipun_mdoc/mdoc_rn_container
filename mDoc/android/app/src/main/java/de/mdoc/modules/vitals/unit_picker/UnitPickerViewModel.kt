package de.mdoc.modules.vitals.unit_picker

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.devices.data.Measurement
import de.mdoc.modules.vitals.data.UnitPersistenceRequest
import de.mdoc.service.IMdocService
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch
import timber.log.Timber

class UnitPickerViewModel(private val mDocService: IMdocService,
                          unitItems: Array<Measurement>): ViewModel() {

    val units: MutableLiveData<Array<Measurement>> = MutableLiveData(unitItems)
    var isLoading = MutableLiveData(false)

    fun saveSelectionApi() {
        viewModelScope.launch {
            try {
                networkCallsAsync()
            } catch (e: Exception) {
                Timber.e("v2/common/auth/account %s", e.message)
                isLoading.value=false

            }
        }
    }

    private suspend fun networkCallsAsync() {
        isLoading.value=true
        val request = UnitPersistenceRequest()
        with(MdocAppHelper.getInstance()) {
            request.username = this.username
            request.firstName = this.userFirstName
            request.email = this.email
            request.lastName = this.userLastName
            request.lastSeenUnitByCode = AppPersistence.lastSeenUnitByCode
        }

         mDocService.sendChosenUnitSuspend(request)

        isLoading.value=false
    }
}
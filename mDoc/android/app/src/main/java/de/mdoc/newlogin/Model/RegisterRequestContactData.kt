package de.mdoc.newlogin.Model

class RegisterRequestContactData() {
    var email: String? = ""
    var firstName: String? = ""
    var lastName: String? = ""
    var name: String? = ""
    var phone: String? = ""
    var relation: String? = ""

}
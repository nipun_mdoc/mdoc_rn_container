package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Pramod on 12/31/20.
 */

public class ConfigurationListItem {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isEnabled")
    @Expose
    private boolean isEnabled;
    @SerializedName("order")
    @Expose
    private int order;
    @SerializedName("featureTitle")
    @Expose
    private String featureTitle;
    @SerializedName("iconName")
    @Expose
    private String iconName;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("subConfig")
    @Expose
    private ArrayList<SubConfigurationData> subConfig;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setIsEnabled(boolean isEnabled){
        this.isEnabled = isEnabled;
    }
    public boolean getIsEnabled(){
        return this.isEnabled;
    }
    public void setOrder(int order){
        this.order = order;
    }
    public int getOrder(){
        return this.order;
    }
    public void setFeatureTitle(String featureTitle){
        this.featureTitle = featureTitle;
    }
    public String getFeatureTitle(){
        return this.featureTitle;
    }
    public void setIconName(String iconName){
        this.iconName = iconName;
    }
    public String getIconName(){
        return this.iconName;
    }
    public void setIcon(String icon){
        this.icon = icon;
    }
    public String getIcon(){
        return this.icon;
    }
    public void setSubConfig(ArrayList<SubConfigurationData> subConfig){
        this.subConfig = subConfig;
    }
    public ArrayList<SubConfigurationData> getSubConfig(){
        return this.subConfig;
    }
}

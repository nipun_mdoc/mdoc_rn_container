package de.mdoc.modules.medications.create_plan

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.PagerAdapter
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel

class CreateMedicationPagerAdapter internal constructor(val fragments: ArrayList<Fragment>,
                                                        fm: FragmentManager?,
                                                        val viewModel: CreatePlanViewModel):
        FragmentStatePagerAdapter(fm!!) {

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getItemPosition(`object`: Any): Int {
        val index = fragments.indexOf(`object`)

        return if (index == -1) {
            PagerAdapter.POSITION_NONE
        }
        else
            index
    }
}
///2. how often(daily) -> ((null)->morning noon...)->What Time
///2. how often(Weekly) -> (Monday..tue-> morning ...noon) -> What time
///2. how often(every certain) -> (period-> morning ...noon) -> What time


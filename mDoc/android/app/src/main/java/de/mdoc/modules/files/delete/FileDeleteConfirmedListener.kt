package de.mdoc.modules.files.delete

interface FileDeleteConfirmedListener {
    fun onDeleteFileConfirmed(id: String)
}

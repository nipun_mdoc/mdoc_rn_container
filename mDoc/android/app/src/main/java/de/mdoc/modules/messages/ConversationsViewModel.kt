package de.mdoc.modules.messages

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.messages.data.ListItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class ConversationsViewModel(private val mDocService: IMdocService) : ViewModel(){

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    val isShowNoDataClosed = MutableLiveData(false)
    var isShowLoadingErrorClosed = MutableLiveData(false)
    val isShowContentClosed = MutableLiveData(false)

    var activeConversations: MutableLiveData<List<ListItem>> =
            MutableLiveData(emptyList())
    var closedConversations: MutableLiveData<List<ListItem>> =
            MutableLiveData(emptyList())
    var query: MutableLiveData<String> = MutableLiveData()
    val isShowNoActiveSearchResults = MutableLiveData(false)
    val isShowNoSearchResults = MutableLiveData(false)

//    init {
//        getActiveConversations(false)
//        getClosedConversations(false)
//    }


    fun getActiveConversations(isSearch:Boolean) {
        viewModelScope.launch {
            try {
                isShowLoadingError.value = false
                isShowNoData.value = false
                isShowContent.value = false
                isShowNoActiveSearchResults.value=false
                isLoading.value = true
                activeConversations.value = mDocService.getConversations("*reason*",
                        "*letterbox*",
                        "*participant*",
                        query.value,
                        false,
                        "lastMessageDate",
                        "DESC").data.list
                isLoading.value = false
                if(activeConversations.value?.size == 0){
                    isShowContent.value = false
                    if(isSearch){
                        isShowNoActiveSearchResults.value=true
                    }else{
                        isShowNoData.value = true
                    }
                }else{
                    isShowContent.value = true
                    isShowNoData.value = false
                }

            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }

    fun getClosedConversations(isSearch:Boolean) {
        viewModelScope.launch {
            try {
                isShowLoadingErrorClosed.value = false
                isShowNoDataClosed.value = false
                isShowContentClosed.value = false
                isShowNoSearchResults.value=false
                isLoading.value = true

                closedConversations.value = mDocService.getConversations("*reason*",
                        "*letterbox*",
                        "*participant*",
                        query.value,
                        true,
                        "lastMessageDate",
                        "DESC").data.list
                isLoading.value = false
                if(closedConversations.value?.size == 0){
                    if(isSearch){
                        isShowNoSearchResults.value=true
                    }else{
                        isShowNoDataClosed.value = true
                        isShowContentClosed.value = false
                    }
                }else{
                    isShowContentClosed.value = true
                    isShowNoDataClosed.value = false
                }

            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContentClosed.value = false
                isShowLoadingErrorClosed.value = true
                isShowNoDataClosed.value = false
            }
        }
    }
}
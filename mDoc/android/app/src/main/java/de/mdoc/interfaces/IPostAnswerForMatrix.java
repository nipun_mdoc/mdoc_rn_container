package de.mdoc.interfaces;

import java.util.ArrayList;

public interface IPostAnswerForMatrix {
    void postAnswerForMatrix(String answer, ArrayList<String> selection, String questionId);
}

package de.mdoc.modules.profile.data

data class ContactsRequest(
        val emergencyContacts: ArrayList<Contact>
                          )
package de.mdoc.data.create_bt_device

data class SubjectObservation(
        val reference: String
)
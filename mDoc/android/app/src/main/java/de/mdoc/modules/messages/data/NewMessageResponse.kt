package de.mdoc.modules.messages.data

data class NewMessageResponse(val code: String = "",
                              val data: DataNewMessage,
                              val message: String = "",
                              val timestamp: Long = 0)

data class DataNewMessage(val senderId: String = "",
                val exportDate: Int = 0,
                val cts: Long = 0,
                val uts: Long = 0,
                val conversationId: String = "",
                val id: String = "",
                val text: String = "",
                val priority: Int = 0)
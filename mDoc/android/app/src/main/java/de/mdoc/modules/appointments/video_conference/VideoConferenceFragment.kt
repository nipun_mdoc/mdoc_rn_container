package de.mdoc.modules.appointments.video_conference

import android.Manifest
import android.content.res.Resources
import android.graphics.Color
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.util.TypedValue
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Chronometer
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.*
import com.opentok.android.*
import de.mdoc.R
import de.mdoc.activities.MainActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentVideoConferenceBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.appointments.video_conference.data.ActiveSpeakerData
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.network.RestClient
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.parcelableArgument
import de.mdoc.util.serializableArgument
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_video_conference.*
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import timber.log.Timber
import ua.naiksoftware.stomp.client.StompClient

class VideoConferenceFragment: NewBaseFragment(), Session.SessionListener,
        PublisherKit.PublisherListener, SubscriberKit.VideoListener, CommonDialogFragment.OnButtonClickListener, SubscriberKit.AudioLevelListener {

    override val navigationItem: NavigationItem = NavigationItem.Therapy
    private val videoConferenceViewModel by viewModel { VideoConferenceViewModel(RestClient.getService()) }
    lateinit var activity: MdocActivity

    var appointmentDetails by serializableArgument<AppointmentDetails>(MdocConstants.APPOINTMENT)
    var bookingAdditionalFields by parcelableArgument(
            MdocConstants.BOOKING_ADDITIONAL_FIELDS) { BookingAdditionalFields() }

    private var tokBoxToken: String? = null
    private var tokBoxSessionId: String? = null
    private var mSession: Session? = null
    private var mPublisher: Publisher? = null
    private var mSubscriber: Subscriber? = null
    private var stompClient: StompClient? = null
    private var llDoctorNameParent: LinearLayout? = null
    private var mPublisherViewContainer: LinearLayout? = null
    private var mSubscriberViewContainer: RelativeLayout? = null
    private var recyclerView: RecyclerView? = null
    private var activeSpeakerGridView: RecyclerView? = null
    private var webSocket: String? = null
    private var isMicEnabled = true
    private var isCameraEnabled = true
    private var apiHandler: Handler? = null
    private var isFullScreen = true
    private var motion: MotionLayout? = null
    private var callStartTime: DateTime? = null
    private var attendee: String? = null
    private var isHangUp = false
    private var attendiesNameList = hashMapOf<String, String>()
    private var attendiesList: ArrayList<ActiveSpeakerData> = arrayListOf()
    private var activeSpeakerList: ArrayList<ActiveSpeakerData> = arrayListOf()
    private var shareScreenList: ArrayList<ActiveSpeakerData> = arrayListOf()
    private var shareScreenAttendeesList: ArrayList<ActiveSpeakerData> = arrayListOf()
    private var txtCallDuration: Chronometer? = null
    private var txtPatientName: TextView? = null
    var doctorName = ""

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        activity = context as MdocActivity
        val baseUrl = activity.getString(R.string.deeplink)
        webSocket = "wss://${baseUrl}:443/appointments-websocket/websocket"
        val binding: FragmentVideoConferenceBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_video_conference, container, false)

        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (getActivity() as MainActivity?)?.hideBottomAndTopBar()

        llDoctorNameParent = view.findViewById(R.id.llDoctorNameParent)
        mPublisherViewContainer = view.findViewById(R.id.publisher_container)
        mSubscriberViewContainer = view.findViewById(R.id.subscriber_container)
        txtCallDuration = view?.findViewById(R.id.txtCallDuration)
        txtPatientName = view?.findViewById(R.id.txtPatientName)
        motion = activity.findViewById(R.id.motionParent)
        activeSpeakerGridView = view.findViewById(R.id.gridView)
        recyclerView = view.findViewById(R.id.bottomView_recycler)

        initVideoSession()

        btnHangUp?.setOnClickListener { hangUp() }
        btnScreenShare?.setOnClickListener { toggleShare() }
        btnMute?.setOnClickListener { toggleMute() }
        btnFullScreen?.setOnClickListener { toggleFullScreen() }
        btnFullScreen1?.setOnClickListener { toggleFullScreen() }
        fillStrings()

        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && event.action === KeyEvent.ACTION_UP) {
                controls?.visibility = View.GONE
                minimizeScreen()
                (getActivity() as MainActivity?)?.showBottomAndTopBar()
                return@OnKeyListener true
            }
            false
        })
    }

    private fun toggleFullScreen() {
        if (!isFullScreen) {
            controls?.visibility = View.VISIBLE
            maximizeScreen()
            (getActivity() as MainActivity?)?.hideBottomAndTopBar()
        } else {
            controls?.visibility = View.GONE
            minimizeScreen()
            (getActivity() as MainActivity?)?.showBottomAndTopBar()
        }
    }

    private fun maximizeScreen() {
        motion?.transitionToStart()

        isFullScreen = true

        val r: Resources = resources
        val pxBottomMargin: Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 140F, r.getDisplayMetrics())
        val params: ViewGroup.MarginLayoutParams = llDoctorNameParent!!.layoutParams as ViewGroup.MarginLayoutParams
        params.bottomMargin = Math.round(pxBottomMargin)

        if(shareScreenList.size > 0){
            gridView_parent.visibility = View.GONE
            subscriber_parent.visibility = View.GONE
            initShareListAdapter()
            share_parent.visibility = View.VISIBLE
            displayShareScreenVideo()

        }else{
            if (activeSpeakerList.size > 1) {
                gridView_parent.visibility = View.VISIBLE
                subscriber_parent.visibility = View.GONE
                Handler().postDelayed({
                    initActiveListAdapter()
                }, 1000L)
            } else if (activeSpeakerList.size == 1) {
                gridView_parent.visibility = View.GONE
                bottomView_parent.visibility = View.GONE
                subscriber_parent.visibility = View.VISIBLE
            }
            if (attendiesList.size > 0) {
                bottomView_parent.visibility = View.VISIBLE
                initParticipantsListAdapter()
            }
        }


        btnFullScreen1?.visibility = View.GONE
        publisher_parent?.visibility = View.VISIBLE
        btnFullScreen.setImageResource(R.drawable.ic_minimize_video)
        btnFullScreen.setBackgroundColor(Color.parseColor("#00000000"))
        if (callStartTime != null) {
            callDuration?.visibility = View.VISIBLE
        }
        first.visibility = if (activeSpeakerList.size > 0) {
            View.GONE
        } else {
            View.VISIBLE
        }


    }

    private fun minimizeScreen() {
        subscriber_parent.visibility = View.VISIBLE
        if (activeSpeakerList.size > 0 && shareScreenList.size == 0) {
            val r: Resources = resources
            val pxBottomMargin: Float = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20F, r.getDisplayMetrics())
            val params: ViewGroup.MarginLayoutParams = llDoctorNameParent!!.layoutParams as ViewGroup.MarginLayoutParams
            params.bottomMargin = Math.round(pxBottomMargin)
            txtDoctorName.text = (activeSpeakerList.get(0).participantName)
            mSubscriber = activeSpeakerList.get(0).subscriber
            mSubscriberViewContainer?.removeAllViews();
            if (mSubscriber?.view?.parent != null) {
                (mSubscriber?.view?.parent as ViewGroup)?.removeView(mSubscriber?.view)
                mSubscriberViewContainer?.addView(mSubscriber?.view)
            }else{
                mSubscriberViewContainer?.addView(mSubscriber?.view)
            }
        }
        gridView_parent.visibility = View.GONE
        bottomView_parent.visibility = View.GONE

        btnFullScreen1?.visibility = View.VISIBLE
        publisher_parent?.visibility = View.GONE
        motion?.transitionToEnd()
        callDuration?.visibility = View.GONE

        btnFullScreen.setImageResource(R.drawable.ic_icon_fullscreenon)
        btnFullScreen.setBackgroundColor(Color.parseColor("#000000"))
        first.visibility = View.GONE
        isFullScreen = false
    }

    private fun fillStrings() {
        val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy - HH:mm")
        val hourFormatter = DateTimeFormat.forPattern("HH:mm")
        val startHour = hourFormatter.print(appointmentDetails.start)
        val endHour = hourFormatter.print(appointmentDetails.end)

        txt_appointment_time.text = resources.getString(R.string.online_appointment_full_time,
                dateFormatter.print(appointmentDetails.start))
        txt_appointment_title.text = appointmentDetails.title
        txt_department.text = appointmentDetails.repeatDefinition.info.specialty
        txt_time_interval.text = resources.getString(R.string.online_appointment_time_interval, startHour, endHour)

        fillAttendees()
        getTimeUntilAppointmentStart()
    }

    private fun getTimeUntilAppointmentStart() {
        val diff = appointmentDetails.start - System.currentTimeMillis()
        val minute = (diff / 60_000).toInt()
        txt_minutes_to_start?.text = resources.getQuantityString(R.plurals.online_appointment_minute, minute, minute)

        start_information.visibility = if (minute > 0) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun fillAttendees() {
        var patientName = ""
        appointmentDetails.participants.forEachIndexed { index, participant ->
            if ((!participant.type.isNullOrEmpty()) && participant.type.equals("ORGANISER")) {
                doctorName = if (participant?.title.isNullOrEmpty()) {
                    "${participant?.firstName} ${participant?.lastName}"
                } else {
                    "${participant?.title} ${participant?.firstName} ${participant?.lastName}"
                }
            } else {
                patientName = if (participant?.title.isNullOrEmpty()) {
                    "${participant?.firstName} ${participant?.lastName}"
                } else {
                    "${participant?.title} ${participant?.firstName} ${participant?.lastName}"
                }
            }
            attendee = if (participant?.title.isNullOrEmpty()) {
                "${participant?.firstName} ${participant?.lastName}"
            } else {
                "${participant?.title} ${participant?.firstName} ${participant?.lastName}"
            }

            txt_attendee.append(attendee)
            txtDoctorName.text = (doctorName)
            if (index != appointmentDetails.participants.size - 1) {
                txt_attendee.append(" \n")
            }
        }
    }

    private fun updateAttendeeName() {
        if (activeSpeakerList.size > 0) {
            txtDoctorName.text = (activeSpeakerList.get(0).participantName)
        } else {
            txtDoctorName.text = ("")
        }
    }

    private fun initVideoSession() {
        lifecycleScope.launch {
            videoConferenceViewModel.joinAppointment(appointmentDetails.appointmentId, onSuccess = {
                requestPermissions()
            }, onError = { error ->
                var finalError: String = if (error == "" || error == null) resources.getString(R.string.something_went_wrong) else error
                showError(finalError)
            })
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    @AfterPermissionGranted(RC_VIDEO_APP_PERM)
    private fun requestPermissions() {
        val perms =
                arrayOf(Manifest.permission.INTERNET, Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO)
        if (EasyPermissions.hasPermissions(activity, *perms)) {
            getDataRecursive()
        } else {
            EasyPermissions.requestPermissions(this, "This app needs access to your camera and mic to make video calls",
                    RC_VIDEO_APP_PERM, *perms)
        }
    }

    private fun getDataRecursive() {
        videoConferenceViewModel.getTokBoxTokenApi(appointmentDetails.appointmentId, onSuccess = {
            tokBoxToken = it
            getSessionId()
        }, onError = {
            showError(resources.getString(R.string.error_message_call))
        })
    }

    private fun getSessionId() {
        videoConferenceViewModel.getSessionIdApi(appointmentDetails.appointmentId,
                onSuccess = { sessionId ->
                    tokBoxSessionId = sessionId
                    if (mSession == null && hasSessionId()) {
                        connectToSession()
                    }
                }, onError = {
        })
    }

    private fun setCallDuration() {
        if (callStartTime != null) {
            callDuration.visibility = View.VISIBLE
            txtCallDuration?.start();
        }
    }

    private fun connectToSession() {
        val builder = Session.Builder(activity, activity.resources.getInteger(R.integer.tokbox_api_key).toString(), tokBoxSessionId)

        val proxyUrl = resources.getString(R.string.tokbox_proxy)
        if (proxyUrl.isNotEmpty()) {
            builder.setProxyUrl(proxyUrl)
        }

        mSession = builder.build()
        mSession?.setSessionListener(this)
        mSession?.connect(tokBoxToken)
    }

    private fun hasSessionId(): Boolean {
        return !tokBoxSessionId.isNullOrEmpty()
    }

    private fun hasTokboxToken(): Boolean {
        return !tokBoxToken.isNullOrEmpty()
    }

    override fun onStreamDropped(session: Session?, stream: Stream?) {
        removeOnDrop(stream)
    }

    private fun removeOnDrop(stream: Stream?){
        activity.runOnUiThread{
            attendiesNameList.remove(stream?.streamId!!)
            if (stream?.name.equals("Screen share")) {
                shareScreenList.clear()
                shareScreenAttendeesList.clear()
                share_parent.visibility = View.GONE
                bottomView_parent.visibility = View.GONE

                if (activeSpeakerList.size == 1) {
                    displaySingleVideo()
                }else if (activeSpeakerList.size > 1) {
                    if(isFullScreen) {
                        gridView_parent.visibility = View.VISIBLE
                    }
                    initActiveListAdapter()
                    if(attendiesList.size > 0){
                        if(isFullScreen) {
                            bottomView_parent.visibility = View.VISIBLE
                        }
                        initParticipantsListAdapter()
                    }
                }else {
                    first.visibility = View.VISIBLE
                }
            }else{
                if (attendiesList.size > 0) {
                    val user: ActiveSpeakerData? = attendiesList.find { it.streamId == stream.streamId }
                    if (user != null) {
                        attendiesList.remove(user)
                        if(shareScreenList.size > 0){
                            initShareListAdapter()
                        }else{
                            share_parent.visibility = View.GONE
                            initParticipantsListAdapter()
                        }
                    } else if (activeSpeakerList.size > 0) {
                        val user: ActiveSpeakerData? = activeSpeakerList.find { it.streamId == stream.streamId }
                        if (user != null) {
                            activeSpeakerList.remove(user)
                            activeSpeakerList.add(attendiesList.get(0))
                            attendiesList.removeAt(0)
                            if(shareScreenList.size > 0){
                                initShareListAdapter()
                            }else {
                                share_parent.visibility = View.GONE
                                initActiveListAdapter()
                                initParticipantsListAdapter()
                            }
                        }
                    }
                } else {
                    if (activeSpeakerList.size > 0) {
                        val user: ActiveSpeakerData? = activeSpeakerList.find { it.streamId == stream.streamId }
                        if (user != null) {
                            activeSpeakerList.remove(user)
                            if(shareScreenList.size > 0){
                                initShareListAdapter()
                            }else {
                                share_parent.visibility = View.GONE
                                if (activeSpeakerList.size > 1) {
                                    initActiveListAdapter()
                                }else if (activeSpeakerList.size == 1) {
                                    displaySingleVideo()
                                }else{
                                    first.visibility = View.GONE
                                }
                            }
                        }
                    }
                }
            }

            Log.d("aaaaaaaa", ""+(stream?.streamVideoType == Stream.StreamVideoType.StreamVideoTypeScreen))
//            if (mSubscriber != null && stream?.streamVideoType == Stream.StreamVideoType.StreamVideoTypeScreen) {
            if(activeSpeakerList.size > 0){
                Log.d("aaaaaaaa", stream?.name)
                if(!isFullScreen) {
                    initSubscriber()
                }
            } else {
                mSubscriber = null
                mSubscriberViewContainer?.removeAllViews()
            }

            updateAttendeeName()
        }
    }

    private fun initActiveListAdapter(){
        height = activeSpeakerGridView?.height!!
        width = activeSpeakerGridView?.height!!
        if (activeSpeakerAdapterGrid == null) {
            val gridLayoutManager = GridLayoutManager(activity, 2)
            activeSpeakerGridView!!.layoutManager = gridLayoutManager
            activeSpeakerGridView?.setHasFixedSize(true);
            activeSpeakerGridView?.addItemDecoration(DividerItemDecoration(activity,
                    DividerItemDecoration.HORIZONTAL))
            activeSpeakerGridView?.addItemDecoration(DividerItemDecoration(activity,
                    DividerItemDecoration.VERTICAL))
            activeSpeakerGridView?.setNestedScrollingEnabled(false);
            activeSpeakerAdapterGrid = ActiveSpeakerAdapter(activeSpeakerList, height, width)
            activeSpeakerGridView?.adapter = activeSpeakerAdapterGrid
        } else {
            activeSpeakerAdapterGrid?.notifyDataSetChanged()
        }
    }

    private fun initParticipantsListAdapter(){
        if (participantsAdapter == null) {
            participantsAdapter = ParticipantsAdapter(attendiesList, recyclerView?.height!!, recyclerView?.width!!)
            val mLayoutManager = LinearLayoutManager(context)
            mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
            recyclerView?.layoutManager = mLayoutManager
            recyclerView?.itemAnimator = DefaultItemAnimator()
            recyclerView?.adapter = participantsAdapter
        } else {
            participantsAdapter?.notifyDataSetChanged()
        }
    }

    private fun initShareListAdapter(){
        if(shareScreenList.size > 0){
            if(isFullScreen)
                bottomView_parent.visibility = View.VISIBLE
            shareScreenAttendeesList.clear()
            shareScreenAttendeesList.addAll(activeSpeakerList)
            shareScreenAttendeesList.addAll(attendiesList)
            if (shareAdapter == null) {
                shareAdapter = ShareAttendeesAdapter(shareScreenAttendeesList, recyclerView?.height!!, recyclerView?.width!!)
                val mLayoutManager = LinearLayoutManager(context)
                mLayoutManager.orientation = LinearLayoutManager.HORIZONTAL
                recyclerView?.layoutManager = mLayoutManager
                recyclerView?.itemAnimator = DefaultItemAnimator()
                recyclerView?.adapter = shareAdapter
            } else {
                shareAdapter?.notifyDataSetChanged()
            }
        }else {
            share_parent.visibility = View.GONE
            shareAdapter = null
        }
    }

    private fun displaySingleVideo(){
        txtDoctorName.text = (activeSpeakerList.get(0).participantName)
        mSubscriber = activeSpeakerList.get(0).subscriber
        share_parent.visibility = View.GONE
        gridView_parent?.visibility = View.GONE
        subscriber_parent.visibility = View.VISIBLE
        mSubscriberViewContainer?.visibility = View.VISIBLE
        mSubscriberViewContainer?.removeAllViews();
        setLayoutParams(mSubscriber)
        mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        if (mSubscriber?.view?.parent != null) {
            (mSubscriber?.view?.parent as ViewGroup)?.removeView(mSubscriber?.view)
            mSubscriberViewContainer?.addView(mSubscriber?.view)
        } else {
            mSubscriberViewContainer?.addView(mSubscriber?.view)
        }
    }

    private fun displayShareScreenVideo(){
        if(isFullScreen) {
            share_parent.visibility = View.VISIBLE
            share_container.visibility = View.VISIBLE
        }
        val _mSubscriber = shareScreenList.get(0).subscriber
        setLayoutParams(_mSubscriber)
        _mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FIT);
        if (_mSubscriber?.view?.parent != null) {
            (_mSubscriber?.view?.parent as ViewGroup)?.removeView(_mSubscriber?.view)
            share_container?.addView(_mSubscriber?.view)
        } else {
            share_container?.addView(_mSubscriber?.view)
        }
        txtShareName.text = shareScreenList.get(0).participantName
        if (_mSubscriber?.view is GLSurfaceView) {
            (_mSubscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
        }
    }

    private fun initSubscriber(){

        if(shareScreenList.size > 0){
            mSubscriber = shareScreenList.get(0).subscriber
            mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FIT);
        }else {
            setLayoutParams(mSubscriber)
            mSubscriber = activeSpeakerList.get(0).subscriber
            mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
        }

        if (mSubscriber?.view?.parent != null) {
            (mSubscriber?.view?.parent as ViewGroup)?.removeView(mSubscriber?.view)
            mSubscriberViewContainer?.addView(mSubscriber?.view)
        }else{
            mSubscriberViewContainer?.addView(mSubscriber?.view)
        }

        if (mSubscriber?.view is GLSurfaceView) {
            (mSubscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
        }
    }

    private fun setLayoutParams(subscriber: Subscriber?){
        try{
            subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
        }catch (ex: Exception){
            ex.printStackTrace()
            Log.d("aaaaaaaaaaa", "class cast exception")
            subscriber?.view?.layoutParams = RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    private fun setLayoutParamsPublisher(subscriber: Publisher?){
        try{
            subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
        }catch (ex: Exception){
            ex.printStackTrace()
            Log.d("aaaaaaaaaaa", "class cast exception")
            subscriber?.view?.layoutParams = RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
        }
    }

    var isActiveListUpdated = true
    var height: Int = 0
    var width: Int = 0
    var shareAdapter: ShareAttendeesAdapter? = null
    var participantsAdapter: ParticipantsAdapter? = null
    var activeSpeakerAdapterGrid: ActiveSpeakerAdapter? = null
    override fun onStreamReceived(session: Session?, stream: Stream?) {
        render(session, stream)
    }

    private fun render(session: Session?, stream: Stream?) {
        activity.runOnUiThread {
            Log.d("aaaaaaaa2", ""+(stream?.streamVideoType == Stream.StreamVideoType.StreamVideoTypeScreen))
            if (stream?.name.equals("Screen share")) {
                var _mSubscriber = Subscriber.Builder(activity, stream).build()
                _mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
                _mSubscriber?.setVideoListener(this)
                _mSubscriber?.setAudioLevelListener(this)
                session?.subscribe(_mSubscriber)
                val activeSpeakerData = ActiveSpeakerData(
                        participantName = stream?.name!!,
                        streamId = stream?.streamId!!,
                        session = mSession!!,
                        subscriber = _mSubscriber)
                shareScreenList.add(activeSpeakerData)
            } else {
                if (activeSpeakerList.size < 4) {
                    isActiveListUpdated = true
                    var _mSubscriber = Subscriber.Builder(activity, stream).build()
                    _mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
                    _mSubscriber?.setVideoListener(this)
                    _mSubscriber?.setAudioLevelListener(this)
                    session?.subscribe(_mSubscriber)
                    _mSubscriber?.setAudioLevelListener(this)
                    val activeSpeakerData = ActiveSpeakerData(
                            participantName = stream?.name!!,
                            streamId = stream?.streamId!!,
                            session = mSession!!,
                            subscriber = _mSubscriber)
                    if (doctorName.trim().equals(stream.name.trim(), true)) {
                        activeSpeakerList.add(0, activeSpeakerData)
                    } else {
                        activeSpeakerList.add(activeSpeakerData)
                    }
                } else {
                    isActiveListUpdated = false
                    var _mSubscriber = Subscriber.Builder(activity, stream).build()
                    _mSubscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
                    _mSubscriber?.setVideoListener(this)
                    _mSubscriber?.setAudioLevelListener(this)
                    session?.subscribe(_mSubscriber)
                    _mSubscriber?.setAudioLevelListener(this)
                    val activeSpeakerData = ActiveSpeakerData(
                            participantName = stream?.name!!,
                            streamId = stream?.streamId!!,
                            session = mSession!!,
                            subscriber = _mSubscriber)

                    if (doctorName.trim().equals(stream.name.trim())) {
                        isActiveListUpdated = true
                        val user: ActiveSpeakerData = activeSpeakerList.get(0)
                        activeSpeakerList.removeAt(0)
                        activeSpeakerList.add(0, activeSpeakerData)
                        attendiesList.add(user)
                    } else {
                        attendiesList.add(activeSpeakerData)
                    }
                }
            }
            attendiesNameList.put(stream?.streamId!!, stream?.name!!)
            callStartTime = DateTime.now()
            if(isFullScreen)
                setCallDuration()

            if(shareScreenList.size > 0){
                addPublisherWindow(stream)
                first.visibility = View.GONE
                subscriber_parent.visibility = View.GONE
                gridView_parent.visibility = View.GONE
                share_parent.visibility = View.VISIBLE
                if(isFullScreen) {
                    bottomView_parent.visibility = View.VISIBLE
                    publisher_parent.visibility = View.VISIBLE
                    share_parent.visibility = View.VISIBLE
                    share_container.visibility = View.VISIBLE
                }

                if(!isFullScreen) {
                    initSubscriber()
                }
                displayShareScreenVideo()
                initShareListAdapter()
            }else{
                share_parent.visibility = View.GONE
                addPublisherWindow(stream)
                if (activeSpeakerList.size == 1) {
                    first.visibility = View.GONE
                    if(isFullScreen) {
                        subscriber_parent.visibility = View.VISIBLE
                        publisher_parent.visibility = View.VISIBLE
                    }
                    gridView_parent.visibility = View.GONE
                    bottomView_parent.visibility = View.GONE

                    initSubscriber()

                } else if (activeSpeakerList.size <= 4) {

                    if(isFullScreen) {
                        gridView_parent.visibility = View.VISIBLE
                        subscriber_parent.visibility = View.GONE
                    }
                    if (isActiveListUpdated) {
                        initActiveListAdapter()
                    }

                    if (attendiesList.size > 0) {
                        if(isFullScreen) {
                            bottomView_parent.visibility = View.VISIBLE
                        }
                        initParticipantsListAdapter()
                    }
                }
            }
            updateAttendeeName()
        }
    }

    override fun onConnected(session: Session?) {
        Timber.d(LOG_TAG, "onConnected")
        // here we must implement open session for call!
        initPublisher()
    }

    private fun initPublisher() {
        mPublisher = Publisher.Builder(activity).
        name(MdocAppHelper.getInstance().userFirstName + " " + MdocAppHelper.getInstance().userLastName).
        resolution(Publisher.CameraCaptureResolution.LOW)
                .frameRate(Publisher.CameraCaptureFrameRate.FPS_15).build()
        mPublisher?.setPublisherListener(this)
    }

    private fun addPublisherWindow(stream: Stream?) {
        if (mPublisher?.view?.parent != null) {
            mSubscriber = null
            mPublisherViewContainer?.removeView(mPublisher?.view)
        }
        if (stream?.streamVideoType == Stream.StreamVideoType.StreamVideoTypeCamera) {
            setLayoutParamsPublisher(mPublisher);
            mPublisher?.view?.setBackgroundResource(R.drawable.video_container_frame);
            mPublisher?.view?.clipToOutline  =true
            mPublisher?.publishAudio = isMicEnabled
            mPublisherViewContainer?.addView(mPublisher?.view)

            if (mPublisher?.view is GLSurfaceView) {
                (mPublisher?.view as GLSurfaceView).setZOrderMediaOverlay(true)
            }
        } else {
            mPublisher?.publisherVideoType = PublisherKit.PublisherKitVideoType.PublisherKitVideoTypeScreen
            mPublisher?.audioFallbackEnabled = false
            mPublisher?.publishAudio = isMicEnabled
            setLayoutParamsPublisher(mPublisher);
            mPublisher?.view?.setBackgroundResource(R.drawable.video_container_frame);
            mPublisher?.view?.clipToOutline  =true
            mPublisherViewContainer?.addView(mPublisher?.view)
            if (mPublisher?.view is GLSurfaceView) {
                (mPublisher?.view as GLSurfaceView).setZOrderMediaOverlay(true)
            }
        }
        txtPatientName?.text = """${MdocAppHelper.getInstance().userFirstName} ${MdocAppHelper.getInstance().userLastName}"""//(MdocAppHelper.getInstance().getUsername())
        mSession?.publish(mPublisher)

    }

    override fun onDisconnected(session: Session?) {
        Timber.d(LOG_TAG, "onDisconnected")
    }

    override fun onError(session: Session?, error: OpentokError?) {
        Timber.d(LOG_TAG, "onError")
        if (!isHangUp && error?.errorCode != OpentokError.ErrorCode.SessionNullOrInvalidParameter) {
            showError(error?.errorMessage ?: resources.getString(R.string.something_went_wrong))
        }
    }

    override fun onStreamCreated(p0: PublisherKit?, stream: Stream?) {
        Timber.d(LOG_TAG, "onStreamCreated")
    }

    override fun onStreamDestroyed(p0: PublisherKit?, stream: Stream?) {
        Timber.d(LOG_TAG, "onStreamDestroyed")
    }

    override fun onError(p0: PublisherKit?, error: OpentokError?) {
        Timber.d(LOG_TAG, "onError")
    }

    private fun showError(error: String) {
        val positive = resources.getString(R.string.button_ok)

        CommonDialogFragment.Builder()
                .description(error)
                .setPositiveButton(positive)
                .setOnClickListener(this)
                .build()
                .showNow(parentFragmentManager, CommonDialogFragment::class.simpleName)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        hangUp()
    }

    private fun toggleMute() {
        isMicEnabled = !isMicEnabled
        if (isMicEnabled) {
            btnMute.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_mute_video))
        } else {
            btnMute.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_mute_off_video))
        }
        mPublisher?.publishAudio = isMicEnabled
    }

    private fun toggleShare() {
        isCameraEnabled = !isCameraEnabled

        if (isCameraEnabled) {
            btnScreenShare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_camera_video))
        } else {
            btnScreenShare.setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.ic_camera_off_video))
        }
        mPublisher?.publishVideo = isCameraEnabled
    }

    private fun hangUp() {
        leaveAppointmentCall()
        isHangUp = true
        if (mSession?.isSessionConnected == true) {
            mSession?.disconnect()
        }
        mSubscriberViewContainer?.removeAllViews()
        mPublisherViewContainer?.removeAllViews()
        motion?.transitionToStart()
        removeFragment()
    }

    private fun leaveAppointmentCall() {
        if (hasTokboxToken()) {
            tokBoxToken = null
            videoConferenceViewModel.leaveAppointmentCall(appointmentDetails.appointmentId)
        }
    }

    private fun removeFragment() {
        if (isAdded) {
            for (fragment in parentFragmentManager.fragments) {
                if (fragment is VideoConferenceFragment) {
                    parentFragmentManager.beginTransaction()
                            .remove(fragment)
                            .commit()
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mSession?.onPause()
        (getActivity() as MainActivity?)?.showBottomAndTopBar()
    }

    override fun onResume() {
        super.onResume()
        mSession?.onResume()
        (getActivity() as MainActivity?)?.hideBottomAndTopBar()
    }

    override fun onDestroy() {
        super.onDestroy()
        clearSession()
    }

    private fun clearSession() {
        leaveAppointmentCall()
        apiHandler?.removeCallbacksAndMessages(null)
        mSession?.disconnect()
        mSubscriberViewContainer?.removeAllViews()
        mPublisherViewContainer?.removeAllViews()
        stompClient?.disconnect()
        txtCallDuration?.stop()
        removeFragment()
        (getActivity() as MainActivity?)?.showBottomAndTopBar()
    }

    companion object {
        const val RC_VIDEO_APP_PERM = 124
        private val LOG_TAG = VideoConferenceFragment::class.java.simpleName
        const val API_CALL_TIMEOUT = 1000L
    }

    override fun onVideoDataReceived(p0: SubscriberKit?) {
        Timber.d(LOG_TAG, "onVideoDataReceived")
    }

    override fun onVideoDisabled(subscriber: SubscriberKit?, reason: String?) {
//        avatar.visibility = View.VISIBLE
    }

    override fun onVideoEnabled(subscriber: SubscriberKit?, reason: String?) {
//        avatar.visibility = View.GONE
    }

//    override fun onAudioDisabled(subscriber: SubscriberKit?, reason: String?) {
////        avatar.visibility = View.VISIBLE
//    }
//
//    override fun onAudioEnabled(subscriber: SubscriberKit?, reason: String?) {
////        avatar.visibility = View.GONE
//    }

    override fun onVideoDisableWarning(p0: SubscriberKit?) {
        Timber.d(LOG_TAG, "onVideoDisableWarning")
    }

    override fun onVideoDisableWarningLifted(p0: SubscriberKit?) {
        Timber.d(LOG_TAG, "onVideoDisableWarningLifted")
    }

    class ParticipantsAdapter(private var participantsList: List<ActiveSpeakerData>, private var height: Int, private var width: Int) :
            RecyclerView.Adapter<ParticipantsAdapter.MyViewHolder>() {
        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var txtSubscriberName: TextView = view.findViewById(R.id.txtSubscriberName)
            var subscriber_item_container: LinearLayout = view.findViewById(R.id.subscriber_item_container)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.video_conference_item2, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onViewAttachedToWindow(holder: MyViewHolder) {
            if (holder is MyViewHolder) {
                holder.setIsRecyclable(false);
            }
            super.onViewAttachedToWindow(holder);
        }

        override fun onViewDetachedFromWindow(holder: MyViewHolder) {
            if (holder is MyViewHolder){
                holder.setIsRecyclable(true);
            }
            super.onViewDetachedFromWindow(holder);
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = participantsList[position]
        holder.setIsRecyclable(false)
        holder.txtSubscriberName.text = item.participantName
            if (item.subscriber?.view?.parent != null) {
                (item.subscriber?.view?.parent as ViewGroup)?.removeView(item.subscriber?.view)
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
                item.subscriber?.view?.setBackgroundResource(R.drawable.video_container_frame);
                item.subscriber?.view?.clipToOutline  =true
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
                }
            } else {
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
                item.subscriber?.view?.setBackgroundResource(R.drawable.video_container_frame);
                item.subscriber?.view?.clipToOutline  =true
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
                }
            }
        }

        override fun getItemCount(): Int {
            return participantsList.size
        }
    }

    class ActiveSpeakerAdapter(private var participantsList: List<ActiveSpeakerData>, private var height: Int, private var width: Int) :
            RecyclerView.Adapter<ActiveSpeakerAdapter.MyViewHolder>() {
        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var txtSubscriberName: TextView = view.findViewById(R.id.txtSubscriberName)
            var subscriber_item_container: LinearLayout = view.findViewById(R.id.subscriber_item_container)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.video_conference_item, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val item = participantsList[position]
            holder.setIsRecyclable(false)
            item.subscriber?.getRenderer()?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL);
            if (item.subscriber?.view?.parent != null) {
                (item.subscriber?.view?.parent as ViewGroup)?.removeView(item.subscriber?.view)
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(width / 2,
                        height / 2
                )
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(true)
                }
            } else {
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        height / 2
                )
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(true)
                }
            }
            holder.txtSubscriberName.text = item.participantName

        }

        override fun getItemCount(): Int {
            return participantsList.size
        }
    }


    class ShareAttendeesAdapter(private var shareScreenAttendeesList: List<ActiveSpeakerData>, private var height: Int, private var width: Int) :
            RecyclerView.Adapter<ShareAttendeesAdapter.MyViewHolder>() {
        inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var txtSubscriberName: TextView = view.findViewById(R.id.txtSubscriberName)
            var subscriber_item_container: LinearLayout = view.findViewById(R.id.subscriber_item_container)
        }

        @NonNull
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                    .inflate(R.layout.video_conference_item2, parent, false)
            return MyViewHolder(itemView)
        }

        override fun onViewAttachedToWindow(holder: MyViewHolder) {
            if (holder is MyViewHolder) {
                holder.setIsRecyclable(false);
            }
            super.onViewAttachedToWindow(holder);
        }

        override fun onViewDetachedFromWindow(holder: MyViewHolder) {
            if (holder is MyViewHolder){
                holder.setIsRecyclable(true);
            }
            super.onViewDetachedFromWindow(holder);
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val item = shareScreenAttendeesList[position]
            holder.setIsRecyclable(false)
            holder.txtSubscriberName.text = item.participantName
            if (item.subscriber?.view?.parent != null) {
                (item.subscriber?.view?.parent as ViewGroup)?.removeView(item.subscriber?.view)
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
                item.subscriber?.view?.setBackgroundResource(R.drawable.video_container_frame);
                item.subscriber?.view?.clipToOutline  =true
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
                }
            } else {
                item.subscriber?.view?.layoutParams = ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT
                )
                item.subscriber?.view?.setBackgroundResource(R.drawable.video_container_frame);
                item.subscriber?.view?.clipToOutline  =true
                holder.subscriber_item_container?.addView(item.subscriber?.view)
                if (item.subscriber?.view is GLSurfaceView) {
                    (item.subscriber?.view as GLSurfaceView).setZOrderMediaOverlay(false)
                }
            }
        }

        override fun getItemCount(): Int {
            return shareScreenAttendeesList.size
        }
    }

    override fun onAudioLevelUpdated(suscriber: SubscriberKit, p1: Float) {
        try {
            if (p1 > 0.2) {
                Log.d("aaaaaa first", ""+suscriber.getStream().streamId)
                var user1: ActiveSpeakerData? = activeSpeakerList.find { it?.streamId == suscriber.getStream().streamId }
                if(user1 == null) {
                    Log.d("aaaaaa second", ""+suscriber.getStream().streamId)
                    if (attendiesList.size > 0) {
                        var activeSpeaker: ActiveSpeakerData? = attendiesList.find { it?.streamId == suscriber.getStream().streamId }
                        if(activeSpeaker != null) {
                            Log.d("aaaaaa third", ""+suscriber.getStream().streamId)
                            attendiesList.remove(activeSpeaker)
                            var user: ActiveSpeakerData = activeSpeakerList.get(1)
                            attendiesList.add(user)
                            activeSpeakerList.add(1, activeSpeaker)
                            activity.runOnUiThread {
                                initActiveListAdapter()
                                initParticipantsListAdapter()
                            }
                        }
                    }
                }
            }
        }catch (exception: Exception){
            exception.printStackTrace()
        }
    }

}

package de.mdoc.modules.files.filter;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class FilterViewModelFactory implements ViewModelProvider.Factory {
    private FilterRepository repository;
    
    public FilterViewModelFactory(FilterRepository repository) {
        this.repository = repository;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new FilterViewModel(repository);
    }
}
package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DeviceImage(var system: String,
                       var code:String,
                       var display:String,
                       var id:String,
                       var imageBase64:String):Parcelable
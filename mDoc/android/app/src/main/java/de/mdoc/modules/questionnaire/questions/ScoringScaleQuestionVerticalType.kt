package de.mdoc.modules.questionnaire.questions

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.scoring_scale_layout.view.beginningScaleTv
import kotlinx.android.synthetic.main.scoring_scale_layout.view.endScaleTv
import kotlinx.android.synthetic.main.scoring_scale_layout.view.label
import kotlinx.android.synthetic.main.scoring_scale_layout.view.seekBar
import kotlinx.android.synthetic.main.scoring_scale_layout_vertical.view.*

internal class ScoringScaleQuestionVerticalType: QuestionType() {
    var start = 0;

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateQuestionView(parent: ViewGroup,
                                      question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.scoring_scale_layout_vertical, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value = false

        view.beginningScaleTv?.text = question.scaleBeginningText
        view.endScaleTv?.text = question.scaleEndText
        start = ((question.questionData.scoringScaleMin * 1) - 1).toInt()
        val end = question.questionData.scoringScaleMax * 1
        val label = view.label

        val seekBar = view.seekBar

        seekBar.max = end.toInt()
        seekBar.min = start.toInt()
        seekBar.progressDrawable.colorFilter =
                PorterDuffColorFilter(ContextCompat.getColor(view.context, R.color.colorPrimary),
                        PorterDuff.Mode.SRC_IN)

        seekBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
//                var show = 0
//                show = if(start.toInt() == -1) progress - 1
//                else progress
//                Log.e("progress",show.toString())
//
//                if(seekBar.progress == start.toInt()|| (seekBar.progress == 0 && start.toInt() < 0)) {
//                    MdocUtil.setTextPositionVertical(0, seekBar, label,labelLayout, 0)
//                    label.text = "Select"
//                    questionIsAnswered.value = false
//                    label.textSize = 8.0f
//                    answers.clear()
//                    answers.add((-1).toString())
//                }else{
//                    MdocUtil.setTextPositionVertical(show, seekBar, label,labelLayout, progress)
//                    questionIsAnswered.value = true
//                    isNewAnswer = true
//                    label.textSize = 10.0f
//                    answers.clear()
//                    answers.add((show).toString())
//                }

                seekBar.thumb = getThumb(progress,label)
                answers.clear()
                answers.add(progress.toString())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })

        // Fill answer if exists
        if (question.answer != null) {
            seekBar.post {
                val progress = (question.answer.answerData.toFloatOrNull() ?: 0f) * 1
                seekBar.progress = (progress).toInt()
                seekBar.thumb = getThumb(progress.toInt(), label)
            }

            answers.add(question.answer.answerData)
        }else{
            seekBar.thumb = getThumb(start, label)
        }

        mandatoryCheck(mandatory, null, question)
        return view
    }

    fun getThumb(progress: Int, label: TextView): Drawable? {
        if(progress == start) {
            label.text = "Select"
            questionIsAnswered.value = false
        }
        else {
            label.text = progress.toString() + ""
            questionIsAnswered.value = true
            isNewAnswer = true
        }

        label.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)
        val bitmap = Bitmap.createBitmap(label.measuredWidth, label.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        canvas.rotate(-90F, (label.measuredWidth/2).toFloat(), (label.measuredHeight/2).toFloat())
        label.layout(0, 0, label.measuredWidth, label.measuredHeight)
        label.draw(canvas)
        return BitmapDrawable(bitmap)
    }
    override fun onSaveQuestion(questionView: View, question: ExtendedQuestion) {
        super.onSaveQuestion(questionView, question)
        isNewAnswer = true
    }
}
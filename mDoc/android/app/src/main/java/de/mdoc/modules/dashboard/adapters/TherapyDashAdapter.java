package de.mdoc.modules.dashboard.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.Appointment;

/**
 * Created by ema on 1/20/17.
 */

public class TherapyDashAdapter extends BaseAdapter {

    ArrayList<Appointment> appointments;
    Context context;
    Typeface regular;
    Typeface thin;
    Typeface medium;

    public TherapyDashAdapter(ArrayList<Appointment> appointments, Context context) {
        this.appointments = appointments;
        this.context = context;
        this.regular = ResourcesCompat.getFont(context, R.font.hk_grotesk_regular);
        this.thin = ResourcesCompat.getFont(context, R.font.hk_grotesk_regular);
        this.medium = ResourcesCompat.getFont(context, R.font.hk_grotesk_medium);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView =  LayoutInflater.from(context).inflate(R.layout.dash_therapy_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        Appointment item = (Appointment) getItem(position);
        holder.timeTv.setTypeface(regular);
        holder.locationTv.setTypeface(thin);
        holder.apoitmentNameTv.setTypeface(regular);
        holder.locationTv.setText(item.getClinic().getRoom());
        holder.apoitmentNameTv.setText(item.getName());

        if(context.getResources().getBoolean(R.bool.phone)){
            holder.timeTv.setText(item.getFrom());

        }else{
            holder.timeTv.setText(item.getFrom() + " " + context.getString(R.string.to) + " " + item.getTo());
            holder.doctorNameTv.setText(item.getDoctor().getName());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return appointments.size();
    }

    @Override
    public Object getItem(int position) {
        return appointments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    static class ViewHolder {

        @BindView(R.id.timeTv)
        TextView timeTv;
        @BindView(R.id.locationTv)
        TextView locationTv;
        @Nullable
        @BindView(R.id.contentHolderLl)
        LinearLayout contentHolderLl;
        @BindView(R.id.apoitmentNameTv)
        TextView apoitmentNameTv;
        @Nullable
        @BindView(R.id.doctorNameTv)
        TextView doctorNameTv;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

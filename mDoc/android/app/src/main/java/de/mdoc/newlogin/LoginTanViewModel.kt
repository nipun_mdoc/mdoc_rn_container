package de.mdoc.newlogin

import android.content.Context
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.network.RestClient
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.newlogin.Model.ImprintResponse
import de.mdoc.newlogin.Model.LoginResponseData
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class LoginTanViewModel : ViewModel() {
    var progressHandler: ProgressHandler? = null
    var context: Context? = null
    var authListener: AuthListener? = null
    val uesr_edit: MutableLiveData<String> = MutableLiveData()
    val captcha_edit: MutableLiveData<String> = MutableLiveData()
    var userMutableLiveData: MutableLiveData<LoginUser>? = null
    val user: MutableLiveData<LoginUser>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData()
            }
            return userMutableLiveData as MutableLiveData<LoginUser>
        }

    fun onLoginClick(view: View?) {
        view?.isClickable = false
        Handler().postDelayed(Runnable {
            view?.isClickable = true
        }, 3000)
        context = view!!.context
//        if (username.length == 0) {
//            MdocUtil.showToastLong( context, "Please check your username")
//            return
//        }
        progressHandler = ProgressHandler(this.context!!)
        val instanceId = FirebaseInstanceId.getInstance().id
        val deviceFriendly = "${android.os.Build.MODEL} # $instanceId"
        var loginUser = LoginUser()
        loginUser!!.client_id = this.context?.getString(R.string.keycloack_clinic_Id)
        loginUser!!.client_secret = this.context?.getString(R.string.client_secret)
        loginUser!!.friendlyName = deviceFriendly
        loginUser!!.totp = ""
        loginUser!!.username = if (uesr_edit.value != null) "otp:"+uesr_edit.value!! else ""//username
        loginUser!!.password = if (uesr_edit.value != null) "OTPa1!otp:"+uesr_edit.value!! else ""
        loginUser!!.captcha = if (captcha_edit.value != null) captcha_edit.value!!.trim() else ""
        userMutableLiveData!!.value = loginUser
    }

    fun onImpressumClick(view: View?) {
        loadImprintData("imprint", view!!.context)
    }

    fun onDatenschutzDieClick(view: View?) {
        loadImprintData("data", view!!.context)
    }

    fun onAGBClick(view: View?) {
        loadImprintData("privacy", view!!.context)
    }

    fun onCloseClick(view: View?) {
        authListener?.onCloseClick()
    }

    fun onRefreshCaptchaClick(view: View?) {
        var context: Context = view!!.context
        loadCaptcha(context)
    }

    public fun loadData(loginUser: LoginUser, context: Context) {
        progressHandler?.showProgress()
        var call: Call<LoginResponseData>;
        if(loginUser.captcha != null && loginUser!!.captcha!!.length > 0){
            call = RestClient.getAuthServiceLogin().authenticateUserCaptcha(loginUser.captcha, loginUser)
        }else{
            call = RestClient.getAuthServiceLogin().authenticateUser(loginUser)
        }
        call!!.enqueue(object : Callback<LoginResponseData?> {
            override fun onResponse(call: Call<LoginResponseData?>, response: Response<LoginResponseData?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    MdocAppHelper.getInstance()
                            .accessToken =
                            response.body()!!.data.tokenType + " " + response.body()!!.data.accessToken
                    MdocAppHelper.getInstance()
                            .refreshToken =
                            response.body()!!.data.tokenType + " " + response.body()!!.data.refreshToken
                    MdocAppHelper.getInstance()
                            .refreshTokenNoHeader =
                            response.body()!!.data.refreshToken
                    authListener?.sucess()

                } else {
                    try{
                        val errorBody: ResponseBody? = response.errorBody()
                        var errorMessage: String = errorBody?.string()!!
                        if(errorMessage.contains("\"data\":\"\""))
                            errorMessage = errorMessage?.replace(",\"data\":\"\"","")
                        val errorResponse = Gson().fromJson(errorMessage, LoginResponseData::class.java)
                        MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.invalid_tan))
                        if(errorResponse.code == "MDOC-IAM-022"||errorResponse.code == "MDOC-IAM-023"){
                            loadCaptcha(context)
                        }else{
                            if(authListener?.isCaptchaVisible()!!)
                                loadCaptcha(context)
                        }
                    }catch (ex : Exception){
                        MdocUtil.showToastLong( context, context?.resources?.getString(R.string.invalid_tan))
                    }
                }
            }

            override fun onFailure(call: Call<LoginResponseData?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong( context, context?.resources?.getString(R.string.login_failed))
            }
        })
    }

    fun loadCaptcha(context: Context) {
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        authListener?.clearCaptchaField()
        var call = RestClient.getAuthServiceLogin().getCaptcha()
        call!!.enqueue(object : Callback<CaptchaResponse?> {
            override fun onResponse(call: Call<CaptchaResponse?>, response: Response<CaptchaResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    authListener?.onLoadCaptcha(response.body());
                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<CaptchaResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.error_upload))
            }
        })
    }

    fun loadImprintData(requestType: String, context: Context) {
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getImprintData(Locale.getDefault().language)
        call!!.enqueue(object : Callback<ImprintResponse?> {
            override fun onResponse(call: Call<ImprintResponse?>, response: Response<ImprintResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    if(requestType.equals("imprint")){
                        authListener?.onAGBClick(response.body()?.data?.get(0)?.display!!)
                    }else if(requestType.equals("data")){
                        authListener?.onAGBClick(response.body()?.data?.get(1)?.display!!)
                    }else if(requestType.equals("privacy")){
                        authListener?.onAGBClick(response.body()?.data?.get(2)?.display!!)
                    }

                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<ImprintResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.error_upload))
            }
        })
    }

}
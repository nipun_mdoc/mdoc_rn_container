package de.mdoc.modules.files.details

import android.content.Context
import android.view.View
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.media.view.ImageMediaView
import de.mdoc.modules.media.view.MediaPdfView
import de.mdoc.pojo.FileListItem
import de.mdoc.pojo.supportedDocumentTypes
import de.mdoc.pojo.supportedImageTypes

fun viewFile(context: Context?,
             item: FileListItem?,
             previewOnly: Boolean = false) {
    if (supportedImageTypes().contains(item?.type)) {

        val url = getUrl(context, item)
        val action = MainNavDirections.globalActionToImage(url)
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                .navigate(action)

    } else if (supportedDocumentTypes().contains(item?.type)) {
        val url = getUrl(context, item)
        val action = MainNavDirections.globalActionToPdf(url = url, previewOnly = previewOnly)
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                .navigate(action)
    }
}

fun viewFileInView(context: Context?,
                   pdfView: MediaPdfView?,
                   imageView: ImageMediaView?,
                   item: FileListItem?,
                   previewOnly: Boolean = false
) {
    if (supportedImageTypes().contains(item?.type)) {
        imageView?.visibility = View.VISIBLE
        imageView?.load(getUrl(context, item))

    } else if (supportedDocumentTypes().contains(item?.type)) {
        pdfView?.visibility = View.VISIBLE
        val url = getUrl(context, item)
        pdfView?.load(url, previewOnly)
    }
}

private fun getUrl(context: Context?, item: FileListItem?): String {
    return context?.getString(R.string.base_url) + "v2/common/files/download/" + item?.id
}


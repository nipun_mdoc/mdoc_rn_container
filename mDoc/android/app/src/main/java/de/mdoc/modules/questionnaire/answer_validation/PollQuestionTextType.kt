package de.mdoc.modules.questionnaire.answer_validation

import androidx.annotation.StringRes
import de.mdoc.R
import java.util.*

enum class PollQuestionTextType(val key:String,@StringRes val description:Int)  {
    STRING("STRING", R.string.questionnaire_validation_default),
    DOUBLE("DECIMAL", R.string.questionnaire_validation_decimal),
    INTEGER("INTEGER", R.string.questionnaire_validation_integer);

    companion object {
        fun fromKey(key: String?) : PollQuestionTextType {
            return values().firstOrNull {
                key?.toLowerCase(Locale.getDefault()) == it.key.toLowerCase(Locale.getDefault())
            } ?: STRING
        }
    }
}
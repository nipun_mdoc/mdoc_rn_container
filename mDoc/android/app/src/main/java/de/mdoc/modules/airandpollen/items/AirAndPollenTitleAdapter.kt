package de.mdoc.modules.airandpollen.items

import android.view.LayoutInflater
import android.view.ViewGroup
import de.mdoc.R
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder

object AirAndPollenTitle

class AirAndPollenTitleAdapter :
    ListItemTypeAdapter<AirAndPollenTitle, ListItemViewHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): ListItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.air_and_pollen_title_line, parent, false)
        return ListItemViewHolder(view)
    }

}
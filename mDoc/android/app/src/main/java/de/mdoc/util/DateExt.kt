package de.mdoc.util

import java.text.SimpleDateFormat
import java.util.*


fun Date.formatMdocDate (dateFormat: String = "dd.MM.yyyy"): String{
    return SimpleDateFormat(dateFormat).format(this)
}

fun Long.formatMdocDate(dateFormat: String = "dd.MM.yyyy"): String {
    val sdf = SimpleDateFormat(dateFormat)
    return sdf.format(this)
}

fun Long.getMonthName (): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val monthDate = SimpleDateFormat("MMMM", Locale.getDefault())
    return monthDate.format(calendar.time)
}

fun Long.getYear (): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    return calendar[Calendar.YEAR].toString()
}

fun Long.isCurrentYear (): Boolean {
    val now = Calendar.getInstance()

    val date = Calendar.getInstance()
    date.timeInMillis = this

    return now[Calendar.YEAR] == date[Calendar.YEAR]
}

fun Long.getMonth (): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    val monthDate = SimpleDateFormat("MM", Locale.getDefault())
    return monthDate.format(calendar.time)
}

fun Long.isDateDifferent(input: Long): Boolean {
    val inputMonth = input.getMonth()
    val inputYear = input.getYear()
    val currentMonth = this.getMonth()
    val currentYear = this.getYear()
    return (inputMonth != currentMonth) || (inputYear != currentYear)
}

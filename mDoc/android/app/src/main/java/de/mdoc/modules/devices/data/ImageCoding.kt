package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageCoding(
        val active: Boolean?=null,
        val code: String?=null,
        val imageBase64: String?=null,
        val system: String?=null,
        val display:String?=null
):Parcelable
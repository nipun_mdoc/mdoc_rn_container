package de.mdoc.modules.covid19.history

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentTestHistoryBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.viewmodel.viewModel

class TestHistoryFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.HealthStatusHistory

    private val testHistoryViewModel by viewModel {
        TestHistoryViewModel()
    }

    private val args: TestHistoryFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentTestHistoryBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_test_history, container, false)
        binding.lifecycleOwner = this
        testHistoryViewModel.allHealthStatus.value=args.healthStatusList.asList()
        binding.testHistoryViewModel = testHistoryViewModel
        binding.handler=TestHistoryHandler()
        return binding.root
    }
}

package de.mdoc.modules.help_support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import kotlinx.android.synthetic.main.help_support_layout.*

class HelpSupportDialog: DialogFragment() {

    private val args: HelpSupportDialogArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.help_support_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnBackHelpSupport.setOnClickListener { dismiss() }
        tvNameTxt.text = args.helpSupportData.name
        tvAppTxt.text = args.helpSupportData.app
        tvBranchTxt.text = args.helpSupportData.branch
        tvApiEndpointTxt.text = args.helpSupportData.api
        tvIamEndpointTxt.text = args.helpSupportData.iam
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.MATCH_PARENT
        dialog?.window?.setLayout(width, height)
        dialog?.setCanceledOnTouchOutside(true)

        super.onResume()
    }
}
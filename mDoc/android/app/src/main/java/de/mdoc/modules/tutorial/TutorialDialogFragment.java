package de.mdoc.modules.tutorial;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.viewpagerindicator.CirclePageIndicator;

import de.mdoc.R;


/**
 * Created by ema on 2/16/18.
 */

public class TutorialDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tutorial_layout, container, false);
        getDialog().setTitle("Tutorial");

        final ViewPager pager = rootView.findViewById(R.id.pager);
        pager.setAdapter(new FragmentPagerAdapter(getChildFragmentManager()) {
                    @Override
                    public Fragment getItem(int position) {
                        switch (position){
                            case 0: return new TutorialFragment(TutorialDialogFragment.this, pager);
                            case 1: return new TutorialQuestionnaireFrag(TutorialDialogFragment.this, pager);
                            case 2: return new TutorialMealFragment(TutorialDialogFragment.this, pager);
                            case 3: return new TutorialNotificationFragment(TutorialDialogFragment.this, pager);
                            case 4: return new TutorialProfileFrag(TutorialDialogFragment.this);
                        }
                        return new TutorialFragment();
                    }

                    @Override
                    public int getCount() {
                        return 5;
                    }
                });

        CirclePageIndicator titleIndicator = rootView.findViewById(R.id.indicator);
        titleIndicator.setViewPager(pager);


        return rootView;
    }

    @Override
    public void onResume() {
        // Sets the height and the width of the DialogFragment
        int width = RelativeLayout.LayoutParams.MATCH_PARENT;
        int height = RelativeLayout.LayoutParams.MATCH_PARENT;
        getDialog().getWindow().setLayout(width, height);

        super.onResume();
    }
}

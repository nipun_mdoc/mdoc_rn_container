package de.mdoc.modules.profile.patient_details.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.mdoc.R;
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt;


public class AutoCompleteInsuranceAdapter extends ArrayAdapter<InsuranceDataKt> {
        private List<InsuranceDataKt> insuranceListFull;

        public AutoCompleteInsuranceAdapter(@NonNull Context context, @NonNull List<InsuranceDataKt> insuranceList) {
            super(context, 0, insuranceList);
            insuranceListFull = new ArrayList<>(insuranceList);
        }

        @NonNull
        @Override
        public Filter getFilter() {
            return countryFilter;
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(
                        R.layout.simple_spinner_item, parent, false
                );
            }

            TextView textViewName = convertView.findViewById(android.R.id.text1);

            InsuranceDataKt insuranceItem = getItem(position);

            if (insuranceItem != null) {
                textViewName.setText(insuranceItem.getName());
            }

            return convertView;
        }

        private Filter countryFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                List<InsuranceDataKt> suggestions = new ArrayList<>();

                if (constraint == null || constraint.length() == 0) {
                    suggestions.addAll(insuranceListFull);
                } else {
                    String filterPattern = constraint.toString().toLowerCase(Locale.getDefault()).trim();

                    for (InsuranceDataKt item : insuranceListFull) {
                        if (item.getName().toLowerCase(Locale.getDefault()).contains(filterPattern)) {
                            suggestions.add(item);
                        }
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                clear();
                addAll((List) results.values);
                notifyDataSetChanged();
            }

            @Override
            public CharSequence convertResultToString(Object resultValue) {
                return ((InsuranceDataKt) resultValue).getName();
            }
        };
    }



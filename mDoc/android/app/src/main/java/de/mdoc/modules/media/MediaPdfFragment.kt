package de.mdoc.modules.media

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.setActionTitle
import kotlinx.android.synthetic.main.fragment_view_pdf.*

class MediaPdfFragment: NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.FilesPreview
    val args: MediaPdfFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view_pdf, null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed {
            val hasMediaTransitionFragment = findNavController().popBackStack(R.id.mediaTransitionFragment, true)
            if (!hasMediaTransitionFragment) {

                val hasFilesTransitionFragment = findNavController().popBackStack(R.id.filesTransitionFragment, true)
                if (!hasFilesTransitionFragment) {
                    findNavController().popBackStack()
                }
            }
        }
        if (args.isComingFromMediaModule) {
            setActionTitle(getString(R.string.media))
        }

        mediaPdfView.load(args.url, args.previewOnly)
    }
}


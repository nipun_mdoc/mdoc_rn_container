package de.mdoc.modules.messages.conversations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputLayout
import de.mdoc.R
import de.mdoc.databinding.FragmentClosedConversationsBinding
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.messages.ConversationsViewModel
import de.mdoc.util.debounce
import kotlinx.android.synthetic.main.fragment_closed_conversations.*

class ClosedConversationsFragment : Fragment(){
    var conversationsViewModel : ConversationsViewModel?=null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentClosedConversationsBinding  = DataBindingUtil.inflate(
                inflater, R.layout.fragment_closed_conversations, container, false)

        binding.lifecycleOwner = this
        binding.handler = ConversationsHandler()
        binding.conversationsViewModel = conversationsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        conversationsViewModel?.getClosedConversations(false)
    }
}
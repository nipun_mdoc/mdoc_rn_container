package de.mdoc.network.response;

import de.mdoc.pojo.InfoData;

/**
 * Created by ema on 3/9/18.
 */

public class InfoResponse extends MdocResponse {

   private InfoData data;

    public InfoData getData() {
        return data;
    }

    public void setData(InfoData data) {
        this.data = data;
    }
}

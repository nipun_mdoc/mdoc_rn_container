package de.mdoc.modules.profile.preview.data

import androidx.annotation.StringRes

class Preview (@StringRes val name: Int, val value: String)
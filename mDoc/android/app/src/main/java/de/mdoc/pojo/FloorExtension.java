package de.mdoc.pojo;

import com.jibestream.jmapandroidsdk.components.Floor;

/**
 * Created by ema on 6/14/17.
 */

public class FloorExtension  {

    private Floor floor;

    public FloorExtension(Floor floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return floor.getName();
    }

    public Floor getFloor() {
        return floor;
    }

    public void setFloor(Floor floor) {
        this.floor = floor;
    }
}

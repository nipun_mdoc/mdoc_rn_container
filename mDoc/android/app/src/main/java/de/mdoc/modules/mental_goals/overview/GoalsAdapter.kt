package de.mdoc.modules.mental_goals.overview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsData
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_mental_goals.view.*

class GoalsAdapter(
        private val listener: Listener
) : RecyclerView.Adapter<GoalsViewHolder>() {

    interface Listener {

        fun onMentalGoalClick(item: MentalGoalsData)

    }

    var items: List<MentalGoalsData> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GoalsViewHolder {
        val view: View =
                LayoutInflater.from(parent.context).inflate(
                        R.layout.placeholder_mental_goals,
                        parent,
                        false
                )

        return GoalsViewHolder(view)
    }

    override fun onBindViewHolder(holder: GoalsViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item = items[position]
        holder.txtPlanNumber.text = String.format("%02d", position + 1)
        holder.txtGoalTitle.text = item.title
        holder.txtDate.text =
                MdocUtil.getDateFromMS(MdocConstants.D_IN_WEEK_DD_MM_YY_FORMAT, item.dueDate)

        holder.txtCreatedBy.text =if(item.coachCreated){
            holder.itemView.context.getString(R.string.created_by_coach)
        }else{
            holder.itemView.context.getString(R.string.created_by_you)
        }

        holder.cvGoal.setOnClickListener {
            listener.onMentalGoalClick(item)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class GoalsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtPlanNumber: TextView = itemView.txt_plan_number
    var txtGoalTitle: TextView = itemView.txt_goal_title
    var txtDate: TextView = itemView.txt_date_holder
    var txtCreatedBy: TextView = itemView.txt_created_by
    var cvGoal: CardView = itemView.cv_goal
}
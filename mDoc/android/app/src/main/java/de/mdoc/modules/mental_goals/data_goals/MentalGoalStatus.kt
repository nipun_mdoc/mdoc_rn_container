package de.mdoc.modules.mental_goals.data_goals

enum class MentalGoalStatus {
   OPEN, ACHIEVED, NEARLY_ACHIEVED, NOT_ACHIEVED, CANCELLED, MODIFIED
}
package de.mdoc.modules.meal_plan.view_model

import android.content.Context
import androidx.lifecycle.MutableLiveData
import de.mdoc.R
import de.mdoc.base.BaseViewModel
import de.mdoc.network.RestClient
import de.mdoc.network.request.MealChoiceRequest
import de.mdoc.network.request.MealPlanRequest
import de.mdoc.pojo.MealChoice
import de.mdoc.pojo.MealOffer
import de.mdoc.pojo.MealPlan
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import java.util.*
import kotlin.collections.ArrayList

class MealWidgetViewModel(val context: Context) : BaseViewModel() {

    var adapterItems = MutableLiveData(listOf<MealOffer>())
    var selectedMealOffer = MutableLiveData(MealOffer())
    var dessert = MutableLiveData(String())

    var mealPlans: List<MealPlan> = emptyList()
    var mealChoices: List<MealChoice> = emptyList()


    override suspend fun networkCallAsync() {
        isLoading.value = true
        val mealInfoResult = RestClient.getService().coroutineGetMealInfo()

        MdocAppHelper.getInstance().allergens = mealInfoResult.data.configurationDetailsMultiValue.allergens
        MdocAppHelper.getInstance().additives = mealInfoResult.data.configurationDetailsMultiValue.additives

        val request = MealPlanRequest()

        with(request) {
            clinicId = MdocAppHelper.getInstance().clinicId
            isStandardOffersOnly = !MdocAppHelper.getInstance().isPremiumPatient
            timeZone = TimeZone.getDefault().id
            fromDate = Calendar.getInstance().timeInMillis
        }

        mealPlans = RestClient.getService().coroutineGetMealPlan(request).data

        Collections.sort(mealPlans) { o1, o2 -> java.lang.Long.valueOf(o1.dateNumber).compareTo(java.lang.Long.valueOf(o2.dateNumber)) }

        val ids = ArrayList<String>()

        for (i in mealPlans.indices) {
            ids.add(mealPlans.get(i).mealPlanId)
        }

        val mealChoiceRequest = MealChoiceRequest()
        with(mealChoiceRequest) {
            caseId = MdocAppHelper.getInstance().caseId
            mealPlanIds = ids
        }

        mealChoices = RestClient.getService().couroutineGetMealChoice(mealChoiceRequest).data

        for (i in mealPlans.indices) {
            for (j in mealChoices.indices) {
                for (k in 0 until mealPlans.get(i).offers.size) {
                    if (mealChoices.get(j).mealId != null && mealChoices.get(j).mealId == mealPlans.get(i).offers[k]._id) {
                        mealPlans.get(i).mealChoice = mealChoices.get(j)
                        mealPlans.get(i).offers[k].isSelected = true
                        mealPlans.get(i).offers[k].isRated = mealChoices.get(j).review != null && mealChoices.get(j).review.isNotEmpty()
                    }
                }
            }
        }

        for (i in mealPlans.indices) {
            if (MdocUtil.isToday(mealPlans[i].dateNumber)) {
                if (context.resources.getBoolean(R.bool.can_user_choose_meal)) {
                    selectedMealOffer.value = getSelectedOffer(mealPlans[i].offers)
                    dessert.value = getDessert(mealPlans[i].offers)
                } else {
                    adapterItems.value = mealPlans[i].offers
                }
            }
        }

        isLoading.value = false
        if(adapterItems.value?.size == 0){
            isShowNoDataMessage.value = true
            isShowContent.value = false
        }else{
            isShowNoDataMessage.value = false
            isShowContent.value = true
        }
    }


    fun getSelectedOffer(offers: ArrayList<MealOffer>): MealOffer? {
        for (mo in offers) {
            if (mo.isSelected) {
                return mo
            }
        }
        return null
    }

    fun getMealName (): String {
        return selectedMealOffer.value?.getName(context.resources)?:context.resources.getString(R.string.menu)
    }

    private fun getDessert(offers: ArrayList<MealOffer>): String? {
        for (dmo in offers) {
            if (dmo.getName(context.resources).equals("Dessert", ignoreCase = true)) {
                return dmo.description
            }
        }
        return offers[0].description
    }
}
package de.mdoc.modules.questionnaire.widget

import android.content.Context
import androidx.annotation.StringRes
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.util.MdocUtil

enum class Section(
    @StringRes val nameResId: Int,
    val typeKeys: Array<String>
) {
    Anamnesis(R.string.anamnesis, arrayOf(MdocConstants.ANAMNESE, MdocConstants.ANAMNESIS)),
    Scoring(R.string.feedback, arrayOf(MdocConstants.FEEDBACK)),
    Feedback(R.string.scoring, arrayOf(MdocConstants.SCORING));

    fun getName(context: Context): String {
        return context.getString(nameResId)
    }

}

data class SectionData(
    val type: Section,
    val started: Int,
    val total: Int,
    val displayName:String
) {

    fun getPercentage(): String {
        return "${MdocUtil.calculateProgress(started, total)}%"
    }

    fun getStarted(): String {
        return "$started/$total"
    }

}

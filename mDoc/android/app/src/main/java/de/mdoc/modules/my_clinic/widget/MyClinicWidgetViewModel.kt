package de.mdoc.modules.my_clinic.widget

import androidx.lifecycle.ViewModel
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class MyClinicWidgetViewModel(
private val repository: RxStayRepository
) : ViewModel() {

    val isInProgress = liveData<Boolean>(false)

    val isLoadingErrorVisible = liveData<Boolean>(false)

    val widgetData = liveData<CurrentClinicData?>(null)

    fun loadData() {
        isInProgress.set(true)
        repository.getCurrentClinic(
                resultListener = ::onDataLoaded,
                errorListener = ::onError
        )
    }

    private fun onDataLoaded(result: CurrentClinicData) {
        isInProgress.set(false)

        widgetData.set(result)
    }


    private fun onError(e: Throwable) {
        Timber.w(e)
        isInProgress.set(false)
        isLoadingErrorVisible.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }
}
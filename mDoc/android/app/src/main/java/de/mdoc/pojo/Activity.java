package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AdisMulabdic on 9/5/17.
 */

public class Activity {

    @SerializedName("de/mdoc/activities")
    @Expose
    private List<Object> activities = null;
    @SerializedName("goals")
    @Expose
    private Goals goals;
    @SerializedName("summary")
    @Expose
    private Summary summary;

    public List<Object> getActivities() {
        return activities;
    }

    public void setActivities(List<Object> activities) {
        this.activities = activities;
    }

    public Goals getGoals() {
        return goals;
    }

    public void setGoals(Goals goals) {
        this.goals = goals;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }
}

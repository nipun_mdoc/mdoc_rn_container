package de.mdoc.util

import de.mdoc.network.response.APIError
import retrofit2.Response

fun Response<*>.getErrorMessage(): String {
    return APIError.fromResponse(this)?.message
        ?: this.message()
        ?: "Error ${this.code()}"
}


fun Response<*>.getErrorCode(): String {
    return APIError.fromResponse(this)?.code ?: ""
}

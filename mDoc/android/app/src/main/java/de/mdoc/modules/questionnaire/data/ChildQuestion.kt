package de.mdoc.modules.questionnaire.data

import de.mdoc.pojo.Answer

data class ChildQuestion(
        var questionId: String? = null,
        var value: String? = null,
        var key: String? = null,
        var answer: Answer? = null
)
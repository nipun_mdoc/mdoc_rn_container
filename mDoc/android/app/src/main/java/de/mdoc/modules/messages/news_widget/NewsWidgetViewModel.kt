package de.mdoc.modules.messages.news_widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.messages.data.ListItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class NewsWidgetViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var conversations: MutableLiveData<List<ListItem>> =
            MutableLiveData(emptyList())
    var query = ""
    fun getNews() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                isShowContent.value = false
                isShowNoData.value = false

                val response = mDocService.getConversations("*reason*",
                        "*letterbox*",
                        "*participant*",
                        query,
                        false,
                        "lastMessageDate",
                        "DESC").data.list?.filter { message -> (message.isPublic) }

                conversations.value=response?.filterIndexed {  index, _ -> (index < 2)}
                isShowContent.value = conversations.value?.isNotEmpty()
                isShowNoData.value = conversations.value?.isEmpty()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowNoData.value = true
                isShowContent.value = false
            }
        }
    }
}
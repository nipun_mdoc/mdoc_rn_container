package de.mdoc.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

class ValueHolder<T>(val value: T)

class ValueLiveData<T> : MutableLiveData<ValueHolder<T>?>() {

    fun set(value: T) {
        setValue(ValueHolder(value))
    }

    fun get(): T {
        val valueHolder = value
        if (valueHolder != null) {
            return valueHolder.value
        } else {
            throw KotlinNullPointerException("Value is not set yet")
        }
    }

    fun doIfSet(action: (T) -> Unit) {
        val valueHolder = value
        if (valueHolder != null) {
            action.invoke(valueHolder.value)
        }
    }

    fun update(action: (T) -> T) {
        val valueHolder = value
        if (valueHolder != null) {
            val result = action.invoke(valueHolder.value)
            value = ValueHolder(result)
        }
    }

    fun isSet(): Boolean {
        return value != null
    }

    override fun setValue(value: ValueHolder<T>?) {
        val current = this.value
        if (current == null || current.value != value?.value) {
            super.setValue(value)
        }
    }

    fun observe(callback: (T) -> Unit) {
        observeForever(Observer {
            if (it != null) {
                callback.invoke(it.value)
            }
        })
    }

    fun observe(owner: LifecycleOwner, callback: (T) -> Unit) {
        observe(owner, Observer {
            if (it != null) {
                callback.invoke(it.value)
            }
        })
    }

    /**
     * This is designed for events which should be delivered to UI. After data is delivered it will
     * clear state.
     */
    fun observerAndClean(owner: LifecycleOwner, callback: (T) -> Unit) {
        observe(owner, Observer {
            if (it != null) {
                callback.invoke(it.value)
                value = null
            }
        })
    }

}
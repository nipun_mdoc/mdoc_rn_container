package de.mdoc.modules.common.export

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.databinding.FragmentGoalExportOptionsBinding
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.util.MimeTypes
import kotlinx.android.synthetic.main.fragment_goal_export_options.*
import java.io.File

abstract class BaseExportFragment : BottomSheetDialogFragment(), BaseExportViewModel.ExportCallback {

    abstract fun downloadPdf()
    abstract fun downloadXml()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentGoalExportOptionsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_goal_export_options, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnExportAsPdf.setOnClickListener {
            downloadPdf()
        }

        btnExportAsXls.setOnClickListener {
            downloadXml()
        }
    }

    override fun onDownload(file: File, type: ExportType) {
        if (file.exists()) {
            when (type) {
                ExportType.PDF -> viewFile(file, MimeTypes.MIME_APPLICATION_PDF)
                ExportType.XLSX -> viewFile(file, MimeTypes.MIME_APPLICATION_VND_MSEXCEL)
            }
        }
    }

    private fun viewFile(file: File, mimeType: String) {
        context?.let {
            val fileURI = FileProvider.getUriForFile(it, it.applicationContext.packageName.toString() + ".provider", file)

            val pdfIntentView = Intent(Intent.ACTION_VIEW)
            pdfIntentView.setDataAndType(fileURI, mimeType)
            pdfIntentView.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            pdfIntentView.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            pdfIntentView.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)

            try {
                dismiss()
                val chooserIntent = Intent.createChooser(pdfIntentView, resources.getString(R.string.open_with))
                startActivity(chooserIntent)
            } catch (e: ActivityNotFoundException) {
                Toast.makeText(context, R.string.no_application, Toast.LENGTH_SHORT).show()
            }
        }
    }
}
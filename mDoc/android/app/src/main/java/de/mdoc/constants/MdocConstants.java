package de.mdoc.constants;

import java.util.ArrayList;
import java.util.HashMap;

import de.mdoc.pojo.Case;

/**
 * Created by ema on 4/6/17.
 */

public class MdocConstants {

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String CONTENT_TYPE = "Content-Type";
    public static final String USER_SOURCE = "User-Source";
    public static final String APPLICATION_JSON = "application/json";
    public static final String ANDROID = "ANDROID";
    public static final String FORM_URL_ENCODED = "application/x-www-form-urlencoded";
    public static final String AUTHORIZATION = "Authorization";

    public static final int MEAL_FRAGMENT = 2;
    public static final int NOTIFICATION_FRAGMENT = 8;
    public static final int FAMILY_ACC_FRAGMENT = 25;
    public static final int EXERCISE_FRAGMENT = 30;

    public static final String DELEGATED_CASE = "delegated-case";
    public static final String ACCEPT_LANGUAGE = "Accept-Language";

    public static final String SCORING = "SCORING";
    public static final String ANAMNESIS = "ANAMNESIS";
    public static final String ANAMNESE = "ANAMNESE";
    public static final String FEEDBACK = "FEEDBACK";
    public static final String CHECKLIST = "CHECKLISTE";


    public static final String POLL_END_MESSAGE = "poll_end_message";

    public static final String INPUT = "INPUT";
    public static final String TEXT = "TEXT";
    public static final String RADIO = "RADIO";
    public static final String CHECKBOX = "CHECKBOX";
    public static final String RATING = "RATING";
    public static final String SCORING_SCALE = "SCORING_SCALE";
    public static final String DROPDOWN = "DROPDOWN";
    public static final String RADIO_MATRIX = "RADIO_MATRIX";

    public static final String EXTERNAL_INPUT_TYPE_DATE = "DATE";
    public static final String EXTERNAL_INPUT_TYPE_TIME = "TIME";
    public static final int PUSH_NOTIFICATION_LIST_LIMIT = 100;

    public static final int SEARCH_WAIT_TIME = 1000;
    public static final String CLINIC_DETAILS = "clinic_details";
    public static final String APPOINTMENTS_NUMBER = "appointments_number";
    public static final String NOTIFICATION_FILTER = "NOTIFICATION_FILTER";

    public static final String SELECTED = "selected";
    public static final String NOT_SELECTED = "not_selected";

    public static final String VERY_POOR = "VERY_POOR";
    public static final String POOR = "POOR";
    public static final String NORMAL = "NORMAL";
    public static final String EXCELLENT = "EXCELLENT";
    public static final String ABSOLUTE = "ABSOLUTE";

    public static final String ROOM = "ROOM";
    public static final String DELEGATED_CLINIC = "delegated-clinic";
    public static final String EXTRA = "extra_string";
    public static final String RESCHEDULING_CONFIRMATION = "AppointmentInstanceReschedulingConfirmationState";

    public static final String DEVICE_NAME = "device_name";
    public static final String REGISTER_URL = "register_url";

    public static final String VEGETARIAN = "vegetarian";
    public static final String GLUTEN_FREE = "glutenFree";
    public static final String PIG = "pig";
    public static final String COW = "cow";
    public static final String CHICKEN = "chicken";
    public static final String FISH = "fish";
    public static final String INFO = "info";
    public static final String KOSHER = "kosher";

    public static final String CASES = "cases";
    public static ArrayList<Case> caseList = new ArrayList<>();
    public static boolean IS_UPDAE_PASSWORD = false;
    public static String NEW_PASSWORD = "";

    //user device
    public static final String USERNAME = "username";
    public static final String USER_DEVICE_ID = "userId";
    public static final String FRIENDLY_NAME = "friendlyName";
    public static final String SECRET_KEY = "secret_key";

    public static final String ERROR_MESSAGE = "Account is not fully set up";

    public static final String INCLUDE_ENCOUNTER = "includeEncounter";
    public static final String SKIP = "skip";
    public static final String LIMIT = "limit";
    public static final String QUERY = "query";
    public static final String FROM = "from";
    public static final String TO = "to";
    public static final String PARTICIPANT = "participant";
    public static final String LAST_SEEN_UPDATE = "lastSeenUpdate";

    public static final String OTP_REQ_MESSAGE = "TOTP and EMOTP property missing";

    public static String OTP_CHALLENGE = "OTP_CHALLENGE";

    public static final String HOUR_AND_MINUTES = "HH:mm";

    public static final String DAY_MONTH_DOT_FORMAT = "dd.MM.yyyy";

    public static final String D_IN_WEEK_DD_MM_YY_FORMAT = "EEEE, dd.MM.yyyy";
    public static final String DAY_MONTH_TIME_DOT_FORMAT = "dd.MM.yyyy HH:mm";

    public static final String PHOTO = "PHOTO";
    public static final String VIDEO = "VIDEO";
    public static final String EPUB = "EPUB";
    public static final String URL = "url";

    public static String STATUS = "activityStatus";
    public static String STATUS_COMPLETED = "completed";
    public static String ACTIVITY_ID = "activityId";
    public static String CARE_PLAN_ID = "careplanId";
    public static String CURRENT_CLINIC_DATA = "current_clinic_data";

    public static String APPOINTMENT_REMINDER = "APPOINTMENT_REMINDER";

    public static final String METRIC_CODE_STEPS_METER = "55423-8";
    public static final String METRIC_CODE_BLOOD_PRESSURE_SYS = "8480-6";
    public static final String METRIC_CODE_BLOOD_PRESSURE_DIA = "8462-4";


    public static final String MAGAZINE = "MAGAZINE";
    public static final String MAGAZINE_ID = "MAGAZINE_ID";
    public static final String MAGAZINE_CATEGORY = "MAGAZINE_CATEGORY";
    public static final String MAGAZINE_PAGE_NUMBER = "MAGAZINE_PAGE_NUMBER";

    public static final String APPOINTMENT_UTS = "APPOINTMENT_UTS";

    public static final String DAY_MONTH_FORMAT_H_M_IMAGE = "ddMMMMHHmmss";

    public static final String CATEGORY_LIST = "CategoryList";
    public static final String CATEGORY_ITEM = "CategoryItem";
    public static final String DIARY_IS_SEARCH = "DiaryIsSearch";

    public static final int DIARY_HEADER = 1;
    public static final int DIARY_FOOTER = 3;

    public static final String DEFAULT_GRAPH_TYPE = "line";
    public static final String USER_AGENT = "User-Agent";
    public static final String DEEP_LINK_URI = "DEEP_LINK_URI";

    public static boolean KEY_MISSING = false;

    public static final double LAT = 50.94767761230469;
    public static final double LON = 6.844818115234375;

    public static final String QUESTIONNAIRE_SMILEY_TEXT = "SMILEY_TEXT";
    public static final String QUESTIONNAIRE_POINTS = "POINTS";
    public static final String APPOINTMENT = "appointment";
    public static final String BOOKING_ADDITIONAL_FIELDS = "booking_additional_fields";

    public static String appointment_title = "appointment_title";

    public static final String DIGITAL_SIGNATURE = "DIGITAL_SIGNATURE";

    public static String DASHBOARD = "DASHBOARD";
    public static String MYPROFILE = "MYPROFILE";
    public static String MODULE_COVID19 = "MODULE_COVID19";
    public static String MODULE_COVID_HASHTAG = "MODULE_COVID_HASHTAG";
    public static String MODULE_THERAPY = "MODULE_THERAPY";
    public static String MODULE_MEALS = "MODULE_MEALS";
    public static String MODULE_AIR_POLLEN = "MODULE_AIR_POLLEN";
    public static String MODULE_QUESTIONNAIRES = "MODULE_QUESTIONNAIRES";
    public static String MODULE_MY_CLINIC = "MODULE_MY_CLINIC";
    public static String MODULE_CHECKLIST = "MODULE_CHECKLIST";
    public static String MODULE_VITALS = "MODULE_VITALS";
    public static String MODULE_HELP_SUPPORT = "MODULE_HELP_SUPPORT";
    public static String MODULE_MEDIA = "MODULE_MEDIA";
    public static String MODULE_PATIENT_JOURNEY = "MODULE_PATIENT_JOURNEY";
    public static String MODULE_FILES = "MODULE_FILES";
    public static String MODULE_DIARY = "MODULE_DIARY";
    public static String MODULE_FAMILY_ACCOUNT = "MODULE_FAMILY_ACCOUNT";
    public static String MODULE_CONSENT = "MODULE_CONSENT";
    public static String MODULE_NOTIFICATION = "MODULE_NOTIFICATION";
    public static String MODULE_EXERCISE = "MODULE_EXERCISE";
    public static String MODULE_BOOKING = "MODULE_BOOKING";
    public static String MODULE_MENTAL_GOALS = "MODULE_MENTAL_GOALS";
    public static String MODULE_MEDICATION_PLAN = "MODULE_MEDICATION_PLAN";
    public static String MODULE_DEVICES = "MODULE_DEVICES";
    public static String MODULE_ENTERTAINMENT = "MODULE_ENTERTAINMENT";
    public static String MODULE_MESSAGE = "MODULE_MESSAGE";
    public static String MODULE_EXTERNAL_SERVICE = "MODULE_EXTERNAL_SERVICE";
    public static String MODULE_MY_STAY = "MODULE_MY_STAY";
    public static String CONFIG_PUSH_NOTIFICATION = "CONFIG_PUSH_NOTIFICATION";
    public static String CONFIG_ROOT_DETECTON_ENABLED = "CONFIG_ROOT_DETECTON_ENABLED";
    public static String MODULE_COVID_MESSAGING = "MODULE_COVID_MESSAGING";
    public static String MODULE_PAIR = "MODULE_PAIR";
    public static String CONFIG_PRIVACY_POLICY = "CONFIG_PRIVACY_POLICY";
    public static String MODULE_MORE = "MODULE_MORE";

    public static String widget = "WIDGET";
    public static String history = "HISTORY";
    public static String bottom_nav = "BOTTOM_NAVIGATION";

    public static String WIDGET_API_DISPLAY_LIMIT = "WIDGET_API_DISPLAY_LIMIT";
    public static String WIDGET_TIME_SPAN_DAYS = "WIDGET_TIME_SPAN_DAYS";
    public static String SHOW_ROOM_NUMBER = "SHOW_ROOM_NUMBER";
    public static String HAS_INDOOR_NAVIGATION = "HAS_INDOOR_NAVIGATION";
    public static String HAS_VIRTUAL_PRACTICE = "HAS_VIRTUAL_PRACTICE";
    public static String HAS_METADATA = "HAS_METADATA";
    public static String HAS_EDIT_APPOINTMENT = "HAS_EDIT_APPOINTMENT";
    public static String APPOINTMENT_EDIT_ICON_ENABLED = "APPOINTMENT_EDIT_ICON_ENABLED";
    public static String HAS_MULTI_CLINIC_APPOINTMENT = "HAS_MULTI_CLINIC_APPOINTMENT";
    public static String IS_PAIR_BUTTON_VISIBLE = "IS_PAIR_BUTTON_VISIBLE";
    public static String HAS_CHANGE_APPOINTMENT = "HAS_CHANGE_APPOINTMENT";
    public static String CAN_ADD_TEST = "CAN_ADD_TEST";
    public static String HAS_EMAIL_IN_EMERGENCY_CONTACT = "HAS_EMAIL_IN_EMERGENCY_CONTACT";
    public static String RATE_MEAL_TODAY = "RATE_MEAL_TODAY";
    public static String CAN_USER_CHOOSE_MEAL = "CAN_USER_CHOOSE_MEAL";
    public static String HAS_HISTORY = "HAS_HISTORY";
    public static String HIDE_VITALS_UNIT = "HIDE_VITALS_UNIT";
    public static String HIDE_COACH_SHARE = "HIDE_COACH_SHARE";
    public static String CAN_FILTER_BOOKABLE_APPOINTMENTS = "CAN_FILTER_BOOKABLE_APPOINTMENTS";
    public static String HAS_COMMENT_FIELDS = "HAS_COMMENT_FIELDS";
    public static String CAN_SEND_MESSAGE = "CAN_SEND_MESSAGE";
    public static String CAN_SELECT_RECEIVER = "CAN_SELECT_RECEIVER";
    public static String HIDE_SENDER_PRPOFESSIONAL = "HIDE_SENDER_PRPOFESSIONAL";
    public static String HIDE_CASE_ID = "HIDE_CASE_ID";
    public static String HIDE_DEPARTMENT = "HIDE_DEPARTMENT";
    public static String HIDE_PHYSICIAN = "HIDE_PHYSICIAN";
    public static String ENTER_QR_MANUALLY = "ENTER_QR_MANUALLY";
    public static String MANDATROY_PAIRING = "MANDATROY_PAIRING";

}
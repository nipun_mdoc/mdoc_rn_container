package de.mdoc.util.token_otp;


/**
 * Created by AdisMulabdic on 9/19/17.
 */

public class TokenCode {
    private final String mCode;
    private final long mStart;
    private final long mUntil;
    private TokenCode mNext;

    public TokenCode(String code, long start, long until) {
        mCode = code;
        mStart = start;
        mUntil = until;
    }

    public TokenCode(String code, long start, long until, TokenCode next) {
        this(code, start, until);
        mNext = next;
    }

    public String getCurrentCode(long time) {
        TokenCode active = getActive(time);
        if (active == null)
            return null;
        return active.mCode;
    }
    public String getCurrentCode1() {
//        TokenCode active = getActive(time);
//        if (active == null)
//            return null;
        return mCode;
    }

    private TokenCode getActive(long curTime) {
        if (curTime >= mStart && curTime < mUntil)
            return this;

        if (mNext == null)
            return null;

        return this.mNext.getActive(curTime);
    }

    private TokenCode getLast() {
        if (mNext == null)
            return this;
        return this.mNext.getLast();
    }
}

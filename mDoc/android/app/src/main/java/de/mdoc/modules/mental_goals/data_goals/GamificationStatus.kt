package de.mdoc.modules.mental_goals.data_goals

enum class GamificationStatus {
    NEW_GOAL, NEARLY_FINISHED, ACHIEVED, NONE
}
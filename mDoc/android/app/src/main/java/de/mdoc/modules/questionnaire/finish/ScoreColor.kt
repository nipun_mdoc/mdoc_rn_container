package de.mdoc.modules.questionnaire.finish

import androidx.annotation.ColorRes
import de.mdoc.R
import java.util.*

enum class ScoreColor(
    val key: String,
    @ColorRes val colorRes: Int
) {
    Red("MdocRed", R.color.scoring_result_red),
    Green("MdocGreen", R.color.scoring_result_green),
    Orange("MdocOrange", R.color.scoring_result_orange),
    Black("MdocBlack", R.color.scoring_result_black),
    Yellow("MdocYellow", R.color.scoring_result_orange);

    companion object {
        fun fromName(key: String) : ScoreColor{
            return values().firstOrNull {
                key.toLowerCase(Locale.getDefault()) == it.key.toLowerCase(Locale.getDefault())
            } ?: Black
        }
    }

}
package de.mdoc.network.request;

public class InsuranceRequest {

    private String type;
    private String subType;

    public InsuranceRequest(String type, String subType) {
        this.type = type;
        this.subType = subType;
    }

    public InsuranceRequest(String subType) {
        this.subType = subType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
}

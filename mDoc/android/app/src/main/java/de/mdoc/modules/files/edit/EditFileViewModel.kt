package de.mdoc.modules.files.edit

import androidx.lifecycle.ViewModel
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.pojo.CodingData
import de.mdoc.pojo.CodingItem
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class EditFileViewModel(
    private val repository: FilesRepository
) : ViewModel() {

    val categories = liveData<List<CodingItem>>()

    init {
        repository.getCategoryCoding(::onCodingReceived, ::onError)
    }

    private fun onCodingReceived(data: CodingData) {
        categories.set(data.list)
    }

    fun onError(e: Throwable) {
        Timber.w(e)
    }

}
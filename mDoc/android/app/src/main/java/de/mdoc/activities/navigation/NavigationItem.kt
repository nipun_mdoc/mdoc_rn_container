package de.mdoc.activities.navigation

import androidx.annotation.DrawableRes
import de.mdoc.R

enum class NavigationItem(
        @DrawableRes val iconRes: Int,
        val menuItem: MenuItem?) {

    AirAndPollen(
            iconRes = R.drawable.meal_gray,
            menuItem = MenuItem.AirAndPollen),
    BookAppointment(
            iconRes = R.drawable.book_appointment,
            menuItem = MenuItem.BookAppointment),
    BookingClinicSelection(
            iconRes = R.drawable.book_appointment,
            menuItem = MenuItem.BookAppointment),
    BookingConfirm(
            iconRes = R.drawable.book_appointment,
            menuItem = MenuItem.BookAppointment),
    BookingFailed(
            iconRes = R.drawable.book_appointment,
            menuItem = MenuItem.BookAppointment),
    BookingSuccess(
            iconRes = R.drawable.book_appointment,
            menuItem = MenuItem.BookAppointment),
    Checklist(
            iconRes = R.drawable.checklist,
            menuItem = MenuItem.CheckList),
    Dashboard(
            iconRes = R.drawable.dashboard_gray,
            menuItem = MenuItem.Dashboard),
    Devices(
            iconRes = R.drawable.stats,
            menuItem = MenuItem.Devices),
    Diary(
            iconRes = R.drawable.diary,
            menuItem = MenuItem.Diary),
    EmergencyContacts(
            iconRes = R.drawable.ic_covid_molecule,
            menuItem = MenuItem.Covid19),
    EmergencyContactsEdit(
            iconRes = R.drawable.ic_covid_molecule,
            menuItem = MenuItem.Covid19),
    EMPTY(
            iconRes = -1,
            menuItem = null),
    Entertainment(
            iconRes = R.drawable.media_not_selected,
            menuItem = MenuItem.Entertainment),
    EntertainmentMagazine(
            iconRes = R.drawable.media_not_selected,
            menuItem = MenuItem.Entertainment),
    EntertainmentSeeAll(
            iconRes = R.drawable.media_not_selected,
            menuItem = MenuItem.Entertainment),
    EntertainmentSearchMagazines(
            iconRes = R.drawable.media_not_selected,
            menuItem = MenuItem.Entertainment),
    Exercise(
            iconRes = R.drawable.exercise_not_selected,
            menuItem = MenuItem.Exercise),
    ExternalServices(
            iconRes = R.drawable.ic_external_services,
            menuItem = MenuItem.ExternalServices),
    FamilyAccount(
            iconRes = R.drawable.ic_family_accounts,
            menuItem = MenuItem.FamilyAccount),
    Files(
            iconRes = R.drawable.files_module_icon,
            menuItem = MenuItem.Files),
    FilesAddNewFile(
            iconRes = R.drawable.files_module_icon,
            menuItem = MenuItem.Files),
    FilesList(
            iconRes = R.drawable.files_module_icon,
            menuItem = MenuItem.Files),
    FilesPreview(
            iconRes = R.drawable.files_module_icon,
            menuItem = MenuItem.Files),
    FilesSearch(
            iconRes = R.drawable.files_module_icon,
            menuItem = MenuItem.Files),
    HealthStatus(
            iconRes = R.drawable.ic_covid_molecule,
            menuItem = MenuItem.Covid19),
    HealthStatusHistory(
            iconRes = R.drawable.ic_covid_molecule,
            menuItem = MenuItem.Covid19),
    HelpSupport(
            iconRes = R.drawable.help,
            menuItem = MenuItem.HelpSupport),
    MealPlan(
            iconRes = R.drawable.meal_gray,
            menuItem = MenuItem.MealPlan),
    Media(iconRes = R.drawable.media_not_selected,
            menuItem = MenuItem.Media),
    MentalGoals(
            iconRes = R.drawable.ic_mental_goals_menu,
            menuItem = MenuItem.MentalGoals),
    MentalGoalsWithHiddenNavigationBar(
            iconRes = R.drawable.ic_mental_goals_menu,
            menuItem = MenuItem.MentalGoals),
    Medication(
            iconRes = R.drawable.ic_medication,
            menuItem = MenuItem.Medication),
    MedicationWithHiddenNavigationBar(
            iconRes = R.drawable.ic_medication,
            menuItem = MenuItem.Medication),
    More(
            iconRes = R.drawable.more,
            menuItem = null),
    Messages(
            iconRes = R.drawable.chat,
            menuItem = MenuItem.Messages),
    MyClinic(
            iconRes = R.drawable.clinic_gray,
            menuItem = MenuItem.MyClinic),
    MyProfile(
            iconRes = R.drawable.profile_img,
            menuItem = MenuItem.MyProfile),
    Notifications(
            iconRes = R.drawable.notification,
            menuItem = MenuItem.Notifications),
    PatientJourney(
            iconRes = R.drawable.ic_journey_widget,
            menuItem = MenuItem.PatientJourney),
    Questionnaire(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Questionnaire),
    QuestionnaireHistory(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Questionnaire),
    QuestionnaireProgress(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Questionnaire),
    QuestionnaireTitle(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Questionnaire),
    QuestionnaireResults(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Questionnaire),
    SearchClinic(
            iconRes = R.drawable.clinic_gray,
            menuItem = MenuItem.MyClinic),
    Therapy(
            iconRes = R.drawable.ic_calendar_module,
            menuItem = MenuItem.Therapy),
    Consent(
            iconRes = R.drawable.questionnaire_gray,
            menuItem = MenuItem.Consent),
    Units(
            iconRes = R.drawable.stats,
            menuItem = MenuItem.Vitals),
    Vitals(
            iconRes = R.drawable.stats,
            menuItem = MenuItem.Vitals),
    VitalsObservations(
            iconRes = R.drawable.stats,
            menuItem = MenuItem.Vitals),
}
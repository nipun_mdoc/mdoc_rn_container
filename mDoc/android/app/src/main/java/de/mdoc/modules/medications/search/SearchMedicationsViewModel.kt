package de.mdoc.modules.medications.search

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.medications.search.data.MedicationData
import de.mdoc.modules.medications.search.data.MedicineDetail
import de.mdoc.modules.medications.search.data.MedicineDetailData
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import kotlinx.coroutines.withTimeoutOrNull
import java.net.SocketTimeoutException

class SearchMedicationsViewModel(private val mDocService: IMdocService): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(true)
    val requestTimeout = MutableLiveData(false)
    var content: MutableLiveData<List<MedicationData>> = MutableLiveData(emptyList())
    var query: MutableLiveData<String> = MutableLiveData()

    fun searchRequest() {
        viewModelScope.launch {
            try {
                networkCallAsync()
            }catch (e: SocketTimeoutException){
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = false
                isShowNoDataMessage.value = true
                requestTimeout.value = true

            }
            catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
                requestTimeout.value = false
            }
        }
    }

    private suspend fun networkCallAsync() = run {
        isLoading.value = true
        isShowNoDataMessage.value = false
        isShowLoadingError.value = false

//        val result = withTimeoutOrNull(60_000L) {
//            content.value = mDocService.getMedications(query.value, null,
//                    "*image*",
//                    "*prescriptions*",
//                    "*compounds*")
//                .data.list
//        }
//        content.value = mDocService.getMedications(query.value, null,
//                "*image*",
//                "*prescriptions*",
//                "*compounds*")
//                .data.list

        content.value = mDocService.getMedications(query.value)
                .data.list
        isLoading.value = false

        if (content.value == null) {
            requestTimeout.value = true
            isShowContent.value = false
            isShowNoDataMessage.value = false
            isShowLoadingError.value = false
        }
        else {

            if (content.value.isNullOrEmpty()) {
                isShowNoDataMessage.value = true
                isShowContent.value = false
            }
            else if (!query.value.isNullOrEmpty()) {
                isShowContent.value = true
                isShowNoDataMessage.value = false
            }
            else {
                isShowNoDataMessage.value = true
                isShowContent.value = false
            }
        }
    }

    fun clearContent() {
        isShowNoDataMessage.value = true
        isShowContent.value = false
        isLoading.value = false
        isShowLoadingError.value = false
    }
}
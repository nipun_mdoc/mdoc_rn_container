package de.mdoc.modules.mental_goals.mental_goal_adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import kotlinx.android.synthetic.main.placeholder_dashboard_action_item.view.*


class DashboardActionItemsAdapter(var context: Context, var items: List<ActionItem>) :
    RecyclerView.Adapter<DashboardActionItemViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DashboardActionItemViewHolder {
        context = parent.context
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_dashboard_action_item, parent, false)


        return DashboardActionItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: DashboardActionItemViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item = items[position]
        val planNumber = String.format("%02d. ", position + 1)

        holder.txtNumber.text = planNumber
        holder.txtAction.text = item.item
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class DashboardActionItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtAction: TextView = itemView.txt_action
    var txtNumber: TextView = itemView.txt_number
}
package de.mdoc.activities.navigation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NavigationViewModel : ViewModel() {

    var onMenuItemClickListener: (MenuItem) -> Unit = {}

    fun updateSelectedItem(navigationItem: NavigationItem) {
        currentItem.value = navigationItem
        if (navigationItem.menuItem != null) {
            currentMenuItem.value = navigationItem.menuItem
        }
    }

    val currentItem = MutableLiveData<NavigationItem>()
    val currentMenuItem = MutableLiveData<MenuItem>()
    val countOfNotifications = MutableLiveData<Int>()

    fun onMenuItemClick(menuItem: MenuItem) {
        onMenuItemClickListener.invoke(menuItem)
    }
}
package de.mdoc.modules.vitals.adapters

import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.devices.data.Measurement
import de.mdoc.modules.vitals.MeasurementBottomSheetDialogFragment
import de.mdoc.modules.vitals.MeasurementBottomSheetDialogFragmentDirections
import kotlinx.android.synthetic.main.placeholder_measurement.view.*


class MeasurementsAdapter(var context: MdocActivity, var items:List<Measurement>?,
                          private var dialogFragment:MeasurementBottomSheetDialogFragment) :
        RecyclerView.Adapter<MeasurementsHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeasurementsHolder {
        val view: View =
                LayoutInflater.from(parent.context).
                        inflate(R.layout.placeholder_measurement,
                                parent, false)

        return MeasurementsHolder(view)
    }

    override fun onBindViewHolder(holder: MeasurementsHolder, position: Int) {

        val item = items?.get(position)
        val rawImage=item?.imageCoding?.imageBase64
        holder.txtName.text=item?.imageCoding?.display

        if(!rawImage.isNullOrEmpty()) {
            val decodedString = Base64.decode(rawImage, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)

            holder.imgMeasurement.setImageBitmap(decodedByte)

            holder.placeholder.setOnClickListener {
                dialogFragment.dismiss()

                val action = MeasurementBottomSheetDialogFragmentDirections.globalToManualEntry(item)
                context.findNavController(R.id.navigation_host_fragment).navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return items?.size?:0
    }
}

class MeasurementsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtName:TextView=itemView.txt_measurement_name
    var imgMeasurement:ImageView=itemView.img_measurement
    val placeholder:LinearLayout=itemView.ll_placeholder
}
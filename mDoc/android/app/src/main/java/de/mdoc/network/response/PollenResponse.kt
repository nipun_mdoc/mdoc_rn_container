package de.mdoc.network.response

data class PollenResponse(
    val code: String,
    val timestamp: Long,
    val description: String,
    val message: String,
    val data: Array<PollenResponseData>
)

data class PollenResponseData(
    val id: String,
    val cts: String,
    val uts: String,
    val rawData: PollenResponseRawData
)

data class PollenResponseRawData(
    val last_update: String,
    val name: String,
    val sender: String,
    val content: Array<PollenResponseContent>
)


data class PollenResponseContent(
    val region_name: String,
    val Pollen: Map<String, Pollen>,
    val partregion_id: Int,
    val partregion_name: String
)

data class Pollen(
    val today: String,
    val tomorrow: String,
    val dayafter_to: String
)
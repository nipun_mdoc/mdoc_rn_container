package de.mdoc.modules.media

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.modules.media.media_adapters.InnerMediaDashboardAdapter
import de.mdoc.modules.media.media_view_model.MediaViewModel
import de.mdoc.network.RestClient
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.util.setActionTitle
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_media_category.*

class MediaCategoryFragment: MdocBaseFragment() {
    private val args: MediaCategoryFragmentArgs by navArgs()
    private val mediaViewModel by viewModel {
        MediaViewModel(
                RestClient.getService())
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_media_category
    }

    override fun init(savedInstanceState: Bundle?) {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setActionTitle(args.category)
        getMediaCategory(args.category ?: "")
    }

    private fun getMediaCategory(mediaCategory: String) {
        mediaViewModel.getMediaCategory(mediaCategory, onSuccess = {
            if (it.isNotEmpty()) {
                rv_category?.layoutManager = LinearLayoutManager(context)
                rv_category?.adapter = InnerMediaDashboardAdapter(requireContext(), it)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_media)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mediaCategories) {
            val action = MediaCategoryFragmentDirections.actionMediaCategoryFragmentToMediaBottomSheetDialogFragment()
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }
}
package de.mdoc.interfaces;

public interface ChecklistListener  {
    void updateList(String id);
    void updatePollVotingId(String pollVotingId);
}

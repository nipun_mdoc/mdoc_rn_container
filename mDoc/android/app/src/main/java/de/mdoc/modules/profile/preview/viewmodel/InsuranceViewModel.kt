package de.mdoc.modules.profile.preview.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.profile.patient_details.PatientDetailsViewModel
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.network.request.CodingRequest
import de.mdoc.network.request.InsuranceRequest
import de.mdoc.network.response.CoverageResponse
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch


class InsuranceViewModel(val mDocService: IMdocService) : ViewModel() {

    val isInsuranceLoaded = de.mdoc.viewmodel.liveData(false)

    // Insurances data
    var accidentInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())
    var militaryInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())
    var disabilityInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())
    var supplementaryInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())
    var supplementaryAccidentInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())
    var healthInsurances: MutableLiveData<ArrayList<InsuranceDataKt>> = MutableLiveData(arrayListOf())

    // Coding
    val insuranceClassList: MutableLiveData<ArrayList<CodingItem>> = MutableLiveData(arrayListOf())
    val supplementaryInsuranceClassList: MutableLiveData<ArrayList<CodingItem>> = MutableLiveData(arrayListOf())

    init {
        getInsurance()
    }

    private fun getInsurance() {
        viewModelScope.launch {
            try {
                isInsuranceLoaded.set(false)
                getInsuranceAsync()
                isInsuranceLoaded.set(true)
            } catch (e: Exception) {
                isInsuranceLoaded.set(false)
            }
        }
    }

    private suspend fun getInsuranceAsync() = coroutineScope {
        val insurance = async {
            healthInsurances.value = mDocService.getInsurance(InsuranceRequest("krank"))
                    .data
        }
        val accident = async {
            accidentInsurances.value = mDocService.getInsurance(InsuranceRequest("unfall"))
                    .data
        }
        val military = async {
            militaryInsurances.value = mDocService.getInsurance(InsuranceRequest("militar"))
                    .data
        }
        val disability = async {
            disabilityInsurances.value = mDocService.getInsurance(InsuranceRequest("invalid"))
                    .data
        }
        val supplementary = async {
            supplementaryInsurances.value = mDocService.getInsurance(InsuranceRequest("zusatz"))
                    .data
        }
        val supplementaryAccident = async {
            supplementaryAccidentInsurances.value = mDocService.getInsurance(InsuranceRequest("unfall_zusatz")).data
        }

        val insuranceClass = async {
            insuranceClassList.value = mDocService.getCodingKt(CodingRequest(PatientDetailsViewModel.INSURANCE_CLASS)).data?.list
        }
        val supplementaryInsuranceClass = async {
            supplementaryInsuranceClassList.value = mDocService.getCodingKt(CodingRequest(PatientDetailsViewModel.SUPPLEMENTARY_INSURANCE_CLASS)).data?.list
        }

        awaitAll(insurance, accident, military, disability, supplementary, supplementaryAccident, insuranceClass, supplementaryInsuranceClass)
    }

    fun getCurrentHealthInsurance(coverage: CoverageResponse? = null): InsuranceDataKt? {
        healthInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.krankReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

    fun getCurrentInsuranceClass(coverage: CoverageResponse? = null): CodingItem? {
        insuranceClassList.value?.forEachIndexed { position, codingItem ->
            if (codingItem.code == coverage?.krankClass) {
                return codingItem
            }
        }
        return null
    }

    fun getCurrentSupplementaryInsuranceClass(coverage: CoverageResponse? = null): CodingItem? {
        supplementaryInsuranceClassList.value?.forEachIndexed { position, codingItem ->
            if (codingItem.code == coverage?.suplementeryClass) {
                return codingItem
            }
        }
        return null
    }

    fun getCurrentAccidentInsuranceClass(coverage: CoverageResponse? = null): CodingItem? {
        insuranceClassList.value?.forEachIndexed { position, codingItem ->
            if (codingItem.code == coverage?.accidentClass) {
                return codingItem
            }
        }
        return null
    }

    fun getCurrentSupplementaryInsurance(coverage: CoverageResponse? = null): InsuranceDataKt? {
        supplementaryInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.suplementeryReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

    fun getCurrentAccidentInsurance(coverage: CoverageResponse?): InsuranceDataKt? {
        accidentInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.acidentReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

    fun getSupplementaryAccidentInsurance(coverage: CoverageResponse? = null): InsuranceDataKt? {
        supplementaryAccidentInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.suplementeryAccidentReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

    fun getSupplementaryAccidentInsuranceClass(coverage: CoverageResponse? = null): CodingItem? {
        supplementaryInsuranceClassList.value?.forEachIndexed { position, codingItem ->
            if (codingItem.code == coverage?.suplementeryAccidentClass) {
                return codingItem
            }
        }
        return null
    }

    fun getDisabilityInsurance(coverage: CoverageResponse? = null): InsuranceDataKt? {
        disabilityInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.invalidReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

    fun getMilitaryInsurance(coverage: CoverageResponse?): InsuranceDataKt? {
        militaryInsurances.value?.forEachIndexed { _, insuranceDataKt ->
            if (insuranceDataKt.id == coverage?.militarReferenceId) {
                return insuranceDataKt
            }
        }
        return null
    }

}
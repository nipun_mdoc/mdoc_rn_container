package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 11/15/16.
 */

public class Doctor implements Serializable {

    private String name;
    private String type;
    private String description;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

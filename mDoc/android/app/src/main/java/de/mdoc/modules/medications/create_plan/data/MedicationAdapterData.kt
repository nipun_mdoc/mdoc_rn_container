package de.mdoc.modules.medications.create_plan.data

import de.mdoc.modules.medications.data.MedicationAdministration
import org.joda.time.DateTime

data class MedicationAdapterData(var fullTimeOfDay:DateTime?,
                                 var timeOfDay: TimeOfDay?,
                                 var innerMedicationData: ArrayList<MedicationAdministration>)

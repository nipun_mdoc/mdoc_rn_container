package de.mdoc.newlogin.network;

import androidx.annotation.Keep;


import de.mdoc.newlogin.LoginUser;
import de.mdoc.newlogin.Model.ForgetPasswordResponseData;
import de.mdoc.newlogin.Model.ForgetRequestData;
import de.mdoc.newlogin.Model.LoginResponseData;
import de.mdoc.newlogin.Model.RegisterRequestData;
import de.mdoc.newlogin.Model.RegistrationResponseData;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;


@Keep
public interface APIService {



    @Headers({"Accept: application/json"})
    @POST("user/login")
    Call<LoginResponseData> doLoginWeb(@Header("Content-Type") String content_type, @Body LoginUser loginRequest);

    @Headers({"Accept: application/json"})
    @POST("user/forgot-password")
    Call<ForgetPasswordResponseData> forgetPassword(@Header("Content-Type") String content_type, @Body ForgetRequestData forgetPassword);

    @Headers({"Accept: application/json"})
    @POST("user/register")
    Call<RegistrationResponseData> registration(@Header("Content-Type") String content_type, @Body RegisterRequestData forgetPassword);


}

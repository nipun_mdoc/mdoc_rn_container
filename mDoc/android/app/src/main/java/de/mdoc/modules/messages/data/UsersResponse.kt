package de.mdoc.modules.messages.data

import de.mdoc.pojo.UserData

data class UsersResponse(
        val code: String? = "",
        val `data`: Data? = Data(),
        val message: String? = "",
        val timestamp: Long? = 0
                        ) {
    data class Data(
            val list: List<UserData>? = listOf(),
            val moreDataAvailable: Boolean? = false,
            val totalCount: Int? = 0
                   ) {
    }
}
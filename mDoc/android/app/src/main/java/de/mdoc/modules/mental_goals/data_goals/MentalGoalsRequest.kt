package de.mdoc.modules.mental_goals.data_goals


data class MentalGoalsRequest(
        var description: String?=null,
        var id: String?=null,
        var ifThenPlans: List<IfThenPlan>?=null,
        var items: ArrayList<ActionItem>?=null,
        var title: String?=null,
        var status:String?=null,
        var dueDate:Long?=null,
        var statusNote:String?=null,
        var notStatus:String?=null
        )
{
    constructor() : this(null, null,
            null, null,
            null,null,null,null)
}
package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.ClinicDetails;

/**
 * Created by ema on 1/24/17.
 */

public class SearchClinicResponse extends MdocResponse {

    private ArrayList<ClinicDetails> data;

    public ArrayList<ClinicDetails> getData() {
        return data;
    }

    public void setData(ArrayList<ClinicDetails> data) {
        this.data = data;
    }
}

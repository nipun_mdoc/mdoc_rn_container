package de.mdoc.modules.diary.export

import android.os.Bundle
import android.view.View
import de.mdoc.modules.common.export.BaseExportFragment
import de.mdoc.modules.diary.data.DiaryFilter
import de.mdoc.network.RestClient
import de.mdoc.network.request.export.DiaryExportRequest
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_goal_export_options.*

class DiaryExportFragment : BaseExportFragment() {

    private val viewModel by viewModel {
        DiaryExportViewModel(RestClient.getService(), this)
    }

    lateinit var filter: DiaryFilter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindVisibleGone(downloadExportProgress, viewModel.isLoading)
    }

    override fun downloadPdf() {
        val request = DiaryExportRequest(clinicIds = listOf<String>(MdocAppHelper.getInstance().clinicId), caseId = MdocAppHelper.getInstance().caseId, from = filter.from?.millis, to = filter.to?.millis, diaryConfigId = filter.categories)
        viewModel.download(request, ExportType.PDF, context)
    }

    override fun downloadXml() {
        val request = DiaryExportRequest(clinicIds = listOf<String>(MdocAppHelper.getInstance().clinicId), caseId = MdocAppHelper.getInstance().caseId, from = filter.from?.millis, to = filter.to?.millis, diaryConfigId = filter.categories)
        viewModel.download(request, ExportType.XLSX, context)
    }
}
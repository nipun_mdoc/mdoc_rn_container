package de.mdoc.pojo;

import java.util.ArrayList;

public class Diary {

    private Integer totalCount;
    private ArrayList<DiaryList> list;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<DiaryList> getList() {
        return list;
    }

    public void setList(ArrayList<DiaryList> list) {
        this.list = list;
    }
}

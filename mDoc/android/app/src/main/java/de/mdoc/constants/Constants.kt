/*
 * Copyright 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

@file:JvmName("Constants")

package de.mdoc.constants

@JvmField val REQUEST_CAMERA_PERMISSION = 1
@JvmField val REQUEST_IMAGE_CAPTURE = 3
@JvmField val FILE_PATH = "file_path"
@JvmField val FILE_NAME = "file_name"


@JvmField val BM57 = "BM57"
@JvmField val AS87 = "AS87"
@JvmField val GL50 = "GL50"
@JvmField val BF600 = "BF600"

@JvmField val SHORT_TIME_FORMAT = "HH"

@JvmField val FACEBOOK = "FACEBOOK"
@JvmField val INSTAGRAM = "INSTAGRAM"
@JvmField val TWITTER = "TWITTER"

@JvmField val CGM_PUSH = "CGM_PUSH"

@JvmField val ACTIVE_INBOX = 0
@JvmField val EXTERNAL_WRITE_PERMISSION_GRANT = 114
@JvmField val REFERENCE_TYPE_MESSAGE = "Message"

@JvmField val TERMS_AND_CONDITION = "TERMS_AND_CONDITION"
@JvmField val PRIVACY_POLICY = "PRIVACY_POLICY"
@JvmField val IMPRESSUM = "IMPRESSUM"

@JvmField val FILE_REJECTED_STATE = "FileInstanceRejectedState"
@JvmField val FILE_UPLOADED_STATE = "FileInstanceUploadedState"
@JvmField val FILE_READY_STATE = "FileInstanceReadyState"
@JvmField val FILE_PROCESSING_STATE = "FileInstanceProcessingState"



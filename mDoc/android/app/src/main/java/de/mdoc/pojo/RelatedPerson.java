package de.mdoc.pojo;

import java.util.ArrayList;

import de.mdoc.modules.profile.patient_details.PersonalIDataFragment;

public class RelatedPerson {

    private String firstName;
    private String lastName;
    private String phone;
    private String relationshipType;
    private ArrayList<ContactPoint> contactPoints;
    private Address address;

    public RelatedPerson() {
        contactPoints = new ArrayList<>();
        address = new Address();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRelationshipType() {
        return relationshipType;
    }

    public void setRelationshipType(String relationshipType) {
        this.relationshipType = relationshipType;
    }

    public ArrayList<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(ArrayList<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getHomePhoneNumber(){
        for (ContactPoint c : contactPoints){
            if(c.getUse() != null && c.getUse().equals(PersonalIDataFragment.ContactPointUse.HOME.name())){
                return c.getValue();
            }
        }
        return "";
    }

    public void setHomePhoneNumber(String homePhoneNumber){
        boolean hasNumber = false;
        for (ContactPoint c : contactPoints){
            if(c.getUse() != null && c.getUse().equals(PersonalIDataFragment.ContactPointUse.HOME.name())){
                hasNumber = true;
                c.setValue(homePhoneNumber);
            }
        }
        if(!hasNumber){
            if (contactPoints == null){
                contactPoints = new ArrayList<>();
            }
            contactPoints.add(new ContactPoint(homePhoneNumber, PersonalIDataFragment.ContactPointUse.HOME.name()));
        }
    }

    public String getWorkPhoneNumber(){
        for (ContactPoint c : contactPoints){
            if(c.getUse() != null && c.getUse().equals(PersonalIDataFragment.ContactPointUse.WORK.name())){
                return c.getValue();
            }
        }
        return "";
    }

    public void setWorkPhoneNumber(String workPhoneNumber){
        boolean hasNumber = false;
        for (ContactPoint c : contactPoints){
            if(c.getUse() != null && c.getUse().equals(PersonalIDataFragment.ContactPointUse.WORK.name())){
                hasNumber = true;
                c.setValue(workPhoneNumber);
            }
        }
        if(!hasNumber){
            if (contactPoints == null){
                contactPoints = new ArrayList<>();
            }
            contactPoints.add(new ContactPoint(workPhoneNumber, PersonalIDataFragment.ContactPointUse.WORK.name()));
        }
    }
}

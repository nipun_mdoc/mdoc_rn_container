package de.mdoc.modules.mental_goals.onboard_mental_goals

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_onboard_second.*

class SecondOnBoardFragment: MdocBaseFragment() {
    var listener:ItemSelectionListener?=null
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_onboard_second
    }
    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupListeners()

        val infoText=resources.getString(R.string.info_mental_onboarding_second)
        txt_info_second.text=MdocUtil.fromHtml(infoText)
    }


    private fun setupListeners() {
        txt_back.setOnClickListener {
            listener?.pagerIndex(0)
        }

        txt_next.setOnClickListener {
            listener?.pagerIndex(2)

        }
        txt_skip_intro?.setOnClickListener {
            val action= MainNavDirections.globalActionToMentalGoalCreation()
            findNavController().navigate(action)
            MdocAppHelper.getInstance().isShowGoalsOnboarding = false
        }
    }

    fun setItemSelectionListener(listener:ItemSelectionListener){
        this.listener=listener
    }
}
package de.mdoc.data.questionnaire

import com.google.gson.annotations.SerializedName

data class QuestionData(
        @SerializedName("question") val question: Map<String, String>,
        @SerializedName("description") val description: Map<String, String>,
        val matrixQuestions: List<Any>,
        val options: List<Option>,
        @SerializedName("scaleBeginningText") val scaleBeginningText: Map<String, String>,
        @SerializedName("scaleEndText") val scaleEndText: Map<String, String>,
        val showCondition: Boolean,
        val showDescription: Boolean,
        val showPoints: Boolean,
        val scoringScaleOrientation: String
)
package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;

public class EncounterData implements Serializable {

    private ArrayList<Account> account;

    public ArrayList<Account> getAccount() {
        return account;
    }

    public void setAccount(ArrayList<Account> account) {
        this.account = account;
    }
}

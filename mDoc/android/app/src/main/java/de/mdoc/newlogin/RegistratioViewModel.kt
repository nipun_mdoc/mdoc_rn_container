package de.mdoc.newlogin

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.network.RestClient
import de.mdoc.newlogin.Model.*
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class RegistratioViewModel() : ViewModel() {

    var context: Context? = null
    var authListener: AuthListener? = null
    var toastListener: RegistratioToastListener? = null
    var progressHandler: ProgressHandler? = null

    val uesr_edit: MutableLiveData<String> = MutableLiveData()
    val firstname_edit: MutableLiveData<String> = MutableLiveData()
    val email_edit: MutableLiveData<String> = MutableLiveData()
    val password_edit: MutableLiveData<String> = MutableLiveData()
    val surname_edit: MutableLiveData<String> = MutableLiveData()
    val mobileNo_edit: MutableLiveData<String> = MutableLiveData()
    val fisrtname1_edit: MutableLiveData<String> = MutableLiveData()
    val surename1_edit: MutableLiveData<String> = MutableLiveData()
    val mobileNO1_edit: MutableLiveData<String> = MutableLiveData()
    val passwordConfirm_edit: MutableLiveData<String> = MutableLiveData()

    private var userMutableLiveData: MutableLiveData<RegisterRequestData>? = null


    val requestData: MutableLiveData<RegisterRequestData>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData()
            }
            return userMutableLiveData as MutableLiveData<RegisterRequestData>
        }


    fun onRegisterClick(view: View?) {

        context = view!!.context
        var loginUser = RegisterRequestData()
        loginUser.email = if (email_edit.value != null) email_edit.value!! else ""
        loginUser.client_id = context?.getString(R.string.keycloack_clinic_Id)
        loginUser.client_secret = context?.getString(R.string.client_secret)
        loginUser.password = if (password_edit.value != null) password_edit.value!! else ""
        loginUser.confirmPassword = if (passwordConfirm_edit.value != null) passwordConfirm_edit.value!! else ""
        loginUser.lastName = if (surname_edit.value != null) surname_edit.value!! else ""
        loginUser.username = if (uesr_edit.value != null) uesr_edit.value!! else ""
        loginUser.firstName = if (firstname_edit.value != null) firstname_edit.value!! else ""
        loginUser.mobilePhone = if (mobileNo_edit.value != null) mobileNo_edit.value!! else ""

        var contactData = RegisterRequestContactData()
        var contactArray = ArrayList<RegisterRequestContactData>()

        contactData.firstName = if (fisrtname1_edit.value != null) fisrtname1_edit.value!! else ""
        contactData.lastName = if (surename1_edit.value != null) surename1_edit.value!! else ""
        contactData.phone = if (mobileNO1_edit.value != null) mobileNO1_edit.value!! else ""
        contactArray.add(contactData)
        loginUser.contacts = contactArray
        userMutableLiveData!!.value = loginUser

    }

    fun loadData(loginUser: RegisterRequestData, context: Context) {
        progressHandler = ProgressHandler(context!!)
        progressHandler?.showProgress()
        val call = RestClient.getAuthServiceLogin().registration(loginUser)
        call!!.enqueue(object : Callback<RegistrationResponseData?> {
            override fun onResponse(call: Call<RegistrationResponseData?>, response: Response<RegistrationResponseData?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    MdocAppHelper.getInstance().accessToken = response.body()!!.data.tokenType + " " + response.body()!!.data.accessToken
                    MdocAppHelper.getInstance().refreshToken = response.body()!!.data.tokenType + " " + response.body()!!.data.refreshToken
                    MdocAppHelper.getInstance().refreshTokenNoHeader = response.body()!!.data.refreshToken

                    MdocAppHelper.getInstance().userPassword = password_edit.value
                    authListener?.sucess()

                } else {
                    progressHandler?.hideProgress()
                    try{
                        val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), RegistrationResponseData::class.java)
                        MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.login_failed))
                    }catch (exc:Exception){
                        MdocUtil.showToastLong(context, context?.resources?.getString(R.string.login_failed))
                    }
                }
            }

            override fun onFailure(call: Call<RegistrationResponseData?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.login_failed))
            }
        })
    }

    fun loadImprintData(requestType: String, context: Context) {
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getImprintData(Locale.getDefault().language)
        call!!.enqueue(object : Callback<ImprintResponse?> {
            override fun onResponse(call: Call<ImprintResponse?>, response: Response<ImprintResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    if(requestType.equals("imprint")){
                        authListener?.onAGBClick(response.body()?.data?.get(0)?.display!!)
                    }else if(requestType.equals("data")){
                        authListener?.onAGBClick(response.body()?.data?.get(1)?.display!!)
                    }else if(requestType.equals("privacy")){
                        authListener?.onAGBClick(response.body()?.data?.get(2)?.display!!)
                    }

                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<ImprintResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.error_upload))
            }
        })
    }

    fun onCloseClick(view: View?) {
        authListener?.onCloseClick()
    }

    fun onImpressumClick(view: View?) {
        loadImprintData("imprint", view!!.context)
    }

    fun onDatenschutzDieClick(view: View?) {
        loadImprintData("data", view!!.context)
    }

    fun onAGBClick(view: View?) {
        loadImprintData("privacy", view!!.context)
    }

    fun onInfolick(view: View?) {
        toastListener?.registrationToast(view)

    }
}
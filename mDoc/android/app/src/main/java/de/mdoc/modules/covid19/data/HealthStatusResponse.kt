package de.mdoc.modules.covid19.data

data class HealthStatusResponse(
        val data: HealthStatusData? = HealthStatusData(),
        val timestamp: Long? = 0
                               )

data class HealthStatusData(
        val list: List<HealthStatus>? = listOf(),
        val moreDataAvailable: Boolean? = false,
        val totalCount: Int? = 0
                           )
package de.mdoc.modules.entertainment.fragments.pager

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.view.animation.AlphaAnimation
import android.view.animation.AnimationSet
import android.view.animation.DecelerateInterpolator
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.gson.Gson
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.entertainment.adapters.pager.MagazinesPagerAdapter
import de.mdoc.modules.entertainment.repository.MagazinesTrackingRepository
import de.mdoc.modules.entertainment.viewmodels.MagazineBookmarkViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineSharedViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineTrackingViewModel
import de.mdoc.network.RestClient
import de.mdoc.pojo.entertainment.MediaResponseDto
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_magazine_preview.*
import android.view.animation.Animation
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.util.*


class MagazinesPagerFragment : MdocFragment(), ViewPager.OnPageChangeListener, View.OnClickListener {

    private var mPreviousPage: Int = -1
    private lateinit var magazine: MediaResponseDto
    private var magazinePageNumber: Int = 0
    private lateinit var pagerAdapter: MagazinesPagerAdapter

    private val viewModelTracking by viewModel {
        MagazineTrackingViewModel(
                repository = MagazinesTrackingRepository(),
                mDocService = RestClient.getService()
        )
    }

    private val viewModelBookmark by viewModel {
        MagazineBookmarkViewModel(mDocService = RestClient.getService(), mediaId = magazine.id)
    }

    private lateinit var sharedViewModel: MagazineSharedViewModel

    override val navigationItem: NavigationItem = NavigationItem.EntertainmentMagazine

    override fun setResourceId(): Int {
        return de.mdoc.R.layout.fragment_magazine_preview
    }

    override fun init(savedInstanceState: Bundle?) {
        val bundle = arguments
        if (bundle != null) {
            magazine = Gson().fromJson(bundle.getString(MdocConstants.MAGAZINE), MediaResponseDto::class.java)
            magazinePageNumber = bundle.getInt(MdocConstants.MAGAZINE_PAGE_NUMBER)
        }

        sharedViewModel = this.run {
            ViewModelProviders.of(this).get(MagazineSharedViewModel::class.java)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        btnMarkAsFinished.setOnClickListener(this)
        btnCloseMagazine.setOnClickListener(this)
        showActionBar()
        setActionTitle(magazine.magazineName)

        initAdapter()

        // Observe page picker
        val navController = findNavController()
        navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>(MagazinePagePickerFragment.SELECTED_PAGE)?.observe(
                viewLifecycleOwner, Observer { item ->
            mPreviousPage = magazinesViewPager.currentItem + 1
            magazinesViewPager.currentItem = item
        })

        // Track open event
        viewModelTracking.trackPageOpen.observe(viewLifecycleOwner, Observer<Int> { item ->
            if (item != -1) {
                viewModelTracking.trackPageOpen(media = magazine, page = item.toString())
            }
        })

        // Track first page after session is successful
        viewModelTracking.isSessionOpen.observe(viewLifecycleOwner, Observer<Boolean> { item ->
            if (item) {
                viewModelTracking.trackPageOpen(media = magazine, page = getUserFriendlyPosition(magazinePageNumber).toString())
            }
        })

        // Enable or disable magazine scrolling
        sharedViewModel.shouldEnableScrolling.observe(viewLifecycleOwner, Observer<Boolean> { item ->
            magazinesViewPager.setPagingEnabled(item)
        })


        // Bookmark
        viewModelBookmark.bookmark.observe(viewLifecycleOwner, Observer {
            magazine.readingHistory = it
            activity?.invalidateOptionsMenu()
            setButtonsVisibility(magazinesViewPager.currentItem)
        })
    }

    private fun initAdapter() {
        pagerAdapter = MagazinesPagerAdapter(parentFragmentManager, magazine.pdfId, magazine.magazineId, (magazine.pageMax?.toInt()
                ?: 0), this)

        magazinesViewPager.addOnPageChangeListener(this)
        magazinesViewPager.adapter = pagerAdapter
        magazinesViewPager.currentItem = magazinePageNumber
        magazinesViewPager.post(Runnable { this.onPageSelected(magazinesViewPager.currentItem) })
    }

    override fun onPageScrollStateChanged(state: Int) {
        // Indicates that the pager is currently being dragged by the user.
        if (state == ViewPager.SCROLL_STATE_DRAGGING) {
            mPreviousPage = magazinesViewPager.currentItem + 1
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    }

    override fun onPageSelected(position: Int) {
        activity?.invalidateOptionsMenu()
        sharedViewModel.disableMagazineScrolling()
        if (mPreviousPage != -1) {
            viewModelTracking.trackPageClose(media = magazine, pageToClose = mPreviousPage.toString(), pageToOpen = getUserFriendlyPosition(position))
        }

        reloadPage(position)
        updatePageIndicator(position)

        if (!isLastPage(position) && !isRead()) {
            updateReadingProgress(position)
            updatePageTooltip(position)
        }

        setButtonsVisibility(position)
    }

    private fun reloadPage(position: Int) {
        (pagerAdapter.instantiateItem(magazinesViewPager, position) as MagazinePageViewFragment).reloadData()
    }

    private fun updateReadingProgress(position: Int) {
        val realPosition = getUserFriendlyPosition(position)
        val calculation: Float = (realPosition.toFloat() / (magazine.pageMax?.toInt() ?: 0))
        val progress = calculation * 100
        readingProgressIndicator.progress = progress.toInt()
    }

    private fun getUserFriendlyPosition(position: Int): Int {
        return position + 1
    }

    private fun updatePageIndicator(position: Int) {
        val realPosition = position + 1
        val indicatorValue = getString(de.mdoc.R.string.entertainment_magazines_page)
        indicator.text = "$indicatorValue $realPosition"
    }

    private fun updatePageTooltip(position: Int) {
        val realPosition = getUserFriendlyPosition(position)
        txtProgressTooltip.text = realPosition.toString()

        var progress: Float = (realPosition.toFloat() / (magazine.pageMax?.toInt() ?: 0))

        // Prevent tooltip view to go outside of the screen
        when (progress) {
            in 0.0..0.1 -> {
                progress = 0.1f
            }
            in 0.9..1.0 -> {
                progress = 0.9f
            }
        }

        progressGuideline.setGuidelinePercent(progress)
        addTooltipAnimation(progressTooltip)
    }

    private fun addTooltipAnimation(view: View) {
        val fadeIn = AlphaAnimation(0f, 1f)
        fadeIn.interpolator = DecelerateInterpolator()
        fadeIn.duration = 1000

        val fadeOut = AlphaAnimation(1f, 0f)
        fadeOut.interpolator = AccelerateInterpolator()
        fadeOut.startOffset = 1000
        fadeOut.duration = 500

        val animation = AnimationSet(false)
        animation.addAnimation(fadeIn)
        animation.addAnimation(fadeOut)

        animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                view.visibility = View.GONE
            }

            override fun onAnimationStart(p0: Animation?) {
                view.visibility = View.VISIBLE
            }
        })

        view.animation = animation
    }

    private fun isLastPage(position: Int): Boolean {
        return magazine.pageMax?.toInt() == getUserFriendlyPosition(position)
    }

    private fun isRead(): Boolean {
        return this.magazine.readingHistory?.read == true
    }

    private fun setButtonsVisibility(position: Int) {
        if (isRead()) {
            setCloseMagazineVisibility()
        } else {
            setMarkAsReadVisibility(position)
        }
    }

    private fun setCloseMagazineVisibility() {
        progressTooltip.visibility = View.GONE
        readingProgressIndicator.visibility = View.GONE
        layoutMarkFinished.visibility = View.GONE
        layoutCloseMagazine.visibility = View.VISIBLE
    }

    private fun setMarkAsReadVisibility(position: Int) {
        if (isLastPage(position)) {
            layoutMarkFinished.visibility = View.VISIBLE
            layoutCloseMagazine.visibility = View.GONE
            readingProgressIndicator.visibility = View.GONE
            progressTooltip.visibility = View.GONE
        } else {
            layoutMarkFinished.visibility = View.GONE
            layoutCloseMagazine.visibility = View.GONE
            readingProgressIndicator.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        viewModelTracking.trackSessionClosed(magazine)
        viewModelBookmark.saveMagazineReadingHistory(magazinesViewPager.currentItem)
        super.onPause()
    }

    override fun onResume() {
        viewModelTracking.trackSessionOpen(magazine)
        super.onResume()
    }

    override fun onClick(view: View?) {
        when (view) {
            btnMarkAsFinished -> {
                viewModelBookmark.markAsFinished(magazinesViewPager.currentItem)
            }
            btnCloseMagazine -> {
                findNavController().popBackStack()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu ,inflater, R.menu.menu_entertainment_magazine)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.entertainmentBookmark -> {
                viewModelBookmark.addOrRemove(magazinesViewPager.currentItem)
            }

            R.id.entertainmentMagazinePagePicker -> {
                val args = Bundle()
                args.putSerializable(MdocConstants.MAGAZINE, Gson().toJson(magazine))
                findNavController().navigate(R.id.action_entertainmentMagazine_to_entertainmentMagazinePagePicker, args)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val menuItem = menu.findItem(R.id.entertainmentBookmark)
        when (viewModelBookmark.bookmark.value?.bookMarkedPages?.find { it -> it == magazinesViewPager.currentItem }) {
            null -> {
                menuItem.setIcon(R.drawable.ic_bookmark_border_24_px)
            }
            else -> {
                menuItem.setIcon(R.drawable.ic_bookmark_24_px)
            }
        }
        menuItem.icon.setTint(ColorUtil.contrastColor(resources))
        super.onPrepareOptionsMenu(menu)
    }
}
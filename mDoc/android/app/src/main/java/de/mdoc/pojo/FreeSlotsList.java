package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FreeSlotsList implements Serializable {

    @SerializedName("appointmentDetails")
    @Expose
    private FreeSlotsAppointmentDetails appointmentDetails;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("integrationType")
    @Expose
    private String integrationType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public FreeSlotsAppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(FreeSlotsAppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


}

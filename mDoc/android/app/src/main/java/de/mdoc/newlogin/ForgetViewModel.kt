package de.mdoc.newlogin

import android.content.Context
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.network.RestClient
import de.mdoc.newlogin.Model.ForgetPasswordRequestData
import de.mdoc.newlogin.Model.ForgetPasswordResponseData
import de.mdoc.newlogin.Model.ImprintResponse
import de.mdoc.newlogin.Model.LoginResponseData
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ForgetViewModel : ViewModel() {

    var context: Context? = null
    var authListener: AuthListener? = null
    var progressHandler: ProgressHandler? = null

    val uesr_edit: MutableLiveData<String> = MutableLiveData()
    var registrationResponse: MutableLiveData<ForgetPasswordResponseData>? = MutableLiveData()
    private var userMutableLiveData: MutableLiveData<ForgetPasswordRequestData>? = null
    val user: MutableLiveData<ForgetPasswordRequestData>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData()
            }
            return userMutableLiveData as MutableLiveData<ForgetPasswordRequestData>
        }

    fun onForgetPasswordClick(view: View?) {
        context = view!!.context

        progressHandler = ProgressHandler(context!!)

        var loginUserData = ForgetPasswordRequestData()

        loginUserData!!.username = if (uesr_edit.value != null) uesr_edit.value!! else ""//username
        userMutableLiveData!!.value = loginUserData
    }


    fun onImpressumClick(view: View?) {
        loadImprintData("imprint", view!!.context)
    }

    fun onDatenschutzDieClick(view: View?) {
        loadImprintData("data", view!!.context)
    }

    fun onAGBClick(view: View?) {
        loadImprintData("privacy", view!!.context)
    }

    fun onCloseClick(view: View?) {
        authListener?.onCloseClick()
    }

    public fun loadData(email: String, forgetRequest: ForgetPasswordRequestData, context: Context) {
        progressHandler?.showProgress()
        MdocAppHelper.getInstance().accessToken = null;
        MdocAppHelper.getInstance().refreshToken =null
        MdocAppHelper.getInstance().refreshTokenNoHeader =null
        val call = RestClient.getAuthServiceLogin().forgetPassword( email,forgetRequest)
        call!!.enqueue(object : Callback<ForgetPasswordResponseData?> {
            override fun onResponse(call: Call<ForgetPasswordResponseData?>, response: Response<ForgetPasswordResponseData?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    MdocUtil.showToastLong( context, if(response.body() != null) response.body()!!.message else "Please check your email and click the reset link")
                    authListener?.sucess()
                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }
            override fun onFailure(call: Call<ForgetPasswordResponseData?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong( context, context?.resources?.getString(R.string.login_failed))
            }
        })
    }

    fun loadImprintData(requestType: String, context: Context) {
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getImprintData(Locale.getDefault().language)
        call!!.enqueue(object : Callback<ImprintResponse?> {
            override fun onResponse(call: Call<ImprintResponse?>, response: Response<ImprintResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    if(requestType.equals("imprint")){
                        authListener?.onAGBClick(response.body()?.data?.get(0)?.display!!)
                    }else if(requestType.equals("data")){
                        authListener?.onAGBClick(response.body()?.data?.get(1)?.display!!)
                    }else if(requestType.equals("privacy")){
                        authListener?.onAGBClick(response.body()?.data?.get(2)?.display!!)
                    }

                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<ImprintResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.error_upload))
            }
        })
    }
}
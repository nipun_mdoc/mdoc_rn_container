package de.mdoc.fragments.chat;


import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.adapters.ChatListAdapater;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.Chat;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChatFragment extends MdocBaseFragment {

    MdocActivity activity;
    @BindView(R.id.btn_send)
    Button btnSend;

    @BindView(R.id.edittext_chatbox)
    EditText messageEditText;

    @BindView(R.id.chatMessagesRv)
    RecyclerView chatMessagesRv;

    private ChatListAdapater adapater;
    private ArrayList<Chat> messages;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_chat;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(activity);
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        chatMessagesRv.setLayoutManager(linearLayoutManager);
        messages = new ArrayList<>();
        adapater = new ChatListAdapater(activity, messages);
        chatMessagesRv.setAdapter(adapater);

        sendChatMessage("hello");

        messageEditText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {

                    String answer = messageEditText.getText().toString();

                    Chat chat = new Chat();
                    chat.setMine(true);
                    chat.setAnswer(answer);

                    messages.add(chat);
                    adapater.notifyDataSetChanged();

                    sendChatMessage(answer);
                    return true;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.btn_send)
    public void onSendClick() {

        String answer = messageEditText.getText().toString();

        Chat chat = new Chat();
        chat.setMine(true);
        chat.setAnswer(answer);

        messages.add(chat);
        adapater.notifyDataSetChanged();

        sendChatMessage(answer);
    }

    private void sendChatMessage(String msg) {
        JsonObject data = new JsonObject();
        data.addProperty("user_talk", msg);
        String language = Locale.getDefault().toString();
        language = language.replace("_", "-");
        data.addProperty("lang", language);

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("token", "f56f5567cabc7c486d975ef57592caff");
        params.put("action", "chat");
        params.put("data", data.toString());

        MdocManager.chatsMessages(params, new Callback<Chat>() {
            @Override
            public void onResponse(Call<Chat> call, Response<Chat> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        messageEditText.setText("");
                        if (response.body() != null) {
                            messages.add(response.body());
                            adapater.notifyDataSetChanged();
                            int lastIndex = messages.size() - 1;
                            messageEditText.setHint(messages.get(lastIndex).getReplySuggestions());
                            chatMessagesRv.scrollToPosition(lastIndex);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<Chat> call, Throwable t) {

            }
        });
    }
}

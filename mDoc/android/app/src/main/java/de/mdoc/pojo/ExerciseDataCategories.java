package de.mdoc.pojo;

import java.util.ArrayList;

public class ExerciseDataCategories {

    private ArrayList<CarePlanDetailsResponses> carePlanDetailsResponses;
    private long cts;
    private String description;
    private String id;
    private String intent;
    private String levelType;
    private String status;
    private String title;
    private long uts;

    public ExerciseDataCategories(ArrayList<CarePlanDetailsResponses> carePlanDetailsResponses, long cts, String description, String id, String intent, String levelType, String status, String title, long uts) {
        this.carePlanDetailsResponses = carePlanDetailsResponses;
        this.cts = cts;
        this.description = description;
        this.id = id;
        this.intent = intent;
        this.levelType = levelType;
        this.status = status;
        this.title = title;
        this.uts = uts;
    }

    public ArrayList<CarePlanDetailsResponses> getCarePlanDetailsResponses() {
        return carePlanDetailsResponses;
    }

    public void setCarePlanDetailsResponses(ArrayList<CarePlanDetailsResponses> carePlanDetailsResponses) {
        this.carePlanDetailsResponses = carePlanDetailsResponses;
    }

    public long getCts() {
        return cts;
    }

    public void setCts(long cts) {
        this.cts = cts;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getLevelType() {
        return levelType;
    }

    public void setLevelType(String levelType) {
        this.levelType = levelType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getUts() {
        return uts;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    @Override
    public String toString(){
        return title;
    }
}

package de.mdoc.modules.devices.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupMenu
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.create_bt_device.MedicalDeviceAdapterData
import de.mdoc.modules.devices.DevicesUtil.deviceItem
import de.mdoc.modules.devices.DevicesUtil.metricCode
import de.mdoc.modules.devices.connected_devices.ConnectedDevicesFragmentDirections
import de.mdoc.modules.vitals.FhirConfigurationUtil.deviceByDeviceName
import de.mdoc.modules.vitals.FhirConfigurationUtil.imageByDeviceName
import de.mdoc.modules.vitals.FhirConfigurationUtil.longNameByDeviceModel
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.util.toBitmap
import kotlinx.android.synthetic.main.placeholder_connected_medical_device.view.*
import org.joda.time.format.DateTimeFormat
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*

private val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
private val timeFormat = DateTimeFormat.forPattern("HH:mm")

class MedicalConnectedAdapter(var context: Context, var items: ArrayList<MedicalDeviceAdapterData>):
        RecyclerView.Adapter<MedicalConnectedViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MedicalConnectedViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(
                    R.layout.placeholder_connected_medical_device,
                    parent, false)
        context = parent.context

        return MedicalConnectedViewHolder(view)
    }

    override fun onBindViewHolder(holder: MedicalConnectedViewHolder, position: Int) {
        val item = items[position]
        holder.updateDevices(item, context)

        holder.constraintLayout.setOnClickListener {
            deviceItem = item
            metricCode = arrayListOf("")

            val data = deviceByDeviceName(item.deviceName)
            if (item.deviceName!!.contains("BF600")) {
                val action = ConnectedDevicesFragmentDirections.actionToFragmentInitializeBf600(data, item.deviceId)
                it.findNavController().navigate(action)

            } else {
                val action = ConnectedDevicesFragmentDirections.actionToFragmentLoadingDevice(
                        deviceData = data,
                        deviceId = item.deviceId)
                it.findNavController().navigate(action)
            }
        }
        holder.constraintLayout.ic_popup_remove.setOnClickListener {
            showPopupMenu(context, items[position].deviceId, position, holder)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun showPopupMenu(context: Context,
                              deviceId: String,
                              position: Int,
                              holder: MedicalConnectedViewHolder) {
        val popup = PopupMenu(context, holder.itemView.findViewById(R.id.ic_popup_remove))

        popup.menuInflater.inflate(R.menu.popup_devices_menu, popup.menu)

        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.removeDevice -> {
                    removeDevice(deviceId, position)
                }
            }
            true
        }
        popup.show()
    }

    private fun removeDevice(deviceId: String, position: Int) {
        MdocManager.removeDevice(deviceId, object: Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    items.removeAt(position)
                    notifyDataSetChanged()
                }
                else {
                    Timber.w("removeDevice ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                Timber.w(t, "removeDevice")
            }
        })
    }
}

class MedicalConnectedViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    var imageView: ImageView = itemView.findViewById<View>(R.id.img_device) as ImageView
    var constraintLayout: ConstraintLayout =
            itemView.findViewById<View>(R.id.cl_medical_device) as ConstraintLayout
    var txtDeviceName: TextView = itemView.findViewById<View>(R.id.txt_device_id) as TextView
    var lastMeasurement: TextView =
            itemView.findViewById<View>(R.id.txt_last_measurement) as TextView

    fun updateDevices(devices: MedicalDeviceAdapterData, context: Context) {
        val lastMeasure = getDateTime(devices.timeOfObservation, context as MdocActivity)

        lastMeasurement.text = lastMeasure

        txtDeviceName.text = longNameByDeviceModel(devices.deviceName)
        imageView.setImageBitmap(imageByDeviceName(devices.deviceName ?: "").toBitmap())
    }
}

private fun getDateTime(effectiveDateTime: Long, activity: MdocActivity): String {
    val date = dateFormat.print(effectiveDateTime)
    val time = timeFormat.print(effectiveDateTime)

    return activity.getString(R.string.vitals_time, date, time)
}
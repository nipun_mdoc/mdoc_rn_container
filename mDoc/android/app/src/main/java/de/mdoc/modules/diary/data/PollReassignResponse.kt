package de.mdoc.modules.diary.data

data class PollReassignResponse(
    val code: String,
    val `data`: Data,
    val message: String,
    val timestamp: Long
)
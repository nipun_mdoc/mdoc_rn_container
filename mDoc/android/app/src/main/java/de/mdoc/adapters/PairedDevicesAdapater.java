package de.mdoc.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.JsonObject;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.modules.profile.MyProfileFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.pojo.PairedDevices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by AdisMulabdic on 10/5/17.
 */

public class PairedDevicesAdapater extends RecyclerView.Adapter<PairedDevicesViewHolder> {

    public Context context;
    public ArrayList<PairedDevices> items;
    public MyProfileFragment fragment;

    public PairedDevicesAdapater(Context context, ArrayList<PairedDevices> items, MyProfileFragment fragment) {
        this.context = context;
        this.items = items;
        this.fragment = fragment;
    }

    @Override
    public PairedDevicesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.paired_devices_item, parent, false);
        context = parent.getContext();

        return new PairedDevicesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final PairedDevicesViewHolder holder, final int position) {

        final PairedDevices item = items.get(position);
        holder.updateUI(item, context);

        holder.txtRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.txtRemove.getText().toString().equals(context.getResources().getString(R.string.confirm))) {
                    changeFirendlyName(position, holder);
                } else {
                    removeDevice(position);
                }
            }
        });

        holder.editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                holder.txtRemove.setText(context.getResources().getString(R.string.confirm));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    private void removeDevice(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.remove_device, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        Button btnConfirm = view.findViewById(R.id.btnConfirmRemove);
        Button btnCancel = view.findViewById(R.id.btnCancelRemove);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userId = items.get(position).getId();
                removePairedDevice(userId, position);
                dialog.dismiss();
            }
        });
    }

    private void changeFirendlyName(final int position, final PairedDevicesViewHolder holder) {
        String deviceId = items.get(position).getId();

        JsonObject body = new JsonObject();
        body.addProperty("friendlyName", holder.editName.getText().toString());

        MdocManager.changeFirendlyName(deviceId, body,  new Callback<MdocResponse>() {
            @Override
            public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                if (response.isSuccessful()) {
                    holder.txtRemove.setText(context.getResources().getString(R.string.remove));
                    items.get(position).setFriendlyName(holder.editName.getText().toString());
                    holder.editName.clearFocus();
                    fragment.getUserDevices();
                }
            }

            @Override
            public void onFailure(Call<MdocResponse> call, Throwable t) {

            }
        });
    }

    private void removePairedDevice(String userId, final int position) {
        MdocManager.deletePairedDevice(userId, new Callback<MdocResponse>() {
            @Override
            public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                if (response.isSuccessful()) {
                    items.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(context, context.getResources().getString(R.string.device_removed), Toast.LENGTH_LONG).show();
                } else {
                    Timber.w("deletePairedDevice %s", APIErrorKt.getErrorDetails(response));
                }
            }

            @Override
            public void onFailure(Call<MdocResponse> call, Throwable t) {
                Timber.w(t, "deletePairedDevice");
            }
        });
    }
}

class PairedDevicesViewHolder extends RecyclerView.ViewHolder {

    private TextView txtStatus;
    public TextView txtRemove;
    public EditText editName;

    public PairedDevicesViewHolder(View itemView) {
        super(itemView);
        this.txtStatus = itemView.findViewById(R.id.txtStatus);
        this.editName = itemView.findViewById(R.id.editName);
        this.txtRemove = itemView.findViewById(R.id.txtRemoveDevice);
    }

    public void updateUI(PairedDevices device, Context context) {
        editName.setText(device.getFriendlyName());
        txtRemove.setText(context.getResources().getString(R.string.remove));

        if (device.getActive()) {
            txtStatus.setText(context.getResources().getString(R.string.connected));
        } else {
            txtStatus.setText(context.getResources().getString(R.string.no_connected));
        }

    }
}

package de.mdoc.modules.mental_goals.goals_widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.mental_goals.data_goals.GamificationStatus
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsData
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.service.IMdocService
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.launch

class GoalsWidgetViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowGamificationStatus = MutableLiveData(false)
    val isShowOpenGoal = MutableLiveData(false)
    var newGoals: MutableLiveData<String> = MutableLiveData("-")
    var almostDone: MutableLiveData<String> = MutableLiveData("-")
    var achieved: MutableLiveData<String> = MutableLiveData("-")
    var mentalGoal: MutableLiveData<MentalGoalsData?> = MutableLiveData(null)

    fun getGoalsData() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            isLoading.value = true

            try {
                isLoading.value = true
                val goalsResponse = async { getOpenGoals() }
                val gamificationResponse = async { getGamification() }

                awaitAll(goalsResponse, gamificationResponse)

                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowOpenGoal.value = false
                isShowGamificationStatus.value = false
            }
        }
    }

    private suspend fun getOpenGoals() {
        try {
            val request = MentalGoalsRequest()
            request.status = "OPEN"

            mentalGoal.value = mDocService.searchMentalGoalsSuspend(request)
                .data.firstOrNull()

            isShowOpenGoal.value = mentalGoal.value != null
        } catch (e: Exception) {
        }
    }

    private suspend fun getGamification() {
        try {
            val gamificationMap = mDocService.searchGamificationMentalGoalsSuspend(MentalGoalsRequest())
                .data

            newGoals.value = intToString(gamificationMap[GamificationStatus.NEW_GOAL.toString()])
            almostDone.value = intToString(gamificationMap[GamificationStatus.NEARLY_FINISHED.toString()])
            achieved.value = intToString(gamificationMap[GamificationStatus.ACHIEVED.toString()])

            isShowGamificationStatus.value = !gamificationMap.isNullOrEmpty()
        } catch (e: Exception) {
        }
    }

    private fun intToString(value: Int?) = value?.let { String.format("%02d", it) } ?: run { "-" }
}
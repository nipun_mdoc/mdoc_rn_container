package de.mdoc.modules.tutorial;

import android.annotation.SuppressLint;
import android.os.Bundle;

import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.fragments.MdocBaseFragment;

/**
 * Created by ema on 2/16/18.
 */

public class TutorialProfileFrag extends MdocBaseFragment {

    TutorialDialogFragment dialog;

    public TutorialProfileFrag(){

    }

    @SuppressLint("ValidFragment")
    public TutorialProfileFrag(TutorialDialogFragment dialog){
        this.dialog = dialog;
    }

    @Override
    protected int setResourceId() {
        return R.layout.tutorial_profile;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @OnClick(R.id.nextBtn)
    public void onNextBtnClick(){
        dialog.dismiss();
    }

    @OnClick(R.id.skipTv)
    public void onSkipTvClick(){
        dialog.dismiss();
    }
}

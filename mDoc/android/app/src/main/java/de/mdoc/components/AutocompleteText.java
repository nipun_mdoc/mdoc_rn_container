package de.mdoc.components;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatAutoCompleteTextView;

/**
 * Created by ema on 3/24/17.
 */

public class AutocompleteText extends AppCompatAutoCompleteTextView {

    public AutocompleteText(Context context) {
        super(context);
    }

    public AutocompleteText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutocompleteText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (focused) {
            performFiltering(getText(), 0);
        }
    }
}

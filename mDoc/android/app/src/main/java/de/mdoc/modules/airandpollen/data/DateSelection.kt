package de.mdoc.modules.airandpollen.data

class DateSelection(
    val date: Date
)

fun MutableList<DateSelection>.addIfNotEmppty (input: List<AirAndPollenInfo>?, type: Date) {
    if (input?.hasAmountData() == true) {
        add(DateSelection(type))
    }
}

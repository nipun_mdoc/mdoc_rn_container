package de.mdoc.modules.patient_journey

import android.content.Context
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.patient_journey.data.MediaDetail

fun openMedia(media: MediaDetail?, context: Context?) {
    if (media?.type == "PHOTO") {
        val url = context?.getString(R.string.base_url) + "v2/media/Media/" + media.id
        val action = MainNavDirections.globalActionToPdf(url = url)
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(action)
    }
    else if (media?.type == "VIDEO") {
        val action = MainNavDirections.globalActionToVideo(media.publicUrl)
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(action)
    }
}
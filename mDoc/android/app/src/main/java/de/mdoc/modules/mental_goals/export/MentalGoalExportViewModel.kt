package de.mdoc.modules.mental_goals.export

import android.content.Context
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.common.export.BaseExportViewModel
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class MentalGoalExportViewModel(private val mDocService: IMdocService, cb: ExportCallback) : BaseExportViewModel<String>(cb) {

    override fun download(request: String, type: ExportType, context: Context?) {
        isLoading.set(true)
        viewModelScope.launch {
            try {
                val response = mDocService.downloadMentalGoals(request, type.value.toUpperCase())
                writeFile(type, context, response)
                isLoading.set(false)
            } catch (e: java.lang.Exception) {
                Timber.d(e)
                isLoading.set(false)
            }
        }
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/20/17.
 */

public class Address implements Serializable {

    private String description;
    private String country;
    private String postalCode;
    private String state;
    private String city;
    private String street;
    private String houseNumber;
    private String countryCode;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getAddressForClinic() {
        if (houseNumber != null) {
            return street + " " + houseNumber + ", " + postalCode + " " + city;
        } else {
            return street + ", " + postalCode + " " + city;
        }
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
}

package de.mdoc.modules.devices.data

data class DevicesForUserRequest(
        val status:String="",
        val fhirDeviceId:String="",
        var deviceId:String="",
        var model:String?=null
){
    //deviceId = id of android device
    //fhirDeviceId=id of bluetooth device
    constructor() : this("","","")
}

package de.mdoc.pojo;

import java.util.ArrayList;

import de.mdoc.modules.profile.patient_details.PersonalIDataFragment;

public class ExtendedUserDetails {

    private String familyDoctorId;
    private ArrayList<Address> addressList;
    private Employer employer;
    private String maidenName;
    private String homeTown;
    private String ssn;
    private ArrayList<ContactPoint> contactPoints;
    private ArrayList<RelatedPerson> relatedPersonsInfo;
    private String nationality;
    private String maritalStatus;
    private String religion;
    private String emergencyInfo;

    public ExtendedUserDetails() {
        employer = new Employer();
        addressList = new ArrayList<Address>();
        contactPoints = new ArrayList<ContactPoint>();
        relatedPersonsInfo = new ArrayList<RelatedPerson>();
    }

    public String getEmergencyInfo() {
        return emergencyInfo;
    }

    public void setEmergencyInfo(String emergencyInfo) {
        this.emergencyInfo = emergencyInfo;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public ArrayList<RelatedPerson> getRelatedPersonsInfo() {
        return relatedPersonsInfo;
    }

    public void setRelatedPersonsInfo(ArrayList<RelatedPerson> relatedPersonsInfo) {
        this.relatedPersonsInfo = relatedPersonsInfo;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }

    public String getHomeTown() {
        return homeTown;
    }

    public void setHomeTown(String homeTown) {
        this.homeTown = homeTown;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public ArrayList<ContactPoint> getContactPoints() {
        return contactPoints;
    }

    public void setContactPoints(ArrayList<ContactPoint> contactPoints) {
        this.contactPoints = contactPoints;
    }

    public String getFamilyDoctorId() {
        return familyDoctorId;
    }

    public void setFamilyDoctorId(String familyDoctorId) {
        this.familyDoctorId = familyDoctorId;
    }

    public ArrayList<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(ArrayList<Address> addressList) {
        this.addressList = addressList;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public String getHomePhoneNumber(){
        for (ContactPoint c : contactPoints){
            if(c.getUse().equals(PersonalIDataFragment.ContactPointUse.HOME.name())){
                return c.getValue();
            }
        }
        return "";
    }

    public void setHomePhoneNumber(String homePhoneNumber){
        boolean hasNumber = false;
        for (ContactPoint c : contactPoints){
            if(c.getUse().equals(PersonalIDataFragment.ContactPointUse.HOME.name())){
                hasNumber = true;
                c.setValue(homePhoneNumber);
            }
        }
        if(!hasNumber){
            if (contactPoints == null){
                contactPoints = new ArrayList<>();
            }
            contactPoints.add(new ContactPoint(homePhoneNumber, PersonalIDataFragment.ContactPointUse.HOME.name()));
        }
    }


    public String getWorkPhoneNumber(){
        for (ContactPoint c : contactPoints){
            if(c.getUse().equals(PersonalIDataFragment.ContactPointUse.WORK.name())){
                return c.getValue();
            }
        }
        return "";
    }

    public void setWorkPhoneNumber(String workPhoneNumber){
        boolean hasNumber = false;
        for (ContactPoint c : contactPoints){
            if(c.getUse().equals(PersonalIDataFragment.ContactPointUse.WORK.name())){
                hasNumber = true;
                c.setValue(workPhoneNumber);
            }
        }
        if(!hasNumber){
            if (contactPoints == null){
                contactPoints = new ArrayList<>();
            }
            contactPoints.add(new ContactPoint(workPhoneNumber, PersonalIDataFragment.ContactPointUse.WORK.name()));
        }
    }

}

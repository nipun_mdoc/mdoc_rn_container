package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 1/20/17.
 */

public class CurrentClinicData implements Serializable {

    private ClinicDetails clinicDetails;
    private PlanDetails planDetails;

    public CurrentClinicData(ClinicDetails clinicDetails) {
        this.clinicDetails = clinicDetails;
    }

    public ClinicDetails getClinicDetails() {
        return clinicDetails;
    }

    public PlanDetails getPlanDetails() {
        return planDetails;
    }

}


package de.mdoc.network.request;

/**
 * Created by ema on 7/10/17.
 */

public class SearchClinicRequest {

    private String query;

    public SearchClinicRequest(String query) {
        this.query = query;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}

package de.mdoc.modules.airandpollen

import androidx.lifecycle.ViewModel
import de.mdoc.modules.airandpollen.data.*
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.combine
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class AirAndPollenViewModel(
    private val mdocAppHelper: MdocAppHelper,
    private val repository: AirAndPollenRepository
) : ViewModel() {

    val dates = liveData<List<DateSelection>>()

    val isInProgress = liveData(true)
    val isError = liveData(false)
    val isContentVisible = liveData(false)
    val selectedDate = liveData<DateSelection>()

    val regionsForSelection = liveData<List<RegionSelector>>()
    val selectedRegion = liveData<Region?>()
    val showNoRegionSelectedMessage = liveData(false)
    val showAirAndPollenInfo = liveData(false)
    val infoList = liveData<List<AirAndPollenInfo>>(emptyList())

    val selectionIndex = combine(regionsForSelection, selectedRegion) { list, item ->
        val index = list.indexOfFirst {
            it.getRegion() == item
        }
        if (index >= 0) index else 0
    }

    init {
        combine(selectedRegion, selectedDate) { region, date ->
            if (region == null) {
                showAirAndPollenInfo.set(false)
                infoList.set(emptyList())
            } else {
                showAirAndPollenInfo.set(true)
                infoList.set(
                    when (date.date) {
                        Date.Today -> region.today
                        Date.Tomorrow -> region.tomorrow
                        Date.DayAfterTomorrow -> region.dayAfterTomorrow
                    }
                )
            }
        }
        combine(regionsForSelection, selectedRegion) { list, current ->
            if (list.isNotEmpty()) {
                showNoRegionSelectedMessage.set(current == null)
            } else {
                showNoRegionSelectedMessage.set(false)
            }
        }
        repository.getAirAndPollen(::onDataLoaded, ::onError)
    }

    private fun onDataLoaded(data: List<Region>) {
        isContentVisible.set(true)
        regionsForSelection.set(mutableListOf<RegionSelector>().also { list ->
            list.add(RegionSelector.NoSelection())
            list.addAll(data.map { region ->
                RegionSelector.RegionSelection(region)
            })
            val savedSelection = mdocAppHelper.selectedPollenRegionId
            list.firstOrNull {
                it is RegionSelector.RegionSelection && it.getRegion()?.id == savedSelection
            }?.let {
                selectedRegion.set(it.getRegion())
            } ?: run {
                selectedRegion.set(null)
            }
        })
        isInProgress.set(false)

        selectedRegion.observe {
            mdocAppHelper.selectedPollenRegionId = it?.id ?: -1
            addTabs(it)
        }
    }
    private fun addTabs (region: Region? = null) {
        // When a region is not selected, we notify fragment and in that case tabs should be set to GONE
        if (region == null) {
            dates.set(listOf())
            return

        }

        if (!dates.isSet() || dates.get().isEmpty()) {
            val datesList = mutableListOf<DateSelection>()
            datesList.addIfNotEmppty(region.today, Date.Today)
            datesList.addIfNotEmppty(region.tomorrow, Date.Tomorrow)
            datesList.addIfNotEmppty(region.dayAfterTomorrow, Date.DayAfterTomorrow)

            dates.set(datesList)
            if (datesList.isNotEmpty()) {
                selectedDate.set(datesList[0])
            }
        }
    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        isInProgress.set(false)
        isError.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }

}
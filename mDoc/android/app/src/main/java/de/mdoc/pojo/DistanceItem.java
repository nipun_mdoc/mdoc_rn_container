package de.mdoc.pojo;

/**
 * Created by AdisMulabdic on 9/5/17.
 */

public class DistanceItem {

    private String activity;
    private double distance;

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
}

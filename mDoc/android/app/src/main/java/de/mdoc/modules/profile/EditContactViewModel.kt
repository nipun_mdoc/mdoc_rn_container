package de.mdoc.modules.profile

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.profile.data.Contact
import de.mdoc.modules.profile.data.ContactsRequest
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch

class EditContactViewModel(private val mDocService: IMdocService): ViewModel() {
    var name: MutableLiveData<String> = MutableLiveData("")
    var email: MutableLiveData<String> = MutableLiveData("")
    var telephone: MutableLiveData<String> = MutableLiveData("")
    var isLoading: MutableLiveData<Boolean> = MutableLiveData(false)
    var relation: MutableLiveData<String> = MutableLiveData("")

    fun updateEmergencyContacts(contacts: ArrayList<Contact>, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                mDocService.patchEmergencyContacts(ContactsRequest(contacts))
                MdocAppHelper.getInstance()
                    .publicUserDetailses.emergencyContacts = contacts
                onSuccess()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }
}
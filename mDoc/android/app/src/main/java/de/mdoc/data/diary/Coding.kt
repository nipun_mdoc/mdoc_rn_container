package de.mdoc.data.diary

data class Coding(
        val active: Boolean,
        val code: String,
        val color: String?,
        val imageBase64: String?,
        val system: String,
        val display: String?
        )
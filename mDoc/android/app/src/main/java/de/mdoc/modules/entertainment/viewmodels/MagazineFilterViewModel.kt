package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.medications.create_plan.data.ListItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class MagazineFilterViewModel(private val mDocService: IMdocService): ViewModel() {

    val LANGUAGE_CODING_URL = "https://mdoc.one/hl7/fhir/ukb-usz/languages"
    val CATEGORIES_CODING_URL = "https://mdoc.one/coding/media/categories/entertainment"

    private val _categories = MutableLiveData<List<ListItem?>?>().apply {
        viewModelScope.launch {
            try {
                value = mDocService.getCodingLight(CATEGORIES_CODING_URL).data?.list?.filter { it?.display?.count() != 0 && it?.display?.contains(",") == false }
            } catch (e: java.lang.Exception) {
                Timber.d(e)
            }
        }
    }

    private val _languages = MutableLiveData<List<ListItem?>?>().apply {
        viewModelScope.launch {
            try {
                value = mDocService.getCodingLight(LANGUAGE_CODING_URL).data?.list
            } catch (e: java.lang.Exception) {
                Timber.d(e)
            }
        }
    }

    val categories: LiveData<List<ListItem?>?> = _categories
    val languages: LiveData<List<ListItem?>?> = _languages

}
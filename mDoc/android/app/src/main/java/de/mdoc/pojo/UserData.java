package de.mdoc.pojo;

/**
 * Created by ema on 1/23/17.
 */

public class UserData {

    private Access accesses;
    private PublicUserDetails publicUserDetails;
    private String userId;
    private String userType;
    private boolean vipPatient;
    private String department;
    private ExtendedUserDetails extendedUserDetails;

    public Access getAccesses() {
        return accesses;
    }

    public void setAccesses(Access accesses) {
        this.accesses = accesses;
    }

    public PublicUserDetails getPublicUserDetails() {
        return publicUserDetails;
    }

    public void setPublicUserDetails(PublicUserDetails publicUserDetails) {
        this.publicUserDetails = publicUserDetails;
    }

    public ExtendedUserDetails getExtendedUserDetails() {
        return extendedUserDetails;
    }

    public void setExtendedUserDetails(ExtendedUserDetails extendedUserDetails) {
        this.extendedUserDetails = extendedUserDetails;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public boolean isVipPatient() {
        return vipPatient;
    }

    public void setVipPatient(boolean vipPatient) {
        this.vipPatient = vipPatient;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
}

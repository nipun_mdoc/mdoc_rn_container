package de.mdoc.pojo;

import de.mdoc.network.response.FileUpdate;
import de.mdoc.network.response.MdocResponse;

public class FileUpdateResponse extends MdocResponse {

    private FileUpdate data;

    public FileUpdate getData() {
        return data;
    }

    public void setData(FileUpdate data) {
        this.data = data;
    }
}
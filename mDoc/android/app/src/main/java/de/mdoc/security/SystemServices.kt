package de.mdoc.security

import android.annotation.TargetApi
import android.app.Activity
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.widget.Toast
import androidx.biometric.BiometricConstants.*
import androidx.biometric.BiometricManager
import androidx.core.content.ContextCompat
import androidx.core.hardware.fingerprint.FingerprintManagerCompat
import de.mdoc.R
import de.mdoc.activities.LoginActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.biometric.EasyFingerPrint
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.newlogin.Interactor.LogiActivty
import de.mdoc.newlogin.Interactor.RegistrationActivity
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper

@TargetApi(Build.VERSION_CODES.M)
class SystemServices(private val context: Context) {

    companion object {
        fun hasPie() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.P
    }

    private val keyguardManager: KeyguardManager

    init {
        keyguardManager = context.getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
    }

    fun canAuthWithBiometric(): Boolean {
        return BiometricManager.from(context)
            .canAuthenticate() == BiometricManager.BIOMETRIC_SUCCESS
    }

    fun canOptIn(): Boolean {
        return !MdocAppHelper.getInstance().isBiometricPrompted && SystemServices(context).canAuthWithBiometric()
    }

    fun optIn(password: String) {
        val encryptionService = EncryptionServices(context)
        val secret = MdocAppHelper.getInstance()
            .secretKey
        if (encryptionService.getFingerprintKey() == null) {
            encryptionService.createFingerprintKey()
        }
        MdocAppHelper.getInstance()
            .isOptIn = true
        MdocAppHelper.getInstance()
            .secretKey = secret
        MdocAppHelper.getInstance()
            .userPassword = password
    }

    fun optOut() {
        val secret = MdocAppHelper.getInstance()
            .secretKey
        MdocAppHelper.getInstance()
            .userAuth = null
        MdocAppHelper.getInstance()
            .authExpireTime = java.lang.Long.valueOf(0)
        MdocAppHelper.getInstance()
            .patientId = null
        MdocAppHelper.getInstance()
            .userId = null
        MdocAppHelper.getInstance()
            .userLastName = null
        MdocAppHelper.getInstance()
            .userFirstName = null
        MdocAppHelper.getInstance()
            .userType = null
        MdocAppHelper.getInstance()
            .department = null
        MdocAppHelper.getInstance()
            .accessToken = null
        MdocAppHelper.getInstance()
            .refreshToken = null
        MdocAppHelper.getInstance()
            .checklistOrder = null
        MdocAppHelper.getInstance()
            .setChildLoged(false)
        MdocAppHelper.getInstance()
            .caseId = null
        MdocAppHelper.getInstance()
            .externalCaseId = null
        MdocAppHelper.getInstance()
            .userPassword = null
        MdocAppHelper.getInstance()
            .isOptIn = false

        MdocAppHelper.getInstance()
            .secretKey = null

        AppPersistence.clearData()
        MentalGoalsUtil.clearData()
        EncryptionServices(context).removeFingerprintKey()
        MdocAppHelper.getInstance()
            .secretKey = secret
        if (context is MdocActivity) {
            var loginIntent : Intent
            if(context.getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
                loginIntent = Intent(context, LogiActivty::class.java)
            }else{
                loginIntent = Intent(context, LoginActivity::class.java)
            }
            loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(loginIntent)
            context.finish()
        }
        else if (context is LogiActivty) {
            var loginIntent : Intent
            if(context.getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
                loginIntent = Intent(context, LogiActivty::class.java)
            }else{
                loginIntent = Intent(context, LoginActivity::class.java)
            }
            loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(loginIntent)
            context.finish()
        }
    }

    fun disableBiometricLogin() {
        MdocAppHelper.getInstance()
            .userPassword = null
        MdocAppHelper.getInstance()
            .isOptIn = false
        EncryptionServices(context).removeFingerprintKey()
    }

    fun authenticateFingerprint(successAction: () -> Unit, cancelAction: () -> Unit, context1: Activity) {
//        try{
//            val activity = try {
//                context as MdocActivity
//            } catch (e: ClassCastException) {
//                context as LogiActivty
//            }
//        } catch (e: java.lang.ClassCastException){
//            val activity = try {
//                context as MdocActivity
//            } catch (e: ClassCastException) {
//                context as RegistrationActivity
//            }
//        }

        if (canAuthWithBiometric()) {

            var easyFingerPrint = EasyFingerPrint(context1)
            easyFingerPrint.setTittle(context.resources.getString(R.string.sign_in))
                    .setSubTittle(context.resources.getString(R.string.confirm_identity))
                    .setColorPrimary(R.color.colorPrimary)
                    .setIcon(ContextCompat.getDrawable(context1,R.mipmap.ic_launcher))
                    .setListern(object : EasyFingerPrint.ResultFingerPrintListern{
                        override fun onError(mensage: String, code: Int) {

                            when(code){
                                EasyFingerPrint.CODE_ERRO_CANCEL -> { easyFingerPrint.cancelScan()
                                    cancelAction()
                                } // TO DO
                                EasyFingerPrint.CODE_ERRO_GREATER_ANDROID_M -> { } // TO DO
                                EasyFingerPrint.CODE_ERRO_HARDWARE_NOT_SUPPORTED -> { } // TO DO
                                EasyFingerPrint.CODE_ERRO_NOT_ABLED -> { } // TO DO
                                EasyFingerPrint.CODE_ERRO_NOT_FINGERS -> { } // TO DO
                                EasyFingerPrint.CODE_NOT_PERMISSION_BIOMETRIC -> { } // TO DO
                            }

                        }

                        override fun onSucess(cryptoObject: FingerprintManagerCompat.CryptoObject?) {
                            successAction()
                        }

                    })
                    .startScan()

        }
        else {
            optOut()
        }
    }
}
package de.mdoc.modules.checklist_details.view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.pojo.Answer
import de.mdoc.pojo.OptionExtension
import de.mdoc.pojo.ShortQuestion
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONException
import java.util.*

class ChecklistDetailsViewModel (private val mDocService: IMdocService, val pollId : String, var pollVotingId : String, var pollAssignId: String, var pollVotingActionId: String) : ViewModel(){

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    var userAnswers: ArrayList<Answer> = arrayListOf()

    var items : MutableLiveData<ArrayList<ShortQuestion>> = MutableLiveData(arrayListOf())
    val allAnswers = HashMap<Int, ArrayList<String>>()
    val allSelections = HashMap<Int, ArrayList<String>>()

    var tempItems : ArrayList<ShortQuestion> = arrayListOf()

    init {
        getAnswersAndQuestionData()
    }

    fun getAnswersAndQuestionData() {
        viewModelScope.launch {
            try {
                isLoading.value = true

                val data = mDocService.getUserAnswers().data
                for (i in data.indices) {
                    if (data.get(i).pollId == pollId && data.get(i).id != null && data.get(i).id == pollVotingId) {
                        userAnswers = data.get(i).answers
                    }
                }

                val response = mDocService.coroutineGetQuestionnaireById(pollAssignId)
                val sections = response.data?.sections?.sortedBy{it.index}
                tempItems.clear()

                sections?.forEach{
                    it.questions.sortBy { it.index }

                    it.questions?.forEach {question ->
                        val optionExtensions = ArrayList<OptionExtension>()

                        for (option in question.questionData.options) {
                            optionExtensions.add(OptionExtension(option))
                        }

                        tempItems.add(ShortQuestion(question.pollId, pollAssignId, question.id, optionExtensions, question.questionData.question, question.questionData.description))

                    }
                }

                setChecklistDetails(response.data.defaultLanguage)

                items.value = tempItems

                isLoading.value = false

                if(items.value?.size == 0){
                    isShowNoData.value = true
                }else{
                    isShowContent.value = true
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }

    private fun setChecklistDetails(language: String) {

        tempItems.forEachIndexed { index, _ ->

            val optionExtensions = ArrayList<OptionExtension>()

            for (j in userAnswers.indices) {

                val uAnswered = ArrayList<String>()

                if (userAnswers.getOrNull(j)?.pollQuestionId == tempItems.getOrNull(index)?.id) {
                    //Convert stringify array to  list
                    try {
                        val array = JSONArray(userAnswers.getOrNull(j)?.answerData)
                        for (k in 0 until array.length()) {
                            uAnswered.add(array.get(k).toString())
                        }
                        allAnswers.put(index, uAnswered)
                        allSelections.put(index, userAnswers.get(j).selection)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                }
            }

            tempItems.getOrNull(index)?.options?.forEach{

                val tempAnswers = if (allAnswers.size == 0 || !allAnswers.containsKey(index))
                    arrayListOf()
                else
                    allAnswers[index]

                val tempSelections = if (allSelections.size == 0 || !allSelections.containsKey(index))
                    arrayListOf()
                else
                    allSelections[index]

                optionExtensions.add(OptionExtension(getMatchedSelection(tempSelections, it.key), getMatchedAnswer(tempAnswers, it.getValue(language)), it, language))
            }

            tempItems.getOrNull(index)?.options = optionExtensions
        }

    }

    private fun getMatchedAnswer(answers: ArrayList<String>?, optionValue: String): String? {

        answers?.forEach {
            if (it == optionValue) {
                return it
            }
        }
        return null
    }

    private fun getMatchedSelection(selections: ArrayList<String>?, optionKey: String): String? {

        selections?.forEach {
            if (it == optionKey) {
                return it
            }
        }
        return null
    }
}
package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.entertainment.data.OverviewAndBookmarks
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaReadingHistoryResponseDto_
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaStatisticResponseDto_
import de.mdoc.service.IMdocService
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import timber.log.Timber

class MagazineOverviewViewModel(private val mDocService: IMdocService, private val languages: String, private val categories: String) : ViewModel() {

    val data = liveData<OverviewAndBookmarks?>()
    val isError = liveData(false)
    private var job: Job? = null

    init {
        getEntertainmentStatistic(languages, categories)
    }

    fun getEntertainmentStatistic(languages: String, categories: String, forceFetch: Boolean = false) {
        isError.set(false)
        getEntertainmentStatisticSuspend(languages, categories, forceFetch)
    }

    private fun getEntertainmentStatisticSuspend(languages: String, categories: String, forceFetch: Boolean) {
        if (job?.isActive == false || job == null || forceFetch) {
            Timber.d("Reload statistic")
            job = viewModelScope.launch {
                var responseReadingHistory: ResponseSearchResultsMediaReadingHistoryResponseDto_? = null
                var responseMagazines: ResponseSearchResultsMediaStatisticResponseDto_? = null

                try {
                    responseReadingHistory = mDocService.getMagazinesReadingHistory()

                } catch (ex: Exception) {
                    Timber.d(ex)
                }

                try {
                    responseMagazines = mDocService.getEntertainmentStatistic(languages, categories)

                    // Sort categories - ASC
                    responseMagazines.data?.list?.sortWith(compareBy { it.category })

                    // Sort magazines by publicationDate - DESC
                    responseMagazines.data?.list?.forEach {
                        it.content?.sortWith(compareByDescending { it.publicationDate })
                    }

                } catch (ex: Exception) {
                    Timber.d(ex)
                    isError.set(true)
                }

                data.set(OverviewAndBookmarks(responseMagazines, responseReadingHistory))
            }
        }
    }
}
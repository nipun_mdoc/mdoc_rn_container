package de.mdoc.newlogin

interface LogInHandler {
    fun onLogInClicked()
}
package de.mdoc.modules.indoor_navigation

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.NewBaseFragment
import kotlinx.android.synthetic.main.webview_fragment.*

class IndoorNavigationFragment: NewBaseFragment() {
    override val navigationItem = NavigationItem.EMPTY

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.webview_fragment, container, false)
    }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView?.settings?.javaScriptEnabled = true
        webView?.settings?.javaScriptCanOpenWindowsAutomatically = true
        webView?.settings?.defaultTextEncodingName = TEXT_ENCODING
        webView?.requestFocus(View.FOCUS_DOWN)
        webView?.loadUrl(INDOOR_URL)
        webView?.webViewClient = object: WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
                view.loadUrl(request.toString())
                return true
            }
        }
    }

    companion object {
        const val INDOOR_URL = "https://webglv2.virtual-twins.com/#/9_3011_03.2T"
        const val TEXT_ENCODING = "utf-8"
    }
}
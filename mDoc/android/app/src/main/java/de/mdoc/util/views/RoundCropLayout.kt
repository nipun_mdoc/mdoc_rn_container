package de.mdoc.util.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.util.AttributeSet
import android.view.View
import android.widget.RelativeLayout
import de.mdoc.R

class RoundCropLayout : RelativeLayout {

    private var cornersSize = 0
    private val path = Path()

    constructor(context: Context) : super(
        context
    )

    constructor(context: Context, attrs: AttributeSet) : super(
        context, attrs
    ) {
        initAttrs(attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context, attrs, defStyleAttr
    ) {
        initAttrs(attrs)
    }

    private fun initAttrs(attrs: AttributeSet) {
        context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.RoundCropLayout,
            0, 0
        ).apply {
            try {
                cornersSize = getDimensionPixelSize(R.styleable.RoundCropLayout_corners, 0)
            } finally {
                recycle()
            }
        }
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        if (changed) {
            path.reset()
            path.addRoundRect(
                0f + paddingLeft, 0f + paddingTop,
                width.toFloat() - paddingRight,
                height.toFloat() - paddingBottom,
                cornersSize.toFloat(), cornersSize.toFloat(), Path.Direction.CW
            )
        }
    }

    override fun drawChild(canvas: Canvas, child: View, drawingTime: Long): Boolean {
        val count = canvas.save()
        canvas.clipPath(path)
        val result = super.drawChild(canvas, child, drawingTime)
        canvas.restoreToCount(count)
        return result
    }

}
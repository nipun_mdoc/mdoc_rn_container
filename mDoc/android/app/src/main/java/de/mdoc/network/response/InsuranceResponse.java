package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt;
import de.mdoc.pojo.InsuranceData;

public class InsuranceResponse extends MdocResponse {

    private ArrayList<InsuranceDataKt> data;

    public ArrayList<InsuranceDataKt> getData() {
        return data;
    }

    public void setData(ArrayList<InsuranceDataKt> data) {
        this.data = data;
    }
}

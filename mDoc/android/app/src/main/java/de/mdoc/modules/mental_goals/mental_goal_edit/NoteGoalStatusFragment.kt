package de.mdoc.modules.mental_goals.mental_goal_edit

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.MentalGoalStatus
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.modules.mental_goals.data_goals.SingleMentalGoalResponse
import de.mdoc.network.managers.MdocManager
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_goal_status_note.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class NoteGoalStatusFragment: MdocFragment() {
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_goal_status_note
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        et_note?.background?.mutate()?.colorFilter =
                PorterDuffColorFilter(ContextCompat.getColor(activity, R.color.black),
                        PorterDuff.Mode.SRC_ATOP)

        fillTextBySelection()
    }

    private fun fillTextBySelection() {
        val status = parseSelection()

        txt_status_label?.text = activity.resources.getString(R.string.describe_status_selected, status)
    }

    private fun parseSelection(): String? {
        var goalStatus = ""
        when (MentalGoalsUtil.statusChange) {
            MentalGoalStatus.ACHIEVED.toString()        -> goalStatus = resources.getString(R.string.fully_achieved)
            MentalGoalStatus.NEARLY_ACHIEVED.toString() -> goalStatus = resources.getString(R.string.partially_achieved)
            MentalGoalStatus.NOT_ACHIEVED.toString()    -> goalStatus = resources.getString(R.string.not_achieved)
            MentalGoalStatus.CANCELLED.toString()       -> goalStatus = resources.getString(R.string.rejected)
            MentalGoalStatus.MODIFIED.toString()        -> goalStatus = resources.getString(R.string.modified)
        }


        return goalStatus.toLowerCase(Locale.getDefault())
    }

    private fun setupListeners() {
        txt_cancel?.setOnClickListener {
            MentalGoalsUtil.clearGoalStatus()
            findNavController().popBackStack()
        }

        txt_set_status?.setOnClickListener {
            if (isEmpty()) {
                MdocUtil.showToastShort(activity, resources.getString(R.string.fill_all_fields))
            }
            else {
                changeGoalStatusApi()
            }
        }

        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun changeGoalStatusApi() {
        val request = MentalGoalsRequest()
        val goalId = MentalGoalsUtil.goalId

        request.status = MentalGoalsUtil.statusChange
        request.statusNote = et_note?.text.toString()

        MdocManager.updateMentalGoals(goalId, request, object: Callback<SingleMentalGoalResponse> {
            override fun onResponse(call: Call<SingleMentalGoalResponse>,
                                    response: Response<SingleMentalGoalResponse>) {
                if (response.isSuccessful) {
                    MentalGoalsUtil.clearGoalStatus()
                    val action =
                            NoteGoalStatusFragmentDirections.actionNoteGoalStatusFragmentToNotificationGoalFragment()
                    findNavController().navigate(action)
                }
                else {
                    MdocUtil.showToastLong(activity, response.message())
                }
            }

            override fun onFailure(call: Call<SingleMentalGoalResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
            }
        })
    }

    fun isEmpty(): Boolean {
        return et_note?.text.isNullOrBlank()
    }
}

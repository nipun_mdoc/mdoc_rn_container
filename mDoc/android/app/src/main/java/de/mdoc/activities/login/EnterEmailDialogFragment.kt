package de.mdoc.activities.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import de.mdoc.R
import de.mdoc.activities.LoginActivity
import de.mdoc.network.response.LoginResponse
import de.mdoc.newlogin.Interactor.LogiActivty
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.layout_enter_email.*
import retrofit2.Response

class EnterEmailDialogFragment(private val dialogCallback: SaveMailDialogFragment.DialogSaveCallback,
                             private val resp: Response<LoginResponse>): DialogFragment() {

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_enter_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)

        skipBtn?.setOnClickListener {
            dialog?.dismiss()
            try {
                SaveMailDialogFragment(dialogCallback = dialogCallback, resp = resp).showNow(
                        (context as LoginActivity).supportFragmentManager, "")
            }catch (e: Exception) {
                SaveMailDialogFragment(dialogCallback = dialogCallback, resp = resp).showNow(
                        (context as LogiActivty).supportFragmentManager, "")
            }
        }

        saveBtn?.setOnClickListener {
            val email = emailEdt.text.toString()
            if (email.isEmpty()) {
                emailEdt.error = context?.getString(R.string.required_field)
            }
            else if (!MdocUtil.isValidEmailFormat(email)) {
                emailEdt.error = context?.getString(R.string.valid_email)
            }
            else {
                disableButtons()
                dialogCallback.saveMail(email, resp, dialog)
            }
        }
    }

    fun setErrorMessageForEmailInUse(){
        emailEdt.error = context?.getString(R.string.email_in_use)
    }

    fun enableButtons(){
        skipBtn?.isEnabled = true
        saveBtn?.isEnabled = true
    }

    fun disableButtons(){
        skipBtn?.isEnabled = false
        saveBtn?.isEnabled = false
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)

        super.onResume()
    }
}

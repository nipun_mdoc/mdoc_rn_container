package de.mdoc.modules.messages.adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.R
import de.mdoc.modules.messages.ConversationsViewModel
import de.mdoc.modules.messages.conversations.ActiveConversationsFragment
import de.mdoc.modules.messages.conversations.ClosedConversationsFragment

class MessagesPagerAdapter(fm: FragmentManager,
                           val conversationsViewModel: ConversationsViewModel,
                           val context: Context?): FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0    -> ActiveConversationsFragment().also { it.conversationsViewModel = conversationsViewModel }

            else -> {
                return ClosedConversationsFragment().also { it.conversationsViewModel = conversationsViewModel }
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence {
        return when (position) {
            0    -> context?.resources?.getString(R.string.messages_active_inbox).toString()

            else -> {
                return context?.resources?.getString(R.string.messages_closed_conversations)
                    .toString()
            }
        }
    }
}
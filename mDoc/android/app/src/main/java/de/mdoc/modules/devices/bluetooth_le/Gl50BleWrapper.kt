package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import de.mdoc.interfaces.BleResponseListener
import de.mdoc.storage.AppPersistence
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class Gl50BleWrapper(context: Context, deviceAddress: String): BluetoothGattCallback() {

    private var bluetoothDevice: BluetoothDevice? = null
    private var acquiredMeasurementResponseArrayList: ArrayList<Byte> = ArrayList()
    private var bleListener: BleResponseListener? = null
    private var counter = 0
    private val measurementMap: HashMap<Int, ArrayList<Byte>> = hashMapOf()
    private val markerMap: HashMap<Int, ArrayList<Byte>> = hashMapOf()

    fun setBleResponseListener(bleListener: BleResponseListener) {
        this.bleListener = bleListener
    }

    init {
        Timber.d(": initCalled")
        if (bluetoothDevice == null) {
            bluetoothDevice = getBluetoothDevice(context, deviceAddress)
        }
        bluetoothDevice!!.connectGatt(context, false, this)
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {
        val bluetoothManager = context
            .getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter
        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
        super.onConnectionStateChange(gatt, status, newState)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            when (newState) {
                BluetoothGatt.STATE_CONNECTED    -> {
                    Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                    gatt.discoverServices()
                }

                BluetoothGatt.STATE_DISCONNECTED -> {
                    gatt.close()
                    bleListener!!.dataFromDeviceCallback(acquiredMeasurementResponseArrayList, gatt)
                }
            }
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt,
                                      status: Int) {
        super.onServicesDiscovered(gatt, status)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            Timber.d("onServicesDiscovered: $gatt")

            writeCommand(gatt,
                    CUSTOM_SERVICE,
                    CUSTOM_CHARACTERISTIC_1,
                    GET_FIRMWARE_VERSION)
        }
    }

    override fun onDescriptorWrite(gatt: BluetoothGatt,
                                   descriptor: BluetoothGattDescriptor,
                                   status: Int) {
        super.onDescriptorWrite(gatt, descriptor, status)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            when (counter) {
                0 -> {
                    val glucoseMeasurementContextCharacteristic: BluetoothGattCharacteristic =
                            gatt.getService(UUID.fromString(GLUCOSE_SERVICE))
                                .getCharacteristic(UUID.fromString(GLUCOSE_MEASUREMENT_CONTEXT_CHARACTERISTIC))
                    enableNotifications(glucoseMeasurementContextCharacteristic, gatt)
                }

                1 -> {
                    val recordAccessControlPointCharacteristic: BluetoothGattCharacteristic =
                            gatt.getService(UUID.fromString(GLUCOSE_SERVICE))
                                .getCharacteristic(UUID.fromString(RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC))
                    enableIndications(recordAccessControlPointCharacteristic, gatt)
                }

                2 -> {
                    val recordAccessControlPointCharacteristic: BluetoothGattCharacteristic =
                            gatt.getService(UUID.fromString(GLUCOSE_SERVICE))
                                .getCharacteristic(UUID.fromString(RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC))
                    enableIndications(recordAccessControlPointCharacteristic, gatt)
                }

                3 -> {
                    writeCommand(gatt,
                            GLUCOSE_SERVICE,
                            RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC,
                            GET_ALL_DATA_COMMAND)
                }
            }
            counter++
        }
        else {
            Timber.d("onDescriptorWrite: FAILED $gatt")
        }
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt,
                                         characteristic: BluetoothGattCharacteristic) {
        super.onCharacteristicChanged(gatt, characteristic)
        Timber.d("onCharacteristicCHANGED: %s", Arrays.toString(characteristic.value))
        val tempArray: ArrayList<Byte> = ArrayList()

        characteristic.value.forEach {
            tempArray.add(it)
        }

        if (characteristic.value.size == 13) {
            measurementMap[characteristic.value[1].toInt()] = tempArray
        }
        else if (characteristic.value.size == 4 && characteristic.value[0].toInt() == 6) {
            measurementMap.forEach {
                acquiredMeasurementResponseArrayList.addAll(it.value)

                if (markerMap.containsKey(it.key)) {
                    acquiredMeasurementResponseArrayList.addAll(markerMap.getValue(it.key))
                }
                else {
                    acquiredMeasurementResponseArrayList.addAll(
                            arrayListOf(2.toByte(), 0.toByte(), 0.toByte(), 0.toByte()))
                }
            }

            Timber.d("onCharacteristicLAST: %s", Arrays.toString(characteristic.value))
            writeCommand(gatt,
                    CUSTOM_SERVICE,
                    CUSTOM_CHARACTERISTIC_1,
                    TURN_OFF)


            gatt.disconnect()
        }
        else if (characteristic.value.size == 4) {
            markerMap[characteristic.value[1].toInt()] = tempArray
        }
    }

    override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                       characteristic: BluetoothGattCharacteristic?,
                                       status: Int) {
        super.onCharacteristicWrite(gatt, characteristic, status)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            if (characteristic?.uuid.toString() == CUSTOM_CHARACTERISTIC_1) {
                Thread.sleep(2000)
                val readChar: BluetoothGattCharacteristic? = gatt?.getService(UUID.fromString(CUSTOM_SERVICE))
                    ?.getCharacteristic((UUID.fromString(CUSTOM_CHARACTERISTIC_2)))
                gatt?.readCharacteristic(readChar)
            }

            Timber.d("onCharacteristicWrite: %s", Arrays.toString(characteristic?.value))
        }
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                      characteristic: BluetoothGattCharacteristic?,
                                      status: Int) {
        super.onCharacteristicRead(gatt, characteristic, status)
        if (characteristic?.uuid.toString() == CUSTOM_CHARACTERISTIC_2) {
            val value = characteristic?.value ?: byteArrayOf()
            if (value.size > 3) {
                val firmwareVersion = String(value)

                Timber.i("onCharacteristicRead firmwareVersion: ${firmwareVersion[3]}")
                AppPersistence.latestConnectedGL50Device=firmwareVersion.substring(3,4).toInt()
                val glucoseMeasurementCharacteristic: BluetoothGattCharacteristic? =
                        gatt?.getService(UUID.fromString(GLUCOSE_SERVICE))
                            ?.getCharacteristic(UUID.fromString(GLUCOSE_MEASUREMENT_CHARACTERISTIC))
                enableNotifications(glucoseMeasurementCharacteristic, gatt)
            }
            else {
                writeCommand(gatt,
                        CUSTOM_SERVICE,
                        CUSTOM_CHARACTERISTIC_1,
                        GET_FIRMWARE_VERSION)
            }
        }
        else {
            Timber.i("onCharacteristicRead $characteristic")
            val value = characteristic?.value ?: byteArrayOf()
            val v = String(value)
            Timber.i("onCharacteristicRead Value: $v")
        }
    }

    private fun enableNotifications(characteristic: BluetoothGattCharacteristic?, gatt: BluetoothGatt?) {
        characteristic?.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

        gatt?.setCharacteristicNotification(characteristic, true)
        val descriptor = characteristic?.getDescriptor(
                UUID.fromString(NOTIFICATION_CHARACTERISTIC))
        descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        gatt?.writeDescriptor(descriptor)
    }

    private fun enableIndications(characteristic: BluetoothGattCharacteristic, gatt: BluetoothGatt) {
        characteristic.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        gatt.setCharacteristicNotification(characteristic, true)
        val descriptor = characteristic.getDescriptor(
                UUID.fromString(NOTIFICATION_CHARACTERISTIC))
        descriptor.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        gatt.writeDescriptor(descriptor)
    }

    private fun writeCommand(gatt: BluetoothGatt?, service: String, btCharacteristic: String, command: ByteArray) {
        val characteristic: BluetoothGattCharacteristic? =
                gatt?.getService(UUID.fromString(service))
                    ?.getCharacteristic(UUID.fromString(btCharacteristic))
        characteristic?.value = command
        gatt?.writeCharacteristic(characteristic)
    }

    companion object {

        private const val GLUCOSE_SERVICE = "00001808-0000-1000-8000-00805f9b34fb"
        private const val GLUCOSE_MEASUREMENT_CHARACTERISTIC = "00002A18-0000-1000-8000-00805f9b34fb"
        private const val GLUCOSE_MEASUREMENT_CONTEXT_CHARACTERISTIC = "00002A34-0000-1000-8000-00805f9b34fb"
        private const val RECORD_ACCESS_CONTROL_POINT_CHARACTERISTIC = "00002A52-0000-1000-8000-00805f9b34fb"
        private const val NOTIFICATION_CHARACTERISTIC = "00002902-0000-1000-8000-00805f9b34fb"
        private val GET_ALL_DATA_COMMAND = byteArrayOf(0x01, 0x01)
        private const val CUSTOM_SERVICE = "0000fff0-0000-1000-8000-00805f9b34fb"
        private const val CUSTOM_CHARACTERISTIC_1 = "0000fff6-0000-1000-8000-00805f9b34fb"
        private val GET_FIRMWARE_VERSION = byteArrayOf(0x01, 0, 0, 0, 0, 0, 0, 0)
        private const val CUSTOM_CHARACTERISTIC_2 = "0000fff7-0000-1000-8000-00805f9b34fb"
        private val TURN_OFF = byteArrayOf(0x04, 0, 0, 0, 0, 0, 0, 0)
    }
}

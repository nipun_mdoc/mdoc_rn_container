package de.mdoc.modules.medications.create_plan.data

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.modules.medications.data.Medication
import de.mdoc.modules.medications.search.data.MedicationData
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import kotlin.collections.ArrayList

class CreatePlanViewModel(private val mDocService: IMdocService, val medication: MedicationData):
        ViewModel() {

    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")
    private val hourFormat = DateTimeFormat.forPattern("HH:mm")
    var isPlanCreated: MutableLiveData<Boolean> = MutableLiveData(false)
    var unitsOfMeasurement: MutableLiveData<ArrayList<CodingItem>> =
            MutableLiveData(
                    arrayListOf())

    init {
        codingRequest()
    }

    private fun codingRequest() {
        viewModelScope.launch {
            try {
                networkCallAsync()
            } catch (e: java.lang.Exception) {
                Timber.d("exception %s", e.toString())
            }
        }
    }

    fun createPlanRequest() {
        val medicationPlanRequest = MedicationPlanRequest()

        medicationPlanRequest.dispenseRequest = if (isInfinite.value == false) {
            val startMs = dispenseRequest.value?.validityPeriod?.start as DateTime
            val endMs = dispenseRequest.value?.validityPeriod?.end as DateTime
            val start = formatter.print(startMs.millis)
            val end = formatter.print(endMs.millis)

            DispenseRequest(isInfinite.value,
                    ValidityPeriod(end as String, start as String))
        }
        else {
            DispenseRequest(isInfinite.value, null)
        }
        val dayOfWeekList: ArrayList<String> = arrayListOf()

        weeklyPeriod.value?.forEach {
            if (it.selected) {
                val str = it.code ?: ""

                dayOfWeekList.add(str)
            }
        }
        val timing = Timing(
                repeat = Repeat(
                        dayOfWeek = dayOfWeekList,
                        period = repeat.value?.period,
                        periodUnit = periodUnit.value,
                        timeOfDay = getTimeOfDays()
                               ))

        medicationPlanRequest.dosageInstruction = listOf(DosageInstruction(
                doseQuantity = dosageQuantity.value?.plus(dosageQuantityPartial.value!!),
                timing = timing,
                text = prescriptionReason.value,
                patientInstruction = intakeInformation.value,
                doseQuantityCode = doseQuantityCode.value
                                                                          ))
        medicationPlanRequest.authoredOn = formatter.print(DateTime.now().millis)
        medicationPlanRequest.medicationId = medication.code
        medicationPlanRequest.showReminder = isShowReminder.value
        medicationPlanRequest.requester = clientId
        medicationPlanRequest.subject = clientId

        viewModelScope.launch {
            try {
                createPlan(medicationPlanRequest)
                isPlanCreated.value = true
            } catch (e: java.lang.Exception) {
                Timber.d("exception %s", e.toString())
                isPlanCreated.value = false
            }
        }
    }


    fun createPlanRequestOnDemand(){
        val medicationPlanRequest = MedicationPlanRequest()

        medicationPlanRequest.dispenseRequest = if (isInfinite.value == false) {
            val startMs = dispenseRequest.value?.validityPeriod?.start as DateTime
            val endMs = dispenseRequest.value?.validityPeriod?.end as DateTime
            val start = formatter.print(startMs.millis)
            val end = formatter.print(endMs.millis)

            DispenseRequest(isInfinite.value, ValidityPeriod(end as String, start as String))
        } else {
            DispenseRequest(isInfinite.value, null)
        }

        val dayOfWeekList: ArrayList<String> = arrayListOf()

        weeklyPeriod.value?.forEach {
            if (it.selected) {
                val str = it.code ?: ""
                dayOfWeekList.add(str)
            }
        }
        val timing = Timing(event = listOf(formatter.print(DateTime.now().millis)))

        medicationPlanRequest.dosageInstruction = listOf(DosageInstruction(
                timing = timing,
                text = prescriptionReason.value,
                patientInstruction = intakeInformation.value
        ))

        medicationPlanRequest.authoredOn = formatter.print(DateTime.now().millis)
        medicationPlanRequest.medicationId = medication.code
        medicationPlanRequest.requester = clientId
        medicationPlanRequest.showReminder = false
        medicationPlanRequest.subject = clientId

        viewModelScope.launch {
            try {
                createPlan(medicationPlanRequest)
                isPlanCreated.value = true
            } catch (e: java.lang.Exception) {
                Timber.d("exception %s", e.toString())
                isPlanCreated.value = false
            }
        }
    }

    private suspend fun createPlan(medicationPlan: MedicationPlanRequest) {
        mDocService.postMedicationPlan(medicationPlan)
    }

    private suspend fun networkCallAsync() {
        mDocService.getCodingLight(unitCoding)
            .data?.list?.forEach {
            unitsOfMeasurement.value?.add(CodingItem(
                    display = it?.display,
                    code = it?.code
                                                    ))
        }

        if (medication.manualEntry) {
            medication.code = mDocService.postMedication(
                    Medication(
                            description = medication.description)).data?.id ?: ""
        }
    }

    //IndicatePeriodFragment
    var clientId: String = MdocAppHelper.getInstance()
        .userId
    var medicationId: MutableLiveData<String> = MutableLiveData("")

    val dispenseRequest: MutableLiveData<DispenseRequest> = MutableLiveData(DispenseRequest())
    var isInfinite: MutableLiveData<Boolean> = MutableLiveData(true)

    //HowOftenFragment
    val periodUnit: MutableLiveData<String> = MutableLiveData("D")
    val selectedPeriod: MutableLiveData<ListItemTypes?> = MutableLiveData(ListItemTypes.ON_DEMAND)
    val howOften: MutableLiveData<ArrayList<ListItems>> =
            MutableLiveData(
                    arrayListOf(ListItems(text = R.string.med_plan_daily_selection, type = ListItemTypes.DAILY),
                            ListItems(text = R.string.med_plan_weekly_selection, type = ListItemTypes.WEEKLY),
                            ListItems(text = R.string.med_plan_every_certain_period, type = ListItemTypes.EVERY_CERTAIN_PERIOD)))

    fun isHowOftenSelected(): Boolean {
        howOften.value?.forEach {
            if (it.selected) {
                return true
            }
        }
        return false
    }

    fun howOftenSelected(item: ListItems?) {
        howOften.value?.forEach {
            if (item?.text == it.text) {
                it.selected = true
                it.color = R.color.colorSecondary32
            }
        }
    }

    //WeeklyIntakeFragment
    val weeklyPeriod: MutableLiveData<ArrayList<ListItems>> = MutableLiveData(arrayListOf())

    fun weekSelected(item: ListItems?) {
        weeklyPeriod.value?.forEach {
            if (item?.text == it.text) {
                if (it.selected) {
                    it.selected = false
                    it.color = R.color.colorSecondary8
                }
                else {
                    it.selected = true
                    it.color = R.color.colorSecondary32
                }
            }
        }
    }

    fun isWeeklyIntakeSelected(): Boolean {
        weeklyPeriod.value?.forEach {
            if (it.selected) {
                return true
            }
        }
        return false
    }

    fun isWhatTimeSelected(): Boolean {
        if (additionalDailyPeriod.value?.isNotEmpty() == true) {
            return true
        }
        AppPersistence.timeCoding?.forEach {
            if (it.selected) {
                return true
            }
        }
        return false
    }

    fun hourEdited(item: ListItems?, time: LocalDateTime) {
        AppPersistence.timeCoding?.forEach {
            if (item?.text == it.text) {
                val hour = String.format("%02d", time.hourOfDay)
                val minute = String.format("%02d", time.minuteOfHour)

                it.code = "$hour:$minute"
            }
        }
        additionalDailyPeriod.value?.forEach {
            if (item?.text == it.text) {
                val hour = String.format("%02d", time.hourOfDay)
                val minute = String.format("%02d", time.minuteOfHour)

                it.code = "$hour:$minute"
            }
        }
    }

    //EveryCertainPeriodFragment
    var repeat: MutableLiveData<Repeat> = MutableLiveData(Repeat())

    fun daySelected(item: ListItems?) {
        AppPersistence.timeCoding?.forEach {
            if (item?.text == it.text) {
                if (it.selected) {
                    it.selected = false
                    it.color = R.color.colorSecondary8
                }
                else {
                    it.selected = true
                    it.color = R.color.colorSecondary32
                }
            }
        }
    }

    //WhatTimeFragment
    var additionalDailyPeriod: MutableLiveData<ArrayList<ListItems>> =
            MutableLiveData(
                    arrayListOf())
    val isShowReminder: MutableLiveData<Boolean> = MutableLiveData(true)
    //DosageFragment
    var form: MutableLiveData<String> = MutableLiveData("")
    val medicationName: MutableLiveData<String> = MutableLiveData("")
    val doseQuantityCode: MutableLiveData<String> = MutableLiveData("")
    val dosageQuantity: MutableLiveData<Double> = MutableLiveData(1.0)
    val dosageQuantityPartial: MutableLiveData<Double> = MutableLiveData(0.0)
    //IntakeInformationFragment
    val intakeInformation: MutableLiveData<String> = MutableLiveData("")
    val prescriptionReason: MutableLiveData<String> = MutableLiveData("")

    private fun getTimeOfDays(): ArrayList<TimeOfDay> {
        val timeList =
                arrayListOf<TimeOfDay>()


        AppPersistence.timeCoding?.forEach {
            if (it.selected) {
                val localTime = hourFormat.parseLocalTime(it.code)

                timeList.add(TimeOfDay(
                        localTime.hourOfDay, localTime.minuteOfHour))
            }
        }
        additionalDailyPeriod.value?.forEach {
            val localTime = hourFormat.parseLocalTime(it.code)

            timeList.add(TimeOfDay(
                    localTime.hourOfDay, localTime.minuteOfHour))
        }
        return timeList
    }

    fun deleteFutureSelection() {
        weeklyPeriod.value?.forEach {
            it.selected = false
            it.color = R.color.colorSecondary8
        }

        AppPersistence.timeCoding?.forEach {
            it.selected = false
            it.color = R.color.colorSecondary8
        }

        repeat.value = Repeat(period = 1)
        additionalDailyPeriod.value = arrayListOf()
        isShowReminder.value = true
        dosageQuantity.value = 1.0
        dosageQuantityPartial.value = 0.0
        doseQuantityCode.value = ""
        intakeInformation.value = ""
        prescriptionReason.value = ""
    }

    companion object {
        const val unitCoding = "http://unitsofmeasure.org/medication-intake"
    }
}


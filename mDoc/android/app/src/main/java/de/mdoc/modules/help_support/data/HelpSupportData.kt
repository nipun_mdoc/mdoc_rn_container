package de.mdoc.modules.help_support.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HelpSupportData(var name: String = "",
                           var app: String = "",
                           var branch: String = "",
                           var api: String = "",
                           var iam: String = ""): Parcelable
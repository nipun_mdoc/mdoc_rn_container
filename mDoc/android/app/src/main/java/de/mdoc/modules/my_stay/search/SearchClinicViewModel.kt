package de.mdoc.modules.my_stay.search

import androidx.lifecycle.ViewModel
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.pojo.ClinicDetails
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class SearchClinicViewModel(
        private val repository: RxStayRepository
) :ViewModel()
{
    val isLoading= liveData(false)

    val isShowLoadingError = liveData(false)

    val isShowContent= liveData(false)

    val isNoContent= liveData(false)

    val searchResults= liveData<List<ClinicDetails>>()



    init {
        isLoading.set(true)
    }


    fun requestSearch(query:String?){
        isLoading.set(true)

        isShowLoadingError.set(false)
        isShowContent.set(false)
        isNoContent.set(false)

        repository.searchMyStay(
                request = query,
                resultListener = ::onDataLoaded,
                errorListener = ::onError)
        }

    private fun onDataLoaded(result: List<ClinicDetails>) {
        isLoading.set(false)
        if (result.isNotEmpty()) {
            searchResults.set(result)
            isShowContent.set(true)
        } else {
            isNoContent.set(true)
            isShowContent.set(false)
        }
    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        isLoading.set(false)
        isShowLoadingError.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }
}
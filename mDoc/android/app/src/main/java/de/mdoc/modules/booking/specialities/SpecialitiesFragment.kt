package de.mdoc.modules.booking.specialities

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentSpecialitiesBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.network.response.SpecialtyServices
import de.mdoc.network.response.SpeciltyData
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_specialities.*
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class SpecialitiesFragment: NewBaseFragment() {

    lateinit var activity: MdocActivity
    private var specialtyAdapter: SpecialtyAdapter? = null
    private var specialityDataLocal = ArrayList<SpeciltyData>()
    private val specialtyServicesAll = ArrayList<SpecialtyServices>()
    private var specialtyServicesPart = ArrayList<SpecialtyServices>()

    private val args: SpecialitiesFragmentArgs by navArgs()

    private val specialitiesViewModel by viewModel {
        SpecialitiesViewModel(
                RestClient.getService(), args.booking.specialtyProvider)
    }
    override val navigationItem: NavigationItem = NavigationItem.BookAppointment

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity

        val binding: FragmentSpecialitiesBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_specialities, container, false)
        binding.specialitiesViewModel = specialitiesViewModel
        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        specialitiesViewModel.content.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                val specialtyCodes = ArrayList<String>()
                specialtyServicesAll.clear()
                val specialtyList = ArrayList<String>()
                specialtyList.add(resources.getString(R.string.all))

                for (item in it) {
                    if(!context?.resources?.getBoolean(R.bool.SHOW_ALL_SPECIALITIES)!!){
                        for (spec in args.booking.specialities!!) {
                            if (spec == item.code) {
                                specialtyCodes.add(item.code)
                                specialtyList.add(item.specialty)
                                specialityDataLocal.add(item)
                                specialtyServicesAll.addAll(item.specialtyServices)
                            }
                        }
                    }else{
                        specialtyList.add(item.specialty)
                        specialityDataLocal.add(item)
                        specialtyServicesAll.addAll(item.specialtyServices)
                    }

                }
                setupSpecialtyDropdown(specialtyList, specialtyCodes)
            }
        })

        RxTextView.textChanges(searchSpecialtyEt)
            .debounce(1000, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                specialtyAdapter?.filter?.filter(it.toString())
            }
    }

    private fun setupSpecialtyDropdown(data: List<String>, codes: ArrayList<String>) {
        val adapter = ArrayAdapter(activity, android.R.layout.simple_spinner_item, data)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        specialityDropDown?.adapter = adapter
        specialityDropDown?.isVerticalScrollBarEnabled = true
        specialityDropDown?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    specialtyAdapter = SpecialtyAdapter(specialtyServicesAll, "", codes, args.booking)
                }
                else {
                    specialtyServicesPart = specialityDataLocal[position - 1].specialtyServices
                    if(!context?.resources?.getBoolean(R.bool.SHOW_ALL_SPECIALITIES)!!)
                        args.booking.specialtyCode = codes[position - 1]
                    specialtyAdapter = SpecialtyAdapter(specialtyServicesPart, specialityDataLocal[position - 1].specialty, null, args.booking)
                }
                bookAppointmentsRv?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                bookAppointmentsRv?.adapter = specialtyAdapter
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }
}

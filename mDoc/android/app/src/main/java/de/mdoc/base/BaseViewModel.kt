package de.mdoc.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import java.lang.Exception

abstract class BaseViewModel : ViewModel(){
    var isLoading = MutableLiveData(true)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(false)

    abstract suspend fun networkCallAsync()

    init {
        viewModelScope.launch {
            try {
                networkCallAsync()
            } catch (e: Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowNoDataMessage.value = true
            }
        }
    }
}
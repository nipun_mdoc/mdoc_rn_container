package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by AdisMulabdic on 2/12/18.
 */

public class AppointmentData implements Serializable {
    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("list")
    @Expose
    private ArrayList<ListData> list = null;

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public List<ListData> getList() {
        return list;
    }

    public void setList(ArrayList<ListData> list) {
        this.list = list;
    }
}


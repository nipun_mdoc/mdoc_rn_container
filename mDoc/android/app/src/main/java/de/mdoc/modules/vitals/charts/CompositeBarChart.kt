package de.mdoc.modules.vitals.charts

import android.app.Activity
import android.content.Context
import android.view.View
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import de.mdoc.R
import de.mdoc.modules.devices.data.ObservationPeriodType
import de.mdoc.modules.vitals.FhirConfigurationUtil.componentMetricLabel
import de.mdoc.util.ChartFormatter.DayFormatter
import de.mdoc.util.ChartFormatter.MonthFormatter
import de.mdoc.util.ChartFormatter.WeekFormatter
import de.mdoc.util.ChartFormatter.YearFormatter
import java.util.*

fun drawCompositeBarChart(context: Context,
                          periodType: ObservationPeriodType,
                          inputData: ArrayList<ArrayList<BarEntry>>,
                          metricCodes:List<String>,
                          lastDayOfMonth:Int) {
    val activity=context as Activity
    val graphColors = context.resources.getIntArray(R.array.graph)

    var maxValueInCollection=0F

    val barChart = activity.findViewById<BarChart>(R.id.bar_chart)
    barChart.setNoDataText("")

    if(barChart!=null) {
        barChart.visibility = View.VISIBLE
        barChart.setScaleEnabled(false)

        val barDataSet = arrayListOf<IBarDataSet>()

        for ((index, observationList) in inputData.withIndex()) {
            val graphColor = graphColors[index]
            val dataSet = BarDataSet(
                    observationList,
                    componentMetricLabel(metricCodes.getOrNull(index) ?: "")
            )
            dataSet.setDrawValues(false)
            dataSet.color = graphColor
            barDataSet.add(dataSet)
            maxValueInCollection = if (observationList.maxBy { it.y }?.y ?: 200F >= maxValueInCollection) {
                observationList.maxBy { it.y }?.y ?: 200F
            } else {
                maxValueInCollection
            }
        }
        val yAxisLimitTop=maxValueInCollection+(maxValueInCollection*(10F/100F))

        barChart.axisRight.isEnabled = false
        barChart.description.isEnabled = false
        barChart.axisLeft.axisMinimum = 0F

        //x axis
        barChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        barChart.xAxis.setDrawAxisLine(true)
        barChart.xAxis.setDrawGridLines(false)

        when (periodType) {
            ObservationPeriodType.DAY -> {
                val myValueFormatter = DayFormatter()
                barChart.xAxis.setLabelCount(5, true)
                barChart.xAxis.axisMinimum = 0F
                barChart.xAxis.axisMaximum = 24F
                barChart.xAxis.valueFormatter = myValueFormatter

                barChart.axisLeft.setLabelCount(5, true)
                barChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.WEEK -> {
                val weekFormatter = WeekFormatter(context)

                barChart.xAxis.setLabelCount(7, true)
                barChart.xAxis.axisMinimum = 0F
                barChart.xAxis.axisMaximum = 6F
                barChart.xAxis.valueFormatter = weekFormatter

                barChart.axisLeft.setLabelCount(5, true)
                barChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.MONTH -> {
                val monthFormatter = MonthFormatter()
                barChart.xAxis.setLabelCount(7, true)
                barChart.xAxis.axisMinimum = 01F
                barChart.xAxis.axisMaximum = lastDayOfMonth.toFloat()
                barChart.xAxis.valueFormatter = monthFormatter

                barChart.axisLeft.setLabelCount(5, true)
                barChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.YEAR -> {
                val yearFormatter = YearFormatter()
                barChart.xAxis.setLabelCount(12, true)
                barChart.xAxis.axisMinimum = 0F
                barChart.xAxis.axisMaximum = 11F
                barChart.xAxis.valueFormatter = yearFormatter

                barChart.axisLeft.setLabelCount(5, true)
                barChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            else -> {
            }
        }

        val barData = BarData(barDataSet)

        if (inputData.isEmpty()) {
            inputData.add(arrayListOf(BarEntry(0.toFloat(), 0.toFloat())))
        }
        val groupSpace = 0.1f
        val barSpace = 0.01f
        val barWidth = 0.45f
        barChart.data = barData
        barData.barWidth = barWidth

        barChart.groupBars(0f, groupSpace, barSpace)

        barChart.invalidate()

        if (inputData.isNullOrEmpty()) {
            inputData.clear()
            barChart.invalidate()
            barChart.clear()
        }
    }
}
package de.mdoc.modules.messages.messages

import android.Manifest
import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.EXTERNAL_WRITE_PERMISSION_GRANT
import de.mdoc.databinding.FragmentMessagesBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.interfaces.IDownloadId
import de.mdoc.network.RestClient
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_messages.*

class MessagesFragment: NewBaseFragment(), IDownloadId, MessagesListAdapter.OnItemClickListener {
    override val navigationItem: NavigationItem = NavigationItem.Messages
    var downloadID: Long? = null
    private val messagesViewModel by viewModel {
        MessagesViewModel(
                RestClient.getService()
                         )
    }
    val args: MessagesFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMessagesBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_messages, container, false)

        binding.lifecycleOwner = this
        binding.messagesViewModel = messagesViewModel
        binding.isPublic = args.additionalFields.isPublic
        binding.replyAllowed = args.additionalFields.replyAllowed
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (BuildConfig.FLAV == "covid19") {
            setupAdapter()
        }
        else {
            requestStoragePermissions()
        }

        activity?.registerReceiver(
                onDownloadCompleted,
                IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
                                  )
    }

    private fun setupAdapter() {
        messagesViewModel.isClosedConverstions.value = args.additionalFields.isClosedConversation
        messagesViewModel.conversationId.value = args.additionalFields.conversationId
        messagesViewModel.letterboxTitle.value = args.additionalFields.letterboxTitle
        messagesViewModel.reasonTitle.value = args.additionalFields.reasonTitle

        messagesViewModel.getMessages()

        messagesViewModel.messagesList.observe(viewLifecycleOwner, Observer {
            val adapter = context?.let { context ->
                MessagesListAdapter(
                        context,
                        it,
                        args.additionalFields.isClosedConversation,
                        args.additionalFields.letterboxTitle)
            }
            adapter?.setDownloadListener(this)
            adapter?.setOnItemClickListener(this)
            chatMessagesRv.adapter = adapter
            val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            chatMessagesRv.layoutManager = layoutManager
            val listSize = messagesViewModel.messagesList.value?.size ?: 0

            scrollToBottom()
            if (args.additionalFields.isNewMessage && listSize > 0) {
                messagesViewModel.setMessageSeen()
            }
        })

        chatMessagesRv.addOnLayoutChangeListener { _, _, _, _, bottom, _, _, _, oldBottom ->
            if (bottom < oldBottom) scrollToBottom()
        }
    }

    private fun scrollToBottom() {
        val position = messagesViewModel.messagesList.value?.size ?: 0
        Handler().postDelayed({ chatMessagesRv.smoothScrollToPosition(position) }, 50)
    }

    private val onDownloadCompleted = object: BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            //Fetching the download id received with the broadcast
            val id = intent?.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1)
            //Checking if the received broadcast is for our enqueued download by matching download id
            if (downloadID == id && isAdded) {
                MdocUtil.showToastLong(context, getString(R.string.file_dowloaded_succ))
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        context?.let {
            LocalBroadcastManager.getInstance(it)
                .unregisterReceiver(onDownloadCompleted)
        }
    }

    override fun passDownloadId(id: Long?) {
        downloadID = id
    }

    private fun requestStoragePermissions() {
        if (activity?.let {
                ContextCompat.checkSelfPermission(it, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            } != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    EXTERNAL_WRITE_PERMISSION_GRANT
                              )
        }
        else {
            setupAdapter()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == EXTERNAL_WRITE_PERMISSION_GRANT) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                messagesViewModel.isPermissionDenied.value = false
                setupAdapter()
            }
            else {
                messagesViewModel.isPermissionDenied.value = true
                findNavController().popBackStack()
            }
        }
    }

    override fun onItemClicked(view: View?, url: String) {
        val urlEdited = url.replace("#", "?")
        val strArray = urlEdited.split(":").toTypedArray()

        if (strArray[1].trim() == "url") {
            val vidSignerUrl = strArray[2]+ ":"+ strArray[3]
            navigateSignDocument(vidSignerUrl)

        } else if (strArray[1].trim() !== null) {
            val docId : String = strArray[1].trim()
            messagesViewModel.getDigitalSignatureNextActionData(docId, onSuccess = {
                nextActionData ->
                val strArray = nextActionData?.split(":")?.toTypedArray()
                if (strArray?.get(1)?.trim()  == "url") {
                    val vidSignerUrl = strArray[2]+ ":"+ strArray[3]
                    navigateSignDocument(vidSignerUrl)
                } else if (docId !== null) {
                    val bundle = bundleOf("docId" to docId)
                    findNavController().navigate(R.id.documentReviewFragment, bundle)
                }
            }, onError = {})
        }
    }

    private fun navigateSignDocument(url : String) {
        val bundle = bundleOf("url" to url)
        findNavController().navigate(R.id.signDocumentFragment, bundle)
    }

}
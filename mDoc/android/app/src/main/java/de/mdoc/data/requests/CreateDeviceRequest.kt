package de.mdoc.data.requests

import de.mdoc.data.create_bt_device.*

data class CreateDeviceRequest(
        var expirationDate: String?=null,
        var identifier: List<Identifier>?=null,
        var location: Location?=null,
        var manufactureDate: String?=null,
        var owner: Owner?=null,
        var resourceType: String?=null,
        var status: String?=null,
        var type: Type? =null,
        var udi:Udi?=null,
        var model: String? =null
)


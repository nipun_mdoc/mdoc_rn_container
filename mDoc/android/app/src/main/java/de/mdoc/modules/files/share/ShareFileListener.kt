package de.mdoc.modules.files.share

interface ShareFileListener {

    fun onShareFileCompleted()

}
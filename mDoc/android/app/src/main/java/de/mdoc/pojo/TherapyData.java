package de.mdoc.pojo;

import java.util.List;

/**
 * Created by ema on 11/15/16.
 */

public class TherapyData {

    private List<MonthTherapy> therapyPlanMonths;

    public List<MonthTherapy> getTherapyPlanMonths() {
        return therapyPlanMonths;
    }

    public void setTherapyPlanMonths(List<MonthTherapy> therapyPlanMonths) {
        this.therapyPlanMonths = therapyPlanMonths;
    }
}

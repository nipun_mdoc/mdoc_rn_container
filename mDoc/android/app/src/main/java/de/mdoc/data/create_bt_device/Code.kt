package de.mdoc.data.create_bt_device

data class Code(val coding: List<Coding>){

    fun isSystolic() : Boolean{
        for(coding in coding){
            if(coding.isSystolic()){
                return true
            }
        }
        return false
    }

    fun isDiastolic() : Boolean{
        for(coding in coding){
            if(coding.isDiastolic()){
                return true
            }
        }
        return false
    }
}
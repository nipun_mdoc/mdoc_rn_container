package de.mdoc.modules.vitals

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.data.my_stay_data.MyStayWidgetData
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.launch
import timber.log.Timber

class ManualEntryViewModel (private val mDocService: IMdocService): ViewModel() {

    private val _myStayData = liveData<MyStayWidgetData>().apply {
        viewModelScope.launch {
            try {
                set(mDocService.coroutineGetClinicByCase(MdocAppHelper.getInstance().caseId).data)
            }catch (ex: Exception){
                Timber.d(ex)
            }
        }
    }

    val myStayData = _myStayData

}

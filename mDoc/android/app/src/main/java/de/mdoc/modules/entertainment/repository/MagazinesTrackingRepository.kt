package de.mdoc.modules.entertainment.repository

import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.MediaTrackingRequest
import de.mdoc.pojo.entertainment.MediaResponseDto
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class MagazinesTrackingRepository {

    fun trackSessionOpen (media: MediaResponseDto?, onSuccess: () -> Unit){
        var request = MediaTrackingRequest (
                eventType = MediaTrackingRequest.EventType.sESSIONOPEN.value,
                externalMagazineId = media?.externalMagazineId,
                pdfID = media?.pdfId,
                timestamp =  System.currentTimeMillis(),
                publicationID = media?.publicationId)
        trackEvent(request, onSuccess)
    }

    fun trackSessionClosed (media: MediaResponseDto?) {
        var request = MediaTrackingRequest (
                eventType = MediaTrackingRequest.EventType.sESSIONCLOSE.value,
                externalMagazineId = media?.externalMagazineId,
                pdfID = media?.pdfId,
                timestamp =  System.currentTimeMillis(),
                publicationID = media?.publicationId)
        trackEvent(request)
    }

    fun trackPageOpen (media: MediaResponseDto?, page: String) {
        var request = MediaTrackingRequest (MediaTrackingRequest.EventType.oPEN.value, media?.pdfId, media?.externalMagazineId, page, System.currentTimeMillis(), media?.publicationId)
        trackEvent(request)
    }

    fun trackPageClose (media: MediaResponseDto?, pageToClose: String, pageToOpen: Int, onSuccess: (Int) -> Unit) {
        var request = MediaTrackingRequest (MediaTrackingRequest.EventType.cLOSE.value, media?.pdfId , media?.externalMagazineId, pageToClose, System.currentTimeMillis(), media?.publicationId)
        trackEvent(request,pageToOpen, onSuccess)
    }

    private fun trackEvent (mediaTrackingRequest: MediaTrackingRequest) {
        MdocManager.trackEntertainment(mediaTrackingRequest, object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.d(t)
            }
        })
    }

    private fun trackEvent (mediaTrackingRequest: MediaTrackingRequest, onSuccess: () -> Unit) {
        MdocManager.trackEntertainment(mediaTrackingRequest, object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    onSuccess.invoke()
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.d(t)
            }
        })
    }

    private fun trackEvent (mediaTrackingRequest: MediaTrackingRequest, pageToOpen: Int, onSuccess: (Int) -> Unit) {
        MdocManager.trackEntertainment(mediaTrackingRequest, object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    onSuccess.invoke(pageToOpen)
                }
            }
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Timber.d(t)
            }
        })
    }
}
package de.mdoc.modules.devices

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.data.configuration.ConfigurationDetailsMultiValue
import de.mdoc.data.create_bt_device.BtDevice
import de.mdoc.data.create_bt_device.MedicalDeviceAdapterData
import de.mdoc.data.responses.ConfigurationResponse
import de.mdoc.data.responses.DevicesResponse
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.devices.DevicesUtil.medicalDevicesList
import de.mdoc.modules.devices.data.DevicesForUserRequest
import de.mdoc.modules.devices.data.FhirConfigurationResponse
import de.mdoc.network.managers.MdocManager
import de.mdoc.storage.AppPersistence.deviceConfiguration
import de.mdoc.storage.AppPersistence.fhirDevicesData
import de.mdoc.storage.AppPersistence.medicalDeviceMacAddresses
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_devices.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DevicesFragment: MdocFragment() {
    lateinit var activity: MdocActivity

    private val args: DevicesFragmentArgs by navArgs()

    override fun setResourceId(): Int {
        return R.layout.fragment_devices
    }

    override val navigationItem: NavigationItem = NavigationItem.Devices

    override fun init(savedInstanceState: Bundle?) {
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getFhirConfiguration()
    }

    private fun getFhirConfiguration() {
        progressBar?.visibility=View.VISIBLE

        MdocManager.getFhirConfiguration(object: Callback<FhirConfigurationResponse> {
            override fun onResponse(call: Call<FhirConfigurationResponse>,
                                    response: Response<FhirConfigurationResponse>) {
                if (response.isSuccessful) {
                    checkDeviceSettingsApi()
                    fhirDevicesData = response.body()
                        ?.physicalDevices()
                }
                else {
                    progressBar?.visibility=View.GONE
                    checkDeviceSettingsApi()
                }
            }

            override fun onFailure(call: Call<FhirConfigurationResponse>, t: Throwable) {
                progressBar?.visibility=View.GONE
                checkDeviceSettingsApi()
            }
        })
    }

    private fun checkDeviceSettingsApi() {
        progressBar?.visibility=View.VISIBLE
        MdocManager.checkDeviceSettings(object: Callback<ConfigurationResponse> {
            override fun onResponse(call: Call<ConfigurationResponse>,
                                    response: Response<ConfigurationResponse>) {
                if (response.isSuccessful) {
                    val configData = response.body()
                        ?.data?.configurationDetailsMultiValue
                    deviceConfiguration =
                            configData ?: ConfigurationDetailsMultiValue(medicals = false,
                                    vitals = false)
                    getAllDevicesForUser()
                }
                else {
                    MdocUtil.showToastLong(activity,
                            resources.getString(R.string.err_request_failed))
                    progressBar?.visibility=View.GONE
                }
            }

            override fun onFailure(call: Call<ConfigurationResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
                progressBar?.visibility=View.GONE
            }
        })
    }

    private fun getAllDevicesForUser() {
        val request = DevicesForUserRequest()
        request.deviceId = FirebaseInstanceId.getInstance()
            .id

        MdocManager.getDevicesForUser(request, object: Callback<DevicesResponse> {
            override fun onResponse(call: Call<DevicesResponse>,
                                    response: Response<DevicesResponse>) {
                if (response.isSuccessful) {
                    medicalDeviceMacAddresses =
                            DevicesUtil.filterMacAddresses(response.body()?.data)
                    fillConnectedDevicesList(response.body()?.data)
                    if(isAdded){
                        navigateNext()
                    }
                }
                else {
                    progressBar?.visibility=View.GONE
                }
            }

            override fun onFailure(call: Call<DevicesResponse>, t: Throwable) {
                progressBar?.visibility=View.GONE
            }
        })
    }

    private fun fillConnectedDevicesList(data: List<BtDevice>?) {
        medicalDevicesList.clear()

        if (data != null) {
            for (item in data) {
                item.udi?.name?.let {
                    val medDevice =
                            MedicalDeviceAdapterData(item.id, item.udi.name,
                                    item.getObservationTime())
                    medicalDevicesList.add(medDevice)
                }
            }
        }
    }

    private fun navigateNext() {
        if (medicalDeviceMacAddresses.isNullOrEmpty()) {
            val action = DevicesFragmentDirections.actionToFragmentAvailableDevices()
            findNavController().navigate(action)
        }
        else {

            val action = DevicesFragmentDirections.actionToFragmentConnectedDevices(args.isFromMedical)
            findNavController().navigate(action)
        }
    }
}
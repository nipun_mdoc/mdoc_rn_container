package de.mdoc.modules.covid19.history

import android.content.Context
import android.content.res.ColorStateList
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.overrideNullText
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import java.util.*

class TestHistoryHandler {

    fun setTestStatus(healthStatus: HealthStatus?): String {
        var status = healthStatus?.healthStatus ?: ""
        AppPersistence.covidStatusCoding?.forEach {
            if (status == it.code) {
                status = it.display ?: ""
            }
        }
        return status
    }

    fun setTestTime(context: Context, healthStatus: HealthStatus?): String {
        val time = healthStatus?.testDate
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm")

        return if (time != null) {
            context.resources.getString(R.string.covid_test_time, formatter.print(time))
        }
        else {
            ""
        }
    }
    fun setBackgroundTint(context: Context, healthStatus: HealthStatus?): ColorStateList {
        val color = when (healthStatus?.healthStatus ?: "") {
            CovidStatusChangeBottomSheetFragment.POSITIVE -> {
                R.color.covid_positive
            }

            CovidStatusChangeBottomSheetFragment.NEGATIVE -> {
                R.color.covid_negative
            }

            else                                          -> {
                R.color.gray_e6e6e6
            }
        }

        return ColorStateList.valueOf(ContextCompat.getColor(context, color))
    }
    fun getUpdateTime(context: Context, healthStatus: HealthStatus?): String {
        val time = healthStatus?.uts
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy HH:mm")
        val lastName: String
        return if (time != null) {
            lastName = if (healthStatus.isCratedByAdmin()) {
                healthStatus.lastName ?: "admin"
            }
            else {
                MdocAppHelper.getInstance().userLastName ?: "user"
            }
            context.resources.getString(R.string.covid_update_time, formatter.print(time), lastName)
        }
        else {
            ""
        }
    }
}
package de.mdoc.pojo;

/**
 * Created by kodecta-mac on 8/7/17.
 */

public class NotificationListItem {

    private String id;
    private String username;
    private String icon;
    private String title;
    private String description;
    private boolean seen;
    private boolean visible;
    private String action;
    private long createdTime;
    private String referenceType;
    private String applicationEvent;
    public NotificationCallToAction callToAction;

    public NotificationCallToAction getCallToAction() {
        return callToAction;
    }

    public void setCallToAction(NotificationCallToAction callToAction) {
        this.callToAction = callToAction;
    }

    public String getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(String applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public String getReferenceType() {
        return referenceType;
    }

    public void setReferenceType(String referenceType) {
        this.referenceType = referenceType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSeen() {
        return seen;
    }

    public boolean isNotSeen() {
        return !seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public long getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(long createdTime) {
        this.createdTime = createdTime;
    }
}

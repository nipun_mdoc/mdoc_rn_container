package de.mdoc.fragments.exercise;


import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.OnTextChanged;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.adapters.ExerciseAdapterNew;
import de.mdoc.components.CustomSpinner;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.CarePlanDetailsResponses;
import de.mdoc.pojo.ExerciseDataCategories;
import de.mdoc.pojo.ExerciseResponseNew;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExercisesFragment extends MdocFragment {

    MdocActivity activity;
    @BindView(R.id.magazineListRv)
    RecyclerView exerciesRv;
    @BindView(R.id.searchMagazineEdit)
    EditText searchExerciesEdit;
    @BindView(R.id.mediaSpinner)
    CustomSpinner categorySpinner;

    ProgressDialog progressDialog;
    private ArrayList<CarePlanDetailsResponses> exercises = new ArrayList<>();
    ExerciseAdapterNew adapter;
    private ArrayList<ExerciseDataCategories> categories = new ArrayList<>();
    ArrayAdapter spinnerAdapter;

    private int positionOdSpinner = -1;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_magazines;
    }

    @NonNull
    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Exercise;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);

        if(!getResources().getBoolean(R.bool.phone)) {
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 4);
            exerciesRv.setLayoutManager(layoutManager);
            exerciesRv.addItemDecoration(new MdocUtil.GridSpacingItemDecoration(4, MdocUtil.dpToPx(10, activity), true));
            exerciesRv.setItemAnimator(new DefaultItemAnimator());
        } else {
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 1);
            exerciesRv.setLayoutManager(layoutManager);
        }
        adapter = new ExerciseAdapterNew(activity, exercises, this);
        exerciesRv.setAdapter(adapter);

        spinnerAdapter = new ArrayAdapter(activity, android.R.layout.simple_spinner_item, categories);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(spinnerAdapter);
        getCareplan(false);

    }

    private void getCareplan(final boolean isRefresh){
        progressDialog.show();
        MdocManager.getCareplan(new Callback<ExerciseResponseNew>() {
            @Override
            public void onResponse(Call<ExerciseResponseNew> call, Response<ExerciseResponseNew> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        categories.clear();
                        categories.addAll(response.body().getData());
                        categorySpinner.setAdapter(null);
                        categorySpinner.setAdapter(spinnerAdapter);

                        if (isRefresh) {
                            categorySpinner.setSelection(positionOdSpinner);
                        }

                        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                                exercises.clear();
                                exercises.addAll(categories.get(position).getCarePlanDetailsResponses());
                                adapter.notifyDataSetChanged();
                                adapter.setCarePlanId(categories.get(position).getId());
                                positionOdSpinner = position;

                                if (searchExerciesEdit.getText().length() > 0 && categories.size() > 0 && exercises.size() > 0) {
                                    adapter.getFilter().filter(searchExerciesEdit.getText().toString());
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ExerciseResponseNew> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    private Timer timer=new Timer();

    @OnTextChanged(value = {R.id.searchMagazineEdit}, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    public void onTextChanged(final Editable editable) {
        timer.cancel();
            timer = new Timer();
            timer.schedule(
                    new TimerTask() {
                        @Override
                        public void run() {
                            activity.runOnUiThread(new Runnable() {
                                public void run() {

                                if (categories.size() > 0 && exercises.size() > 0) {
                                    adapter.getFilter().filter(editable.toString());
                                }
                                }
                            });
                        }
                    },
                    1000);
    }

    public void refreshData(){
        getCareplan(true);
    }

}




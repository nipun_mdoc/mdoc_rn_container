package de.mdoc.modules.booking.data

data class BookingRequest(var query:String?="",
                          var limit: Int? = null,
                          var participant: String? = null,
                          var from: Long? = null,
                          var to: Long? = null,
                          var clinicIds:List<String>?=null,
                          var isLastSeenUpdate : Boolean = false)
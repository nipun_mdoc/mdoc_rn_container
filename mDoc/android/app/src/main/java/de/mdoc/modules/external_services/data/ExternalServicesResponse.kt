package de.mdoc.modules.external_services.data

data class ExternalServicesResponse(
        val code: String,
        val data: Data? = null,
        val message: String,
        val timestamp: Long
                                   )

data class Data(
        val list: ArrayList<ExternalServiceItem>? = null,
        val moreDataAvailable: Boolean,
        val totalCount: Int
               )

data class ExternalServiceItem(
        val absolutePath: String,
        val basePath: String,
        val cts: Long,
        val description: String,
        val id: String,
        val requestParams: String,
        val title: String,
        val uts: Long,
        val image: String?,
        val partner: Partner? = null
                              )

data class Partner(
        val name: String? = null,
        val logo: String? = null
                  )
package de.mdoc.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnItemClick;
import de.mdoc.BuildConfig;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.adapters.CasesAdapter;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.pojo.Case;
import de.mdoc.util.MdocAppHelper;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class CaseFragment extends MdocBaseFragment {

    @BindView(R.id.casesLv)
    ListView casesLv;

    ArrayList<Case> cases;
    CasesAdapter adapter;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_case;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        cases = (ArrayList<Case>)getArguments().getSerializable(MdocConstants.CASES);
        adapter = new CasesAdapter(cases, getActivity());
        casesLv.setAdapter(adapter);
    }

    @OnItemClick(R.id.casesLv)
    public void onCasesItemClick(int position){
        Case item = (Case)adapter.getItem(position);
        MdocAppHelper.getInstance().setCaseId(item.getCaseId());
        MdocAppHelper.getInstance().setExternalCaseId(item.getExternalCaseId());
        MdocAppHelper.getInstance().setClinicId(item.getClinicId());
        MdocAppHelper.getInstance().setIsPremiumPatient(item.isPremium());
        MdocAppHelper.getInstance().setDepartment(item.getDepartment());

        MdocAppHelper.getInstance().setExternalCaseId(item.getExternalCaseId());
        navigateToDashboard();
    }

    public void navigateToDashboard() {
        Intent intent = new Intent(getActivity(), MdocActivity.class);
        startActivity(intent);
        if(!BuildConfig.FLAV.equals("kiosk")){
            getActivity().finish();
        }
    }
}

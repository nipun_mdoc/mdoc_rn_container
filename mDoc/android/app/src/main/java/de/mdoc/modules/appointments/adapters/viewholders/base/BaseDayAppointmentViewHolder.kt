package de.mdoc.modules.appointments.adapters.viewholders.base

import android.content.Context
import android.graphics.Paint
import android.graphics.Typeface
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.constants.MdocConstants.RESCHEDULING_CONFIRMATION
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.*
import kotlinx.android.synthetic.main.placeholder_appointment.view.*
import org.joda.time.DateTime
import kotlinx.android.synthetic.main.placeholder_appointment.view.infoIv
import kotlinx.android.synthetic.main.placeholder_appointment.view.txtDescription
import kotlinx.android.synthetic.main.placeholder_appointment.view.txtTime
import kotlinx.android.synthetic.main.placeholder_appointment.view.txtUpdateMessage

abstract class BaseDayAppointmentViewHolder(itemView: View) : BaseAppointmentViewHolder(itemView) {
    private val _txtHour: TextView = itemView.txtHour
    private val _txtAppointmentMonth: TextView = itemView.txtAppointmentMonth
    private val _txtAppointmentWeekDay: TextView = itemView.txtWeekDay
    protected val _txtTime: TextView = itemView.txtTime
    protected val _txtDescription: TextView = itemView.txtDescription
    private val _separator: LinearLayout = itemView.mainSeparator
    protected val _dayItemPlaceHolder: ConstraintLayout = itemView.dayItemPlaceHolder
    protected val _txtUpdateMessage: TextView = itemView.txtUpdateMessage
    protected val _infoIv: ImageView = itemView.infoIv
    protected val _txtMetaDataStatus: TextView = itemView.txtMetaDataStatus
    protected val _txtOnlineAppointment:TextView = itemView.txtOnlineAppointment
    protected val  txtRoomNumber:TextView = itemView.txtRoomNumber


    init {
        _txtMetaDataStatus.visibility = View.GONE
    }

    fun prepareUI(item: AppointmentDetails, context: Context, isFromWidget: Boolean) {

        _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left)
        _infoIv.visibility = if (item.comment == null || item.comment.isEmpty()) View.GONE else View.VISIBLE
        _txtOnlineAppointment.visibility = if (item.isOnline) View.VISIBLE else View.GONE
        showRoomNumber(item, context)

        displayMonth(isFromWidget)

        _txtDescription.text = item.title

        displayTimeOrDay(context, isFromWidget)
        isAppointmentUpdated(context)
        isAppointmentConfirmRequired(item, context)
    }

    private fun showRoomNumber(item: AppointmentDetails, context: Context){
        txtRoomNumber.text = item.location
        txtRoomNumber.visibility = if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.WIDGET_TIME_SPAN_DAYS)) View.VISIBLE else View.GONE
    }

    private fun displayMonth (isFromWidget: Boolean) {
        if (isFromWidget) {
            _txtAppointmentMonth.visibility = if (_item.isFirstAppointment || _hasHeader) View.VISIBLE else View.GONE
            _txtAppointmentWeekDay.visibility = View.VISIBLE

            // Month
            if (_item.start.isCurrentYear()) {
                _txtAppointmentMonth.text = _item.start.getMonthName()
            } else {
                val month = _item.start.getMonthName()
                val year = _item.start.getYear()
                _txtAppointmentMonth.text = "$month, $year"
            }

            // Weekday
            _txtAppointmentWeekDay.text = MdocUtil.getTimeFromMs(_item.start, "E").take(2)

        } else {
            _txtAppointmentMonth.visibility = View.GONE
            _txtAppointmentWeekDay.visibility = View.GONE
        }
    }

    private fun displayTimeOrDay(context: Context, isFromWidget: Boolean) {
        val shortFromTime = MdocUtil.getTimeFromMs(_item.start, SHORT_TIME_FORMAT)
        val fromTime = MdocUtil.getTimeFromMs(_item.start, TIME_FORMAT)
        if (isFromWidget) {
            _txtHour.text = MdocUtil.getTimeFromMs(_item.start, "dd")
        } else {
            _txtHour.text = shortFromTime
        }
        if (_item.isShowEndTime && _item.end!=null && _item.end.toString().trim()!="" && _item.end != 0.toLong()) {
            val endTime = MdocUtil.getTimeFromMs(_item.end, TIME_FORMAT)
            val periodValue = "$fromTime - $endTime"
            _txtTime.text = periodValue
        } else {
            _txtTime.text = fromTime
        }
        val dt = DateTime()
        val hour = dt.hourOfDay
        if (shortFromTime.toInt() == hour && _item.isDark) {
            _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left_highlight)
            _txtHour.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
            _txtHour.setTypeface(null, Typeface.BOLD)
        } else {
            _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left)
            _txtHour.setTextColor(ContextCompat.getColor(context, R.color.text_dark_gray))
            _txtHour.setTypeface(null, Typeface.NORMAL)
        }
    }

    private fun isAppointmentUpdated(context: Context) {
        when {
            _item.isCanceled -> {
                _txtUpdateMessage.visibility = View.GONE
                _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left_canceled)
            }

            isAppointmentUpdated() -> {
                _txtUpdateMessage.visibility = View.VISIBLE
                _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left_highlight)
            }
            else -> {
                _txtUpdateMessage.visibility = View.GONE
            }
        }
    }

    private fun isAppointmentConfirmRequired(data: AppointmentDetails, context: Context){
        when (data.state) {
            RESCHEDULING_CONFIRMATION -> {
                if (data.isDark) {
                    setConfirmationRequiredAppointmentView(R.drawable.dark_stripe, context)
                } else {
                    setConfirmationRequiredAppointmentView(R.drawable.white_stripe, context)
                }
            }
        }
    }

    private fun setConfirmationRequiredAppointmentView(image: Int, context: Context) {
        _txtUpdateMessage.visibility = View.VISIBLE
        _txtUpdateMessage.text = context.resources.getString(R.string.appointment_update_text)
        _separator.background = ContextCompat.getDrawable(context, image)
        _txtTime.paintFlags = _txtTime.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }
}
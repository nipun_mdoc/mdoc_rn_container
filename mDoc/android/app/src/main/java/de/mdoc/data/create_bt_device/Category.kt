package de.mdoc.data.create_bt_device

data class Category(
        val coding: List<Coding>
)
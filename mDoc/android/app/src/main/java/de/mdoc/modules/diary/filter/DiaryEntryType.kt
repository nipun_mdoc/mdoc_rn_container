package de.mdoc.modules.diary.filter

data class DiaryEntryType(
    var diaryId: String?,
    var isSelected: Boolean,
    var base64Image: String?,
    var name: String?
)
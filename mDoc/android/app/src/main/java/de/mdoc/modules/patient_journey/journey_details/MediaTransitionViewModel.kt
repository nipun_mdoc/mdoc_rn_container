package de.mdoc.modules.patient_journey.journey_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.patient_journey.data.MediaDetail
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class MediaTransitionViewModel(private val mDocService: IMdocService,val mediaId:String): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    val proceed = MutableLiveData(false)

    var media: MutableLiveData<MediaDetail> =
            MutableLiveData(MediaDetail())

    init {
        getMedia()
    }

    private fun getMedia() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            try {
                isLoading.value = true
                media.value= mDocService.getMediaDetails(mediaId).data
                isShowContent.value=true
                isLoading.value = false
                proceed.value=true
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowLoadingError.value = true
                isShowContent.value=false
            }
        }
    }
}
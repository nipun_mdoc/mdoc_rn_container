package de.mdoc.modules.medications.search

import android.util.Log
import android.view.View
import androidx.navigation.findNavController
import de.mdoc.R
import de.mdoc.modules.medications.create_plan.data.ListItemTypes
import de.mdoc.modules.medications.search.data.MedicationData
import de.mdoc.modules.medications.search.data.MedicineDetail
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.getErrorDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MedicationSearchHandler(var type: ListItemTypes) {

    fun onMedicationDataClick(view: View, item: MedicationData) {
        if(item.code.isEmpty()){
            item.code= view.context.resources.getString(R.string.med_plan_not_available)
        }

        if(item.form.isEmpty()){
            item.form= view.context.resources.getString(R.string.med_plan_no_description_available)
        }
        MdocManager.getDetails(item.code,
                "*image*",
                "*prescriptions*", object : Callback<MedicineDetail> {
            override fun onResponse(
                    call: Call<MedicineDetail>,
                    response: Response<MedicineDetail>
            ) {
                if (response.isSuccessful) {
                    item.cts = response?.body()!!.data?.cts
                    item.uts = response?.body()!!.data?.uts
                    item.prescriptionReasons = response?.body()!!.data?.prescriptionReasons
                    item.manualEntry = response?.body()!!.data?.manualEntry
                    item.image = response?.body()!!.data?.image
                    val direction = SearchMedicationsFragmentDirections.actionToFragmentConfirmMedication(item, type)
                    view.findNavController().navigate(direction)
                } else {
                    Log.e("test %s", response.getErrorDetails())
                }
            }

            override fun onFailure(call: Call<MedicineDetail>, t: Throwable) {
                Log.e("test", t.localizedMessage)

            }
        })


    }



}
package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.pojo.Address
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.newCodingItem
import kotlinx.android.synthetic.main.activity_patient_details.*
import kotlinx.android.synthetic.main.fragment_residence.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.annotation.Nullable

class ResidenceFragment: Fragment() {
    var countryList: ArrayList<CodingItem>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_residence, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PatientDetailsActivity).setNavigationItem(1)

        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.address?.street?.let {
            streetAndNumbEdt.setText(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.address?.houseNumber?.let {
            streetAndNumbEdt.append(" " + it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.address?.houseNumber?.trim()
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.address?.postalCode?.let {
            postCodeEdt.append(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.address?.city?.let { cityEdt.append(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.homePhoneNumber?.let {
            homePhoneEdt.append(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.workPhoneNumber?.let {
            workPhoneEdt.append(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.phone?.let { mobilePhoneEdt.append(it) }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.publicUserDetails?.email?.let { emailEdt.append(it) }


        country_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.address?.countryCode =
                        countryList?.getOrNull(p2)
                            ?.code
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.address?.country =
                        countryList?.getOrNull(p2)
                            ?.display
            }
        }
        //country
        MdocManager.getCoding("https://mdoc.one/hl7/fhir/countryList",
                object: Callback<CodingResponse> {
                    override fun onResponse(call: Call<CodingResponse>,
                                            response: Response<CodingResponse>) {
                        if (response.isSuccessful) {
                            if (isAdded) {
                                countryList = response.body()!!.data?.list
                                countryList!!.add(0, newCodingItem("", ""))
                                var spinnerAdapter: SpinnerAdapter =
                                        SpinnerAdapter(context!!, countryList!!)
                                country_spinner?.adapter = spinnerAdapter
                                val index: Int? =
                                        countryList?.indexOf(newCodingItem(
                                                PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address?.countryCode))
                                if (index != null) {
                                    country_spinner?.setSelection(index)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CodingResponse>, t: Throwable) {
                    }
                })

        previous1Btn.setOnClickListener {
            activity?.onBackPressed()
        }


        continue1Btn.setOnClickListener {
            if (!hasError()) {
                val text = streetAndNumbEdt.text.toString()
                val index = text.lastIndexOf(" ")
                if (index != -1) {
                    val street = text.substring(0, index)
                    val number = text.substring(index, text.length)
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.publicUserDetails?.address?.street = street
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.publicUserDetails?.address?.houseNumber = number
                }
                else {
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.publicUserDetails?.address?.street =
                            streetAndNumbEdt.text.toString()
                }
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.address?.postalCode =
                        postCodeEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.address?.city =
                        cityEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.homePhoneNumber =
                        homePhoneEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.workPhoneNumber =
                        workPhoneEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.phone =
                        mobilePhoneEdt.text.toString()
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.publicUserDetails?.email = emailEdt.text.toString()
                if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.addressList == null || PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.addressList?.size == 0) {
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.extendedUserDetails?.addressList =
                            ArrayList<Address>()
                    PatientDetailsHelper.getInstance()
                        .patientDetailsData?.extendedUserDetails?.addressList?.add(
                            PatientDetailsHelper.getInstance().patientDetailsData?.publicUserDetails?.address)
                }
                else {
                    if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.addressList?.size!! > 0) {
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.city = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.city
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.countryCode = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.countryCode
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.country = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.country
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.street = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.street
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.houseNumber = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.houseNumber
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.state = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.state
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.description = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.description
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.addressList?.getOrNull(0)
                            ?.postalCode = PatientDetailsHelper.getInstance()
                            .patientDetailsData?.publicUserDetails?.address?.postalCode
                    }
                }
                showNewFragment(
                        EmployerFragment(), R.id.container)
            }
            else {
                scrollViewResidence.smoothScrollTo(0, 0)
            }
        }

        streetAndNumbEdt.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                streetRfTv.visibility = View.GONE
                streetLabelTv.setTextColor(resources.getColor(R.color.taupe))
                streetAndNumbEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        postCodeEdt.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                postCodeRfTv.visibility = View.GONE
                postCodeLabelTv.setTextColor(resources.getColor(R.color.taupe))
                postCodeEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        cityEdt.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                cityRfTv.visibility = View.GONE
                cityLabelTv.setTextColor(resources.getColor(R.color.taupe))
                cityEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        mobilePhoneEdt.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                mobileRfTv.visibility = View.GONE
                mobileLabelTv.setTextColor(resources.getColor(R.color.taupe))
                mobilePhoneEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })

        emailEdt.addTextChangedListener(object: TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                emailRfTv.visibility = View.GONE
                emailLabelTv.setTextColor(resources.getColor(R.color.taupe))
                emailEdt.background = resources.getDrawable(R.drawable.edittext_style_focus)
            }

            override fun afterTextChanged(p0: Editable?) {
            }
        })
    }

   private fun hasError(): Boolean {
        var isError = false
        streetAndNumbEdt.text.toString()
            .trim()
        if (streetAndNumbEdt.text.isEmpty()) {
            streetAndNumbEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
            streetRfTv.visibility = View.VISIBLE
            streetLabelTv.setTextColor(resources.getColor(R.color.lust))
            isError = true
        }
        else {
            streetRfTv.visibility = View.GONE
            streetLabelTv.setTextColor(resources.getColor(R.color.taupe))
        }

        if (postCodeEdt.text.isEmpty()) {
            postCodeEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
            postCodeRfTv.visibility = View.VISIBLE
            postCodeLabelTv.setTextColor(resources.getColor(R.color.lust))
            isError = true
        }
        else {
            postCodeRfTv.visibility = View.GONE
            postCodeLabelTv.setTextColor(resources.getColor(R.color.taupe))
        }

        if (cityEdt.text.isEmpty()) {
            cityEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
            cityRfTv.visibility = View.VISIBLE
            cityLabelTv.setTextColor(resources.getColor(R.color.lust))
            isError = true
        }
        else {
            cityRfTv.visibility = View.GONE
            cityLabelTv.setTextColor(resources.getColor(R.color.taupe))
        }

        if (mobilePhoneEdt.text.isEmpty()) {
            mobilePhoneEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
            mobileRfTv.visibility = View.VISIBLE
            mobileLabelTv.setTextColor(resources.getColor(R.color.lust))
            isError = true
        }
        else {
            mobileRfTv.visibility = View.GONE
            mobileLabelTv.setTextColor(resources.getColor(R.color.taupe))
        }

        if (emailEdt.text.isEmpty()) {
            emailEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
            emailRfTv.visibility = View.VISIBLE
            emailLabelTv.setTextColor(resources.getColor(R.color.lust))
            emailRfTv.text = getString(R.string.required_field)
            isError = true
        }
        else {
            if (isEmailValid(emailEdt.text.toString())) {
                emailRfTv.visibility = View.GONE
                emailLabelTv.setTextColor(resources.getColor(R.color.taupe))
            }
            else {
                emailEdt.background = resources.getDrawable(R.drawable.red_rr_radius_3)
                emailRfTv.visibility = View.VISIBLE
                emailLabelTv.setTextColor(resources.getColor(R.color.lust))
                emailRfTv.text = getString(R.string.valid_email)
                isError = true
            }
        }
        return isError
    }

    private fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email)
            .matches()
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity!!.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }
}

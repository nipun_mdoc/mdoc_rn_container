package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Admir on 12/4/2016.
 */

public class WeeklyMealOffer implements Serializable {

    private long mondayDate;
    private ArrayList<DayMealOffer> mondayOffers;
    private ArrayList<DayMealOffer> tuesdayOffers;
    private ArrayList<DayMealOffer> wednesdayOffers;
    private ArrayList<DayMealOffer> thursdayOffers;
    private ArrayList<DayMealOffer> fridayOffers;
    private ArrayList<DayMealOffer> saturdayOffers;
    private ArrayList<DayMealOffer> sundayOffers;

    public long getMondayDate() {
        return mondayDate;
    }

    public void setMondayDate(long mondayDate) {
        this.mondayDate = mondayDate;
    }

    public ArrayList<DayMealOffer> getMondayOffers() {
        return mondayOffers;
    }

    public void setMondayOffers(ArrayList<DayMealOffer> mondayOffers) {
        this.mondayOffers = mondayOffers;
    }

    public ArrayList<DayMealOffer> getTuesdayOffers() {
        return tuesdayOffers;
    }

    public void setTuesdayOffers(ArrayList<DayMealOffer> tuesdayOffers) {
        this.tuesdayOffers = tuesdayOffers;
    }

    public ArrayList<DayMealOffer> getWednesdayOffers() {
        return wednesdayOffers;
    }

    public void setWednesdayOffers(ArrayList<DayMealOffer> wednesdayOffers) {
        this.wednesdayOffers = wednesdayOffers;
    }

    public ArrayList<DayMealOffer> getThursdayOffers() {
        return thursdayOffers;
    }

    public void setThursdayOffers(ArrayList<DayMealOffer> thursdayOffers) {
        this.thursdayOffers = thursdayOffers;
    }

    public ArrayList<DayMealOffer> getFridayOffers() {
        return fridayOffers;
    }

    public void setFridayOffers(ArrayList<DayMealOffer> fridayOffers) {
        this.fridayOffers = fridayOffers;
    }

    public ArrayList<DayMealOffer> getSaturdayOffers() {
        return saturdayOffers;
    }

    public void setSaturdayOffers(ArrayList<DayMealOffer> saturdayOffers) {
        this.saturdayOffers = saturdayOffers;
    }

    public ArrayList<DayMealOffer> getSundayOffers() {
        return sundayOffers;
    }

    public void setSundayOffers(ArrayList<DayMealOffer> sundayOffers) {
        this.sundayOffers = sundayOffers;
    }
}

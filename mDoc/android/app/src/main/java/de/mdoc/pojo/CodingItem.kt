package de.mdoc.pojo

import android.os.Parcelable
import de.mdoc.util.overrideNullText
import kotlinx.android.parcel.Parcelize
import java.util.*

fun newCodingItem(code: String?, display: String?): CodingItem {
    return CodingItem(
        system = null,
        code = code ?: "",
        display = display ?: ""
    )
}

fun newCodingItem(code: String?): CodingItem {
    return CodingItem(
        system = null,
        code = code ?: "",
        display = null
    )
}
@Parcelize
data class CodingItem (
    val system: String? = null,
    val code: String? = null,
    val display: String? = null
): Parcelable {

    override fun equals(other: Any?): Boolean {
        return other === this || (other as? CodingItem)?.let {
            it.code == code
        } ?: false

    }

    override fun hashCode(): Int {
        return system.hashCode() + code.hashCode() * 0x10000
    }

    fun getInitials() : String{
        val words = display?.trim()?.split(" ")
        var first : String? = null
        var second : String? = null

        if(!words.isNullOrEmpty()){
            first = words.getOrNull(0)?.first().toString().toUpperCase(Locale.getDefault())
            second = words.getOrNull(words.size-1)?.first().toString().toUpperCase(Locale.getDefault())
        }
        return overrideNullText(first) + overrideNullText(second)
    }
}

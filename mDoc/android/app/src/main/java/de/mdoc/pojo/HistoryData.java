package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AdisMulabdic on 9/8/17.
 */

public class HistoryData {

    @SerializedName("data")
    @Expose
    private DataDeviceHistory data;

    public DataDeviceHistory getData() {
        return data;
    }

    public void setData(DataDeviceHistory data) {
        this.data = data;
    }
}

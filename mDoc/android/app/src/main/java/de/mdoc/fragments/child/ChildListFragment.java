package de.mdoc.fragments.child;


import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.adapters.ChildListAdapter;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.Child;
import de.mdoc.pojo.ChildResponse;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChildListFragment extends MdocFragment {

    MdocActivity activity;

    @BindView(R.id.childListRv)
    RecyclerView childListRv;
    ChildListAdapter adapter;
    ProgressDialog progressDialog;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_child_list;
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.FamilyAccount;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        activity = getMdocActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.show();

        childListRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.VERTICAL, false));
        getAllChilds();
    }

    private void getAllChilds() {
        progressDialog.show();
        MdocManager.getChildAccounts(new Callback<ChildResponse>() {
            @Override
            public void onResponse(Call<ChildResponse> call, Response<ChildResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        ArrayList<Child> childs;
                        if (response.body().getData().size() >= 0) {
                            childs = response.body().getData();
                            adapter = new ChildListAdapter(activity, childs);
                            childListRv.setAdapter(adapter);
                        }
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ChildResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}

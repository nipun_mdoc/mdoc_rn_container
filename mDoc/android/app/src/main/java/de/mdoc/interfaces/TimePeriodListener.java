package de.mdoc.interfaces;

public interface TimePeriodListener {
    void timePeriodSelected();
}
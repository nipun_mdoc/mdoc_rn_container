package de.mdoc.data.responses

import de.mdoc.data.my_stay_data.MyStayWidgetData

data class MyStayWidgetResponse(val data:MyStayWidgetData)
package de.mdoc.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import butterknife.ButterKnife;
import de.mdoc.R;


/**
 * Created by ema on 4/6/17.
 */
public abstract class MdocBaseActivity extends AppCompatActivity {

    abstract public int getResourceId();

    abstract public void init(Bundle savedInstanceState);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DataBindingUtil.setContentView(this, getResourceId());
        ButterKnife.bind(this);
        if (getResources().getBoolean(R.bool.phone) && savedInstanceState == null) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        init(savedInstanceState);
    }

    //Hide keyboard when user clicks on view that is not EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int[] scrcoords = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    /**
     * Showing fragments using old way before navigation migration -> https://mdocplatform.atlassian.net/browse/MDPD-9414.
     * This method is left only because of VideoConferenceFragment.
     *
     * @deprecated Use Navigation component instead.
     */
    @Deprecated
    public void showNewFragmentAdd(Fragment frag, int container) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment currentFragment = fragmentManager.findFragmentById(container);
        if (currentFragment != null && currentFragment.getClass().equals(frag.getClass()))
            return;
        fragmentManager.beginTransaction().add(container, frag)
                .commit();
    }
}

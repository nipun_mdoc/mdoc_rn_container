package de.mdoc.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.UserNotAuthenticatedException;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import de.mdoc.constants.MdocConstants;
import de.mdoc.constants.MdocSharedPreferencesConst;
import de.mdoc.pojo.ConfigurationData;
import de.mdoc.pojo.ConfigurationListItem;
import de.mdoc.pojo.NotificationListItem;
import de.mdoc.pojo.PublicUserDetails;
import de.mdoc.pojo.SubConfigurationData;
import de.mdoc.security.EncryptionServices;

/**
 * Created by ema on 4/6/17.
 */

public class MdocAppHelper {

    private static MdocAppHelper instance;
    private Context context;
    private MutableLiveData<List<NotificationListItem>> notifications = new MutableLiveData();
    private MutableLiveData<Integer> notificationsCount = new MutableLiveData();
    private long checkInDate, checkOutDate;
    private String code;
    private PublicUserDetails publicUserDetailses;
    private String errorMsg;
    private SharedPreferences mSecurePrefs;
    private EncryptionServices encryptionServices;

    private ConfigurationData configurationData;

    private String sessionLoginErrMessage = null;
    private boolean isSelfRegister = false;

    private MdocAppHelper() {
        notifications.setValue(new ArrayList<>(0));
        notifications.observeForever(new Observer<List<NotificationListItem>>() {
            @Override
            public void onChanged(List<NotificationListItem> list) {
                updateNotificationCounter(list);
            }
        });
    }

    private HashMap<String, String> allergens = new HashMap<>();

    private HashMap<String, String> additives = new HashMap<>();

    public HashMap<String, String> getAllergens() {
        return allergens;
    }

    public void setAllergens(HashMap<String, String> allergens) {
        this.allergens = allergens;
    }

    public HashMap<String, String> getAdditives() {
        return additives;
    }

    public void setAdditives(HashMap<String, String> additives) {
        this.additives = additives;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getSessionLoginErrMessage() {
        return sessionLoginErrMessage;
    }

    public void setSessionLoginErrMessage(String sessionLoginErrMessage) {
        this.sessionLoginErrMessage = sessionLoginErrMessage;
    }

    public static MdocAppHelper getInstance() {
        if (instance == null) {
            instance = new MdocAppHelper();
        }
        return instance;
    }

    public LiveData<Integer> getNotificationIndex() {
        return notificationsCount;
    }

    public void setIsPremiumPatient(boolean isPremium) {
        writeToSharedPreferences(MdocSharedPreferencesConst.IS_PREMIUM_PATIENT, isPremium);
    }

    public boolean isPremiumPatient() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.IS_PREMIUM_PATIENT, false);
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public PublicUserDetails getPublicUserDetailses() {
        return publicUserDetailses;
    }

    public void setPublicUserDetailses(PublicUserDetails publicUserDetailses) {
        this.publicUserDetailses = publicUserDetailses;
    }

    public LiveData<List<NotificationListItem>> getNotifications() {
        return notifications;
    }

    public NotificationListItem getNotificationByPosition(int position) {
        List<NotificationListItem> list = notifications.getValue();
        if (list != null && position >= 0 && position < list.size()) {
            return list.get(position);
        } else {
            return null;
        }
    }

    public void setNotifications(ArrayList<NotificationListItem> notifications) {
        this.notifications.setValue(notifications);
    }

    public void setAllNotificationsSeen() {
        List<NotificationListItem> list = notifications.getValue();
        if (list != null) {
            for (NotificationListItem item : list) {
                item.setSeen(true);
            }
        }
        notifications.setValue(list);

    }

    private void updateNotificationCounter(List<NotificationListItem> list) {
        int unread = 0;
        if (list != null) {
            for (NotificationListItem item : list) {
                if (item.isNotSeen()) {
                    unread++;
                }
            }
        }
        notificationsCount.setValue(unread);
    }

    public long getCheckInDate() {
        return checkInDate;
    }

    public void setCheckInDate(long checkInDate) {
        this.checkInDate = checkInDate;
    }

    public long getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(long checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public void deleteAllNotifications() {
        setNotifications(new ArrayList<>(0));
    }

    public void deleteNotification(String id) {
        List<NotificationListItem> list = notifications.getValue();
        if (list != null) {
            Iterator<NotificationListItem> iterator = list.iterator();
            while (iterator.hasNext()) {
                if (iterator.next().getId().equals(id)) {
                    iterator.remove();
                }
            }
        }
        notifications.setValue(list);
    }

    public void init(Context cont) {
        this.context = cont;
        encryptionServices = new EncryptionServices(context);
    }

    public String getChecklistOrder() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.CHECKLIST_ORDER, null);
    }

    public void setChecklistOrder(String checklistOrder) {
        writeToSharedPreferences(MdocSharedPreferencesConst.CHECKLIST_ORDER, checklistOrder);
    }

    public long getAuthExpireTime() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.LOGIN_TIME, Long.valueOf(0));
    }

    public void setAuthExpireTime(long time) {
        writeToSharedPreferences(MdocSharedPreferencesConst.LOGIN_TIME, time);
    }

    public String getUserAuth() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.USER_AUTH, null);
    }

    public void setUserAuth(String userAuth) {
        writeToSharedPreferences(MdocSharedPreferencesConst.USER_AUTH, userAuth);
    }

    public boolean isSelfRegister() {
        return isSelfRegister;
    }

    public void setSelfRegister(boolean selfRegister) {
        isSelfRegister = selfRegister;
    }

    public boolean isShowGoalsOnboarding() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.SHOW_GOALS_ONBORDING, true);
    }

    public void setShowGoalsOnboarding(boolean showOnboarding) {
        writeToSharedPreferences(MdocSharedPreferencesConst.SHOW_GOALS_ONBORDING, showOnboarding);
    }

    public boolean isOptIn() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.IS_OPT_IN, false);
    }

    public void setOptIn(boolean isOptIn) {
        writeToSharedPreferences(MdocSharedPreferencesConst.IS_OPT_IN, isOptIn);
    }

    public String getClinicId() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.CLINIC_ID);
    }

    public void setClinicId(String clinicId) {
        String encryptedValue = null;
        if (clinicId != null) {
            encryptedValue = encryptionServices.encrypt(clinicId);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.CLINIC_ID, encryptedValue);
    }

    public String getClinicName() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.CLINIC_NAME, null);
    }

    public void setClinicName(String clinicId) {
        writeToSharedPreferences(MdocSharedPreferencesConst.CLINIC_NAME, clinicId);
    }


    public void setPatientId(String patientId) {
        String encryptedValue = null;
        if (patientId != null) {
            encryptedValue = encryptionServices.encrypt(patientId);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.PATIENT_ID, encryptedValue);
    }

    public String getPatientId() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.PATIENT_ID);
    }

    public void setUserId(String userId) {
        String encryptedValue = null;
        if (userId != null) {
            encryptedValue = encryptionServices.encrypt(userId);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.USER_ID, encryptedValue);
    }

    public String getUserId() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.USER_ID);
    }

    public void setChildLoged(boolean isChild) {
        writeToSharedPreferences(MdocSharedPreferencesConst.CHILD_ACCOUNT, isChild);
    }

    public boolean getChildLogin() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.CHILD_ACCOUNT, false);
    }

    public void setUserLastName(String userLastName) {
        String encryptedValue = null;
        if (userLastName != null) {
            encryptedValue = encryptionServices.encrypt(userLastName);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.USER_LAST_NAME, encryptedValue);
    }

    public String getUserLastName() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.USER_LAST_NAME);
    }

    public void setUserFirstName(String userFirstName) {
        String encryptedValue = null;
        if (userFirstName != null) {
            encryptedValue = encryptionServices.encrypt(userFirstName);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.USER_FIRST_NAME, encryptedValue);
    }

    public String getUserFirstName() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.USER_FIRST_NAME);
    }


    public void setUserType(String userType) {
        writeToSharedPreferences(MdocSharedPreferencesConst.USER_TYPE, userType);
    }

    public String getUserType() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.USER_TYPE, null);
    }

    public void setLanguage(String language) {
        writeToSharedPreferences(MdocSharedPreferencesConst.LANGUAGE, language);
    }

    public String getLanguage() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.LANGUAGE, null);
    }

    public void setDepartment(String department) {
        writeToSharedPreferences(MdocSharedPreferencesConst.DEPARTMENT, department);
    }

    public String getDepartment() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.DEPARTMENT, null);
    }

    public void setIsClosed(boolean isClosed) {
        writeToSharedPreferences(MdocSharedPreferencesConst.IS_CLOSED_WELCOME_MESSAGE, isClosed);
    }

    public boolean getIsClosed() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.IS_CLOSED_WELCOME_MESSAGE, false);
    }

    public void setClinicLatitude(float latitude) {
        writeToSharedPreferences(MdocSharedPreferencesConst.LATITUDE, latitude);
    }

    public float getClinicLatitude() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.LATITUDE, 0f);
    }

    public void setClinicLongitude(float longitude) {
        writeToSharedPreferences(MdocSharedPreferencesConst.LONGITUDE, longitude);
    }

    public float getClinicLongitude() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.LONGITUDE, 0f);
    }

    public void setExternalCaseId(String externalCaseIdcaseId) {
        writeToSharedPreferences(MdocSharedPreferencesConst.EXTERNAL_CASE_ID, externalCaseIdcaseId);
    }

    public String getExternalCaseId() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.EXTERNAL_CASE_ID, null);
    }

    public void setCaseId(String caseId) {
        String encryptedValue = null;
        if (caseId != null) {
            encryptedValue = encryptionServices.encrypt(caseId);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.CASE_ID, encryptedValue);
    }

    public String getCaseId() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.CASE_ID);
    }

    public void setSensitiveData(String sensitiveData) {
        writeToSharedPreferences("SENSITIVE_DATA", sensitiveData);
    }

    public String getSensitiveData() {
        return readFromSharedPreferences("SENSITIVE_DATA", null);
    }

    public void setIsFirstTime(boolean isFirstTime) {
        writeToSharedPreferences(MdocSharedPreferencesConst.FIRST_TIME, isFirstTime);
    }

    public boolean getFirstTime() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.FIRST_TIME, false);
    }

    public void setPatientFileUploadAllowed(boolean patientFileUploadAllowed) {
        writeToSharedPreferences(MdocSharedPreferencesConst.PATIENT_FILE_UPLOAD, patientFileUploadAllowed);
    }

    public boolean getPatientFileUpload() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.PATIENT_FILE_UPLOAD, false);
    }

    public void setSelectedPollenRegionId(int id) {
        writeToSharedPreferences(MdocSharedPreferencesConst.SELECTED_POLLEN_REGION_ID, id);
    }

    public Integer getSelectedPollenRegionId() {
        int result = readFromSharedPreferences(MdocSharedPreferencesConst.SELECTED_POLLEN_REGION_ID, -1);
        if (result >= 0) {
            return result;
        } else
            return null;
    }

    public boolean isBiometricPrompted() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.BIOMETRIC_PROMPT, false);
    }

    public void setBiometricPrompted(boolean biometricPrompted) {
        writeToSharedPreferences(MdocSharedPreferencesConst.BIOMETRIC_PROMPT, biometricPrompted);
    }

    public void setUsername(String username) {
        String encryptedValue = null;
        if (username != null) {
            encryptedValue = encryptionServices.encrypt(username);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.USERNAME, encryptedValue);
    }

    public String getUsername() {
        return decryptWithMasterKey(MdocConstants.USERNAME);
    }

    public void setSecretKey(String secretKey) {
        String encryptedValue = null;
        if (secretKey != null) {
            encryptedValue = encryptionServices.encrypt(secretKey);
        }
        writeToSharedPreferences(MdocConstants.SECRET_KEY, encryptedValue);
    }

    public String getSecretKey() {

        String savedValue = readFromSharedPreferences(MdocConstants.SECRET_KEY, null);
        if (savedValue != null && !savedValue.isEmpty()) {
            try {
                savedValue = encryptionServices.decrypt(savedValue);
            } catch (Exception e) {
                //
            }
        }
        return savedValue;
    }

    public Boolean hasSecretKey() {
        return readFromSharedPreferences(MdocConstants.SECRET_KEY, null) != null;
    }

    public String getUserPassword() {

        String encryptedPassword = readFromSharedPreferences(MdocSharedPreferencesConst.PASSWORD, null);
        String password ="";
        if (isOptIn() && encryptedPassword != null) {
            try {
                password = encryptionServices.decryptWithFingerprint(encryptedPassword);
            } catch (UserNotAuthenticatedException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
            }
            if(password==""){
                try {
                    password = encryptionServices.decrypt(encryptedPassword);
                } catch (Exception e) {
                }
            }
        } else if (encryptedPassword != null && !encryptedPassword.isEmpty()) {
            try {
                password = encryptionServices.decrypt(encryptedPassword);
            } catch (Exception e) {
            }
        }
        return password;
    }

    public void setUserPassword(String password) {
        Log.e("password","setting...."+password);
        String encryptedValue = null;
        if (isOptIn() && password != null) {
            try {
                encryptedValue = encryptionServices.encryptWithFingerprint(password);
            } catch (InvalidKeyException e) {
                e.printStackTrace();

            }
        } else if (password != null) {
            encryptedValue = encryptionServices.encrypt(password);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.PASSWORD, encryptedValue);
    }

    public String getRefreshToken() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.REFRESH_TOKEN);
    }

    public void setRefreshToken(String refreshToken) {
        String encryptedValue = null;
        if (refreshToken != null) {
            encryptedValue = encryptionServices.encrypt(refreshToken);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.REFRESH_TOKEN, encryptedValue);
    }

    public String getRefreshTokenNoHeader() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.REFRESH_TOKEN_NO_HEADER);
    }

    public void setRefreshTokenNoHeader(String refreshToken) {
        String encryptedValue = null;
        if (refreshToken != null) {
            encryptedValue = encryptionServices.encrypt(refreshToken);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.REFRESH_TOKEN_NO_HEADER, encryptedValue);
    }

    public String getAccessToken() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.ACCESS_TOKEN);
    }

    public void setAccessToken(String accessToken) {
        String encryptedValue = null;
        if (accessToken != null) {
            encryptedValue = encryptionServices.encrypt(accessToken);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.ACCESS_TOKEN, encryptedValue);
    }

    public String getEmail() {
        return decryptWithMasterKey(MdocSharedPreferencesConst.EMAIL);
    }

    public void setEmail(String email) {
        String encryptedValue = null;
        if (email != null) {
            encryptedValue = encryptionServices.encrypt(email);
        }
        writeToSharedPreferences(MdocSharedPreferencesConst.EMAIL, encryptedValue);
    }

    public void setDisclaimerSeenDiary(Boolean seen) {
        writeToSharedPreferences(MdocSharedPreferencesConst.DIARY_DISCLAIMER, seen);
    }

    public Boolean getDisclaimerSeenDiary() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.DIARY_DISCLAIMER, false);
    }

    public void setDisclaimerSeenQuestionnaire(Boolean seen) {
        writeToSharedPreferences(MdocSharedPreferencesConst.QUESTIONNAIRE_DISCLAIMER, seen);
    }

    public Boolean getDisclaimerSeenQuestionnaire() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.QUESTIONNAIRE_DISCLAIMER, false);
    }

    public void setDisclaimerSeenVitals(Boolean seen) {
        writeToSharedPreferences(MdocSharedPreferencesConst.VITALS_DISCLAIMER, seen);
    }

    public Boolean getDisclaimerSeenVitals() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.VITALS_DISCLAIMER, false);
    }

    public void setDisclaimerSeenChecklist(Boolean seen) {
        writeToSharedPreferences(MdocSharedPreferencesConst.CHECKLIST_DISCLAIMER, seen);
    }

    public Boolean getDisclaimerSeenChecklist() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.CHECKLIST_DISCLAIMER, false);
    }

    public void setEmergencyInfo(String emergencyInfo) {
        writeToSharedPreferences(MdocSharedPreferencesConst.EMERGENCY_INFO, emergencyInfo);
    }

    public String getEmergencyInfo() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.EMERGENCY_INFO, null);
    }

    public void setPaperPrintInfo(String printStatus) {
        writeToSharedPreferences(MdocSharedPreferencesConst.PRINT_THERAPY_INFO, printStatus);
    }

    public String getPaperPrintInfo() {
        return readFromSharedPreferences(MdocSharedPreferencesConst.PRINT_THERAPY_INFO, null);
    }

    private void writeToSharedPreferences(String key, String value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putString(
                key,
                value
        );
        editor.apply();
    }

    private void writeToSharedPreferences(String key, boolean value) {

        SharedPreferences.Editor editor = context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putBoolean(
                key,
                value);
        editor.apply();
    }

    private void writeToSharedPreferences(String key, int value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putInt(
                key,
                value
        );
        editor.apply();
    }

    private void writeToSharedPreferences(String key, long value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putLong(
                key,
                value
        );
        editor.apply();
    }

    private void writeToSharedPreferences(String key, float value) {
        SharedPreferences.Editor editor = context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).edit();
        editor.putFloat(
                key,
                value
        );
        editor.apply();
    }


    private long readFromSharedPreferences(String key, long defaultValue) {
        return context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).getLong(key, defaultValue);
    }

    private String readFromSharedPreferences(String key, String defaultValue) {
        return context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).getString(key, defaultValue);
    }

    private boolean readFromSharedPreferences(String key, boolean defaultValue) {
        return context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).getBoolean(key, defaultValue);
    }

    private int readFromSharedPreferences(String key, int defaultValue) {
        return context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).getInt(key, defaultValue);
    }

    private float readFromSharedPreferences(String key, float defaultValue) {
        return context.getSharedPreferences(MdocSharedPreferencesConst.SHARED_PREFS_KEY, Context.MODE_PRIVATE).getFloat(key, defaultValue);
    }

    private String decryptWithMasterKey(String sharedPrefKey) {
        String savedValue = readFromSharedPreferences(sharedPrefKey, null);
        if (savedValue != null && !savedValue.isEmpty()) {
            savedValue = encryptionServices.decrypt(savedValue);
        }
        return savedValue;
    }

//    public void vibrate(Context context) {
//        var vibs = context.getSystemService(VIBRATOR_SERVICE) as Vibrator
//        vibs.vibrate(duration.toLong())
//    }

    public ConfigurationData getConfigurationData() {
        return configurationData;
    }

    public void setConfigurationData(ConfigurationData configurationData) {
        this.configurationData = configurationData;
    }

    public String getPairedIdForMoreMenu(String id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("dashboardRl", "DASHBOARD");
        map.put("profileRl", "MYPROFILE");
        map.put("covid19Rl", "MODULE_COVID19");
        map.put("therapyRl", "MODULE_THERAPY");
        map.put("mealRl", "MODULE_MEALS");
        map.put("airAndPollenRl", "MODULE_AIR_POLLEN");
        map.put("questionnaireRl", "MODULE_QUESTIONNAIRES");
        map.put("myClinicRl", "MODULE_MY_CLINIC");
        map.put("checklistRl", "MODULE_CHECKLIST");
        map.put("statisticsRl", "MODULE_VITALS");
        map.put("helpSupportRl", "MODULE_HELP_SUPPORT");
        map.put("mediaRl", "MODULE_MEDIA");
        map.put("patientJourneyRl", "MODULE_PATIENT_JOURNEY");
        map.put("filesRl", "MODULE_FILES");
        map.put("diaryRl", "MODULE_DIARY");
        map.put("childsRl", "MODULE_FAMILY_ACCOUNT");
        map.put("thiemeRl", "MODULE_CONSENT");
        map.put("notificationRl", "MODULE_NOTIFICATION");
        map.put("exerciseRl", "MODULE_EXERCISE");
        map.put("bookingRl", "MODULE_BOOKING");
        map.put("mentalGoalsRl", "MODULE_MENTAL_GOALS");
        map.put("medicationRl", "MODULE_MEDICATION_PLAN");
        map.put("devicesRl", "MODULE_DEVICES");
        map.put("entertainmentRl", "MODULE_ENTERTAINMENT");
        map.put("messagesRl", "MODULE_MESSAGE");
        map.put("externalServicesRl", "MODULE_EXTERNAL_SERVICE");
        map.put("myStayRl", "MODULE_MY_STAY");
//MODULE_PAIR, MODULE_COVID_MESSAGING, MODULE_NEWS, MODULE_COVID_HASHTAG,
        return map.get(id);
    }

    public String getPairedIdForBottomNav(String id) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("dashboardFragment", "DASHBOARD");
        map.put("navigation_questionnaire", "MODULE_QUESTIONNAIRES");
        map.put("navigation_my_stay", "MODULE_MY_CLINIC");
        map.put("moreFragment", "MODULE_MORE");
//Note:- dashboardFragment, navigation_questionnaire, navigation_my_stay these id needs to change basis on build varient (menu_main_bottom_navigation.xml)
        return map.get(id);
    }


    public boolean getSubConfigVisibility(String configId, String subConfigId){
        ConfigurationListItem listItem = null;
        for (ConfigurationListItem item : configurationData.getList()) {
            if (item.getId().equalsIgnoreCase(configId)) {
                listItem = item;
            }
        }

        if(listItem == null) {
            return false;
        }else{
            if(listItem.getIsEnabled()) {
                SubConfigurationData subConfigurationData = null;
                for (SubConfigurationData item : listItem.getSubConfig()) {
                    if (item.getId().equalsIgnoreCase(subConfigId)) {
                        subConfigurationData = item;
                    }
                }
                if (subConfigurationData == null) {
                    return false;
                }else {
                    return subConfigurationData.isEnabled();
                }
            }else{
                return false;
            }
        }
    }

    public int getSubConfigValues(String id, String widget){
        ConfigurationListItem listItem = null;
        for (ConfigurationListItem item : configurationData.getList()) {
            if (item.getId().equalsIgnoreCase(id)) {
                listItem = item;
            }
        }

        if(listItem == null) {
            return 10;
        }else{
            if(listItem.getIsEnabled()) {
                SubConfigurationData subConfigurationData = null;
                for (SubConfigurationData item : listItem.getSubConfig()) {
                    if(item != null) {
                        if (item.getId().equalsIgnoreCase(widget)) {
                            subConfigurationData = item;
                        }
                    }
                }
                if (subConfigurationData == null) {
                    return 10;
                }else {
                    return subConfigurationData.getValue();
                }
            }else{
                return 10;
            }
        }
    }
}

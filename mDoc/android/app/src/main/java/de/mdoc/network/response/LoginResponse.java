package de.mdoc.network.response;


import de.mdoc.pojo.UserData;

/**
 * Created by ema on 10/28/16.
 */

public class LoginResponse extends MdocResponse {

    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }
}

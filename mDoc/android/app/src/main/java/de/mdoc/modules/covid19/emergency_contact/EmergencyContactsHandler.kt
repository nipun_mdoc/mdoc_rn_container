package de.mdoc.modules.covid19.emergency_contact

import de.mdoc.storage.AppPersistence
import de.mdoc.util.overrideNullText
import timber.log.Timber
import java.util.*

class EmergencyContactsHandler {

    fun setRelationship(relationshipCoding: String?): String {
        var relationship = relationshipCoding
        AppPersistence.relationshipCoding.forEach {
            if (relationshipCoding == it.code) {
                relationship = it.display ?: ""
                return relationship ?: ""
            }
        }
        return relationship ?: ""
    }

    fun getInitials(name: String?): String {
        val words = name?.trim()
            ?.split(" ")
        var first: String? = null
        var second: String? = null

        if (!words.isNullOrEmpty()) {
            try {
                first = words.getOrNull(0)
                    ?.first()
                    ?.toString()
                    ?.toUpperCase(Locale.getDefault())
                second = words.getOrNull(words.size - 1)
                    ?.first()
                    ?.toString()
                    ?.toUpperCase(Locale.getDefault())
            } catch (e: Exception) {
                Timber.d(e)
            }
        }
        return overrideNullText(first) + overrideNullText(second)
    }
}
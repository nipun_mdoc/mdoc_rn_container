package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 12/12/16.
 */

public class MealChoiceData implements Serializable {

    private MealChoiceOld mealChoice;
    private MealPlanOld mealPlan;

    public MealChoiceOld getMealChoice() {
        return mealChoice;
    }

    public void setMealChoice(MealChoiceOld mealChoice) {
        this.mealChoice = mealChoice;
    }

    public MealPlanOld getMealPlan() {
        return mealPlan;
    }

    public void setMealPlan(MealPlanOld mealPlan) {
        this.mealPlan = mealPlan;
    }
}

package de.mdoc.activities.login.clinicselection

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.data.requests.ClinicSelectionRequest
import de.mdoc.network.response.SpeciltyData
import de.mdoc.pojo.ClinicDetails
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class ClinicSelectionViewModel(private val mDocService: IMdocService) : ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(true)
    var content: MutableLiveData<ArrayList<ClinicDetails>> = MutableLiveData(ArrayList())
    val isCaseCreated = MutableLiveData(false)
    var content1: MutableLiveData<ArrayList<SpeciltyData>> = MutableLiveData(ArrayList())

    init {
        getClinics()
    }

    private fun getClinics() {
        viewModelScope.launch {
            try {
                getSpecialities()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
            }
        }
    }

    fun generateCase(clinicId: String?) {
        clinicId?.let {
            viewModelScope.launch {
                try {
                    isLoading.value = true
                    isShowNoDataMessage.value = false
                    isShowLoadingError.value = false

                    mDocService.generateCaseWithClinicId(ClinicSelectionRequest(it))

                    isCaseCreated.value = true
                    isLoading.value = false
                } catch (e: java.lang.Exception) {
                    isLoading.value = false
                    isShowContent.value = false
                    isShowLoadingError.value = true
                    isShowNoDataMessage.value = false
                }
            }
        }
    }
    private suspend fun getSpecialities(){
        content1.value = mDocService.getSpecialities("").data
        viewModelScope.launch {
            try {
                getClinicsAsync()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
            }
        }
    }

    private suspend fun getClinicsAsync() {
        isLoading.value = true
        isShowNoDataMessage.value = false
        isShowLoadingError.value = false

        content.value = mDocService.getSelfRegClinics().data
        var i=0;
        var special: ArrayList<String> = ArrayList();
        for (item in (content.value as java.util.ArrayList<ClinicDetails>?)!!) {
            special.clear()
            for(item1 in item.specialities!!){
                var s : String = content1.value?.filter { it.code == item1 }?.get(0)!!.specialty
                special.add(s)
            }
            (content.value as java.util.ArrayList<ClinicDetails>?)!![i]?.specialitiesNames?.addAll(special)
            i += 1;
        }

        isLoading.value = false

        if (content.value.isNullOrEmpty()) {
            isShowNoDataMessage.value = true
            isShowContent.value = false
        } else {
            isShowContent.value = true
            isShowNoDataMessage.value = false
        }
    }

}
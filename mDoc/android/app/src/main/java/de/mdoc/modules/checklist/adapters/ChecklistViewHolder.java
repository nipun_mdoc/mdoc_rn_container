package de.mdoc.modules.checklist.adapters;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.RecyclerView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import java.text.SimpleDateFormat;
import java.util.Date;

import at.blogc.android.views.ExpandableTextView;
import de.mdoc.R;
import de.mdoc.interfaces.ItemTouchHelperViewHolder;
import de.mdoc.pojo.ChecklistItem;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;

class ChecklistViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {

    public static String CHECKLIST_TYPE_ARRIVAL = "ARRIVAL";
    public static String CHECKLIST_TYPE_DEPARTURE = "DEPARTURE";
    public LinearLayout checklistLinear;
    public ExpandableTextView txtChecklistIntroduction;
    private TextView txtChecklistTitle, txtChecklistTotal, txtChecklistCurrent, txtChecklistDate;
    private CircularProgressBar circularProgressBar;
    private ImageView imageCheckComplete;
    private AppCompatButton toogle;
    private LinearLayout viewToogle;

    public ChecklistViewHolder(View itemView) {
        super(itemView);

        this.txtChecklistTitle = itemView.findViewById(R.id.txtChecklistTitle);
        this.txtChecklistIntroduction = itemView.findViewById(R.id.txtChecklistIntroduction);
        this.txtChecklistTotal = itemView.findViewById(R.id.txtChecklistTotal);
        this.txtChecklistCurrent = itemView.findViewById(R.id.txtChecklistCurrent);
        this.txtChecklistDate = itemView.findViewById(R.id.txtChecklistDate);
        this.checklistLinear = itemView.findViewById(R.id.checklistLinear);
        this.circularProgressBar = itemView.findViewById(R.id.circularView);
        this.imageCheckComplete = itemView.findViewById(R.id.imageCheckComplete);

        this.toogle = itemView.findViewById(R.id.button_toggle);
        this.viewToogle = itemView.findViewById(R.id.view_toogle);
        this.viewToogle.setOnClickListener(this);
        this.toogle.setOnClickListener(this);

        txtChecklistIntroduction.setInterpolator(new OvershootInterpolator());
    }

    public void updateChecklist(ChecklistItem checklistItem, Context context) {

        if (CHECKLIST_TYPE_ARRIVAL.equals(checklistItem.getCheckListType())) {
            String in = new SimpleDateFormat("dd MMM yyyy").format(new Date(MdocAppHelper.getInstance().getCheckInDate()));
            txtChecklistDate.setText(context.getResources().getString(R.string.check_until) + " " + in);
        } else if (CHECKLIST_TYPE_DEPARTURE.equals(checklistItem.getCheckListType())) {
            String out = new SimpleDateFormat("dd MMM yyyy").format(new Date(MdocAppHelper.getInstance().getCheckOutDate()));
            txtChecklistDate.setText(context.getResources().getString(R.string.check_until) + " " + out);
        }
        txtChecklistTitle.setText(checklistItem.getTitle());
        txtChecklistIntroduction.setText(Html.fromHtml(checklistItem.getIntroduction()));
        txtChecklistTotal.setText(checklistItem.getTotal());
        txtChecklistCurrent.setText(checklistItem.getCurrent());

        // Progress
        int current = Integer.valueOf(checklistItem.getCurrent());
        int total = Integer.valueOf(checklistItem.getTotal());


        if (total == current) {
            imageCheckComplete.setVisibility(View.VISIBLE);
            circularProgressBar.setVisibility(View.GONE);
        } else {
            imageCheckComplete.setVisibility(View.GONE);
            circularProgressBar.setVisibility(View.VISIBLE);
        }

        float progress = MdocUtil.calculateProgress(current, total);
        circularProgressBar.setProgress(progress);

        resetExpandView();
    }

    private void resetExpandView() {
        toogle.setText(R.string.pat_journey_view_more);
        toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_24_px, 0);
        txtChecklistIntroduction.collapse();
        if (hasEllipsis(txtChecklistIntroduction)) {
            toogle.setVisibility(View.VISIBLE);
        } else {
            toogle.setVisibility(View.GONE);
        }
    }

    private Boolean hasEllipsis(TextView textView) {
        if (textView!= null) {
            textView.onPreDraw();
        }

        Layout layout = textView.getLayout();
        if (layout != null) {
            int lines = layout.getLineCount();
            if (lines > 0) {
                int ellipsisCount = layout.getEllipsisCount(lines - 1);
                return ellipsisCount > 0;
            }
        }
        return false;
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }

    @Override
    public void onClick(View v) {
        if (v == viewToogle || v == toogle) {
            if (txtChecklistIntroduction.isExpanded()) {
                txtChecklistIntroduction.collapse();
                toogle.setText(R.string.pat_journey_view_more);
                toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down_24_px, 0);
            } else {
                txtChecklistIntroduction.expand();
                toogle.setText(R.string.pat_journey_view_less);
                toogle.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up_24_px, 0);
            }
        }
    }
}

package de.mdoc.modules.questionnaire

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.fragments.MdocFragment
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.layout_questionnaire_finish.*

/**
 * Created by ema on 1/17/17.
 */
class OutroQuestionnaireFragment: MdocFragment() {

    lateinit var activity: MdocActivity
    private var message: String? = null

    override fun setResourceId(): Int {
        return R.layout.fragment_questionnaire_finish
    }

    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireProgress

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity

        message = arguments?.getString(MdocConstants.POLL_END_MESSAGE)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        closingMessage?.text = MdocUtil.fromHtml(message)
    }

    private fun setupListeners() {
        finishBtn?.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}

package de.mdoc.newlogin.Interactor

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Rect
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.ImageViewCompat
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import de.mdoc.R
import de.mdoc.activities.CaseActivity
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.login.BiometricPromptDialogFragment
import de.mdoc.activities.login.BiometricPromptDialogFragmentCallback
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.activities.login.TermsPrivacyDialogHandler
import de.mdoc.activities.login.clinicselection.ClinicSelectionActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.ActivityRegistrationBinding
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.newlogin.AuthListener
import de.mdoc.newlogin.Model.CaptchaResponse
import de.mdoc.newlogin.Model.RegisterRequestData
import de.mdoc.newlogin.RegistratioToastListener
import de.mdoc.newlogin.RegistratioViewModel
import de.mdoc.pojo.Case
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.activity_registration.*
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern

class RegistrationActivity : AppCompatActivity(), LoginHelper.LoginCallback, AuthListener, RegistratioToastListener,
        TermsPrivacyDialogHandler.TermsPrivacyCallback,
        BiometricPromptDialogFragmentCallback, CommonDialogFragment.OnButtonClickListener {

    private var registrationViewModel: RegistratioViewModel? = null
    private var binding: ActivityRegistrationBinding? = null

    private lateinit var encryptionService: EncryptionServices
    private lateinit var systemServices: SystemServices

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 100
        const val REFRESH_TOKEN_RESPONSE_CODE = "REFRESH_TOKEN_RESPONSE_CODE"
        const val FORCED_LOGOUT = "FORCED_LOGOUT"
        const val LOGIN_SCREEN_CONDITION = "redirect_uri=mdocapp"
    }

    private val progressHandler = ProgressHandler(this)
    private val loginHelper by lazy {
        LoginHelper(this, this, progressHandler)
    }
    private var _cases: ArrayList<Case>? = null
    private val termsPrivacyDialogHandler by lazy {
        TermsPrivacyDialogHandler(this, this, progressHandler)
    }
    private var isFingerprintAuthenticated: Boolean = false

    private var isPasswordValidated: Boolean = false
    private lateinit var llPasswordValidator : LinearLayout
    private lateinit var layout : View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        registrationViewModel = ViewModelProviders.of(this).get(RegistratioViewModel::class.java)
        binding = DataBindingUtil.setContentView(this@RegistrationActivity, R.layout.activity_registration)
        binding?.setLifecycleOwner(this)
        binding?.setRegistratioViewModel(registrationViewModel)

        llPasswordValidator = findViewById(R.id.llPasswordValidator)
        layout = findViewById(R.id.passwordDialog)
        val txtInfo1 = layout.findViewById<TextView>(R.id.txtInfo1)
        val sLength = resources.getString(R.string.info1)
        val sFinal = String.format(sLength, resources.getInteger(R.integer.password_length))
        txtInfo1.text = sFinal
        txtPassword1.doAfterTextChanged { text ->
//            if(llPasswordValidator.visibility == View.VISIBLE){
                validatePassword(text.toString())
//            }
        }





        var shakeAnimation: Animation? = AnimationUtils.loadAnimation(this, R.anim.shake)
        registrationViewModel!!.requestData.observe(this, Observer<RegisterRequestData> { registerRequestModel ->

            txtUserNamelayout.setError(null);
            txtFirstNamelayout.setError(null);
            txtSurNamelayout.setError(null);
            txtEmaial_layout.setError(null);
            txtPasswordlayout.setError(null);
            txtConfrimPasswordlayout.setError(null);

            if (TextUtils.isEmpty(Objects.requireNonNull(registerRequestModel)!!.username) || !validateUserName(Objects.requireNonNull(registerRequestModel)!!.username)) {
                txtUserNamelayout?.setError(" ")
                txtUserNamelayout.errorIconDrawable = null
                txtUserNamelayout?.requestFocus();
                txtUserNamelayout.startAnimation(shakeAnimation)
            } else if (TextUtils.isEmpty(Objects.requireNonNull(registerRequestModel)!!.firstName)) {
                txtFirstNamelayout?.setError(" ")
                txtFirstNamelayout.errorIconDrawable = null
                txtFirstNamelayout?.requestFocus();
                txtFirstNamelayout.startAnimation(shakeAnimation)

            } else if (TextUtils.isEmpty(Objects.requireNonNull(registerRequestModel)!!.lastName)) {
                txtSurNamelayout?.setError(" ")
                txtSurNamelayout.errorIconDrawable = null
                txtSurNamelayout?.requestFocus();
                txtSurNamelayout.startAnimation(shakeAnimation)

            } else if (TextUtils.isEmpty(Objects.requireNonNull(registerRequestModel)!!.email)) {
                txtEmaial_layout?.setError(" ")
                txtEmaial_layout.errorIconDrawable = null
                txtEmaial_layout?.requestFocus();
                txtEmaial_layout.startAnimation(shakeAnimation)

            } else if (!Patterns.EMAIL_ADDRESS.matcher(registerRequestModel!!.email).matches()) {
                txtEmaial_layout?.setError(" ")
                txtEmaial_layout.errorIconDrawable = null
                txtEmaial_layout?.requestFocus();
                txtEmaial_layout.startAnimation(shakeAnimation)

            } else if (TextUtils.isEmpty(Objects.requireNonNull(registerRequestModel)!!.confirmPassword)) {
                txtConfrimPasswordlayout?.setError(" ")
                txtConfrimPasswordlayout.errorIconDrawable = null
                txtConfrimPasswordlayout?.requestFocus();
                txtConfrimPasswordlayout.startAnimation(shakeAnimation)

            } else if (!registerRequestModel!!.confirmPassword.equals(registerRequestModel!!.password)) {
                txtPasswordlayout?.setError(" ")
                txtPasswordlayout.errorIconDrawable = null
                txtPasswordlayout?.requestFocus();
                txtPasswordlayout.startAnimation(shakeAnimation)

            } else if ((registerRequestModel)!!.password!!.length < 8 || !isPasswordValidated) {
                txtPasswordlayout?.setError(" ")
                txtPasswordlayout.errorIconDrawable = null
                txtPasswordlayout?.requestFocus();
                txtPasswordlayout.startAnimation(shakeAnimation)

            } else {
                registrationViewModel?.loadData(registerRequestModel!!, this)
            }
        })

//        displayToastAboveButton(infoPassword, "")

        registrationViewModel?.authListener = this
        registrationViewModel?.toastListener = this


        encryptionService = EncryptionServices(this)
        systemServices = SystemServices(this)

        if (encryptionService.getMasterKey() == null) {
            encryptionService.createMasterKey()
        }


        if (savedInstanceState == null) {
            BandwidthCheck.initBandwidthCheck()
        }
    }

    fun validateUserName(text: String?): Boolean {
        val regex = "^(?![_.])(?!.*[_.]{2})[a-z0-9._]+(?<![_.])\$"
        val p: Pattern = Pattern.compile(regex)
        if (text == null) {
            return false
        }
        val m: Matcher = p.matcher(text)
        return m.matches()
    }

    private fun decideFlowBasedOnCases() {
        if (_cases == null) {
            navigateToDashboard()
        } else {
            openCases(_cases)
        }
    }

    private fun navigateToDashboard() {
        val intent = Intent(this, MdocActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)

//        if (BuildConfig.FLAV != "kiosk") {
            finish()
//        }
    }

    private fun openCases(cases: ArrayList<Case>?) {
        val intent = Intent(applicationContext, CaseActivity::class.java)
        intent.putExtra(MdocConstants.CASES, cases)
        startActivity(intent)
        finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == ClinicSelectionActivity.CLINIC_SELECTION_RESULT) {
            progressHandler.showProgress()
            if (resultCode == Activity.RESULT_OK) {
                val handler = Handler()
                handler.postDelayed({
                    loginHelper.loginWithSession()
                }, 7000)
            }
        }
    }

    override fun openTermsAndConditions() {
        termsPrivacyDialogHandler.openTermsFromCoding()
    }

    override fun openPrivacyPolicy() {
        termsPrivacyDialogHandler.openPrivacyFromCoding()
    }

    override fun openNextActivity(cases: ArrayList<Case>?) {
        _cases = cases

        if (MdocAppHelper.getInstance()
                        .isOptIn && MdocConstants.IS_UPDAE_PASSWORD) {
            MdocConstants.IS_UPDAE_PASSWORD = false
            SystemServices(this).authenticateFingerprint(successAction = {
                systemServices.optIn(MdocConstants.NEW_PASSWORD)
                decideFlowBasedOnCases()
            }, cancelAction = {
                this.runOnUiThread {
                    MdocAppHelper.getInstance().isOptIn = false
                    decideFlowBasedOnCases()
                }
            }, context1 = this@RegistrationActivity)

        } else {
            if (systemServices.canOptIn()) {
                BiometricPromptDialogFragment(this).also { it.isCancelable = false }
                        .showNow(supportFragmentManager, "BiometricPromptDialogFragment")
            } else {
                decideFlowBasedOnCases()
            }
        }


    }

    override fun onPrivacyPolicyAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onTermsAndConditionsAccepted() {
        loginHelper.loginWithSession()
    }

    override fun onPrivacyPolicyDeclined() {
//        loadInitialPage()
    }

    override fun onTermsAndConditionsDeclined() {
//        loadInitialPage()
    }

    override fun openClinics() {
        val intent = Intent(this, ClinicSelectionActivity::class.java)
        startActivityForResult(intent, ClinicSelectionActivity.CLINIC_SELECTION_RESULT)
    }

    override fun onBiometricPromptDialogButtonClick(isPositiveButton: Boolean) {
        MdocAppHelper.getInstance()
                .isBiometricPrompted = true
        if (isPositiveButton) {
            optInBiometricAuthentication()
        } else {
            decideFlowBasedOnCases()
            MdocAppHelper.getInstance()
                    .userPassword = null
        }
    }

    var isShowing = false;
    override fun onMissingSecret() {
        if (isShowing) return
        isShowing = true
        CommonDialogFragment.Builder()
                .title(resources.getString(R.string.login_not_possible))
                .description(resources.getString(R.string.login_not_possible_description))
                .setPositiveButton(resources.getString(R.string.button_ok))
                .setOnClickListener(this)
                .build().showNow(supportFragmentManager, CommonDialogFragment::class.simpleName)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        isShowing = false
    }

    private fun optInBiometricAuthentication() {
        systemServices.authenticateFingerprint(successAction = {
            Toast.makeText(applicationContext,
                    resources.getString(R.string.biometric_auth_success), Toast.LENGTH_SHORT)
                    .show()
            val token = MdocAppHelper.getInstance()
                    .secretKey

            if (encryptionService.getFingerprintKey() == null) {
                encryptionService.createFingerprintKey()
            }
            MdocAppHelper.getInstance().isOptIn = true
            val password = MdocAppHelper.getInstance().userPassword
            MdocAppHelper.getInstance().userPassword = password
            MdocAppHelper.getInstance().secretKey = token

            decideFlowBasedOnCases()
        }, cancelAction = {
            this.runOnUiThread {
                decideFlowBasedOnCases()
            }
        }, context1 = this@RegistrationActivity)
    }

    override fun sucess() {
        MdocAppHelper.getInstance().isSelfRegister = true
        if (MdocAppHelper.getInstance().secretKey == null && MdocConstants.KEY_MISSING) {
            MdocConstants.KEY_MISSING = false
            loginHelper.fetchSecretKey();
        } else {
            loginHelper.loginWithSession()
        }
    }

    override fun failer() {
//        Toast.makeText(applicationContext, "Success112", Toast.LENGTH_SHORT).show()
    }

    override fun onLoadCaptcha(response: CaptchaResponse?) {
    }

    override fun isCaptchaVisible(): Boolean? {
        TODO("Not yet implemented")
    }

    override fun clearCaptchaField() {
        TODO("Not yet implemented")
    }
    override fun changePassword(token: String) {
        TODO("Not yet implemented")
    }

    override fun showMobileNumberScreen() {
        TODO("Not yet implemented")
    }
    override fun showOTPScreen(){
        TODO("Not yet implemented")
    }
    override fun onImpressumClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)    }
    override fun onDatenschutzDieClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)  }
    override fun onAGBClick(data: String) {
        var intent = Intent(this, ImpressumActivity::class.java)
        intent.putExtra("url", data);
        startActivity(intent)   }

    override fun onCloseClick() {
        finish()
    }

    override fun onScanClick() {
        TODO("Not yet implemented")
    }

    private fun validatePassword(password: String) {

        var passwordEightChar = layout.findViewById<ImageView>(R.id.passwordEightChar)
        var passwordNumricChar = layout.findViewById<ImageView>(R.id.passwordNumricChar)
        var passwordLowerChar = layout.findViewById<ImageView>(R.id.passwordLowerChar)
        var passwordUpperChar = layout.findViewById<ImageView>(R.id.passwordUpperChar)
        var passwordSpecialChar = layout.findViewById<ImageView>(R.id.passwordSpecialChar)


        var lower = false
        var upper = false
        var numbers = false
        var special = false

        if(password.length > 0) {
            for (i in 0..password.length - 1) {
                val c = password[i]
                if (c.isDigit()) numbers = true
                else if (c.isLowerCase()) lower = true
                else if (c.isUpperCase()) upper = true
                else special = true
            }
        }

        if (password.length < resources.getInteger(R.integer.password_length)) {
            passwordEightChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordEightChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!numbers) {
            passwordNumricChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordNumricChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!lower) {
            passwordLowerChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordLowerChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!upper) {
            passwordUpperChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordUpperChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!special) {
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)))
        }else{
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if(password.length >= 8 && numbers && lower && upper && special){
            isPasswordValidated = true
        }else{
            isPasswordValidated = false
        }
    }

    private fun displayToastAboveButton(v: View, password: String) {
        var xOffset = 0
        var yOffset = 0
        var gvr = Rect()
        var parent = infoPassword.parent as View
        var parentHeight = parent.height
        if (v.getGlobalVisibleRect(gvr)) {
            val root = v.rootView
            val halfWidth = root.right / 2
            val halfHeight = root.bottom / 2
            val parentCenterX: Int = ((gvr.right - gvr.left) / 2 + gvr.left)
            val parentCenterY: Int = ((gvr.bottom - gvr.top) / 2 + gvr.top)-95
            yOffset = if (parentCenterY <= halfHeight) {
                -(halfHeight - parentCenterY) - parentHeight
            } else {
                parentCenterY - halfHeight - parentHeight
            }
            if (parentCenterX < halfWidth) {
                xOffset = -(halfWidth - parentCenterX)
            }
            if (parentCenterX >= halfWidth) {
                xOffset = parentCenterX - halfWidth
            }
        }

        var toast = Toast(this)
        toast.duration = Toast.LENGTH_LONG
        var layout = LayoutInflater.from(this).inflate(R.layout.infotoast_layout, null, false)
        var passwordEightChar = layout.findViewById<ImageView>(R.id.passwordEightChar)
        var passwordNumricChar = layout.findViewById<ImageView>(R.id.passwordNumricChar)
        var passwordLowerChar = layout.findViewById<ImageView>(R.id.passwordLowerChar)
        var passwordUpperChar = layout.findViewById<ImageView>(R.id.passwordUpperChar)
        var passwordSpecialChar = layout.findViewById<ImageView>(R.id.passwordSpecialChar)

        var lower = false
        var upper = false
        var numbers = false
        var special = false

        if(password.length > 0) {
            for (i in 0..password.length - 1) {
                val c = password[i]
                if (c.isDigit()) numbers = true
                else if (c.isLowerCase()) lower = true
                else if (c.isUpperCase()) upper = true
                else special = true
            }
        }

        if (password.length < 8) {
            passwordEightChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordEightChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordEightChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!numbers) {
            passwordNumricChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordNumricChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordNumricChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!lower) {
            passwordLowerChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordLowerChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordLowerChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!upper) {
            passwordUpperChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)));
        }else{
            passwordUpperChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordUpperChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }
        if (!special) {
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_wrong);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.pollen_level_3)))
        }else{
            passwordSpecialChar.setImageResource(R.drawable.ic_icon_correct);
            ImageViewCompat.setImageTintList(passwordSpecialChar, ColorStateList.valueOf(resources.getColor(R.color.received_color)));
        }

        toast.view = layout
        toast.setGravity(Gravity.CENTER, xOffset, yOffset)
        toast.show()

    }

    override fun registrationToast(view: View?) {
        if (view != null) {
            if(llPasswordValidator.visibility == View.GONE){
                llPasswordValidator.visibility = View.VISIBLE
            }else{
                llPasswordValidator.visibility = View.GONE
            }
//            displayToastAboveButton(view, txtPassword1.text.toString())
        }
    }

}
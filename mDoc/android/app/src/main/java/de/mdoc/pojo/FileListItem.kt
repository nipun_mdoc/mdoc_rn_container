package de.mdoc.pojo

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import de.mdoc.R
import de.mdoc.constants.FILE_REJECTED_STATE
import java.io.Serializable
import java.util.*

enum class FileType(
    @DrawableRes val imageRes: Int = R.drawable.file_icon,
    val isImage: Boolean = false
) {
    IMAGE_PNG(
        imageRes = R.drawable.image_icon,
        isImage = true
    ),
    IMAGE_JPEG(
        imageRes = R.drawable.image_icon,
        isImage = true
    ),
    PDF(
        imageRes = R.drawable.pdf_icon
    ),
    DOC,
    UNKNOWN
}

enum class FileDetailsItemType(@StringRes val itemName: Int = -1) {
    MAIN,
    LIST_ITEM_CATEGORY(itemName = R.string.category)
}

fun getFileTypeFromExtension(extension: String?): FileType {
    return when (extension?.toLowerCase(Locale.getDefault()) ?: "") {
        "png" -> FileType.IMAGE_PNG
        "jpg", "jpeg" -> FileType.IMAGE_JPEG
        "pdf" -> FileType.PDF
        "doc", "docx" -> FileType.DOC
        else -> FileType.UNKNOWN
    }
}

fun getSharedByVisibility(userInfoList: ArrayList<userInfoListItem>?): Boolean {
    return !userInfoList.isNullOrEmpty() && userInfoList.size > 0
}

fun supportedImageTypes () = listOf(FileType.IMAGE_PNG, FileType.IMAGE_JPEG)
fun supportedDocumentTypes() = listOf(FileType.PDF)

fun FileListItem.isSupported (): Boolean {
    return supportedImageTypes().contains(type) || supportedDocumentTypes().contains(type)
}

/**
 * Created by ema on 4/2/18.
 */
data class FileListItem(
    val id: String = "",
    val name: String = "",
    val owner: String? = "",
    val path: String? = "",
    val extension: String? = "",
    val size: Int = 0,
    val title: String? = "",
    val category: String? = "",
    val cts: Long = 0,
    val uts: Long = 0,
    val description: String? = "",
    val sharingList: ArrayList<SharingList>? = arrayListOf(),
    val userInfoList: ArrayList<userInfoListItem>? = arrayListOf(),
    val sharable: Boolean = false,
    val updatable: Boolean = false,
    val deletable: Boolean = false,
    val sharedWithPatient: Boolean = false,
    val sharedByClinic: Boolean = false,
    val sharedWithClinic: Boolean = false,
    val stateDisplay: String= "",
    val state: String= ""
) : Serializable {

    val type: FileType
        get() = getFileTypeFromExtension(extension)

    val shareByVisibility: Boolean
        get() = getSharedByVisibility(userInfoList)

    fun isRejected() : Boolean{
        return state == FILE_REJECTED_STATE
    }
}


fun FileListItem.getFileDetailsItems (categories: List<CodingItem> = ArrayList()): Map<FileDetailsItemType, Any>? {
    val items = mutableMapOf<FileDetailsItemType,Any>()

    // Main item
    items[FileDetailsItemType.MAIN] = this

    // Sub items
    val category = categories.firstOrNull { category == it.code }?.display ?: "-"
    items[FileDetailsItemType.LIST_ITEM_CATEGORY] = category
    return items
}

data class userInfoListItem(
    val sharedBy: SharedBy,
    val user: User
)

data class SharedBy(
    val firstName: String,
    val lastName: String,
    val userId: String,
    val username: String
)

data class User(
    val firstName: String,
    val lastName: String,
    val userId: String,
    val username: String
)



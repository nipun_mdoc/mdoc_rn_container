package de.mdoc.modules.questionnaire.ext

import de.mdoc.pojo.ExtendedQuestion

fun ExtendedQuestion?.isMandatory(): Boolean {
    return this != null && required != null && required
}

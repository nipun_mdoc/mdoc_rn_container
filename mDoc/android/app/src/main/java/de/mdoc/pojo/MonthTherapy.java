package de.mdoc.pojo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ema on 11/15/16.
 */

public class MonthTherapy implements Serializable {

    private int month;
    private int year;
    private List<DayTherapy> therapies;

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<DayTherapy> getTherapies() {
        return therapies;
    }

    public void setTherapies(List<DayTherapy> therapies) {
        this.therapies = therapies;
    }
}

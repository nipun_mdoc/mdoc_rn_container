package de.mdoc.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ema on 3/20/17.
 */

public class Agenda implements Serializable {

    private String _id;
    @SerializedName("char")
    private String _char;
    private String description;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_char() {
        return _char;
    }

    public void set_char(String _char) {
        this._char = _char;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

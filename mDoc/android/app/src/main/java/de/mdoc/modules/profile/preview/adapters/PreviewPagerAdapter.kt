package de.mdoc.modules.profile.preview.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.R
import de.mdoc.modules.profile.preview.PreviewDataFactory
import de.mdoc.modules.profile.preview.fragments.InsuranceViewFragment
import de.mdoc.modules.profile.preview.fragments.PreviewMetadataFragment

class PreviewPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PreviewMetadataFragment().also {
                it.datasource = PreviewDataFactory.getPersonalData()
                it.headerTitle = R.string.meta_data_personal_data
            }
            1 -> PreviewMetadataFragment().also {
                it.datasource = PreviewDataFactory.getResidenceData()
                it.headerTitle = R.string.residence
            }
            2 -> PreviewMetadataFragment().also {
                it.datasource = PreviewDataFactory.getEmployerData()
                it.headerTitle = R.string.employer
            }
            3 -> PreviewMetadataFragment().also {
                it.datasource = PreviewDataFactory.getCaregiverData()
                it.headerTitle = R.string.caregiver
            }
            4 -> PreviewMetadataFragment().also {
                it.datasource = PreviewDataFactory.getFamilyDoctorData()
                it.headerTitle = R.string.family_doctor
            }
            else -> InsuranceViewFragment()
        }
    }

    override fun getCount(): Int {
        return 6
    }
}
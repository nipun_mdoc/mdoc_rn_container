package de.mdoc.viewmodel.compositelist

import android.view.View

open class ListItemViewHolder(
    val itemView: View
) {

    fun bind(position: Int, item: Any, adapter: ListItemTypeAdapter<*, *>) {
        (adapter as ListItemTypeAdapter<Any, ListItemViewHolder>).onBind(
            position,
            item,
            this
        )
    }
}
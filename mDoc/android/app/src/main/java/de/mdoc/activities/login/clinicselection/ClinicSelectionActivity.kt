package de.mdoc.activities.login.clinicselection

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import de.mdoc.R
import de.mdoc.databinding.ClinicSelectionLayoutBinding
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class ClinicSelectionActivity: AppCompatActivity() {

    companion object {
        const val CLINIC_SELECTION_RESULT = 1
    }

    private val viewModel by viewModel {
        ClinicSelectionViewModel(RestClient.getService())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ClinicSelectionLayoutBinding>(this, R.layout.clinic_selection_layout)
        binding.lifecycleOwner = this
        binding.viewModel = viewModel
        binding.handler = ClinicSelectionHandler(viewModel)

        viewModel.isCaseCreated.observe(this, Observer {
            if (it) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
    }
}
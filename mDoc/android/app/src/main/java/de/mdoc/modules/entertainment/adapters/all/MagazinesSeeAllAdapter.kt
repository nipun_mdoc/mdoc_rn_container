package de.mdoc.modules.entertainment.adapters.all

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.pojo.entertainment.*
import kotlinx.android.synthetic.main.entertainment_magazines_child_item.view.*
import java.text.SimpleDateFormat

class MagazinesSeeAllAdapter(private val items: Array<MediaLightDTO>?,
                             private val context: Context?) : BaseAdapter() {

    override fun getCount(): Int {
        return when (items) {
            null -> 0
            else -> items.size
        }
    }

    override fun getItem(position: Int): MediaLightDTO? {
        return items?.get(position)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        val holder: ViewHolder
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.entertainment_magazines_child_item, null)
            holder = ViewHolder(view)
            view.tag = holder
        } else {
            holder = view.tag as ViewHolder
        }

        holder.bind(items?.get(position))
        return view!!
    }

    private fun getFormattedDate(date: String?): String {
        return when (date) {
            null -> ""
            else -> {
                val formatter = SimpleDateFormat("yyyy-mm-dd")
                val parsedDate = formatter.parse(date)
                SimpleDateFormat("dd.mm.yyyy").format(parsedDate)
            }
        }
    }

    private fun getFormattedLanguage(language: String?): String {
        return when (language) {
            null -> ""
            else -> language.toUpperCase()
        }
    }

    private fun getCoverImage(item: MediaLightDTO?): String? {
        return when (item?.thumbails.isNullOrEmpty()) {
            true -> null
            false -> item?.thumbails?.get(0)?.content?.url
        }
    }


    inner class ViewHolder(itemView: View) {
        private val textView: TextView = itemView.child_textView
        private val imgMagazineCover: ImageView = itemView.imgMagazineCover
        private val txtDate: TextView = itemView.txtMagazinesDate
        private val emptyView: View = itemView.magazinesEmptyView
        private val progress: ProgressBar = itemView.progress
        private val txtRead: TextView = itemView.txtRead

        fun bind(item: MediaLightDTO?) {
            val placeholder = ColorDrawable(ContextCompat.getColor(imgMagazineCover.context, R.color.colorGray97))
            val options = RequestOptions()
                    .placeholder(placeholder)

            emptyView.visibility = View.GONE
            progress.visibility = View.VISIBLE

            // Download image
            Glide.with(imgMagazineCover.context)
                    .setDefaultRequestOptions(options)
                    .load(getCoverImage(item))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            emptyView.visibility = View.VISIBLE
                            progress.visibility = View.GONE
                            txtRead.visibility = View.GONE
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            progress.visibility = View.GONE
                            return false
                        }
                    })
                    .into(imgMagazineCover)

            // Set magazine name
            textView.text = item?.getMagazineName() ?: ""

            // Set date and language
            val date = getFormattedDate(item?.getPublicationDate())
            var language = getFormattedLanguage(item?.language)
            txtDate.text = "$date - $language"

            // setOnClickListener
            imgMagazineCover.setOnClickListener {
                val bundle = Bundle()
                val pageNumber = item?.readingHistory?.pageNumber?:0

                bundle.putString(MdocConstants.MAGAZINE, Gson().toJson(item?.getMediaResponseDto()))
                bundle.putInt(MdocConstants.MAGAZINE_PAGE_NUMBER, pageNumber)
                it?.findNavController()?.navigate(R.id.action_global_entertainmentMagazine, bundle)
            }

            // Read
            when (item?.readingHistory?.read == true) {
                true -> {
                    txtRead.visibility = View.VISIBLE
                }
                else -> {
                    txtRead.visibility = View.GONE
                }
            }
        }
    }
}
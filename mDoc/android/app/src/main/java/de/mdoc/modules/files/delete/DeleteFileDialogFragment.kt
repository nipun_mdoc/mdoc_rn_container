package de.mdoc.modules.files.delete

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import de.mdoc.pojo.FileListItem
import de.mdoc.util.serializableArgument
import kotlinx.android.synthetic.main.delete_confirmation_dialog.*


class DeleteFileDialogFragment : DialogFragment() {

    private var item by serializableArgument<FileListItem>("file_item")

    fun <T> newInstance(item: FileListItem, listener: T): DeleteFileDialogFragment
            where T : Fragment, T : FileDeleteConfirmedListener {
        return DeleteFileDialogFragment().also {
            it.item = item
            it.setTargetFragment(listener, 0)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(de.mdoc.R.layout.delete_confirmation_dialog, container)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fileNameTv.text = item.name
        fileTitleTv.text = item.description
        iconIv.setImageResource(item.type.imageRes)

        cancelBtn.setOnClickListener {
            dismiss()
        }

        deleteBtn.setOnClickListener {
            (targetFragment as? FileDeleteConfirmedListener)?.onDeleteFileConfirmed(item.id)
            dismiss()
        }
    }


    override fun onResume() {
        // Sets the height and the width of the DialogFragment
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog!!.window!!.setLayout(width, height)

        super.onResume()
    }
}

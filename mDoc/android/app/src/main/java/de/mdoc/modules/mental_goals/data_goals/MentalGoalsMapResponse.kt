package de.mdoc.modules.mental_goals.data_goals

data class MentalGoalsMapResponse(
        val data:Map<String,ArrayList<MentalGoalsData>>)

package de.mdoc.modules.vitals

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.data.create_bt_device.*
import de.mdoc.data.requests.CreateDeviceRequest
import de.mdoc.data.responses.DevicesResponse
import de.mdoc.data.responses.SingleDeviceResponse
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.devices.data.DevicesForUserRequest
import de.mdoc.modules.vitals.FhirConfigurationUtil.getComponentByCodeAndIndex
import de.mdoc.modules.vitals.FhirConfigurationUtil.getImageByCode
import de.mdoc.modules.vitals.FhirConfigurationUtil.minMaxValues
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_manual_entry.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class ManualEntryFragment : MdocFragment() {
    lateinit var activity: MdocActivity
    var deviceId:String?=null
    private var metricCode:String?=null
    val calendar:Calendar= Calendar.getInstance()
    private var selectedDate=calendar.timeInMillis
    val hh = calendar.get(Calendar.HOUR_OF_DAY)
    val mm = calendar.get(Calendar.MINUTE)
    var isInvalidFormat = false

    private val args: ManualEntryFragmentArgs by navArgs()

    private val manualEntryViewModel by viewModel {
        ManualEntryViewModel(
                RestClient.getService())
    }

    override fun setResourceId(): Int {
        return R.layout.fragment_manual_entry
    }

    override val navigationItem: NavigationItem = NavigationItem.Vitals

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        metricCode = args.measurement.measure?.code
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getMedicalDevicesForUser()
        setOnClickListeners()
        setupUi()
    }

    private fun setupUi() {
        drawImage()
        if(isComposite()){
            args.measurement.components?.forEach { item ->
                addMeasurementPlaceholder(item.measurePrompt())
            }
            txt_add_unit_label?.text = args.measurement.getFirstComponentDisplayMeasure()
            txt_when_did_you_measure?.text=args.measurement.getFirstComponentTime()
        }else {
            addMeasurementPlaceholder(args.measurement.measurePrompt())
            txt_add_unit_label?.text=args.measurement.displayMeasurement()
            txt_when_did_you_measure?.text=args.measurement.measureTimesPrompt()
        }

        txt_date_picker?.setText(MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT,selectedDate))
        txt_time_picker?.setText(MdocUtil.getTimeFromMs(selectedDate,MdocConstants.HOUR_AND_MINUTES))
    }

    @SuppressLint("InflateParams")
    private fun addMeasurementPlaceholder(textLabel:String){
        val childView =
                LayoutInflater.from(context).inflate(R.layout.placeholder_manual_measure, null)
        val txtMetric=childView.findViewById<TextView>(R.id.txt_metric_unit)
        txtMetric.text=textLabel
        measurements_placeholder.addView(childView)
    }

    private fun drawImage(){
        val decodedString = Base64.decode(
                getImageByCode(metricCode?:""), Base64.DEFAULT
        )
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        ic_measurement?.setImageBitmap(decodedByte)
        ic_measurement?.setColorFilter(
                ContextCompat.getColor(activity, R.color.blue_68D6C9), android.graphics.PorterDuff.Mode.SRC_IN)
    }

    private fun setOnClickListeners() {
        txt_cancel.setOnClickListener {
            findNavController().popBackStack()
        }
        txt_save.setOnClickListener {
            validateEntry()
        }
        txt_date_picker?.setOnClickListener {
            showDatePicker()
        }
        txt_time_picker.setOnClickListener {
            showTimePicker()
        }
    }
    private fun validateEntry(){
        val pair= minMaxValues(metricCode)

        if (parsedUserInput().isNullOrEmpty() && isInvalidFormat) {
            showValidationMessage(pair.first.toString(), pair.second.toString())

            isInvalidFormat = false
            return
        }
        if(filledEverything()){
            if(isComposite()){
                complexRangeCheck(parsedUserInput())
            }else{
                if(isInRange(parsedUserInput())) {
                    val observations = createManualObservations(parsedUserInput())
                    postObservationsApi(observations)
                }else{
                    showValidationMessage(pair.first.toString(), pair.second.toString())
                }
            }

        }else{
            MdocUtil.showToastLong(context,resources.getString(R.string.fill_all_fields))
        }
    }

    private fun showValidationMessage(min: String, max: String) {
        MdocUtil.showToastLong(
                context,
                resources.getString(
                        R.string.fill_data_that_is_between_range,
                        min,
                        max))
    }

    private fun complexRangeCheck(userInput: ArrayList<Double>?): Boolean {
        userInput?.forEachIndexed {
            index, element ->
            val component=getComponentByCodeAndIndex(metricCode,index)
            val min=component?.min?.toInt()!!
            val max=component.max?.toInt()!!
            if(element in min..max){

            }else{
                showValidationMessage(min.toString(), max.toString())
                return false
            }
        }
        val observations = createManualObservations(parsedUserInput())
        postObservationsApi(observations)
        return true
    }

    private fun filledEverything():Boolean{
        return !parsedUserInput().isNullOrEmpty()
    }

    private fun isInRange(input: ArrayList<Double>?): Boolean {
        val pair= minMaxValues(metricCode)

        return (input?.min()!! >= pair.first && input.max()!! <= pair.second)
    }

    private fun parsedUserInput(): ArrayList<Double>? {
        val userInputs= ArrayList<Double>()
        val itemCount=measurements_placeholder.childCount
        for (position in 0..itemCount){
            val itemPlaceholder=measurements_placeholder.getChildAt(position)

            if(itemPlaceholder is LinearLayout){
                for(innerPosition in 0..itemPlaceholder.childCount){
                    var editText:EditText?
                    if( itemPlaceholder.getChildAt(innerPosition)is EditText){
                        editText=itemPlaceholder.getChildAt(innerPosition)as EditText
                        if(editText.text.toString().isEmpty()){
                            return ArrayList()

                        }else {
                            try {
                                val value: String = editText.text.toString()
                                userInputs.add(value.toDouble())
                            } catch (e: NumberFormatException) {
                                userInputs.add(-1.0)
                                isInvalidFormat = true
                            }
                        }
                    }
                }
            }
        }
        return userInputs
    }

    private fun createManualObservations(input: ArrayList<Double>?): ArrayList<Observation> {
        val userId= MdocAppHelper.getInstance().userId
        val performerOne= Performer("user/$userId")
        val observation= Observation()
        observation.note= et_comment?.text.toString()


        if(isComposite()){
            val componentList= ArrayList<Component>()
            input?.forEachIndexed {
                index, value->
                val component = Component(
                        code = Code(listOf(Coding(
                                code = args.measurement.components?.getOrNull(index)?.measure?.code,
                                display = null,
                                system = "http://loinc.org"))),

                        valueQuantity = ValueQuantity(
                                code = args.measurement.components?.getOrNull(index)?.unit?.code?:"",
                                system = "http://unitsofmeasure.org",
                                unit = args.measurement.components?.getOrNull(index)?.unit?.display?:"",
                                value = value.toDouble()
                        ))
                componentList.add(component)
            }

            with(observation) {
                code = Code(listOf(Coding(
                        code = metricCode,
                        display = null,
                        system = "http://loinc.org"
                )))
                device = Device("device/$deviceId")
                performer = listOf(performerOne)
                status = "FINAL"
                subject = Subject("user/$userId")
                effectiveDateTime = selectedDate.toString()
                component= componentList
            }
        }else {
            with(observation) {
                code = Code(listOf(Coding(
                        code = metricCode,
                        display = null,
                        system = "http://loinc.org"
                )))
                device = Device("device/$deviceId")
                performer = listOf(performerOne)
                status = "FINAL"
                subject = Subject("user/$userId")
                valueQuantity = ValueQuantity(
                        code= args.measurement.unit?.code?:"",
                        system = "http://unitsofmeasure.org",
                        unit = args.measurement.unit?.display?:"",
                    value = input?.getOrNull(0)?.toDouble() ?: 0.0
                )
                effectiveDateTime = selectedDate.toString()
            }
        }

        return arrayListOf(observation)
    }

    private fun postObservationsApi(observations: ArrayList<Observation>) {
        MdocManager.createObservations(observations, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    findNavController().popBackStack(R.id.fragmentVitals, false)
                }
                else{
                    MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))

                }
            }
            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))

            }
        })
    }

    private fun getMedicalDevicesForUser() {
        val request = DevicesForUserRequest()
        request.model = "MANUAL"

        MdocManager.getDevicesForUser(request, object : Callback<DevicesResponse> {
            override fun onResponse(call: Call<DevicesResponse>, response: Response<DevicesResponse>) {
                if (response.isSuccessful) {
                    deviceId= response.body()!!.getManualDeviceId()

                    if(deviceId==null){

                        createManualDeviceApi()
                    }
                }else{
                    MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))

                }
            }

            override fun onFailure(call: Call<DevicesResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))

            }
        })
    }

    private fun createManualDeviceApi() {

        val request = CreateDeviceRequest()
        val identifier= Identifier("OFFICIAL", FirebaseInstanceId.getInstance().id)

        request.identifier= listOf(identifier)
        request.model="MANUAL"

        MdocManager.createDevice(request, object : Callback<SingleDeviceResponse> {
            override fun onResponse(call: Call<SingleDeviceResponse>, response: Response<SingleDeviceResponse>) {
                if (response.isSuccessful) {
                    deviceId=response.body()!!.getManualDeviceId()

                } else {
                    Timber.w("createDevice ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<SingleDeviceResponse>, t: Throwable) {
                Timber.w(t, "createDevice")
            }
        })
    }

    private fun isComposite():Boolean{
        return args.measurement.components?.isNotEmpty()?:false
    }

    private fun showDatePicker(){
        manualEntryViewModel.myStayData.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            val value = it?.value
            val currentCalendar: Calendar = Calendar.getInstance()

            val mYear = calendar.get(Calendar.YEAR)
            val mMonth = calendar.get(Calendar.MONTH)
            val mDay = calendar.get(Calendar.DAY_OF_MONTH)

            val datePickerDialog =
                    DatePickerDialog(activity, { _, year, month, dayOfMonth ->
                        calendar.set(Calendar.YEAR, year)
                        calendar.set(Calendar.MONTH, month)
                        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                        if(calendar.timeInMillis>currentCalendar.timeInMillis){
                            calendar.set(Calendar.YEAR,currentCalendar.get(Calendar.YEAR))
                            calendar.set(Calendar.MONTH,currentCalendar.get(Calendar.MONTH))
                            calendar.set(Calendar.DAY_OF_MONTH,currentCalendar.get(Calendar.DAY_OF_MONTH))
                        }
                        selectedDate=calendar.timeInMillis
                        txt_date_picker?.setText(MdocUtil.getParsedDate(MdocConstants.DAY_MONTH_DOT_FORMAT, calendar))

                    }, mYear, mMonth, mDay)
            if (value?.checkIn != null) {
                datePickerDialog.datePicker.minDate= value?.checkIn
            }
            datePickerDialog.datePicker.maxDate= Calendar.getInstance().timeInMillis
            datePickerDialog.show()
        })
    }

    private fun showTimePicker(){
        val currentCalendar: Calendar = Calendar.getInstance()
        val timePickerDialog = TimePickerDialog(
                activity, TimePickerDialog.OnTimeSetListener
        { _, hourOfDay, minute ->
            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
            calendar.set(Calendar.MINUTE, minute)

            if(calendar.timeInMillis>currentCalendar.timeInMillis) {
                calendar.set(Calendar.HOUR_OF_DAY, currentCalendar.get(Calendar.HOUR_OF_DAY))
                calendar.set(Calendar.MINUTE, currentCalendar.get(Calendar.MINUTE))
            }

            selectedDate=calendar.timeInMillis
            txt_time_picker?.setText(MdocUtil.getTimeFromMs(selectedDate,MdocConstants.HOUR_AND_MINUTES))
        },hh,mm,true)
        timePickerDialog.show()
    }
}
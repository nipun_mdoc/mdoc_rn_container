package de.mdoc.modules.meal_plan;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapter;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapterTabletFuture;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.VoteRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.VoteResponse;
import de.mdoc.pojo.MealOffer;
import de.mdoc.pojo.MealPlan;
import de.mdoc.util.BandwidthCheck;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 11/3/17.
 */

public class MealPlanFragmentNew extends MealUtil {

    @BindView(R.id.monthAndDateTv)
    TextView monthAndDateTv;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.youSelectedTv)
    TextView youSelectedTv;
    @BindView(R.id.showOrderBtn)
    Button showOrderBtn;
    @BindView(R.id.txtHelloName)
    TextView txtHelloName;

    MealOfferAdapter adapter;

    MdocActivity activity;

    private int dayIndex = 0;
    private int todayIndex = -1;
    MealOfferAdapterTabletFuture adapterFuture;


    @Override
    protected int setResourceId() {
        return R.layout.fragment_mealplan_new;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        progressDialog = activity.getParentProgressDialog();
        if (progressDialog != null) {
            progressDialog.setCancelable(false);
        }
        if(!getResources().getBoolean(R.bool.phone)) {
            if(savedInstanceState == null){
                progressDialog.show();
                getMealPlan();
            }
        }else{
            String patientName =  MdocAppHelper.getInstance().getUserFirstName() + " " + MdocAppHelper.getInstance().getUserLastName();
            txtHelloName.setText(getResources().getString(R.string.hello) + " " + patientName + ",");
            getMealInfo();

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BandwidthCheck.INSTANCE.checkBandwidth(getResources().getString(R.string.err_bandwidth),view);

    }

    @Override
    void setData(){
        for(int i=0; i<items.size(); i++){
            if(MdocUtil.isToday(items.get(i).getDateNumber())){
                todayIndex = i;
                dayIndex = i;
                monthAndDateTv.setText(getDate(items.get(i).getDateNumber()));
                setLayout();
            }
        }
    }

    private String getDate(long timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    @OnClick(R.id.leftDayIv)
    public void onLeftMonthIvClick() {
        if (dayIndex > 0) {
            dayIndex--;
            monthAndDateTv.setText(getDate(items.get(dayIndex).getDateNumber()));
            setLayout();
        }
    }

    @OnClick(R.id.rightDayIv)
    public void onRightMonthIvClick() {

        if (dayIndex < items.size() - 1) {
            dayIndex++;
            monthAndDateTv.setText(getDate(items.get(dayIndex).getDateNumber()));
            setLayout();
        }
    }

    private void setLayout(){
        container.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(activity);
        View today= inflater.inflate(R.layout.layout_cant_choose_today, null, false);
        View future= inflater.inflate(R.layout.layout_meal_future, null, false);
        View past= inflater.inflate(R.layout.layout_meal_past, null, false);

        MealPlan item = items.get(dayIndex);

        if(MdocUtil.isPast(items.get(dayIndex).getDateNumber())){
            container.addView(past);
            setPastLayout(getSelectedOffer(item.getOffers()));
        }else if(MdocUtil.isToday(items.get(dayIndex).getDateNumber())){
            container.addView(today);
            setTodayLayout(item);
        }else {
            container.addView(future);
            setFutureLayout();
        }
    }

    private void setTodayLayout(MealPlan item) {
        MealOffer selected = getSelectedOffer(item.getOffers());
        if(selected == null){
            showOrderBtn.setVisibility(View.INVISIBLE);
            youSelectedTv.setText(getString(R.string.you_no_select_meal));
        }else{
            showOrderBtn.setVisibility(View.VISIBLE);
            youSelectedTv.setText(getString(R.string.for_today_you_selected) + " " + selected.getDescription());
        }

        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        adapterFuture = null;
        adapterFuture = new MealOfferAdapterTabletFuture(item.getOffers(), activity, item.getMealPlanId(), item.getMealChoice() != null ? item.getMealChoice().getMealChoiceId() : "");

        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        mealOfferRv.setAdapter(adapterFuture);
    }

    private MealOffer getSelectedOffer(ArrayList<MealOffer> offers){
        for(MealOffer mo: offers){
            if(mo.isSelected()){
                return mo;
            }
        }
        return null;
    }

    private void setFutureLayout() {
        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers(); //getTomorrowsMealOffer(thisWeek.getMealPlanOld().getWeeklyMealOffer(), nextWeek.getMealPlanOld().getWeeklyMealOffer());

        MealOfferAdapter.previousItem = null;
        adapter = null;
        adapter = new MealOfferAdapter(tommorow, activity, items.get(dayIndex).getMealPlanId(), items.get(dayIndex).getMealChoice() != null ? items.get(dayIndex).getMealChoice().getMealChoiceId() : "");
        mealOfferRv.setAdapter(adapter);
    }

    @OnClick(R.id.showOrderBtn)
    public void onShowOrderClick(){
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.menu_popup);

        TextView menuNameTv = dialog.findViewById(R.id.menuNameTv);
        TextView mealDesTv = dialog.findViewById(R.id.mealDesTv);
        TextView mealTypeTv = dialog.findViewById(R.id.mealTypeTv);

        MealPlan todayItem = items.get(todayIndex);
        MealOffer selected = getSelectedOffer(todayItem.getOffers());
        menuNameTv.setText(selected.getName(getResources()));
        mealDesTv.setText(selected.getDescription());

        String type = "";

        if(selected.getOptions().contains(MdocConstants.GLUTEN_FREE)){
            type += " " + getString(R.string.gluten_free);
        }else if(selected.getOptions().contains(MdocConstants.VEGETARIAN)){
            type += " " + getString(R.string.vegetarian);
        }else if(selected.getOptions().contains(MdocConstants.PIG)){
            type += " " + getString(R.string.pig);
        }else if(selected.getOptions().contains(MdocConstants.CHICKEN)){
            type += " " + getString(R.string.chicken);
        }else if(selected.getOptions().contains(MdocConstants.COW)){
            type += " " + getString(R.string.cow);
        }else if(selected.getOptions().contains(MdocConstants.FISH)){
            type += " " + getString(R.string.fish);
        }

        mealTypeTv.setText(type);

        dialog.show();
    }

    private void setPastLayout(MealOffer selected) {

        TextView voteMealTv = container.findViewById(R.id.voteMealTv);
        Button confirmBtn = container.findViewById(R.id.confirmBtn);
        voteEdt = container.findViewById(R.id.voteEdt);

        if(selected != null) {

            absoluteIv = container.findViewById(R.id.absoluteIv);
            yesIv = container.findViewById(R.id.yesIv);
            slightlyYesIv = container.findViewById(R.id.slightlyYesIv);
            mediumIv = container.findViewById(R.id.mediumIv);
            noIv = container.findViewById(R.id.noIv);

            absoluteIv.setTag(MdocConstants.NOT_SELECTED);
            yesIv.setTag(MdocConstants.NOT_SELECTED);
            slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
            mediumIv.setTag(MdocConstants.NOT_SELECTED);
            noIv.setTag(MdocConstants.NOT_SELECTED);

            voteMealTv.setText(getSelectedOffer(items.get(dayIndex).getOffers()).getDescription());

            absoluteIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onAbsoluteIvClick();
                }
            });

            yesIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onYesIvClick();
                }
            });

            slightlyYesIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onSlightlYesIvClick();
                }
            });

            mediumIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onMediumIvClick();
                }
            });

            noIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onNoIvClick();
                }
            });

            confirmBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onConfirmBtnClick();
                }
            });

            setRated();
        }else{
            confirmBtn.setEnabled(false);
            voteEdt.setEnabled(false);
            voteEdt.setFocusable(false);
            voteEdt.setFocusableInTouchMode(false);
            confirmBtn.setVisibility(View.GONE);
        }
    }

    public void onConfirmBtnClick() {

        if (getReview() != null) {

            VoteRequest request = new VoteRequest();
            request.setCaseId(MdocAppHelper.getInstance().getCaseId());
            request.setDescription(voteEdt.getText().toString());
            request.setMealId(getSelectedOfferId(items.get(dayIndex).getOffers()));
            request.setMealPlanId(items.get(dayIndex).getMealPlanId());
            request.setReview(getReview());

            if(items.get(dayIndex).getMealChoice().getMealChoiceId() == null || items.get(dayIndex).getMealChoice().getMealChoiceId().isEmpty()) {
                MdocManager.vote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if (isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));
                                items.get(dayIndex).getMealChoice().setReview(getReview());
                                items.get(dayIndex).getMealChoice().setDescription(voteEdt.getText().toString());
                                items.get(dayIndex).getMealChoice().setMealId(getSelectedOfferId(items.get(dayIndex).getOffers()));
                            } else {
                                Timber.w("vote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                        Timber.w(throwable, "vote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), throwable.getMessage());
                        }
                    }
                });
            } else {
                request.setMealChoiceId(items.get(dayIndex).getMealChoice().getMealChoiceId());
                MdocManager.updateVote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if (isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));
                                items.get(dayIndex).getMealChoice().setReview(getReview());
                                items.get(dayIndex).getMealChoice().setDescription(voteEdt.getText().toString());
                                items.get(dayIndex).getMealChoice().setMealId(getSelectedOfferId(items.get(dayIndex).getOffers()));
                            } else {
                                Timber.w("updateVote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable throwable) {
                        Timber.w(throwable, "updateVote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), throwable.getMessage());
                        }
                    }
                });
            }
        }
    }

    private String getSelectedOfferId(ArrayList<MealOffer> offers){
        for (MealOffer o : offers){
            if(o.isSelected()){
                return o.get_id();
            }
        }
        return offers.get(0).get_id();
    }

    private void setRated(){
        if(items.get(dayIndex).getMealChoice() != null && items.get(dayIndex).getMealChoice().getReview() != null && !items.get(dayIndex).getMealChoice().getReview().isEmpty()){
            switch (items.get(dayIndex).getMealChoice().getReview()){
                case MdocConstants.POOR:
                    mediumIv.setTag(MdocConstants.SELECTED);
                    break;
                case MdocConstants.NORMAL:
                    slightlyYesIv.setTag(MdocConstants.SELECTED);
                    break;
                case MdocConstants.EXCELLENT:
                    yesIv.setTag(MdocConstants.SELECTED);
                    break;
                case MdocConstants.VERY_POOR:
                    noIv.setTag(MdocConstants.SELECTED);
                    break;
                case MdocConstants.ABSOLUTE:
                    absoluteIv.setTag(MdocConstants.SELECTED);
                    break;
            }

            if(items.get(dayIndex).getMealChoice().getDescription() != null && !items.get(dayIndex).getMealChoice().getDescription().isEmpty()){
                voteEdt.setText(items.get(dayIndex).getMealChoice().getDescription());
            }
        }



        setSelection();
    }

}

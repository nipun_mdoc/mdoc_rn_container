package de.mdoc.modules.questionnaire

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields

class QuestionnaireTransitionFragment: MdocFragment() {

    lateinit var activity: MdocActivity
    private var pollId: String? = null
    private var pollAssignId: String? = null
    private var skipIntro: Boolean = false
    private val args: QuestionnaireTransitionFragmentArgs by navArgs()
    override fun setResourceId(): Int {
        return R.layout.fragment_blank
    }

    override val navigationItem: NavigationItem = NavigationItem.Questionnaire

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        pollId = args.pollId
        pollAssignId = args.pollAssignId
        skipIntro = args.skipIntro

        decideFlow()
    }

    private fun decideFlow() {
        if (skipIntro) {
            val additionalFields = QuestionnaireAdditionalFields(pollId = pollId, pollAssignId = pollAssignId)
            val action = MainNavDirections.globalActionToQuestion(
                            additionalFields)
            findNavController().navigate(action)
        }
        else {
            val action =
                    QuestionnaireTransitionFragmentDirections.actionQuestionnaireTransitionFragmentToIntroQuestionnaireFragment(
                            pollId, pollAssignId)
            findNavController().navigate(action)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
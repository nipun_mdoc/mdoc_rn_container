package de.mdoc.modules.my_stay.data

import de.mdoc.network.request.SearchClinicRequest
import de.mdoc.network.response.CurrentClinicResponse
import de.mdoc.network.response.SearchClinicResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.pojo.ClinicDetails
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.service.IMdocService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class RxStayRepository(
        val mdocService: IMdocService
) {

    private val disposables = CompositeDisposable()

    fun searchMyStay(
            request: String?,
            resultListener:(List<ClinicDetails>)->Unit,
            errorListener:(Throwable)->Unit
    ){
        mdocService.searchClinic(SearchClinicRequest(request)).enqueue(object :Callback<SearchClinicResponse>{
            override fun onResponse(
                    call: Call<SearchClinicResponse>,
                    response: Response<SearchClinicResponse>
            ){
                val result = response.body()
                if (result != null) {
                    parseSearchData(result, resultListener, errorListener)
                } else {
                    Timber.w("searchClinic ${response.getErrorDetails()}")
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<SearchClinicResponse>, t: Throwable) {
                Timber.w(t, "searchClinic")
                errorListener.invoke(t)            }
        })

    }

    private fun parseSearchData(
            response: SearchClinicResponse,
            resultListener: (List<ClinicDetails>) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {
        Single.just(response).map(::mapSearchResponse)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resultListener, errorListener)
                .addTo(disposables)
    }

    private fun mapSearchResponse(response: SearchClinicResponse): List<ClinicDetails> {

        return response.data
    }


    fun recycle() {
        disposables.dispose()
    }
    fun getCurrentClinic(
            resultListener: (CurrentClinicData) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {
        mdocService.currentClinic.enqueue(object : Callback<CurrentClinicResponse> {
            override fun onResponse(
                    call: Call<CurrentClinicResponse>,
                    response: Response<CurrentClinicResponse>
            ) {
                val result = response.body()
                if (result != null) {
                    parseCurrentClinicData(result, resultListener, errorListener)
                } else {
                    Timber.w("currentClinic ${response.getErrorDetails()}")
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                }
            }

            override fun onFailure(call: Call<CurrentClinicResponse>, t: Throwable) {
                Timber.w(t, "currentClinic")
                errorListener.invoke(t)
            }
        })
    }

    private fun parseCurrentClinicData(
            response: CurrentClinicResponse,
            resultListener: (CurrentClinicData) -> Unit,
            errorListener: (Throwable) -> Unit
    ) {
        Single.just(response).map(::mapCurrentClinicData)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(resultListener, errorListener)
                .addTo(disposables)
    }

    private fun mapCurrentClinicData(response: CurrentClinicResponse): CurrentClinicData {
        return response.data
    }
}
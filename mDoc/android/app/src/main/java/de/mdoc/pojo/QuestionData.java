package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by ema on 12/29/16.
 */

public class QuestionData implements Serializable {

    private Map<String,String> question;
    private Boolean showDescription;
    private Map<String,String> description;
    private ArrayList<Option> options;
    private Map<String,String> scaleBeginningText;
    private Map<String,String> scaleEndText;
    private boolean showCondition;
    private boolean showPoints;
    private float scoringScaleMin;
    private float scoringScaleMax;
    private String scoringScaleOrientation;
    private ArrayList<ExtendedQuestion> matrixQuestions;

    public Map<String, String> getQuestion() {
        return question;
    }

    public void setQuestion(Map<String, String> question) {
        this.question = question;
    }

    public Map<String, String> getDescription() {
        return description;
    }

    public void setDescription(Map<String, String> description) {
        this.description = description;
    }


    public ArrayList<ExtendedQuestion> getMatrixQuestions() {
        return matrixQuestions;
    }

    public void setMatrixQuestions(ArrayList<ExtendedQuestion> matrixQuestions) {
        this.matrixQuestions = matrixQuestions;
    }

    public String getScoringScaleOrientation() {
        return scoringScaleOrientation;
    }

    public void setScoringScaleOrientation(String scoringScaleOrientation) {
        this.scoringScaleOrientation = scoringScaleOrientation;
    }

    public float getScoringScaleMin() {
        return scoringScaleMin;
    }

    public void setScoringScaleMin(float scoringScaleMin) {
        this.scoringScaleMin = scoringScaleMin;
    }

    public float getScoringScaleMax() {
        return scoringScaleMax;
    }

    public void setScoringScaleMax(float scoringScaleMax) {
        this.scoringScaleMax = scoringScaleMax;
    }

    public String getQuestion(String language) {
        return question.get(language);
    }

    public Boolean getShowDescription() {
        return showDescription;
    }

    public void setShowDescription(Boolean showDescription) {
        this.showDescription = showDescription;
    }

    public String getDescription(String language) {
        return description.get(language);
    }

    public ArrayList<Option> getOptions() {
        return options;
    }

    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

    protected String getScaleBeginningText(String language) {
        return scaleBeginningText.get(language);
    }


    protected String getScaleEndText(String language) {
        return scaleEndText.get(language);
    }

    public boolean isShowCondition() {
        return showCondition;
    }

    public void setShowCondition(boolean showCondition) {
        this.showCondition = showCondition;
    }

    public boolean isShowPoints() {
        return showPoints;
    }

    public void setShowPoints(boolean showPoints) {
        this.showPoints = showPoints;
    }
}

package de.mdoc.modules.media.media_view_model

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.data.media.MediaStatisticsData
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class MediaStatisticsViewModel(private val mDocService: IMdocService
                              ): ViewModel() {

    var isLoading = MutableLiveData(false)
    val mediaStatistics = MutableLiveData<List<MediaStatisticsData>>()

    init {
        getMediaStatistics()
    }

    private fun getMediaStatistics() {
        viewModelScope.launch {
            try {
                isLoading.value = true

                getMediaStatisticsAsync()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
            }
            isLoading.value = false
        }
    }

    private suspend fun getMediaStatisticsAsync() {
        mediaStatistics.value = mDocService.getMediaStatistics()
            .data.list.filter { !it.content.isNullOrEmpty() }
    }
}
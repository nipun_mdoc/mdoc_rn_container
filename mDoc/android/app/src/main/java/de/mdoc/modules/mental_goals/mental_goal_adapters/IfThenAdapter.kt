package de.mdoc.modules.mental_goals.mental_goal_adapters

import android.app.Activity
import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import kotlinx.android.synthetic.main.placeholder_if_then.view.*


class IfThenAdapter(var context: Context, var items:ArrayList<IfThenPlan>,val isEditing:Boolean) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        context = parent.context
        val view:View = if (isEditing){
            LayoutInflater.from(parent.context).inflate(R.layout.placeholder_edit_if_then, parent, false)
        }else{
            LayoutInflater.from(parent.context).inflate(R.layout.placeholder_if_then, parent, false)
        }

        return CustomViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item=items[position]
        val planNumber = String.format("%02d", position+1)

        holder.txtWhen.imeOptions = EditorInfo.IME_ACTION_DONE
        holder.txtWhen.setRawInputType(InputType.TYPE_CLASS_TEXT)

        holder.txtThen.imeOptions = EditorInfo.IME_ACTION_DONE
        holder.txtThen.setRawInputType(InputType.TYPE_CLASS_TEXT)

        holder.txtPlanNumber.text = context.resources.getString(R.string.plan,planNumber)
        if(item.ifValue.isNotEmpty()) {
            holder.txtWhen.setText(item.ifValue)
        }else{}
        if(item.thenValue.isNotEmpty()) {
            holder.txtThen.setText(item.thenValue)
        }else{}


        holder.txtWhen.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                item.ifValue=holder.txtWhen.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })
        holder.txtWhen.setOnEditorActionListener { _, actionId, _ ->
            if(actionId== EditorInfo.IME_ACTION_DONE){
                holder.txtThen.requestFocus()
            }
            true
        }

        holder.txtThen.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                item.thenValue=holder.txtThen.text.toString()
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        holder.txtThen.setOnEditorActionListener { _, actionId, _ ->
            if(actionId== EditorInfo.IME_ACTION_DONE){
                MentalGoalsUtil.hideKeyboard(context as Activity)
            }
            true
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    var txtWhen:EditText=itemView.txt_if
    var txtThen:EditText=itemView.txt_then
    var txtPlanNumber:TextView=itemView.txt_plan_number
}
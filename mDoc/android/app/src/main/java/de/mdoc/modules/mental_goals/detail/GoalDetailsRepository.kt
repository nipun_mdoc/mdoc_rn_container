package de.mdoc.modules.mental_goals.detail

import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.modules.mental_goals.data_goals.IfThenPlan
import de.mdoc.modules.mental_goals.data_goals.MentalGoalStatus

class GoalDetailsRepository {

    fun isGoalEditable(): Boolean {
        return MentalGoalsUtil.status == MentalGoalStatus.OPEN.toString()
    }

    fun getTitle(): String {
        return MentalGoalsUtil.goalTitle
    }

    fun getDescription(): String {
        return MentalGoalsUtil.goalDescription
    }

    fun getTimePeriod(): Long {
        return MentalGoalsUtil.timePeriod
    }

    fun getActionItems(): List<ActionItem> {
        return MentalGoalsUtil.actionItems ?: emptyList()
    }

    fun getIfThenPlans(): List<IfThenPlan> {
        return MentalGoalsUtil.ifThenItems ?: emptyList()
    }

}
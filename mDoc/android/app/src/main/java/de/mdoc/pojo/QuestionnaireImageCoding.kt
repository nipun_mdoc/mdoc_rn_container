package de.mdoc.pojo

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Base64

data class QuestionnaireImageCoding (val imageBase64: String?, val display: String? = "") {

    fun getImage (): Bitmap? {
        val bytes = Base64.decode(imageBase64, Base64.DEFAULT)
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
    }
}
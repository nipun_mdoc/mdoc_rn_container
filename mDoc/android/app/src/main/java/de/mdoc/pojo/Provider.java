package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Provider {

    @SerializedName("synced")
    @Expose
    private Object synced;
    @SerializedName("linked")
    @Expose
    private Boolean linked;
    @SerializedName("device")
    @Expose
    private String device;
    @SerializedName("link_url")
    @Expose
    private String linkUrl;

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public Object getSynced() {
        return synced;
    }

    public void setSynced(Object synced) {
        this.synced = synced;
    }

    public Boolean getLinked() {
        return linked;
    }

    public void setLinked(Boolean linked) {
        this.linked = linked;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}

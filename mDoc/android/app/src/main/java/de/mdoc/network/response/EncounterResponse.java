package de.mdoc.network.response;

import de.mdoc.pojo.EncounterData;

public class EncounterResponse extends MdocResponse {

    private EncounterData data;

    public EncounterData getData() {
        return data;
    }

    public void setData(EncounterData data) {
        this.data = data;
    }
}

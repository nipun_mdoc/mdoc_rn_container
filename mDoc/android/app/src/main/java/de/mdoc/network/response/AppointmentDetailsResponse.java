package de.mdoc.network.response;

import java.util.List;

import de.mdoc.modules.booking.data.AppointmentData;

public class AppointmentDetailsResponse extends MdocResponse {

    private List<AppointmentData> data;

    public List<AppointmentData> getData() {
        return data;
    }

    public void setData(List<AppointmentData> data) {
        this.data = data;
    }
}

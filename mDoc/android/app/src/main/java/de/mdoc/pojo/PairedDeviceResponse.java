package de.mdoc.pojo;

import java.util.ArrayList;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class PairedDeviceResponse extends MdocResponse {
    private ArrayList<PairedDevices> data;

    public ArrayList<PairedDevices> getData() {
        return data;
    }

    public void setData(ArrayList<PairedDevices> data) {
        this.data = data;
    }
}
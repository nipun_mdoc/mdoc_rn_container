package de.mdoc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.pojo.QuestionnaireData;
import de.mdoc.pojo.QuestionnaireProgress;
import de.mdoc.util.MdocUtil;

/**
 * Created by ema on 7/13/17.
 */

public class QuestionnaireExpandableAdapter extends BaseExpandableListAdapter {

    ArrayList<QuestionnaireProgress> listDataHeader;
    Context context;
    private HashMap<QuestionnaireProgress, ArrayList<QuestionnaireData>> listDataChild;

    public QuestionnaireExpandableAdapter(Context context, ArrayList<QuestionnaireProgress> listDataHeader,
                                 HashMap<QuestionnaireProgress, ArrayList<QuestionnaireData>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final QuestionnaireData child = (QuestionnaireData) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.questionnarie_type_item, null);
        }

        TextView txtListChild = convertView
                .findViewById(R.id.questionnarieTypeName);
        TextView progressTv = convertView
                .findViewById(R.id.progressTv);
        TextView txtLastUpdateTitle = convertView.findViewById(R.id.txt_last_update_title);
        TextView txtChildDate = convertView
                .findViewById(R.id.txt_detail);

        progressTv.setText(child.getAnswearsDetails().getCompleted()+"/"+ child.getAnswearsDetails().getTotal());
        txtListChild.setText(child.getName());

        if(child.getAnswearsDetails().getCompleted()!=0) {
            String date = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, child.getLastUpdate());
            String time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, child.getLastUpdate());
            txtChildDate.setText(context.getResources().getString(R.string.history_questionnaire,date,time));
        }else{
            txtLastUpdateTitle.setVisibility(View.INVISIBLE);
            txtChildDate.setVisibility(View.GONE);
        }


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if (listDataChild != null) {
            if (listDataHeader != null)
                return this.listDataChild.get(this.listDataHeader.get(groupPosition))
                        .size();
            else
                return 0;
        } else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        QuestionnaireProgress header = (QuestionnaireProgress) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.questionnaire_header_type_item, null);
        }
//
        TextView lblListHeader = convertView.findViewById(R.id.typeTv);
        ImageView typeImg = convertView.findViewById(R.id.typeIv);
        TextView descTv = convertView.findViewById(R.id.descTv);
        ImageView indicatorIv = convertView.findViewById(R.id.indicatorIv);
        TextView completedTv = convertView.findViewById(R.id.completedTv);

        if(header.getName().equalsIgnoreCase(context.getString(R.string.anamnesis))){
            typeImg.setImageResource(R.drawable.anamnesis);
            descTv.setText(context.getString(R.string.anamnesis_title));
        }else if(header.getName().equalsIgnoreCase(context.getString(R.string.feedback))){
            typeImg.setImageResource(R.drawable.feedback);
            descTv.setText(context.getString(R.string.feedback_title));
        }else if(header.getName().equalsIgnoreCase(context.getString(R.string.scoring))){
            typeImg.setImageResource(R.drawable.scoring);
            descTv.setText(context.getString(R.string.scoring_title));
        }

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(header.getDisplayName());
        indicatorIv.setImageResource(isExpanded ? R.drawable.arrow_up_dark : R.drawable.arrow_down_dark);
        completedTv.setText(header.getStarted() + "/" + header.getCount());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}

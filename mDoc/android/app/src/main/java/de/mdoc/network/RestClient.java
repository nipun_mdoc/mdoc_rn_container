package de.mdoc.network;

import android.content.Context;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mdoc.R;
import de.mdoc.network.auth.TokenAuthenticator;
import de.mdoc.network.certificate.CertificatePinnerBuilderKt;
import de.mdoc.service.IKeycloackService;
import de.mdoc.service.IMdocService;
import de.mdoc.service.IWeatherService;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.CertificatePinner;
import okhttp3.CookieJar;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

/**
 * Created by ema on 4/6/17.
 */

public class RestClient {

    public static Retrofit retrofit, chatRetrofit;
    private static IMdocService service, testService = null;
    private static RestClient instance;
    private static Context context;
    public static Retrofit keycloackRetrofit;
    public static Retrofit authRetrofit;
    public static IMdocService authService = null;
    public static IMdocService authServiceLogin = null;
    private static IKeycloackService keycloackService = null;
    private static IWeatherService weatherService = null;

    public static Retrofit weatherRetrofit;

    public static OkHttpClient okHttpClient=null;

    public static RestClient getInstance() {
        if (null == instance) {
            instance = new RestClient();
        }
        return instance;
    }

    public static Retrofit getRetrofit(){
        return getRetrofitImpl(true);
    }

    public static Retrofit getRetrofitWithoutClinicId(){
        return getRetrofitImpl(false);
    }

    private static Retrofit getRetrofitImpl(boolean shouldAddClinicId){

        okHttpClient = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
            @Override
            public Unit invoke(OkHttpClient.Builder builder) {
                builder.authenticator(new TokenAuthenticator(context, context.getString(R.string.keycloack_base_url)));
                CertificatePinner certificatePinner = CertificatePinnerBuilderKt.buildCertificatePinner(context);
                if (certificatePinner != null) {
                    builder.certificatePinner(certificatePinner);
                }
                ClearableCookieJar cookieJar =
                        new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                builder.cookieJar(cookieJar);
                return null;
            }
        }, shouldAddClinicId);

        GsonBuilder builder = new GsonBuilder();
        Converters.registerDateTime(builder);
        Converters.registerLocalDateTime(builder);
        Converters.registerDateTimeZone(builder);
        Converters.registerInstant(builder);
        Gson gson = builder.setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.base_url)) //context.getString(R.string.base_url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return retrofit;
    }

    public static Retrofit getWeatherRetrofit(){

        OkHttpClient client = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
            @Override
            public Unit invoke(OkHttpClient.Builder builder) {
                ClearableCookieJar cookieJar =
                        new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                builder.cookieJar(cookieJar);
                return null;
            }
        });

        weatherRetrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.weather_base_url)) //context.getString(R.string.base_url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return weatherRetrofit;
    }

    public static Retrofit getChatRetrofit() {
        OkHttpClient client = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
            @Override
            public Unit invoke(OkHttpClient.Builder builder) {
                ClearableCookieJar cookieJar =
                        new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                builder.cookieJar(cookieJar);
                return null;
            }
        });

        chatRetrofit = new Retrofit.Builder()
                .baseUrl("https://dev.sistemasexpertos.cl/mdoc/") //context.getString(R.string.base_url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        return chatRetrofit;
    }

    public static Retrofit getKeycloackRetrofit(){
        try {
            OkHttpClient client = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
                @Override
                public Unit invoke(OkHttpClient.Builder builder) {
                    CookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                    builder.cookieJar(cookieJar);
                    return null;
                }
            });

            keycloackRetrofit = new Retrofit.Builder()
                    .baseUrl(context.getString(R.string.keycloack_base_url)) //context.getString(R.string.base_url)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        } catch (Exception e) {
            Timber.w(e);
        }
        return keycloackRetrofit;
    }

    public static Retrofit getAuthRetrofit(){
        try {
            OkHttpClient client = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
                @Override
                public Unit invoke(OkHttpClient.Builder builder) {
                    CookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                    builder.cookieJar(cookieJar);
                    return null;
                }
            });

            authRetrofit = new Retrofit.Builder()
                    .baseUrl(context.getString(R.string.base_url))
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        } catch (Exception e) {
            Timber.w(e);
        }
        return authRetrofit;
    }
    public static Retrofit getAuthRetrofitForLogin(){
        try {
            OkHttpClient client = OkHttpClientBuilderKt.buildOkHttpClient(new Function1<OkHttpClient.Builder, Unit>() {
                @Override
                public Unit invoke(OkHttpClient.Builder builder) {
                    CookieJar cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(context));
                    builder.cookieJar(cookieJar);
                    return null;
                }
            });

            authRetrofit = new Retrofit.Builder()
                    .baseUrl(context.getString(R.string.base_url).replace("/api/", ""))
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        } catch (Exception e) {
            Timber.w(e);
        }
        return authRetrofit;
    }

    public static IMdocService getChatService(){
        if(testService == null) {
            return testService = getChatRetrofit().create(IMdocService.class);
        }
        return testService;
    }

    public static IMdocService getService(){
        if(service == null) {
            return service = getRetrofit().create(IMdocService.class);
        }
        return service;
    }

    public static IMdocService getServiceWithoutClinicId(){
        return getRetrofitWithoutClinicId().create(IMdocService.class);
    }

    public static IWeatherService getWeatherService(){
        if(weatherService == null) {
            return weatherService = getWeatherRetrofit().create(IWeatherService.class);
        }
        return weatherService;
    }

    public static IKeycloackService getKeycloackService(){
        if(keycloackService == null) {
            return keycloackService = getKeycloackRetrofit().create(IKeycloackService.class);
        }
        return keycloackService;
    }

    public static IMdocService getAuthService(){
        if(authService == null) {
            return authService = getAuthRetrofit().create(IMdocService.class);
        }
        return authService;
    }

    public static IMdocService getAuthServiceLogin(){
        if(authServiceLogin == null) {
            return authServiceLogin = getAuthRetrofitForLogin().create(IMdocService.class);
        }
        return authServiceLogin;
    }

    public void init(Context cont){
        context = cont;
    }

    private static int responseCount(Response response) {
        int result = 1;
        while ((response = response.priorResponse()) != null) {
            result++;
        }
        return result;
    }
}

package de.mdoc.modules.tutorial;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.viewpager.widget.ViewPager;

import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.fragments.MdocBaseFragment;

/**
 * Created by ema on 3/15/18.
 */

public class TutorialNotificationFragment extends MdocBaseFragment {

    ViewPager pager;
    TutorialDialogFragment dialog;

    public TutorialNotificationFragment(){

    }

    @SuppressLint("ValidFragment")
    public TutorialNotificationFragment(TutorialDialogFragment dialog, ViewPager pager){
        this.pager = pager;
        this.dialog = dialog;
    }

    @Override
    protected int setResourceId() {
        return R.layout.tutorial_notification;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

    }

    @OnClick(R.id.nextBtn)
    public void onNextBtnClick(){
        pager.setCurrentItem(4);
    }

    @OnClick(R.id.skipTv)
    public void onSkipTvClick(){
        dialog.dismiss();
    }
}
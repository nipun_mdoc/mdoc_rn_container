package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.Coverage;

public class CoverageResponse extends MdocResponse{

    private ArrayList<Coverage> data;

    public ArrayList<Coverage> getData() {
        return data;
    }

    public void setData(ArrayList<Coverage> data) {
        this.data = data;
    }

    public String getKrankReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isKrank()){
                return c.getReferenceId();
            }
        }
        return "";
    }

    public String getSuplementeryReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null &&c.getType().isSuplementary()){
                return c.getReferenceId();
            }
        }
        return "";
    }

    public String getSuplementeryAccidentReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null &&c.getType().isSuplementaryAccident()){
                return c.getReferenceId();
            }
        }
        return "";
    }


    public String getInvalidReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isInvalid()){
                return c.getReferenceId();
            }
        }
        return "";
    }

    public String getAcidentReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isUnfal()){
                return c.getReferenceId();
            }
        }
        return "";
    }

    public String getMilitarReferenceId(){
        for(Coverage c :data){
            if(c != null && c.getType() != null &&  c.getType().isMilitar()){
                return c.getReferenceId();
            }
        }
        return "";
    }

    public String getKrankIdentifier(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isKrank()){
                return c.getOfficialIdentifier();
            }
        }
        return "";
    }

    public String getSuplementeryIdentifier(){
        for(Coverage c :data){
            if(c != null && c.getType() != null &&  c.getType().isSuplementary()){
                return c.getOfficialIdentifier();
            }
        }
        return "";
    }

    public String getSuplementeryAccidentIdentifier(){
        for(Coverage c :data){
            if(c != null && c.getType() != null &&  c.getType().isSuplementaryAccident()){
                return c.getOfficialIdentifier();
            }
        }
        return "";
    }

    public String getInvalidIdentifier(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isInvalid()){
                return c.getOfficialIdentifier();
            }
        }
        return "";
    }

    public String getAcidentIdentifier(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isUnfal()){
                return c.getOfficialIdentifier();
            }
        }
        return "";
    }


    public String getKrankClass(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isKrank()){
                return c.getType().getKrankClassCode();
            }
        }
        return "";
    }

    public String getAccidentClass(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isUnfal()){
                return c.getType().getAccidentClassCode();
            }
        }
        return "";
    }

    public String getSuplementeryClass(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isSuplementary()){
                return c.getType().getSuplementeryClassCode();
            }
        }
        return "";
    }

    public String getSuplementeryAccidentClass(){
        for(Coverage c :data){
            if(c != null && c.getType() != null && c.getType().isSuplementaryAccident()){
                return c.getType().getSuplementeryClassCode();
            }
        }
        return "";
    }
}

package de.mdoc.modules.meal_plan.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.pojo.MealOffer;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.ProgressDialog;

/**
 * Created by ema on 1/26/18.
 */

public class MealOfferAdapterTabletFuture extends RecyclerView.Adapter<MealOfferAdapterTabletFuture.ViewHolder> {

    ArrayList<MealOffer> items;
    private LayoutInflater inflater;
    Context context;
    ProgressDialog progressDialog;
    String mealChoiceId;
    public static MealOffer previousItem = null;
    boolean fromDashboard = false;

    public MealOfferAdapterTabletFuture(ArrayList<MealOffer> items, Context context, String mealPlanId, String mealChoiceId) {
        this.items = items;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.mealChoiceId = mealChoiceId;
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (context.getResources().getBoolean(R.bool.phone)) {
            if (context.getResources().getBoolean(R.bool.can_user_choose_meal)) {
                view = inflater.inflate(R.layout.meal_offer_item, parent, false);
            } else {
                view = inflater.inflate(R.layout.meal_offer_item_phone, parent, false);
            }
        } else {
            if (fromDashboard) {
                view = inflater.inflate(R.layout.meal_offer_item_phone, parent, false);
            } else {
                view = inflater.inflate(R.layout.meal_offer_item, parent, false);
            }
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MealOffer item = items.get(position);

        if (item.isSoup()) {
            holder.mealNameTv.setText(context.getString(R.string.soup));
        } else {
            holder.mealNameTv.setText(item.getName(context.getResources()));
        }
        holder.mealDescriptionTv.setText(item.getDescription());
        if (item.getCalories() > 0) {
            holder.kcalTv.setVisibility(View.VISIBLE);
            holder.kcalTv.setText("Kcal: " + item.getCalories());
        } else {
            holder.kcalTv.setVisibility(View.GONE);
        }

        if (item.getOptions().contains(MdocConstants.GLUTEN_FREE)) {
            holder.gfIv.setVisibility(View.VISIBLE);
        } else {
            holder.gfIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.VEGETARIAN)) {
            holder.veganIv.setVisibility(View.VISIBLE);
        } else {
            holder.veganIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.PIG)) {
            holder.pigIv.setVisibility(View.VISIBLE);
        } else {
            holder.pigIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.CHICKEN)) {
            holder.chickenIv.setVisibility(View.VISIBLE);
        } else {
            holder.chickenIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.COW)) {
            holder.cowIv.setVisibility(View.VISIBLE);
        } else {
            holder.cowIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.FISH)) {
            holder.fishIv.setVisibility(View.VISIBLE);
        } else {
            holder.fishIv.setVisibility(View.GONE);
        }
        if (item.getOptions().contains(MdocConstants.INFO)) {
            holder.infoIv.setVisibility(View.VISIBLE);
        } else {
            holder.infoIv.setVisibility(View.GONE);
        }
        if(item.isKosher()){
            holder.kosherIv.setVisibility(View.VISIBLE);
        }else{
            holder.kosherIv.setVisibility(View.GONE);
        }

        if (context.getResources().getBoolean(R.bool.can_user_choose_meal)) {
            if (item.isSelected()) {
                holder.rootRl.setBackground(ContextCompat.getDrawable(context, R.drawable.gray_yellow_storke_rr));
            } else {
                holder.rootRl.setBackground(ContextCompat.getDrawable(context, R.drawable.platinum_meal_rr));
            }
        }

        holder.infoIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAditivesDialog(context, item.getInfo());
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.mealNameTv)
        TextView mealNameTv;
        @BindView(R.id.mealDescriptionTv)
        TextView mealDescriptionTv;
        @BindView(R.id.kcalTv)
        TextView kcalTv;


        @BindView(R.id.infoIv)
        ImageView infoIv;
        @BindView(R.id.gfIv)
        ImageView gfIv;
        @BindView(R.id.veganIv)
        ImageView veganIv;
        @BindView(R.id.pigIv)
        ImageView pigIv;
        @BindView(R.id.chickenIv)
        ImageView chickenIv;
        @BindView(R.id.cowIv)
        ImageView cowIv;
        @BindView(R.id.fishIv)
        ImageView fishIv;
        @BindView(R.id.rootRl)
        RelativeLayout rootRl;
        @BindView(R.id.selectedIv)
        ImageView selectedIv;
        @BindView(R.id.kosherIv)
        ImageView kosherIv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }

    }

    private void showAditivesDialog(Context context, ArrayList<String> infoList) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.aditives_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        ImageView closeIv = view.findViewById(R.id.closeIv);
        ListView aditivesLv = view.findViewById(R.id.aditivesLv);
        HashMap<String, String> hashMapAllergens = MdocAppHelper.getInstance().getAllergens();
        HashMap<String, String> hashMapAdditives = MdocAppHelper.getInstance().getAdditives();

        ArrayList<String> showAllergens = new ArrayList<>();

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAdditives.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        for (int j = 0; j < infoList.size(); j++) {
            String value = hashMapAllergens.get(infoList.get(j));
            if (value != null) {
                showAllergens.add(" • " + value);
            }
        }

        ArrayAdapter<String> additivesAdapter =
                new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, showAllergens);

        aditivesLv.setAdapter(additivesAdapter);

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }
}




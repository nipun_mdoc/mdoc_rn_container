package de.mdoc.modules.vitals

import de.mdoc.constants.MdocConstants
import de.mdoc.modules.devices.data.Component
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.modules.devices.data.Measurement
import de.mdoc.storage.AppPersistence.fhirConfigurationData
import de.mdoc.storage.AppPersistence.fhirDevicesData

object FhirConfigurationUtil {

    fun metricLabel(loincCode:String):String?{
        val fhirConfig= fhirConfigurationData

        fhirConfig?.forEach { item ->
            if(item.measure?.code==loincCode){
                return item.unit?.display
            }
        }
        return ""
    }

    fun componentMetricLabel(loincCode:String):String?{
        val fhirConfig= fhirConfigurationData

        fhirConfig?.forEach { item ->
            if(item.components?.isNotEmpty()==true){
                for (index in item.components.indices) {
                    if (item.components.getOrNull(index)?.measure?.code == loincCode) {
                        return item.components.getOrNull(index)?.unit?.display
                    }
                }
            }
        }
        return ""
    }

    fun getImageByCode(code:String):String{
        var thisCode=code
        if (code=="8480-6"||code=="8462-4"){
            thisCode="35094-2"
        }
        val config= fhirConfigurationData
        config?.forEach { item ->
            if(item.measure?.code==thisCode){
                return item.imageCoding?.imageBase64?:""
            }
        }
        return ""
    }

    fun getTitleByCode(code:String):String{
        val config= fhirConfigurationData
        config?.forEach { item ->
            if(item.measure?.code==code){
                return item.imageCoding?.display?:""
            }
        }
        return ""
    }

    fun minMaxValues(code:String?):Pair<Int,Int>{
        var min=0
        var max=0
        val config= fhirConfigurationData

        config?.forEach { item ->
            if(item.measure?.code==code){
                min=item.min?.toInt()?:0
                max=item.max?.toInt()?:100
            }
        }

        return Pair(min,max)
    }

    fun getComponentByCodeAndIndex(code:String?,index:Int):Component?{
        val config= fhirConfigurationData
        config?.forEach { item ->
            if(item.measure?.code==code){
                return item.components?.getOrNull(index)
            }
        }
        return null
    }

    fun measurementByCode(code:String): Measurement {
        val config= fhirConfigurationData

        config?.forEach { item ->
            if(item.measure?.code==code){
                return item
            }
        }
        return Measurement()
    }

    fun getGraphType(code:String):String{
        val measurement=measurementByCode(code)

        return measurement.graphType?:MdocConstants.DEFAULT_GRAPH_TYPE
    }

    fun isCompositeByCode(code:String?):Boolean{
        val config= fhirConfigurationData
        config?.forEach { item ->
            if(item.measure?.code==code){
                return item.components?.isNotEmpty()==true

            }
        }

        return false
    }
    fun codesForComposite(code:String):ArrayList<String>{
        val codes= arrayListOf<String>()
        val config= fhirConfigurationData
        config?.forEach { item ->
            if(item.measure?.code==code){
                item.components?.forEach {
                    codes.add(it.measure?.code?:"")
                }

            }
        }

        return codes
    }
    fun longNameByDeviceModel(deviceName:String?):String{
        val hardwareDevices= fhirDevicesData
        var nameOfDevice = deviceName ?: ""
        if (nameOfDevice.contains("LUNG")) {
            nameOfDevice = "TIO"
        }
        if (nameOfDevice.contains("GL50")) {
            nameOfDevice = "GL50"
        }
        hardwareDevices?.forEach {device->
            if(device.deviceCode==nameOfDevice){
                return device.deviceImage?.display?:""
            }
        }
        return ""
    }

    fun imageByDeviceName(deviceName:String?):String{
        val hardwareDevices= fhirDevicesData
        var nameOfDevice = deviceName

        nameOfDevice?.let {
            if(it.contains("LUNG")){
                nameOfDevice = "TIO"
            }
        }
        nameOfDevice?.let {
            if (it.contains("GL50")) {
                nameOfDevice = "GL50"
            }
        }
        hardwareDevices?.forEach {device->
            if(device.deviceCode==nameOfDevice){
                return device.deviceImage?.imageBase64?:""
            }
        }
        return ""
    }

    fun deviceByDeviceName(deviceName:String?):FhirConfigurationData{
        val hardwareDevices= fhirDevicesData
        var nameOfDevice = deviceName
        nameOfDevice?.let {
            if(it.contains("LUNG")){
                nameOfDevice = "TIO"
            }
        }
        hardwareDevices?.forEach {device->
            if(device.deviceCode==nameOfDevice){
                return device
            }
        }
        return FhirConfigurationData()
    }
}
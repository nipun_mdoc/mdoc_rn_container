package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import de.mdoc.data.create_bt_device.*
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil

class Po60Device(val gatt: BluetoothGatt, val data: ArrayList<Byte>) : BluetoothDevice {

    override fun getDeviceObservations(list: ArrayList<Byte>,deviceId:String,updateFrom: Long?): ArrayList<Observation> {
        val userId= MdocAppHelper.getInstance().userId

        val codingOne= Coding("fitness","Fitness Data","http://hl7.org/fhir/observation-category")
        val codingListOne= listOf(codingOne)
        val category= Category(codingListOne)
        val categoryList= listOf(category)

        val spoCodingTwo= Coding("20564-1","O2 Saturation","http://loinc.org")
        val spoCodingThree= Coding("431314004","O2 Saturation","http://snomed.info/sct")
        val spoCodingListTwo= listOf(spoCodingTwo,spoCodingThree)
        val spoCode= Code(spoCodingListTwo)

        val prCodingTwo= Coding("8867-4","Heart Rate","http://loinc.org")
        val prCodingThree= Coding("78564009","Heart Rate","http://snomed.info/sct")
        val prCodingListTwo= listOf(prCodingTwo,prCodingThree)
        val prCode= Code(prCodingListTwo)

        val performer= Performer("user/$userId")
        val performerList= listOf(performer)
        val subject= Subject("user/$userId")
        val text= Text("<div>[name] : [value] [hunits] @ [date]</div>","GENERATED")

        val observations: java.util.ArrayList<Observation> = arrayListOf()
        var observedItem:Observation

        var year=0
        var month=0
        var day=0
        var hour=0
        var minute=0
        var prAvg: Int
        var dateMs:Long=0
        val readsByGroup =list.chunked(24)

        for(chunk in readsByGroup){

            for((index,byte) in chunk.withIndex()){

                when(index){
                    2->{year=byte.toInt()}
                    3->{month=byte.toInt()}
                    4->{day=byte.toInt()}
                    5->{hour=byte.toInt()}
                    6->{minute=byte.toInt()}
                    7->{
                       val second=byte.toInt()

                        year += 2000
                        dateMs= MdocUtil.getTimeInMS(year,month-1,day,hour,minute,second)
                    }
                    19->{
                        val spoAvg=byte.toInt()
                        observedItem= Observation(categoryList
                                ,spoCode
                                , Device("device/$deviceId")
                                ,dateMs.toString()
                                ,performerList,"Observation"
                                ,"FINAL"
                                ,subject
                                ,text
                                , ValueQuantity("%","http://unitsofmeasure.org","percent",spoAvg.toDouble()), "PO60")

                        if (updateFrom == null) {
                            observations.add(observedItem)
                        } else {
                            if (dateMs > updateFrom) {
                                observations.add(observedItem)
                            }
                        }                    }
                    22->{
                        prAvg=byte.toInt()

                        observedItem= Observation(categoryList
                                ,prCode
                                ,Device("device/$deviceId")
                                ,dateMs.toString()
                                ,performerList,"Observation"
                                ,"FINAL"
                                ,subject
                                ,text
                                , ValueQuantity("/min","http://unitsofmeasure.org","beats/minute",prAvg.toDouble()),
                                "PO60")
                        if (updateFrom == null) {
                            observations.add(observedItem)
                        } else {
                            if (dateMs > updateFrom) {
                                observations.add(observedItem)
                            }
                        }
                    }
                }
            }
        }

        return observations    }

    override fun deviceAddress(): String {
        return gatt.device.address
    }

    override fun deviceData(): java.util.ArrayList<Byte> {
        return data
    }

    override fun deviceName(): String {
        return gatt.device.name
    }
}
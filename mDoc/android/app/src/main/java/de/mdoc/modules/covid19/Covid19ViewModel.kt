package de.mdoc.modules.covid19

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.covid19.CovidStatusChangeBottomSheetFragment.Companion.NOT_TESTED
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class Covid19ViewModel(private val mDocService: IMdocService): ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    var healthStatus: MutableLiveData<HealthStatus> = MutableLiveData(HealthStatus())
    var hasHealthStatus = MutableLiveData(false)
    var allHealthStatus: MutableLiveData<List<HealthStatus>> = MutableLiveData(listOf())
    var isEditEnabled = MutableLiveData(false)

    init {
        getHealthStatus()
    }

    private fun getHealthStatus() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                allHealthStatus.value = mDocService.getHealthStatus(PHYSICIAN).data?.list?.sortedByDescending { it.cts }
                healthStatus.value = allHealthStatus.value?.getOrNull(0)
                hasHealthStatus.value = !healthStatus.value?.healthStatus.isNullOrEmpty()
                isEditEnabled.value = shouldEnableEditing()
                isLoading.value = false
                isShowContent.value = true
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                hasHealthStatus.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
            }
        }
    }

    fun postHealthStatus(request: HealthStatus) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val response = mDocService.postHealthStatus(request).data
                response?.let {
                    getHealthStatus()
                }
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
            }
        }
    }

    private fun shouldEnableEditing(): Boolean {
        return (hasHealthStatus.value == true) && (healthStatus.value?.healthStatus != NOT_TESTED) && (healthStatus.value?.patientUpdateAllowed
                ?: true)
    }

    companion object {
        const val STATUS_CODING = "https://mdoc.one/coding/health-status/status"
        const val ILLNES_CODING = "https://mdoc.one/coding/health-status/illnesType"
        const val PHYSICIAN = "*physician*"
    }
}
package de.mdoc.newlogin.Model


data class LoginResponseData(
    val code: String,
    val data: Data,
    val message: String,
    val timestamp: Long
)

data class Data(
    val accessToken: String,
    val expiresIn: Int,
    val refreshExpiresIn: Int,
    val refreshToken: String,
    val scope: String,
    val tokenType: String,
    val passwordResetToken: String,
    val deviceSecret: String

)
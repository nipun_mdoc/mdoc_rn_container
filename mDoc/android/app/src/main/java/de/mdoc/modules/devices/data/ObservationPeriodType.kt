package de.mdoc.modules.devices.data

enum class ObservationPeriodType{
    MONTH,WEEK,DAY,HOUR,YEAR
}
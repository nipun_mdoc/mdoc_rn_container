package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class PairedDevices  {
    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("friendlyName")
    @Expose
    private String friendlyName;
    @SerializedName("secret")
    @Expose
    private String secret;
    @SerializedName("active")
    @Expose
    private Boolean active;
    @SerializedName("algorithm")
    @Expose
    private String algorithm;
    @SerializedName("issuer")
    @Expose
    private String issuer;
    @SerializedName("period")
    @Expose
    private Integer period;
    @SerializedName("digits")
    @Expose
    private Integer digits;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("limitReached")
    @Expose
    private Boolean limitReached;

    public PairedDevices(String friendlyName, Boolean active, String userId) {
        this.friendlyName = friendlyName;
        this.active = active;
        this.id = userId;
    }

    public Boolean getLimitReached() {
        return limitReached;
    }

    public void setLimitReached(Boolean limitReached) {
        this.limitReached = limitReached;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getAlgorithm() {
        return algorithm;
    }

    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Integer getDigits() {
        return digits;
    }

    public void setDigits(Integer digits) {
        this.digits = digits;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }
}
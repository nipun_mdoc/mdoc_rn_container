package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.pojo.*
import kotlinx.android.synthetic.main.fragment_insurance.*
import javax.annotation.Nullable

class InsuranceFragment: Fragment() {

    lateinit var activity: PatientDetailsActivity
    private var insuranceClassList: ArrayList<CodingItem>? = null
    private var supplementaryInsuranceClassList: ArrayList<CodingItem>? = null
    private var healthInsurances: ArrayList<InsuranceDataKt>? = null  //list from backend
    private var healthInsuranceId: String? = null //autoCompleteText
    private var healthInsuranceCoverage: Coverage? = null //list for spinner from backend
    private var healthClassCode: String? = null  //spinner
    private var supplementaryInsurances: ArrayList<InsuranceDataKt>? = null
    private var supplementaryInsuranceId: String? = null
    private var supplementaryInsuranceCoverage: Coverage? = null
    private var supplementaryClassCode: String? = null
    private var accidentInsurances: ArrayList<InsuranceDataKt>? = null
    private var accidentInsuranceId: String? = null
    private var accidentInsuranceCoverage: Coverage? = null
    private var accidentClassCode: String? = null
    private var supplementaryAccidentInsurances: ArrayList<InsuranceDataKt>? = null
    private var supplementaryAccidentInsuranceId: String? = null
    private var supplementaryAccidentInsuranceCoverage: Coverage? = null
    private var supplementaryAccidentClassCode: String? = null
    private var disabilityInsurances: ArrayList<InsuranceDataKt>? = null
    private var disabilityInsuranceCoverage: Coverage? = null
    private var disabilityInsuranceId: String? = null
    private var militaryInsurances: ArrayList<InsuranceDataKt>? = null
    private var militaryInsuranceCoverage: Coverage? = null
    private var militaryInsuranceId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as PatientDetailsActivity

        return inflater.inflate(R.layout.fragment_insurance, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setNavigationItem(5)

        healthInsurances = activity.patientDetailsViewModel.healthInsurances.value ?: arrayListOf()
        accidentInsurances = activity.patientDetailsViewModel.accidentInsurances.value ?: arrayListOf()
        supplementaryAccidentInsurances =
                activity.patientDetailsViewModel.supplementaryAccidentInsurances.value
                        ?: arrayListOf()
        supplementaryInsurances =
                activity.patientDetailsViewModel.supplementaryInsurances.value ?: arrayListOf()
        militaryInsurances =
                activity.patientDetailsViewModel.militaryInsurances.value ?: arrayListOf()
        disabilityInsurances =
                activity.patientDetailsViewModel.disabilityInsurances.value ?: arrayListOf()

        insuranceClassList = activity.patientDetailsViewModel.insuranceClassList.value ?: arrayListOf()
        supplementaryInsuranceClassList =
                activity.patientDetailsViewModel.supplementaryInsuranceClassList.value
                        ?: arrayListOf()

        previous5Btn.setOnClickListener {
            activity.onBackPressed()
        }

        continue5Btn.setOnClickListener {
            fillInsuranceCoverages()
            showNewFragment(ConfirmationFragment(), R.id.container)
        }

        generateAutoCompleteTextViews()
        generateSpinners()

        fillPreSelection()
    }

    private fun generateAutoCompleteTextViews() {
        activity.generateAutoComplete(ac_healthInsurance,
                healthInsurances!!) {
            healthInsuranceId = setAutoCompleteItemId(it)
        }

        activity.generateAutoComplete(ac_supplementaryInsurance,
                supplementaryInsurances!!) {
            supplementaryInsuranceId = setAutoCompleteItemId(it)
        }

        activity.generateAutoComplete(ac_accidentInsurance,
                accidentInsurances!!) {
            accidentInsuranceId = setAutoCompleteItemId(it)
        }

        activity.generateAutoComplete(ac_supplementaryAccidentInsurance,
                supplementaryAccidentInsurances!!) {
            supplementaryAccidentInsuranceId = setAutoCompleteItemId(it)
        }

        activity.generateAutoComplete(ac_disabilityInsurance,
                disabilityInsurances!!) {
            disabilityInsuranceId = setAutoCompleteItemId(it)
        }

        activity.generateAutoComplete(ac_militaryInsurance,
                militaryInsurances!!) {
            militaryInsuranceId = setAutoCompleteItemId(it)
        }
    }

    private fun generateSpinners() {
        generateSpinner(insuranceClassSpn,
                insuranceClassList!!) { codingItem ->
            healthClassCode = setSpinnerItemId(codingItem)
        }

        generateSpinner(actidentInsuranceClassSpn,
                insuranceClassList!!) { codingItem ->
            accidentClassCode = setSpinnerItemId(codingItem)
        }
        generateSpinner(supplementeryClassSpn,
                supplementaryInsuranceClassList!!) { codingItem ->
            supplementaryClassCode = setSpinnerItemId(codingItem)
        }
        generateSpinner(supplementeryAccidentClassSpn,
                supplementaryInsuranceClassList!!) { codingItem ->
            supplementaryAccidentClassCode = setSpinnerItemId(codingItem)
        }
    }

    private fun fillPreSelection() {
        //fill spinners
        PatientDetailsHelper.getInstance()
            ?.coveragesResponse?.let { coverage ->
            setSpinnerPreSelection(insuranceClassSpn, insuranceClassList, coverage.krankClass)
            setSpinnerPreSelection(actidentInsuranceClassSpn, insuranceClassList, coverage.accidentClass)
            setSpinnerPreSelection(supplementeryClassSpn, supplementaryInsuranceClassList, coverage.suplementeryClass)
            setSpinnerPreSelection(supplementeryAccidentClassSpn, supplementaryInsuranceClassList,
                    coverage.suplementeryAccidentClass)
            //fill autocomplete
            setAutoCompletePreSelection(ac_healthInsurance, healthInsurances, coverage.krankReferenceId)
            setAutoCompletePreSelection(ac_supplementaryInsurance, supplementaryInsurances,
                    coverage.suplementeryReferenceId)
            setAutoCompletePreSelection(ac_accidentInsurance, accidentInsurances, coverage.acidentReferenceId)
            setAutoCompletePreSelection(ac_supplementaryAccidentInsurance, supplementaryAccidentInsurances,
                    coverage.suplementeryAccidentReferenceId)
            setAutoCompletePreSelection(ac_disabilityInsurance, disabilityInsurances, coverage.invalidReferenceId)
            setAutoCompletePreSelection(ac_militaryInsurance, militaryInsurances, coverage.militarReferenceId)
            //fill EditTexts
            coverage.krankIdentifier?.let { healthInsEdt.setText(it) }
            coverage.suplementeryIdentifier?.let { supplementeryInsEdt.setText(it) }
            coverage.invalidIdentifier?.let { disabilityInsEdt.setText(it) }
            coverage.acidentIdentifier?.let { acidentInsPlanIdEdt.setText(it) }
            coverage.suplementeryAccidentIdentifier?.let { supplementeryAccidnetInsEdt.setText(it) }
        }
    }

    private fun setAutoCompletePreSelection(autoComplete: AutoCompleteTextView?,
                                            inputList: ArrayList<InsuranceDataKt>?,
                                            coverage: String?) {
        val index: Int = activity.getPositionOfItem(inputList, coverage)

        if (index > -1) {
            autoComplete?.setText(inputList?.getOrNull(index)?.name, true)
            when (autoComplete) {
                ac_healthInsurance                -> {
                    healthInsuranceId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }

                ac_supplementaryInsurance         -> {
                    supplementaryInsuranceId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }

                ac_supplementaryAccidentInsurance -> {
                    supplementaryAccidentInsuranceId =
                            setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }

                ac_accidentInsurance              -> {
                    accidentInsuranceId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }

                ac_disabilityInsurance            -> {
                    disabilityInsuranceId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }

                ac_militaryInsurance              -> {
                    militaryInsuranceId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }
            }
        }
    }

    private fun setSpinnerPreSelection(spinner: AppCompatSpinner?,
                                       inputList: ArrayList<CodingItem>?,
                                       coverage: String?) {
        inputList?.forEachIndexed { position, codingItem ->
            if (codingItem.code == coverage) {
                spinner?.setSelection(position)
            }
        }
    }

    private fun fillInsuranceCoverages() {
        PatientDetailsHelper.getInstance()
            .coverages = ArrayList<Coverage>()
        //Basic Health insurance
        healthInsuranceId?.let {
            healthInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(it))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "krank"))
                healthClassCode?.let { code ->
                    coverage.type.coding.add(
                            Coding("https://mdoc.one/hl7/fhir/ukb-usz/insuranceTypes",
                                    code))
                }

                if (healthInsEdt.text.toString().isNotEmpty()) {
                    val identifier = ArrayList<ContactPoint>()
                    identifier.add(ContactPoint(healthInsEdt.text.toString(), OFFICIAL))
                    coverage.identifier = identifier
                }
            }
        }

        healthInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
        //Supplementary insurance
        supplementaryInsuranceId?.let { id ->
            supplementaryInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(id))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "zusatz"))
                supplementaryClassCode?.let { code ->
                    coverage.type.coding.add(
                            Coding("https://mdoc.one/hl7/fhir/ukb-usz/supplementaryInsuranceClass",
                                    code))
                }

                if (supplementeryInsEdt.text.toString().isNotEmpty()) {
                    val identifier = ArrayList<ContactPoint>()
                    identifier.add(ContactPoint(supplementeryInsEdt.text.toString(), OFFICIAL))
                    coverage.identifier = identifier
                }
            }
        }

        supplementaryInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
        //Accident insurance
        accidentInsuranceId?.let { id ->
            accidentInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(id))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "unfall"))
                accidentClassCode?.let { code ->
                    coverage.type.coding.add(
                            Coding("https://mdoc.one/hl7/fhir/ukb-usz/insuranceTypes",
                                    code))
                }

                if (acidentInsPlanIdEdt.text.toString().isNotEmpty()) {
                    val identifier = ArrayList<ContactPoint>()
                    identifier.add(ContactPoint(acidentInsPlanIdEdt.text.toString(), OFFICIAL))
                    coverage.identifier = identifier
                }
            }
        }
        accidentInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
        //Supplementary accident insurance
        supplementaryAccidentInsuranceId?.let { id ->
            supplementaryAccidentInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(id))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "unfall_zusatz"))
                supplementaryAccidentClassCode?.let { code ->
                    coverage.type.coding.add(
                            Coding("https://mdoc.one/hl7/fhir/ukb-usz/supplementaryInsuranceClass",
                                    code))
                }

                if (supplementeryAccidnetInsEdt.text.toString().isNotEmpty()) {
                    val identifier = ArrayList<ContactPoint>()
                    identifier.add(
                            ContactPoint(supplementeryAccidnetInsEdt.text.toString(), OFFICIAL))
                    coverage.identifier = identifier
                }
            }
        }
        supplementaryAccidentInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
        //Disability insurance
        disabilityInsuranceId?.let { id ->
            disabilityInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(id))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "invalid"))

                if (disabilityInsEdt.text.toString().isNotEmpty()) {
                    val identifier = ArrayList<ContactPoint>()
                    identifier.add(ContactPoint(disabilityInsEdt.text.toString(), OFFICIAL))
                    coverage.identifier = identifier
                }
            }
        }
        disabilityInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
        //Military insurance
        militaryInsuranceId?.let { id ->
            militaryInsuranceCoverage = Coverage().also { coverage ->
                coverage.payor = arrayListOf(Payor(id))
                coverage.type = Type()
                coverage.type.coding = arrayListOf(Coding(COVERAGE_CODING_SYSTEM, "militar"))
            }
        }

        militaryInsuranceCoverage?.let {
            PatientDetailsHelper.getInstance()
                .coverages.add(it)
        }
    }

    private fun generateSpinner(view: AppCompatSpinner?,
                                codingList: ArrayList<CodingItem>,
                                callback: (CodingItem) -> Unit) {
        val adapterItems = ArrayList<CodingItem>(codingList)
        adapterItems.add(0,
                newCodingItem("none", getString(R.string.none))
                        )
        val spinnerAdapter = SpinnerAdapter(context!!, adapterItems)
        view?.adapter = spinnerAdapter
        view?.onItemSelectedListener =
                object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        callback(codingList.getOrNull(p2) ?: CodingItem())
                    }
                }
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }


    private fun setAutoCompleteItemId(insuranceData: InsuranceDataKt): String? {
        return if (insuranceData.id.equals("none")) {
            null
        }
        else {
            ORGANIZATION + insuranceData.id
        }
    }

    private fun setSpinnerItemId(insuranceClass: CodingItem): String? {
        return if (insuranceClass.code.equals("none")) {
            null
        }
        else {
            insuranceClass.code
        }
    }

    companion object {

        const val COVERAGE_CODING_SYSTEM = "https://mdoc.one/hl7/fhir/ukb-usz/healthInsuranceTypes"
        const val OFFICIAL = "OFFICIAL"
        const val ORGANIZATION = "organization/"
    }
}
package de.mdoc.modules.vitals.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.create_bt_device.ObservationResponseData
import de.mdoc.modules.devices.DevicesUtil.trimDoubleToDot
import de.mdoc.modules.vitals.FhirConfigurationUtil
import de.mdoc.modules.vitals.FhirConfigurationUtil.getImageByCode
import de.mdoc.modules.vitals.FhirConfigurationUtil.measurementByCode
import de.mdoc.modules.vitals.VitalsFragmentDirections
import org.joda.time.format.DateTimeFormat

private val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
private val timeFormat = DateTimeFormat.forPattern("HH:mm")

class VitalsAdapter(var context: Context):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var observations = emptyList<ObservationResponseData>()

    fun setData(items: List<ObservationResponseData>) {
        observations = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return if (viewType == TYPE_COMPOSITE) {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_composite_vital,
                        viewGroup,
                        false
                        )
            CompositeViewHolder(view)
        }
        else {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_vital,
                        viewGroup,
                        false
                        )
            DefaultViewHolder(view)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val activity = context as MdocActivity
        val item = observations[position]

        if (getItemViewType(position) == TYPE_COMPOSITE) {
            (viewHolder as CompositeViewHolder).bind(item, activity)
        }
        else {
            (viewHolder as DefaultViewHolder).bind(item, activity)
        }
    }

    override fun getItemCount() = observations.size

    override fun getItemViewType(position: Int): Int {
        return if (observations[position].component.isNotEmpty()) {
            TYPE_COMPOSITE
        }
        else {
            TYPE_DEFAULT
        }
    }

    companion object {
        const val TYPE_DEFAULT = 1
        const val TYPE_COMPOSITE = 2
    }
}

class DefaultViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var txtDate: TextView = itemView.findViewById(R.id.txt_measurement_time)
    var txtValue: TextView = itemView.findViewById(R.id.txt_value)
    var imgDevice: ImageView = itemView.findViewById(R.id.ic_connect_device)
    var imgManualEntry: ImageView = itemView.findViewById(R.id.ic_add_data)
    var txtMetric: TextView = itemView.findViewById(R.id.txt_metric)
    var imgMeasurement: ImageView = itemView.findViewById(R.id.img_measurement)
    var clMain: ConstraintLayout = itemView.findViewById(R.id.cl_main)

    fun bind(item: ObservationResponseData, activity: MdocActivity) {
        imgDevice.visibility = if (activity.resources.getBoolean(R.bool.has_devices)) {
            View.VISIBLE
        }
        else {
            View.INVISIBLE
        }

        txtDate.text = getDateTime(item.effectiveDateTime, activity)

        txtValue.text = trimDoubleToDot(item.valueQuantity?.value.toString())
        txtMetric.text = item.valueQuantity?.display ?: ""
        val decodedString = Base64.decode(getImageByCode(item.code.loincCode()), Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imgMeasurement.setImageBitmap(decodedByte)


        clMain.setOnClickListener {
            val action = VitalsFragmentDirections.actionToFragmentCharts(
                    metricCode = arrayOf(item.code.loincCode()),
                    measurementUnit = item.valueQuantity?.display ?: "")
            it.findNavController().navigate(action)
        }

        imgDevice.setOnClickListener {
            val action = VitalsFragmentDirections.actionToNavigationDevices(true)
            it.findNavController().navigate(action)
        }

        imgManualEntry.setOnClickListener {
            val action = VitalsFragmentDirections.globalToManualEntry(measurementByCode(item.code.loincCode()))
            it.findNavController().navigate(action)
        }
    }
}

class CompositeViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var txtDate: TextView = itemView.findViewById(R.id.txt_measurement_time)
    var txtValue: TextView = itemView.findViewById(R.id.txt_value)
    var imgDevice: ImageView = itemView.findViewById(R.id.ic_connect_device)
    var imgManualEntry: ImageView = itemView.findViewById(R.id.ic_add_data)
    var txtMetric: TextView = itemView.findViewById(R.id.txt_metric)
    var txtMetric2: TextView = itemView.findViewById(R.id.txt_metric2)
    var txtValue2: TextView = itemView.findViewById(R.id.txt_value2)
    var imgMeasurement: ImageView = itemView.findViewById(R.id.img_measurement)
    var clMain: ConstraintLayout = itemView.findViewById(R.id.cl_main)

    fun bind(item: ObservationResponseData, activity: MdocActivity) {
        imgDevice.visibility = if (activity.resources.getBoolean(R.bool.has_devices)) {
            View.VISIBLE
        }
        else {
            View.INVISIBLE
        }

        txtDate.text = getDateTime(item.effectiveDateTime, activity)
        val decodedString = Base64.decode(getImageByCode(item.code.loincCode()), Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imgMeasurement.setImageBitmap(decodedByte)
        val metric1 = FhirConfigurationUtil.componentMetricLabel(
                item.component.getOrNull(0)?.code?.coding?.getOrNull(0)?.code ?: ""
                                                                )
        val metric2 = FhirConfigurationUtil.componentMetricLabel(
                item.component.getOrNull(1)?.code?.coding?.getOrNull(0)?.code ?: ""
                                                                )

        txtMetric.text = metric1
        txtMetric2.text = metric2

        txtValue.text = trimDoubleToDot(item.component.getOrNull(0)?.valueQuantity?.value.toString())
        txtValue2.text = trimDoubleToDot(item.component.getOrNull(1)?.valueQuantity?.value.toString())

        clMain.setOnClickListener {
            val action = VitalsFragmentDirections.actionToFragmentCharts(
                    metricCode = arrayOf(item.code.loincCode()),
                    measurementUnit = item.valueQuantity?.display ?: "")
            it.findNavController().navigate(action)
        }

        imgDevice.setOnClickListener {
            val action = VitalsFragmentDirections.actionToNavigationDevices(true)
            it.findNavController().navigate(action)
        }

        imgManualEntry.setOnClickListener {
            val action = VitalsFragmentDirections.globalToManualEntry(measurementByCode(item.code.loincCode()))
            it.findNavController().navigate(action)
        }
    }
}

private fun getDateTime(effectiveDateTime: Long, activity: MdocActivity): String {
    val date = dateFormat.print(effectiveDateTime)
    val time = timeFormat.print(effectiveDateTime)

    return activity.getString(R.string.vitals_time, date, time)
}
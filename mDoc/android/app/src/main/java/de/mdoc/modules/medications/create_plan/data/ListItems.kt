package de.mdoc.modules.medications.create_plan.data

import de.mdoc.R

enum class ListItemTypes {
    DAILY,
    WEEKLY,
    EVERY_CERTAIN_PERIOD,
    ON_DEMAND,
    PERMENANT
}

data class ListItems(var text: Any,
                     var selected: Boolean = false,
                     var color: Int = R.color.colorSecondary8,
                     var imageBase64: String? = null,
                     var code: String? = null,
                     var type: ListItemTypes? = null)
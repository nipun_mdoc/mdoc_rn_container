package de.mdoc.modules.files.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentFileDetailsBinding
import de.mdoc.modules.files.adapters.FileDetailsAdapter
import de.mdoc.modules.files.base.FilesBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.pojo.getFileDetailsItems
import de.mdoc.pojo.isSupported
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_file_details.*

class FileDetailsFragment : FilesBaseFragment(), View.OnClickListener {

    private val args: FileDetailsFragmentArgs by navArgs()

    override val navigationItem: NavigationItem = NavigationItem.FilesPreview

    private val filesTransitionViewModel by viewModel {
        FilesTransitionViewModel(RestClient.getService(), args.fileId)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentFileDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_file_details, container, false)
        binding.lifecycleOwner = this
        binding.filesTransitionViewModel = filesTransitionViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnFileExpand.setOnClickListener(this)
        registerObservers()
        filesTransitionViewModel.getFileById()
    }

    private fun registerObservers() {
        filesTransitionViewModel.proceed.observe(viewLifecycleOwner, Observer {
            if (it) {
                filesTransitionViewModel.proceed.value = false
                rvFileDetails.apply {
                    layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                    adapter = FileDetailsAdapter(filesTransitionViewModel.file.value?.getFileDetailsItems(args.categories.toList()), args.categories.toList(), this@FileDetailsFragment)
                }

                when (filesTransitionViewModel.file.value?.isSupported()) {
                    false -> {
                        viewFilePreview.visibility = View.GONE
                    }
                    else -> {
                        if(filesTransitionViewModel.file.value!!.isRejected()){
                            viewFilePreview.visibility = View.GONE
                        }else {
                            viewFilePreview.visibility = View.VISIBLE
                            viewFileInView(context, mediaPdfView, mediaImageView ,filesTransitionViewModel.file.value)
                        }
                    }
                }
            }
        })
    }

    override fun onClick(view: View?) {
        when (view) {
            btnFileExpand -> {
                viewFile(context = context, item = filesTransitionViewModel.file.value)
            }
        }
    }

    override fun reloadData() {
        filesTransitionViewModel.getFileById()
    }

    override fun onFileDeleted(id: String) {
        findNavController().popBackStack()
    }
}
package de.mdoc.modules.common.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import timber.log.Timber

abstract class BaseSearchViewModel<T> : ViewModel(), BaseSearchViewModelFunctions {

    var searchResponse = MutableLiveData<T?>()

    val isLoading = MutableLiveData(false)

    val isShowLoadingError = MutableLiveData(false)

    val isShowContent = MutableLiveData(false)

    val isNoContent = MutableLiveData(false)

    override fun requestSearch(query: String) {
        isShowLoadingError.value = false

        if (query.isEmpty()) {
            isShowContent.value = false
        }

        isNoContent.value = false
        isLoading.value = query.isNotEmpty()
    }

    protected fun onDataLoaded(data: T?, listSize: Int? = 0) {
        Timber.d("onDataLoaded")
        isLoading.value = false
        searchResponse.value = data

        when (listSize) {
            0 -> {
                isNoContent.value = true
                isShowContent.value = false
            }
            else -> {
                isShowContent.value = true
            }
        }
    }

    protected fun onError(e: Throwable) {
        Timber.d(e)
        isShowContent.value = false
        isLoading.value = false
        isShowLoadingError.value = true
    }
}
package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AutoCompleteTextView
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.modules.profile.patient_details.data.InsuranceDataKt
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.CodingResponse
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.RelatedPerson
import de.mdoc.pojo.newCodingItem
import kotlinx.android.synthetic.main.fragment_caregiver.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.annotation.Nullable

class CaregiverFragment: Fragment() {

    lateinit var activity: PatientDetailsActivity
    var countryList: ArrayList<CodingItem>? = null
    var relationList: ArrayList<InsuranceDataKt>? = null
    private var relationshipStatusId: String? = null //autoCompleteText

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as PatientDetailsActivity

        return inflater.inflate(R.layout.fragment_caregiver, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        activity.setNavigationItem(3)
        relationList = activity.patientDetailsViewModel.relationshipStatusList.value ?: arrayListOf()
        relationshipStatusId = PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
            ?.relationshipType

        if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo!!.size > 0) {
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.firstName?.let { relPersonNameEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.lastName?.let { relPersonLastNameEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.street?.let { relPersonStreetHNEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.houseNumber?.let { relPersonStreetHNEdt.append(" " + it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.city?.let { relPersonCityEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.postalCode?.let { relPersonPostCodeEdt.setText(it) }
            //            PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)?.address?.country?.let { relPersonCountryEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.homePhoneNumber?.let { relPersonPrivatePhoneEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.workPhoneNumber?.let { relPersonWorkPhoneEdt.setText(it) }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.phone?.let { relPersonMobilePhone.setText(it) }
        }

        previous3Btn.setOnClickListener {
            activity.onBackPressed()
        }

        continue3Btn.setOnClickListener {
            if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo!!.size == 0) {
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.add(
                        RelatedPerson())
            }
            val text = relPersonStreetHNEdt.text.toString()
            val index = text.lastIndexOf(" ")
            if (index != -1) {
                val street = text.substring(0, index)
                val number = text.substring(index, text.length)
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                    ?.address?.street = street
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                    ?.address?.houseNumber = number
            }
            else {
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                    ?.address?.street = relPersonStreetHNEdt.text.toString()
            }

            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.firstName = relPersonNameEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.lastName = relPersonLastNameEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.city = relPersonCityEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.address?.postalCode = relPersonPostCodeEdt.text.toString()
            //                PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)?.address?.country = relPersonCountryEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.homePhoneNumber = relPersonPrivatePhoneEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.workPhoneNumber = relPersonWorkPhoneEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.phone = relPersonMobilePhone.text.toString()

            showNewFragment(
                    FamilyDoctorFragment(), R.id.container)
        }
        //country list
        MdocManager.getCoding("https://mdoc.one/hl7/fhir/countryList",
                object: Callback<CodingResponse> {
                    override fun onResponse(call: Call<CodingResponse>,
                                            response: Response<CodingResponse>) {
                        if (response.isSuccessful) {
                            if (isAdded) {
                                countryList = response.body()!!.data?.list
                                countryList!!.add(0, newCodingItem("", ""))
                                val spinnerAdapter = SpinnerAdapter(context!!, countryList!!)
                                relPersonCountrySpinner?.adapter = spinnerAdapter
                                val index: Int? = countryList?.indexOf(
                                        newCodingItem(
                                                PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(
                                                        0
                                                                                                                                                         )?.address?.countryCode
                                                     )
                                                                      )
                                if (index != null) {
                                    relPersonCountrySpinner?.setSelection(index)
                                }
                            }
                        }
                    }

                    override fun onFailure(call: Call<CodingResponse>, t: Throwable) {
                    }
                })

        relPersonCountrySpinner.onItemSelectedListener =
                object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(
                                0)
                            ?.address?.countryCode = countryList?.getOrNull(p2)
                            ?.code
                        PatientDetailsHelper.getInstance()
                            .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(
                                0)
                            ?.address?.country = countryList?.getOrNull(p2)
                            ?.display
                    }
                }




        activity.generateAutoComplete(ac_relationship,
                relationList!!,false) {
            relationshipStatusId = setAutoCompleteItemId(it)

            if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.relatedPersonsInfo!!.size == 0) {
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.add(RelatedPerson())
            }

            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.relatedPersonsInfo?.getOrNull(0)
                ?.relationshipType = relationshipStatusId
        }
        setAutoCompletePreSelection(ac_relationship, relationList, relationshipStatusId)
    }

    private fun setAutoCompletePreSelection(autoComplete: AutoCompleteTextView?,
                                            inputList: ArrayList<InsuranceDataKt>?,
                                            coverage: String?) {
        val index: Int = activity.getPositionOfItem(inputList, coverage)

        if (index > -1) {
            autoComplete?.setText(inputList?.getOrNull(index)?.name, true)
            when (autoComplete) {
                ac_relationship -> {
                    relationshipStatusId = setAutoCompleteItemId(inputList?.getOrNull(index) ?: InsuranceDataKt())
                }
            }
        }
    }

    private fun setAutoCompleteItemId(coding: InsuranceDataKt): String? {
        return if (coding.id.equals("none")) {
            null
        }
        else {
            coding.id
        }
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }
}

package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.UserAnswerData;


/**
 * Created by ema on 1/16/17.
 */

public class UserAnswersResponse extends MdocResponse {

    private ArrayList<UserAnswerData> data;

    public ArrayList<UserAnswerData> getData() {
        return data;
    }

    public void setData(ArrayList<UserAnswerData> data) {
        this.data = data;
    }
}

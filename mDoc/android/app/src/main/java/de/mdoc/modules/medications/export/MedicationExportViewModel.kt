package de.mdoc.modules.medications.export

import android.content.Context
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.common.export.BaseExportViewModel
import de.mdoc.network.request.export.MedicationExportRequest
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class MedicationExportViewModel(private val service: IMdocService, cb: ExportCallback) : BaseExportViewModel<MedicationExportRequest>(cb) {

    override fun download(request: MedicationExportRequest, type: ExportType, context: Context?) {
        isLoading.set(true)
        viewModelScope.launch {
            try {
                val response = service.downloadMedications(type.value.toUpperCase(), request)
                writeFile(type, context, response)
                isLoading.set(false)
            } catch (e: java.lang.Exception) {
                Timber.d(e)
                isLoading.set(false)
            }
        }
    }
}
package de.mdoc.modules.airandpollen.data

import android.content.Context
import de.mdoc.R
import org.joda.time.LocalDate
import org.joda.time.format.DateTimeFormat

private val FORMAT = DateTimeFormat.forPattern("dd. MMM")

enum class Date {
    Today {
        override fun getName(context: Context): String = context.getString(R.string.today_date)
    },
    Tomorrow {
        override fun getName(context: Context): String = FORMAT.print(LocalDate.now().plusDays(1))
    },
    DayAfterTomorrow {
        override fun getName(context: Context): String = FORMAT.print(LocalDate.now().plusDays(2))
    };

    abstract fun getName(context: Context): String

}
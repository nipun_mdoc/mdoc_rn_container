package de.mdoc.modules.entertainment.fragments.overview

import android.app.AlertDialog
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.*
import android.widget.Button
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.entertainment.adapters.overview.MagazinesOverviewMainAdapter
import de.mdoc.modules.entertainment.viewmodels.EntertainmentViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterSharedViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineFilterViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineOverviewViewModel
import de.mdoc.network.RestClient
import de.mdoc.util.ConnectivityObserver
import de.mdoc.util.KBus
import de.mdoc.util.onCreateOptionsMenuWithBadge
import de.mdoc.util.updateBadge
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.common_search_view.*
import kotlinx.android.synthetic.main.fragment_entertainment_magazines.*


class MagazinesOverviewFragment : MdocFragment() {

    class StatisticRefresh

    private val entertainmentViewModel by viewModel {
        EntertainmentViewModel(mDocService = RestClient.getService())
    }

    private val viewModel by viewModel {
        MagazineOverviewViewModel(mDocService = RestClient.getService(), languages =  sharedViewModel.getActiveLanguagesQuery(), categories = sharedViewModel.getActiveCategoriesQuery())
    }

    private val filterViewModel by viewModel {
        MagazineFilterViewModel(RestClient.getService())
    }

    private lateinit var activity: MdocActivity
    private lateinit var sharedViewModel: MagazineFilterSharedViewModel
    private lateinit var connectivityObserver: ConnectivityObserver

    private var savedRecyclerLayoutState: Parcelable? = null
    private var connectionAlert: AlertDialog? = null

    override val navigationItem: NavigationItem = NavigationItem.Entertainment


    override fun setResourceId(): Int {
        return R.layout.fragment_entertainment_magazines
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        connectivityObserver = ConnectivityObserver(mdocActivity)

        sharedViewModel = mdocActivity?.run {
            ViewModelProviders.of(this).get(MagazineFilterSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        registerMagazinesAvailabilityObserver()
    }

    private fun setupMagazinesOverview (){
        setHasOptionsMenu(true)

        searchView.setOnQueryTextFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                findNavController().navigate(R.id.action_entertainmentOverview_to_entertainmentSearch)
            }
        }

        KBus.subscribe<StatisticRefresh>(this) {
            viewModel.getEntertainmentStatistic(sharedViewModel.getActiveLanguagesQuery(), sharedViewModel.getActiveCategoriesQuery(), true)
        }

        viewModel.data.observe(viewLifecycleOwner, Observer {
            magazinesMainRv.apply {
                layoutManager = LinearLayoutManager(mdocActivity, RecyclerView.VERTICAL, false)
                adapter = MagazinesOverviewMainAdapter(it?.value)

                magazinesMainRv.setHasFixedSize(true)

                if (savedRecyclerLayoutState != null) {
                    magazinesMainRv.layoutManager?.onRestoreInstanceState(savedRecyclerLayoutState)
                }
            }
        })

        registerFilterObservers()
        registerInternetConnectionObserver()
        bindVisibleGone(txtError, viewModel.isError)
    }

    private fun registerMagazinesAvailabilityObserver (){
        entertainmentViewModel.myStayData.observe(viewLifecycleOwner, Observer {
            val value = it?.value
            if (value != null) {
                if (value.canPatientAccessEntertainment()) {
                    entertainmentRoot.visibility = View.VISIBLE
                    setupMagazinesOverview()
                } else {
                    entertainmentRoot.visibility = View.GONE
                    val bundle = Bundle()
                    bundle.putSerializable("myStayData", value)
                    findNavController().navigate(R.id.action_entertainmentOverview_to_entertainmentNotAvailable, bundle)
                }
            }
        })
    }

    private fun registerInternetConnectionObserver () {
        connectivityObserver.observe(viewLifecycleOwner, Observer {
            if (it) {
                disconnected()
            } else {
                connected()
            }
        })
    }

    private fun registerFilterObservers () {
        filterViewModel.categories.observe(viewLifecycleOwner, Observer {
            if (sharedViewModel.categories?.size == 0) {
                sharedViewModel.categories = it
            }
        })

        filterViewModel.languages.observe(viewLifecycleOwner, Observer {
            if (sharedViewModel.languages?.size == 0) {
                sharedViewModel.languages = it
            }
        })

        sharedViewModel.filterCounter.observe(viewLifecycleOwner, Observer {
            activity.invalidateOptionsMenu()
            viewModel.getEntertainmentStatistic(sharedViewModel.getActiveLanguagesQuery(), sharedViewModel.getActiveCategoriesQuery())
        })
    }


    override fun onStart() {
        super.onStart()
        connectivityObserver.registerReceiver()
    }

    override fun onStop() {
        super.onStop()
        savedRecyclerLayoutState =  magazinesMainRv.layoutManager?.onSaveInstanceState()
        connectivityObserver.unregisterReceiver()
        KBus.unsubscribe(this)
    }

    private fun disconnected() {
        // Prevent showing alert multiple times
        connectionAlert?.dismiss()

        val builder = AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(de.mdoc.R.layout.entertainment_no_connection, null)
        builder.setView(view)
        val btnOk = view.findViewById<Button>(de.mdoc.R.id.btnConnectionOk)

        btnOk.setOnClickListener {
            connectionAlert?.dismiss()
        }

        connectionAlert = builder.show()
    }

    private fun connected() {
        connectionAlert?.dismiss()
        viewModel.getEntertainmentStatistic(sharedViewModel.getActiveLanguagesQuery(), sharedViewModel.getActiveCategoriesQuery())
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenuWithBadge(menu, inflater, R.menu.menu_entertainment_overview, R.id.entertainmentFilter)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        updateBadge(menu, sharedViewModel.filterCounter.value, R.id.entertainmentFilter)
        super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.entertainmentFilter) {
            findNavController().navigate(R.id.action_entertainmentOverview_to_entertainmentFilter)
        }

        return super.onOptionsItemSelected(item)
    }
}
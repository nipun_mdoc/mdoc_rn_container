package de.mdoc.modules.booking

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.modules.booking.data.Doctor
import de.mdoc.network.request.FreeSlotsRequest
import de.mdoc.pojo.FreeSlotsList
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class BookAppointmentViewModel(private val mDocService: IMdocService, additionalFields: BookingAdditionalFields):
        ViewModel() {

    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(true)
    var filteredAppointments: MutableLiveData<List<FreeSlotsList>> = MutableLiveData(ArrayList())
    var appointment: MutableLiveData<FreeSlotsList> = MutableLiveData(FreeSlotsList())
    var doctors: MutableLiveData<List<Doctor>> = MutableLiveData(ArrayList())
    var bookingAdditionalFields: MutableLiveData<BookingAdditionalFields> = MutableLiveData(additionalFields)
    var allAppointments: MutableLiveData<List<FreeSlotsList>> = MutableLiveData(ArrayList())

    fun getEncountersApi(onSuccess: (Boolean) -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val shouldAskForMasterData = mDocService.getEncounters().data.isNullOrEmpty()
                isLoading.value = false
                onSuccess(shouldAskForMasterData)
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun getDoctorList() {
        viewModelScope.launch {
            try {
                getDoctorListAsync()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
            }
        }
    }

    private suspend fun getDoctorListAsync() {
        doctors.value = mDocService.getListOfDoctors()
            .data
    }

    fun getFreeSlots(request: FreeSlotsRequest,onSuccess: () -> Unit,onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                getFreeSlotsAsync(request)
                onSuccess()
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoDataMessage.value = false
                onError(e)
            }
        }
    }

    private suspend fun getFreeSlotsAsync(request: FreeSlotsRequest) {
        isLoading.value = true
        isShowNoDataMessage.value = false
        isShowLoadingError.value = false
        allAppointments.value = mDocService.getFreeSlots(request)
            .data.list

    }

    fun filterAppointmentsByCode(code: String?) {
        when (code) {
            ALL_APPOINTMENTS       -> {
                filteredAppointments.value = allAppointments.value
            }
            VIDEO_APPOINTMENTS     -> {
                filteredAppointments.value = allAppointments.value?.filter { it.appointmentDetails.isOnline }
            }
            NOT_VIDEO_APPOINTMENTS -> {
                filteredAppointments.value = allAppointments.value?.filter { !it.appointmentDetails.isOnline }
            }
            else                   -> {
                filteredAppointments.value = allAppointments.value
            }
        }
        if (filteredAppointments.value.isNullOrEmpty()) {
            isShowNoDataMessage.value = true
            isShowContent.value = false
        }
        else {
            isShowContent.value = true
            isShowNoDataMessage.value = false
        }
        isLoading.value = false

    }

    fun isAppointmentChanging() = bookingAdditionalFields.value?.appointmentId?.isNotEmpty()

    companion object {
        const val VIDEO_APPOINTMENTS = "VIDEOCONSULTATION"
        const val ALL_APPOINTMENTS = "ALL"
        const val NOT_VIDEO_APPOINTMENTS = "ATLOCATION"
    }
}
package de.mdoc.network.response

data class SelfAssignResponse(var data: Data? = null)

data class Data(var id:String ="")
package de.mdoc.network.response;

import de.mdoc.pojo.SearchFilesData;

/**
 * Created by ema on 4/2/18.
 */

public class SearchFilesResponse extends MdocResponse {

    private SearchFilesData data;

    public SearchFilesData getData() {
        return data;
    }

    public void setData(SearchFilesData data) {
        this.data = data;
    }
}

package de.mdoc.data.requests

class ClinicSelectionRequest (val clinicId: String)
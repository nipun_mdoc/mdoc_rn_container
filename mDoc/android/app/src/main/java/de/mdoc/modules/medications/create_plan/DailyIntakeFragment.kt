package de.mdoc.modules.medications.create_plan

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentDailyIntakeBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import de.mdoc.storage.AppPersistence
import de.mdoc.util.toBitmap
import kotlinx.android.synthetic.main.fragment_daily_intake.*

class DailyIntakeFragment internal constructor(val viewModel: CreatePlanViewModel):
        NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar
    lateinit var activity: MdocActivity

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity
        val binding: FragmentDailyIntakeBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_daily_intake, container, false)
        binding.createPlanViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupDaily()
    }

    private fun setupDaily() {
        daily_group?.removeAllViews()
        AppPersistence.timeCoding?.forEach { listItem ->
            val childView =
                    LayoutInflater.from(context)
                        .inflate(R.layout.placeholder_checkbox_item, null)

            val placeholder: ConstraintLayout = childView.findViewById(R.id.placeholder_image)
            val label: TextView = childView.findViewById(R.id.label_img)
            val image: ImageView = childView.findViewById(R.id.image)
            placeholder.visibility=View.VISIBLE
            placeholder.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, listItem.color))

            label.text = listItem.text as String

            listItem.imageBase64?.let {
                image.setImageBitmap(it.toBitmap())
            }

            daily_group.addView(childView)
            placeholder.setOnClickListener {
                viewModel.daySelected(listItem)

                placeholder.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(activity, listItem.color))
            }
        }
    }

    companion object {
        val TAG = DailyIntakeFragment::class.java.canonicalName
    }
}

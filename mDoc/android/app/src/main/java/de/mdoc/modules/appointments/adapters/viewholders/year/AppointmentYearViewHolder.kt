package de.mdoc.modules.appointments.adapters.viewholders.year

import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseYearAppointmentViewHolder
import de.mdoc.pojo.AppointmentDetails

class AppointmentYearViewHolder(itemView: View) : BaseYearAppointmentViewHolder(itemView) {
    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        super.bind(item, context, appointmentCallback, hasHeader)
        prepareUI(context)

        _txtMetaData.visibility = View.GONE
        _txtUpdateMessage.visibility = View.GONE

        when {
            item.isCanceled -> {
                _txtMetaData.visibility = View.VISIBLE
                _txtMetaData.setBackgroundColor(ContextCompat.getColor(context, R.color.custom_bg))
                _txtMetaData.setText(R.string.appointment_canceled)
                _txtMetaData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_error_48_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.color_404040))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.color_7a7a7a))

            }
            item.isMetaDataPopulated -> {
                _txtMetaData.visibility = View.VISIBLE
                _txtMetaData.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSecondary32))
                _txtMetaData.setText(R.string.appointment_metadata_populated)
                _txtMetaData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_circle_24_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))

            }
            item.isMetaDataNeeded -> {
                _txtMetaData.visibility = View.VISIBLE
                _txtMetaData.setBackgroundColor(ContextCompat.getColor(context, R.color.colorSecondary32))
//                _txtMetaData.setText(R.string.appointment_metadata_needed)
                _txtMetaData.text = MdocConstants.appointment_title
                _txtMetaData.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_report_problem_24_px, 0, 0, 0)

                _txtTime.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
                _txtDescription.setTextColor(ContextCompat.getColor(context, R.color.black_yellow))
            }
        }

    }
}
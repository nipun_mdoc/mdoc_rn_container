package de.mdoc.viewmodel.compositelist

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Base adapter which allows to add dynamically any number of view types using [ListItemTypeAdapter]s
 */
class CompositeListAdapter : RecyclerView.Adapter<ItemViewHolder>() {

    private var viewTypeIndex = 1

    private class AdapterType(
        val viewType: Int,
        val typeAdapter: ListItemTypeAdapter<*, *>
    )

    private val items = mutableListOf<Any>()
    private val typeAdapters = mutableMapOf<Class<*>, AdapterType>()

    inline fun <reified T> addAdapter(typeAdapter: ListItemTypeAdapter<T, *>) {
        addAdapter(T::class.java, typeAdapter)
    }

    fun <T> addAdapter(clazz: Class<T>, typeAdapter: ListItemTypeAdapter<T, *>) {
        typeAdapters[clazz] = AdapterType(viewTypeIndex++, typeAdapter)
    }

    private fun getAdapter(item: Any): AdapterType {
        var clazz: Class<*>? = item.javaClass
        do {
            val result = typeAdapters[clazz]
            if (result != null) {
                return result
            }
            clazz = clazz?.superclass
        } while (clazz != null)
        throw IllegalArgumentException("Not found adapter for class ${item.javaClass}")
    }

    fun setItems(list: List<out Any>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return getAdapter(items[position]).viewType
    }

    override fun getItemId(position: Int): Long {
        val item = items[position]
        val adapter = getAdapter(items[position]).typeAdapter as ListItemTypeAdapter<Any, *>
        return adapter.getId(position, item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val result = typeAdapters.values.firstOrNull {
            it.viewType == viewType
        }?.let {
            it.typeAdapter.onCreateViewHolder(parent)
        } ?: throw IllegalArgumentException("ViewType not registered")
        return ItemViewHolder(result.itemView, result)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = items[position]
        holder.compositeItemHolder.bind(position, item, getAdapter(item).typeAdapter)
    }

}

class ItemViewHolder(
    view: View,
    val compositeItemHolder: ListItemViewHolder
) : RecyclerView.ViewHolder(view)
package de.mdoc.modules.devices.connected_devices

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.R

class ConnectedDevicePagerAdapter(fm: FragmentManager?, val context: Context?, val fragments:List<Fragment>) :
        FragmentStatePagerAdapter(fm!!){

    override fun getItem(position: Int): Fragment {

        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (fragments[position] is VitalConnectedFragment){
            context?.resources?.getString(R.string.vital)
        }else{
            context?.resources?.getString(R.string.medical)
        }
    }
}

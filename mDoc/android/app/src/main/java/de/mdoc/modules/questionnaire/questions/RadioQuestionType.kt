package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.TextView
import de.mdoc.R
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import kotlinx.android.synthetic.main.radio_btn_layout.view.*

internal class RadioQuestionType: QuestionType() {

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.radio_btn_layout, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value = false
        val radioBtnHolder = view.radioBtnHolder
        val answer = question.answer

        question.questionData.options.mapIndexed { index, option ->
            val radioButton = inflater.inflate(R.layout.radio_button_template, radioBtnHolder, false) as RadioButton
            radioBtnHolder.addView(radioButton)

            radioButton.text = option.getValue(question.selectedLanguage)
            radioButton.id = index
            radioButton.tag = option.key
            radioButton.setOnClickListener {
                questionIsAnswered.value = true
                answers.clear()
                answers.add(option.getValue(question.selectedLanguage))
                selection.clear()
                selection.add(option.key)
                isNewAnswer = true
            }

            if (answer != null && answer.answerData == option.getValue(question.selectedLanguage)) {
                questionIsAnswered.value = true
                radioButton.isChecked = true
                answers.clear()
                answers.add(option.getValue(question.selectedLanguage))
                selection.clear()
                selection.add(option.key)
            }
        }

        mandatoryCheck(mandatory, null, question)

        return view
    }
}
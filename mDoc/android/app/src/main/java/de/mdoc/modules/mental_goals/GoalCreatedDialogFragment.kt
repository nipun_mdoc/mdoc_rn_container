package de.mdoc.modules.mental_goals

import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import de.mdoc.R


class GoalCreatedDialogFragment : DialogFragment() {

    fun newInstance(): GoalCreatedDialogFragment {
        return GoalCreatedDialogFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.dialog_goal_created, container)
    }


    override fun onResume() {
        // Sets the height and the width of the DialogFragment
        dialog?.window?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)

        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)
        dialog?.window?.setWindowAnimations(
                R.style.DialogAnimation)


        object : CountDownTimer(2000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
            }
            override fun onFinish() {
                dialog?.dismiss()
            }
        }.start()

        super.onResume()
    }
}

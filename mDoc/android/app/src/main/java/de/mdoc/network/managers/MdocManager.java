package de.mdoc.network.managers;


import android.content.Context;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import de.mdoc.BuildConfig;
import de.mdoc.constants.MdocConstants;
import de.mdoc.data.create_bt_device.Observation;
import de.mdoc.data.requests.CreateDeviceRequest;
import de.mdoc.data.requests.ObservationStatisticsRequest;
import de.mdoc.data.responses.ConfigurationResponse;
import de.mdoc.data.responses.DevicesResponse;
import de.mdoc.data.responses.QuestionnaireAnswersResponse;
import de.mdoc.data.responses.SingleDeviceResponse;
import de.mdoc.modules.devices.data.DevicesForUserRequest;
import de.mdoc.modules.devices.data.FhirConfigurationResponse;
import de.mdoc.modules.devices.data.ObservationResponse;
import de.mdoc.modules.diary.data.PollReassignResponse;
import de.mdoc.modules.medications.create_plan.data.CodingResponseKt;
import de.mdoc.modules.medications.search.data.MedicineDetail;
import de.mdoc.modules.mental_goals.MentalGoalsUtil;
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest;
import de.mdoc.modules.mental_goals.data_goals.SingleMentalGoalResponse;
import de.mdoc.network.RestClient;
import de.mdoc.network.request.CodingRequest;
import de.mdoc.network.request.FirebaseTokenRequest;
import de.mdoc.network.request.KeycloackLoginRequest;
import de.mdoc.network.request.MealChoiceRequest;
import de.mdoc.network.request.MealPlanRequest;
import de.mdoc.network.request.MediaTrackingRequest;
import de.mdoc.network.request.MetaDataPostRequest;
import de.mdoc.network.request.NotificationSeenRequest;
import de.mdoc.network.request.PostAnswerRequest;
import de.mdoc.network.request.PostNotificationRequest;
import de.mdoc.network.request.SaveEmailRequest;
import de.mdoc.network.request.SearchClinicRequest;
import de.mdoc.network.request.VoteRequest;
import de.mdoc.network.response.AnswerDataResponse;
import de.mdoc.network.response.CodingResponse;
import de.mdoc.network.response.CurrentClinicResponse;
import de.mdoc.network.response.DiaryResponse;
import de.mdoc.network.response.FileUpdate;
import de.mdoc.network.response.FinishQuestionnaireResponse;
import de.mdoc.network.response.InfoResponse;
import de.mdoc.network.response.KeycloackLoginResponse;
import de.mdoc.network.response.LoginResponse;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.network.response.MealChoiceResponse;
import de.mdoc.network.response.MealPlanResponse;
import de.mdoc.network.response.MediaResponse;
import de.mdoc.network.response.NotificationResponse;
import de.mdoc.network.response.PollVotingScoreResponse;
import de.mdoc.network.response.PublicMealResponse;
import de.mdoc.network.response.QuestionnaireDetailsResponse;
import de.mdoc.network.response.QuestionnaireResponse;
import de.mdoc.network.response.SearchClinicResponse;
import de.mdoc.network.response.SelfAssignResponse;
import de.mdoc.network.response.ServerTimeResponse;
import de.mdoc.network.response.UserAnswersResponse;
import de.mdoc.network.response.VoteResponse;
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_;
import de.mdoc.pojo.AppointmentResponse;
import de.mdoc.pojo.Chat;
import de.mdoc.pojo.ChildResponse;
import de.mdoc.pojo.ClinicDetails;
import de.mdoc.pojo.ConfigurationData;
import de.mdoc.pojo.CreateUserDeviceResponse;
import de.mdoc.pojo.EmptyBody;
import de.mdoc.pojo.ExerciseResponseNew;
import de.mdoc.pojo.FileUpdateResponse;
import de.mdoc.pojo.HistoryData;
import de.mdoc.pojo.PairedDeviceResponse;
import de.mdoc.pojo.PrivacyPolicyResponse;
import de.mdoc.pojo.ProvidersData;
import de.mdoc.pojo.PublicClinicResponse;
import de.mdoc.pojo.SecretModel;
import de.mdoc.pojo.StatisticData;
import de.mdoc.pojo.WeatherResponse;
import de.mdoc.storage.AppPersistence;
import de.mdoc.util.MdocAppHelper;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

/**
 * Created by ema on 4/6/17.
 */

public class MdocManager {

    private static MdocManager instance;

    public static MdocManager getInstance() {
        if (null == instance) {
            instance = new MdocManager();
        }
        return instance;
    }

    public static void logout() {
        MdocAppHelper.getInstance().setUserAuth(null);
        MdocAppHelper.getInstance().setAuthExpireTime(Long.valueOf(0));
        MdocAppHelper.getInstance().setPatientId(null);
        MdocAppHelper.getInstance().setUserLastName(null);
        MdocAppHelper.getInstance().setUserFirstName(null);
        MdocAppHelper.getInstance().setUserType(null);
        MdocAppHelper.getInstance().setDepartment(null);
        MdocAppHelper.getInstance().setAccessToken(null);
        MdocAppHelper.getInstance().setRefreshToken(null);
        MdocAppHelper.getInstance().setChecklistOrder(null);
        MdocAppHelper.getInstance().setChildLoged(false);
        MdocAppHelper.getInstance().setCaseId(null);
        MdocAppHelper.getInstance().setExternalCaseId(null);
        AppPersistence.INSTANCE.setLastSeenUnitByCode(new HashMap<>());
        MdocConstants.caseList = null;
        if(!BuildConfig.FLAV.equals("kiosk")) {
            MdocAppHelper.getInstance().setClinicId(null);
        }
        AppPersistence.INSTANCE.clearData();
        MentalGoalsUtil.INSTANCE.clearData();
    }


    public static void getQuestionnaries(Callback<QuestionnaireResponse> responseCallback) {
        Call<QuestionnaireResponse> call = RestClient.getInstance().getService().getQuestionaries(true);
        call.enqueue(responseCallback);
    }

    public static void getQuestionnaireById(String pollAssignId, Callback<QuestionnaireDetailsResponse> responseCallback){
        Call<QuestionnaireDetailsResponse> call = RestClient.getInstance().getService().getQuestionnaireById(pollAssignId);
        call.enqueue(responseCallback);
    }

    public static void reAssignQuestionnaire(String pollActionId, Callback<PollReassignResponse> responseCallback){
        Call<PollReassignResponse> call = RestClient.getInstance().getService().reassignQuestionnaire(pollActionId, new EmptyBody());
        call.enqueue(responseCallback);
    }

    public static void addLastSeenQuestionnaire(String pollAssignId, String questionId, Callback<ResponseBody> responseCallback){
        Call<ResponseBody> call = RestClient.getInstance().getService().addLastSeenQuestionnaire(pollAssignId, questionId);
        call.enqueue(responseCallback);
    }

    public static void postAnswer(String pollId, String pollAssignId, String pollQuestionId, String answerData, String votingActionId, ArrayList<String> selection, String language, Callback<AnswerDataResponse> responseCallback){
        Call<AnswerDataResponse> call = RestClient.getInstance().getService().postAnswer(pollId, new PostAnswerRequest(pollId, pollAssignId, pollQuestionId, answerData, votingActionId, selection, language));
        call.enqueue(responseCallback);
    }

    public static void postAnswer(String pollId, String pollAssignId, String pollQuestionId, String answerData, String votingActionId, ArrayList<String> selection, Callback<AnswerDataResponse> responseCallback, String defaultLanguage){
        Call<AnswerDataResponse> call = RestClient.getInstance().getService().postAnswer(pollId, new PostAnswerRequest(pollId, pollAssignId, pollQuestionId, answerData, votingActionId, selection, defaultLanguage));
        call.enqueue(responseCallback);
    }

    public static void setUserBirthdate(String userId, String userDate, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().setUserBirthdate(userId, userDate);
        call.enqueue(responseCallback);
    }

    public static void getUserAnswers(Callback<UserAnswersResponse> responseCallback){
        Call<UserAnswersResponse> call = RestClient.getInstance().getService().getUserAnswers();
        call.enqueue(responseCallback);
    }

    public static void getCurrentClinic(Callback<CurrentClinicResponse> responseCallback){
        Call<CurrentClinicResponse> call = RestClient.getInstance().getService().getCurrentClinic();
        call.enqueue(responseCallback);
    }



    public static void finishQuestionnaires(String pollId, String userConsent, String pollVotingActionId, String pollAssignId, Callback<FinishQuestionnaireResponse> callback){
        Call<FinishQuestionnaireResponse> call = RestClient.getInstance().getService().finishQuestionnaire(pollId, userConsent, pollVotingActionId, pollAssignId);
        call.enqueue(callback);
    }

    public static void getPollVotingScore(String pollVotingId, Callback<PollVotingScoreResponse> callback){
        Call<PollVotingScoreResponse> call = RestClient.getInstance().getService().getPollVotingScoreById(pollVotingId);
        call.enqueue(callback);
    }

    public static void loginWithKeycloack(KeycloackLoginRequest request, Callback<KeycloackLoginResponse> callback){
        Call<KeycloackLoginResponse> call = RestClient.getInstance().getKeycloackService().loginWithKeyCloack(request.getUsername(), request.getPassword(), request.getGrant_type(), request.getClient_id(), request.getClient_secret(), request.getTotp(), request.getDeviceName());
        call.enqueue(callback);
    }

    public static void loginWithKeycloackCode(KeycloackLoginRequest request, Callback<KeycloackLoginResponse> callback){
        Call<KeycloackLoginResponse> call = RestClient.getInstance().getKeycloackService().loginWithKeyCloackCode(request.getGrant_type(), request.getClient_id(), request.getClient_secret(), request.getCode(), request.getRedirect_uri(), request.getDeviceName());
        call.enqueue(callback);
    }


    public static void loginWithChildAccount(KeycloackLoginRequest request, Callback<KeycloackLoginResponse> callback){
        Call<KeycloackLoginResponse> call = RestClient.getInstance().getKeycloackService().loginWithChildAccount(request.getUsername(), request.getPassword(), request.getGrant_type(), request.getClient_id(), request.getClient_secret(), request.getTotp(), request.getDeviceName(), request.getRequest_subject());
        call.enqueue(callback);
    }

    public static void loginWithSession(Callback<LoginResponse> callback){
        Call<LoginResponse> call = RestClient.getInstance().getService().loginWithSession(new EmptyBody());
        call.enqueue(callback);
    }


    public static void postForAllNotifcations(int skip, int limit, Context context, Callback<NotificationResponse> responseCallback) {
        Call<NotificationResponse> call = RestClient.getInstance().getService().getAllNotifications(new PostNotificationRequest(skip, limit, context));
        call.enqueue(responseCallback);
    }

    public static void deleteAllNotifications(Callback<ResponseBody> callback) {
        Call<ResponseBody> call = RestClient.getInstance().getService().deleteAllNotifications();
        call.enqueue(callback);
    }

    public static void deleteNotification(String id, Callback<ResponseBody> callback) {
        Call<ResponseBody> call = RestClient.getInstance().getService().deleteNotification(id);
        call.enqueue(callback);
    }

    public static void getConfigurations(Callback<ConfigurationData> responseCallback) {
        Call<ConfigurationData> call = RestClient.getInstance().getAuthServiceLogin().getConfiguration();
        call.enqueue(responseCallback);
    }

    public static void getStatistics(LinkedHashMap<String, String> params, Callback<StatisticData> responseCallback){
        Call<StatisticData> call = RestClient.getInstance().getService().getProviderStatistic(params);
        call.enqueue(responseCallback);
    }

    public static void getConnectedDevices(String userId, Callback<ProvidersData> responseCallback) {
        Call<ProvidersData> call = RestClient.getInstance().getService().getConnectedDevices(userId);
        call.enqueue(responseCallback);
    }

    public static void getHistoryForDevices(LinkedHashMap<String, String> params, Callback<HistoryData> responseCallback) {
        Call<HistoryData> call = RestClient.getInstance().getService().getHistoryStatistic(params);
        call.enqueue(responseCallback);
    }

    public static void disconnectDevice(String userId, Callback<ResponseBody> callback) {
        Call<ResponseBody> call = RestClient.getInstance().getService().disconnectDevice(userId);
        call.enqueue(callback);
    }

    public static void searchClinicKiosk(String query, Callback<SearchClinicResponse> callback){
        Call<SearchClinicResponse> call = RestClient.getInstance().getService().searchClinicKiosk(new SearchClinicRequest(query));
        call.enqueue(callback);
    }

    public static void getPublicClinicDetails(String clinicId, Callback<PublicClinicResponse> callback){
        Call<PublicClinicResponse> call = RestClient.getInstance().getService().getKioskClinicDetails(clinicId);
        call.enqueue(callback);
    }

    public static void getPublicMealRepsonse(MealPlanRequest request, Callback<PublicMealResponse> callback){
        Call<PublicMealResponse> call = RestClient.getInstance().getService().getPublicMealResponse(request);
        call.enqueue(callback);
    }


    public static void getWeather(Callback<WeatherResponse> callback){
        Call<WeatherResponse> call = RestClient.getInstance().getWeatherService().getWeather(MdocAppHelper.getInstance().getClinicLatitude(), MdocAppHelper.getInstance().getClinicLongitude(), "b7e91de96310658b7be41ce0d72ccf1c", "metric");
        call.enqueue(callback);
    }

    public static void chatsMessages(LinkedHashMap<String, String> params, Callback<Chat> responseCallback) {
        Call<Chat> call = RestClient.getInstance().getChatService().getChatMessages(params);
        call.enqueue(responseCallback);
    }

    public static void changePasswordProfile(JsonObject body, Callback<MdocResponse> callback){
        Call<MdocResponse> call = RestClient.getInstance().getService().changePasswordProfile(body);
        call.enqueue(callback);
    }

    public static void changePhoneProfile(JsonObject body, Callback<MdocResponse> callback){
        Call<MdocResponse> call = RestClient.getInstance().getService().changePhoneProfile(body);
        call.enqueue(callback);
    }

    public static void changeEmailProfile(JsonObject body, Callback<MdocResponse> callback){
        Call<MdocResponse> call = RestClient.getInstance().getService().changeEmailProfile(body);
        call.enqueue(callback);
    }

    public static void deletePairedDevice(String deviceId, Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().deletePairedDevice(deviceId);
        call.enqueue(callback);
    }

    public static void createUserDevice(JsonObject query, Callback<CreateUserDeviceResponse> callback){
        Call<CreateUserDeviceResponse> call = RestClient.getInstance().getService().createUserDevice(query);
        call.enqueue(callback);
    }

    public static void getAllDevices(Callback<PairedDeviceResponse> responseCallback){
        Call<PairedDeviceResponse> call = RestClient.getInstance().getService().getAllDevices();
        call.enqueue(responseCallback);
    }

    public static void changeFirendlyName(String deviceId, JsonObject body, Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().changeFirendlyName(deviceId, body);
        call.enqueue(callback);
    }

    public static void changeFirstTimePassword(JsonObject body, Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().changeFirstTimePassword(body);
        call.enqueue(callback);
    }

    public static void getMealPlan(MealPlanRequest request, Callback<MealPlanResponse> responseCallback){
        Call<MealPlanResponse> call = RestClient.getInstance().getService().getMealPlan(request);
        call.enqueue(responseCallback);
    }

    public static void getMealChoice(MealChoiceRequest request, Callback<MealChoiceResponse> responseCallback){
        Call<MealChoiceResponse> call = RestClient.getInstance().getService().getMealChoice(request);
        call.enqueue(responseCallback);
    }

    public static void vote(VoteRequest request, Callback<VoteResponse> responseCallback){
        Call<VoteResponse> call = RestClient.getInstance().getService().vote(request);
        call.enqueue(responseCallback);
    }

    public static void updateVote(VoteRequest request, Callback<VoteResponse> responseCallback) {
        Call<VoteResponse> call = RestClient.getInstance().getService().updateVote(request);
        call.enqueue(responseCallback);
    }

    public static void deselectMeal(String choiceId, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().deselectMeal(choiceId);
        call.enqueue(responseCallback);
    }

    public static void getPrivacy(Callback<PrivacyPolicyResponse> callback) {
        Call<PrivacyPolicyResponse> call = RestClient.getInstance().getService().getPrivacy();
        call.enqueue(callback);
    }

    public static void confirmPrivacy(Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().confirmPrivacy();
        call.enqueue(callback);
    }

    public static void getChildAccounts(Callback<ChildResponse> callback) {
        Call<ChildResponse> call = RestClient.getInstance().getService().getChildAccounts();
        call.enqueue(callback);
    }

    public static void getTermsAndConditions(Callback<PrivacyPolicyResponse> callback) {
        Call<PrivacyPolicyResponse> call = RestClient.getInstance().getService().getTermsAndConditions();
        call.enqueue(callback);
    }


    public static void confirmTermAndCondition(Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().confirmTermsAndCondition();
        call.enqueue(callback);
    }

    public static void searchAppointments(JsonObject body, Callback<AppointmentResponse> callback) {
        Call<AppointmentResponse> call = RestClient.getInstance().getService().searchAppointments(body);
        call.enqueue(callback);
    }

    public static void getMealInfo(Callback<InfoResponse> responseCallback){
        Call<InfoResponse> call = RestClient.getInstance().getService().getMealInfo();
        call.enqueue(responseCallback);
    }

    public static void loginWithKeycloackEmotp(KeycloackLoginRequest request, Callback<KeycloackLoginResponse> callback){
        Call<KeycloackLoginResponse> call = RestClient.getInstance().getKeycloackService().loginWithKeyCloackEmotp(request.getUsername(), request.getPassword(), request.getGrant_type(), request.getClient_id(), request.getClient_secret(), request.getEmotp(), request.getDeviceName());
        call.enqueue(callback);
    }

    public static void sendRequestForEmail(KeycloackLoginRequest request, Callback<KeycloackLoginResponse> callback){
        Call<KeycloackLoginResponse> call = RestClient.getInstance().getKeycloackService().sendRequestForEmail(request.getUsername(), request.getPassword(), request.getGrant_type(), request.getClient_id(), request.getClient_secret(), request.getEmotp(), request.getDeviceName());
        call.enqueue(callback);
    }


    public static void createDiary(JsonObject body, Callback<DiaryResponse> responseCallback){
        Call<DiaryResponse> call = RestClient.getInstance().getService().createDiary(body);
        call.enqueue(responseCallback);
    }

    public static void updateDiary(String id, JsonObject body, Callback<DiaryResponse> responseCallback){
        Call<DiaryResponse> call = RestClient.getInstance().getService().updateDiary(id, body);
        call.enqueue(responseCallback);
    }

    public static void uploadFile(RequestBody desc, MultipartBody.Part file, Callback<FileUpdateResponse> responseCallback){
        Call<FileUpdateResponse> call = RestClient.getInstance().getService().uploadFile(desc, file);
        call.enqueue(responseCallback);
    }

    public static void updateFile(FileUpdate request, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().updateFile(request.getId(), request);
        call.enqueue(responseCallback);
    }

    public static void getCareplan(Callback<ExerciseResponseNew> callback){
        Call<ExerciseResponseNew> call = RestClient.getInstance().getService().getCareplan();
        call.enqueue(callback);
    }

    public static void changeActivityStatus(JsonObject body, Callback<MdocResponse> callback){
        Call<MdocResponse> call = RestClient.getInstance().getService().changeActivityStatus(body);
        call.enqueue(callback);
    }

    public static void updateFirebaseToken(Callback<MdocResponse> responseCall) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    String instanceId = task.getResult().getId();
                    String token = task.getResult().getToken();
                    boolean pushEnabled = true;
                    Call<MdocResponse> call = RestClient.getInstance().getService().updateFirebaseToken(new FirebaseTokenRequest(instanceId,token,pushEnabled));
                    call.enqueue(responseCall);
                });
    }

    public static void fetchSecret(String deviceName, Callback<SecretModel> callback) {
        Call<SecretModel> call = RestClient.getInstance().getService().fetchSecret(deviceName);
        call.enqueue(callback);
    }

    public static void setAllNotificationsSeen(NotificationSeenRequest request, Callback<MdocResponse> callback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().setNotificaitonSeen(request);
        call.enqueue(callback);
    }


    public static void cancelAppointment(String appointmentId, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().cancelAppointment(appointmentId);
        call.enqueue(responseCallback);
    }

    public static void getCoding(String systemId, Callback<CodingResponse> callback){
        Call<CodingResponse> call = RestClient.getInstance().getService().getCoding(new CodingRequest(systemId));
        call.enqueue(callback);
    }
    public static void postMetadata(MetaDataPostRequest postRequest, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().postMetadata(postRequest);
        call.enqueue(responseCallback);
    }

    public static void getDevicesForUser(DevicesForUserRequest request,Callback<DevicesResponse> responseCallback){
        Call<DevicesResponse> call = RestClient.getInstance().getService().getDevicesForUser(request);
        call.enqueue(responseCallback);
    }
    public static void createDevice(CreateDeviceRequest request, Callback<SingleDeviceResponse> responseCallback){
        Call<SingleDeviceResponse> call = RestClient.getInstance().getService().createDevice(request);
        call.enqueue(responseCallback);
    }
    public static void removeDevice(String deviceId, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().removeDevice(deviceId);
        call.enqueue(responseCallback);
    }

    public static void createObservations(List<Observation> request, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().createObservations(request);
        call.enqueue(responseCallback);
    }
    public static void getObservationStatistics(ObservationStatisticsRequest request, Callback<ObservationResponse> responseCallback){
        Call<ObservationResponse> call = RestClient.getInstance().getService().getObservationStatistics(request);
        call.enqueue(responseCallback);
    }

    public static void checkDeviceSettings( Callback<ConfigurationResponse> responseCallback){
        Call<ConfigurationResponse> call = RestClient.getInstance().getService().checkDeviceSettings();
        call.enqueue(responseCallback);
    }

    public static void shareFile(String id, Callback<MdocResponse> responseCallback) {
        Call<MdocResponse> call = RestClient.getInstance().getService().shareFile(id);
        call.enqueue(responseCallback);
    }
    public static void getAnsweredQuestionnaires(String from,String to,String pollVotingActionId,String pollId,Boolean includeAnswers, Callback<QuestionnaireAnswersResponse> responseCallback) {
        Call<QuestionnaireAnswersResponse> call = RestClient.getInstance().getService().getAnsweredQuestionnaires(from,to,pollVotingActionId,pollId,includeAnswers);
        call.enqueue(responseCallback);
    }

    public static void getServerTime(Callback<ServerTimeResponse> callback) {
        Call<ServerTimeResponse> call = RestClient.getInstance().getService().getServerTime();
        call.enqueue(callback);
    }

    public static void postMentalGoals(MentalGoalsRequest request,Callback<SingleMentalGoalResponse> responseCallback){
        Call<SingleMentalGoalResponse> call = RestClient.getInstance().getService().postMentalGoals(request);
        call.enqueue(responseCallback);
    }

    public static void updateMentalGoals(String goalID,MentalGoalsRequest requestBody,Callback<SingleMentalGoalResponse> responseCallback){
        Call<SingleMentalGoalResponse> call = RestClient.getInstance().getService().updateMentalGoalsStatus(goalID,requestBody);
        call.enqueue(responseCallback);
    }
    public static void updateMentalGoal(String goalID,MentalGoalsRequest requestBody,Callback<SingleMentalGoalResponse> responseCallback){
        Call<SingleMentalGoalResponse> call = RestClient.getInstance().getService().updateMentalGoal(goalID,requestBody);
        call.enqueue(responseCallback);
    }

    public static void assignQuestionnaire(String pollId, Callback<SelfAssignResponse> responseCallback){
        Call<SelfAssignResponse> call = RestClient.getInstance().getService().assignQuestionnaire(pollId);
        call.enqueue(responseCallback);
    }
    public static void getFhirConfiguration(Callback<FhirConfigurationResponse> responseCallback){
        Call<FhirConfigurationResponse> call = RestClient.getInstance().getService().getFhirConfiguration();
        call.enqueue(responseCallback);
    }

    public static void getMediaById(String id, Callback<MediaResponse> responseCallback){
        Call<MediaResponse> call = RestClient.getInstance().getService().getMediaById(id);
        call.enqueue(responseCallback);
    }

    public static void getMediaById(String mediaId, String pageNumber, Callback<ResponseBody> responseCallback) {
        Call<ResponseBody> call = RestClient.getInstance().getService().getMediaById(mediaId, pageNumber);
        call.enqueue(responseCallback);
    }

    public static void getEntertainmentStatisticAll(String category,Callback<ResponseSearchResultsMediaLightDTO_> responseCallback) {
        Call<ResponseSearchResultsMediaLightDTO_> call = RestClient.getInstance().getService().getEntertainmentStatisticAll(category);
        call.enqueue(responseCallback);
    }

    public static void trackEntertainment(MediaTrackingRequest request, Callback<ResponseBody> responseCallback) {
        Call<ResponseBody> call = RestClient.getInstance().getService().trackEntertainment(request);
        call.enqueue(responseCallback);
    }

    public static void saveEmail(SaveEmailRequest request, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().saveEmail(request);
        call.enqueue(responseCallback);
    }

    public static void skipEmail(String patientId, Callback<MdocResponse> responseCallback){
        Call<MdocResponse> call = RestClient.getInstance().getService().skipEmailDialog(patientId);
        call.enqueue(responseCallback);
    }

    public static void getCodingLightCall(String systemId, Callback<CodingResponseKt> responseCallback){
        Call<CodingResponseKt> call = RestClient.getInstance().getService().getCodingLightCall(systemId);
        call.enqueue(responseCallback);
    }

    public static void getDetails(String id,String query1, String query2, Callback<MedicineDetail> responseCallback){
        Call<MedicineDetail> call = RestClient.getInstance().getService().getMedicineDetails(id, query1, query2);
        call.enqueue(responseCallback);
    }


    public static void getConfiguration(String systemId, Callback<CodingResponseKt> responseCallback){
        Call<CodingResponseKt> call = RestClient.getInstance().getService().getCodingLightCall(systemId);
        call.enqueue(responseCallback);
    }
}
package de.mdoc.modules.help_support

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.constants.IMPRESSUM
import de.mdoc.constants.PRIVACY_POLICY
import de.mdoc.constants.TERMS_AND_CONDITION
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class HelpAndSupportViewModel(private val mDocService: IMdocService): ViewModel() {

    var isLoading = MutableLiveData(false)
    var impressum = MutableLiveData("")
    var privacy = MutableLiveData("")
    var terms = MutableLiveData("")

    init {
        getCodingLight()
    }

    private fun getCodingLight() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val item = mDocService.getCodingLight(PRIVACY_CODING_URL).data
                impressum.value = item?.getItemForCode(IMPRESSUM)?.display
                privacy.value = item?.getItemForCode(PRIVACY_POLICY)?.display
                terms.value = item?.getItemForCode(TERMS_AND_CONDITION)?.display
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
            }
        }
    }

    companion object {
        const val PRIVACY_CODING_URL = "https://mdoc.one/public/help"
    }
}
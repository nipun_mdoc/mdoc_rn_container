package de.mdoc.service

import com.google.gson.JsonObject
import de.mdoc.data.create_bt_device.Observation
import de.mdoc.data.diary.DiaryConfigResponse
import de.mdoc.data.requests.ClinicSelectionRequest
import de.mdoc.data.requests.CreateDeviceRequest
import de.mdoc.data.requests.MetaDataRequest
import de.mdoc.data.requests.ObservationStatisticsRequest
import de.mdoc.data.responses.*
import de.mdoc.modules.appointments.video_conference.data.SingleAppointmentResponse
import de.mdoc.modules.appointments.video_conference.data.TokBoxResponse
import de.mdoc.data.responses.ConfigurationResponse
import de.mdoc.modules.booking.data.BookingRequest
import de.mdoc.modules.booking.data.DoctorsResponse
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.modules.covid19.data.HealthStatusResponse
import de.mdoc.modules.covid19.data.SingleHealthStatusResponse
import de.mdoc.modules.dashboard.data.MessageResponse
import de.mdoc.modules.devices.data.DevicesForUserRequest
import de.mdoc.modules.devices.data.FhirConfigurationResponse
import de.mdoc.modules.devices.data.ObservationResponse
import de.mdoc.modules.diary.data.DiarySearchRequest
import de.mdoc.modules.diary.data.PollReassignResponse
import de.mdoc.modules.external_services.data.ExternalServicesResponse
import de.mdoc.modules.files.data.FileItemResponse
import de.mdoc.modules.media.data.MediaLightSearchResponse
import de.mdoc.modules.medications.create_plan.data.CodingResponseKt
import de.mdoc.modules.medications.create_plan.data.MedicationPlanRequest
import de.mdoc.modules.medications.create_plan.data.response.MedicationItemResponse
import de.mdoc.modules.medications.create_plan.data.response.MedicationPlanResponse
import de.mdoc.modules.medications.data.Medication
import de.mdoc.modules.medications.data.MedicationAdministrationResponse
import de.mdoc.modules.medications.data.MedicationImageResponse
import de.mdoc.modules.medications.search.data.MedicationsResponse
import de.mdoc.modules.medications.search.data.MedicineDetail
import de.mdoc.modules.mental_goals.data_goals.GamificationResponse
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsRequest
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsResponse
import de.mdoc.modules.mental_goals.data_goals.SingleMentalGoalResponse
import de.mdoc.modules.messages.data.*
import de.mdoc.modules.patient_journey.data.CarePlanDetailsResponse
import de.mdoc.modules.patient_journey.data.CarePlanResponse
import de.mdoc.modules.patient_journey.data.MediaDetailResponse
import de.mdoc.modules.profile.data.ContactsRequest
import de.mdoc.modules.profile.data.EmergencyInfoRequest
import de.mdoc.modules.profile.data.TherapyPrintRequest
import de.mdoc.modules.vitals.data.UnitPersistenceRequest
import de.mdoc.modules.vitals.data.UnitPersistenceResponse
import de.mdoc.network.request.*
import de.mdoc.network.request.export.DiaryExportRequest
import de.mdoc.network.request.export.MedicationExportRequest
import de.mdoc.network.response.*
import de.mdoc.network.response.entertainment.ResponseMediaReadingHistoryResponseDto_
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaReadingHistoryResponseDto_
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaStatisticResponseDto_
import de.mdoc.network.response.files.ResponseSearchResultsFileStorageDocumentImpl_
import de.mdoc.newlogin.LoginUser
import de.mdoc.newlogin.Model.*
import de.mdoc.newlogin.UpdatePasswordRequest
import de.mdoc.pojo.*
import de.mdoc.pojo.digitalsignature.SignatureDocumentInfoData
import de.mdoc.pojo.digitalsignature.SignatureDocumentInfoResponse
import de.mdoc.pojo.digitalsignature.SignatureNextActionResponse
import de.mdoc.pojo.digitalsignature.SignatureSignResponse
import de.mdoc.pojo.login.request.LoginRequest
import io.reactivex.Single
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*
import java.util.*

/**
 * Created by ema on 4/6/17.
 */
interface IMdocService {


    @get:GET("v2/patient/polls/answers/all")
    val userAnswers: Call<UserAnswersResponse>

    @get:GET("v2/common/notifications/new")
    val notifications: Call<NewNotificationsResponse>

    @get:GET("v2/clinics/current")
    val currentClinic: Call<CurrentClinicResponse>

    @get:GET("v1/account/devices/all")
    val allDevices: Call<PairedDeviceResponse>

    @get:GET("v2/public/configurations/privacy/mobile")
    val privacy: Call<PrivacyPolicyResponse>

    @get:GET("v2/patient/oauth2/users/child-accounts")
    val childAccounts: Call<ChildResponse>

    @get:GET("v2/public/configurations/terms/mobile")
    val termsAndConditions: Call<PrivacyPolicyResponse>

    @get:GET("v2/media/Media/categories")
    val categories: Call<CategoryResponse>

    @get:GET("v2/public/configurations/meal/info")
    val mealInfo: Call<InfoResponse>

    @get:GET("v2/patient/careplan")
    val careplan: Call<ExerciseResponseNew>

    @get:GET("v2/public/information/server/time")
    val serverTime: Call<ServerTimeResponse>

    @get:GET("v2/patient/observations/latest")
    val observations: Call<ObservationsResponse>

    @get:GET("v2/patient/fhir/device/fhir-conf")
    val fhirConfiguration: Call<FhirConfigurationResponse>

    @get:GET("v2/patient/diary/config")
    val diaryConfig: Call<DiaryConfigResponse>

    @get:GET("v2/common/pollen")
    val pollen: Call<PollenResponse>

    @GET("v2/patient/polls/search")
    fun getQuestionaries(@Query("ignoreDiaryEntries")
                         ignoreDiaryEntries: Boolean): Call<QuestionnaireResponse>

    //    @GET("v1/questionnaires/search/{poll_id}")
    @GET("v2/patient/polls/{pollAssignId}")
    fun getQuestionnaireById(@Path("pollAssignId") pollAssignId: String): Call<QuestionnaireDetailsResponse>

    @POST("v2/patient/polls/{pollActionId}/reassign")
    fun reassignQuestionnaire(@Path("pollActionId") pollActionId: String, @Body body: EmptyBody): Call<PollReassignResponse>

    @GET("v2/patient/polls/{pollAssignId}")
    suspend fun getQuestionnaireByIdSuspend(@Path("pollAssignId") pollAssignId: String): QuestionnaireDetailsResponse

    // Questionnaire - last seen
    @PATCH("/api/v2/patient/polls/{pollAssignId}/mark/as-seen/questionnaire/{questionId}")
    fun addLastSeenQuestionnaire(@Path("pollAssignId") pollAssignId: String,
                                 @Path("questionId") questionId: String): Call<ResponseBody>

    @POST("v2/patient/polls/answers/{pollId}")
    fun postAnswer(@Path("pollId") pollId: String, @Body
    request: PostAnswerRequest): Call<AnswerDataResponse>


    @GET("v2/patient/polls/voting/score/{pollVotingId}")
    fun getPollVotingScoreById(@Path("pollVotingId") pollVotingId: String): Call<PollVotingScoreResponse>

    @POST("v2/common/notifications/search")
    fun getAllNotifications(@Body request: PostNotificationRequest): Call<NotificationResponse>


    @POST("config/assets/mobile")
    fun getConfiguration(): Call<ConfigurationData>

    @PATCH("v2/patient/polls/answers/{pollId}/finish")
    fun finishQuestionnaire(@Path("pollId") pollId: String, @Header("consent") consent: String, @Header("pollVotingActionId") pollVotingActionId: String, @Header("pollAssignId") pollAssignId: String): Call<FinishQuestionnaireResponse>

    @GET("v2/patient/polls/answers/{id}")
    suspend fun getQuestionnaireAnswers(@Path("id")id:String): QuestionnairesAnswersResponse

    @POST("v2/common/auth/session-login")
    fun loginWithSession(@Body body: EmptyBody): Call<LoginResponse>

    @DELETE("v2/common/notifications/all")
    fun deleteAllNotifications(): Call<ResponseBody>

    @DELETE("v2/common/notifications/{id}")
    fun deleteNotification(@Path("id") id: String): Call<ResponseBody>

    @GET("v1/statistics/getAccess")
    fun getProviderStatistic(@QueryMap
                             params: LinkedHashMap<String, String>): Call<StatisticData> //@Query("date") String date, @Query("provider") String provider, @Query("userId") String userId);

    @GET("v1/statistics/getW2eConnectionStatus")
    fun getConnectedDevices(@Query("userId") userId: String): Call<ProvidersData>

    @GET("v1/statistics/getMonth")
    fun getHistoryStatistic(@QueryMap params: LinkedHashMap<String, String>): Call<HistoryData>

    @DELETE("v1/statistics/removeAccess")
    fun disconnectDevice(@Query("userId") userId: String): Call<ResponseBody>

    @POST("v2/public/clinics/kiosk/search")
    fun searchClinicKiosk(@Body request: SearchClinicRequest): Call<SearchClinicResponse>

    @GET("v2/public/clinics/kiosk/{clinic_id}")
    fun getKioskClinicDetails(@Path("clinic_id") clinicId: String): Call<PublicClinicResponse>

    @POST("v2/public/meals/kiosk")
    fun getPublicMealResponse(@Body request: MealPlanRequest): Call<PublicMealResponse>

    @FormUrlEncoded
    @POST("ws.php")
    fun getChatMessages(@FieldMap params: LinkedHashMap<String, String>): Call<Chat>

    @PUT("v2/common/auth/password")
    fun changePasswordProfile(@Body body: JsonObject): Call<MdocResponse>

    @PATCH("v2/common/auth/account/phone")
    fun changePhoneProfile(@Body body: JsonObject): Call<MdocResponse>

    @PATCH("v2/common/auth/account/email")
    fun changeEmailProfile(@Body body: JsonObject): Call<MdocResponse>

    @DELETE("v1/account/devices/{devices}")
    fun deletePairedDevice(@Path("devices") deviceId: String): Call<MdocResponse>

    @POST("v1/account/devices")
    fun createUserDevice(@Body request: JsonObject): Call<CreateUserDeviceResponse>

    @PUT("v1/account/devices/{devices}")
    fun changeFirendlyName(@Path("devices") deviceId: String, @Body
    body: JsonObject): Call<MdocResponse>

    @POST("v2/general/auth/password")
    fun changeFirstTimePassword(@Body body: JsonObject): Call<MdocResponse>

    @POST("v1/meals/plan/filter")
    fun getMealPlan(@Body request: MealPlanRequest): Call<MealPlanResponse>

    @POST("v1/meals/choice/filter")
    fun getMealChoice(@Body request: MealChoiceRequest): Call<MealChoiceResponse>

    @POST("v1/meals/choice")
    fun vote(@Body request: VoteRequest): Call<VoteResponse>

    @PUT("v1/meals/choice")
    fun updateVote(@Body request: VoteRequest): Call<VoteResponse>

    @DELETE("v1/meals/choice/{choiceId}")
    fun deselectMeal(@Path("choiceId") choiceId: String): Call<MdocResponse>

    @PUT("v1/account/privacy")
    fun confirmPrivacy(): Call<MdocResponse>

    @PUT("v1/account/terms")
    fun confirmTermsAndCondition(): Call<MdocResponse>

    @POST("v2/patient/appointments/search")
    fun searchAppointments(@Body body: JsonObject): Call<AppointmentResponse>

    @POST("v2/common/diaries/items/search")
    fun getAllDiaries(@Body request: DiarySearchRequest?): Call<DiaryResponse>

    @POST("v2/common/diaries/items")
    fun createDiary(@Body body: JsonObject): Call<DiaryResponse>
    
    @PUT("v2/common/diaries/items/{id}")
    fun updateDiary(@Path("id") id: String,@Body body: JsonObject): Call<DiaryResponse>

    @PUT("v2/common/diaries/items/{id}")
    fun updateDiary(@Path("id") id: String, @Body body: DiaryListUpdate): Call<deleteDiaryPjo>

    @DELETE("v2/common/diaries/items/{id}")
    fun deleteDiary(@Path("id") id: String): Call<deleteDiaryPjo>

    @POST("v2/common/files/my/search")
    fun searchFiles(@Body request: SearchFileRequest): Single<SearchFilesResponse>

    @Multipart
    @POST("v2/common/files/upload")
    fun uploadFile(@Part("id") description: RequestBody, @Part
    file: MultipartBody.Part): Call<FileUpdateResponse>

    @PUT("v2/common/files/{id}")
    fun updateFile(@Path("id") id: String, @Body request: FileUpdate): Call<MdocResponse>

    @DELETE("v2/common/files/{id}")
    fun deleteFile(@Path("id") id: String): Single<MdocResponse>

    @PUT("v2/patient/careplan/activity")
    fun changeActivityStatus(@Body body: JsonObject): Call<MdocResponse>

    @PUT("v1/auth/device/token")
    fun updateFirebaseToken(@Body request: FirebaseTokenRequest): Call<MdocResponse>


    @POST("v2/common/notifications/seen")
    fun setNotificaitonSeen(@Body request: NotificationSeenRequest): Call<MdocResponse>

    @PUT("v2/patient/appointments/cancel/{id}")
    fun cancelAppointment(@Path("id") id: String): Call<MdocResponse>

    @POST("v2/patient/coding/search")
    fun getCoding(@Body request: CodingRequest): Call<CodingResponse>

    @POST("v2/patient/coding/search")
    fun rxGetCoding(@Body request: CodingRequest): Single<CodingResponse>

    @POST("v2/patient/user/metadata")
    fun postMetadata(@Body request: MetaDataPostRequest): Call<MdocResponse>

    @POST("v2/patient/fhir/device/search")
    fun getDevicesForUser(@Body request: DevicesForUserRequest): Call<DevicesResponse>

    @POST("v2/patient/fhir/device")
    fun createDevice(@Body request: CreateDeviceRequest): Call<SingleDeviceResponse>

    @DELETE("v2/patient/fhir/device/{device_id}")
    fun removeDevice(@Path("device_id") deviceId: String): Call<MdocResponse>

    @POST("v2/patient/observations/many")
    fun createObservations(@Body request: List<Observation>): Call<MdocResponse>

    @POST("v2/patient/observation/statistics")
    fun getObservationStatistics(@Body
                                 request: ObservationStatisticsRequest): Call<ObservationResponse>

    @GET("v2/public/configurations/fhir/device")
    fun checkDeviceSettings(): Call<ConfigurationResponse>

    @PATCH("v2/common/files/{fileId}/share")
    fun shareFile(@Path("fileId") id: String): Call<MdocResponse>

    @GET("v2/patient/polls/answers/history")
    fun getAnsweredQuestionnaires(
            @Query("from") from: String, @Query("to") to: String, @Query("pollVotingActionId")
            pollVotingActionId: String, @Query("pollId") pollId: String, @Query("includeAnswers")
            includeAnswers: Boolean?): Call<QuestionnaireAnswersResponse>

    @POST("v2/patient/mental-goals")
    fun postMentalGoals(@Body request: MentalGoalsRequest): Call<SingleMentalGoalResponse>

    @PUT("v2/patient/mental-goals/{goal_id}/status")
    fun updateMentalGoalsStatus(@Path("goal_id") goalId: String, @Body
    request: MentalGoalsRequest): Call<SingleMentalGoalResponse>

    @PUT("v2/patient/mental-goals/{goal_id}")
    fun updateMentalGoal(@Path("goal_id") goalId: String, @Body
    request: MentalGoalsRequest): Call<SingleMentalGoalResponse>

    @POST("v2/patient/mental-goals/search")
    fun searchMentalGoals(@Body request: MentalGoalsRequest): Single<MentalGoalsResponse>

    @POST("v2/patient/mental-goals/search/by-gamification")
    fun searchGamificationMentalGoals(@Body
                                      request: MentalGoalsRequest): Single<GamificationResponse>

    @POST("v2/patient/polls/{pollId}/diary/self-assign")
    fun assignQuestionnaire(@Path("pollId") pollId: String): Call<SelfAssignResponse>

    @POST("v1/clinics/search")
    fun searchClinic(@Body request: SearchClinicRequest): Call<SearchClinicResponse>

    @GET("v2/common/media-light/{id}?fields=*mediaUrl*")
    fun getMediaById(@Path("id") id: String): Call<MediaResponse>

    @GET("v2/public/configurations/fhir/device")
    suspend fun coroutineCheckDeviceSettings(): ConfigurationResponse

    @GET("v2/patient/fhir/device/fhir-conf")
    suspend fun coroutineFhirConfiguration(): FhirConfigurationResponse

    @POST("v2/patient/fhir/device/search")
    suspend fun coroutineGetDevicesForUser(@Body request: DevicesForUserRequest): DevicesResponse

    @GET("v2/patient/observations/latest")
    suspend fun coroutineGetLatestObservations(): ObservationsResponse

    @GET("v2/patient/user/mystay/{caseId}")
    suspend fun coroutineGetClinicByCase(@Path("caseId") caseId: String): MyStayWidgetResponse

    @POST("v2/patient/appointments/search")
    suspend fun coroutineSearchAppointments(@Body body: BookingRequest): AppointmentResponse

    @GET("v2/public/configurations/meal/info")
    suspend fun coroutineGetMealInfo(): InfoResponse

    @POST("v1/meals/plan/filter")
    suspend fun coroutineGetMealPlan(@Body request: MealPlanRequest): MealPlanResponse

    @POST("v1/meals/choice/filter")
    suspend fun couroutineGetMealChoice(@Body request: MealChoiceRequest): MealChoiceResponse

    @GET("/api/v2/common/files/{id}")
    suspend fun getFileById(@Path("id") id: String): FileItemResponse
    //GENERAL NETWORK CALLS
    @POST("v2/patient/coding/search")
    suspend fun getCodingKt(@Body request: CodingRequest): CodingResponse

    @POST("v2/patient/user/masterdata")
    suspend fun postMasterDataKt(@Body request: MetaDataRequest): MdocResponse

    @POST("v1/clinics/search")
    suspend fun searchClinics(@Body request: AllClinicsRequest): SearchClinicResponse

    @POST("v2/common/case/providers/requests/generate")
    suspend fun generateCaseWithClinicId(@Body clinicId: ClinicSelectionRequest) : ResponseBody

    @GET("v2/clinics/all")
    suspend fun getSelfRegClinics() : SearchClinicResponse

    @PUT("v2/patient/user/{userId}/birthdate/{birthdate}")
    fun setUserBirthdate(@Path("userId") userId: String, @Path("birthdate") birthdate: String): Call<MdocResponse>

    @GET("v2/common/specialties/{specialtyProvider}")
    suspend fun getSpecialities(@Path("specialtyProvider") specialtyProvider: String): SpecialtiesResponse

    @POST("v2/patient/appointments/freeslots")
    suspend fun getFreeSlots(@Body request: FreeSlotsRequest): FreeSlotsResponse

    @POST("v2/patient/appointments/book")
    suspend fun bookAppointment(@Header("delegated-clinic") delegatedClinic: String?, @Body request: BookAppointmentRequest): Response<MdocResponse>

    @PUT("v2/patient/appointments/update/{id}")
    suspend fun updateAppointment(@Path("id") id: String, @Header("delegated-clinic") delegatedClinic: String?, @Body request: BookAppointmentRequest): Response<MdocResponse>

    @PUT("v2/patient/appointments/cancel/{id}")
    suspend fun cancelAppointmentKt(@Path("id") id: String): MdocResponse

    @GET("v2/patient/appointments/{appointmentId}")
    suspend fun getReschedulingAppointmentId(@Path("appointmentId") appointmentId: String?): ReschedulingResponse

    @PUT("v2/patient/appointments/confirm/rescheduling/{appointmentId}")
    suspend fun confirmRescheduling(@Path("appointmentId") id: String, @Body request: NewAppointmentSchedule): ReschedulingResponse

    @GET("v2/common/xconcept/resources")
    suspend fun getListOfDoctors(): DoctorsResponse
    //MEDICATION PLAN MODULE
    @GET("/api/v2/patient/medication-requests?sortField=authoredOn&sortDirection=DESC&fields=*medication*")
    suspend fun getMedicationOnDemandEvents(): MedicationAdministrationResponse

    @GET("v2/patient/medications/")
    suspend fun getMedications(@Query("query") query: String?,
                               @Query("pagingDisabled") pagingDisabled: Boolean?,
                               @Query("additionalData.fields")
                                   additionalDataFields: String?,
                               @Query("additionalData.fields")
                                   additionalDataFieldsTwo: String?,
                               @Query("additionalData.fields")
                                   additionalDataFieldsThree: String?,
                               @Query("limit") limit: Int? = null
    ): MedicationsResponse

    @GET("v2/patient/medications/details/{id}/")
    fun getMedicineDetails(
                                @Path("id")
                                id: String?,
                                @Query("additionalData.fields")
                               additionalDataFields: String?,
                               @Query("additionalData.fields")
                               additionalDataFieldsTwo: String?): Call<MedicineDetail>


    @GET("v2/patient/medications")
    suspend fun getMedications(@Query("query") query: String?): MedicationsResponse

    @POST("v2/patient/medications")
    suspend fun postMedication(@Body request: Medication): MedicationItemResponse

    @GET("v2/patient/medications/image/{code}")
    suspend fun getMedicationImage(@Path("code") pznCode: String): MedicationImageResponse

    @GET("v2/patient/medication-administration?additionalData.fields=*medication*&additionalData.fields=*medicationRequest*")
    suspend fun getMedicationAdministration(@Query("effectivePeriodStartGTE")
                                                effectivePeriodStartGTE: String? = null,
                                            @Query("effectivePeriodStartLT")
                                                effectivePeriodStartLT: String? = null,
                                            @Query("subject")
                                                subject: String? = null): MedicationAdministrationResponse

    @POST("v2/patient/medication-requests")
    suspend fun postMedicationPlan(@Body request: MedicationPlanRequest): MedicationPlanResponse

    @DELETE("v2/patient/medication-requests/{id}")
    suspend fun deleteMedicationRequest(@Path("id") medicationRequestId: String)

    @GET("v2/patient/coding-light")
    suspend fun getCodingLight(@Query("system") query: String?): CodingResponseKt

    @GET("v2/patient/coding-light")
    fun getCodingLightCall(@Query("system") query: String?): Call<CodingResponseKt>

    @GET("v2/patient/image-coding-light")
    suspend fun getImageCodingLight(@Query("system") query: String?,
                                    @Query("sortField")
                                    sortField: String? = null): CodingResponseKt

    // ENTERTAINMENT MODULE
    @GET("/api/v2/common/media-light/statistic/entertainment?fields=*reading-history*")
    suspend fun getEntertainmentStatistic(@Query("languages") languages: String,
                                          @Query("categories")
                                          categories: String): ResponseSearchResultsMediaStatisticResponseDto_

    @GET("/api/v2/media/{id}")
    @Streaming
    fun getMediaById(@Path("id") id: String, @Query("pageNumber") pageNumber: String): Call<ResponseBody>

    @GET("/api/v2/common/media-light/entertainment?fields=*reading-history*")
    fun getEntertainmentStatisticAll(@Query("category") category: String): Call<ResponseSearchResultsMediaLightDTO_>

    @GET("/api/v2/common/media-light/entertainment?fields=*reading-history*")
    suspend fun getEntertainmentStatisticAllSearch(@Query("query") search: String): ResponseSearchResultsMediaLightDTO_?

    @POST("/api/v2/patient/entertainment/tracking")
    fun trackEntertainment(@Body request: MediaTrackingRequest): Call<ResponseBody>

    @POST("/api/v2/patient/media-reading-history")
    suspend fun createMagazineReadingHistory(
            @Body request: MediaReadingHistoryUpsert): ResponseMediaReadingHistoryResponseDto_

    @PUT("/api/v2/patient/media-reading-history/{id}")
    suspend fun updateMagazineReadingHistory(@Path("id") id: String?,
                                             @Body
                                             request: MediaReadingHistoryUpsert): ResponseMediaReadingHistoryResponseDto_

    @GET("/api/v2/patient/media-reading-history")
    suspend fun getMagazineReadingHistory(
            @Query("mediaId") id: String?): ResponseSearchResultsMediaReadingHistoryResponseDto_

    @GET("/api/v2/patient/media-reading-history?fields=*media*&sortField=uts&sortDirection=DESC")
    suspend fun getMagazinesReadingHistory(): ResponseSearchResultsMediaReadingHistoryResponseDto_
    //MESSAGES MODULE
    @GET("v2/patient/conversations")
    suspend fun getConversations(@Query("additionalData.fields") additonDataReason: String?,
                                 @Query("additionalData.fields") additonDataLetterbox: String?,
                                 @Query("additionalData.fields") additonDataParticipants: String?,
                                 @Query("query") query: String?,
                                 @Query("archived") archived: Boolean?,
                                 @Query("sortField") sortField: String?,
                                 @Query("sortDirection") sortDirection: String?): ConversationResponse

    @POST("v2/patient/coding/search")
    suspend fun getCodingSuspend(@Body request: CodingRequest): CodingResponse

    @POST("v2/patient/messaging")
    suspend fun sendMessage(@Body request: MessageRequest): NewMessageResponse

    @GET("v2/patient/messaging")
    suspend fun getMessages(@Query("additionalData.fields") additonDataReason: String?,
                            @Query("additionalData.fields") additonDataLetterbox: String?,
                            @Query("additionalData.fields") additonDataFiles: String?,
                            @Query("additionalData.fields") additonDataParticipants: String?,
                            @Query("sortField") sortField: String?,
                            @Query("sortDirection") sortDirection: String?,
                            @Query("conversationId") conversationId: String?,
                            @Query("herf") href: String?): MessagesResponse

    @PUT("v2/patient/conversations/{id}/seen")
    suspend fun setMessageSeen(@Path("id") id: String?): MdocResponse
    //PROFILE MODULE
    @POST("v2/patient/organization/search")
    suspend fun getInsurance(@Body request: InsuranceRequest): InsuranceResponse

    @GET("v2/patient/user")
    suspend fun getPatientDetails(): PatientDetailsResponse

    @GET("v2/patient/user/professional/{id}")
    suspend fun getProfessionalDetails(@Path("id") id: String): PatientDetailsResponse

    @GET("v2/patient/encounter/case/latest")
    suspend fun getLatestCase(): EncounterResponse

    @GET("/api/v2/patient/coverage/account/{id}")
    suspend fun getCoverageById(@Path("id") accountId: String): CoverageResponse

    @PATCH("v2/common/auth/account/emergencyinfo")
    suspend fun patchEmergencyInfo(@Body request: EmergencyInfoRequest): MdocResponse

    @PATCH("v2/common/auth/account/contacts")
    suspend fun patchEmergencyContacts(@Body request: ContactsRequest): MdocResponse

    @PATCH("v2/common/auth/account/printplan")
    suspend fun patchTherapyPrint(@Body request: TherapyPrintRequest): MdocResponse

    //MEDIA MODULE
    @GET("v2/common/media-light/statistic")
    suspend fun getMediaStatistics(): MediaStatisticsResponse

    @GET("v2/common/media-light")
    suspend fun searchMedia(@Query("category") category: String?,
                            @Query("fields") fields: String? = null,
                            @Query("sortField") sortField: String = "content.title",
                            @Query("sortDirection") sortDirection: String = "ASC"
    ): MediaLightSearchResponse

    @GET("v2/common/media-light/{id}?fields=*mediaUrl*")
    suspend fun getMediaDetails(@Path("id") id: String): MediaDetailResponse
    //PATIENT JOURNEY MODULE
    @GET("v2/patient/careplan-light")
    suspend fun getCarePlans(@Query("category") category: String?,
                             @Query("sortField") sortField: String = "meta.lastUpdated",
                             @Query("sortDirection") sortDirection: String = "ASC",
                             @Query("periodEndGTE")
                                 periodEndGTE: String? = null,
                             @Query("periodEndLTE")
                                 periodEndLTE: String? = null,
                             @Query("limit")
                                 limit: Int? = null): CarePlanResponse

    @GET("v2/patient/careplan-light/{careplan_id}")
    suspend fun getJourneyDetails(@Path("careplan_id") carePlanId: String,
                                  @Query("fields") activityDescription: String? = null,
                                  @Query("fields") childCarePlans: String? = null): CarePlanDetailsResponse
    //CHECKLIST MODULE
    @GET("v2/patient/polls/answers/all")
    suspend fun getUserAnswers(): UserAnswersResponse

    @GET("v2/patient/polls/{pollAssignId}")
    suspend fun coroutineGetQuestionnaireById(@Path("pollAssignId") pollAssignId: String?): QuestionnaireDetailsResponse

    @GET("v2/patient/polls/search")
    suspend fun getQuestionariesSuspend(@Query("ignoreDiaryEntries") ignoreDiaryEntries: Boolean): QuestionnaireResponse
    //SAVE EMAIL AND SKIP
    @POST("v2/patient/oauth2/users/resend")
    fun saveEmail(@Body request: SaveEmailRequest): Call<MdocResponse>

    @PUT("v2/patient/user/skip_email_request/{patientId}")
    fun skipEmailDialog(@Path("patientId") patientId: String): Call<MdocResponse>

    // FILES MODULE V2
    @GET("/api/v2/common/files-light/storage")
    suspend fun getFilesStorage(): ResponseSearchResultsFileStorageDocumentImpl_

    //EXTERNAL SERVICES
    @GET("v2/patient/external/system")
    suspend fun getExternalServices(): ExternalServicesResponse

    @POST("v2/patient/mental-goals/search/by-gamification")
    suspend fun searchGamificationMentalGoalsSuspend(@Body request: MentalGoalsRequest): GamificationResponse

    @POST("v2/patient/mental-goals/search")
    suspend fun searchMentalGoalsSuspend(@Body request: MentalGoalsRequest): MentalGoalsResponse

    @PUT("v2/common/auth/account")
    suspend fun sendChosenUnitSuspend(@Body request: UnitPersistenceRequest): Response<UnitPersistenceResponse>

    //BOOKING MODULE
    @GET("v2/patient/encounter/by-clinic")
    suspend fun getEncounters(): AppointmentDetailsResponse

    // EXPORTS
    @GET("/api/v2/patient/mental-goals/download/{id}/{type}")
    @Streaming
    suspend fun downloadMentalGoals(@Path("id") id: String, @Path("type") type: String): ResponseBody

    @POST("/api/v2/common/diaries/items/download/{type}")
    @Streaming
    suspend fun downloadDiaries(@Path("type") type: String, @Body body: DiaryExportRequest): ResponseBody

    @POST("/api/v2/patient/medication-requests/download/{type}")
    @Streaming
    suspend fun downloadMedications(@Path("type") type: String, @Body body: MedicationExportRequest): ResponseBody

    @POST("/api/v2/patient/observation/statistics/download/{type}")
    @Streaming
    suspend fun downloadVitals(@Path("type") type: String, @Body body: ObservationStatisticsRequest): ResponseBody

    //VIDEO CONFERENCE MODULE
    @GET("v2/patient/appointments/{appointmentId}")
    fun getAppointmentById(@Path("appointmentId") appointmentId: String?): Call<SingleAppointmentResponse>

    @POST("v2/patient/appointments/tokbox-token/{appointmentId}")
    fun getTokboxToken(@Path("appointmentId") appointmentId: String): Call<TokBoxResponse>

    @PUT("v2/patient/appointments/join/{appointmentId}")
    fun joinCall(@Path("appointmentId") appointmentId: String): Call<ResponseBody>


    @PUT("v2/patient/appointments/left/{id}")
    suspend fun leaveCall(@Path("id") appointmentId: String): MdocResponse

    //Welcome message
    @GET("v2/clinics/details/welcomemsg/{clinicId}")
    suspend fun getWelcomeMessage(@Path("clinicId") clinicId: String?): MessageResponse

    @GET("v2/clinics/current")
    suspend fun getCurrentClinic(): CurrentClinicResponse

    //Covid module
    @GET("v2/patient/health/status")
    suspend fun getHealthStatus(@Query("additionalData.fields")
                                    additionalDataFields: String?): HealthStatusResponse

    @POST("v2/patient/health/status")
    suspend fun postHealthStatus(@Body request: HealthStatus): SingleHealthStatusResponse

    @GET("v2/patient/user-light")
    suspend fun getListOfProfessionals(@Query("query") query: String?,
                                       @Query("adminSearch") adminSearch: Boolean? = true): UsersResponse

    @GET("/api/v2/patient/user/device/{deviceName}/reset")
    fun fetchSecret(@Path("deviceName") deviceName: String): Call<SecretModel>

    // Digital Signature
    @GET("v2/digital-signature/next-action/{id}")
    suspend fun getSignatureNextAction(@Path("id") id: String): SignatureNextActionResponse

    @GET("v2/digital-signature/document/patient/{id}")
    fun getSignaturePatientDocument(@Path("id") id: String):  Call<ResponseBody>

    @GET("v2/digital-signature/documentInfo/patient/{id}")
    suspend fun getSignaturePatientDocumentInfo(@Path("id") id: String): SignatureDocumentInfoResponse

    @POST("v2/digital-signature/sign")
    suspend fun postSignaturePatientSign(@Body request: SignatureDocumentInfoData): SignatureSignResponse

    @POST("iam-service/v1/user/login")
    fun authenticateUser(@Body loginRequest: LoginUser?): Call<LoginResponseData>
    

    @POST("iam-service/v1/user/login")
    fun authenticateUserCaptcha(@Query("captcha") lang: String?, @Body loginRequest: LoginUser?): Call<LoginResponseData>

    @POST("iam-service/v1/user/forgot-password")
    fun forgetPassword(@Query("email") email: String?, @Body forgetPassword: ForgetPasswordRequestData?): Call<ForgetPasswordResponseData?>?


    @POST("iam-service/v1/user/register")
    fun registration(@Body forgetPassword: RegisterRequestData?): Call<RegistrationResponseData?>?

    @GET("iam-service/v1/coding/policy/all")
    fun getImprintData(@Query("lang") lang: String?): Call<ImprintResponse?>?

    @POST("iam-service/v1/user/reset-password")
    fun updatePassword(@Body updatePassword: UpdatePasswordRequest?, @Query("lang") lang: String?): Call<UpdatePasswordResponseData?>?

    @POST("iam-service/v1/user/activate")
    fun isAccountActivated(@Body updatePassword: UpdatePasswordRequest?, @Query("lang") lang: String?): Call<UpdatePasswordResponseData?>?

    @GET("iam-service/v1/captcha")
    fun getCaptcha(): Call<CaptchaResponse?>?




    @GET("/api/v2/patient/user/device/{deviceName}/reset")
    fun fetchConfiguration(@Path("deviceName") deviceName: String): Call<SecretModel>
}
package de.mdoc.data.create_bt_device

data class ValueQuantityObservation(
        val code: String,
        val system: String,
        val unit: String,
        val value: Double,
        val display:String
)
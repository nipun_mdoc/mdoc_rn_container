package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ema on 1/16/17.
 */

public class UserAnswerData implements Serializable {

    private String pollId;
    private String pollAssignId;
    private String userId;
    private String id;
    private QuestionnaireDetailsData pollDetails;
    private ArrayList<Answer> answers;
    private String pollQuestionId;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public String getPollQuestionId() {
        return pollQuestionId;
    }

    public void setPollQuestionId(String pollQuestionId) {
        this.pollQuestionId = pollQuestionId;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public QuestionnaireDetailsData getPollDetails() {
        return pollDetails;
    }

    public void setPollDetails(QuestionnaireDetailsData pollDetails) {
        this.pollDetails = pollDetails;
    }

    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }
}

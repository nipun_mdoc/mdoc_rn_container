package de.mdoc.modules.appointments.video_conference.data

data class SingleAppointmentResponse(
        val code: String? = "",
        val `data`: SingleAppointmentData? = SingleAppointmentData(),
        val message: String? = "",
        val timestamp: Long? = 0
                                    )

data class SingleAppointmentData(
        val appointmentDetails: AppointmentDetails? = AppointmentDetails(),
        val cts: Long? = 0,
        val deleted: Boolean? = false,
        val encounter: List<Any?>? = listOf(),
        val id: String? = "",
        val metadataNeeded: Boolean? = false,
        val metadataPopulated: Boolean? = false,
        val uts: Long? = 0
                                ) {

    data class AppointmentDetails(
            val allDay: Boolean? = false,
            val appointmentType: String? = "",
            val clinicId: String? = "",
            val comment: String? = "",
            val description: String? = "",
            val end: Long? = 0,
            val identifiers: List<Any?>? = listOf(),
            val participants: List<Participant?>? = listOf(),
            val reminder: Reminder? = Reminder(),
            val resourceType: String? = "",
            val showEndTime: Boolean? = false,
            val specialty: String? = "",
            val start: Long? = 0,
            val timeVisible: Boolean? = false,
            val title: String? = "",
            val tokboxSessionId: String? = "",
            val visible: Boolean? = false
                                 ) {

        data class Participant(
                val firstName: String? = "",
                val lastName: String? = "",
                val status: String? = "",
                val type: String? = "",
                val username: String? = ""
                              )

        data class Reminder(
                val notificationIds: List<String?>? = listOf(),
                val time: Long? = 0
                           )
    }
}
package de.mdoc.pojo;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class CreateUserDeviceResponse extends MdocResponse {

    private QRData data;

    public QRData getData() {
        return data;
    }

    public void setData(QRData data) {
        this.data = data;
    }
}

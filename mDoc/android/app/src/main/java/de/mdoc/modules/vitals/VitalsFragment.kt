package de.mdoc.modules.vitals

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentVitalsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.network.RestClient
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_vitals.*

class VitalsFragment: NewBaseFragment(), CommonDialogFragment.CancelListener,CommonDialogFragment.OnButtonClickListener {

    override val navigationItem: NavigationItem = NavigationItem.VitalsObservations

    private val vitalsViewModel by viewModel {
        VitalsViewModel(
                RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentVitalsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_vitals, container, false)

        binding.lifecycleOwner = this
        binding.model = vitalsViewModel

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleOnBackPressed {
            findNavController().popBackStack(R.id.fragmentVitals, true)
            findNavController().popBackStack(R.id.fragmentDevices, true)
        }

        setHasOptionsMenu(true)
        shouldShowDisclaimer()
        vitalsViewModel.getLatestObservations()
        setFabListener()
    }

    private fun setFabListener() {
        fab_device_add?.setOnClickListener {
           findNavController().navigate(R.id.action_to_fragmentMeasurementDialog)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_my_vitals)
        menu.findItem(R.id.unitPicker).isVisible = !MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_VITALS, MdocConstants.HIDE_VITALS_UNIT)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.unitPicker) {
            val unitItems = AppPersistence.fhirConfigurationData?.filter { it.allUnits != null && it.allUnits.size > 1 }
            val action = VitalsFragmentDirections.actionToFragmentUnitPicker(unitItems?.toTypedArray())
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun shouldShowDisclaimer() {
        val moduleName = resources.getString(R.string.my_statistics)
        val list = resources.getStringArray(R.array.disclaimer_modules)
        if (list.contains(moduleName)&& (MdocAppHelper.getInstance().disclaimerSeenVitals == false)) {
            showDisclaimerDialog()
        }
    }

    private fun showDisclaimerDialog() {
        CommonDialogFragment.Builder()
            .title(resources.getString(R.string.disclaimer))
            .description(resources.getString(R.string.disclaimer_body))
            .setOnCancelListener(this)
            .setOnClickListener(this)
            .setPositiveButton(resources.getString(R.string.disclaimer_accept))
            .navigateBackOnCancel(true)
            .build()
            .showNow(childFragmentManager, "")
    }

    override fun onCancelDialog() {
        findNavController().popBackStack(R.id.fragmentVitals, true)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        if (button == ButtonClicked.POSITIVE) {
            MdocAppHelper.getInstance().disclaimerSeenVitals = true
        }
    }
}
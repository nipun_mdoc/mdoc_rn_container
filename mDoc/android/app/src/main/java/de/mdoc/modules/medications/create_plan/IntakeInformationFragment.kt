package de.mdoc.modules.medications.create_plan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentIntakeInformationBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel

class IntakeInformationFragment internal constructor(val viewModel: CreatePlanViewModel):
        NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentIntakeInformationBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_intake_information, container, false)
        binding.createPlanViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    companion object {
        val TAG = IntakeInformationFragment::class.java.canonicalName
    }
}

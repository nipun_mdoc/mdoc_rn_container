package de.mdoc.modules.messages.messages

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.messages.data.Conversation
import de.mdoc.modules.messages.data.ListItemMessage
import de.mdoc.modules.messages.data.MessageRequest
import de.mdoc.network.request.CodingRequest
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import java.util.*

class MessagesViewModel(private val mDocService: IMdocService): ViewModel() {

    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.ms")
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var groupList: MutableLiveData<List<CodingItem>> =
            MutableLiveData(listOf())
    var reasonList: MutableLiveData<List<CodingItem>> =
            MutableLiveData(emptyList())
    var reason = MutableLiveData("")
    var description = MutableLiveData("")
    var letterbox = MutableLiveData("")
    var recipient = MutableLiveData("")
    var hasError = MutableLiveData(false)
    var messageSent = MutableLiveData(false)
    var isClosedConverstions = MutableLiveData(false)
    var conversationId = MutableLiveData("")
    var messagesList: MutableLiveData<ArrayList<ListItemMessage>> =
            MutableLiveData(arrayListOf())
    var messageText = MutableLiveData("")
    var messageHref = MutableLiveData("")
    var letterboxTitle = MutableLiveData("")
    var reasonTitle = MutableLiveData("")
    var isPermissionDenied = MutableLiveData(false)
    var query: MutableLiveData<String> = MutableLiveData()

    init {
        getCodingLetterBox()
    }

    private fun getCodingLetterBox() {
        viewModelScope.launch {
            try {
                isLoading.value = true

                groupList.value = mDocService.getCodingSuspend(
                        CodingRequest("https://mdoc.one/coding/messaging/letterbox")).data.list

                isLoading.value = false
                if (groupList.value?.size == 0) {
                    isShowNoData.value = true
                }
                else {
                    isShowContent.value = true
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }

    fun getCodingAPI() {
        viewModelScope.launch {
            try {
                isLoading.value = true

                groupList.value = mDocService.getCodingSuspend(
                        CodingRequest("https://mdoc.one/coding/messaging/letterbox")).data.list

                isLoading.value = false
                if (groupList.value?.size == 0) {
                    isShowNoData.value = true
                    isShowContent.value = false
                }
                else {
                    isShowContent.value = true
                    isShowNoData.value = false
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }

    fun getProfessionalsApi() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                isShowLoadingError.value = false

                groupList.value = mDocService.getListOfProfessionals(
                        query.value).data?.list?.map {
                    val fullName = "${it.publicUserDetails?.firstName} ${it
                        .publicUserDetails?.lastName}"
                    CodingItem(it.userId, fullName, fullName)
                }

                isLoading.value = false
                if (groupList.value?.size == 0) {
                    isShowNoData.value = true
                    isShowContent.value = false
                }
                else {
                    isShowContent.value = true
                    isShowNoData.value = false
                }
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
            }
        }
    }

    fun getCodingReason() {
        viewModelScope.launch {
            try {
                reasonList.value = mDocService.getCodingSuspend(
                        CodingRequest("https://mdoc.one/coding/messaging/reason")).data.list
            } catch (e: java.lang.Exception) {
                Timber.d("Exception %s", e.message)
            }
        }
    }

    fun startConversation() {
        val conversation = Conversation().also {
            it.letterbox = letterbox.value
            it.reason = reason.value
            it.type = "Patientenportal"
            it.description = description.value
        }
        val request = MessageRequest().also {
            it.receiverId = recipient.value
            it.text = description.value
            it.priority = 0
            it.conversation = conversation
        }

        viewModelScope.launch {
            try {
                mDocService.sendMessage(request).data.conversationId
                messageSent.value = true
            } catch (e: java.lang.Exception) {
                hasError.value = true
            }
        }
    }

    fun getMessages() {
        viewModelScope.launch {
            try {
                isLoading.value = true

                messagesList.value = mDocService.getMessages("*reason*",
                        "*letterbox*",
                        "*files*",
                        "*participant*",
                        "date",
                        "ASC",
                        conversationId.value, href = null).data.list

                isLoading.value = false


                if (messagesList.value?.size == 0) {
                    isShowNoData.value = true
                    isShowContent.value = false
                }
                else {
                    isShowContent.value = true
                    isShowNoData.value = false
                }
            } catch (e: java.lang.Exception) {
                isShowLoadingError.value = true
                isLoading.value = false
                isShowContent.value = false
                isShowNoData.value = false
                isPermissionDenied.value = false
            }
        }
    }

    fun sendMessage() {
        val request = MessageRequest().also {
            it.text = messageText.value
            it.priority = 0
            it.conversationId = conversationId.value
        }

        viewModelScope.launch {
            try {
                mDocService.sendMessage(request)
                messageText.value = ""
                getMessages()
            } catch (e: java.lang.Exception) {
            }
        }
    }

    fun setMessageSeen() {
        viewModelScope.launch {
            try {
                mDocService.setMessageSeen(conversationId.value)
            } catch (e: java.lang.Exception) {
                Timber.d("Exception %s", e.message)
            }
        }
    }

    fun clearContent() {
        isShowNoData.value = true
        isShowContent.value = false
        isLoading.value = false
        isShowLoadingError.value = false
    }

    fun getDigitalSignatureNextActionData(id: String, onSuccess: (String?) -> Unit, onError: (Throwable) -> Unit ) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val data = mDocService.getSignatureNextAction(id).data
                isLoading.value = false
                onSuccess(data)
            }catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }
}
package de.mdoc.network.request.export

data class MedicationExportRequest(val sortField: String = "authoredOn", val sortDirection: String = "DESC")
package de.mdoc.data.questionnaire

data class Question(
        val childQuestionsIds: List<Any>,
        val clinicId: String,
        val id: String,
        val index: Int,
        val ordinal: String,
        val pollId: String,
        val pollQuestionType: String,
        val pollSectionId: String,
        val questionData: QuestionData,
        val showContent: Boolean,
        val uuid: String
)
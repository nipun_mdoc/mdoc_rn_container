package de.mdoc.network.response

data class FinishQuestionnaireResponse(
        val code: String,
        val timestamp: Long,
        val description: String,
        val message: String,
        var data: FinishQuestionnaireResponseData
) {

    fun isCompleted(): Boolean {
        return data.pollVotingActionDetails.completed
    }

    fun getType() : String {
        return data.pollDetails.pollType
    }

    fun getClosingMessage(language: String) : String? {
        return data.pollDetails.closingMessage.get(language)
    }

}

data class FinishQuestionnaireResponseData(
        val cts: Long,
        val uts: Long,
        val id: String,
        val pollId: String,
        val pollDetails: PollDetails,
        val pollVotingActionDetails: PollVotingActionDetails
)

data class PollDetails(
        val pollId: String,
        val pollType: String,
        val closingMessage: Map<String, String>
)

data class PollVotingActionDetails(
        val pollId: String,
        val caseId: String,
        val completed: Boolean,
        val language: String
)
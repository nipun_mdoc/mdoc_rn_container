package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.LinkedHashMap

@Parcelize
data class Measurement(
        val components: List<Component>? = null,
        val imageCoding: ImageCoding? = null,
        val measure: Measure? = null,
        val unit: Unit? = null,
        val measureConfig: ArrayList<MeasureConfig>? = null,
        val graphType: String? = null,
        val min: String? = null,
        val max: String? = null,
        val allUnits: List<AllUnits>? = null
                      ): Parcelable {

    fun displayMeasurement(): String {
        if (measureConfig != null) {
            for (item in measureConfig) {
                if (item.name == "displayMeasure") {
                    return item.display?:""
                }
            }
        }
        return ""
    }

    fun measureTimesPrompt(): String {
        if (measureConfig != null) {
            for (item in measureConfig) {
                if (item.name == "measureTimesPrompt") {
                    return item.display?:""
                }
            }
        }
        return ""
    }

    fun measurePrompt(): String {
        if (measureConfig != null) {
            for (item in measureConfig) {
                if (item.name == "measurePrompt") {
                    return item.display?:""
                }
            }
        }
        return ""
    }

    fun getFirstComponentMeasure(): String? {
        val comp = components?.getOrNull(0)
        return comp?.measurePrompt()
    }

    fun getFirstComponentDisplayMeasure(): String? {
        val comp = components?.getOrNull(0)
        return comp?.displayMeasurement()
    }

    fun getFirstComponentTime(): String? {
        val comp = components?.getOrNull(0)
        return comp?.measureTimesPrompt()
    }
}


@Parcelize
data class AllUnits(val unit: Unit? = null, val formulaTranslation: FormulaTranslation? = null): Parcelable

@Parcelize
data class FormulaTranslation(val map: LinkedHashMap<String, String>? = null): Parcelable

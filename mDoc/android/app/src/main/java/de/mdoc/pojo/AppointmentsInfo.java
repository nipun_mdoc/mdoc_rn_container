package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AppointmentsInfo  implements Serializable {

    @SerializedName("specialty")
    @Expose
    private String specialty;
    @SerializedName("specialtyType")
    @Expose
    private String specialtyType;
    @SerializedName("deleteExecuted")
    @Expose
    private boolean deleteExecuted;

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getSpecialtyType() {
        return specialtyType;
    }

    public void setSpecialtyType(String specialtyType) {
        this.specialtyType = specialtyType;
    }

    public boolean isDeleteExecuted() {
        return deleteExecuted;
    }

    public void setDeleteExecuted(boolean deleteExecuted) {
        this.deleteExecuted = deleteExecuted;
    }
}

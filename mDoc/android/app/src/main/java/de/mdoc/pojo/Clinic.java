package de.mdoc.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ema on 11/15/16.
 */

public class Clinic implements Serializable{

    @SerializedName("name")
    private String name;
    @SerializedName("room")
    private String room;
    @SerializedName("id")
    private String id;
    @SerializedName("description")
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

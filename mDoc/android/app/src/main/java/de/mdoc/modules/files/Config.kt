package de.mdoc.modules.files

class Config {

    companion object {
        const val STORAGE_ANIMATION_DURATION:Long = 900
        const val PERCENTAGE_RED_STATE = 90

        const val MB_30 = 1024 * 1024 * 30
        const val MAX_UPLOAD_COUNT: Long = 100
    }
}
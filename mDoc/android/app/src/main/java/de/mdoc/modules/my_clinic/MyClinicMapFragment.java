package de.mdoc.modules.my_clinic;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.pojo.ClinicDetails;
import de.mdoc.util.MdocUtil;

/**
 * Created by ema on 9/25/17.
 */

public class MyClinicMapFragment extends MdocBaseFragment {

    @BindView(R.id.mapView)
    MapView mMapView;
    @BindView(R.id.addressTv)
    TextView addressTv;

    private GoogleMap googleMap;
    MdocActivity activity;
    public ClinicDetails clinicDetails;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_clinic_map;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        if (clinicDetails==null) {
            clinicDetails = (ClinicDetails) getArguments().getSerializable(MdocConstants.CLINIC_DETAILS);
        }
        String address = clinicDetails.getAddress().getStreet() + " " + clinicDetails.getAddress().getHouseNumber() + ", " + clinicDetails.getAddress().getPostalCode() + " " + clinicDetails.getAddress().getCity();
        addressTv.setText(address);

        setupMap(savedInstanceState);
    }

    private void setupMap(Bundle savedInstanceState) {
        mMapView.onCreate(savedInstanceState);

        mMapView.onResume(); // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                // For dropping a marker at a point on the Map
                LatLng loc = new LatLng(clinicDetails.getLatitude(), clinicDetails.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromResource(R.drawable.location)));

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(loc).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @OnClick(R.id.mapIcon)
    public void onIconClick(){

        String uri = "http://maps.google.com/maps?daddr=" + clinicDetails.getLatitude() + "," + clinicDetails.getLongitude();
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        intent.setPackage("com.google.android.apps.maps");
        if (intent.resolveActivity(activity.getPackageManager()) != null) {
            startActivity(intent);
        }else{
            MdocUtil.showToastLong(activity, getString(R.string.google_maps_disabled));
        }
    }
}

package de.mdoc.pojo.digitalsignature

/**
 * Created by Trenser on 29/12/20.
 */

data class SignatureNextActionResponse (
    val timestamp : String,
    val message : String,
    val code : String,
    val data : String)


package de.mdoc.pojo;

import java.util.ArrayList;

/**
 * Created by ema on 4/2/18.
 */

public class SearchFilesData {

    private int totalCount;
    private ArrayList<FileListItem> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<FileListItem> getList() {
        return list;
    }

    public void setList(ArrayList<FileListItem> list) {
        this.list = list;
    }
}

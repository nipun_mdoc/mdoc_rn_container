package de.mdoc.data.questionnaire

data class Answer(
        val answerData: String,
        val caseId: String,
        val id: String,
        val pollId: String,
        val pollQuestionId: String,
        val pollVotingActionId: String,
        val question: Question?,
        val selection: Any?
)
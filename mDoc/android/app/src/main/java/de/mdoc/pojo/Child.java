package de.mdoc.pojo;

/**
 * Created by AdisMulabdic on 1/16/18.
 */

public class Child {

    private String id;
    private long cts;
    private long uts;
    private boolean superAdmin;
    private String userId;
    private PublicUserDetails publicUserDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getCts() {
        return cts;
    }

    public void setCts(long cts) {
        this.cts = cts;
    }

    public long getUts() {
        return uts;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    public boolean isSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        this.superAdmin = superAdmin;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public PublicUserDetails getPublicUserDetails() {
        return publicUserDetails;
    }

    public void setPublicUserDetails(PublicUserDetails publicUserDetails) {
        this.publicUserDetails = publicUserDetails;
    }
}


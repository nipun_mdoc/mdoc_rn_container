package de.mdoc.modules.messages.data

import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.PatientDetailsData
import de.mdoc.util.MdocAppHelper
import org.joda.time.Instant
import java.util.*

data class ConversationResponse(val code: String = "",
                                val data: Data,
                                val message: String = "",
                                val timestamp: Long = 0)

data class Data(val moreDataAvailable: Boolean = false,
                val list: List<ListItem>?,
                val totalCount: Int = 0)

data class ListItem(val reason: String = "",
                    val cts: Long = 0,
                    val reasonCodingItem: CodingItem?,
                    val letterboxCodingItem: CodingItem?,
                    val lastSeenDate: Hashtable<String, Instant>?,
                    val lastMessageDate: Instant?,
                    val description: String = "",
                    val externalId: String = "",
                    val type: String = "",
                    val archived: Boolean = false,
                    val uts: Long = 0,
                    val letterbox: String = "",
                    val id: String = "",
                    val participants: List<String>?,
                    val isPublic: Boolean = false,
                    val senderDTO: PatientDetailsData? = null,
                    val receiverDTO: PatientDetailsData? = null,
                    val replyAllowed: Boolean = false) {

    fun isNotSeen(): Boolean {
        var isNotSeen = false
        try {
            if (lastSeenDate != null) {
                if (lastSeenDate.containsKey(MdocAppHelper.getInstance().userId)) {
                    isNotSeen = lastMessageDate != null && lastMessageDate.isAfter(
                            lastSeenDate.getValue(MdocAppHelper.getInstance().userId))
                }
                else {
                    isNotSeen = true
                }
            }
        } catch (e: java.lang.Exception) {
        }

        return isNotSeen
    }

    fun getFormattedLatterBox(): String {
        val senderFullName = getSenderInformation()
        return if (senderFullName == letterbox) {
            letterbox.take(2).toUpperCase()
        } else {
            var initials = ""
            if (senderFullName != null) {
                val list = senderFullName.trim().split(" ")
                initials = if (list.size > 1) list[0].take(1) + list[list.lastIndex].take(1) else list[0].take(2)
            }
            return initials.toUpperCase()
        }
    }

    fun getSenderInformation(): String? {
        var senderFullName = ""


        if (senderDTO != null && senderDTO.publicUserDetails != null) {
            if (senderDTO.publicUserDetails.firstName != null) {
                senderFullName = senderDTO.publicUserDetails.firstName + " "
            }
            if (senderDTO.publicUserDetails.lastName != null) {
                senderFullName += senderDTO.publicUserDetails.lastName
            }
        }
        return  if(letterboxCodingItem?.display != null && letterboxCodingItem.display?.trim().isNotEmpty()){
            letterboxCodingItem?.display ?: letterbox
        }else if(!senderFullName.isNullOrEmpty()) {
            senderFullName
        }else{
            letterbox
        }

    }

    fun getReasonText(): String? {
        if (reasonCodingItem != null) {
            return reasonCodingItem.display
        }
        else {
            return reason
        }
    }
}

data class Translations(val de: String = "",
                        val en: String = "",
                        val it: String = "",
                        val fr: String = "")


package de.mdoc.modules.vitals.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.devices.data.AllUnits
import de.mdoc.modules.vitals.unit_picker.UnitPickerViewModel
import de.mdoc.storage.AppPersistence

class InnerUnitAdapter(val viewModel: UnitPickerViewModel,val items: List<AllUnits>,private val loincCode:String): RecyclerView.Adapter<InnerUnitViewHolder>() {
    private var lastSelectedPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): InnerUnitViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_inner_unit, parent, false)

        return InnerUnitViewHolder(view)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onBindViewHolder(viewHolder: InnerUnitViewHolder, position: Int) {
        val item = items[position]
        viewHolder.radioButton.text = item.unit?.code ?: "Radio with no text"
        viewHolder.radioButton.isChecked= AppPersistence.lastSeenUnitByCode[loincCode] ==item.unit?.code

        viewHolder.radioButton.setOnTouchListener(View.OnTouchListener { radio, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    if (!viewHolder.radioButton.isChecked) {
                        AppPersistence.lastSeenUnitByCode[loincCode] = item.unit?.code?:""
                        viewModel.saveSelectionApi()
                        lastSelectedPosition = position
                        notifyDataSetChanged()
                        viewHolder.radioButton.isChecked=false
                    }
                }
            }
            return@OnTouchListener true
        })
    }

    override fun getItemCount(): Int {
        return items.size
    }
}

class InnerUnitViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val radioButton: RadioButton = itemView.findViewById(R.id.radioSelection)
}
package de.mdoc.modules.questionnaire.questionnaire_view_model

import androidx.lifecycle.MutableLiveData
import de.mdoc.data.questionnaire.AnswersRequest
import de.mdoc.data.questionnaire.Data
import de.mdoc.data.responses.QuestionnaireAnswersResponse
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.getErrorDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class QuestionnaireRepository{

    fun getQuestionnaireAnswers(request:AnswersRequest):MutableLiveData<Data>{

        val from= request.from
        val to= request.to
        val pollVotingActionId= request.pollVotingActionId
        val pollId= request.pollId
        val includeAnswers=request.includeAnswers

        MdocManager.getAnsweredQuestionnaires(from,to,pollVotingActionId,pollId,includeAnswers,object : Callback<QuestionnaireAnswersResponse> {
            override fun onResponse(call: Call<QuestionnaireAnswersResponse>, response: Response<QuestionnaireAnswersResponse>) {
                if (response.isSuccessful) {
                    questionnaireLiveData.value = response.body()?.data?.get(0)
                } else {
                    Timber.w("getAnsweredQuestionnaires ${response.getErrorDetails()}")
                }
            }
            override fun onFailure(call: Call<QuestionnaireAnswersResponse>, t: Throwable) {
                Timber.w(t, "getAnsweredQuestionnaires")
            }
        })


        return questionnaireLiveData
    }

    companion object {

        val questionnaireLiveData = MutableLiveData<Data>()

    }

}
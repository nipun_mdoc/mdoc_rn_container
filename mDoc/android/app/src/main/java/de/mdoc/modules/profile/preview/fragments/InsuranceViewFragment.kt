package de.mdoc.modules.profile.preview.fragments

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.profile.patient_details.PatientDetailsHelper
import de.mdoc.modules.profile.preview.PreviewDataFactory
import de.mdoc.modules.profile.preview.adapters.PreviewDataAdapter
import de.mdoc.modules.profile.preview.viewmodel.InsuranceViewModel
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_metadata_preview.*


class InsuranceViewFragment : Fragment(R.layout.fragment_metadata_preview) {

    private val insuranceViewModel by viewModel {
        InsuranceViewModel(RestClient.getService())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        metaDataPreviewTitle.setText(R.string.meta_data_insurance)

        insuranceViewModel.isInsuranceLoaded.observe(viewLifecycleOwner, {
            if (it) {
                rvMetadataPreview.apply {
                    layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                    val coverage = PatientDetailsHelper.getInstance().coveragesResponse
                    adapter = PreviewDataAdapter(PreviewDataFactory.getInsuranceData(
                            coverage = coverage,
                            healthInsurance = insuranceViewModel.getCurrentHealthInsurance(coverage),
                            insuranceClassList = insuranceViewModel.getCurrentInsuranceClass(coverage),
                            supplementaryInsurance = insuranceViewModel.getCurrentSupplementaryInsurance(coverage),
                            supplementaryInsuranceClass = insuranceViewModel.getCurrentSupplementaryInsuranceClass(coverage),
                            accidentInsurance = insuranceViewModel.getCurrentAccidentInsurance(coverage),
                            accidentInsuranceClass = insuranceViewModel.getCurrentAccidentInsuranceClass(coverage),
                            supplementaryAccidentInsurance = insuranceViewModel.getSupplementaryAccidentInsurance(coverage),
                            supplementaryAccidentInsuranceClass = insuranceViewModel.getSupplementaryAccidentInsuranceClass(coverage),
                            disabilityInsurance = insuranceViewModel.getDisabilityInsurance(coverage),
                            militaryInsurance = insuranceViewModel.getMilitaryInsurance(coverage)
                    ))
                }
            }
        })
    }
}

package de.mdoc.modules.messages.messages;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.interfaces.IDownloadId;
import de.mdoc.modules.messages.data.ListItemMessage;
import de.mdoc.modules.messages.data.Meta;

public class MessagesListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private Context context;
    private static View.OnClickListener clickListener;
    private ArrayList<ListItemMessage> chats;
    private boolean archived;
    private String letterboxTitle;

    private IDownloadId listener;

    private static OnItemClickListener mOnItemClickLister;

    public interface OnItemClickListener {
        void onItemClicked(View view, String url);
    }


    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }


    public void setDownloadListener(IDownloadId listener) {
        this.listener = listener;
    }


    public MessagesListAdapter(Context context, ArrayList<ListItemMessage> chats, boolean archived, String letterboxTitle) {
        this.context = context;
        this.chats = chats;
        this.archived = archived;
        this.letterboxTitle = letterboxTitle;
        
    }

    @Override
    public int getItemViewType(int position) {
        ListItemMessage message = chats.get(position);

        if (message.isMine()) {
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.placeholder_my_message, parent, false);
            return new MessagesListAdapter.SentMessageHolder(view);
        } else if (viewType == VIEW_TYPE_MESSAGE_RECEIVED) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.placeholder_recived_message, parent, false);
            return new MessagesListAdapter.ReceivedMessageHolder(view);
        } else {
            return null;
        }

//        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListItemMessage message = chats.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((MessagesListAdapter.SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((MessagesListAdapter.ReceivedMessageHolder) holder).bind(message);
                ((ReceivedMessageHolder) holder).messageHref.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mOnItemClickLister.onItemClicked(view, chats.get(position).getHref());
                    }
                });
        }

    }

    private class SentMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, myMessageDateTv;

        public SentMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.myText);
            myMessageDateTv = itemView.findViewById(R.id.myMessageDateTv);
        }

        void bind(ListItemMessage message) {
            messageText.setText(message.getText());
            if (message.getDate() == null) {
                myMessageDateTv.setVisibility(View.GONE);
            } else {
                if (archived) {
                    myMessageDateTv.setText(message.getFormatedDateArchived());
                } else {
                    myMessageDateTv.setText(message.getFormatedDate());
                }
                myMessageDateTv.setVisibility(View.VISIBLE);
            }

        }
    }

    private class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        TextView messageText, recivedMessageDateTv, senderName, messageHref;
        RecyclerView rv_inner_files;

        public ReceivedMessageHolder(View itemView) {
            super(itemView);

            messageText = itemView.findViewById(R.id.senderText);
            messageHref = itemView.findViewById(R.id.senderHref);
            recivedMessageDateTv = itemView.findViewById(R.id.recivedMessageDateTv);
            rv_inner_files = itemView.findViewById(R.id.rv_inner_files);
            senderName = itemView.findViewById(R.id.senderName);

        }

        void bind(ListItemMessage message) {
            messageText.setText(message.getText());
            if(message.getHref() != null && message.getHref().length() > 0) {
                Meta meta = message.getMeta();
                String messageText = meta != null && !TextUtils.isEmpty(meta.getLINK_TEXT())  ? meta.getLINK_TEXT() :  context.getResources().getString(R.string.link_to_your_document);
                messageHref.setText(messageText);
                messageHref.setPaintFlags(messageHref.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            } else {
                messageHref.setVisibility(View.GONE);
            }

            String senderFullName = getSenderName(message);
            if (senderFullName.equals(letterboxTitle)) {
                senderName.setVisibility(View.GONE);
            } else {
                senderName.setVisibility(View.VISIBLE);
                senderName.setText(senderFullName);
            }

            if (message.getDate() == null) {
                recivedMessageDateTv.setVisibility(View.GONE);
            } else {
                if (archived) {
                    recivedMessageDateTv.setText(message.getFormatedDateArchived());
                } else {
                    recivedMessageDateTv.setText(message.getFormatedDate());
                }
                recivedMessageDateTv.setVisibility(View.VISIBLE);
            }

            if (message.hasFiles()) {
                rv_inner_files.setVisibility(View.VISIBLE);
                InnerFilesAdapter adapter = new InnerFilesAdapter(context, message.getFileList(), listener);
                rv_inner_files.setAdapter(adapter);
            } else {
                rv_inner_files.setVisibility(View.GONE);
            }

        }

        private Boolean isLetterbox(Boolean isLetterbox) {
            return letterboxTitle == "";
        }

        private String getSenderName(ListItemMessage message) {
            String fullName = "";
            if (message.getSenderDTO() != null && message.getSenderDTO().getPublicUserDetails() != null) {
                senderName.setVisibility(View.VISIBLE);
                if (message.getSenderDTO().getPublicUserDetails().getFirstName() != null) {
                    fullName = message.getSenderDTO().getPublicUserDetails().getFirstName() + " ";
                }
                if (message.getSenderDTO().getPublicUserDetails().getLastName() != null) {
                    fullName += message.getSenderDTO().getPublicUserDetails().getLastName();
                }
                senderName.setText(fullName);
            }
            return fullName;
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }
}

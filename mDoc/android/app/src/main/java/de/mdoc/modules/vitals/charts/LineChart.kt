package de.mdoc.modules.vitals.charts

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import de.mdoc.R
import de.mdoc.modules.devices.data.Observation
import de.mdoc.modules.devices.data.ObservationPeriodType
import de.mdoc.modules.vitals.FhirConfigurationUtil.metricLabel
import de.mdoc.util.ChartFormatter.DayFormatter
import de.mdoc.util.ChartFormatter.MonthFormatter
import de.mdoc.util.ChartFormatter.WeekFormatter
import de.mdoc.util.ChartFormatter.YearFormatter
import java.util.*

fun drawLineChart(context: Context,
                  periodType: ObservationPeriodType,
                  inputData: ArrayList<Entry>,lastDayOfMonth:Int,
                  unit:String) {

    val activity:Activity=context as Activity
    var graphColor = ContextCompat.getColor(activity, R.color.blue_chart)

    val maxValueInCollection: Float
    val minValueInCollection: Float
    val yAxisLimitTop: Float
    val yAxisLimitBottom: Float

    val lineChart = activity.findViewById<LineChart>(R.id.chart_pulse)

    if(lineChart!=null) {
        lineChart.visibility = View.VISIBLE

        val lineDataSets = arrayListOf<ILineDataSet>()

            maxValueInCollection=inputData.maxBy {it.y }?.y?:200F
            minValueInCollection=inputData.minBy {it.y }?.y?:0F
            yAxisLimitTop=maxValueInCollection+(maxValueInCollection*(10F/100F))
            yAxisLimitBottom=minValueInCollection-(minValueInCollection*(10F/100F))

        val set1 =LineDataSet(inputData,unit)

        lineChart.setNoDataText("")
        lineChart.setScaleEnabled(false)
        set1.lineWidth = 3F

        lineChart.axisLeft.setDrawGridLines(false)
        lineChart.axisRight.isEnabled = false
        lineChart.description.isEnabled = false

        //x axis
        lineChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        lineChart.xAxis.setDrawAxisLine(true)
        lineChart.xAxis.setDrawGridLines(false)

        when (periodType) {
            ObservationPeriodType.DAY -> {
                val dayFormatter = DayFormatter()
                lineChart.xAxis.setLabelCount(5, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 24F
                lineChart.xAxis.valueFormatter = dayFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.WEEK -> {
                val weekFormatter = WeekFormatter(context)
                lineChart.xAxis.setLabelCount(7, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 6F
                lineChart.xAxis.valueFormatter = weekFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum =yAxisLimitTop
            }
            ObservationPeriodType.MONTH -> {
                val monthFormatter = MonthFormatter()
                lineChart.xAxis.setLabelCount(7, true)
                lineChart.xAxis.axisMinimum = 01F
                lineChart.xAxis.axisMaximum = lastDayOfMonth.toFloat()
                lineChart.xAxis.valueFormatter = monthFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            ObservationPeriodType.YEAR -> {
                val yearFormatter = YearFormatter()
                lineChart.xAxis.setLabelCount(12, true)
                lineChart.xAxis.axisMinimum = 0F
                lineChart.xAxis.axisMaximum = 11F
                lineChart.xAxis.valueFormatter = yearFormatter

                lineChart.axisLeft.axisMinimum = yAxisLimitBottom
                lineChart.axisLeft.axisMaximum = yAxisLimitTop
            }
            else -> {
            }
        }

        if (inputData.isEmpty()) {
            inputData.add(Entry(0.toFloat(), 0.toFloat()))
            graphColor = ContextCompat.getColor(activity, R.color.white_transparent)
            set1.setDrawValues(false)
        }

        set1.setCircleColor(graphColor)
        set1.setCircleColorHole(graphColor)
        set1.color = graphColor

        lineDataSets.add(set1)

        val lineData = LineData(lineDataSets)

        lineChart.data = lineData
        lineChart.invalidate()
    }

}

fun observationToChartEntry(input: ArrayList<Observation>?, periodType: ObservationPeriodType): ArrayList<Entry> {
    val output:ArrayList<Entry> = arrayListOf()
    val time: Calendar = Calendar.getInstance()
    if(!input.isNullOrEmpty()) {
        time.timeInMillis = input[0].period
        for (item in input) {
            time.timeInMillis = item.period
            var xAxisValue = 0

            when (periodType) {
                ObservationPeriodType.DAY -> {
                    xAxisValue = time.get(Calendar.HOUR_OF_DAY)
                }
                ObservationPeriodType.WEEK -> {
                    xAxisValue = time.get(Calendar.DAY_OF_WEEK)-2

                    if (xAxisValue == -1) xAxisValue = 6
                }
                ObservationPeriodType.MONTH -> {
                    xAxisValue = time.get(Calendar.DAY_OF_MONTH)

                }
                ObservationPeriodType.YEAR -> {
                    xAxisValue = time.get(Calendar.MONTH)
                }
                else -> {
                }
            }

            val entryElement = Entry(xAxisValue.toFloat(), item.avg.toFloat())
            output.add(entryElement)
        }
    }
    val sort:ArrayList<Entry> = output
    sort.sortBy { it.x }
    return sort
}
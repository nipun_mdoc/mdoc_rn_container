package de.mdoc.pojo;

import java.util.ArrayList;

/**
 * Created by ema on 7/14/17.
 */

public class NotificationData {

    private int totalCount;
    private ArrayList<NotificationListItem> list = null;

    public NotificationData(int totalCount, ArrayList<NotificationListItem> list) {
        this.totalCount = totalCount;
        this.list = list;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<NotificationListItem> getList() {
        return list;
    }

    public void setList(ArrayList<NotificationListItem> list) {
        this.list = list;
    }
}

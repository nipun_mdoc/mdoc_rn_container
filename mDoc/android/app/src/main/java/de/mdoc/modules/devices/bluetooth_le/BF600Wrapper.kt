package de.mdoc.modules.devices.bluetooth_le

import android.bluetooth.*
import android.content.Context
import android.os.Handler
import android.os.Looper
import de.mdoc.interfaces.DeviceDataListener
import de.mdoc.modules.devices.data.BleMessage
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.modules.devices.data.ScaleUser
import org.joda.time.DateTime
import timber.log.Timber
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class BF600Wrapper(val context: Context,
                   val deviceAddress: String,
                   val deviceData: FhirConfigurationData): BluetoothGattCallback(), Serializable {

    private var bluetoothDevice: BluetoothDevice? = null
    private var usersList: ArrayList<Byte> = ArrayList()
    private var bleListener: DeviceDataListener? = null
    private var mGatt: BluetoothGatt? = null
    private var mScaleUser: ScaleUser? = null
    private var userListCharacteristic: BluetoothGattCharacteristic? = null
    private var userControlPointCharacteristic: BluetoothGattCharacteristic? = null
    private var dobCharacteristic: BluetoothGattCharacteristic? = null
    private var genderCharacteristic: BluetoothGattCharacteristic? = null
    private var heightCharacteristic: BluetoothGattCharacteristic? = null
    private var dbIncrementCharacteristic: BluetoothGattCharacteristic? = null
    private var takeMeasurementCharacteristic: BluetoothGattCharacteristic? = null
    private var weightMeasurementCharacteristic: BluetoothGattCharacteristic? = null
    private var currentTimeCharacteristic: BluetoothGattCharacteristic? = null
    private val usersMap: HashMap<Int, ArrayList<Byte>> = hashMapOf()
    private var counter = 0
    private var registeredUserPin: String? = null
    private var connectionTimeOutHandler: Handler? = null

    fun setBleResponseListener(bleListener: DeviceDataListener) {
        this.bleListener = bleListener
    }

    init {
        Timber.d(": initCalled")
        connect()
    }

    private fun connect () {
        if (bluetoothDevice == null) {
            bluetoothDevice = getBluetoothDevice(context,
                    deviceAddress)
        }
        bluetoothDevice!!.connectGatt(context,
                false,
                this)
    }

    private fun getBluetoothDevice(context: Context, deviceAddress: String): BluetoothDevice {
        val bluetoothManager = context
            .getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        val bluetoothAdapter = bluetoothManager.adapter

        return bluetoothAdapter.getRemoteDevice(deviceAddress)
    }

    override fun onConnectionStateChange(gatt: BluetoothGatt,
                                         status: Int,
                                         newState: Int) {
        super.onConnectionStateChange(gatt, status, newState)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            when (newState) {
                BluetoothGatt.STATE_CONNECTED    -> {
                    mGatt = gatt
                    Timber.d("onConnectionStateChange: STATE_CONNECTED $gatt $status")
                    gatt.discoverServices()
                    registerConnectionTimeoutHandler()
                }

                BluetoothGatt.STATE_DISCONNECTED -> {
                    gatt.close()
                    Timber.d("onConnectionStateChange: STATE_DISCONNECTED $gatt $status")
                    connectionTimeOutHandler?.removeCallbacksAndMessages(null)
                }
            }
        }
    }

    override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
        super.onServicesDiscovered(gatt, status)

        if (status == BluetoothGatt.GATT_SUCCESS) {
            Timber.d("onServicesDiscovered: $gatt")

            currentTimeCharacteristic = gatt.getService(UUID.fromString(CURRENT_TIME_SERVICE))
                .getCharacteristic(UUID.fromString(CURRENT_TIME_CHARACTERISTIC))

            userListCharacteristic = gatt.getService(UUID.fromString(CUSTOM_SERVICE))
                .getCharacteristic(UUID.fromString(USER_LIST_CHARACTERISTIC))

            userControlPointCharacteristic = gatt.getService(UUID.fromString(USER_DATA_SERVICE))
                ?.getCharacteristic(UUID.fromString(USER_CONTROL_POINT_CHARACTERISTIC))

            dobCharacteristic = gatt.getService(UUID.fromString(USER_DATA_SERVICE))
                ?.getCharacteristic(UUID.fromString(DOB_CHARACTERISTIC))

            genderCharacteristic = gatt.getService(UUID.fromString(USER_DATA_SERVICE))
                ?.getCharacteristic(UUID.fromString(GENDER_CHARACTERISTIC))

            heightCharacteristic = gatt.getService(UUID.fromString(USER_DATA_SERVICE))
                ?.getCharacteristic(UUID.fromString(HEIGHT_CHARACTERISTIC))

            dbIncrementCharacteristic = gatt.getService(UUID.fromString(USER_DATA_SERVICE))
                ?.getCharacteristic(UUID.fromString(DB_INCREMENT_CHARACTERISTIC))

            takeMeasurementCharacteristic = gatt.getService(UUID.fromString(CUSTOM_SERVICE))
                ?.getCharacteristic(UUID.fromString(TAKE_MEASUREMENT_CHARACTERISTIC))

            weightMeasurementCharacteristic = gatt.getService(UUID.fromString(WEIGHT_SCALE_SERVICE))
                ?.getCharacteristic(UUID.fromString(WEIGHT_MEASUREMENT_CHARACTERISTIC))

            enableNotifications(currentTimeCharacteristic)
        }
    }

    override fun onDescriptorWrite(gatt: BluetoothGatt,
                                   descriptor: BluetoothGattDescriptor,
                                   status: Int) {
        super.onDescriptorWrite(gatt, descriptor, status)
        if (status == BluetoothGatt.GATT_SUCCESS) {
            when (counter) {
                0 -> {
                    enableNotifications(userListCharacteristic)
                }

                1 -> {
                    enableNotifications(dbIncrementCharacteristic)
                }

                2 -> {
                    enableIndications(userControlPointCharacteristic)
                }

                3 -> {
                    enableNotifications(takeMeasurementCharacteristic)
                }

                4 -> {
                    enableIndications(weightMeasurementCharacteristic)
                }

                5 -> {
                    val command = setCurrentTimeCommand()
                    writeCommand(currentTimeCharacteristic, command)
                }
            }
            counter++

            Timber.d("onDescriptorWrite: SUCCESS $gatt")
        }
        else {
            Timber.d("onDescriptorWrite: FAILED $gatt")
        }
    }

    override fun onCharacteristicChanged(gatt: BluetoothGatt,
                                         characteristic: BluetoothGattCharacteristic) {
        super.onCharacteristicChanged(gatt, characteristic)
        val response: ArrayList<Byte> = ArrayList()

        characteristic.value.forEach {
            response.add(it)
        }

        if (characteristic == userListCharacteristic) {
            if (!characteristic.value!!.contentEquals(NO_USER_LIST)) {
                getUsers(response)
            }
            else {
                bleListener!!.genericDeviceResponse(usersList,
                        mGatt,
                        BleMessage.NO_USERS)
            }
        }
        /*
        USER AUTHENTICATION RESPONSE
        */
        else if (characteristic == userControlPointCharacteristic) {
            Timber.d("onCharacteristicCHANGED: %s",
                    Arrays.toString(characteristic.value))
            /*
            USER CREATED SUCCESSFUL
             */
            if (response.size == 4 && response.getOrNull(2)?.toInt() == 1) {
                val userIndex = response.getOrNull(3)
                    ?.toInt()

                userLogin(userIndex, registeredUserPin)


                bleListener!!.genericDeviceResponse(usersList,
                        mGatt,
                        BleMessage.USER_CREATED)
            }
            /*
           USER LOG_IN SUCCESSFUL
            */
            else if (response.size == 3 && response.getOrNull(2)?.toInt() == 1) {
                bleListener!!.genericDeviceResponse(usersList,
                        mGatt,
                        BleMessage.LOG_IN_SUCCESS)
            }
            /*
         USER LOG_IN FAIL
          */
            else if (response.size == 3 && response.getOrNull(2)?.toInt() == 5) {
                bleListener!!.genericDeviceResponse(usersList,
                        mGatt,
                        BleMessage.LOG_IN_FAIL)
            }
        }
        else if (characteristic == weightMeasurementCharacteristic) {
            bleListener!!.genericDeviceResponse(response,
                    mGatt,
                    BleMessage.USER_MEASUREMENT)
        }
        Timber.d("onCharacteristicCHANGED: %s",
                Arrays.toString(characteristic.value))
    }

    override fun onCharacteristicWrite(gatt: BluetoothGatt?,
                                       characteristic: BluetoothGattCharacteristic?,
                                       status: Int) {
        super.onCharacteristicWrite(gatt, characteristic, status)

        if (status == BluetoothGatt.GATT_SUCCESS) {
            Timber.d("onCharacteristicWrite: %s", Arrays.toString(characteristic?.value))

            when (characteristic) {
                currentTimeCharacteristic -> {
                    writeCommand(userListCharacteristic, USER_LIST_COMMAND)
                }

                genderCharacteristic      -> {
                    val height = mScaleUser?.height ?: 0
                    heightCharacteristic?.value = byteArrayOf(height.toByte())
                    mGatt?.writeCharacteristic(heightCharacteristic)
                }

                heightCharacteristic      -> {
                    val dob = mScaleUser?.dob ?: 0
                    val dateTime = DateTime(dob)
                    val year = dateTime.year
                    val month = dateTime.monthOfYear
                    val day = dateTime.dayOfMonth
                    val pairOfYear = intToPairOfInt(year)
                    dobCharacteristic?.value = byteArrayOf(
                            pairOfYear.second.toByte(), pairOfYear.first.toByte(), month.toByte(),
                            day.toByte())
                    mGatt?.writeCharacteristic(dobCharacteristic)
                }

                dobCharacteristic         -> {
                    Thread.sleep(200)

                    mGatt?.readCharacteristic(dbIncrementCharacteristic)
                }

                dbIncrementCharacteristic -> {
                    takeMeasurement()
                }
            }
        }
    }

    override fun onCharacteristicRead(gatt: BluetoothGatt?,
                                      characteristic: BluetoothGattCharacteristic?,
                                      status: Int) {
        super.onCharacteristicRead(gatt, characteristic, status)
        Timber.d("onCharacteristicRead: %s", Arrays.toString(characteristic?.value))

        if (characteristic == dbIncrementCharacteristic) {
            val response = (characteristic?.value)
            incrementByteArray(response, response!!.size - 1)
            dbIncrementCharacteristic?.value = response
            mGatt?.writeCharacteristic(dbIncrementCharacteristic)
        }
    }

    private fun enableNotifications(characteristic: BluetoothGattCharacteristic?) {
        characteristic?.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT

        mGatt?.setCharacteristicNotification(characteristic,
                true)
        val descriptor = characteristic?.getDescriptor(
                UUID.fromString(NOTIFICATION_CHARACTERISTIC))
        descriptor?.value = BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
        mGatt?.writeDescriptor(descriptor)
    }

    private fun enableIndications(characteristic: BluetoothGattCharacteristic?) {
        characteristic?.writeType = BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT
        mGatt?.setCharacteristicNotification(characteristic,
                true)
        val descriptor = characteristic?.getDescriptor(
                UUID.fromString(NOTIFICATION_CHARACTERISTIC))
        descriptor?.value = BluetoothGattDescriptor.ENABLE_INDICATION_VALUE
        mGatt?.writeDescriptor(descriptor)
    }

    private fun writeCommand(characteristic: BluetoothGattCharacteristic?,
                             command: ByteArray) {
        characteristic?.value = command
        mGatt?.writeCharacteristic(characteristic)
    }

    private fun getUsers(response: ArrayList<Byte>) {
        userListCharacteristic?.let { characteristic ->
            when {
                characteristic.value.size == 12                 -> {
                    Timber.d("userList %s", Arrays.toString(characteristic.value))
                    usersMap[characteristic.value[1].toInt()] = response


                    if (characteristic.value.getOrNull(1)?.toInt() == 8) {
                        usersMap.forEach { usersList.addAll(it.value) }

                        bleListener!!.genericDeviceResponse(usersList,
                                mGatt,
                                BleMessage.USER_LIST)
                    }
                }

                characteristic.value.getOrNull(0)?.toInt() == 1 -> {
                    usersMap.forEach { usersList.addAll(it.value) }
                    bleListener!!.genericDeviceResponse(usersList,
                            mGatt,
                            BleMessage.USER_LIST)
                }

                characteristic.value.getOrNull(0)?.toInt() == 2 -> {
                    bleListener!!.genericDeviceResponse(usersList,
                            mGatt,
                            BleMessage.NO_USERS)
                }
            }
        }
    }

    fun userLogin(userIndex: Int?, pin: String?) {
        val pair = stringToPairOfInt(pin)
        val command =
                byteArrayOf(2, userIndex!!.toByte(), pair.second.toByte(), pair.first.toByte())

        writeCommand(userControlPointCharacteristic, command)
    }

    fun createUser(pin: String) {
        registeredUserPin = pin
        val pair = stringToPairOfInt(pin)
        val command = byteArrayOf(0x01.toByte(), pair.second.toByte(), pair.first.toByte())

        writeCommand(userControlPointCharacteristic, command)
    }

    fun updateUserDetails(scaleUser: ScaleUser?) {
        mScaleUser = scaleUser
        val gender = mScaleUser?.gender ?: 255
        genderCharacteristic?.value = byteArrayOf(gender.toByte())
        mGatt?.writeCharacteristic(genderCharacteristic)
    }

    fun disconnect() {
        mGatt?.disconnect()
    }

    fun takeMeasurement() {
        val command = byteArrayOf(0x00.toByte())
        writeCommand(takeMeasurementCharacteristic, command)
    }

    private fun stringToPairOfInt(input: String?): Pair<Int, Int> {
        val intPin = input?.toInt()
        var pinHex = String.format("%02X", intPin)
        while (pinHex.length != 4) {
            pinHex = "0$pinHex"
        }
        val pinOneHex = pinHex.substring(0, 2)
        val pinTwoHex = pinHex.substring(2, 4)
        val pinOne = java.lang.Long.parseLong(pinOneHex, 16)
            .toInt()
        val pinTwo = java.lang.Long.parseLong(pinTwoHex, 16)
            .toInt()

        return Pair(pinOne, pinTwo)
    }

    private fun intToPairOfInt(input: Int): Pair<Int, Int> {
        var pinHex = String.format("%02X", input)
        while (pinHex.length != 4) {
            pinHex = "0$pinHex"
        }
        val pairOneHex = pinHex.substring(0, 2)
        val pairTwoHex = pinHex.substring(2, 4)
        val pinOne = java.lang.Long.parseLong(pairOneHex, 16)
            .toInt()
        val pinTwo = java.lang.Long.parseLong(pairTwoHex, 16)
            .toInt()

        return Pair(pinOne, pinTwo)
    }

    private fun incrementByteArray(byteArray: ByteArray?, index: Int?) {
        if (byteArray?.getOrNull(index!!) == 255.toByte()) {
            byteArray[index!!] = 0.toByte()
            if (index > 0) {
                incrementByteArray(byteArray, index - 1)
            }
        }
        else {
            byteArray!![index!!]++
        }
    }


     fun getUsers(){
         usersList.clear()
         usersMap.clear()
         counter = 0

         val bluetoothManager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
         var status = bluetoothManager.getConnectionState(bluetoothDevice, BluetoothProfile.GATT)

         when (status) {
             BluetoothGatt.STATE_CONNECTED -> {
                 Timber.d("STATE_CONNECTED - writeCommand - USER_LIST_COMMAND")
                 writeCommand(userListCharacteristic, USER_LIST_COMMAND)
             }
             BluetoothGatt.STATE_DISCONNECTED -> {
                 Timber.d("STATE_DISCONNECTED - trying to connect")
                 connect()
             }
         }
    }

    private fun setCurrentTimeCommand(): ByteArray {
        val dateTime = DateTime.now()
        val year = dateTime.year
        val month = dateTime.monthOfYear
        val day = dateTime.dayOfMonth
        val hour = dateTime.hourOfDay
        val minute = dateTime.minuteOfHour
        val pairOfYear = intToPairOfInt(year)


        return byteArrayOf(pairOfYear.second.toByte(), pairOfYear.first.toByte(), month.toByte(),
                day.toByte(), hour.toByte(), minute.toByte(), 0, 0, 0, 0)
    }

    private fun registerConnectionTimeoutHandler() {
        if (connectionTimeOutHandler == null) {
            connectionTimeOutHandler = Handler(Looper.getMainLooper())
            connectionTimeOutHandler!!.postDelayed({
                bleListener!!.genericDeviceResponse(arrayListOf(),
                        mGatt,
                        BleMessage.CONNECTION_TIMEOUT)
            }, deviceData.getConnectionTimeoutMs())
        }
    }

    companion object {

        private const val CUSTOM_SERVICE = "0000FFF0-0000-1000-8000-00805f9b34fb"
        private const val USER_LIST_CHARACTERISTIC = "0000FFF2-0000-1000-8000-00805f9b34fb"
        private const val NOTIFICATION_CHARACTERISTIC = "00002902-0000-1000-8000-00805f9b34fb"
        private const val USER_DATA_SERVICE = "0000181C-0000-1000-8000-00805f9b34fb"
        private const val DOB_CHARACTERISTIC = "00002A85-0000-1000-8000-00805f9b34fb"
        private const val GENDER_CHARACTERISTIC = "00002A8C-0000-1000-8000-00805f9b34fb"
        private const val HEIGHT_CHARACTERISTIC = "00002A8E-0000-1000-8000-00805f9b34fb"
        private const val DB_INCREMENT_CHARACTERISTIC = "00002A99-0000-1000-8000-00805f9b34fb"
        private const val USER_CONTROL_POINT_CHARACTERISTIC = "00002A9F-0000-1000-8000-00805f9b34fb"
        private const val TAKE_MEASUREMENT_CHARACTERISTIC = "0000FFF4-0000-1000-8000-00805f9b34fb"
        private const val WEIGHT_SCALE_SERVICE = "0000181D-0000-1000-8000-00805f9b34fb"
        private const val WEIGHT_MEASUREMENT_CHARACTERISTIC = "00002A9D-0000-1000-8000-00805f9b34fb"
        private const val CURRENT_TIME_SERVICE = "00001805-0000-1000-8000-00805f9b34fb"
        private const val CURRENT_TIME_CHARACTERISTIC = "00002a2b-0000-1000-8000-00805f9b34fb"
        private val NO_USER_LIST = byteArrayOf(2)
        private val USER_LIST_COMMAND = byteArrayOf(0x00)
    }
}

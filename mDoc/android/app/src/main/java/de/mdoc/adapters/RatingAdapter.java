package de.mdoc.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.ExtendedOption;
import de.mdoc.pojo.ExtendedQuestion;

/**
 * Created by ema on 1/13/17.
 */

public class RatingAdapter extends BaseAdapter {

    ArrayList<ExtendedOption> extendedOptions;
    Context context;
    private ExtendedQuestion question;

    private RelativeLayout preSelectedRl;
    private TextView preSelectedTv;
    Typeface regular;
    Typeface bold;

    public RatingAdapter(ArrayList<ExtendedOption> extendedOptions, Context context, ExtendedQuestion question) {
        this.question = question;
        this.extendedOptions = extendedOptions;
        this.context = context;
        this.regular = ResourcesCompat.getFont(context, R.font.hk_grotesk_regular);
        this.bold = ResourcesCompat.getFont(context, R.font.hk_grotesk_bold);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView = LayoutInflater.from(context).inflate(R.layout.rating_grid_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        ExtendedOption eo = (ExtendedOption) getItem(position);

        holder.pointsTv.setTypeface(bold);
        holder.pointsTv.setText(eo.getNumber());

        // If user answer question already
        if (eo.getAnswer() != null && eo.getAnswer().getAnswerData() != null && eo.getAnswer().getAnswerData().equals(eo.getValue(question.getSelectedLanguage()))) {
            holder.ratingCircleRl.setBackgroundResource(R.drawable.blue_gray_circle_stroke);
            holder.pointsTv.setTextColor(context.getResources().getColor(R.color.blue_gray));
            preSelectedRl = holder.ratingCircleRl;
            preSelectedTv = holder.pointsTv;
        } else {
            holder.ratingCircleRl.setBackgroundResource(R.drawable.blue_gray_circle);
            holder.pointsTv.setTextColor(context.getResources().getColor(R.color.white));
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return extendedOptions.size();
    }

    @Override
    public Object getItem(int position) {
        return extendedOptions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {


        @BindView(R.id.pointsTv)
        TextView pointsTv;
        @BindView(R.id.ratingCircleRl)
        RelativeLayout ratingCircleRl;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    public RelativeLayout getPreSelectedRl() {
        return preSelectedRl;
    }

    public void setPreSelectedRl(RelativeLayout preSelectedRl) {
        this.preSelectedRl = preSelectedRl;
    }

    public TextView getPreSelectedTv() {
        return preSelectedTv;
    }

    public void setPreSelectedTv(TextView preSelectedTv) {
        this.preSelectedTv = preSelectedTv;
    }
}

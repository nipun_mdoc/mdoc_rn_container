package de.mdoc.modules.profile.patient_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.pojo.Address
import kotlinx.android.synthetic.main.activity_patient_details.*
import kotlinx.android.synthetic.main.fragment_employer.*
import javax.annotation.Nullable

class EmployerFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_employer, container, false)
    }

    override fun onViewCreated(view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as PatientDetailsActivity).setNavigationItem(2)

        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.employer?.occupation?.let {
            jobEdt.setText(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.employer?.name?.let {
            employerEdt.setText(it)
        }

        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.employer?.address?.postalCode?.let {
            employerPostcodeEdt.setText(it)
        }
        PatientDetailsHelper.getInstance()
            .patientDetailsData?.extendedUserDetails?.employer?.address?.city.let {
            placeEdt.setText(it)
        }

        previous2Btn.setOnClickListener { activity?.onBackPressed() }

        continue2Btn.setOnClickListener {
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.occupation =
                    jobEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.name =
                    employerEdt.text.toString()
            if (PatientDetailsHelper.getInstance().patientDetailsData?.extendedUserDetails?.employer?.address == null) {
                PatientDetailsHelper.getInstance()
                    .patientDetailsData?.extendedUserDetails?.employer?.address = Address()
            }
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.address?.postalCode =
                    employerPostcodeEdt.text.toString()
            PatientDetailsHelper.getInstance()
                .patientDetailsData?.extendedUserDetails?.employer?.address?.city =
                    placeEdt.text.toString()

            showNewFragment(
                    CaregiverFragment(), R.id.container)
        }
    }

    fun showNewFragment(frag: Fragment, container: Int) {
        val fragmentManager = activity!!.supportFragmentManager
        val currentFragment = fragmentManager.findFragmentById(container)
        if (currentFragment != null && currentFragment.javaClass == frag.javaClass)
            return
        fragmentManager.beginTransaction()
            .replace(container, frag)
            .addToBackStack(null)
            .commit()
    }
}

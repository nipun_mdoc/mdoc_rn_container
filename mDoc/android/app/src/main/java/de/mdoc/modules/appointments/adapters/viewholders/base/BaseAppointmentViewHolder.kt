package de.mdoc.modules.appointments.adapters.viewholders.base

import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.isTablet
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.pojo.AppointmentDetails

abstract class BaseAppointmentViewHolder(itemView: View): RecyclerView.ViewHolder(itemView), BaseAppointmentInterface {

    protected lateinit var _item: AppointmentDetails
    private var _appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback? = null
    protected var _hasHeader = false

    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        _item = item
        _appointmentCallback = appointmentCallback
        _hasHeader = hasHeader
    }

    fun showAppointmentDetailsWithTabletCheck(view: View?) {
        if ((view?.context as MdocActivity).isTablet()) {
            _appointmentCallback?.onDayItemClicked(_item, view)
        } else {
            _appointmentCallback?.showAppointmentDetails(_item, view)
        }
    }

    fun isAppointmentUpdated(): Boolean {
        return _item.isShowHint
    }

    companion object {
        const val TIME_FORMAT = "HH:mm"
        const val SHORT_TIME_FORMAT = "HH"
    }
}
package de.mdoc.network.request;

import de.mdoc.pojo.FreeSlotsAppointmentDetails;

public class BookAppointmentRequest {

    private FreeSlotsAppointmentDetails appointmentDetails;
    private boolean deleted;
    private String id;
    private String integrationType;

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FreeSlotsAppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(FreeSlotsAppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}

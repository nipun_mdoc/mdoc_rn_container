package de.mdoc.modules.media

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MainActivityViewModel
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.media.media_adapters.MediaDashboardAdapter
import de.mdoc.modules.media.media_view_model.MediaStatisticsViewModel
import de.mdoc.network.RestClient
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_media.*

class MediaFragment: NewBaseFragment() {
    lateinit var activity: MdocActivity
    private val mediaStatisticsViewModel by viewModel {
        MediaStatisticsViewModel(
                RestClient.getService())
    }

    private val viewModel: MainActivityViewModel by activityViewModels()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        activity = context as MdocActivity

        return inflater.inflate(R.layout.fragment_media, container, false)
    }

    override val navigationItem: NavigationItem = NavigationItem.Media

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        getMediaData()
        viewModel.isOpenMenu.observe(viewLifecycleOwner) {
            rv_media_dashboard?.invalidate()
            getMediaData()
        }

    }

    private fun getMediaData() {
        mediaStatisticsViewModel.mediaStatistics.observe(viewLifecycleOwner, Observer { mediaStatResponse ->
            if (!mediaStatResponse.isNullOrEmpty()) {
                rv_media_dashboard?.layoutManager = LinearLayoutManager(activity)
                rv_media_dashboard?.adapter = MediaDashboardAdapter(activity, mediaStatResponse)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_media)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mediaCategories) {
            val action = MediaFragmentDirections.actionMediaFragmentToMediaBottomSheetDialogFragment()
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }
}
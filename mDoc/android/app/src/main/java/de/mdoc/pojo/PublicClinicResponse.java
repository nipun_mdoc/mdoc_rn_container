package de.mdoc.pojo;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by ema on 10/30/17.
 */

public class PublicClinicResponse  extends MdocResponse{

    private ClinicDetails data;

    public ClinicDetails getData() {
        return data;
    }

    public void setData(ClinicDetails data) {
        this.data = data;
    }
}

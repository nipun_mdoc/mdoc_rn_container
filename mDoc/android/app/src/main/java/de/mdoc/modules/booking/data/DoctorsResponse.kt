package de.mdoc.modules.booking.data

data class DoctorsResponse(
        val data: List<Doctor>? = listOf()
                          )

data class Doctor(
        val address: Address? = Address(),
        val description: String? = "",
        val email: String? = "",
        val fax: String? = "",
        val id: String? = "",
        val mobile: String? = "",
        val name: String? = "",
        val oid: String? = "",
        val resourcePurposes: List<String?>? = listOf(),
        val telephone: String? = ""
                  )

data class Address(
        val city: String? = "",
        val country: String? = "",
        val id: String? = "",
        val oid: String? = "",
        val street: String? = "",
        val zip: String? = ""
                  )


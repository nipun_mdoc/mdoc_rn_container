package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.TextView
import de.mdoc.R
import de.mdoc.interfaces.IPostAnswerForMatrix
import de.mdoc.modules.questionnaire.data.ChildQuestion
import de.mdoc.modules.questionnaire.data.QuestionTitle
import de.mdoc.modules.questionnaire.questionnaire_adapter.MatrixAdapter
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question

internal class MatrixQuestionType(val listener: IPostAnswerForMatrix): QuestionType() {

    private var titleList: ArrayList<QuestionTitle> = ArrayList()
    private var lastExpandedPosition = -1

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.matrix_type_layout, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        val expandableListView: ExpandableListView
        val matrixAdapter: ExpandableListAdapter
        questionIsAnswered.value = false
        val questionData: HashMap<QuestionTitle, ArrayList<ChildQuestion>> = extractData(question)
        expandableListView = view.findViewById(R.id.matrixListView)
        if (expandableListView != null) {
            for (questionTitle in questionData.keys) {
                titleList.add(questionTitle)
            }
            matrixAdapter = MatrixAdapter(view.context, titleList, questionData, questionIsAnswered, isNewAnswer)
            matrixAdapter.setPostAnswerListener(listener)
            expandableListView.setAdapter(matrixAdapter)
//            expandableListView.expandGroup(0)
            expandableListView.setOnGroupExpandListener { groupPosition ->
                if (groupPosition != 0) {
                    expandableListView.collapseGroup(0)
                }
                if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                    expandableListView.collapseGroup(lastExpandedPosition)
                }
                lastExpandedPosition = groupPosition
            }
        }

        mandatoryCheck(mandatory, null, question)

        return view
    }

    private fun extractData(question: Question): HashMap<QuestionTitle, ArrayList<ChildQuestion>> {
        val data: LinkedHashMap<QuestionTitle, ArrayList<ChildQuestion>> = LinkedHashMap()
        for (matrixQuestion in question.questionData.matrixQuestions) {
            val questionTitle = QuestionTitle()
            val options: ArrayList<ChildQuestion> = ArrayList()
            questionTitle.poolId = matrixQuestion.id
            questionTitle.title = matrixQuestion.question
            for (answer in matrixQuestion.questionData.options) {
                val childQuestion = ChildQuestion()
                if (answer.getValue(question.selectedLanguage) == matrixQuestion.answer?.answerData) {
                    childQuestion.answer = matrixQuestion.answer
                }
                childQuestion.questionId = matrixQuestion.id
                childQuestion.key = answer.key
                childQuestion.value = answer.getValue(question.selectedLanguage)
                options.add(childQuestion)
            }
            data[questionTitle] = options
        }
        return data
    }
}
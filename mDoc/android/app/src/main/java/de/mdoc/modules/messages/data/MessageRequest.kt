package de.mdoc.modules.messages.data

import org.joda.time.DateTime

data class MessageRequest(var senderId: String? = null,
                          var receiverId: String? = null,
                          var fileIdList: List<String>? = null,
                          var conversationId: String? = null,
                          var text: String? = null,
                          var encounterId: String? = null,
                          var priority: Int? = null,
                          var conversation: Conversation? = null,
                          var date: String? = null)

data class Conversation(var reason: String ? = null,
                        var description: String? = null,
                        var externalId: String? = null,
                        var letterbox: String? = null,
                        var type: String? = null)
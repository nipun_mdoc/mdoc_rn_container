package de.mdoc.modules.entertainment.fragments.access

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.data.my_stay_data.MyStayWidgetData
import de.mdoc.util.formatMdocDate
import de.mdoc.util.serializableArgument
import kotlinx.android.synthetic.main.fragment_entertainment_not_available.*


class NotAvailableFragment : Fragment() {

    var myStayData by serializableArgument<MyStayWidgetData>("myStayData")

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_entertainment_not_available, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.action_entertainmentNotAvailable_to_entertainment_navigation_graph)
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        if (myStayData.isBeforeCheckIn()) {
            txtTitle.setText(R.string.entertainment_check_in_title)
            txtDescription.setText(R.string.entertainment_check_in_description)
            txtDateTitle.setText(R.string.entertainment_check_in_date)
            txtDate.text = myStayData.getCheckInDate()?.formatMdocDate()

        } else if (myStayData.isAfterCheckOut()) {
            txtTitle.setText(R.string.entertainment_check_out_title)
            txtDescription.setText(R.string.entertainment_check_out_description)
            txtDateTitle.setText(R.string.entertainment_check_out_date)
            txtDate.text = myStayData.getCheckOutDate()?.formatMdocDate()
        }
    }
}
package de.mdoc.modules.mental_goals.data_goals

data class IfThenPlan(
        var id: String?,
        var planNumber:Int?,
        var ifValue: String,
        var thenValue: String
)

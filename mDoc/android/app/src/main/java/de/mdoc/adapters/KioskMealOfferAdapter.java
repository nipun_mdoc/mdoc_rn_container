package de.mdoc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.pojo.MealOffer;
/**
 * Created by ema on 10/31/17.
 */

public class KioskMealOfferAdapter  extends RecyclerView.Adapter<KioskMealOfferAdapter.ViewHolder> {

    private LayoutInflater inflater;
    Context context;
    ArrayList<MealOffer> offers;

    public KioskMealOfferAdapter(Context context, ArrayList<MealOffer> offers) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.offers = offers;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.kiosk_meal_offer_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MealOffer item = offers.get(position);

        holder.mealNameTv.setText(item.getName(context.getResources()));
        holder.mealDescriptionTv.setText(item.getDescription());
        String options = "";


        if(item.getOptions().contains(MdocConstants.GLUTEN_FREE)){
            options += context.getString(R.string.gluten_free).toUpperCase() + " ";
        }
        if(item.getOptions().contains(MdocConstants.VEGETARIAN)){
            options += context.getString(R.string.vegetarian).toUpperCase() + " ";
        }
        if(item.getOptions().contains(MdocConstants.COW)) {
            options += context.getString(R.string.cow).toUpperCase() + " ";
        }
        if(item.getOptions().contains(MdocConstants.PIG)){
            options += context.getString(R.string.pig).toUpperCase() + " ";
        }
        if(item.getOptions().contains(MdocConstants.CHICKEN)){
            options += context.getString(R.string.chicken).toUpperCase() + " ";
        }
        if(item.getOptions().contains(MdocConstants.FISH)){
            options += context.getString(R.string.fish).toUpperCase() + " ";
        }


        holder.optionsTv.setText(options);

    }

    @Override
    public int getItemCount() {
        return offers.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.mealNameTv)
        TextView mealNameTv;
        @BindView(R.id.mealDescriptionTv)
        TextView mealDescriptionTv;
        @BindView(R.id.optionsTv)
        TextView optionsTv;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}

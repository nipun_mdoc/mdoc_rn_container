package de.mdoc.network.request;

import android.content.Context;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.constants.MdocConstants;

/**
 * Created by kodecta-mac on 8/7/17.
 */

public class PostNotificationRequest {

    private int skip;
    private int limit;
    private String query;
    private ArrayList<String> excludedApplicationEvents;

    public PostNotificationRequest(int skip, int limit, Context current) {
        this.skip = skip;
        this.limit = limit;
        excludedApplicationEvents = new ArrayList<>();
        boolean isAppointmentExcluded = current.getResources().getBoolean(R.bool.has_exclude_appointment_reminder);
        if(!isAppointmentExcluded) {
            excludedApplicationEvents.add(MdocConstants.APPOINTMENT_REMINDER);
        }
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}

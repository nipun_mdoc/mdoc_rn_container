package de.mdoc.modules.questionnaire

import android.annotation.SuppressLint
import android.os.Bundle
import android.speech.tts.TextToSpeech
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields
import de.mdoc.modules.questionnaire.multilanguage.MultiLanguageViewModel
import de.mdoc.network.RestClient
import de.mdoc.pojo.*
import de.mdoc.util.MdocUtil
import de.mdoc.util.StringUtils
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.layout_questionnaire_start.*
import kotlinx.android.synthetic.main.questionnaire_list_item.*
import java.util.*

/**
 * Created by ema on 12/27/16.
 */
class IntroQuestionnaireFragment : MdocFragment() {

    private val viewModel by viewModel {
        MultiLanguageViewModel(RestClient.getService())
    }

    private var textToSpeech: TextToSpeech? = null
    private var speakSound = true
    private var response: QuestionariesWelcomeResponse? = null
    private var userSelectedLanguage: String? = null

    private val args: IntroQuestionnaireFragmentArgs by navArgs()

    override fun setResourceId(): Int {
        return R.layout.fragment_questionnaire_start
    }

    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireProgress


    private fun initSpinner(items: List<CodingItem>?, spinner: AppCompatSpinner, onClick: (String) -> Unit) {
        if (items?.isNullOrEmpty() == false) {
            val countrySpinnerAdapter = de.mdoc.adapters.SpinnerAdapter(requireContext(), items)
            spinner.adapter = countrySpinnerAdapter

            spinner.onItemSelectedListener =
                    object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(p0: AdapterView<*>?) {}

                        override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                            onClick(items.getOrNull(p2)?.code ?: "")
                        }
                    }
        }
    }

    override fun init(savedInstanceState: Bundle?) {
        args.pollAssignId?.let {
            observeApiData()
            viewModel.getData(it)
        }

        textToSpeech = TextToSpeech(activity) { i ->
            if (i != TextToSpeech.ERROR) {
                textToSpeech!!.language = Locale.getDefault()
            }
        }
    }

    private fun observeApiData() {
        viewModel.response.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            if (it != null) {
                response = it
                fillData(it)
                checkCopyright(it)
                fillMultiLanguage(it)
            }
        })
    }


    private fun getSelectedLanguage(data: QuestionariesWelcomeResponse, userSelection: String?): String? {
        if (userSelection != null) {
            return userSelection
        }
        return data.getAnswersLanguage() ?: data.getDefaultLanguage()
    }

    private fun fillMultiLanguage(data: QuestionariesWelcomeResponse) {
        if (data.isCodingValid()) {

            // Filter API coding data to show only supported languages
            val supportedCoding = data.getCodingData()?.filter {
                data.getSupportedLanguages()?.contains(it.code) ?: false
            }

            // Init multilanguage dropdown with data
            initSpinner(supportedCoding, spinnerLanguages) {
                userSelectedLanguage = it
                fillData(response)
            }

            // Set multilanguage dropdown initial selection
            supportedCoding?.forEachIndexed { index, codingItem ->
                if (codingItem.code == getSelectedLanguage(data, userSelectedLanguage)) {
                    spinnerLanguages.setSelection(index)
                }
            }

            // Disable multilanguage dropdown in case when questionnaire has been started already
            spinnerLanguages.isEnabled = !data.hasAnyAnswers()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressSb?.setOnTouchListener { _, _ -> true }
        spinnerLanguages.isEnabled = false
        setupListeners()
    }

    private fun setupListeners() {
        speakerStart?.setOnClickListener {
            if (pollWelcomeMessageTv!!.text.toString() != "") {
                speakSound = if (speakSound) {
                    val speak = pollWelcomeMessageTv!!.text.toString()
                    textToSpeech?.speak(speak, TextToSpeech.QUEUE_FLUSH, null)
                    false
                } else {
                    textToSpeech?.stop()
                    true
                }
            }
        }

        startBtn?.setOnClickListener {
            MdocUtil.showToastShort(activity?.applicationContext, getString(R.string.save_data))

            val safeResponse = response
            val pollId = args.pollId
            val pollAssignId = args.pollAssignId

            if (pollId != null && safeResponse != null) {
                val questionnaireAdditionalFields =
                        QuestionnaireAdditionalFields(pollAssignId = pollAssignId,
                                pollId = pollId)
                val action = MainNavDirections.globalActionToQuestion(
                        questionnaireAdditionalFields = questionnaireAdditionalFields,
                        selectedLanguage = getSelectedLanguage(safeResponse, userSelectedLanguage))
                findNavController().navigate(action)
            }
        }
    }

    override fun onPause() {
        if (textToSpeech != null) {
            textToSpeech!!.stop()
            textToSpeech!!.shutdown()
        }
        super.onPause()
    }

    private fun fillData(response: QuestionariesWelcomeResponse?) {
        response?.detailsResponse?.data?.let {
            val selectedLanguage = getSelectedLanguage(response, userSelectedLanguage)
            titleTv?.text = it.getNameLang(selectedLanguage)

            progressSb?.max = it.answerDetails.total
            progressSb?.progress = it.answerDetails.completed
            val progressText = MdocUtil.calculateProgress(it.answerDetails.completed, it.answerDetails.total).toString() + "%"
            progressTv?.text = progressText

            val lstValues: List<String>? = it.getIntroductionLang(selectedLanguage).split("<span style=\"text-decoration: underline;\">")?.map { it -> it.trim() }
            val finalString = StringBuilder()
            lstValues?.forEachIndexed { index, it ->
                val str = it.toString()
                if(index == 0){
                    finalString.append(str)
                }else{
                    val ii = StringUtils.matchDetails(str, "</span>", 0)
                    finalString.append(" <span style=\"text-decoration: underline;\"><u>").append(str.substring(0, ii)).append("</u>").append(str.substring(ii, str.length));
                }
            }


            pollWelcomeMessageTv?.text = StringUtils.fromHtml(finalString.toString())
            startBtn?.text = getString(R.string.to_questionnaire)
        }
    }

    private fun checkCopyright(response: QuestionariesWelcomeResponse?) {
        response?.detailsResponse?.data?.let {
            if (it.isCopyright) {
                layoutCopyright.visibility = View.VISIBLE
                txtCopyrightDescription.text = it.copyrightDescription
                imgCopyrightIcon.setImageBitmap(it.copyrightImage)
            }
        }
    }
}
package de.mdoc.modules.help_support

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import kotlinx.android.synthetic.main.fragment_help_and_support_details.*

class HelpSupportDetailsDialog: DialogFragment() {

    private val args: HelpSupportDialogArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_help_and_support_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnBack.setOnClickListener { dismiss() }
        txtInfoAppName.text = args.helpSupportData.name
        txtInfoAppVersion.text = args.helpSupportData.app
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.MATCH_PARENT
        val height = RelativeLayout.LayoutParams.MATCH_PARENT
        dialog?.window?.setLayout(width, height)
        dialog?.setCanceledOnTouchOutside(true)

        super.onResume()
    }
}
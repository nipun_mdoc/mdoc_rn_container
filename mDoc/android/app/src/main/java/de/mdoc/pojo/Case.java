package de.mdoc.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class Case implements Serializable {

    private String clinicAddress;
    private String clinicName;
    private String caseId;
    private boolean checkIn;
    private long checkInTime;
    private long checkOutTime;
    private String clinicId;
    private String department;
    private String departure;
    private String roomStation;
    private String externalCaseId;
    @SerializedName("premium")
    private boolean isPremium;
    @SerializedName("active")
    private boolean isActive;
    private long checkOutActualTime;

    public void setCheckOutActualTime(long checkOutActualTime) {
        this.checkOutActualTime = checkOutActualTime;
    }

    public long getCheckOutActualTime() {
        return checkOutActualTime;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public String getClinicName() {
        return clinicName;
    }

    public void setClinicName(String clinicName) {
        this.clinicName = clinicName;
    }

    public boolean isPremium() {
        return isPremium;
    }

    public void setPremium(boolean premium) {
        isPremium = premium;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public boolean isCheckIn() {
        return checkIn;
    }

    public void setCheckIn(boolean checkIn) {
        this.checkIn = checkIn;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getRoomStation() {
        return roomStation;
    }

    public void setRoomStation(String roomStation) {
        this.roomStation = roomStation;
    }

    public String getExternalCaseId() {
        return externalCaseId;
    }

    public void setExternalCaseId(String externalCaseId) {
        this.externalCaseId = externalCaseId;
    }
}
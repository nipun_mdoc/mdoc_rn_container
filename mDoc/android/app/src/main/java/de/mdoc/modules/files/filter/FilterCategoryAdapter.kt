package de.mdoc.modules.files.filter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.mdoc.R
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder
import kotlinx.android.synthetic.main.common_filter_checkbox_item.view.*
import timber.log.Timber

interface FilterCategoryListener {
    fun onCategoryChecked(item: FilterItem)
}

class FilterCategoryHolder(itemView: View, private val listener: FilterCategoryListener) : ListItemViewHolder(itemView), View.OnClickListener {
    private val checkBox = itemView.checkBoxItem

    lateinit var item: FilterItem

    init {
        checkBox.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        listener.onCategoryChecked(item)
    }

    fun bind(item: FilterItem, position: Int) {
        this.item = item
        checkBox.text = item.getName(itemView.context)
        checkBox.isChecked = item.isChecked

        when (position) {
            // 'All categories' position
            0 -> {
                // Prevent user to uncheck this manually
                checkBox.isEnabled = !checkBox.isChecked
            }
            else -> {
                checkBox.isEnabled = true
            }
        }
    }
}

class FilterCategoryAdapter(
        private val listener: FilterCategoryListener
) : ListItemTypeAdapter<FilterItem, FilterCategoryHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): FilterCategoryHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.common_filter_checkbox_item, parent, false)
        return FilterCategoryHolder(view, listener)
    }

    override fun onBind(position: Int, item: FilterItem, holder: FilterCategoryHolder) {
        super.onBind(position, item, holder)
        holder.bind(item, position)
    }

    override fun getId(position: Int, item: FilterItem): Long {
        return item.category.getId()
    }
}
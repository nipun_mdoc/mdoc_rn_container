package de.mdoc.modules.airandpollen.data

import de.mdoc.network.response.PollenResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.service.IMdocService
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class AirAndPollenRepository(
    val mdocService: IMdocService
) {

    private val disposables = CompositeDisposable()

    fun getAirAndPollen(
        resultListener: (List<Region>) -> Unit,
        errorListener: (Throwable) -> Unit
    ) {
        mdocService.pollen.enqueue(object : Callback<PollenResponse> {

            override fun onResponse(
                call: Call<PollenResponse>,
                response: Response<PollenResponse>
            ) {
                val result = response.body()
                if (result != null) {
                    parseData(result, resultListener, errorListener)
                } else {
                    errorListener.invoke(RuntimeException("Body for ${call.request().url} is null"))
                    Timber.w("getAirAndPollen ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<PollenResponse>, t: Throwable) {
                Timber.w(t, "getAirAndPollen")
                errorListener.invoke(t)
            }
        })
    }

    private fun parseData(
        response: PollenResponse,
        resultListener: (List<Region>) -> Unit,
        errorListener: (Throwable) -> Unit
    ) {
        Single.just(response).map(::mapResponse)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(resultListener, errorListener)
            .addTo(disposables)
    }

    private fun mapResponse(response: PollenResponse): List<Region> {
        val regions = response.data[0].rawData.content
        return regions.map { content ->
            val name = if (content.partregion_name.isBlank()) content.region_name else content.partregion_name
            val today = mutableListOf<AirAndPollenInfo>()
            val tomorrow = mutableListOf<AirAndPollenInfo>()
            val dayAfterTomorrow = mutableListOf<AirAndPollenInfo>()
            PollenType.values().forEach { type ->
                content.Pollen[type.jsonName]?.let { pollen ->
                    today.add(AirAndPollenInfo(type, pollen.today.toAmount()))
                    tomorrow.add(AirAndPollenInfo(type, pollen.tomorrow.toAmount()))
                    dayAfterTomorrow.add(AirAndPollenInfo(type, pollen.dayafter_to.toAmount()))
                } ?: Timber.w("Pollen type $type not found for $name")
            }
            Region(content.partregion_id, name, today, tomorrow, dayAfterTomorrow)
        }.filter {
            it.today.isNotEmpty() || it.tomorrow.isNotEmpty() || it.dayAfterTomorrow.isNotEmpty()
        }
    }

    fun recycle() {
        disposables.dispose()
    }

    private fun String.toAmount(): Int {
        return when (this) {
            "0-1" -> 1
            "1" -> 2
            "1-2" -> 3
            "2" -> 4
            "2-3" -> 5
            "3" -> 6
            "-1" -> -1
            else -> 0
        }
    }

}
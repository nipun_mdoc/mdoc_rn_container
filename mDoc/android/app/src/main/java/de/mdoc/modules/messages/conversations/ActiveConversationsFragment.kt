package de.mdoc.modules.messages.conversations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import de.mdoc.R
import de.mdoc.databinding.FragmentActiveConversationsBinding
import de.mdoc.modules.messages.ConversationsViewModel

class ActiveConversationsFragment: Fragment() {
    var conversationsViewModel: ConversationsViewModel? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentActiveConversationsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_active_conversations, container, false)

        binding.lifecycleOwner = this
        binding.handler = ConversationsHandler()
        binding.conversationsViewModel = conversationsViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        conversationsViewModel?.getActiveConversations(false)
    }
}
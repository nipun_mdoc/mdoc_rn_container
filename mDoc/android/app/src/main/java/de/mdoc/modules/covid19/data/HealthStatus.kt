package de.mdoc.modules.covid19.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HealthStatus(
        val id: String? = null,
        val healthStatus: String? = null,
        var ill: Boolean? = null,
        var illnesType: String? = null,
        var lastUpdate: String? = null,
        var recovered: Boolean? = null,
        var testDate: Long? = null,
        val cts: Long? = null,
        val uts: Long? = null,
        val userId: String? = null,
        val creatorId: String?=null,
        val lastName:String?=null,
        var patientUpdateAllowed:Boolean=true
                       ):Parcelable {

    fun isNotIll(): Boolean {
        return if (ill == null) {
            false
        }
        else {
            !ill!!
        }
    }

    fun isNotRecovered(): Boolean {
        return if (recovered == null) {
            false
        }
        else {
            !recovered!!
        }
    }
    fun isCratedByAdmin()= (userId!=null)&&(userId!=creatorId)

}
package de.mdoc.service;

import de.mdoc.pojo.WeatherResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ema on 11/1/17.
 */

public interface IWeatherService {

    @GET("data/2.5/forecast")
    Call<WeatherResponse> getWeather(@Query("lat") float lat, @Query("lon") float lon, @Query("APPID") String apiKey, @Query("units") String units);

}

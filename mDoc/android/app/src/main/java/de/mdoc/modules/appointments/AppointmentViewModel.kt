package de.mdoc.modules.appointments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class AppointmentViewModel: ViewModel() {

    val dateRange:MutableLiveData<String> = MutableLiveData("")
}

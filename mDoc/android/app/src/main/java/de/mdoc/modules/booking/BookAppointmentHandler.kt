package de.mdoc.modules.booking

import android.content.Context
import de.mdoc.R
import de.mdoc.pojo.FreeSlotsList
import org.joda.time.format.DateTimeFormat

class BookAppointmentHandler(private val isAppointmentChange: Boolean) {

    interface BookAppointmentCallback {
        fun onFreeSlotClick(appointment: FreeSlotsList)
    }
    private var bookingCallback: BookAppointmentCallback? = null

    fun setBleResponseListener(bookingCallback: BookAppointmentCallback) {
        this.bookingCallback = bookingCallback
    }

    fun bookAppointment(appointment: FreeSlotsList) {
        bookingCallback?.onFreeSlotClick(appointment)
    }

    fun getSlotsTime(item: FreeSlotsList): String {
        val formatter = DateTimeFormat.forPattern("HH:mm")
        val timeFrom = item.appointmentDetails?.start
        val timeTo = item.appointmentDetails?.end

        return when {
            timeFrom != null && timeTo != null && item.appointmentDetails.isShowEndTime && item.appointmentDetails.end!=null && item.appointmentDetails.end.toString().trim()!="" && item.appointmentDetails.end != 0.toLong() -> {
                formatter.print(timeFrom)+" - "+formatter.print(timeTo)
            }
            timeFrom != null && !item.appointmentDetails.isShowEndTime -> {
                formatter.print(timeFrom)
            }
            else -> {
                ""
            }
        }
    }

    fun getButtonText(context: Context): String {
        return if (isAppointmentChange) {
            context.getString(R.string.booking_button_change)
        }
        else {
            context.getString(R.string.booking_button_book)
        }
    }
}
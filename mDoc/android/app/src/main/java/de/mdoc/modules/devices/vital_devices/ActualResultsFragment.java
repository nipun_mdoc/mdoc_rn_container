package de.mdoc.modules.devices.vital_devices;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.LinkedHashMap;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.StatisticData;
import de.mdoc.util.MdocAppHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActualResultsFragment extends MdocFragment {

    MdocActivity activity;
    @BindView(R.id.circleStepsX)
    CircularProgressBar circleStepsX;
    @BindView(R.id.circleDistanceX)
    CircularProgressBar circleDistanceX;
    @BindView(R.id.circleCaloriesX)
    CircularProgressBar circleCaloriesX;

    @BindView(R.id.txtStepsCurrentX)
    TextView txtStepsCurrent;
    @BindView(R.id.txtCalloriesCurrentX)
    TextView txtCalloriesCurrent;
    @BindView(R.id.txtDistanceCurrentX)
    TextView txtDistanceCurrent;

    @BindView(R.id.txtStepsDescX)
    TextView txtStepsDesc;
    @BindView(R.id.txtDistanceDescX)
    TextView txtDistanceDesc;
    @BindView(R.id.txtCaloriesDescX)
    TextView txtCaloriesDesc;

    ProgressDialog progressDialog;

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Devices;
    }

    @Override
    protected int setResourceId() {
        return R.layout.fragment_actual_results;
    }

    @Override
    protected void init(Bundle savedInstanceState) {

        activity = getMdocActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);
        progressDialog.show();
        getStatDataForProvider();
    }

    private void getStatDataForProvider() {
        String userId = MdocAppHelper.getInstance().getUserId();
        DateTime dt = new DateTime();
        DateTime utc = new DateTime(dt, DateTimeZone.UTC);
        String date = String.valueOf(utc.getMillis());

        String provider = "fitbit";

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("date", date);
        params.put("provider", provider);
        params.put("userId", userId);

        MdocManager.getStatistics(params, new Callback<StatisticData>() {
            @Override
            public void onResponse(Call<StatisticData> call, Response<StatisticData> response) {

                int goalCallories = 0;
                double goalDistance = 0;
                int goalSteps = 0;
                int currentCallories = 0;
                double currentDistance = 0;
                int currentSteps = 0;

                try {
                    if (response.body() != null) {

                        if (response.body().getData().getActivity().getGoals() != null) {
                            goalCallories = response.body().getData().getActivity().getGoals().getCaloriesOut();
                            goalDistance = response.body().getData().getActivity().getGoals().getDistance();
                            goalSteps = response.body().getData().getActivity().getGoals().getSteps();
                        }

                        if (response.body().getData().getActivity().getSummary() != null) {
                            currentCallories = response.body().getData().getActivity().getSummary().getCaloriesOut();
                            currentDistance = response.body().getData().getActivity().getSummary().getDistances().get(0).getDistance();
                            currentSteps = response.body().getData().getActivity().getSummary().getSteps();
                        }

                        circleDistanceX.setProgressWithAnimation(calculateProcentFromNumbers(currentDistance, goalDistance), 800);
                        circleStepsX.setProgressWithAnimation(calculateProcentFromNumbers(currentSteps, goalSteps), 800);
                        circleCaloriesX.setProgressWithAnimation(calculateProcentFromNumbers(currentCallories, goalCallories), 800);

                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                }

                txtCalloriesCurrent.setText(String.valueOf(currentCallories));
                txtStepsCurrent.setText(String.valueOf(currentSteps));
                txtDistanceCurrent.setText(String.valueOf(currentDistance));

                txtCaloriesDesc.setText(getResources().getString(R.string.calories_text_first) + " " + goalCallories + " " + getResources().getString(R.string.calories_text_second));
                txtDistanceDesc.setText(getResources().getString(R.string.distance_text_first) + " " + goalDistance + " " + getResources().getString(R.string.distacne_text_second));
                txtStepsDesc.setText(getResources().getString(R.string.step_text_first) + " " + goalSteps + " " + getResources().getString(R.string.step_test_second));
            }

            @Override
            public void onFailure(Call<StatisticData> call, Throwable t) {
            }
        });
    }

    private int calculateProcentFromNumbers(double current, double total) {
        return calculateDegreseFromProcent((current / total) * 100);
    }

    private int calculateDegreseFromProcent(double procent) {
        double total = procent * 0.01;
        return (int) (total * 100);
    }
}

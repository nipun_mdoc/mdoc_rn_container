package de.mdoc.pojo

import de.mdoc.constants.FACEBOOK
import de.mdoc.constants.INSTAGRAM
import de.mdoc.constants.TWITTER
import de.mdoc.util.overrideNullText
import java.io.Serializable
import kotlin.collections.ArrayList

/**
 * Created by ema on 1/20/17.
 */
class ClinicDetails: Serializable {

    lateinit var images: ArrayList<Image>
    var specifications: String? = null
    var description: String? = null
    var name: String? = null
    var clinicId: String? = null
    var address: Address? = null
    lateinit var headOfClinic: HeadOfClinic
    lateinit var departments: ArrayList<Department>
    var agendaDetails: ArrayList<AgendaDetails>? = null
    var contactDetails: ContactDetails? = null
    var isAccompany: Boolean = false
    var isChild: Boolean = false
    var isStandard: Boolean = false
    var isPremium: Boolean = false
    var latitude: Float = 0.toFloat()
    var longitude: Float = 0.toFloat()
    var socialMediaDetails: ArrayList<SocialMediaDetails>? = null
    var settings: Setting? = null
    var clinicGroup: String? = null
    var specialities:ArrayList<String>? = null
    var specialitiesNames:ArrayList<String>? = ArrayList()

    override fun toString(): String = this.name ?: ""

    fun getFullAddress(): String {
        var result = overrideNullText(address?.street)
        val houseNumber = overrideNullText(address?.houseNumber)
        result += if (houseNumber.isEmpty()) {
            if (result.isEmpty()) {
                ""
            }
            else {
                ", "
            }
        }
        else {
            "$houseNumber, "
        }
        result += "${overrideNullText(address?.postalCode)} ${overrideNullText(address?.city)}"
        return if (result.trim() == "") "NA" else result
    }

    fun showFacebook(): Boolean {
        socialMediaDetails?.forEach {
            if (it.hasFacebook()) {
                return true
            }
        }
        return false
    }

    fun showInstagram(): Boolean {
        socialMediaDetails?.forEach {
            if (it.hasInstagram()) {
                return true
            }
        }
        return false
    }

    fun getAllSpecialities(): String{

        var result = ""
        specialitiesNames?.forEach {
            result = "$result$it<br><br>"
        }
        return "<html><ul>$result</ul></html>"
    }

    fun showTwitter(): Boolean {
        socialMediaDetails?.forEach {
            if (it.hasTwitter()) {
                return true
            }
        }
        return false
    }

    fun getFacebookLink(): String {
        socialMediaDetails?.forEach {
            if (it.hasFacebook()) {
                return it.link
            }
        }
        return ""
    }

    fun getInstagramLink(): String {
        socialMediaDetails?.forEach {
            if (it.hasInstagram()) {
                return it.link
            }
        }
        return ""
    }

    fun getTwitterLink(): String {
        socialMediaDetails?.forEach {
            if (it.hasTwitter()) {
                return it.link
            }
        }
        return ""
    }

    fun isSocialMediaEnabled(): Boolean {
        var isOneActive = false
        socialMediaDetails?.forEach {
            if (it.active) {
                isOneActive = true
            }
        }
        return isOneActive
    }

    fun shouldShowImage(): Boolean {
        return images?.size > 0 && !images?.getOrNull(0)?.image.isNullOrEmpty()
    }

    fun getFirstImageUrl(): String? =
            if (!images.isNullOrEmpty())
                images.getOrNull(0)?.image
            else
                null
}

data class Setting(var endpoints: Endpoints,
                   var mandatID: String,
                   var destinationID: String,
                   var clusterID: String,
                   var orgUnitID: String,
                   var nodeID: String): Serializable

data class Endpoints(var host: String,
                     var specialtyProvider: String,
                     var freeslots: String,
                     var book: String,
                     var cancel: String): Serializable

data class SocialMediaDetails(var type: String, var link: String, var active: Boolean): Serializable {

    fun hasFacebook(): Boolean {
        return type == FACEBOOK && active
    }

    fun hasInstagram(): Boolean {
        return type == INSTAGRAM && active
    }

    fun hasTwitter(): Boolean {
        return type == TWITTER && active
    }
}

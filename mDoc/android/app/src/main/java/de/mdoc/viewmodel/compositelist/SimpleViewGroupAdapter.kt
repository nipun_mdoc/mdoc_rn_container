package de.mdoc.viewmodel.compositelist

import android.view.ViewGroup

class SimpleViewGroupAdapter<T, Holder : ListItemViewHolder, Adapter : ListItemTypeAdapter<T, Holder>>(
    private val container: ViewGroup,
    private val adapter: Adapter
) {

    private val items = mutableListOf<T>()
    private val holders = mutableListOf<Holder>()

    init {
        refreshContentItems()
    }

    fun setItems(items: List<T>) {
        this.items.clear()
        this.items.addAll(items)
        refreshContentItems()
    }

    private fun refreshContentItems() {
        while (container.childCount < items.size) {
            val holder = adapter.onCreateViewHolder(container)
            container.addView(holder.itemView)
            holders.add(holder)
        }
        while (container.childCount > items.size) {
            val lastIndex = container.childCount - 1
            container.removeViewAt(lastIndex)
            holders.removeAt(lastIndex)
        }
        items.forEachIndexed { index, item ->
            adapter.onBind(index, item, holders[index])
        }
    }

}
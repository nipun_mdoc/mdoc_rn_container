package de.mdoc.modules.vitals.adapters

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.devices.data.Measurement
import de.mdoc.modules.vitals.unit_picker.UnitPickerViewModel
import de.mdoc.util.toBitmap

class UnitPickerAdapter(var context: Context, val viewModel: UnitPickerViewModel):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var measurements = viewModel.units.value?: arrayOf()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.placeholder_unit_picker,
                    viewGroup,
                    false
                    )
        return UnitPickerViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val item = measurements[position]

        (viewHolder as UnitPickerViewHolder).bind(item, viewModel)
    }

    override fun getItemCount() = measurements.size
}

class UnitPickerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    private val measurementName: TextView? = itemView.findViewById(R.id.measurementName)
    private val unitsRecyclerView: RecyclerView = itemView.findViewById(R.id.rv_units)

    fun bind(item: Measurement, viewModel: UnitPickerViewModel) {

        val loincCode = item.measure?.code ?: ""

        measurementName?.text = item.imageCoding?.display ?: ""
        val bitmapImage = item.imageCoding?.imageBase64?.toBitmap()
        val image: Drawable = BitmapDrawable(itemView.context.resources, bitmapImage)
        measurementName?.setCompoundDrawablesWithIntrinsicBounds(image, null, null, null)
        unitsRecyclerView.layoutManager = LinearLayoutManager(unitsRecyclerView.context)
        unitsRecyclerView.adapter = InnerUnitAdapter(viewModel, item.allUnits!!, loincCode)
    }
}
package de.mdoc.modules.entertainment.adapters.overview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.entertainment.adapters.history.MagazinesHistoryAdapter
import de.mdoc.modules.entertainment.fragments.all.MagazinesSeeAllFragment
import de.mdoc.modules.entertainment.data.OverviewAndBookmarks
import de.mdoc.modules.entertainment.data.isBookmarksEmpty
import de.mdoc.modules.entertainment.data.isCategoriesEmpty
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaReadingHistoryResponseDto_
import de.mdoc.pojo.entertainment.MediaStatisticResponseDto
import kotlinx.android.synthetic.main.entertainment_history_main_item.view.*
import kotlinx.android.synthetic.main.entertainment_magazines_main_item.view.*

class MagazinesOverviewMainAdapter(private val data: OverviewAndBookmarks?) : RecyclerView.Adapter<MagazinesOverviewMainAdapter.BaseViewHolder>() {

    companion object {
        private const val EMPTY = 0
        private const val DEFAULT = 1
        private const val HISTORY = 2
        private const val NO_CONTENT = 3

        private const val EXTRA_ROWS_COUNT = 2 // Bookmarks + Empty Data
    }

    private val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            DEFAULT -> {
                ViewHolder(LayoutInflater.from(parent.context).inflate(de.mdoc.R.layout.entertainment_magazines_main_item, parent, false))
            }
            HISTORY -> {
                HistoryViewHolder(LayoutInflater.from(parent.context).inflate(de.mdoc.R.layout.entertainment_history_main_item, parent, false))
            }
            NO_CONTENT -> {
                NoContentViewHolder(LayoutInflater.from(parent.context).inflate(de.mdoc.R.layout.entertainment_no_data, parent, false))
            }
            else -> {
                EmptyViewHolder(View(parent.context))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (isHistoryPosition(position)) {
            return if (data?.isBookmarksEmpty() == false) {
                HISTORY
            } else {
                EMPTY
            }
        }

        if (isNoDataPosition(position)) {
            return if (data?.isCategoriesEmpty() == true) {
                NO_CONTENT
            } else {
                EMPTY
            }
        }

        return DEFAULT
    }

    override fun getItemCount(): Int {
        val categoryCount: Int = data?.overview?.data?.list?.size?:0
        return categoryCount + EXTRA_ROWS_COUNT
    }

    private fun isHistoryPosition(position: Int): Boolean {
        return position == 0
    }

    private fun isNoDataPosition(position: Int): Boolean {
        return position == 1
    }

    private fun getItem(position: Int): MediaStatisticResponseDto? {
        return data?.overview?.data?.list?.get(position - EXTRA_ROWS_COUNT)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> {
                holder.bind(getItem(position))
            }
            is HistoryViewHolder -> {
                holder.bind(data?.bookmarks)
            }
        }
    }

    abstract class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    inner class HistoryViewHolder(itemView: View) : BaseViewHolder(itemView) {
        private val recyclerView: RecyclerView = itemView.magazinesHistoryRv

        fun bind(items: ResponseSearchResultsMediaReadingHistoryResponseDto_?) {
            recyclerView.apply {
                val childLayoutManager = LinearLayoutManager(recyclerView.context, RecyclerView.HORIZONTAL, false)
                childLayoutManager.initialPrefetchItemCount = 4
                layoutManager = childLayoutManager
                adapter = MagazinesHistoryAdapter(items)

                recyclerView.setHasFixedSize(true)

                adapter?.notifyDataSetChanged()
            }
        }
    }

    private inner class ViewHolder(itemView: View) : BaseViewHolder(itemView), View.OnClickListener {
        private var item: MediaStatisticResponseDto? = null

        private val recyclerView: RecyclerView = itemView.rv_child
        private val textView: TextView = itemView.textView
        private val seeAll: Button = itemView.btnMagazinesCategoryNext

        init {
            seeAll.setOnClickListener(this)
        }

        fun bind(item: MediaStatisticResponseDto?) {
            this.item = item
            textView.text = item?.category
            recyclerView.apply {
                val childLayoutManager = LinearLayoutManager(recyclerView.context, RecyclerView.HORIZONTAL, false)
                childLayoutManager.initialPrefetchItemCount = 4

                layoutManager = childLayoutManager
                adapter = MagazinesOverviewItemsAdapter(item?.content)

                recyclerView.setHasFixedSize(true)
                recyclerView.setRecycledViewPool(viewPool)
            }
        }

        override fun onClick(view: View?) {
            val bundle = Bundle()
            bundle.putString(MdocConstants.MAGAZINE_CATEGORY, item?.category)
            view?.findNavController()?.navigate(R.id.action_entertainmentOverview_to_entertainmentSeeAll, bundle)
        }
    }

    private inner class EmptyViewHolder(itemView: View) : BaseViewHolder(itemView)

    private inner class NoContentViewHolder(itemView: View) : BaseViewHolder(itemView)
}
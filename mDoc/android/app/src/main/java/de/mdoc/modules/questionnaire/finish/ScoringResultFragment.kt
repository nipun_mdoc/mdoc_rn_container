package de.mdoc.modules.questionnaire.finish

import android.app.Dialog
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.showProgressDialog
import de.mdoc.viewmodel.compositelist.CompositeListAdapter
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_scoring_result.*
import kotlinx.android.synthetic.main.fragment_scoring_result.view.*

class ScoringResultFragment: MdocFragment() {

    private val args: ScoringResultFragmentArgs by navArgs()

    private val viewModel by viewModel {
        ScoringResultViewModel(args.pollVotingId ?: "", args.showScoringResult, args.resultScreenTypeCode ?: "")
    }
    private val adapter = CompositeListAdapter().apply {
        addAdapter(TitleBar)
        addAdapter(ScoringResult)
    }
    private var progress: Dialog? = null

    override fun setResourceId(): Int = R.layout.fragment_scoring_result

    override val navigationItem: NavigationItem = NavigationItem.QuestionnaireResults

    override fun init(savedInstanceState: Bundle?) {
        // do nothing
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        handleOnBackPressed {
            onBackClicked()
        }
        view.scoringResults.adapter = adapter
        view.scoringResults.layoutManager = LinearLayoutManager(context)
        viewModel.results.observe(this) {
            adapter.setItems(mutableListOf<Any>().apply {
                add(FinishQuestionnaireTitle(args.endMessage ?: "", args.showScoringResult))
                addAll(it)
            })
        }
        viewModel.inProgress.observe(this) {
            if (it) {
                progress = showProgressDialog()
            }
            else {
                progress?.dismiss()
            }
        }
        finish?.setOnClickListener {
           onBackClicked()
        }
    }

    private fun onBackClicked(){
        val hasJourneyDetailsFragment = findNavController().popBackStack(R.id.journeyDetailsFragment, false)
        if (!hasJourneyDetailsFragment) {
            findNavController().popBackStack()
        }
    }
    override fun onDestroyOptionsMenu() {
        super.onDestroyOptionsMenu()
        progress?.dismiss()
    }
}


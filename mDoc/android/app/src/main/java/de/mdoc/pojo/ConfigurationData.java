package de.mdoc.pojo;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Pramod on 12/31/20.
 */

public class ConfigurationData extends ViewModel {
    @SerializedName("appcolor")
    @Expose
    private ConfigurationAppColorData  appColorData;
    @SerializedName("appconfig")
    @Expose
    private ArrayList<ConfigurationListItem> list = null;

    public ConfigurationData(){}
    public ConfigurationData(ConfigurationAppColorData  appColorData, ArrayList<ConfigurationListItem> list) {
        this.appColorData = appColorData;
        this.list = list;
    }

    public void setAppColorData(ConfigurationAppColorData appColorData){
        this.appColorData = appColorData;
    }
    public ConfigurationAppColorData getAppColorData(){
        return this.appColorData;
    }

    public ArrayList<ConfigurationListItem> getList() {
        return list;
    }

    public void setList(ArrayList<ConfigurationListItem> list) {
        this.list = list;
    }

    public boolean getWidgetVisibility(String id, String widget){
        ConfigurationListItem listItem = null;
        for (ConfigurationListItem item : list) {
            if (item.getId().equalsIgnoreCase(id)) {
                listItem = item;
            }
        }

        if(listItem == null) {
            return false;
        }else{
            if(listItem.getIsEnabled()) {
                SubConfigurationData subConfigurationData = null;
                for (SubConfigurationData item : listItem.getSubConfig()) {
                    if(item != null) {
                        if (item.getId().equalsIgnoreCase(widget)) {
                            subConfigurationData = item;
                        }
                    }
                }
                if (subConfigurationData == null) {
                    return false;
                }else {
                    return subConfigurationData.isEnabled();
                }
            }else{
                return false;
            }
        }
    }

    public int getAppointmentDisplayLimitWidget(String id, String widget){
        ConfigurationListItem listItem = null;
        for (ConfigurationListItem item : list) {
            if (item.getId().equalsIgnoreCase(id)) {
                listItem = item;
            }
        }

        if(listItem == null) {
            return 10;
        }else{
            if(listItem.getIsEnabled()) {
                SubConfigurationData subConfigurationData = null;
                for (SubConfigurationData item : listItem.getSubConfig()) {
                    if(item != null) {
                        if (item.getId().equalsIgnoreCase(widget)) {
                            subConfigurationData = item;
                        }
                    }
                }
                if (subConfigurationData == null) {
                    return 10;
                }else {
                    return subConfigurationData.getValue();
                }
            }else{
                return 10;
            }
        }
    }

}

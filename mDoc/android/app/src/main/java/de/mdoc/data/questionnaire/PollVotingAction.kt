package de.mdoc.data.questionnaire


data class PollVotingAction(
        val cts: Long,
        val finished: Long,
        val pollVotingActionId: String,
        val uts: Long,
        val language: String,
        val answers:ArrayList<Answer>
)
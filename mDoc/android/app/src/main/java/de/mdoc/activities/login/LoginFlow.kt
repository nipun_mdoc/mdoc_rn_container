package de.mdoc.activities.login

import android.app.Dialog
import android.content.Context
import android.util.Log
import android.widget.Toast
import com.google.gson.JsonObject
import de.mdoc.R
import de.mdoc.activities.LoginActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.SaveEmailRequest
import de.mdoc.network.response.*
import de.mdoc.pojo.Case
import de.mdoc.pojo.CreateUserDeviceResponse
import de.mdoc.pojo.SecretModel
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.util.token_otp.Token
import kotlinx.coroutines.flow.callbackFlow
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class LoginFlow(
        private val context: Context,
        private val loginCallback: LoginCallback,
        private val progressHandler: ProgressHandler,
        private val loginUtil: LoginUtil
): SaveMailDialogFragment.DialogSaveCallback {

    var userId: String = ""
    var enterEmailDialog: EnterEmailDialogFragment? = null

    interface LoginCallback {

        fun openTermsAndConditions()

        fun openPrivacyPolicy()

        fun openNextActivity(cases: ArrayList<Case>?)

        fun openClinics()

        fun reloadInitialPage()

        fun onMissingSecret ()
    }

    fun keycloackLoginServerTime(url: String) {
        progressHandler.showProgress()
        MdocManager.getServerTime(object : Callback<ServerTimeResponse> {
            override fun onResponse(
                    call: Call<ServerTimeResponse>,
                    response: Response<ServerTimeResponse>
            ) {
                keycloackLogin(url)
            }

            override fun onFailure(call: Call<ServerTimeResponse>, t: Throwable) {
                progressHandler.hideProgress()
                keycloackLogin(url)
            }
        })
    }

    private fun keycloackLogin(url: String) {
        if (MdocAppHelper.getInstance().secretKey != null) {
            try {
            } catch (e: Token.TokenUriInvalidException) {
                e.printStackTrace()
            }
        }
        val request = loginUtil.generateLoginRequest(url)

        MdocManager.loginWithKeycloackCode(request, object : Callback<KeycloackLoginResponse> {
            override fun onResponse(
                    call: Call<KeycloackLoginResponse>,
                    response: Response<KeycloackLoginResponse>
            ) {
                if (response.isSuccessful) {
                    MdocAppHelper.getInstance()
                            .accessToken =
                            response.body()!!.token_type + " " + response.body()!!.access_token
                    MdocAppHelper.getInstance()
                            .refreshToken =
                            response.body()!!.token_type + " " + response.body()!!.refresh_token
                    MdocAppHelper.getInstance()
                            .refreshTokenNoHeader =
                            response.body()!!.refresh_token
                    if(MdocAppHelper.getInstance().secretKey == null && MdocConstants.KEY_MISSING){
                        MdocConstants.KEY_MISSING = false
                        fetchSecretKey()
                    }else {
                        loginWithSession()
                    }
                } else {
                    Timber.w("loginWithKeycloackCode ${response.getErrorDetails()}")
                    MdocUtil.showToastLong(
                            context,
                            context.resources.getString(R.string.login_failed)
                    )
                    progressHandler.hideProgress()
                }
            }

            override fun onFailure(call: Call<KeycloackLoginResponse>, t: Throwable) {
                progressHandler.hideProgress()
                Timber.w(t, "loginWithKeycloackCode")
                MdocUtil.showToastLong(context, "Error " + t.message)
            }
        })
    }


    public fun fetchSecretKey() {

        var deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL


        MdocManager.fetchSecret(deviceName, object : Callback<SecretModel> {
            override fun onResponse(call: Call<SecretModel>, response: Response<SecretModel>) {
                if (response.isSuccessful) {
                    val secret = response?.body()?.data?.key.toString()
                    MdocAppHelper.getInstance().secretKey = secret
                    loginWithSession()
                }else{
                    progressHandler.hideProgress()
                    if(response?.code()==404){
                        loginCallback.reloadInitialPage()
                        loginCallback.onMissingSecret()
                    }
                }
            }

            override fun onFailure(call: Call<SecretModel>, t: Throwable) {

            }
        })
    }

    fun loginWithSession() {
        progressHandler.showProgress()


            MdocManager.loginWithSession(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.isSuccessful) {

                        progressHandler.hideProgress()
                        response.body()
                                ?.data?.publicUserDetails?.lastSeenUnitByCode?.let {
                                    AppPersistence.lastSeenUnitByCode = it
                                }
                        MdocAppHelper.getInstance().emergencyInfo =
                                response.body()?.data?.extendedUserDetails?.emergencyInfo
                        MdocAppHelper.getInstance()
                                .publicUserDetailses =
                                response.body()
                                        ?.data?.publicUserDetails

                        MdocAppHelper.getInstance()
                                .userLastName =
                                response.body()
                                        ?.data?.publicUserDetails?.lastName
                        MdocAppHelper.getInstance()
                                .userFirstName =
                                response.body()
                                        ?.data?.publicUserDetails?.firstName
                        MdocAppHelper.getInstance()
                                .username =
                                response.body()
                                        ?.data?.publicUserDetails?.username
                        MdocAppHelper.getInstance()
                                .paperPrintInfo =
                                response.body()
                                        ?.data?.publicUserDetails?.printPatientPlan
                        MdocAppHelper.getInstance()
                                .userType = response.body()
                                ?.data?.userType
                        MdocAppHelper.getInstance()
                                .userId = response.body()
                                ?.data?.userId

                        MdocAppHelper.getInstance()
                                .clinicId =
                                response.body()
                                        ?.data?.accesses?.cases?.getOrNull(0)
                                        ?.clinicId
                        MdocAppHelper.getInstance()
                                .email = response.body()
                                ?.data?.publicUserDetails?.email
                        updateFirebaseToken()

                        response.body()
                                ?.data?.accesses?.tenantPublicDocument?.fileConfig?.isPatientFileUploadAllowed?.let {
                                    MdocAppHelper.getInstance()
                                            .setPatientFileUploadAllowed(it)
                                }
                        //privacy and policy
                        // here we must delete boolean after privacy is implemented on backend!!
                        if (context.resources.getBoolean(R.bool.privacy_and_terms)) {
                            if (response.body()?.data?.publicUserDetails?.isPrivacyAccepted == false) {
                                loginCallback.openPrivacyPolicy()
                            } else if (response.body()?.data?.publicUserDetails?.isTermsAccepted == false) {
                                loginCallback.openTermsAndConditions()
                            } else if (response.body()?.data?.publicUserDetails?.email.isNullOrEmpty()) {
                                if (response.body()?.data?.publicUserDetails?.emailRequestShown == null || response.body()?.data?.publicUserDetails?.emailRequestShown == false) {
                                    showEmailDialog(response)
                                } else {
                                    setLoginData(response)
                                }
                            } else {
                                setLoginData(response)
                            }
                        } else {
                            setLoginData(response)
                        }
                    } else {
                        Timber.w("loginWithSession ${response.getErrorDetails()}")
                        MdocAppHelper.getInstance().isSelfRegister = true;
                        loginCallback.openClinics()
                        progressHandler.hideProgress()
                    }

                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    progressHandler.hideProgress()
                    Timber.w(t, "loginWithSession")
                }
            })
    }

    private fun updateFirebaseToken() {
        //register firebase token on our server
        MdocManager.updateFirebaseToken(object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                MdocUtil.showToastShort(context, t.message)
            }
        })
    }

    private fun setLoginData(response: Response<LoginResponse>) {
        if (response.body()?.data?.accesses?.cases?.size!! > 0) {
            MdocAppHelper.getInstance()
                .caseId =
                    response.body()
                        ?.data?.accesses?.cases?.get(0)
                        ?.caseId
            MdocAppHelper.getInstance()
                .setIsPremiumPatient(
                        response.body()?.data?.accesses?.cases?.get(0)?.isPremium ?: false
                )

            MdocAppHelper.getInstance()
                .department =
                    response.body()
                        ?.data?.accesses?.cases?.get(0)
                        ?.department
            MdocAppHelper.getInstance()
                .externalCaseId =
                    response.body()
                        ?.data?.accesses?.cases?.get(0)
                        ?.externalCaseId

        if(context.resources.getBoolean(R.bool.has_multi_case) && response.body()?.data?.accesses?.cases?.size!! > 1)
            loginCallback.openNextActivity(response.body()?.data?.accesses?.cases)
        else
            loginCallback.openNextActivity(null)
        }else{
            loginCallback.openNextActivity(null)
        }
    }

    private fun showEmailDialog(resp: Response<LoginResponse>) {
        userId = resp.body()?.data?.userId ?: ""
        enterEmailDialog = EnterEmailDialogFragment(dialogCallback = this, resp = resp)
        enterEmailDialog?.showNow((context as LoginActivity).supportFragmentManager, "")
    }

    private fun saveEmailApiCall(email: String, resp: Response<LoginResponse>, dialog: Dialog?) {
        MdocManager.saveEmail(SaveEmailRequest(email, userId), object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    dialog?.dismiss()
                    SaveMailDialogFragment(dialogCallback = this@LoginFlow, email = email, resp = resp).showNow(
                            (context as LoginActivity).supportFragmentManager, "")
                } else {
                    enterEmailDialog?.setErrorMessageForEmailInUse()
                }
                enterEmailDialog?.enableButtons()
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                dialog?.dismiss()
                setLoginData(resp)
                enterEmailDialog?.enableButtons()
            }
        })
    }

    private fun skipEmailApiCall(userId: String, resp: Response<LoginResponse>, dialog: Dialog?) {
        MdocManager.skipEmail(userId, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                dialog?.dismiss()
                setLoginData(resp)
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                dialog?.dismiss()
                setLoginData(resp)
            }
        })
    }

    override fun saveMail(mail: String?, resp: Response<LoginResponse>, dialog: Dialog?) {
        if (mail != null) {
            saveEmailApiCall(mail, resp, dialog)
        }
        else {
            progressHandler.showProgress()
            skipEmailApiCall(userId, resp, dialog)
        }
    }
}
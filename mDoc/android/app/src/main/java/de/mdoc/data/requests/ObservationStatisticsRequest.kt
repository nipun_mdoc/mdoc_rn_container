package de.mdoc.data.requests

data class ObservationStatisticsRequest(
        var fhirDeviceId: String?=null,
        var metricsCode: ArrayList<String>?=null,
        var from: Long?=null,
        var to: Long?=null,
        var periodType: String?=null,
        var lastSeenUnitByCode: Map<String, String>?=null)

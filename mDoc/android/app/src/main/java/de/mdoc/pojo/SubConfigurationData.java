package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Pramod on 12/31/20.
 */

public class SubConfigurationData {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isEnabled")
    @Expose
    private boolean isEnabled;
    @SerializedName("value")
    @Expose
    private int value;

    public SubConfigurationData(String id, boolean isEnabled) {
        this.id = id;
        this.isEnabled = isEnabled;
    }

    public void setId(String widget){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}

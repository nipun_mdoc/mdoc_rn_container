package de.mdoc.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.activities.CaseActivity;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.KeycloackLoginRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.KeycloackLoginResponse;
import de.mdoc.network.response.LoginResponse;
import de.mdoc.network.response.MdocResponse;
import de.mdoc.network.response.ServerTimeResponse;
import de.mdoc.pojo.Child;
import de.mdoc.pojo.PrivacyPolicyResponse;
import de.mdoc.pojo.PublicUserDetails;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import de.mdoc.util.token_otp.Token;
import de.mdoc.util.token_otp.TokenCode;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by AdisMulabdic on 1/16/18.
 */

public class ChildListAdapter extends RecyclerView.Adapter<ChildListViewHolder> {
    public Context context;
    public ArrayList<Child> items;
    private ProgressDialog progressDialog;
    private MdocActivity activity;

    public ChildListAdapter(MdocActivity activity, ArrayList<Child> items) {
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ChildListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_list_item, parent, false);
        context = parent.getContext();

        return new ChildListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChildListViewHolder holder, int position) {
        final Child item = items.get(position);
        holder.updateUI(item);

        holder.childRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImpersonateDialog(item.getPublicUserDetails());
            }
        });
    }

    private void openImpersonateDialog(final PublicUserDetails userDetails) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.impersonate_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        Button btnConfirm = view.findViewById(R.id.btnImpersonate);
        Button btnCancel = view.findViewById(R.id.btnCancelImpersonate);
        TextView impersonateTitleTv = view.findViewById(R.id.impersonateTitleTv);
        TextView fullNameTv = view.findViewById(R.id.fullNameTv);
        TextView patientIdTv = view.findViewById(R.id.patientIdTv);

        String fullName = MdocUtil.capitalizeFirstLetter(userDetails.getFirstName()) + " " + MdocUtil.capitalizeFirstLetter(userDetails.getLastName());
        fullNameTv.setText(fullName);
        patientIdTv.setText(userDetails.getUsername());
//        impersonateTitleTv.setText(context.getResources().getString(R.string.impersontate_title) + " " + userDetails.getFirstName());
        final EditText parentNameEd = view.findViewById(R.id.parentNameEd);
        parentNameEd.setText(MdocAppHelper.getInstance().getUsername());
        final EditText passwordParentEt = view.findViewById(R.id.passwordParentEt);
        final TextView showHidePass = view.findViewById(R.id.showHidePassTv);

        showHidePass.setOnClickListener(view14 -> {
            if(showHidePass.getTag().toString().equals("1")){
                showHidePass.setText(context.getString(R.string.hide));
                showHidePass.setTag("0");
                passwordParentEt.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }else{
                showHidePass.setText(context.getString(R.string.show));
                showHidePass.setTag("1");
                passwordParentEt.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
            passwordParentEt.setSelection(passwordParentEt.getText().toString().length());
        });

        btnConfirm.setOnClickListener(v -> {
            if (passwordParentEt.getText().length() == 0) {
                MdocUtil.showToastLong(activity, activity.getResources().getString(R.string.password_required));
            } else {

                MdocManager.getInstance().getServerTime(new Callback<ServerTimeResponse>() {

                    @Override
                    public void onResponse(Call<ServerTimeResponse> call, Response<ServerTimeResponse> response) {
                        if (response.isSuccessful()) {
                            keycloackLogin(userDetails, dialog, passwordParentEt, response.body().getData());

                        } else {
                            keycloackLogin(userDetails, dialog, passwordParentEt, System.currentTimeMillis());

                        }
                    }

                    @Override
                    public void onFailure(Call<ServerTimeResponse> call, Throwable t) {
                        keycloackLogin(userDetails, dialog, passwordParentEt, System.currentTimeMillis());

                    }
                });
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    private void keycloackLogin(PublicUserDetails userDetails, AlertDialog dialog, EditText passwordParentEt, long time) {
        progressDialog.show();
        String otp = "";
        if (MdocAppHelper.getInstance()
                .hasSecretKey()) {
            try {
                Token token = new Token(MdocAppHelper.getInstance()
                        .getSecretKey());
                TokenCode code = token.generateCodes(time);
                otp = code.getCurrentCode(time);
            } catch (Token.TokenUriInvalidException e) {
                e.printStackTrace();
            }
        }

        String deviceName = android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
        KeycloackLoginRequest request = new KeycloackLoginRequest();
        request.setUsername(MdocAppHelper.getInstance().getUsername());
        request.setPassword(passwordParentEt.getText().toString());
        request.setClient_id(context.getString(R.string.keycloack_clinic_Id));
        request.setTotp(otp);
        request.setDeviceName(deviceName);
        request.setRequest_subject(userDetails.getUsername());
        request.setClient_secret(context.getString(R.string.client_secret));
        request.setGrant_type("password");

        MdocManager.loginWithChildAccount(request, new Callback<KeycloackLoginResponse>() {
            @Override
            public void onResponse(Call<KeycloackLoginResponse> call, Response<KeycloackLoginResponse> response) {
                if (response.isSuccessful()) {
                    MdocManager.logout();
                    MdocAppHelper.getInstance().setChildLoged(true);
                    MdocAppHelper.getInstance().setAccessToken(response.body().getToken_type() + " " + response.body().getAccess_token());
                    MdocAppHelper.getInstance().setRefreshToken(response.body().getToken_type() + " " + response.body().getRefresh_token());
                    MdocAppHelper.getInstance().setRefreshTokenNoHeader(response.body().getRefresh_token());
                    loginWithSession();
                } else {
                    String errorMsg = MdocAppHelper.getInstance().getErrorMsg();
                    if (errorMsg != null && errorMsg.equals(MdocConstants.ERROR_MESSAGE)) {
                        progressDialog.dismiss();

                    } else {
                        progressDialog.dismiss();
                        Timber.w("loginWithChildAccount %s", APIErrorKt.getErrorDetails(response));
                        MdocUtil.showToastLong(activity, activity.getResources().getString(R.string.login_failed));
                    }
                }
            }

            @Override
            public void onFailure(Call<KeycloackLoginResponse> call, Throwable t) {
                Timber.w(t, "loginWithChildAccount");
                progressDialog.dismiss();
            }
        });
    }

    private void openPrivacyPolicyDialog(String htmlPrivacy) {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.privacy_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        final Button confirmPrivacy = view.findViewById(R.id.btnConfirmPrivacy);
        Button cancelPrivacy = view.findViewById(R.id.btnCancelPrivacy);
        final CheckBox agree = view.findViewById(R.id.checkPrivacy);
        WebView webView = view.findViewById(R.id.webView);
        confirmPrivacy.setEnabled(false);

        WebSettings webSettings = webView.getSettings();
        webSettings.setDefaultTextEncodingName("utf-8");
        webView.loadData(htmlPrivacy, "text/html; charset=utf-8", "utf-8");

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agree.isChecked()) {
                    confirmPrivacy.setAlpha(1);
                    confirmPrivacy.setEnabled(true);
                } else {
                    confirmPrivacy.setAlpha(0.7f);
                    confirmPrivacy.setEnabled(false);
                }
            }
        });

        confirmPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                MdocManager.confirmPrivacy(new Callback<MdocResponse>() {
                    @Override
                    public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                        if (response.isSuccessful()) {
                            loginWithSession();
                        } else {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<MdocResponse> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            }
        });

        cancelPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void getPrivacy() {
        MdocManager.getPrivacy(new Callback<PrivacyPolicyResponse>() {
            @Override
            public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                if (response.isSuccessful()) {
                    String htmlPrivacy = response.body().getData();
                    if (!htmlPrivacy.isEmpty())
                        openPrivacyPolicyDialog(htmlPrivacy);
                } else {
                    Timber.w("getPrivacy %s", APIErrorKt.getErrorDetails(response));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {
                Timber.w(t, "getPrivacy");
                progressDialog.dismiss();
            }
        });
    }

    private void loginWithSession(){
        MdocManager.loginWithSession(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if(response.isSuccessful()){
                    MdocAppHelper.getInstance().setPublicUserDetailses(response.body().getData().getPublicUserDetails());
                    MdocAppHelper.getInstance().setUserLastName(response.body().getData().getPublicUserDetails().getLastName());
                    MdocAppHelper.getInstance().setUserFirstName(response.body().getData().getPublicUserDetails().getFirstName());
                    MdocAppHelper.getInstance().setUsername(response.body().getData().getPublicUserDetails().getUsername());
                    MdocAppHelper.getInstance().setUserType(response.body().getData().getUserType());
                    MdocAppHelper.getInstance().setUserId(response.body().getData().getUserId());

                    if(context.getResources().getBoolean(R.bool.privacy_and_terms)){
                        if (!response.body().getData().getPublicUserDetails().isPrivacyAccepted()) {
                            getPrivacy();
                        }
                        else if(!response.body().getData().getPublicUserDetails().isTermsAccepted()){
                            getTerms();
                        }
                        else {
                            setLoginData(response);
                        }
                    }else{
                        setLoginData(response);
                    }
                }else{
                    Timber.w("loginWithSession %s", APIErrorKt.getErrorDetails(response));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Timber.w(t, "loginWithSession");
                progressDialog.dismiss();
            }
        });
    }

    private void setLoginData(Response<LoginResponse> response) {
        if (response.body().getData().getAccesses().getCases().size() == 1) {
            MdocAppHelper.getInstance().setCaseId(response.body().getData().getAccesses().getCases().get(0).getCaseId());
            MdocAppHelper.getInstance().setExternalCaseId(response.body().getData().getAccesses().getCases().get(0).getExternalCaseId());
            MdocAppHelper.getInstance().setIsPremiumPatient(response.body().getData().getAccesses().getCases().get(0).isPremium());
            MdocAppHelper.getInstance().setClinicId(response.body().getData().getAccesses().getCases().get(0).getClinicId());
            MdocAppHelper.getInstance().setDepartment(response.body().getData().getAccesses().getCases().get(0).getDepartment());
            MdocAppHelper.getInstance().setExternalCaseId(response.body().getData().getAccesses().getCases().get(0).getExternalCaseId());
            progressDialog.dismiss();
            navigateToDashboard();
        } else {
            Bundle b = new Bundle();
            b.putSerializable(MdocConstants.CASES, response.body().getData().getAccesses().getCases());
            progressDialog.dismiss();
//            navigateToCaseFragment(b);
            Intent intent = new Intent(activity, CaseActivity.class);
            intent.putExtra(MdocConstants.CASES, response.body().getData().getAccesses().getCases());
            activity.startActivity(intent);
            activity.finish();
        }
    }

    private void getTerms() {
        MdocManager.getTermsAndConditions(new Callback<PrivacyPolicyResponse>() {
            @Override
            public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                if (response.isSuccessful()) {
                    String htmlPrivacy = response.body().getData();
                    if (!htmlPrivacy.isEmpty())
                        openTermsDialog(htmlPrivacy);
                } else {
                    Timber.w("getTermsAndConditions %s", APIErrorKt.getErrorDetails(response));
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {
                Timber.w(t, "getTermsAndConditions");
                progressDialog.dismiss();
            }
        });
    }

    private void openTermsDialog(String htmlPrivacy) {
        progressDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        assert inflater != null;
        View view = inflater.inflate(R.layout.privacy_layout, null);
        builder.setView(view);
        final AlertDialog dialog = builder.show();
        final Button confirmPrivacy = view.findViewById(R.id.btnConfirmPrivacy);
        Button cancelPrivacy = view.findViewById(R.id.btnCancelPrivacy);
        final CheckBox agree = view.findViewById(R.id.checkPrivacy);
        WebView webView = view.findViewById(R.id.webView);
        confirmPrivacy.setEnabled(false);

        WebSettings webSettings = webView.getSettings();
        webSettings.setDefaultTextEncodingName("utf-8");
        webView.loadData(htmlPrivacy, "text/html; charset=utf-8", "utf-8");

        agree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (agree.isChecked()) {
                    confirmPrivacy.setAlpha(1);
                    confirmPrivacy.setEnabled(true);
                } else {
                    confirmPrivacy.setAlpha(0.7f);
                    confirmPrivacy.setEnabled(false);
                }
            }
        });

        confirmPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                MdocManager.confirmTermAndCondition(new Callback<MdocResponse>() {
                    @Override
                    public void onResponse(Call<MdocResponse> call, Response<MdocResponse> response) {
                        if (response.isSuccessful()) {
                            loginWithSession();
                        } else {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<MdocResponse> call, Throwable t) {
                        progressDialog.dismiss();
                    }
                });
            }
        });

        cancelPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    private void navigateToCaseFragment(Bundle b) {
        Navigation.findNavController(activity, R.id.nav_host_fragment_container).navigate(R.id.action_to_fragmentCase, b);
    }

    private void navigateToDashboard() {
        Intent intent = new Intent(activity, MdocActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(intent);
        activity.finish();
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

class ChildListViewHolder extends RecyclerView.ViewHolder {

    private TextView childName;
    public RelativeLayout childRl;

    ChildListViewHolder(View itemView) {
        super(itemView);
        this.childName = itemView.findViewById(R.id.childNameTv);
        this.childRl = itemView.findViewById(R.id.childRl);
    }

    void updateUI(Child child) {
        childName.setText(child.getPublicUserDetails().getFirstName() + " " + child.getPublicUserDetails().getLastName());
    }
}
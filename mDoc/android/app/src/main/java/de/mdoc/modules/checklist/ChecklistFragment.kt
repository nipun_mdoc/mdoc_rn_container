package de.mdoc.modules.checklist


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentChecklistBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.interfaces.ItemTouchHelperViewHolder
import de.mdoc.modules.checklist.adapters.ChecklistAdapter
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.network.RestClient
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_checklist.*
import java.util.*


class ChecklistFragment : NewBaseFragment(),CommonDialogFragment.OnButtonClickListener,CommonDialogFragment.CancelListener{

    private var adapter: ChecklistAdapter? = null

    override val navigationItem: NavigationItem = NavigationItem.Checklist

    private val checklistViewModel by viewModel {
        ChecklistViewModel(
                RestClient.getService()
        )
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentChecklistBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_checklist, container, false)
        binding.lifecycleOwner = this
        binding.checklistViewModel = checklistViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        shouldShowDisclaimer()

        checklistViewModel.getQuestionnaires()
        checklistViewModel.items.observe(viewLifecycleOwner, Observer {
            if(it.size > 0) {
                checkListRv?.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                adapter = ChecklistAdapter(context, it,findNavController())
                checkListRv?.adapter = adapter
            }
        })

        val ith = ItemTouchHelper(_ithCallback)
        ith.attachToRecyclerView(checkListRv)
    }


    internal var _ithCallback: ItemTouchHelper.Callback = object : ItemTouchHelper.Callback() {
        //and in your imlpementaion of
        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            // get the viewHolder's and target's positions in your adapter data, swap them
            Collections.swap(adapter!!.items, viewHolder.adapterPosition, target.adapterPosition)
            // and notify the adapter that its dataset has changed
            adapter!!.notifyItemMoved(viewHolder.adapterPosition, target.adapterPosition)
            val ids = ArrayList<String>()
            for (i in adapter!!.items) {
                ids.add(i.pollId)
            }
            MdocAppHelper.getInstance().checklistOrder = Gson().toJson(ids)
            return true
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        }

        //defines the enabled move directions in each state (idle, swiping, dragging).
        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
            return ItemTouchHelper.Callback.makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
                    ItemTouchHelper.DOWN or ItemTouchHelper.UP or ItemTouchHelper.START or ItemTouchHelper.END)
        }

        override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?,
                                       actionState: Int) {
            // We only want the active item
            if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
                if (viewHolder is ItemTouchHelperViewHolder) {
                    val itemViewHolder = viewHolder as ItemTouchHelperViewHolder?
                    itemViewHolder!!.onItemSelected()
                }
            }

            super.onSelectedChanged(viewHolder, actionState)
        }

        override fun clearView(recyclerView: RecyclerView,
                               viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder)

            if (viewHolder is ItemTouchHelperViewHolder) {
                val itemViewHolder = viewHolder as ItemTouchHelperViewHolder
                itemViewHolder.onItemClear()
            }
        }
    }

    private fun shouldShowDisclaimer() {
        val moduleName = resources.getString(R.string.checklist)
        val list = resources.getStringArray(R.array.disclaimer_modules)
        if (list.contains(moduleName)&& (MdocAppHelper.getInstance().disclaimerSeenChecklist == false)) {
            showDisclaimerDialog()
        }
    }

    private fun showDisclaimerDialog() {
        CommonDialogFragment.Builder()
            .title(resources.getString(R.string.disclaimer))
            .description(resources.getString(R.string.disclaimer_body))
            .setOnCancelListener(this)
            .setOnClickListener(this)
            .setPositiveButton(resources.getString(R.string.disclaimer_accept))
            .navigateBackOnCancel(true)
            .build()
            .showNow(childFragmentManager, "")
    }

    companion object {

        var POLL_TYPE = "CHECKLISTE"
    }

    override fun onCancelDialog() {
        findNavController().popBackStack()
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        if (button == ButtonClicked.POSITIVE) {
            MdocAppHelper.getInstance().disclaimerSeenChecklist = true
        }
    }
}

package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Measure(
        val active: Boolean?=null,
        val code: String?=null,
        val system: String?=null
): Parcelable
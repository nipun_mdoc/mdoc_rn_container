package de.mdoc.pojo;


import java.util.HashMap;

/**
 * Created by ema on 10/30/17.
 */

public class KioskConfigurationData {

    private String id;
    private String clinic;
    private String configurationType;
    private String applicationEvent;
    private HashMap<String, String> configurationDetails;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public String getConfigurationType() {
        return configurationType;
    }

    public void setConfigurationType(String configurationType) {
        this.configurationType = configurationType;
    }

    public String getApplicationEvent() {
        return applicationEvent;
    }

    public void setApplicationEvent(String applicationEvent) {
        this.applicationEvent = applicationEvent;
    }

    public HashMap<String, String> getConfigurationDetails() {
        return configurationDetails;
    }

    public void setConfigurationDetails(HashMap<String, String> configurationDetails) {
        this.configurationDetails = configurationDetails;
    }
}

package de.mdoc.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.activities.LoginActivity;
import de.mdoc.activities.MdocActivity;

/**
 * Created by ema on 4/6/17.
 */

public abstract class MdocBaseFragment extends Fragment {

    abstract protected int setResourceId();

    abstract protected void init(@Nullable Bundle savedInstanceState);

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(setResourceId(), container, false);
        ButterKnife.bind(this, view);
        init(savedInstanceState);
        return view;
    }

    protected LoginActivity getNewLoginActivity() {
        return (LoginActivity) getActivity();
    }

    protected MdocActivity getMdocActivity() {
        return (MdocActivity) getActivity();
    }

    protected void removeError(EditText edt, TextView errorTv) {
        edt.setBackground(getResources().getDrawable(R.drawable.login_edt_background));
        if (errorTv != null) {
            errorTv.setVisibility(View.INVISIBLE);
        } else {
            edt.setTextColor(getResources().getColor(R.color.text_color));
        }
    }

    protected void setError(EditText edt, TextView errorTv, String message) {
        edt.setBackground(getResources().getDrawable(R.drawable.lust_rectangle));
        if (errorTv != null) {
            errorTv.setVisibility(View.VISIBLE);
            errorTv.setText(message);
        } else {
            edt.setTextColor(getResources().getColor(R.color.lust));
        }
    }
}

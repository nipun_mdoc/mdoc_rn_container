package de.mdoc.modules.mental_goals.detail

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.export.MentalGoalExportFragment
import de.mdoc.modules.mental_goals.mental_goal_adapters.DashboardActionItemsAdapter
import de.mdoc.modules.mental_goals.mental_goal_adapters.DashboardWhenThenAdapter
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.bindText
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_create_goal.*

class GoalDetailFragment: MdocFragment() {

    val viewModel by viewModel {
        GoalDetailsViewModel(
                repository = GoalDetailsRepository()
                            )
    }
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_create_goal
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()
        setHasOptionsMenu(true)

        arrayOf(txt_top_title, txt_summary, ll_toolbar, txt_back).forEach {
            it.visibility = View.GONE
        }
        txt_created_by.visibility = View.VISIBLE
        rv_action_items_dashboard?.layoutManager = LinearLayoutManager(context)
        ViewCompat.setNestedScrollingEnabled(rv_action_items_dashboard, false)
        rv_when_then_dashboard?.layoutManager = LinearLayoutManager(context)
        ViewCompat.setNestedScrollingEnabled(rv_when_then_dashboard, false)
//                bindVisibleGone(btnMentalGoalsEdit, viewModel.isEditButtonVisible)
        bindVisibleGone(btn_goal_status_change, viewModel.isChangeStatusButtonVisible)
        bindText(txt_goal_title, viewModel.title)
        bindText(txt_goal_description, viewModel.description)
        bindText(txt_date_placeholder, viewModel.achievementPeriod)

        txt_created_by.text = if (MentalGoalsUtil.coachCreated) {
            getString(R.string.created_by_coach)
        }
        else {
            getString(R.string.created_by_you)
        }

        viewModel.actionItems.observe(this) {
            rv_action_items_dashboard.adapter = DashboardActionItemsAdapter(view.context, it)
        }
        viewModel.ifThenPlans.observe(this) {
            rv_when_then_dashboard.adapter = DashboardWhenThenAdapter(view.context, it)
        }
    }

    private fun setupListeners() {
        btn_goal_status_change.setOnClickListener {
            val action = GoalDetailFragmentDirections.actionGoalDetailFragmentToChangeGoalStatusFragment()
            findNavController().navigate(action)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_mental_goal_details)
        val item = menu.findItem(R.id.edit)
        item.isVisible = viewModel.isEditButtonVisible
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.export) {
            MentalGoalExportFragment().also {
                it.goalId = MentalGoalsUtil.goalId
            }
                .show(parentFragmentManager, MentalGoalExportFragment::class.simpleName)
        }
        else if (item.itemId == R.id.edit) {
            val action = GoalDetailFragmentDirections.actionGoalDetailFragmentToEditGoalFragment()
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }
}
package de.mdoc.modules.questionnaire;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.interfaces.IPostAnswerForMatrix;
import de.mdoc.modules.diary.data.PollReassignResponse;
import de.mdoc.modules.questionnaire.data.QuestionnaireAdditionalFields;
import de.mdoc.modules.questionnaire.ext.ArrayListExtendedQuestionExtKt;
import de.mdoc.modules.questionnaire.ext.ExtendedQuestionExtKt;
import de.mdoc.modules.questionnaire.questions.QuestionType;
import de.mdoc.modules.questionnaire.questions.QuestionTypeFactoryKt;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.AnswerDataResponse;
import de.mdoc.network.response.QuestionnaireDetailsResponse;
import de.mdoc.network.response.SelfAssignResponse;
import de.mdoc.network.response.UserAnswersResponse;
import de.mdoc.pojo.AnswearDetails;
import de.mdoc.pojo.Answer;
import de.mdoc.pojo.DiaryList;
import de.mdoc.pojo.ExtendedQuestion;
import de.mdoc.pojo.Question;
import de.mdoc.pojo.QuestionnaireDetailsData;
import de.mdoc.pojo.Section;
import de.mdoc.pojo.UserAnswerData;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.ProgressDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 12/28/16.
 */

public class QuestionFragment extends MdocFragment implements IPostAnswerForMatrix {


    private static String SECTION_INDEX = "section_index";
    private static String QUESTION_INDEX = "queston_index";
    private static String INDEX = "index";
    private static String IS_NEW_ANSWER = "is_new_answer";
    private static String SELECTED = "selected";
    private static String POLL_ID = "poll_id";
    private static String POLL_ASSIGN_ID = "poll_assign_id";
    private static String POLL_VOTING_ID = "poll_voting_id";
    private static String QUESTIONS_THAT_ARE_PASS = "question_that_are_pass";
    private static String USER_ANSWERS = "user_answers";
    private static String SECTIONS = "sections";
    private static String ADDED_SUBQUESTIONS = "added_subquestions";
    private static String ANSWERS = "answers";
    private static String NEXT_BTN = "next_btn";
    private static String PREVIOUS_BTN = "previous_btn";
    private static String MATRIX_QUESTIONS = "matrix_questions";

    private int sectionIndex = 0;
    private int questionIndex = 0;
    private int index = 0;
    private boolean isGoToShownAlready = false;

    @BindView(R.id.questionRl)
    LinearLayout questionRl;
    @BindView(R.id.sectionTv)
    TextView sectionTv;
    @BindView(R.id.nextBtn)
    Button nextBtn;
    @BindView(R.id.previousBtn)
    Button previousBtn;

    @BindView(R.id.progressSb)
    ProgressBar progressSb;
    @BindView(R.id.progressTv)
    TextView progressTv;

    @BindView(R.id.progressLl)
    View progressLayout;

    MdocActivity activity;
    ColorStateList colors;
    ExtendedQuestion selected;
    QuestionType selectedQuestionType;
    private ProgressDialog progressDialog;
    private String pollId;
    private String selectedLanguage;
    private String pollAssignId;
    private DiaryList diaryList;

    private String pollVotingId;
    private QuestionnaireDetailsData questionnaireDetailsData;
    private ArrayList<Section> sections;
    private ArrayList<Question> questions = new ArrayList<Question>();
    private ArrayList<Question> parentQuestions = new ArrayList<Question>();
    private ArrayList<ExtendedQuestion> questionsThatArePass = new ArrayList<ExtendedQuestion>();
    private ArrayList<ExtendedQuestion> addedSubquestions = new ArrayList<ExtendedQuestion>();
    private AnswearDetails answearDetails;
    private String pollEndMessage;
    private String diaryConfigId = "";
    private ArrayList<Answer> userAnswers;
    private Boolean showFreeText = false, autoShare = false, allowSharing = false, isDiary = false, shouldAssignQuestionnaire = false;
    private ArrayList<Question> matrixQuestions = new ArrayList<>();


    @Override
    protected int setResourceId() {
        return R.layout.fragment_questionnaire_question;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.QuestionnaireProgress;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        colors = ContextCompat.getColorStateList(activity, R.color.checkbox_text);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);

        getDataFromPreviousFragment();


        progressSb.setOnTouchListener((v, event) -> true);

        if (!getResources().getBoolean(R.bool.phone)) {
            if (savedInstanceState != null) {
                sections.addAll((ArrayList<Section>) savedInstanceState.getSerializable(SECTIONS));
                selected = (ExtendedQuestion) savedInstanceState.getSerializable(SELECTED);
                sectionIndex = savedInstanceState.getInt(SECTION_INDEX);
                index = savedInstanceState.getInt(INDEX);
                questionIndex = savedInstanceState.getInt(QUESTION_INDEX);
                userAnswers.addAll((ArrayList<Answer>) savedInstanceState.getSerializable(USER_ANSWERS));
                selectedQuestionType.setNewAnswer(savedInstanceState.getBoolean(IS_NEW_ANSWER));
                pollId = savedInstanceState.getString(POLL_ID);
                pollAssignId = savedInstanceState.getString(POLL_ASSIGN_ID);
                pollVotingId = savedInstanceState.getString(POLL_VOTING_ID);
                selectedQuestionType = QuestionTypeFactoryKt.createQuestion(selected, this::postAnswerForMatrix);
                addedSubquestions.addAll((ArrayList<ExtendedQuestion>) savedInstanceState.getSerializable(ADDED_SUBQUESTIONS));
                questionsThatArePass.addAll((ArrayList<ExtendedQuestion>) savedInstanceState.getSerializable(QUESTIONS_THAT_ARE_PASS));
                selectedQuestionType.getAnswers().addAll((ArrayList<String>) savedInstanceState.getSerializable(ANSWERS));
                matrixQuestions.addAll((ArrayList<Question>) savedInstanceState.getSerializable(MATRIX_QUESTIONS));
                String answer = null;
                if (selected.getPollQuestionType().equals(MdocConstants.CHECKBOX)) {
                    answer = new Gson().toJson(selectedQuestionType.getAnswers());
                } else {
                    answer = answer != null && answer.length() > 0 ? selectedQuestionType.getAnswers().get(0) : "";
                }
                selected.getAnswer().setAnswerData(answer);

                previousBtn.setVisibility(savedInstanceState.getInt(PREVIOUS_BTN) == View.VISIBLE ? View.VISIBLE : View.INVISIBLE);
                nextBtn.setVisibility(savedInstanceState.getInt(NEXT_BTN) == View.VISIBLE ? View.VISIBLE : View.INVISIBLE);

                fillQuestionList();
                setQuestionUi(index);
            } else {
                previousBtn.setVisibility(View.INVISIBLE);
                if (shouldAssignQuestionnaire) {
                    assignQuestionnaire();
                } else {
                    getQuestionnaireData(pollAssignId);
                }
            }
        } else {
            previousBtn.setVisibility(View.INVISIBLE);
            if (shouldAssignQuestionnaire) {
                assignQuestionnaire();
            } else {
                getQuestionnaireData(pollAssignId);
            }
        }
    }

    private void groupMatrixQuestions(QuestionnaireDetailsData detailsData) {
        for (Section section : detailsData.getSections()) {
            for (Question question : section.getQuestions()) {
                if (question.getPollQuestionType().equals(MdocConstants.RADIO_MATRIX)) {
                    matrixQuestions.add(question);
                }
            }
        }
        for (Question matrixQuestion : matrixQuestions) {
            ArrayList<Question> matrixChildQuestions = new ArrayList<>();
            for (Section section : detailsData.getSections()) {
                for (Question question : section.getQuestions()) {
                    if (question.getPollQuestionType().equals(MdocConstants.RADIO)) {
                        if (question.getParentQuestionId() != null && question.getParentQuestionId().equals(matrixQuestion.getUuid())) {
                            matrixChildQuestions.add(question);
                        }
                    }
                }
            }
            for (int i = 0; i < matrixChildQuestions.size(); i++) {
                Answer answer = MdocUtil.getAnswerByQuestionId(userAnswers, matrixChildQuestions.get(i).getId());
                matrixQuestion.getQuestionData().getMatrixQuestions().set(i, new ExtendedQuestion(matrixChildQuestions.get(i), answer, this.selectedLanguage));
            }
        }
    }

    private void assignQuestionnaire() {
        MdocManager.assignQuestionnaire(pollId, new Callback<SelfAssignResponse>() {
            @Override
            public void onResponse(Call<SelfAssignResponse> call, Response<SelfAssignResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getData() != null) {
                        pollAssignId = response.body().getData().getId();
                    }
                    getQuestionnaireData(pollAssignId);
                }
            }

            @Override
            public void onFailure(Call<SelfAssignResponse> call, Throwable t) {

            }
        });
    }

    private void getDataFromPreviousFragment() {
        if (getArguments() != null) {
            pollId = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getPollId();
            pollAssignId = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getPollAssignId();
            isDiary = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().isDiary();
            shouldAssignQuestionnaire = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getShouldAssignQuestionnaire();
            showFreeText = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getShowFreeText();
            autoShare = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getAutoShare();
            allowSharing = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getAllowSharing();
            diaryConfigId = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getDiaryConfigId();
            selectedLanguage = QuestionFragmentArgs.fromBundle(getArguments()).getSelectedLanguage();
            pollVotingId = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getPollVotingId();
            diaryList = QuestionFragmentArgs.fromBundle(getArguments()).getQuestionnaireAdditionalFields().getDiaryList();
        }
    }

    private void addLastSeen(String pollAssignId, ExtendedQuestion question) {
        if (pollAssignId != null && question != null) {
            MdocManager.addLastSeenQuestionnaire(pollAssignId, question.getId(), new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Timber.d("Successful seen");
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Timber.d("Failed seen");
                }
            });
        }
    }

    private void reAssignQuestionnaire() {
        MdocManager.reAssignQuestionnaire(pollVotingId, new Callback<PollReassignResponse>() {
            @Override
            public void onResponse(Call<PollReassignResponse> call, Response<PollReassignResponse> response) {
                if (response.isSuccessful()) {
                    pollAssignId =  response.body().getData().getPollDetails().getPollAssignId();
                    getQuestionnaireData2(pollAssignId);
                }
            }

            @Override
            public void onFailure(Call<PollReassignResponse> call, Throwable t) {
            }
        });
    }

    private void getQuestionnaireData(String pollAssignId) {
        progressDialog.show();
        if(pollAssignId == null){
            reAssignQuestionnaire();
        }else{
            getQuestionnaireData2(pollAssignId);
        }
    }

    private void getQuestionnaireData2(String pollAssignId){
        MdocManager.getQuestionnaireById(pollAssignId, new Callback<QuestionnaireDetailsResponse>() {
            @Override
            public void onResponse(Call<QuestionnaireDetailsResponse> call, Response<QuestionnaireDetailsResponse> response) {
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        questionnaireDetailsData = response.body().getData();

                        if (questionnaireDetailsData != null) {
                            if(isDiary){
                                selectedLanguage = questionnaireDetailsData.getDefaultLanguage();
                            }
                            fillData(questionnaireDetailsData);
                        }

                        if (questionnaireDetailsData.getSections().size() > 0) {

                            sections = response.body().getData().getSections();
                            Collections.sort(sections, new SectionComparator());
                            getUserAnswers();
                        } else {
                            nextBtn.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        nextBtn.setVisibility(View.INVISIBLE);
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionnaireDetailsResponse> call, Throwable t) {
                if (isAdded()) {
                    progressDialog.dismiss();
                    nextBtn.setVisibility(View.INVISIBLE);
                }
            }
        });

    }

//    private void getQuestionnaireData(String pollAssignId) {
//
//        progressDialog.show();
//        MdocManager.getQuestionnaireById(pollAssignId, new Callback<QuestionnaireDetailsResponse>() {
//            @Override
//            public void onResponse(Call<QuestionnaireDetailsResponse> call, Response<QuestionnaireDetailsResponse> response) {
//                if (isAdded()) {
//                    if (response.isSuccessful()) {
//                        questionnaireDetailsData = response.body().getData();
//
//                        if (questionnaireDetailsData != null) {
//                            selectedLanguage = questionnaireDetailsData.getDefaultLanguage();
//                            fillData(questionnaireDetailsData);
//                        }
//
//                        if (questionnaireDetailsData.getSections().size() > 0) {
//
//                            sections = response.body().getData().getSections();
//                            Collections.sort(sections, new SectionComparator());
//                            getUserAnswers();
//                        } else {
//                            nextBtn.setVisibility(View.INVISIBLE);
//                        }
//                    } else {
//                        nextBtn.setVisibility(View.INVISIBLE);
//                        progressDialog.dismiss();
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<QuestionnaireDetailsResponse> call, Throwable t) {
//                if (isAdded()) {
//                    progressDialog.dismiss();
//                    nextBtn.setVisibility(View.INVISIBLE);
//                }
//            }
//        });
//    }

    private void fillData(QuestionnaireDetailsData questionnaire) {
        answearDetails = questionnaire.getAnswerDetails();

        progressTv.setText(MdocUtil.calculateProgress(answearDetails.getCompleted(), answearDetails.getTotal()) + "%");

        pollEndMessage = questionnaire.getClosingMessage();
        progressSb.setMax(answearDetails.getTotal());
        progressSb.setProgress(answearDetails.getCompleted());

        pollVotingId = questionnaire.getAnswerDetails().getPollVotingActionId();

    }

    private void getUserAnswers() {
        MdocManager.getUserAnswers(new Callback<UserAnswersResponse>() {
            @Override
            public void onResponse(Call<UserAnswersResponse> call, Response<UserAnswersResponse> response) {
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        ArrayList<UserAnswerData> data = response.body().getData();
                        if (data.size() >= 0) {
                            for (int i = 0; i < data.size(); i++) {
                                if (data.get(i).getPollId() != null && data.get(i).getPollId().equals(pollId) && data.get(i).getId() != null && data.get(i).getId().equals(pollVotingId)) {
                                    userAnswers = data.get(i).getAnswers();
                                }
                            }
                        }
                    }
                    groupMatrixQuestions(questionnaireDetailsData);
                    fillQuestionList();
                    progressDialog.dismiss();
                    showGoToAlert();
                }
            }

            @Override
            public void onFailure(Call<UserAnswersResponse> call, Throwable t) {
                if (isAdded()) {
                    fillQuestionList();
                    showGoToAlert();
                    progressDialog.dismiss();
                }
            }
        });
    }


    private void fillQuestionList() {

        for (int i = 0; i < sections.size(); i++) {
            //uzmi sekciju koja je po redu
            if (i == sectionIndex) {
                questions = sections.get(i).getQuestions();
                Collections.sort(questions, new QuestionComparator());
                //create list of parent questions
                parentQuestions.clear();
                for (Question q : questions) {
                    if (q.getParentQuestionId() == null) {
                        parentQuestions.add(q);
                    }
                }

                //prodji kroz pitanja u sekciji i postavi UI
                for (int j = 0; j < parentQuestions.size(); j++) {
                    if (j == questionIndex) {
                        questionIndex++;
                        questionsThatArePass.add(new ExtendedQuestion(sections.get(i), parentQuestions.get(j), MdocUtil.getAnswerByQuestionId(userAnswers, parentQuestions.get(j).getId()), this.selectedLanguage));
                    }
                }
                //Kad je zadnje pitanje u sekciji povecaj sekciju
                if (questionIndex == parentQuestions.size()) {
                    sectionIndex++;
                    questionIndex = 0;
                }
            }

            //when there is one question in questionnaire
            if (index == questionsThatArePass.size() - 1) {
                setFinishButton();
            } else {
                setNextButton();
            }
        }
    }

    private Boolean hasLastSeen() {
        return questionnaireDetailsData != null && questionnaireDetailsData.getLastSeenPollQuestionId() != null;
    }

    private Boolean hasAnyAnswers() {
        return userAnswers != null && userAnswers.size() > 0;
    }

    public void setQuestionUi(int index) {
        progressLayout.setVisibility(View.VISIBLE);
        this.index = index;
        selected = questionsThatArePass.get(index);
        selectedQuestionType = QuestionTypeFactoryKt.createQuestion(selected, this::postAnswerForMatrix);
        selectedQuestionType.fillQuestion(questionRl, selected);
        sectionTv.setText(selected.getSectionName());
        if (isFinish()) {
            setFinishButton();
        } else {
            setNextButton();
        }
        if (isStart()) {
            previousBtn.setVisibility(View.INVISIBLE);
        } else {
            previousBtn.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.nextBtn)
    public void onNextBtnClick() {
        if (isValidAnswer()) {

            try {

                if (questionRl.getChildCount() > 0) {
                    selectedQuestionType.onSaveQuestion(questionRl.getChildAt(0), selected);
                }

                questionRl.removeAllViews();
                if (selected != null && selected.getChildQuestionsIds() != null && selected.getChildQuestionsIds().size() > 0) {
                    for (int counter = selected.getChildQuestionsIds().size() - 1; counter >= 0; counter--) {
                        String id = selected.getChildQuestionsIds().get(counter);
                        for (Question q : selected.getSection().getQuestions()) {
                            if (q.getUuid().equals(id)) {
                                ExtendedQuestion eq = new ExtendedQuestion(selected.getSection(), q, MdocUtil.getAnswerByQuestionId(userAnswers, q.getId()), selectedLanguage);

                                if (checkCondition(selected.getPollQuestionType(), selectedQuestionType.getSelection(), q.parseShowIf())) {
                                    questionsThatArePass.remove(eq);
                                    questionsThatArePass.add(index + 1, eq);
                                    addedSubquestions.add(eq);
                                    if (index == questionsThatArePass.size() - 1) {
                                        setFinishButton();
                                    } else {
                                        setNextButton();
                                    }
                                    ///sort
                                    for (ExtendedQuestion extendedQuestion : questionsThatArePass) {
                                        extendedQuestion.setCompositeIndex(getComspoiteIndexString(extendedQuestion));
                                    }
                                    Collections.sort(questionsThatArePass, new SecQuestComparator());
                                } else {
                                    //remove question from questionThatArePass if existing

                                    if (questionsThatArePass.contains(eq)) {
                                        questionsThatArePass.remove(eq);
                                        addedSubquestions.remove(eq);
                                    }
                                }
                            }
                        }
                    }
                }


                if (selectedQuestionType.getAnswers().size() > 0 && !selected.getPollQuestionType().equals(MdocConstants.RADIO_MATRIX)) {
                    postAnswer(isFinish());
                } else {
                    if (isFinish()) {
                        goToSendQuestionnaireView();
                    }
                }

                if (!isFinish()) {
                    index++;
                    setQuestionUi(index);
                }

                Timber.d("Questionnaire si: %s qi: %s", sectionIndex, questionIndex);
            } catch (Exception e) {
                Timber.d("Questionnaire exception: %s", e.getMessage());
            }
        } else {
            Timber.d("dsa");
        }
    }

    private boolean isValidAnswer() {
        if (selectedQuestionType != null && selectedQuestionType.getValidator() != null) {
            return selectedQuestionType.getValidator().isValidAnswer();
        } else {
            return true;
        }
    }

    private boolean isStart() {
        return index == 0;
    }

    private boolean isFinish() {
        return index >= questionsThatArePass.size() - 1;
    }

    private boolean checkCondition(String poolType, ArrayList<String> answears, ArrayList<String> showIfs) {

        if (poolType.equals(MdocConstants.CHECKBOX)) {
            return answears.size() > 0 && contains(answears, showIfs);
        } else if (poolType.equals(MdocConstants.RADIO) || poolType.equals(MdocConstants.RATING) || poolType.equals(MdocConstants.DROPDOWN)) {
            return showIfs.size() > 0 && answears.size() > 0 && answears.get(0).equals(showIfs.get(0));
        }
        return false;
    }

    private boolean contains(ArrayList<String> answers, ArrayList<String> showIfs) {

        ArrayList<String> contains = new ArrayList<>();

        for (int i = 0; i < showIfs.size(); i++) {

            for (int j = 0; j < answers.size(); j++) {

                if (showIfs.get(i).equals(answers.get(j))) {
                    contains.add(answers.get(j));
                }
            }
        }

        return contains.size() == showIfs.size();

    }

    @OnClick(R.id.previousBtn)
    void onPreviousBtnClick() {

        questionRl.removeAllViews();

        if (addedSubquestions.size() > 0 && selected.getId().equals(addedSubquestions.get(addedSubquestions.size() - 1).getId())) {
            questionsThatArePass.remove(addedSubquestions.get(addedSubquestions.size() - 1));
            addedSubquestions.remove(addedSubquestions.size() - 1);
        }

        if (index >= 1) {
            index--;
            setQuestionUi(index);
        }
    }

    private void postAnswer(final boolean isFinish) {
        List<String> answers = selectedQuestionType.getAnswers();
        ArrayList<String> selections = selectedQuestionType.getSelection();

        String answer = null;
        if (selected.getPollQuestionType().equals(MdocConstants.CHECKBOX)) {
            answer = new Gson().toJson(answers);
        } else {
            answer = answers.get(0);
        }

        for (int i = 0; i < questionsThatArePass.size(); i++) {
            if (questionsThatArePass.get(i).getId().equals(selected.getId())) {
                questionsThatArePass.get(i).setAnswer(new Answer(answer, selections));
            }
        }

        if (selectedQuestionType.isNewAnswer()) {

            progressDialog.show();
            MdocManager.postAnswer(pollId, pollAssignId, selected.getId(), answer, pollVotingId == null ? "" : pollVotingId, selectedQuestionType.getSelection(), selectedLanguage, new Callback<AnswerDataResponse>() {
                @Override
                public void onResponse(Call<AnswerDataResponse> call, Response<AnswerDataResponse> response) {
                    if (isAdded()) {
                        if (response.isSuccessful()) {
                            progressSb.setProgress(response.body().getData().getCompleted());
                            progressTv.setText(MdocUtil.calculateProgress(response.body().getData().getCompleted(), response.body().getData().getTotal()) + "%");
                            answearDetails = response.body().getData();
                            pollVotingId = answearDetails.getPollVotingActionId();
                        } else {
                            Timber.w("postAnswer %s", APIErrorKt.getErrorDetails(response));
                        }
                        if (isFinish) {
                            goToSendQuestionnaireView();
                        } else {
                            progressDialog.dismiss();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AnswerDataResponse> call, Throwable t) {
                    Timber.w(t, "postAnswer");
                    if (isAdded()) {
                        progressDialog.dismiss();

                        if (isFinish) {
                            QuestionnaireAdditionalFields questionnaireAdditionalFields = new QuestionnaireAdditionalFields(
                                    pollId, pollAssignId, pollEndMessage, pollVotingId, showFreeText, allowSharing, autoShare,
                                    diaryConfigId, isDiary, answearDetails.getTotal(), answearDetails.getCompleted(),
                                    questionnaireDetailsData.getName(), questionnaireDetailsData.getPollType(),
                                    questionnaireDetailsData.getSendToKIS(), questionnaireDetailsData.getResultScreenTypeCode(), false,diaryList
                            );
                            if (isDiary) {
                                NavDirections actions = QuestionFragmentDirections.Companion.actionQuestionFragmentToDiaryEntryFragment(questionnaireAdditionalFields);
                                Navigation.findNavController(activity, R.id.navigation_host_fragment).navigate(actions);
                            } else {
                                NavDirections actions = QuestionFragmentDirections.Companion.actionQuestionFragmentToOutroQuestionnaireFragment(pollEndMessage);
                                Navigation.findNavController(activity, R.id.navigation_host_fragment).navigate(actions);
                            }
                        }
                    }
                }
            });
        } else {
            if (isFinish) {
                goToSendQuestionnaireView();
            }
        }
    }

    private void goToSendQuestionnaireView() {

        QuestionnaireAdditionalFields additionalFields = new QuestionnaireAdditionalFields(
                pollId, pollAssignId, pollEndMessage, pollVotingId, showFreeText, allowSharing, autoShare,
                diaryConfigId, isDiary, answearDetails.getTotal(), answearDetails.getCompleted(),
                questionnaireDetailsData.getName(), questionnaireDetailsData.getPollType(),
                questionnaireDetailsData.getSendToKIS(), questionnaireDetailsData.getResultScreenTypeCode(), false,diaryList
        );

        NavDirections action = QuestionFragmentDirections.Companion.actionQuestionFragmentToSendQuestionnaireFragment(additionalFields);
        Navigation.findNavController(activity, R.id.navigation_host_fragment).navigate(action);
        progressDialog.dismiss();
    }

    @Override
    public void postAnswerForMatrix(String answer, ArrayList<String> selection, String questionId) {
        for (int i = 0; i < questionsThatArePass.size(); i++) {
            if (questionsThatArePass.get(i).getId().equals(selected.getId())) {
                for (ExtendedQuestion question : questionsThatArePass.get(i).getQuestionData().getMatrixQuestions()) {
                    if (question.getId().equals(questionId)) {
                        question.setAnswer(new Answer(answer, selection));
                    }
                }
            }
        }
        progressDialog.show();
        MdocManager.postAnswer(pollId, pollAssignId, questionId, answer, pollVotingId, selection, selectedLanguage, new Callback<AnswerDataResponse>() {
            @Override
            public void onResponse(Call<AnswerDataResponse> call, Response<AnswerDataResponse> response) {
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        progressSb.setProgress(response.body().getData().getCompleted());
                        progressTv.setText(MdocUtil.calculateProgress(response.body().getData().getCompleted(), response.body().getData().getTotal()) + "%");
                        answearDetails = response.body().getData();
                        pollVotingId = answearDetails.getPollVotingActionId();
                    } else {
                        Timber.w("postAnswer %s", APIErrorKt.getErrorDetails(response));
                    }
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<AnswerDataResponse> call, Throwable t) {
                if (isAdded()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    public class QuestionComparator implements Comparator<Question> {
        @Override
        public int compare(Question o1, Question o2) {
            return Integer.compare(o1.getIndex(), o2.getIndex());
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(SECTION_INDEX, sectionIndex);
        savedInstanceState.putInt(QUESTION_INDEX, questionIndex);
        savedInstanceState.putInt(INDEX, index);
        savedInstanceState.putSerializable(SELECTED, selected);
        savedInstanceState.putString(POLL_ID, pollId);
        savedInstanceState.putString(POLL_ASSIGN_ID, pollAssignId);
        if (pollVotingId != null) {
            savedInstanceState.putString(POLL_VOTING_ID, pollVotingId);
        }
        savedInstanceState.putSerializable(QUESTIONS_THAT_ARE_PASS, questionsThatArePass);
        savedInstanceState.putSerializable(USER_ANSWERS, userAnswers);
        savedInstanceState.putSerializable(SECTIONS, sections);
        savedInstanceState.putSerializable(ADDED_SUBQUESTIONS, addedSubquestions);
        if (selectedQuestionType != null) {
            savedInstanceState.putSerializable(ANSWERS, selectedQuestionType.getAnswers());
            savedInstanceState.putBoolean(IS_NEW_ANSWER, selectedQuestionType.isNewAnswer());
        }
        savedInstanceState.putInt(PREVIOUS_BTN, previousBtn.getVisibility());
        savedInstanceState.putInt(NEXT_BTN, nextBtn.getVisibility());
    }

    public class SecQuestComparator implements Comparator<ExtendedQuestion> {
        @Override
        public int compare(ExtendedQuestion o1, ExtendedQuestion o2) {
            return o1.getCompositeIndex().compareTo(o2.getCompositeIndex());
        }
    }

    public class SectionComparator implements Comparator<Section> {
        @Override
        public int compare(Section o1, Section o2) {
            return Integer.compare(o1.getIndex(), o2.getIndex());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    private String getComspoiteIndexString(ExtendedQuestion question) {
        String result;
        if (question.getSection().getIndex() < 10) {
            result = "0" + question.getSection().getIndex();
        } else {
            result = question.getSection().getIndex() + "";
        }
        if (question.getIndex() < 10) {
            result += "0" + question.getIndex();
        } else {
            result += question.getIndex() + "";
        }

        return result;
    }

    @Override
    public void onPause() {
        super.onPause();
        addLastSeen(pollAssignId, selected);
    }

    private void showGoToAlert() {
        if (isStart() && (hasAnyAnswers() || hasLastSeen()) && !isGoToShownAlready) {
            int questionIndexInSection = ArrayListExtendedQuestionExtKt.getQuestionIndexForFirstUnfilledQuestion(questionsThatArePass);
            int questionIndexInSectionLastSeen = ArrayListExtendedQuestionExtKt.getRealIndexForLastSeen(questionsThatArePass, questionnaireDetailsData.getLastSeenPollQuestionId());

            String sectionNameFirstUnfilled = ArrayListExtendedQuestionExtKt.getSectionNameForFirstUnfilledQuestion(questionsThatArePass);
            String sectionNameLastSeen = ArrayListExtendedQuestionExtKt.getSectionNameForLastSeen(questionsThatArePass, questionnaireDetailsData.getLastSeenPollQuestionId());

            int firstUnfilledQuestionIndex = ArrayListExtendedQuestionExtKt.getIndexForFirstUnfilledQuestion(questionsThatArePass);
            int lastSeenIndex = ArrayListExtendedQuestionExtKt.getIndexForLastSeen(questionsThatArePass, questionnaireDetailsData.getLastSeenPollQuestionId());

            QuestionGoToFragment goToFragment = QuestionGoToFragment.Companion.newInstance(questionIndexInSection, sectionNameFirstUnfilled, questionIndexInSectionLastSeen, sectionNameLastSeen, firstUnfilledQuestionIndex, lastSeenIndex, this);
            goToFragment.show(getParentFragmentManager(), QuestionGoToFragment.class.getCanonicalName());
            isGoToShownAlready = true;
        } else {
            setQuestionUi(index);
        }
    }

    private void setNextButton() {
        nextBtn.setText(getString(R.string.next));
        nextBtn.setVisibility(View.VISIBLE);
        if (ExtendedQuestionExtKt.isMandatory(selected)) {
            observeCurrentAnswer();
        } else {
            nextBtn.setEnabled(true);
        }
    }

    private void setFinishButton() {
        nextBtn.setText(getString(R.string.questionnaire_complete));
        nextBtn.setVisibility(View.VISIBLE);
        if (ExtendedQuestionExtKt.isMandatory(selected)) {
            observeCurrentAnswer();
        } else {
            observePreviousAndCurrentAnswer();
        }
    }

    private Boolean isAnyPreviousQuestionAnswered() {
        if (questionsThatArePass != null && questionsThatArePass.size() > 0) {
            for (ExtendedQuestion question : questionsThatArePass) {
                if (question.getAnswer() != null) {
                    return true;
                }
            }
        }
        return false;
    }

    private void observePreviousAndCurrentAnswer() {
        if (selectedQuestionType != null) {
            selectedQuestionType.getQuestionIsAnswered().observe(getViewLifecycleOwner(), isAnswered -> {
                if (isAnyPreviousQuestionAnswered() || isAnswered) {
                    nextBtn.setEnabled(true);
                } else {
                    nextBtn.setEnabled(false);
                }
            });
        }
    }

    private void observeCurrentAnswer() {

        if (selectedQuestionType != null) {
            selectedQuestionType.getQuestionIsAnswered().observe(getViewLifecycleOwner(), isAnswered -> {
                if (isAnswered) {
                    nextBtn.setEnabled(true);
                } else {
                    nextBtn.setEnabled(false);
                }
            });
        }
    }
}
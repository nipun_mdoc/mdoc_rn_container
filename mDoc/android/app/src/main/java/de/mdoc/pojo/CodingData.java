package de.mdoc.pojo;

import java.util.ArrayList;

public class CodingData {

    private int totalCount;
    private ArrayList<CodingItem> list;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public ArrayList<CodingItem> getList() {
        return list;
    }

    public void setList(ArrayList<CodingItem> list) {
        this.list = list;
    }
}

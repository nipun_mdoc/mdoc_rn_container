package de.mdoc.modules.booking.confirm_booking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentBookingSuccessBinding
import de.mdoc.fragments.NewBaseFragment
import kotlinx.android.synthetic.main.fragment_booking_success.*


class BookingSuccessFragment : NewBaseFragment() {

    private val args: BookingSuccessFragmentArgs by navArgs()

    override val navigationItem: NavigationItem = NavigationItem.BookingSuccess

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentBookingSuccessBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_booking_success, container, false)
        binding.bookingMessage = resources.getString(R.string.booking_success_description, args.bookingAdditionalFields.appointmentType)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {

            override fun handleOnBackPressed() {
                val isBackToMasterData = findNavController().popBackStack(R.id.fragmentMasterData,false)

                if (!isBackToMasterData) {
                    val isBackToConfirmBooking = findNavController().popBackStack(R.id.confirmBookingFragment,false)

                    if (!isBackToConfirmBooking) {
                        findNavController().popBackStack()
                    }
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        txt_book_another_appointment?.setOnClickListener {
            bookAnotherAppointment()
        }

        txt_open_calendar?.setOnClickListener {
            openCalendar()
        }
    }

    private fun openCalendar() {
        val bundle = Bundle()
        bundle.putLong("DATE_OF_CALENDAR", args.appointment.appointmentDetails?.start ?: 0)
        findNavController().navigate(R.id.global_action_to_tab2, bundle)
    }

    private fun bookAnotherAppointment() {
        val isBackToSpecialities = findNavController().popBackStack(R.id.specialitiesFragment, false)
        if (!isBackToSpecialities) {
            // Case from Calendar
            findNavController().popBackStack(R.id.bookAppointmentFragment, false)
        }
    }
}
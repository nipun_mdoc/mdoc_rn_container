package de.mdoc.modules.devices.devices_factory

import android.bluetooth.BluetoothGatt
import java.util.*

class BluetoothDeviceFactory {

    fun createDevice( gatt: BluetoothGatt,dataFromDevice: ArrayList<Byte>) : BluetoothDevice
    {
        if(gatt.device.name.contains("LUNG")){
            return LungDevice(gatt, dataFromDevice)
        } else{
            return when(gatt.device.name) {
                "PO60" -> Po60Device(gatt, dataFromDevice)
                "AS87" -> As87Device(gatt, dataFromDevice)
                "BM57" -> Bm57Device(gatt, dataFromDevice)
                "Beurer GL50EVO" -> Gl50Device(gatt, dataFromDevice)

                else -> As87Device(gatt, dataFromDevice)
            }
        }
    }
}

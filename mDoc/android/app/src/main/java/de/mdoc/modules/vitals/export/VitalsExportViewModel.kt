package de.mdoc.modules.vitals.export

import android.content.Context
import androidx.lifecycle.viewModelScope
import de.mdoc.data.requests.ObservationStatisticsRequest
import de.mdoc.modules.common.export.BaseExportViewModel
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class VitalsExportViewModel(private val service: IMdocService, cb: ExportCallback) : BaseExportViewModel<ObservationStatisticsRequest>(cb) {

    override fun download(request: ObservationStatisticsRequest, type: ExportType, context: Context?) {
        isLoading.set(true)
        viewModelScope.launch {
            try {
                val response = service.downloadVitals(type.value.toUpperCase(), request)
                writeFile(type, context, response)
                isLoading.set(false)
            } catch (e: java.lang.Exception) {
                Timber.d(e)
                isLoading.set(false)
            }
        }
    }
}
package de.mdoc.activities.login

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Button
import android.widget.CheckBox
import de.mdoc.R
import de.mdoc.constants.PRIVACY_POLICY
import de.mdoc.constants.TERMS_AND_CONDITION
import de.mdoc.modules.medications.create_plan.data.CodingResponseKt
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class TermsPrivacyDialogHandler(
    private val context: Context,
    private val termsPrivacyCallback: TermsPrivacyCallback,
    private val progressHandler: ProgressHandler
) {

    val PRIVACY_CODING_URL = "https://mdoc.one/public/help"


    interface TermsPrivacyCallback {

        fun onTermsAndConditionsAccepted()

        fun onTermsAndConditionsDeclined()

        fun onPrivacyPolicyAccepted()

        fun onPrivacyPolicyDeclined()

    }

    fun openPrivacyFromCoding(){
        MdocManager.getCodingLightCall(PRIVACY_CODING_URL, object : Callback<CodingResponseKt> {
            override fun onResponse(
                    call: Call<CodingResponseKt>,
                    response: Response<CodingResponseKt>
            ) {
                progressHandler.hideProgress()
                if (response.isSuccessful) {
                    val htmlPrivacy = response.body()!!.data?.getItemForCode(PRIVACY_POLICY)?.display ?: ""
                    if (!htmlPrivacy.isEmpty())
                        showDialog(
                                htmlPrivacy,
                                onAccepted = ::onPrivacyPolicyAccepted,
                                onDeclined = termsPrivacyCallback::onPrivacyPolicyDeclined
                        )
                } else {
                    Timber.w("getPrivacy %s", response.getErrorDetails())
                }
            }

            override fun onFailure(call: Call<CodingResponseKt>, t: Throwable) {
                Timber.w(t, "getPrivacy")
                progressHandler.hideProgress()
            }
        })
    }

    fun openTermsFromCoding(){
        MdocManager.getCodingLightCall(PRIVACY_CODING_URL, object : Callback<CodingResponseKt> {
            override fun onResponse(
                    call: Call<CodingResponseKt>,
                    response: Response<CodingResponseKt>
            ) {
                progressHandler.hideProgress()
                if (response.isSuccessful) {
                    val htmlTermsAndConditions = response.body()!!.data?.getItemForCode(TERMS_AND_CONDITION)?.display ?: ""
                    if (!htmlTermsAndConditions.isEmpty())
                        showDialog(
                                htmlTermsAndConditions,
                                onAccepted = ::onTermsAndConditionsAccepted,
                                onDeclined = termsPrivacyCallback::onTermsAndConditionsDeclined
                        )
                } else {
                    Timber.w("getPrivacy %s", response.getErrorDetails())
                }
            }

            override fun onFailure(call: Call<CodingResponseKt>, t: Throwable) {
                Timber.w(t, "getPrivacy")
                progressHandler.hideProgress()
            }
        })
    }

    private fun showDialog(htmlPrivacy: String, onAccepted: () -> Unit, onDeclined: () -> Unit) {
        try {
            val builder = AlertDialog.Builder(context)
            val inflater = LayoutInflater.from(context)
            val view = inflater.inflate(R.layout.privacy_layout, null)
            builder.setView(view)
            val dialog = builder.show()
            dialog.setCancelable(false)
            val confirmPrivacy = view.findViewById<View>(R.id.btnConfirmPrivacy) as Button
            val cancelPrivacy = view.findViewById<View>(R.id.btnCancelPrivacy) as Button
            val agree = view.findViewById<View>(R.id.checkPrivacy) as CheckBox
            val webView = view.findViewById<View>(R.id.webView) as WebView
            confirmPrivacy.isEnabled = false

            val webSettings = webView.settings
            webSettings.defaultTextEncodingName = "utf-8"
            if(!htmlPrivacy.isNullOrEmpty()){
                //if link doesn't have http shouldOverrideUrlLoading won't be triggered and user will end up with with blank screen
                val convertedHtml: String = htmlPrivacy.replace("<a href=\"www", "<a href=\"http://www", true)
                webView.loadDataWithBaseURL(null, convertedHtml, "text/html; charset=utf-8", "UTF-8", null)
            }

            webView?.webViewClient=object : WebViewClient(){
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    //this will disable clicks in webview
                    return true
                }
            }

            agree.setOnClickListener {
                if (agree.isChecked) {
                    confirmPrivacy.alpha = 1f
                    confirmPrivacy.isEnabled = true
                } else {
                    confirmPrivacy.alpha = 0.7f
                    confirmPrivacy.isEnabled = false
                }
            }

            confirmPrivacy.setOnClickListener {
                dialog.dismiss()
                onAccepted.invoke()
            }

            cancelPrivacy.setOnClickListener {
                dialog.dismiss()
                onDeclined.invoke()
            }
        }catch (e:Exception){
            onDeclined.invoke()
        }
    }

    private fun onTermsAndConditionsAccepted() {
        progressHandler.showProgress()
        MdocManager.confirmTermAndCondition(object : Callback<MdocResponse> {
            override fun onResponse(
                call: Call<MdocResponse>,
                response: Response<MdocResponse>
            ) {
                progressHandler.hideProgress()
                if (response.isSuccessful) {
                    termsPrivacyCallback.onTermsAndConditionsAccepted()
                }else{
                    termsPrivacyCallback.onTermsAndConditionsDeclined()
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                progressHandler.hideProgress()
                termsPrivacyCallback.onTermsAndConditionsDeclined()
            }
        })
    }

    private fun onPrivacyPolicyAccepted() {
        progressHandler.showProgress()
        MdocManager.confirmPrivacy(object : Callback<MdocResponse> {
            override fun onResponse(
                call: Call<MdocResponse>,
                response: Response<MdocResponse>
            ) {
                progressHandler.hideProgress()
                if (response.isSuccessful) {
                    termsPrivacyCallback.onPrivacyPolicyAccepted()
                }else{
                    termsPrivacyCallback.onTermsAndConditionsDeclined()
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                progressHandler.hideProgress()
                termsPrivacyCallback.onTermsAndConditionsDeclined()
            }
        })
    }
}
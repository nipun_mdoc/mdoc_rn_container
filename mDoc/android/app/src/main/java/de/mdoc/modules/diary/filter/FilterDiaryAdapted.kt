package de.mdoc.modules.diary.filter

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.util.toBitmap
import kotlinx.android.synthetic.main.common_filter_checkbox_item.view.*


class FilterDiaryAdapted(private val context: Context? = null) : RecyclerView.Adapter<FilterDiaryAdapted.FilterDiaryViewHolder>() {
    var items: List<DiaryEntryType> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterDiaryViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.common_filter_checkbox_item, parent, false)
        return FilterDiaryViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilterDiaryViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getAdapterData(): List<DiaryEntryType> {
        return items
    }

    fun clearCheckboxSelection() {
        items.forEach {
            it.isSelected = false
        }
        notifyDataSetChanged()
    }

    inner class FilterDiaryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), CompoundButton.OnCheckedChangeListener {
        private val checkbox: CheckBox = itemView.checkBoxItem

        init {
            checkbox.setOnCheckedChangeListener(this)
        }

        fun bind(item: DiaryEntryType) {
            checkbox.isChecked = item.isSelected
            checkbox.text = item.name

            val mDrawable: Drawable? = BitmapDrawable(context?.resources, item.base64Image?.toBitmap())
            val transparentDrawable: Drawable = ColorDrawable(Color.TRANSPARENT)
            if (mDrawable != null) {
                checkbox.setCompoundDrawablesWithIntrinsicBounds(mDrawable, transparentDrawable, transparentDrawable, transparentDrawable)
            }
        }

        override fun onCheckedChanged(p0: CompoundButton?, isChecked: Boolean) {
            items[position].isSelected = isChecked
        }
    }
}

package de.mdoc.modules.mental_goals.mental_goal_edit

import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.util.handleOnBackPressed
import kotlinx.android.synthetic.main.fragment_status_notification.*

class NotificationGoalFragment: MdocFragment() {
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_status_notification
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        handleOnBackPressed {
            findNavController().navigate(R.id.navigation_mental_goal)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_to_goal?.setOnClickListener {
            findNavController().navigate(R.id.navigation_mental_goal)
        }
    }
}

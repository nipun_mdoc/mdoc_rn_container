package de.mdoc.newlogin

import de.mdoc.newlogin.Model.CaptchaResponse

interface AuthListener {
    fun sucess()
    fun failer()
    fun onImpressumClick(data : String)
    fun onDatenschutzDieClick(data : String)
    fun onAGBClick(data : String)
    fun onCloseClick()
    fun onScanClick()
    fun onLoadCaptcha(response: CaptchaResponse?)
    fun isCaptchaVisible(): Boolean?
    fun clearCaptchaField()
    fun changePassword(token: String)
    fun showMobileNumberScreen()
    fun showOTPScreen()
}
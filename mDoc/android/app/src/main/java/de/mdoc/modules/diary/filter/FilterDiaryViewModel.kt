package de.mdoc.modules.diary.filter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.mdoc.modules.diary.data.DiaryFilter
import org.joda.time.DateTime

class FilterDiaryViewModel: ViewModel() {


    val from = MutableLiveData<DateTime?>(null)
    val to = MutableLiveData<DateTime?>(null)

    fun getFilter(categories: List<String>): DiaryFilter {
        return DiaryFilter(
                categories = if (categories.isNotEmpty()) categories else null,
                from = from.value,
                to = to.value)
    }
}
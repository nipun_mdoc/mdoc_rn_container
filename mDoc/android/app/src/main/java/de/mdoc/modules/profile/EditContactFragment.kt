package de.mdoc.modules.profile

import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import androidx.appcompat.widget.AppCompatSpinner
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.databinding.FragmentEmergencyContactBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.common.dialog.ButtonClicked
import de.mdoc.modules.common.dialog.CommonDialogFragment
import de.mdoc.modules.profile.data.Contact
import de.mdoc.network.RestClient
import de.mdoc.pojo.CodingItem
import de.mdoc.storage.AppPersistence
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.util.setActionTitle
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_emergency_contact.*

class EditContactFragment: NewBaseFragment(), CommonDialogFragment.OnButtonClickListener {
    lateinit var activity: MdocActivity
    val args: EditContactFragmentArgs by navArgs()
    private val editContactViewModel by viewModel {
        EditContactViewModel(
                RestClient.getService())
    }
    var emergencyContacts = arrayListOf<Contact>()
    var indexOfItem = 0
    override val navigationItem: NavigationItem = NavigationItem.EmergencyContactsEdit
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentEmergencyContactBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_emergency_contact, container, false)
        activity = getActivity() as MdocActivity
        indexOfItem = args.indexOfItem
        emergencyContacts = ArrayList(args.emergencyContacts.asList())

        binding.lifecycleOwner = this
        binding.header = activity.resources.getString(R.string.profile_contact_header, indexOfItem.plus(1))
        editContactViewModel.name.value = emergencyContacts[indexOfItem].name
        editContactViewModel.email.value = emergencyContacts[indexOfItem].email
        editContactViewModel.telephone.value = emergencyContacts[indexOfItem].phone

        binding.viewModel = editContactViewModel
        binding.isDeleteVisible = (emergencyContacts.size == 1 || args.isNewItem)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setActionTitle(resources.getString(R.string.profile_contact_title, indexOfItem.plus(1)))

        shouldShowEmailField()
        setupSpinner(relatonshipSpinner)
        txtDeleteContact?.setOnClickListener {
            promptDeleteContact()
        }
    }

    private fun shouldShowEmailField() {
        if (resources.getBoolean(R.bool.has_email_in_emergency_contact)) {
            txtEmail?.visibility = View.VISIBLE
            tilEmail?.visibility = View.VISIBLE
        }
        else {
            txtEmail.visibility = View.GONE
            tilEmail?.visibility = View.GONE
        }
    }

    private fun setupSpinner(spinnerLayout: AppCompatSpinner) {
        val relationshipList = arrayListOf(CodingItem())
        relationshipList.addAll(AppPersistence.relationshipCoding)
        val spinnerAdapter = SpinnerAdapter(activity, relationshipList)
        spinnerLayout.adapter = spinnerAdapter

        spinnerLayout.setSelection(relationshipList.indexOfFirst { it.code == emergencyContacts[indexOfItem].relation }, false)

        spinnerLayout.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, positon: Int, p3: Long) {
                if (relationshipList.getOrNull(positon)?.code != emergencyContacts[indexOfItem].relation) {
                    emergencyContacts[indexOfItem].relation = relationshipList.getOrNull(positon)?.code
                    editContactViewModel.relation.value = relationshipList.getOrNull(positon)?.code
                }
            }
        }
    }

    private fun promptDeleteContact() {
        CommonDialogFragment.Builder()
            .title(resources.getString(R.string.profile_delete_contact_dialog_title))
            .setPositiveButton(resources.getString(R.string.yes))
            .setNegativeButton(resources.getString(R.string.cancel))
            .setOnClickListener(this)
            .build()
            .showNow(activity.supportFragmentManager, "")
    }

    private fun deleteContact() {
        emergencyContacts.removeAt(indexOfItem)
        txtDeleteContact.visibility = View.INVISIBLE
        if (emergencyContacts.isEmpty()) {
            emergencyContacts
        }
        editContactViewModel.updateEmergencyContacts(emergencyContacts, onSuccess = {
            findNavController().popBackStack()
        }, onError = {
            txtDeleteContact?.visibility = View.VISIBLE
        })
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_contact_edit)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.saveContact) {
            emergencyContacts[indexOfItem].name = editContactViewModel.name.value
            emergencyContacts[indexOfItem].email = editContactViewModel.email.value
            emergencyContacts[indexOfItem].phone = editContactViewModel.telephone.value
            editContactViewModel.updateEmergencyContacts(emergencyContacts, onSuccess = {
                emergencyContacts[indexOfItem].relation = editContactViewModel.relation.value
                findNavController().popBackStack()
            }, onError = {})
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDialogButtonClick(button: ButtonClicked) {
        if (button == ButtonClicked.POSITIVE) {
            deleteContact()
        }
    }
}
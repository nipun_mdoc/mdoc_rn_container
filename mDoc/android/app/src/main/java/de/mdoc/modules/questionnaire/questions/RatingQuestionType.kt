package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.adapters.RatingAdapter
import de.mdoc.pojo.ExtendedOption
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import kotlinx.android.synthetic.main.rating_layout.view.*

class RatingQuestionType: QuestionType() {

    private var previousSelected: RelativeLayout? = null
    private var previousSelectedTv: TextView? = null

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.rating_layout, parent, false)
        val mandatory = view.findViewById<TextView>(R.id.mandatoryLabel)
        questionIsAnswered.value = false
        val gridView = view.ratingGrid
        val startTv = view.startTv
        val endTv = view.endTv

        gridView.isExpanded = true
        val questionData = question.questionData
        val extendedOptions = ArrayList(questionData.options.mapIndexed { index, option ->
            ExtendedOption(
                    option,
                    question.scaleBeginningText,
                    question.scaleEndText,
                    index.toString(),
                    question.answer
                          )
        })

        gridView.numColumns = extendedOptions.size

        startTv.text = question.scaleBeginningText
        endTv.text = question.scaleEndText
        val adapter = RatingAdapter(extendedOptions, parent.context, question)
        gridView.adapter = adapter

        if (question.answer != null) {
            extendedOptions.forEach {
                if (it.answer.answerData == question.answer.answerData) {
                    questionIsAnswered.value = true
                    answers.add(question.answer.answerData)
                    if (question.answer.selection != null) {
                        selection = question.answer.selection
                    }
                }
            }
        }

        gridView.onItemClickListener =
                AdapterView.OnItemClickListener { adapterView, itemView, position, _ ->
                    questionIsAnswered.value = true
                    answers.clear()
                    selection.clear()
                    val e = adapterView.getItemAtPosition(position) as ExtendedOption
                    answers.add(e.getValue(question.selectedLanguage))
                    selection.add(e.key)
                    isNewAnswer = true
                    val rl = (itemView as LinearLayout).getChildAt(0) as RelativeLayout
                    val tv = rl.getChildAt(0) as TextView

                    rl.setBackgroundResource(R.drawable.blue_gray_circle_stroke)
                    tv.setTextColor(ContextCompat.getColor(view.context, R.color.colorPrimary))

                    if (adapter.preSelectedRl != null) {
                        adapter.preSelectedRl.setBackgroundResource(R.drawable.blue_gray_circle)
                        adapter.preSelectedTv.setTextColor(itemView.getResources()
                            .getColor(R.color.white))
                    }

                    if (previousSelected != null && previousSelected !== rl) {
                        previousSelected!!.setBackgroundResource(R.drawable.blue_gray_circle)
                        previousSelectedTv!!.setTextColor(itemView.getResources()
                            .getColor(R.color.white))
                    }

                    previousSelected = rl
                    previousSelectedTv = tv
                    adapter.preSelectedRl = null
                    adapter.preSelectedTv = null
                }

        mandatoryCheck(mandatory, null, question)

        return view
    }
}

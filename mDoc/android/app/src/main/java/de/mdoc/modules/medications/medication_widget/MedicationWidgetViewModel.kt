package de.mdoc.modules.medications.medication_widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.medications.create_plan.data.MedicationAdapterData
import de.mdoc.modules.medications.data.MedicationAdministration
import de.mdoc.modules.medications.data.hasMoreData
import de.mdoc.modules.medications.data.prepareAdapterData
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class MedicationWidgetViewModel(private val mDocService: IMdocService) : ViewModel() {

    val isLoading = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    val isSeeMore = MutableLiveData(false)

    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")

    val content: MutableLiveData<List<MedicationAdapterData>> = MutableLiveData(emptyList())

    fun getUpcomingMedicationAdministration() {
        viewModelScope.launch {
            try {
                isLoading.value = true
                isShowContent.value = false
                isShowNoData.value = false
                getMedicationAdministration()
                isShowContent.value = content.value?.size?:0 > 0
                isShowNoData.value = content.value?.size == 0
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowNoData.value = true
                isShowContent.value = false
            }
        }
    }

    private suspend fun getMedicationAdministration() {
        val today = formatter.print(DateTime.now())
        val tomorrow = formatter.print(DateTime.now().plusHours(24))

        val response = mDocService.getMedicationAdministration(effectivePeriodStartGTE = today, effectivePeriodStartLT = tomorrow,
                subject = MdocAppHelper.getInstance().userId)

        isSeeMore.value = response.hasMoreData()
        content.value =  response.prepareAdapterData(take = 3)
    }
}
package de.mdoc.pojo;

import de.mdoc.network.response.MdocResponse;

/**
 * Created by AdisMulabdic on 12/7/17.
 */

public class PrivacyPolicyResponse extends MdocResponse {
    private String  data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}

package de.mdoc.network.response;

import java.util.List;

public class CategoryResponse extends MdocResponse {

    private List<String> data;

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }
}
package de.mdoc.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by ema on 12/29/16.
 */

public class Option implements Serializable{
    protected String key;
    protected Map<String,String> value;

    public Option() {
    }

    public Option(String key, String value, String language) {
        this.key = key;
        this.value = new HashMap<String, String>() {{
            put(language, value);
        }};
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue(String language) {
        return value.get(language);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, value);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } else if (obj == null) {
            return false;
        } else if (obj instanceof Option) {
            Option q = (Option) obj;
            return this.key.equals(q.key);
        }
        return false;
    }

}

package de.mdoc.modules.entertainment.fragments.pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.davemorrissey.labs.subscaleview.ImageSource
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.entertainment.repository.MagazinesRepository
import de.mdoc.modules.entertainment.viewmodels.MagazinePageViewModel
import de.mdoc.modules.entertainment.viewmodels.MagazineSharedViewModel
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.entertainment_magazine_page_item.*

class MagazinePageViewFragment : Fragment (), SubsamplingScaleImageView.OnImageEventListener {

    lateinit var activity: MdocActivity
    lateinit var pdfId: String
    lateinit var magazineId: String
    lateinit var page: String

    companion object {
        fun newInstance(page: String, pdfId: String?, magazineId: String?, target: Fragment): MagazinePageViewFragment {
            val args = Bundle()
            args.putSerializable(MdocConstants.MAGAZINE, pdfId)
            args.putSerializable(MdocConstants.MAGAZINE_ID, magazineId)
            args.putSerializable(MdocConstants.MAGAZINE_PAGE_NUMBER, page)
            val fragment = MagazinePageViewFragment()
            fragment.arguments = args
            fragment.setTargetFragment(target, 0)
            return fragment
        }
    }

    private val viewModel by viewModel {
        MagazinePageViewModel(
                mdocAppHelper = MdocAppHelper.getInstance(),
                repository = MagazinesRepository()
        )
    }

    private lateinit var sharedViewModel: MagazineSharedViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.entertainment_magazine_page_item, container, false)
        init(savedInstanceState)
        return view
    }

    fun init(savedInstanceState: Bundle?) {
        activity = getActivity() as MdocActivity

        val bundle = arguments
        if (bundle != null) {
            pdfId = bundle.getString(MdocConstants.MAGAZINE)?:""
            magazineId = bundle.getString(MdocConstants.MAGAZINE_ID)?:""
            page = bundle.getString(MdocConstants.MAGAZINE_PAGE_NUMBER)?:""
        }

        sharedViewModel = targetFragment?.run {
            ViewModelProviders.of(this).get(MagazineSharedViewModel::class.java)
        } ?: throw Exception("Invalid Activity")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupImagePreview()
        viewModel.mediaLiveData.observe(this) { data ->
            magazinePreview.setOnImageEventListener(this)
            magazinePreview.setImage(ImageSource.bitmap(data))
        }

        viewModel.isError.observe(this) { data ->
            when (data) {
                true -> {
                    emptyView.visibility = View.VISIBLE
                    sharedViewModel.enableMagazineScrolling()
                }
                false -> emptyView.visibility = View.GONE
            }
        }

        bindVisibleGone(progress, viewModel.isProgress)
    }

    private fun setupImagePreview () {
        magazinePreview.minScale = 1.0f
        magazinePreview.maxScale = 3.5f
        magazinePreview.setDoubleTapZoomDpi(2)
    }

    fun reloadData () {
        if(!isAdded) return

        // Notify live data new task
        viewModel.isProgress.set(true)

        // Clear current page content
        magazinePreview.recycle()
        magazinePreview.invalidate()

        // Load new content from API
        viewModel.getMediaById(pdfId, page)
    }

    override fun onImageLoaded() {
        sharedViewModel.enableMagazineScrolling()
    }

    override fun onImageLoadError(e: java.lang.Exception?) {
        sharedViewModel.enableMagazineScrolling()
    }

    override fun onPreviewLoadError(e: java.lang.Exception?) {
        sharedViewModel.enableMagazineScrolling()
    }

    override fun onReady() {}

    override fun onTileLoadError(e: java.lang.Exception?) {}

    override fun onPreviewReleased() {}
}
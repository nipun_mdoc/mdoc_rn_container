package de.mdoc.pojo;

import java.io.Serializable;

public class Account implements Serializable {

    private String reference;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}

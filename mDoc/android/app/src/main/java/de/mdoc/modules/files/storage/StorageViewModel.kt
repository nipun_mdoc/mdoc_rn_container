package de.mdoc.modules.files.storage

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.network.response.files.ResponseSearchResultsFileStorageDocumentImpl_
import de.mdoc.service.IMdocService
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.launch
import timber.log.Timber

class StorageViewModel (private val mDocService: IMdocService): ViewModel () {

    val filesUploadCount = liveData<Long>(-1)

    private val _storage = MutableLiveData<ResponseSearchResultsFileStorageDocumentImpl_>().apply {
        viewModelScope.launch {
            try {
                value = mDocService.getFilesStorage()
                val uploadCount = value?.data?.list?.get(0)?.fileUploadsTotalCount?:0
                filesUploadCount.set(uploadCount)

            } catch (e: java.lang.Exception) {
                Timber.d(e)
            }
        }
    }

    var storage = _storage
}
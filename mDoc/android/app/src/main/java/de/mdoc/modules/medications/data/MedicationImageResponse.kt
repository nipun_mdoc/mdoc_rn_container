package de.mdoc.modules.medications.data

data class MedicationImageResponse(
        val code: String? = "",
        val `data`: String? = "",
        val message: String? = "",
        val timestamp: Long? = 0
                                  )
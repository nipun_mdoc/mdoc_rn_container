package de.mdoc.modules.common.dialog

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.fragment.app.DialogFragment
import de.mdoc.R
import kotlinx.android.synthetic.main.dialog_common.*

open class CommonDialogFragment private constructor(val title: String?,
                                                    val description: String?,
                                                    val listener: OnButtonClickListener?,
                                                    val cancelListener: CancelListener?,
                                                    private val positiveText: String?,
                                                    val navigateBack: Boolean?,
                                                    private val negativeText: String?): DialogFragment() {

    interface OnButtonClickListener {
        fun onDialogButtonClick(button: ButtonClicked)
    }

    interface CancelListener {
        fun onCancelDialog()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
                             ): View? {
        return inflater.inflate(R.layout.dialog_common, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCanceledOnTouchOutside(false)

        description?.let {
            txtDescription.text = it
        }

        title?.let {
            txtTitle.visibility = View.VISIBLE
            txtTitle.text = it
        }

        positiveText?.let {
            positiveButton.visibility = View.VISIBLE
            positiveButton.text = it
            positiveButton?.setOnClickListener {
                listener?.onDialogButtonClick(ButtonClicked.POSITIVE)
                dismiss()
            }
        }

        negativeText?.let {
            negativeButton.visibility = View.VISIBLE
            negativeButton.text = it
            negativeButton?.setOnClickListener {
                listener?.onDialogButtonClick(ButtonClicked.NEGATIVE)
                dismiss()
            }
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        cancelListener?.onCancelDialog()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        if (navigateBack == true) {
            parentFragmentManager.popBackStack()
        }
    }

    override fun onResume() {
        val width = RelativeLayout.LayoutParams.WRAP_CONTENT
        val height = RelativeLayout.LayoutParams.WRAP_CONTENT
        dialog?.window?.setLayout(width, height)

        super.onResume()
    }

    data class Builder(var title: String? = null,
                       var description: String? = null,
                       var positiveText: String? = null,
                       var negativeText: String? = null,
                       var navigateBack: Boolean = false) {

        var listener: OnButtonClickListener? = null
        var cancelListener: CancelListener? = null

        fun title(title: String) = apply { this.title = title }
        fun description(description: String) = apply { this.description = description }
        fun setPositiveButton(positiveText: String) = apply { this.positiveText = positiveText }
        fun setNegativeButton(negativeText: String) = apply { this.negativeText = negativeText }
        fun navigateBackOnCancel(navigateBack: Boolean) = apply { this.navigateBack = navigateBack }
        fun setOnClickListener(listener: OnButtonClickListener): Builder {
            this.listener = listener
            return this
        }

        fun setOnCancelListener(cancelListener: CancelListener): Builder {
            this.cancelListener = cancelListener
            return this
        }

        fun build() = CommonDialogFragment(title, description,
                listener, cancelListener, positiveText, navigateBack, negativeText)
    }
}
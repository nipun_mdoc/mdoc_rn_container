package de.mdoc.modules.devices
import de.mdoc.data.create_bt_device.BtDevice
import de.mdoc.data.create_bt_device.MedicalDeviceAdapterData

object DevicesUtil {

    var deviceItem: MedicalDeviceAdapterData?=null
    var metricCode:ArrayList<String>?=null
    var medicalDevicesList = arrayListOf<MedicalDeviceAdapterData>()

    fun filterMacAddresses(input:List<BtDevice>?):ArrayList<String>{
        val listOfMacAddresses = arrayListOf<String>()

        if (input != null) {
            for(device in input){

                for (id in device.identifier){
                    if(id.use=="SECONDARY"){
                        if(!listOfMacAddresses.contains(id.value)) {
                            listOfMacAddresses.add(id.value)
                        }
                    }
                }
            }
        }
        return listOfMacAddresses
    }


    fun trimDoubleToDot(input: String): String {
        return when {
            input.contains(".0") -> {
                val indexOfDot = input.indexOf(".")
                input.substring(0, indexOfDot)
            }
            input.contains(".") -> {
                val indexOfDot = input.indexOf(".")
                input.substring(0, indexOfDot+2)
            }
            else -> input
        }
    }
}
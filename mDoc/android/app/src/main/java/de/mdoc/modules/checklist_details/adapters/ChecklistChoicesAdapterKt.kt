package de.mdoc.modules.checklist_details.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.RelativeLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.interfaces.ChecklistListener
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.AnswerDataResponse
import de.mdoc.pojo.OptionExtension
import de.mdoc.pojo.ShortQuestion
import de.mdoc.util.MdocUtil
import de.mdoc.util.getErrorMessage
import kotlinx.android.synthetic.main.placeholder_checklist_choice.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber


class ChecklistChoicesAdapterKt(var items: ArrayList<OptionExtension>, var parentItem : ShortQuestion, var pollVotingId : String, var defaultLanguage : String): RecyclerView.Adapter<ChoicesViewHolder>(){

    private var listener: ChecklistListener? = null
    var context: Context? = null

    fun setChecklistListener(listener: ChecklistListener) {
        this.listener = listener
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ChoicesViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.placeholder_checklist_choice, parent, false)
        context = parent.context
        return ChoicesViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChoicesViewHolder, position: Int) {
        holder.setIsRecyclable(false)
        val item = items[position]

        holder.checkBoxItem.text = item.getValue(item.language)

        if (MdocUtil.isNotNull(item.answer)) {
            holder.checkBoxItem.isChecked = true
            Timber.d("Selected %s", item.answer)
        } else {
            holder.checkBoxItem.isChecked = false
            Timber.d("Not selected %s", item.answer)
        }

        holder.checkBoxItem.setOnCheckedChangeListener { buttonView, isChecked ->
            val text = holder.checkBoxItem.text.toString()
            val key = item.key

            if (isChecked) {
                for (i in items.indices) {
                    if (text == items[i].getValue(item.language)) {
                        items[position].answer = text
                        items[position].selection = key
                    }
                }
            } else {
                for (i in items.indices) {
                    if (text == items[i].getValue(item.language)) {
                        items[position].answer = null
                        items[position].selection = null
                    }
                }
            }

            try {
                postAnswer(parentItem.pollId, parentItem.pollAssignId, parentItem.id)
            }catch (e:Exception){
                Timber.d("Exception: %s", e.message)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    private fun postAnswer(pollId: String, pollAssignId: String, selectedId: String) {
        var tempAnswers : ArrayList<String> = arrayListOf()
        var tempSelections : ArrayList<String> = arrayListOf()

        items.forEach {
            if(it.answer != null){
                tempAnswers?.add(it.answer)
                tempSelections?.add(it.selection)
            }
        }
        val answer = Gson().toJson(tempAnswers)

        MdocManager.postAnswer(pollId, pollAssignId, selectedId, answer, pollVotingId, tempSelections, defaultLanguage, object : Callback<AnswerDataResponse> {
            override fun onResponse(call: Call<AnswerDataResponse>, response: Response<AnswerDataResponse>) {
                if (response.isSuccessful) {
                    pollVotingId = response.body()!!.data.pollVotingActionId

                    listener?.updateList(selectedId)
                    listener?.updatePollVotingId(pollVotingId)
                } else {
                    MdocUtil.showToastLong(context, response.getErrorMessage())
                }
            }

            override fun onFailure(call: Call<AnswerDataResponse>, t: Throwable) {
                MdocUtil.showToastLong(context, "Error " + t.message)
            }
        })
    }

}

class ChoicesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val rootRl: RelativeLayout = itemView.rootRl
    val checkBoxItem: CheckBox = itemView.checkBoxItem
}
package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 12/9/16.
 */

public class Choice implements Serializable {

    private String mealDescription;
    private String mealOfferId;
    private String mealReview;

    public String getMealDescription() {
        return mealDescription;
    }

    public void setMealDescription(String mealDescription) {
        this.mealDescription = mealDescription;
    }

    public String getMealOfferId() {
        return mealOfferId;
    }

    public void setMealOfferId(String mealOfferId) {
        this.mealOfferId = mealOfferId;
    }

    public String getMealReview() {
        return mealReview;
    }

    public void setMealReview(String mealReview) {
        this.mealReview = mealReview;
    }
}

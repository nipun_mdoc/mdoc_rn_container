package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Component(
        val measure: Measure?=null,
        val unit: Unit?=null,
        val measureConfig:ArrayList<MeasureConfig>?=null,
        val min:String?=null,
        val max:String?=null
):Parcelable{

    fun displayMeasurement():String{
        if (measureConfig != null) {
            for(item in measureConfig){
                if(item.name == "displayMeasure"){
                    return item.display?:""
                }
            }
        }
        return ""
    }
    fun measureTimesPrompt():String{
        if (measureConfig != null) {
            for(item in measureConfig){
                if(item.name == "measureTimesPrompt"){
                    return item.display?:""
                }
            }
        }
        return ""
    }
    fun measurePrompt():String{
        if (measureConfig != null) {
            for(item in measureConfig){
                if(item.name == "measurePrompt"){
                    return item.display?:""
                }
            }
        }
        return ""
    }
}
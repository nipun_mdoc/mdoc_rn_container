package de.mdoc.modules.files.data

import de.mdoc.pojo.CodingData
import de.mdoc.pojo.SearchFilesData

data class FilesData(
    val files: SearchFilesData,
    val categoryCoding: CodingData
)

fun FilesData.listSize ():Int {
    return files.list.size
}
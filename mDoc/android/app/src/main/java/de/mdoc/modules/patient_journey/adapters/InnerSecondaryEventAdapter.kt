package de.mdoc.modules.patient_journey.adapters

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.modules.patient_journey.data.Activity
import de.mdoc.modules.patient_journey.journey_details.JourneyDetailsFragmentDirections
import de.mdoc.modules.questionnaire.QuestionnaireListFragmentDirections.Companion.actionQuestionnaireListFragmentToQuestionnaireTransitionFragment
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.SelfAssignResponse
import kotlinx.android.synthetic.main.placeholder_inner_secondary_event.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InnerSecondaryEventAdapter(val items: List<Activity>, val context: Context):
        RecyclerView.Adapter<InnerSecondaryEventViewHolder>() {


    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
    ): InnerSecondaryEventViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_inner_secondary_event, parent, false)

        return InnerSecondaryEventViewHolder(view)
    }

    private fun assignQuestionnaire(pollId: String, holder: InnerSecondaryEventViewHolder) {
        MdocManager.assignQuestionnaire(pollId, object : Callback<SelfAssignResponse?> {
            override fun onResponse(call: Call<SelfAssignResponse?>, response: Response<SelfAssignResponse?>) {
                if (response.isSuccessful) {
                    if (response.body() != null && response.body()!!.data != null) {
                        holder.cardView.setEnabled(false);
                        var pollAssignId = response.body()!!.data!!.id
                        val action =  JourneyDetailsFragmentDirections.actionJourneyDetailsFragmentToQuestionFragment(pollId, pollAssignId, false)
                        (context as MdocActivity).findNavController(R.id.navigation_host_fragment).navigate(action)
                        val handle = Handler()
                        handle.postDelayed({
                            holder.cardView.setEnabled(true);
                        }, 1500)
                    }
                }
            }

            override fun onFailure(call: Call<SelfAssignResponse?>, t: Throwable) {}
        })
    }

    override fun onBindViewHolder(holder: InnerSecondaryEventViewHolder, position: Int) {
        val item = items[position]
        holder.title.text = item.detail?.reasonReference?.getOrNull(0)
            ?.display

        when (item.detail?.kind?.code) {
            MEDIA -> holder.image.setImageResource(R.drawable.ic_media_ondemand)
            FILE -> holder.image.setImageResource(R.drawable.ic_file_ondemand)
            QUESTIONNAIRE -> holder.image.setImageResource(R.drawable.ic_questionnaire)
            else          -> holder.image.setImageResource(0)
        }

        holder.cardView.setOnClickListener {
            item.detail?.reasonReference?.forEach {
                val activityId = it?.reference
                val dashIndex = activityId?.indexOf("/") ?: -1
                var plainId = ""
                if (dashIndex != -1) {
                    plainId = activityId?.substring(dashIndex.plus(1), activityId.length) ?: ""
                }
                when {
                    it?.reference?.contains(MEDIA) == true         -> {
                        val action =
                                JourneyDetailsFragmentDirections.actionJourneyDetailsFragmentToMediaTransitionFragment(
                                        plainId)
                        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                            .navigate(action)
                    }

                    it?.reference?.contains(FILE) == true          -> {
                        val action =
                                JourneyDetailsFragmentDirections.actionJourneyDetailsFragmentToFilesTransitionFragment(
                                        plainId)
                        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
                            .navigate(action)
                    }

                    it?.reference?.contains(QUESTIONNAIRE) == true -> {
                        holder.cardView.setEnabled(false);
                        assignQuestionnaire(plainId, holder)
                        val handle = Handler()
                        handle.postDelayed({
                            holder.cardView.setEnabled(true);
                        }, 1500)
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    companion object {
        const val MEDIA = "media"
        const val FILE = "files"
        const val QUESTIONNAIRE = "questionnaire"
    }
}

class InnerSecondaryEventViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val title: TextView = itemView.title
    val image: ImageView = itemView.img_activity_type
    val cardView: CardView = itemView.cardView
}
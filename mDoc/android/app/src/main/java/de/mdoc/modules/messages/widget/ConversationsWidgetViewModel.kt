package de.mdoc.modules.messages.widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.messages.data.ListItem
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class ConversationsWidgetViewModel (private val mDocService: IMdocService) : ViewModel() {


    var isLoading = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    val newMessagesCount = MutableLiveData(0)

    var activeConversations: MutableLiveData<List<ListItem>> =
            MutableLiveData(emptyList())

    init {
        getActiveConversations()
    }

    private fun getActiveConversations() {
        viewModelScope.launch {
            try {
                isShowNoData.value = false
                isShowContent.value = false
                isLoading.value = true
                var conversations = mDocService.getConversations("*reason*",
                        "*letterbox*",
                        "*participant*",
                        "",
                        false,
                        "lastMessageDate",
                        "DESC").data.list

                var temp : ArrayList<ListItem> = arrayListOf()

               if(conversations?.size!! <= 2){
                   activeConversations.value = conversations
               }else {
                   for(i in 0..1){
                       temp.add(conversations.get(i))
                   }
                   activeConversations.value = temp
               }

                newMessagesCount.value = conversations.filter { it.isNotSeen() }.size


                isLoading.value = false
                if (activeConversations.value?.size == 0) {
                    isShowContent.value = false
                    isShowNoData.value = true
                } else {
                    isShowContent.value = true
                    isShowNoData.value = false
                }

            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowNoData.value = true
            }
        }
    }
}
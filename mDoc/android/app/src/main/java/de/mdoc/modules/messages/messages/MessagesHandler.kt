package de.mdoc.modules.messages.messages

import android.view.View
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.modules.messages.data.AdditionalMessagesFields
import de.mdoc.pojo.CodingItem

class MessagesHandler {

    fun onAdapterItemClick(view: View, item: CodingItem) {
        val additionalFields = AdditionalMessagesFields(
                letterboxTitle = item.display, letterbox = item.code, recipient = item.system)
        val action = MainNavDirections.globalActionToNewMessage(additionalFields)
        view.findNavController()
            .navigate(action)
    }

    fun goBack(view: View) {
        view.findNavController()
            .popBackStack()
    }
}
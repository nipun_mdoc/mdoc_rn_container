package de.mdoc.modules.dashboard.welcome_widget

import android.content.Context
import android.text.Spanned
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.launch

class WelcomeWidgetViewModel(private val context: Context, private val mDocService: IMdocService): ViewModel() {

    val isInProgress = liveData<Boolean>(false)
    private val isLoadingErrorVisible = liveData<Boolean>(false)
    val welcomeMessage: MutableLiveData<Spanned> = MutableLiveData<Spanned>()
    var greetingsText: MutableLiveData<String> = MutableLiveData("")
    var headImage: MutableLiveData<String> = MutableLiveData("")
    var headOfDepartment: MutableLiveData<String> = MutableLiveData("")
    var departmentDescription: MutableLiveData<String> = MutableLiveData("")
    var shouldShowContent: MutableLiveData<Boolean> = MutableLiveData(false)

    init {
        greetingsText.value = context.resources.getString(R.string.dashboard_welcome_name,
                MdocAppHelper.getInstance().userFirstName,
                MdocAppHelper.getInstance().userLastName)
        loadData()
        shouldShowContent.value=!MdocAppHelper.getInstance().isClosed
    }

    fun loadData() {
        isInProgress.set(true)
        viewModelScope.launch {
            getWelcomeMessage()
            getCurrentClinic()
        }
    }

    private suspend fun getWelcomeMessage() {
        try {
            val result = mDocService.getWelcomeMessage(MdocAppHelper.getInstance().clinicId).data
            welcomeMessage.value = if (!result.isNullOrEmpty()) {
                MdocUtil.fromHtml(result)
            }
            else {
                MdocUtil.fromHtml(context.resources.getString(R.string.dashboard_welcome_message))
            }
        } catch (e: Exception) {
            isInProgress.set(false)
            welcomeMessage.value = MdocUtil.fromHtml(context.resources.getString(R.string.dashboard_welcome_message))
        }
    }

    private suspend fun getCurrentClinic() {
        viewModelScope.launch {
            try {
                val result = mDocService.getCurrentClinic().data
                clinicToWelcomeConvert(result)
                isInProgress.set(false)
            } catch (e: Exception) {
                isInProgress.set(false)
                isLoadingErrorVisible.set(true)
            }
        }
    }

    private fun clinicToWelcomeConvert(result: CurrentClinicData) {
        MdocAppHelper.getInstance().clinicName = result.clinicDetails?.name ?: ""
        result.clinicDetails.departments.forEach { department ->
            if (department.name == MdocAppHelper.getInstance().department) {
                headImage.value = department.headOfDepartment?.image
                headOfDepartment.value = department.headOfDepartment.name
                departmentDescription.value = department.headOfDepartment.description
            }
        }
    }
}
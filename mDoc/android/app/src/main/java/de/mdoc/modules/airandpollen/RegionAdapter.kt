package de.mdoc.modules.airandpollen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import de.mdoc.R
import de.mdoc.modules.airandpollen.data.RegionSelector
import kotlinx.android.synthetic.main.air_and_pollen_region.view.*

class RegionAdapter : BaseAdapter() {

    var items: List<RegionSelector> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(parent.context)
            .inflate(R.layout.air_and_pollen_region, parent, false)
        view.regionName.text = items[position].getName(view.context)
        return view
    }

}
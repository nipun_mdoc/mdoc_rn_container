package de.mdoc.modules.messages.data

data class SharingListItem(val expireTime: Long = 0,
                           val role: String = "",
                           val additionalSharingInfo: AdditionalSharingInfo,
                           val permissions: List<String>?,
                           val clinic: String = "",
                           val department: String = "",
                           val username: String = "")
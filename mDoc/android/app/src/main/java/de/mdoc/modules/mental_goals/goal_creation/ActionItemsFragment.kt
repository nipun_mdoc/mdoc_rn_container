package de.mdoc.modules.mental_goals.goal_creation

import android.os.Bundle
import android.view.View
import androidx.core.view.ViewCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.MentalGoalsUtil.hasEmptyActionItems
import de.mdoc.modules.mental_goals.data_goals.ActionItem
import de.mdoc.modules.mental_goals.mental_goal_adapters.ActionItemsAdapter
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.fragment_action_items.*

class ActionItemsFragment: MdocFragment() {
    lateinit var activity: MdocActivity
    private val adapterItems = arrayListOf<ActionItem>()

    override fun setResourceId(): Int {
        return R.layout.fragment_action_items
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoalsWithHiddenNavigationBar

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupListeners()

        setupAdapter()


        if (MentalGoalsUtil.actionItems.isNullOrEmpty()) {
            txt_start_input?.visibility = View.VISIBLE
            rv_action_items?.visibility = View.GONE
        }
        else {
            txt_start_input?.visibility = View.GONE
            rv_action_items?.visibility = View.VISIBLE
            setupAdapterData()
        }

        progressBar?.progress = 3
    }

    private fun setupAdapter() {
        val actionItemsAdapter = ActionItemsAdapter(activity, adapterItems, false)
        rv_action_items?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        ViewCompat.setNestedScrollingEnabled(rv_action_items, false)
        rv_action_items?.adapter = actionItemsAdapter
    }

    private fun setupListeners() {
        txt_start_input?.setOnClickListener {
            txt_start_input?.visibility = View.GONE
            rv_action_items?.visibility = View.VISIBLE
            setupAdapterData()
        }

        txt_cancel?.setOnClickListener {
            val hasGoalOverview = findNavController().popBackStack(R.id.goalOverviewFragment, false)
            if (!hasGoalOverview) {
                val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
                if (!hasMoreFragment) {
                    val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                    if (!hasDashboardFragment) {
                        findNavController().popBackStack()
                    }
                }
            }
        }

        txt_next?.setOnClickListener {
            if (hasEmptyActionItems(adapterItems)
                || txt_start_input?.visibility == View.VISIBLE) {
                MdocUtil.showToastLong(activity, resources.getString(R.string.fill_all_fields))
            }
            else {
                MentalGoalsUtil.actionItems = ArrayList(adapterItems)
                val action = ActionItemsFragmentDirections.actionActionItemsFragmentToStopAndThinkFragment()
                findNavController().navigate(action)
            }
        }

        txt_back?.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setupAdapterData() {
        adapterItems.clear()

        if (!MentalGoalsUtil.actionItems.isNullOrEmpty()) {
            adapterItems.addAll(MentalGoalsUtil.actionItems ?: emptyList())
        }
        else {
            val adapterItem = ActionItem(null, "")
            adapterItems.addAll(arrayListOf(adapterItem))
        }
        rv_action_items?.adapter?.notifyDataSetChanged()
    }
}
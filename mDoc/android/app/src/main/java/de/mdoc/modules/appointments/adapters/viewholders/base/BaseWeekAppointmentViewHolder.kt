package de.mdoc.modules.appointments.adapters.viewholders.base

import android.content.Context
import android.graphics.Paint
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.constants.MdocConstants.RESCHEDULING_CONFIRMATION
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_appointment.view.*
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.*
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.infoIv
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.txtDescription
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.txtOnlineAppointment
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.txtRoomNumber
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.txtTime
import kotlinx.android.synthetic.main.placeholder_appointment_week.view.txtUpdateMessage

abstract class BaseWeekAppointmentViewHolder(itemView: View) : BaseAppointmentViewHolder(itemView), View.OnClickListener {

    protected val _txtTime: TextView = itemView.txtTime
    private val _separator: LinearLayout = itemView.weekSeparator
    private val _otherAppointments: TextView = itemView.otherAppointments
    protected val _txtUpdateMessage: TextView = itemView.txtUpdateMessage
    private val _detailDayPlaceholder: LinearLayout = itemView.detailDayPlaceholder
    private val _weekItemPlaceholder: CardView = itemView.weekCardLayout
    protected val _txtDescription: TextView = itemView.txtDescription
    private val _txtDayName: TextView = itemView.txtDayName
    private val _txtDayNumber: TextView = itemView.txtDayNumber
    protected val _infoIv: ImageView = itemView.infoIv
    protected val _txtMetaData: TextView = itemView.txtMetaDataStatusWeek
    protected val _txtOnlineAppointment:TextView = itemView.txtOnlineAppointment
    protected val  txtRoomNumber:TextView = itemView.txtRoomNumber

    init {
        _weekItemPlaceholder.setOnClickListener(this)
    }

    fun prepareUI(context: Context) {

        if (MdocUtil.isToday(_item.start)) {
            setTodayAppointment(context)
        } else {
            setOtherAppointments(context)
        }

        val timeAppointment = MdocUtil.getTimeFromMs(_item.start, TIME_FORMAT)
        val titleAppointment = _item.title

        _txtDayName.text = MdocUtil.getTimeFromMs(_item.start, "E")
        _txtDayNumber.text = MdocUtil.getTimeFromMs(_item.start, "dd")

        if (_item.isShowEndTime && _item.end!=null && _item.end.toString().trim()!="" && _item.end != 0.toLong()) {
            val endTime = MdocUtil.getTimeFromMs(_item.end, TIME_FORMAT)
            val timeValue = "$timeAppointment - $endTime"
            _txtTime.text = timeValue
        } else {
            _txtTime.text = timeAppointment
        }

        _txtDescription.text = titleAppointment

        if(_item.isMonthView){
            _detailDayPlaceholder.visibility = if (_item.isFirstAppointment) View.VISIBLE else View.INVISIBLE
        }else{
            _detailDayPlaceholder.visibility = if (_item.isFirstItemInDay) View.VISIBLE else View.INVISIBLE
        }

        _infoIv.visibility = if (_item.comment == null || _item.comment.isEmpty()) View.GONE else View.VISIBLE
        _txtOnlineAppointment.visibility = if (_item.isOnline) View.VISIBLE else View.GONE

        isAppointmentUpdated(context)
        isAppointmentConfirmRequired(_item, context)
        showRoomNumber(_item, context)
    }

    private fun showRoomNumber(item: AppointmentDetails, context: Context){
        txtRoomNumber.text = item.location
        txtRoomNumber.visibility = if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_THERAPY, MdocConstants.WIDGET_TIME_SPAN_DAYS)) View.VISIBLE else View.GONE
    }

    private fun isAppointmentUpdated(context: Context) {
        when {
            _item.isCanceled -> {
                _txtUpdateMessage.visibility = View.GONE
                _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left_canceled)
            }
            isAppointmentUpdated() -> {
                _txtUpdateMessage.visibility = View.VISIBLE
                _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left_highlight)
            }
            else -> {
                _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left)
                _txtUpdateMessage.visibility = View.GONE
            }
        }
    }

    private fun setTodayAppointment(context: Context) {
        _separator.background = ContextCompat.getDrawable(context, R.drawable.rectangle_appointment_left)
        _txtDayName.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        _txtDayNumber.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    }

    private fun setOtherAppointments(context: Context) {
        _txtDayName.setTextColor(ContextCompat.getColor(context, R.color.charocal))
        _txtDayNumber.setTextColor(ContextCompat.getColor(context, R.color.charocal))
    }

    override fun onClick(view: View?) {
        when (view) {
            _weekItemPlaceholder -> {
                showAppointmentDetailsWithTabletCheck(view)
            }
        }
    }

    private fun isAppointmentConfirmRequired(data: AppointmentDetails, context: Context){
        when (data.state) {
            RESCHEDULING_CONFIRMATION -> {
                if (data.isDark) {
                    setConfirmationRequiredAppointmentView(R.drawable.dark_stripe, context)
                } else {
                    setConfirmationRequiredAppointmentView(R.drawable.white_stripe, context)
                }
            }
        }
    }

    private fun setConfirmationRequiredAppointmentView(image: Int, context: Context){
        _txtUpdateMessage.visibility = View.VISIBLE
        _txtUpdateMessage.text = context.resources.getString(R.string.appointment_update_text)
        _separator.background = ContextCompat.getDrawable(context, image)
        _txtTime.paintFlags = _txtTime.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }
}

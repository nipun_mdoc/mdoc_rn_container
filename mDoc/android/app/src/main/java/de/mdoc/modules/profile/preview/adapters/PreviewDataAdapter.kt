package de.mdoc.modules.profile.preview.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.profile.preview.data.Preview
import kotlinx.android.synthetic.main.item_metadata_view.view.*

class PreviewDataAdapter(val data: List<Preview>) : RecyclerView.Adapter<PreviewDataAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_metadata_view, parent, false))
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val txtLabel: TextView = itemView.txtMetaDataLabel
        private val txtValue: TextView = itemView.txtMetaDataViewValue

        private lateinit var item: Preview

        fun bind(item: Preview) {
            this.item = item
            txtLabel.setText(item.name)
            txtValue.text = item.value
        }
    }
}
package de.mdoc.modules.questionnaire.finish

import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.PollVotingScoreResponse
import de.mdoc.network.response.getErrorDetails
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class ScoringResultRepository {

    fun loadScoringResult(pollVotingId: String, resultScreenType: String, listener: (List<VotingScoreResultModel>) -> Unit) {
        MdocManager.getPollVotingScore(
            pollVotingId,
            object : Callback<PollVotingScoreResponse> {
                override fun onResponse(
                    call: Call<PollVotingScoreResponse>,
                    response: Response<PollVotingScoreResponse>
                ) {
                    if (response.isSuccessful) {
                        val scores = response.body()?.data?.medicalScores?.map {
                            VotingScoreResultModel(
                                    label = it.label,
                                    title = it.title,
                                    description = it.description,
                                    score = it.acquiredScoreString,
                                    color = ScoreColor.fromName(it.color),
                                    resultScreenType = resultScreenType
                            )
                        }
                        if (scores != null && scores.isNotEmpty()) {
                            listener.invoke(scores)
                        } else {
                            listener.invoke(emptyList())
                        }
                    } else {
                        Timber.w("getPollVotingScore ${response.getErrorDetails()}")
                    }
                }

                override fun onFailure(call: Call<PollVotingScoreResponse>, t: Throwable) {
                    listener.invoke(emptyList())
                    Timber.w(t, "getPollVotingScore")
                }
            })
    }

}
package de.mdoc.modules.patient_journey.data

data class MediaDetailResponse(val data:MediaDetail)

data class MediaDetail(val id:String?="",val type:String?=null,val publicUrl:String?="")
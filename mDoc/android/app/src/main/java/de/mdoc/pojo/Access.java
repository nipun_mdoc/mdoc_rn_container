package de.mdoc.pojo;

import java.util.ArrayList;

/**
 * Created by ema on 7/25/17.
 */

public class Access {

    private ArrayList<Clinic> clinics;
    private Case activeCase;
    private ArrayList<Case> cases;
    private TenantPublicDocument tenantPublicDocument;

    public TenantPublicDocument getTenantPublicDocument() {
        return tenantPublicDocument;
    }

    public void setTenantPublicDocument(TenantPublicDocument tenantPublicDocument) {
        this.tenantPublicDocument = tenantPublicDocument;
    }

    public ArrayList<Case> getCases() {
        return cases;
    }

    public void setCases(ArrayList<Case> cases) {
        this.cases = cases;
    }

    public Case getActiveCase() {
        return activeCase;
    }

    public void setActiveCase(Case activeCase) {
        this.activeCase = activeCase;
    }

    public ArrayList<Clinic> getClinics() {
        return clinics;
    }

    public void setClinics(ArrayList<Clinic> clinics) {
        this.clinics = clinics;
    }
}

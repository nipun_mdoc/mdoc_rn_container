package de.mdoc.modules.booking.clinic_selection

import android.view.View
import android.widget.TextView
import androidx.navigation.findNavController
import at.blogc.android.views.ExpandableTextView
import de.mdoc.R
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.pojo.ClinicDetails

class ClinicSelectionHandler {

    fun onClinicClicked(view: View, item: ClinicDetails) {

        val booking = BookingAdditionalFields(
                clinicId = item.clinicId,
                specialtyProvider = item.settings?.endpoints?.specialtyProvider,
                specialities = item.specialities)

        val action = ClinicSelectionFragmentDirections.actionToSpecialities(booking)

        view.findNavController().navigate(action)
    }

    fun expandView(view: View) {
        view.rootView.findViewById<ExpandableTextView>(R.id.txtExpand).toggle()
        if(view.rootView.findViewById<ExpandableTextView>(R.id.txtExpand).isExpanded) {
            (view.rootView.findViewById<TextView>(R.id.btnMore)).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_down_dark, 0);
        }else{
            (view.rootView.findViewById<TextView>(R.id.btnMore)).setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.arrow_up_dark, 0);
        }

    }
}
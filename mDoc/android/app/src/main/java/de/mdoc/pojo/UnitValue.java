package de.mdoc.pojo;

public class UnitValue {

    private Value unit;
    private Value value;

    public Value getUnit() {
        return unit;
    }

    public void setUnit(Value unit) {
        this.unit = unit;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}

package de.mdoc.modules.files.add

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.medications.create_plan.data.ListItem
import de.mdoc.network.response.files.ResponseSearchResultsFileStorageDocumentImpl_
import de.mdoc.pojo.CodingData
import de.mdoc.pojo.CodingItem
import de.mdoc.service.IMdocService
import de.mdoc.viewmodel.ValueLiveData
import de.mdoc.viewmodel.liveData
import kotlinx.coroutines.launch
import timber.log.Timber

class AddNewFileViewModel(
    private val repository: FilesRepository,
    private val mDocService: IMdocService
) : ViewModel() {

    val filesUploadCount = liveData<Long>(-1)

    private val _storage = MutableLiveData<ResponseSearchResultsFileStorageDocumentImpl_>().apply {
        viewModelScope.launch {
            try {
                value = mDocService.getFilesStorage()
                val uploadCount = value?.data?.list?.get(0)?.fileUploadsTotalCount?:0
                filesUploadCount.set(uploadCount)

            } catch (e: java.lang.Exception) {
                Timber.d(e)
            }
        }
    }

    val categories = liveData<List<CodingItem>>()
    var storage = _storage


    init {
        repository.getCategoryCoding(::onCodingReceived, ::onError)
    }

    private fun onCodingReceived(data: CodingData) {
        categories.set(data.list)
    }

    fun onError(e: Throwable) {
        Timber.w(e)
    }
}
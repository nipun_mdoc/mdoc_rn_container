package de.mdoc.modules.patient_journey

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.patient_journey.data.CarePlan
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import org.joda.time.DateTime

class PatientJourneyWidgetViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var patientJourney: MutableLiveData<CarePlan?> = MutableLiveData(null)

    init {
        getPatientJourney()
    }

    private fun getPatientJourney() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            try {
                isLoading.value = true
                patientJourney.value = mDocService.getCarePlans(category = PatientJourneyViewModel.QUERY,
                        periodEndGTE = DateTime.now().millis.toString(),
                        limit = 1)
                    .data?.list?.getOrNull(0)
                isShowContent.value = patientJourney.value != null
                isShowNoData.value = patientJourney.value == null
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowNoData.value = true
            }
        }
    }
}
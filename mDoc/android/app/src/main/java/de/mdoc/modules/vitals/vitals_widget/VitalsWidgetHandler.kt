package de.mdoc.modules.vitals.vitals_widget

import android.view.View
import androidx.navigation.findNavController
import de.mdoc.R

class VitalsWidgetHandler {

    fun openVitals(view: View) {
        view.findNavController().navigate(R.id.global_action_vitals)
    }
}
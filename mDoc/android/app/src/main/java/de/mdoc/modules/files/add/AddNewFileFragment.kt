package de.mdoc.modules.files.add

import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.exifinterface.media.ExifInterface
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.loader.FileLoader
import com.jaiselrahman.filepicker.model.MediaFile
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.constants.FILE_NAME
import de.mdoc.constants.FILE_PATH
import de.mdoc.constants.REQUEST_IMAGE_CAPTURE
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.files.Config
import de.mdoc.modules.files.camera.CameraActivity
import de.mdoc.modules.files.data.FileCacheRepository
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.FileUpdate
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.pojo.FileUpdateResponse
import de.mdoc.pojo.getFileTypeFromExtension
import de.mdoc.util.MdocUtil
import de.mdoc.util.getErrorMessage
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.viewModel
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.add_new_file_fragment.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.*
import java.util.*

private const val FILE_REQUEST_CODE = 1882
private const val IMAGE_REQUEST_CODE = 1883


class AddNewFileFragment : MdocFragment() {

    val viewModel by viewModel {
        AddNewFileViewModel(
                FilesRepository(RestClient.getService(), FileCacheRepository),
                RestClient.getService()
        )
    }

    lateinit var activity: MdocActivity

    private val mediaFiles = ArrayList<MediaFile>()
    var takenPictureFile: File? = null
    var selectedPdfFile: File? = null

    override fun setResourceId(): Int {
        return R.layout.add_new_file_fragment
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override val navigationItem: NavigationItem = NavigationItem.FilesAddNewFile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        selectFile.setOnClickListener {
            showPopup()
        }

        viewModel.categories.observe(this) {
            categorySpn?.adapter = SpinnerAdapter(context!!, it)
        }

        viewModel.storage.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Timber.d("Size: %s", it.data?.list?.size)
            val storageSize = it.data?.list?.last()?.storageSize ?: 0
            val takenStorageSize = it.data?.list?.last()?.takenStorageSize ?: 0

            if (takenStorageSize >= storageSize) {
                setErrorMessage(R.string.file_upload_limit_error)
                disableUploadButtons()
            }
        })

        viewModel.filesUploadCount.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            Timber.d("Files upload count: %s", it?.value)

            val filesUploadCount = it?.value ?: 0
            if (filesUploadCount >= Config.MAX_UPLOAD_COUNT && !filesUploadCount.equals(-1)) {
                setErrorMessage(R.string.file_upload_limit_error)
                disableUploadButtons()
            }
        })

        confirmBtn.setOnClickListener(
                object : View.OnClickListener {

                    override fun onClick(p0: View?) {
                        confirmBtn?.isEnabled = false
                        cancelBtn?.isEnabled = false
                        when {
                            photoPaths.size > 0 -> {
                                uploadFile(getRealPathFromURI(context!!, photoPaths[0])!!)
                            }
                            takenPictureFile != null -> {
                                uploadFile(takenPictureFile!!.path)
                            }
                            selectedPdfFile != null -> {
                                uploadFile(selectedPdfFile!!.path)
                            }
                            else -> {
                                MdocUtil.showToastLong(activity, getString(R.string.please_select_a_file))
                                confirmBtn.isEnabled = true
                                cancelBtn?.isEnabled = true
                            }
                        }
                    }
                })

        cancelBtn.setOnClickListener {
           findNavController().popBackStack()
        }
    }
    private var photoPaths: ArrayList<Uri> = ArrayList()

    private fun showPopup() {
        val popup = PopupMenu(context, dummyView)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.select_file_type, popup.menu)
        popup.show()
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.takePhoto -> {
                    mediaFiles.clear()
                    takenPictureFile = null
                    selectedPdfFile = null

                    val intent = Intent(activity, CameraActivity().javaClass)
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)
                }
                R.id.choosePhoto -> {
                    takenPictureFile = null
                    mediaFiles.clear()
                    selectedPdfFile = null

                    FilePickerBuilder.instance
                            .setMaxCount(1)
                            .setSelectedFiles(photoPaths)
                            .setActivityTheme(R.style.AppTheme1)
                            .setActivityTitle("Select a photo")
                            .setImageSizeLimit(30)
                            .setVideoSizeLimit(30)
                            .setSpan(FilePickerConst.SPAN_TYPE.FOLDER_SPAN, 3)
                            .setSpan(FilePickerConst.SPAN_TYPE.DETAIL_SPAN, 4)
                            .enableVideoPicker(false)
                            .enableCameraSupport(false)
                            .showGifs(false)
                            .showFolderView(false)
                            .enableSelectAll(false)
                            .enableImagePicker(true)
                            .withOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED)
                            .pickPhoto(this, IMAGE_REQUEST_CODE);


                }
                R.id.chooseDocument -> {
                    takenPictureFile = null
                    mediaFiles.clear()
                    selectedPdfFile = null

                    val intentPDF = Intent(Intent.ACTION_GET_CONTENT)
                    intentPDF.type = "application/pdf"
                    intentPDF.addCategory(Intent.CATEGORY_OPENABLE)
                    startActivityForResult(intentPDF, FILE_REQUEST_CODE)
                }
                R.id.cancel -> {
                }
            }
            false
        }
    }

    private fun uploadFile(path: String) {
        content.visibility = View.GONE
        progressRl.visibility = View.VISIBLE
        progress.showNow()

        val file = File(path)

        // create RequestBody instance from file
        val requestFile = file
            .asRequestBody("multipart/form-data".toMediaTypeOrNull())

        // MultipartBody.Part is used to send also the actual file name
        val body = MultipartBody.Part.createFormData("file", file.name, requestFile)

        // add another part within the multipart request
        val descriptionString = ""
        val description = descriptionString
            .toRequestBody(okhttp3.MultipartBody.FORM)

        MdocManager.uploadFile(description, body, object : Callback<FileUpdateResponse> {
            override fun onResponse(
                    call: Call<FileUpdateResponse>,
                    response: Response<FileUpdateResponse>
            ) {
                if (response.isSuccessful) {
                    updateFile(response.body()!!.data)
                } else {
                    MdocUtil.showToastLong(activity, getString(R.string.error_upload))
                    content?.visibility = View.VISIBLE
                    progressRl?.visibility = View.GONE
                    progress?.hideNow()
                    confirmBtn?.isEnabled = true
                    cancelBtn?.isEnabled = true
                }
            }

            override fun onFailure(call: Call<FileUpdateResponse>, t: Throwable) {
                MdocUtil.showToastLong(activity, getString(R.string.error_upload))
                content?.visibility = View.VISIBLE
                progressRl?.visibility = View.GONE
                progress?.hideNow()
                confirmBtn?.isEnabled = true
                cancelBtn?.isEnabled = true
            }
        })
    }

    private fun updateFile(request: FileUpdate) {
        if (titleEdt?.text.toString().isEmpty()) {
            if (mediaFiles.size > 0) {
                request.description = mediaFiles.get(0).name
            } else {
                request.description = takenPictureFile?.nameWithoutExtension
            }
        } else {
            request.description = titleEdt?.text.toString()
        }
        val categoryCode =
                viewModel.categories.get().getOrNull(categorySpn.selectedItemPosition)?.code ?: ""
        request.category = categoryCode

        MdocManager.updateFile(request, object : Callback<MdocResponse> {
            override fun onResponse(
                    call: Call<MdocResponse>,
                    response: Response<MdocResponse>
            ) {
                if (response.isSuccessful) {
                    progress?.hideNow()
                    findNavController().popBackStack()

                } else {
                    MdocUtil.showToastShort(activity, response.getErrorMessage())
                    content?.visibility = View.VISIBLE
                    progressRl?.visibility = View.GONE
                    progress?.hideNow()
                    Timber.w("updateFile ${response.getErrorDetails()}")
                }
                confirmBtn?.isEnabled = true
                cancelBtn?.isEnabled = true
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                Timber.w(t, "updateFile")
                MdocUtil.showToastShort(activity, "Error " + t.message)
                content?.visibility = View.VISIBLE
                progressRl?.visibility = View.GONE
                progress?.hideNow()
                confirmBtn?.isEnabled = true
                cancelBtn?.isEnabled = true
            }
        })
    }

    @Throws(IOException::class)
    private fun copyFileUsingStream(inputStream: InputStream, dest: File) {
        var os: OutputStream? = null
        try {
            os = FileOutputStream(dest)
            val buffer = ByteArray(1024)
            var length: Int
            while (inputStream.read(buffer).also { length = it } > 0) {
                os.write(buffer, 0, length)
            }
        } finally {
            inputStream!!.close()
            os!!.close()
        }
    }

    private fun openStreamAndCopyContent(uri: Uri, fileName: String) {
        try {
            selectedPdfFile = File(context!!.cacheDir.absolutePath.toString() + File.separator + fileName)
            val inputStream: InputStream? = context!!.contentResolver.openInputStream(uri)
            if (inputStream != null) {
                copyFileUsingStream(inputStream, selectedPdfFile!!)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try{
            when(requestCode){
                FILE_REQUEST_CODE -> {
                    if (resultCode == Activity.RESULT_OK
                            && data != null
                    ) {
                        mediaFiles.clear()
                        selectedPdfFile = null

                        val uri: Uri? = data.data
                        var displayName: String? = null
                        val uriString = uri.toString()

                        if (uriString.startsWith("content://")) {
                            var cursor: Cursor? = null
                            try {
                                cursor = getActivity()!!.contentResolver.query(uri!!, null, null, null, null)
                                if (cursor != null && cursor.moveToFirst()) {
                                    displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                                }
                            } finally {
                                cursor?.close()
                            }
                        } else if (uriString.startsWith("file://")) {
                            displayName = selectedPdfFile?.name
                        }
                        openStreamAndCopyContent(uri!!, displayName!!)

                        if (selectedPdfFile!!.length() <= Config.MB_30) {
                            fileNameTv.text = selectedPdfFile?.name
                            selectedPdfFile?.totalSpace
                            fileIcon.visibility = View.VISIBLE
                            fileNameTv.visibility = View.VISIBLE
                            fileSizeNotification.visibility = View.VISIBLE
                            fileThumbnail.visibility = View.INVISIBLE
                            val type = getFileTypeFromExtension(selectedPdfFile?.extension)
                            fileIcon.setImageResource(type.imageRes)
                        } else {
                            clearSelectedFilesAndShowErrorMessage()
                        }
                    }

                }
                IMAGE_REQUEST_CODE -> {
                    if (resultCode == Activity.RESULT_OK
                            && data != null
                    ) {
                        try {

                            val dataList: ArrayList<Uri> = data.getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)
                            if (dataList != null) {
                                photoPaths = ArrayList()
//                                val contentResolver: ContentResolver? = context?.contentResolver
//                                mediaFiles.clear()
//                                mediaFiles.add(FileLoader.asMediaFile(contentResolver, dataList[0], Configurations.Builder()
//                                        .setCheckPermission(true)
//                                        .setShowImages(true)
//                                        .setShowVideos(false)
//                                        .enableImageCapture(false)
//                                        .setSelectedMediaFiles(mediaFiles)
//                                        .setMaxSelection(1)
//                                        .setSkipZeroSizeFiles(true)
//                                        .build())!!)
                                photoPaths.addAll(dataList)

                            }


                            if (photoPaths.size == 0) {
                                return
                            }

                            val file = File(getRealPathFromURI(context!!, photoPaths[0]))
                            file.totalSpace
                            fileNameTv.text = file.name
                            fileIcon.visibility = View.VISIBLE
                            fileNameTv.visibility = View.VISIBLE
                            fileSizeNotification.visibility = View.VISIBLE
                            fileThumbnail.visibility = View.INVISIBLE
                            val type = getFileTypeFromExtension(file.extension)
                            when {
                                type.isImage -> {
                                    Glide.with(activity)
                                            .load(photoPaths[0])
                                            .into(fileThumbnail)
                                    fileThumbnail.visibility = View.VISIBLE
                                    fileIcon.visibility = View.INVISIBLE
                                    fileNameTv.visibility = View.INVISIBLE
                                    fileSizeNotification.visibility = View.INVISIBLE
                                }
                                else -> {
                                    fileIcon.setImageResource(type.imageRes)
                                }
                            }

//                            if (mediaFiles.size == 0) {
//                                return
//                            }
//
//                            if (mediaFiles[0].size <= Config.MB_30) {
//                                fileSizeNotification.text = getString(R.string.file_limit)
//                                fileSizeNotification.setTextColor(ContextCompat.getColor(activity, R.color.taupe))
//
//                                val file = File(mediaFiles.get(0).path)
//                                file.totalSpace
//                                fileNameTv.text = file.name
//                                fileIcon.visibility = View.VISIBLE
//                                fileNameTv.visibility = View.VISIBLE
//                                fileSizeNotification.visibility = View.VISIBLE
//                                fileThumbnail.visibility = View.INVISIBLE
//                                val type = getFileTypeFromExtension(file.extension)
//                                when {
//                                    type.isImage -> {
//                                        Glide.with(activity)
//                                                .load(mediaFiles[0].path)
//                                                .into(fileThumbnail)
//                                        fileThumbnail.visibility = View.VISIBLE
//                                        fileIcon.visibility = View.INVISIBLE
//                                        fileNameTv.visibility = View.INVISIBLE
//                                        fileSizeNotification.visibility = View.INVISIBLE
//                                    }
//                                    else -> {
//                                        fileIcon.setImageResource(type.imageRes)
//                                    }
//                                }
//                            } else {
//                                clearSelectedFilesAndShowErrorMessage()
//                            }
                        } catch (e: Exception) {
                        }
                    }
                }
                REQUEST_IMAGE_CAPTURE -> {
                    if (resultCode == Activity.RESULT_OK
                            && data != null
                    ) {
                        takenPictureFile = null

                        //compress image and save rotation
                        var bitmap = BitmapFactory.decodeFile(data.extras?.getString(FILE_PATH))
                        var bos = ByteArrayOutputStream()

                        var oldExif = ExifInterface(data.extras?.getString(FILE_PATH) ?: "")
                        var exifOrientation = oldExif.getAttribute(ExifInterface.TAG_ORIENTATION)

                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80 /*ignored for PNG*/, bos)
                        var bitmapdata = bos.toByteArray()


                        takenPictureFile =
                                File(activity.getExternalFilesDir(null), data.extras?.getString(FILE_NAME))
                        takenPictureFile?.createNewFile()

                        var fos = FileOutputStream(takenPictureFile)
                        fos.write(bitmapdata)
                        fos.flush()
                        fos.close()

                        if (exifOrientation != null) {
                            var newExif = ExifInterface(takenPictureFile?.path ?: "")
                            newExif.setAttribute(ExifInterface.TAG_ORIENTATION, exifOrientation)
                            newExif.saveAttributes()
                        }

                        if (takenPictureFile!!.length() <= Config.MB_30) {
                            fileSizeNotification.text = getString(R.string.file_limit)
                            fileSizeNotification.setTextColor(ContextCompat.getColor(activity, R.color.taupe))

                            takenPictureFile?.totalSpace
                            fileNameTv.text = takenPictureFile?.name
                            fileIcon.visibility = View.VISIBLE
                            fileNameTv.visibility = View.VISIBLE
                            fileSizeNotification.visibility = View.VISIBLE
                            fileThumbnail.visibility = View.INVISIBLE
                            Glide.with(activity)
                                    .load(takenPictureFile?.path)
                                    .into(fileThumbnail)
                            fileThumbnail.visibility = View.VISIBLE
                            fileIcon.visibility = View.INVISIBLE
                            fileNameTv.visibility = View.INVISIBLE
                            fileSizeNotification.visibility = View.INVISIBLE

                        } else {
                            clearSelectedFilesAndShowErrorMessage()
                        }
                    }
                }
            }

        }catch (e: FileNotFoundException){
            Toast.makeText(
                    context,
                    resources.getString(R.string.files_unable_to_upload),
                    Toast.LENGTH_LONG
            )
                .show()


        }
    }

    private fun getRealPathFromURI(context: Context, contentUri: Uri): String? {
        var cursor: Cursor? = null
        return try {
            val proj = arrayOf<String>(MediaStore.Images.Media.DATA)
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null)
            val column_index = cursor?.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor?.moveToFirst()
            cursor?.getString(column_index!!)
        } catch (e: Exception) {
            Log.e("TAG", "getRealPathFromURI Exception : $e")
            ""
        } finally {
            cursor?.close()
        }
    }

    private fun clearSelectedFilesAndShowErrorMessage() {
        mediaFiles.clear()
        selectedPdfFile = null
        takenPictureFile = null
        fileNameTv.text = ""
        fileIcon.setImageResource(R.drawable.folder)
        setErrorMessage(R.string.file_exceed_the_file_size)
    }

    private fun setErrorMessage(res: Int) {
        fileSizeNotification.text = getString(res)
        fileSizeNotification.setTextColor(ContextCompat.getColor(activity, R.color.lust))
        fileSizeNotification.setCompoundDrawablesWithIntrinsicBounds(R.drawable.info_icon, 0, 0, 0)
    }

    private fun disableUploadButtons() {
        selectFile.isEnabled = false
        confirmBtn.isEnabled = false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_add_new_file)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.fragmentFileStorage) {
            val direction = AddNewFileFragmentDirections.actionToFragmentFileStorage()
            findNavController().navigate(direction)
        }
        return super.onOptionsItemSelected(item)
    }
}
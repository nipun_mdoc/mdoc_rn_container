package de.mdoc.modules.booking.confirm_booking

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentBookingConfirmBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_booking_confirm.*

class ConfirmBookingFragment : NewBaseFragment() {
    lateinit var activity: MdocActivity

    private val args: ConfirmBookingFragmentArgs by navArgs()

    private val confirmBookingViewModel by viewModel {
        ConfirmBookingViewModel(
                RestClient.getServiceWithoutClinicId(),
                args.appointment,
                args.bookingAdditionalFields)
    }
    private var onlineTooltipDialog = OnlineTooltipBottomSheetFragment()

    override val navigationItem: NavigationItem = NavigationItem.BookingConfirm

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        val binding: FragmentBookingConfirmBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_booking_confirm, container, false)
        binding.confirmBookingViewModel = confirmBookingViewModel
        binding.physicianName = args.physicianName
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {

            override fun handleOnBackPressed() {
                val isBackToMasterData = findNavController().popBackStack(R.id.fragmentMasterData,false)

                if (!isBackToMasterData) {
                    findNavController().popBackStack()
                }
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

        btn_cancel?.setOnClickListener {
            findNavController().popBackStack()
        }

        btn_book_appointment.setOnClickListener {
            bookAppointment()
        }
        btn_appointment_tooltip.setOnClickListener {
            onlineTooltipDialog.showNow(activity.supportFragmentManager, "")
        }
    }

    private fun bookAppointment() {
        confirmBookingViewModel.bookAppointment(onSuccess = {
            onBookingSuccess()
        }, onError = {
            onBookingFailed(it)
        })
    }

    private fun onBookingSuccess() {
        val action = ConfirmBookingFragmentDirections.actionSuccessBooking(args.appointment, args.bookingAdditionalFields)
        findNavController().navigate(action)
    }

    private fun onBookingFailed(failCode: String?) {
        val action = ConfirmBookingFragmentDirections.actionFailedBooking(failCode ?: "")
        findNavController().navigate(action)
    }
}
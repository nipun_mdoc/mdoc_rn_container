package de.mdoc.modules.files.filter

import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FileListItem
import org.joda.time.LocalDate
import java.io.Serializable

data class FilesFilter(
        val categories: List<CodingItem>?,
        val from: LocalDate?,
        val to: LocalDate?
) : Serializable {

    private val millisFrom = from?.toDateTimeAtStartOfDay()?.millis
    private val millisTo = to?.plusDays(1)?.toDateTimeAtStartOfDay()?.millis

    private val categoryCodes = if (categories == null) null else categories.map { it.code }

    fun isMatch(item: FileListItem): Boolean {
        return (
                categoryCodes == null || (item.category != null && categoryCodes.contains(item.category))
                ) && (
                millisFrom == null || item.cts >= millisFrom
                ) && (
                millisTo == null || item.cts < millisTo
                )
    }

}

fun FilesFilter.getActiveFiltersCount(): Int {
    var size = categories?.size ?: 0

    if (from != null) {
        size++
    }

    if (to != null) {
        size++
    }
    return size
}
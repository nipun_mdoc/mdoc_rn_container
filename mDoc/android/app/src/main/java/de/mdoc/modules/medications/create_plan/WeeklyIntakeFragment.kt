package de.mdoc.modules.medications.create_plan

import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentWeeklyIntakeBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import kotlinx.android.synthetic.main.fragment_weekly_intake.*

class WeeklyIntakeFragment internal constructor(val viewModel: CreatePlanViewModel):
        NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar
    lateinit var activity: MdocActivity
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentWeeklyIntakeBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_weekly_intake, container, false)

        activity = context as MdocActivity
        binding.lifecycleOwner = this
        binding.createPlanViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupWeeks()
    }

    private fun setupWeeks() {
        weekly_group.removeAllViews()
        viewModel.weeklyPeriod.value?.forEach { listItem ->
            val childView =
                    LayoutInflater.from(context)
                        .inflate(R.layout.placeholder_checkbox_item, null)
            val placeholder = childView.findViewById<ConstraintLayout>(R.id.placeholder)
            placeholder.visibility=View.VISIBLE

            val label = childView.findViewById<TextView>(R.id.label)

            placeholder.backgroundTintList = ColorStateList.valueOf(
                    ContextCompat.getColor(activity, listItem.color))

            label?.text = listItem.text as String

            weekly_group?.addView(childView)
            placeholder?.setOnClickListener {
                viewModel.weekSelected(listItem)

                placeholder.backgroundTintList = ColorStateList.valueOf(
                        ContextCompat.getColor(activity, listItem.color))
            }
        }
    }

    companion object {
        val TAG = WeeklyIntakeFragment::class.java.canonicalName
    }
}

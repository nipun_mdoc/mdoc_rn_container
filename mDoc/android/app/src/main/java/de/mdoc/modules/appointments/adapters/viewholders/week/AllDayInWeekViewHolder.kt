package de.mdoc.modules.appointments.adapters.viewholders.week

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import de.mdoc.R
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseAppointmentViewHolder
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_all_day_in_week.view.*

class AllDayInWeekViewHolder(itemView: View) : BaseAppointmentViewHolder(itemView), View.OnClickListener {

    private val description: TextView = itemView.description
    private val allDayLabel: TextView = itemView.allDayLabel
    private val dayLabel: TextView = itemView.dayLabel
    private val dayOfMonth: TextView = itemView.dayOfMonth
    private val dayOfMonthPlaceholder: LinearLayout = itemView.dayOfMonthPlaceholder
    private val allDayPlaceholder = itemView.allDayPlaceholder

    init {
        allDayPlaceholder.setOnClickListener(this)
    }

    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        super.bind(item, context, appointmentCallback, hasHeader)

        if (MdocUtil.isToday(item.start)) {
            setTodayAppointment(context)
        } else {
            setOtherAppointments(context)
        }

        description.text = item.title
        dayOfMonthPlaceholder.visibility = if (item.isFirstItemInDay) View.VISIBLE else View.GONE
        dayLabel.text = MdocUtil.getTimeFromMs(item.start, "E")
        dayOfMonth.text = MdocUtil.getTimeFromMs(item.start, "dd")
        allDayLabel.visibility = if (item.isFirst) View.VISIBLE else View.GONE
    }

    private fun setTodayAppointment(context: Context) {
        dayLabel.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
        dayOfMonth.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
    }

    private fun setOtherAppointments(context: Context) {
        dayLabel.setTextColor(ContextCompat.getColor(context, R.color.charocal))
        dayOfMonth.setTextColor(ContextCompat.getColor(context, R.color.charocal))
    }

    override fun onClick(view: View?) {
       when (view) {
           allDayPlaceholder -> {
               showAppointmentDetailsWithTabletCheck(view)
           }
       }
    }
}
package de.mdoc.modules.profile.changepassword

import androidx.lifecycle.ViewModel
import de.mdoc.viewmodel.liveData

class ChangePasswordViewModel : ViewModel() {

    val isShowPassword = liveData(false)
    val isShowPasswordConfirmation = liveData(false)
    val isInProgress = liveData(false)

}
package de.mdoc.modules.devices.data

import android.content.Context

data class ChartData(var context: Context?=null,
                     var rawData: ObservationResponse?=null,
                     var metrics:List<String>?=null,
                     var period: ObservationPeriodType?=null,
                     var maxDaysInMonth:Int=30,
                     var unit:String="")
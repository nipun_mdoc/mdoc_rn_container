package de.mdoc.modules.my_stay

import androidx.lifecycle.ViewModel
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class MyStayViewModel(
        private val repository: RxStayRepository
) : ViewModel() {
    val isLoading = liveData(false)

    val isShowLoadingError = liveData(false)

    val isShowContent = liveData(false)

    val isNoContent = liveData(false)

    val clinicData = liveData<CurrentClinicData>()


    init {
        isLoading.set(true)
    }


    fun loadData() {
        isLoading.set(true)

        isShowLoadingError.set(false)
        isShowContent.set(false)
        isNoContent.set(false)

    }

    fun getCurrentClinic() {
        isLoading.set(true)
        repository.getCurrentClinic(
                resultListener = ::onDataLoaded,
                errorListener = ::onError)
    }

    private fun onDataLoaded(result: CurrentClinicData) {
        isLoading.set(false)
        clinicData.set(result)
        isShowContent.set(true)
        isNoContent.set(true)
        isShowContent.set(false)

    }

    private fun onError(e: Throwable) {
        Timber.w(e)
        isLoading.set(false)
        isShowLoadingError.set(true)
    }

    override fun onCleared() {
        super.onCleared()
        repository.recycle()
    }
}
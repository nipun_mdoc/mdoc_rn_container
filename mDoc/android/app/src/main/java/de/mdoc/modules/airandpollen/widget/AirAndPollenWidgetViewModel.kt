package de.mdoc.modules.airandpollen.widget

import androidx.lifecycle.ViewModel
import de.mdoc.modules.airandpollen.data.AirAndPollenRepository
import de.mdoc.modules.airandpollen.data.Region
import de.mdoc.util.MdocAppHelper
import de.mdoc.viewmodel.liveData
import timber.log.Timber

class AirAndPollenWidgetViewModel(
    private val mdocAppHelper: MdocAppHelper,
    private val repository: AirAndPollenRepository
) : ViewModel() {

    /** Visibility of progress bar */
    val isInProgress = liveData<Boolean>(false)
    /** Name of currently selected region name */
    val selectedRegionName = liveData<String>("")
    /** Is text with selected region name is visible */
    val isSelectedRegionVisible = liveData<Boolean>(false)
    /** Is show message that selected region is not visible */
    val isNoRegionSelectedMessageVisible = liveData<Boolean>(true)
    /** Is show container with pollen info */
    val isPollenInfoVisible = liveData<Boolean>(false)
    /** I show loading error message */
    val isLoadingErrorVisible = liveData<Boolean>(false)
    /** Is show bottom section which holds progress bar pollen info and loading error message */
    val isBottomSectionVisible = liveData<Boolean>(false)
    /** Pollen data to show */
    val pollenData = liveData<PollenWidgetData?>(null)

    /**
     * Invoke to start data loading
     */
    fun loadData() {
        val regionId: Int? = mdocAppHelper.selectedPollenRegionId
        if (regionId == null) {
            showRegionIsNotSelected()
        } else {
            isInProgress.set(true)
            isSelectedRegionVisible.set(true)
            selectedRegionName.set("-")
            isNoRegionSelectedMessageVisible.set(false)
            isLoadingErrorVisible.set(false)
            isPollenInfoVisible.set(false)
            isBottomSectionVisible.set(true)
            repository.getAirAndPollen(::onLoaded, ::onFailedToLoad)
        }
    }

    private fun onLoaded(result: List<Region>) {
        isInProgress.set(false)
        val regionId: Int? = mdocAppHelper.selectedPollenRegionId
        result.firstOrNull {
            it.id == regionId
        }?.let {
            showData(it)
        } ?: run {
            showRegionIsNotSelected()
        }
    }

    private fun showData(region: Region) {
        selectedRegionName.set(region.name)
        isSelectedRegionVisible.set(true)
        pollenData.set(null)
        isBottomSectionVisible.set(true)
        isPollenInfoVisible.set(true)
        isInProgress.set(false)
        isLoadingErrorVisible.set(false)
        isNoRegionSelectedMessageVisible.set(false)
        pollenData.set(PollenWidgetData(region.today))
    }

    private fun showRegionIsNotSelected() {
        selectedRegionName.set("")
        isSelectedRegionVisible.set(false)
        isBottomSectionVisible.set(false)
        isPollenInfoVisible.set(false)
        isInProgress.set(false)
        isLoadingErrorVisible.set(false)
        isNoRegionSelectedMessageVisible.set(true)
        pollenData.set(null)
    }

    private fun onFailedToLoad(e: Throwable) {
        Timber.w(e)
        isSelectedRegionVisible.set(false)
        selectedRegionName.set("")
        isBottomSectionVisible.set(false)
        isPollenInfoVisible.set(false)
        isInProgress.set(false)
        isLoadingErrorVisible.set(true)
    }

}
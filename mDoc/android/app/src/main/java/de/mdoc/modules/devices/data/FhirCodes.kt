package de.mdoc.modules.devices.data

enum class FhirCodes(
        val code:String
){

    SYSTOLIC(
            code="8480-6"
    ),
    DIASTOLIC(
            code="8462-4"
    ),
    BLOOD_PRESSURE(
            code="35094-2"
    ),
    STEPS(
            code="55423-8"
    ),
    BPM(
            code="8867-4"
    ),
    SPO2(
            code="20564-1"
    )
}

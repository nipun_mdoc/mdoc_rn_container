package de.mdoc.modules.vitals.data

import de.mdoc.data.create_bt_device.ValueQuantity

data class UnitPersistenceResponse(val data:List<ValueQuantity>)
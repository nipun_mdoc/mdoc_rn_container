package de.mdoc.modules.medications.search.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class MedicationsResponse(
        val data: MedicationsData)

data class MedicationsData(
        val totalCount: Int,
        val list: ArrayList<MedicationData>)

@Parcelize
data class MedicationData(
        var code: String = "",
        var cts: Long = 0,
        val description: String = "",
        var form: String = "",
        val id: String = "",
        var image: String = "",
        val imageContentType: String = "",
        var manualEntry: Boolean = false,
        val manufacturer: String = "",
        val manufacturerCode: String = "",
        val compounds: ArrayList<Compound>? = null,
        var prescriptionReasons: ArrayList<String>? = null,
        val size: String = "",
        val unit: String = "",
        var uts: Long = 0): Parcelable{

    fun pznInfo(): String {
        return "PZN - $code"
    }

    fun getCompound(): String {
        return compounds?.getOrNull(0)?.name ?: ""
    }

    fun dosageInfo(): String {
        return when {
            form.isNotEmpty() -> "$form - $size $unit"
            size.isNotEmpty() -> "$size $unit"
            else              -> unit
        }
    }

    fun getPrescriptionReason(): String {
        return prescriptionReasons?.getOrNull(0) ?: ""
    }
}

data class Code(
        val coding: List<Coding>)

@Parcelize
data class Compound(
        val id: String = "",
        val name: String = ""): Parcelable

data class Coding(
        val active: Boolean,
        val code: String,
        val display: String,
        val system: String)
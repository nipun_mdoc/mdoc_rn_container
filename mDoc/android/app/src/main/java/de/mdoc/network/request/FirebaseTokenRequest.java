package de.mdoc.network.request;

/**
 * Created by ema on 7/17/17.
 */

public class FirebaseTokenRequest {

    private String deviceIdentifier;
    private String deviceToken;
    private boolean pushNotificationsEnabled;

    public FirebaseTokenRequest(String deviceIdentifier, String deviceToken, boolean pushNotificationsEnabled) {
        this.deviceIdentifier = deviceIdentifier;
        this.deviceToken = deviceToken;
        this.pushNotificationsEnabled = pushNotificationsEnabled;
    }

    public String getDeviceIdentifier() {
        return deviceIdentifier;
    }

    public void setDeviceIdentifier(String deviceIdentifier) {
        this.deviceIdentifier = deviceIdentifier;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public boolean isPushNotificationsEnabled() {
        return pushNotificationsEnabled;
    }

    public void setPushNotificationsEnabled(boolean pushNotificationsEnabled) {
        this.pushNotificationsEnabled = pushNotificationsEnabled;
    }
}

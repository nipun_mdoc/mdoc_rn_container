package de.mdoc.modules.messages.conversations

import android.view.View
import androidx.navigation.findNavController
import de.mdoc.modules.messages.ConversationsFragmentDirections
import de.mdoc.modules.messages.data.AdditionalMessagesFields
import de.mdoc.modules.messages.data.ListItem

class ConversationsHandler {

    fun onFabClicked(view: View) {
        val action = ConversationsFragmentDirections.actionConversationsFragmentToSelectGroupFragment()
        view.findNavController()
            .navigate(action)
    }

    fun onAdapterItemClick(view: View, item: ListItem, isClosed: Boolean) {
        val additionalFields = AdditionalMessagesFields(isClosedConversation = isClosed,
                conversationId = item.id,
                letterboxTitle = item.getSenderInformation(),
                reasonTitle = item.getReasonText(),
                isNewMessage = item.isNotSeen(),
                isPublic = item.isPublic,
                replyAllowed = item.replyAllowed)
        val action = ConversationsFragmentDirections.actionConversationsFragmentToMessagesFragment(additionalFields)
        view.findNavController()
            .navigate(action)
    }
}
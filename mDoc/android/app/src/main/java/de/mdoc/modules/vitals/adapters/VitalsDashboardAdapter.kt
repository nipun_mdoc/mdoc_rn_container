package de.mdoc.modules.vitals.adapters

import android.content.Context
import android.graphics.BitmapFactory
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.data.create_bt_device.ObservationResponseData
import de.mdoc.modules.devices.DevicesUtil.trimDoubleToDot
import de.mdoc.modules.vitals.FhirConfigurationUtil
import de.mdoc.modules.vitals.FhirConfigurationUtil.getImageByCode
import org.joda.time.format.DateTimeFormat

private val dateFormat = DateTimeFormat.forPattern("dd.MM.yyyy")
private val timeFormat = DateTimeFormat.forPattern("HH:mm")

class VitalsDashboardAdapter(var context: Context):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var observations = emptyList<ObservationResponseData>()

    fun setData(items: List<ObservationResponseData>) {
        observations = items
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return if (viewType == TYPE_COMPOSITE) {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_composite_vital_dashboard,
                        viewGroup,
                        false
                        )
            CompositeDashboardViewHolder(view)
        }
        else {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_vital_dashboard,
                        viewGroup,
                        false
                        )
            DefaultDashboardViewHolder(view)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        val activity = context as MdocActivity
        val item = observations[position]
        val isLastItem = position == (observations.size - 1)
        if (getItemViewType(position) == TYPE_COMPOSITE) {
            (viewHolder as CompositeDashboardViewHolder).bind(item, activity, isLastItem)
        }
        else {
            (viewHolder as DefaultDashboardViewHolder).bind(item, activity, isLastItem)
        }
    }

    override fun getItemCount() = observations.size

    override fun getItemViewType(position: Int): Int {
        return if (observations[position].component.isNotEmpty()) {
            TYPE_COMPOSITE
        }
        else {
            TYPE_DEFAULT
        }
    }

    companion object {
        const val TYPE_DEFAULT = 1
        const val TYPE_COMPOSITE = 2
    }
}

class DefaultDashboardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var txtDate: TextView = itemView.findViewById(R.id.txt_measurement_time)
    var txtValue: TextView = itemView.findViewById(R.id.txt_value)
    var txtMetric: TextView = itemView.findViewById(R.id.txt_metric)
    var imgMeasurement: ImageView = itemView.findViewById(R.id.img_measurement)
    var delimiter: View = itemView.findViewById(R.id.vital_delimiter)

    fun bind(item: ObservationResponseData, activity: MdocActivity, isLastItem: Boolean) {
        txtDate.text = getDateTime(item.effectiveDateTime, activity)

        txtValue.text = trimDoubleToDot(item.valueQuantity?.value.toString())
        txtMetric.text = item.valueQuantity?.display?:""
        val decodedString = Base64.decode(getImageByCode(item.code.loincCode()), Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imgMeasurement.setImageBitmap(decodedByte)

        delimiter.visibility = if (isLastItem) View.GONE else View.VISIBLE
    }
}

class CompositeDashboardViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    var txtDate: TextView = itemView.findViewById(R.id.txt_measurement_time)
    var txtValue: TextView = itemView.findViewById(R.id.txt_value)
    var txtMetric: TextView = itemView.findViewById(R.id.txt_metric)
    var txtMetric2: TextView = itemView.findViewById(R.id.txt_metric2)
    var delimiter: View = itemView.findViewById(R.id.vital_delimiter)
    var txtValue2: TextView = itemView.findViewById(R.id.txt_value2)
    var imgMeasurement: ImageView = itemView.findViewById(R.id.img_measurement)
    fun bind(item: ObservationResponseData, activity: MdocActivity, isLastItem: Boolean) {
        txtDate.text = getDateTime(item.effectiveDateTime, activity)
        val decodedString = Base64.decode(getImageByCode(item.code.loincCode()), Base64.DEFAULT)
        val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        imgMeasurement.setImageBitmap(decodedByte)
        val metric1 = FhirConfigurationUtil.componentMetricLabel(
                item.component.getOrNull(0)?.code?.coding?.getOrNull(0)?.code ?: ""
                                                                )
        val metric2 = FhirConfigurationUtil.componentMetricLabel(
                item.component.getOrNull(1)?.code?.coding?.getOrNull(0)?.code ?: ""
                                                                )

        txtMetric.text = metric1
        txtMetric2.text = metric2

        txtValue.text = trimDoubleToDot(item.component.getOrNull(0)?.valueQuantity?.value.toString())
        txtValue2.text = trimDoubleToDot(item.component.getOrNull(1)?.valueQuantity?.value.toString())


        delimiter.visibility = if (isLastItem) View.GONE else View.VISIBLE
    }
}

private fun getDateTime(effectiveDateTime: Long, activity: MdocActivity): String {
    val date = dateFormat.print(effectiveDateTime)
    val time = timeFormat.print(effectiveDateTime)

    return activity.getString(R.string.vitals_time, date, time)
}
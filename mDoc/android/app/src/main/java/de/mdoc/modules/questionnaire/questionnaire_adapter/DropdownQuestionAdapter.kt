package de.mdoc.modules.questionnaire.questionnaire_adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import de.mdoc.R
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Option

class DropdownQuestionAdapter(val context: Context, var listItemsTxt: List<Option>, var question: ExtendedQuestion): BaseAdapter() {


    private val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View = if (position == 0) {
            mInflater.inflate(R.layout.placeholder_spinner, parent, false)
        }
        else {
            mInflater.inflate(R.layout.spinner_item, parent, false)
        }
        val label: TextView? = view.findViewById(R.id.txtDropDownLabel)
        label?.text = listItemsTxt[position].getValue(question.selectedLanguage)

        return view
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val row = super.getDropDownView(position, convertView, parent)
        if (position == 0) {
            row.visibility = View.GONE
            val tv: TextView? = row?.findViewById(R.id.spinnerPlaceholder)
            tv?.height = 0
        }
        return row
    }

    override fun getItem(position: Int): Any? {
        return listItemsTxt.getOrNull(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }
}

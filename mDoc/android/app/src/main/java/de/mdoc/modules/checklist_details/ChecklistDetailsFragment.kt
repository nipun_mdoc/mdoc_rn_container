package de.mdoc.modules.checklist_details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentChecklistDetailsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.checklist_details.adapters.ChecklistTitleAdapter
import de.mdoc.modules.checklist_details.view_model.ChecklistDetailsViewModel
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_checklist_details.*

class ChecklistDetailsFragment : NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.Checklist

    private val args: ChecklistDetailsFragmentArgs by navArgs()

    private val checklistViewModel by viewModel {
        ChecklistDetailsViewModel(
                RestClient.getService(), args.item?.pollId?:"", args.item?.pollVotingId?:"",args.item?.pollAssignId?:"",     args.item?.pollVotingActionId?:"")
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentChecklistDetailsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_checklist_details, container, false)
        binding.lifecycleOwner = this
        binding.checklistViewModel = checklistViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checklistViewModel.items.observe(viewLifecycleOwner, Observer {
            if(it.size > 0) {
                checkListCategoryTitleRv.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                val adapter = context?.let { _ ->
                    ChecklistTitleAdapter(it, checkListChoiceRv, descriptionTxt, args.item?.pollVotingActionId?:"", args.item?.defaultLanguage?:"en")
                }

                checkListCategoryTitleRv.adapter = adapter
            }
        })
    }
}
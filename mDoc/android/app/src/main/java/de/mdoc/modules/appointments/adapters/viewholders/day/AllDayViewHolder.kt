package de.mdoc.modules.appointments.adapters.viewholders.day

import android.content.Context
import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.appointments.adapters.viewholders.base.BaseAppointmentViewHolder
import de.mdoc.pojo.AppointmentDetails
import kotlinx.android.synthetic.main.placeholder_all_day_appointment.view.*

class AllDayViewHolder(itemView: View) : BaseAppointmentViewHolder(itemView), View.OnClickListener {

    private val description: TextView = itemView.description
    private val allDayLabel: TextView = itemView.allDayLabel
    private val allDayPlaceholder: ConstraintLayout = itemView.allDayPlaceholder

    init {
        allDayPlaceholder.setOnClickListener(this)
    }

    override fun bind(item: AppointmentDetails, context: Context, appointmentCallback: DayAppointmentAdapter.AppointmentDayCallback?, hasHeader: Boolean) {
        super.bind(item, context, appointmentCallback, hasHeader)
        description.text = item.title
        allDayLabel.visibility = if (item.isFirst) View.VISIBLE else View.INVISIBLE
    }

    override fun onClick(view: View?) {
        when (view) {
            allDayPlaceholder -> {
               showAppointmentDetailsWithTabletCheck(view)
            }
        }
    }
}
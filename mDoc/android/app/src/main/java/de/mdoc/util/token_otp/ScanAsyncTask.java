package de.mdoc.util.token_otp;

import android.hardware.Camera;
import android.os.AsyncTask;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.ChecksumException;
import com.google.zxing.FormatException;
import com.google.zxing.LuminanceSource;
import com.google.zxing.NotFoundException;
import com.google.zxing.PlanarYUVLuminanceSource;
import com.google.zxing.Reader;
import com.google.zxing.Result;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeReader;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import timber.log.Timber;

/**
 * Created by AdisMulabdic on 9/19/17.
 */

public class ScanAsyncTask extends AsyncTask<Void, Void, String> implements Camera.PreviewCallback {
    private static class Data {
        public byte[] data;
        Camera.Size   size;
    }

    private final BlockingQueue<Data> mBlockingQueue;
    private final Reader mReader;

    public ScanAsyncTask() {
        mBlockingQueue = new LinkedBlockingQueue<Data>(5);
        mReader = new QRCodeReader();
    }

    @Override
    protected String doInBackground(Void... args) {
        while (true) {
            try {
                Timber.w("do in background");

                Data data = mBlockingQueue.take();
                LuminanceSource ls = new PlanarYUVLuminanceSource(
                        data.data, data.size.width, data.size.height,
                        0, 0, data.size.width, data.size.height, false);
                Result r = mReader.decode(new BinaryBitmap(new HybridBinarizer(ls)));
                Timber.w("result %s", r.getText());
                return r.getText();
            } catch (InterruptedException e) {
                return null;
            } catch (NotFoundException e) {
                Timber.w("exception %s", e.getMessage());
            } catch (ChecksumException e) {
                Timber.w("exception %s", e.getMessage());
            } catch (FormatException e) {
                Timber.w("exception %s", e.getMessage());
            } catch (ArrayIndexOutOfBoundsException e) {
                Timber.w("exception %s", e.getMessage());
            } finally {
                mReader.reset();
            }
        }
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        Data d = new Data();
        d.data = data;
        d.size = camera.getParameters().getPreviewSize();
        mBlockingQueue.offer(d);
    }
}
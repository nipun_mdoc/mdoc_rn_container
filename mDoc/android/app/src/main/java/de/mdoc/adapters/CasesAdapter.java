package de.mdoc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.mdoc.R;
import de.mdoc.pojo.Case;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class CasesAdapter extends BaseAdapter {

    ArrayList<Case> items;
    Context context;

    public CasesAdapter(ArrayList<Case> items, Context context) {
        this.items = items;
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView != null) {
            holder = (ViewHolder) convertView.getTag();
        } else {
            convertView =  LayoutInflater.from(context).inflate(R.layout.case_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }

        Case item = (Case)getItem(position);
//        holder.caseNameTv.setText(item.getClinicId() + "-" + item.getDepartment());

        holder.txtClinicName.setText(item.getClinicName());
        holder.txtAddress.setText(item.getClinicAddress());
        holder.txtDate.setText(setDate(item));
        holder.txtRoom.setText(item.getRoomStation());
        holder.txtDepartment.setText(item.getDepartment());

        return convertView;
    }

    private static String getDate(long milliSeconds)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }
    
    private String setDate(Case item){
        if (item.getCheckOutActualTime() == 0){
            return getDate(item.getCheckInTime()) +" - "+getDate(item.getCheckOutTime());
        }
        return getDate(item.getCheckInTime()) +" - "+getDate(item.getCheckOutActualTime());
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ViewHolder {

        @BindView(R.id.txtClinicName)
        TextView txtClinicName;
        @BindView(R.id.txtAddress)
        TextView txtAddress;
        @BindView(R.id.txtDate)
        TextView txtDate;
        @BindView(R.id.txtRoom)
        TextView txtRoom;
        @BindView(R.id.txtDepartment)
        TextView txtDepartment;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
package de.mdoc.modules.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.security.keystore.UserNotAuthenticatedException
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.PairedDevicesAdapater
import de.mdoc.databinding.FragmentMyProfileBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.profile.adapter.EmergencyContactAdapter
import de.mdoc.modules.profile.changepassword.ChangePasswordDialogFragment
import de.mdoc.modules.profile.data.Contact
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.pojo.PairedDeviceResponse
import de.mdoc.pojo.PairedDevices
import de.mdoc.security.EncryptionServices
import de.mdoc.security.SystemServices
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_my_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.InvalidKeyException

class MyProfileFragment: NewBaseFragment(), ValidatePasswordDialogFragment.PasswordValidatorListener {

    lateinit var activity: MdocActivity
    private val list = ArrayList<PairedDevices>()
    lateinit var pairedDevicesAdapter: PairedDevicesAdapater
    override val navigationItem = NavigationItem.MyProfile
    private val myProfileViewModel by viewModel {
        MyProfileViewModel(
                RestClient.getService())
    }
    private lateinit var systemServices: SystemServices
    private var emergencyContacts: ArrayList<Contact> = arrayListOf()
    private lateinit var encryptionService: EncryptionServices
    private var emergencyContactAdapter: EmergencyContactAdapter? = null
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMyProfileBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_profile, container, false)
        activity = getActivity() as MdocActivity
        encryptionService = EncryptionServices(activity)
        systemServices = SystemServices(activity)
        binding.lifecycleOwner = this
        binding.isOptedIn = MdocAppHelper.getInstance()
            .isOptIn
        if(MdocAppHelper.getInstance().paperPrintInfo == null){
            binding.shouldShowTherapyPrint = false
        }else if(MdocAppHelper.getInstance().paperPrintInfo == "1"){
            binding.isTherapyPrintEnable = true
            binding.shouldShowTherapyPrint = true
        }else{
            binding.isTherapyPrintEnable = false
            binding.shouldShowTherapyPrint = true
        }

        binding.myProfileViewModel = myProfileViewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list.clear()
        changeAddTextState(false, R.color.new_text_grey)
        getUserDevices()

        pariedDevicesRv?.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)

        if (resources.getBoolean(R.bool.has_scan_qr_code)) {
            txtAddDevice?.visibility = View.VISIBLE
            textView3?.visibility = View.VISIBLE
            pariedDevicesRv?.visibility = View.VISIBLE
        }

        myProfileViewModel.getPublicUserDetails(onSuccess = {
            setupContactAdapter()
        }, onError = {})
        setOnClickListeners()

        setOnFocusChangeListeners()

        txtAddContact.setOnClickListener { addEmergencyContact() }
    }

    private fun addEmergencyContact() {
        emergencyContacts.add(Contact())
        val position = emergencyContacts.size.minus(1)
        val action = MyProfileFragmentDirections.actionMyProfileFragmentToEditContactFragment(
                emergencyContacts.toTypedArray(), position, true)
        (context as MdocActivity).findNavController(R.id.navigation_host_fragment)
            .navigate(action)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setOnClickListeners() {
        swBiometricOpt.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    if (!swBiometricOpt.isChecked) {
                        if (SystemServices(activity).canAuthWithBiometric() && !MdocAppHelper.getInstance().isOptIn) {
                            ValidatePasswordDialogFragment.Builder()
                                .setOnClickListener(this)
                                .build()
                                .show(parentFragmentManager, "")
                        }
                        else {
                            Toast.makeText(context, resources.getString(R.string.biometric_err_no_fingerprints),
                                    Toast.LENGTH_LONG)
                                .show()
                        }
                    }
                    else {
                        swBiometricOpt.isChecked = false
                        SystemServices(activity).disableBiometricLogin()
                    }
                }
            }
            return@OnTouchListener true
        })

        swPrintOpt.setOnTouchListener(View.OnTouchListener { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_UP -> {
                    if (!swPrintOpt.isChecked) {
                        myProfileViewModel.updateTherapyPrintInfo("1", onSuccess = {
                            swPrintOpt.isChecked = true
                            MdocAppHelper.getInstance().paperPrintInfo = "1"
                            swPrintOpt?.requestFocus()
                        }, onError = {
                        })

                    } else {
                        myProfileViewModel.updateTherapyPrintInfo("0", onSuccess = {
                            swPrintOpt.isChecked = false
                            MdocAppHelper.getInstance().paperPrintInfo = "0"
                            swPrintOpt?.requestFocus()
                        }, onError = {
                        })

                    }
                }
            }
            return@OnTouchListener true
        })

        txtEditEmergencyInfo.setOnClickListener {
            if (txtEditEmergencyInfo?.text.toString() == activity.resources.getString(R.string.confirm)) {
                txtEmergencyInfo?.setSelection(txtEmergencyInfo?.text.toString().length)
                myProfileViewModel.sendEmergencyInfo(onSuccess = {
                    txtEditEmergencyInfo?.text = activity.resources.getString(R.string.profile_edit)
                    swBiometricOpt?.requestFocus()
                }, onError = {
                })
            }
            else {
                txtEmergencyInfo?.setSelection(txtEmergencyInfo?.text.toString().length)
                txtEmergencyInfo?.isCursorVisible = true
                txtEmergencyInfo?.requestFocus()
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }
        txtEditEmail?.setOnClickListener {
            if (txtEditEmail?.text.toString() == activity.resources.getString(R.string.confirm)) {
                txtEmailProfile?.setSelection(txtEmailProfile?.text.toString().length)
                changeEmail(txtEmailProfile?.text.toString())
            }
            else {
                txtEmailProfile?.setSelection(txtEmailProfile?.text.toString().length)
                txtEmailProfile?.isCursorVisible = true
                txtEmailProfile?.requestFocus()
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }

        txtAddDevice?.setOnClickListener {
            findNavController().navigate(R.id.fragmentAddPairedDevice)
        }

        txtChangePassword?.setOnClickListener {
            ChangePasswordDialogFragment().show(parentFragmentManager, "CHANGE_PASSWORD_DIALOG")
        }

        txtEditPhone.setOnClickListener {
            if (txtEditPhone?.text.toString() == activity.resources.getString(R.string.confirm)) {
                txtPhoneProfile?.setSelection(txtPhoneProfile?.text.toString().length)
                changePhone(txtPhoneProfile?.text.toString())
            }
            else {
                txtPhoneProfile?.setSelection(txtPhoneProfile?.text.toString().length)
                txtPhoneProfile?.isCursorVisible = true
                txtPhoneProfile?.requestFocus()
                activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
            }
        }
    }

    private fun setOnFocusChangeListeners() {
        txtEmailProfile?.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                txtEmailProfile?.isCursorVisible = true
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(txtEmailProfile, InputMethodManager.SHOW_IMPLICIT)
                txtEmailProfile?.addTextChangedListener(object: TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    }

                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                        txtEditEmail?.text = activity.resources.getString(R.string.confirm)
                    }

                    override fun afterTextChanged(editable: Editable) {
                    }
                })
            }
            else {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(txtEmailProfile?.windowToken, 0)
            }
        }
        txtPhoneProfile?.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                txtPhoneProfile?.isCursorVisible = true
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(txtPhoneProfile, InputMethodManager.SHOW_IMPLICIT)
                txtPhoneProfile?.addTextChangedListener(object: TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    }

                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                        txtEditPhone?.text = activity.resources.getString(R.string.confirm)
                    }

                    override fun afterTextChanged(editable: Editable) {
                    }
                })
            }
            else {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(txtPhoneProfile!!.windowToken, 0)
            }
        }
        txtEmergencyInfo?.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                txtEmergencyInfo?.isCursorVisible = true
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(txtEmergencyInfo, InputMethodManager.SHOW_IMPLICIT)
                txtEmergencyInfo?.addTextChangedListener(object: TextWatcher {
                    override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                    }

                    override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                        txtEditEmergencyInfo?.text = activity.resources.getString(R.string.confirm)
                    }

                    override fun afterTextChanged(editable: Editable) {
                    }
                })
            }
            else {
                val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(txtPhoneProfile!!.windowToken, 0)
            }
        }
    }

    private fun changeEmail(newEmail: String) {
        if (isEmailValid(newEmail)) {
            val body = JsonObject()
            body.addProperty(EMAIL, newEmail)
            MdocManager.changeEmailProfile(body, object: Callback<MdocResponse> {
                override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                    if (response.isSuccessful) {
                        Toast.makeText(activity, activity.resources.getString(R.string.email_change_success),
                                Toast.LENGTH_SHORT)
                            .show()
                        MdocAppHelper.getInstance()
                            .publicUserDetailses.email = newEmail
                        txtEditEmail!!.text = activity.resources.getString(R.string.profile_edit)
                        clearFocusOnTextViews()
                        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(txtEmailProfile!!.windowToken, 0)
                    }
                    else {
                        MdocUtil.showToastLong(activity, activity.resources.getString(R.string.email_failed))
                    }
                }

                override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                }
            })
        }
        else {
            Toast.makeText(activity, activity.resources.getString(R.string.valid_email), Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun changePhone(newPhone: String) {
        val body = JsonObject()
        body.addProperty(PHONE, newPhone)
        MdocManager.changePhoneProfile(body, object: Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        Toast.makeText(activity, activity.resources.getString(R.string.phone_change_success),
                                Toast.LENGTH_SHORT)
                            .show()
                        MdocAppHelper.getInstance()
                            .publicUserDetailses.phone = newPhone
                        txtEditPhone?.text = activity.resources.getString(R.string.profile_edit)
                        clearFocusOnTextViews()
                        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(txtPhoneProfile!!.windowToken, 0)
                    }
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
            }
        })
    }

    private fun clearFocusOnTextViews() {
        activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        txtEmailProfile?.clearFocus()
        txtEmailProfile?.isCursorVisible = false
        txtPhoneProfile?.clearFocus()
        txtPhoneProfile?.isCursorVisible = false
        txtEmergencyInfo?.clearFocus()
        txtEmergencyInfo?.isCursorVisible = false
    }

    fun getUserDevices() {
        list.clear()
        clearFocusOnTextViews()
        MdocManager.getAllDevices(object: Callback<PairedDeviceResponse> {
            override fun onResponse(call: Call<PairedDeviceResponse>, response: Response<PairedDeviceResponse>) {
                if (isAdded) {
                    if (response.isSuccessful) {
                        for (i in 0 until if (response.body() != null) response.body()!!.data.size else 0) {
                            val name = response.body()!!.data[i].friendlyName
                            val status = response.body()!!.data[i].active!!
                            val deviceID = response.body()!!.data[i].id
                            list.add(PairedDevices(name, status, deviceID))
                        }

                        pairedDevicesAdapter = PairedDevicesAdapater(activity, list, this@MyProfileFragment)
                        pariedDevicesRv!!.adapter = pairedDevicesAdapter

                        val isElevatedUser = MdocAppHelper.getInstance().hasSecretKey()
                        if ((response.body()?.data.isNullOrEmpty() || response.body()?.data?.get(0)?.limitReached == false) && isElevatedUser) {
                            changeAddTextState(true, R.color.cool_blue)
                        }
                    }
                }
            }

            override fun onFailure(call: Call<PairedDeviceResponse>, t: Throwable) {
                if (isAdded) {
                    changeAddTextState(false, R.color.new_text_grey)
                }
            }
        })
    }

    private fun changeAddTextState(enabled: Boolean, color: Int) {
        txtAddDevice?.isEnabled = enabled
        txtAddDevice?.setTextColor(ContextCompat.getColor(activity, color))
    }

    override fun onDialogButtonClick(password: String) {
        myProfileViewModel.validatePassword(context = context,
                password = password,
                successAction = {
                    try {
                        SystemServices(activity).authenticateFingerprint(successAction = {
                            systemServices.optIn(password)
                            swBiometricOpt.isChecked = true
                        }, cancelAction = {
                        }, context1 = getActivity() as Activity)
                    } catch (e: UserNotAuthenticatedException) {
                        MdocAppHelper.getInstance()
                            .isOptIn = false
                        Toast.makeText(context, "Session expired", Toast.LENGTH_SHORT)
                            .show()
                    } catch (e: InvalidKeyException) {
                        MdocAppHelper.getInstance()
                            .isOptIn = false
                        Toast.makeText(context, "Invalid Key exception", Toast.LENGTH_SHORT)
                            .show()
                    }
                },
                failAction = {
                    Toast.makeText(activity, resources.getString(R.string.biometric_validate_password_failed),
                            Toast.LENGTH_LONG)
                        .show()
                })
    }

    private fun setupContactAdapter() {
        rvEmergencyContacts.layoutManager = LinearLayoutManager(activity)
        emergencyContacts = myProfileViewModel.emergencyContacts.value ?: arrayListOf()
        emergencyContactAdapter = EmergencyContactAdapter(emergencyContacts, activity)
        rvEmergencyContacts.adapter = emergencyContactAdapter
    }

    companion object {
        private const val PHONE = "phone"
        private const val EMAIL = "email"

        private fun isEmailValid(target: CharSequence?): Boolean {
            return if (target == null) {
                false
            }
            else {
                Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches()
            }
        }
    }
}
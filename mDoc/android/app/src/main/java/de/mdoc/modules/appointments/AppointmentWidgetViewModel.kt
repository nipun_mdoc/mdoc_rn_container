package de.mdoc.modules.appointments

import android.content.Context
import android.text.format.DateUtils
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.booking.data.BookingRequest
import de.mdoc.network.RestClient
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.*

class AppointmentWidgetViewModel(val context: Context) : ViewModel() {

    var list:MutableLiveData<List<AppointmentDetails>> = MutableLiveData(emptyList())
    var day: MutableLiveData<String> = MutableLiveData("")
    var dayName: MutableLiveData<String> = MutableLiveData("")
    val nowMilis = System.currentTimeMillis()


    var isLoading = MutableLiveData(true)
    val isShowContent = MutableLiveData(false)
    val isShowNoDataMessage = MutableLiveData(false)


    init {
        viewModelScope.launch {
            try {
                networkCallAsync()
            } catch (e: Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowNoDataMessage.value = true
            }
        }
    }

    suspend fun networkCallAsync() {
//        val apiLimit = context.resources.getInteger(R.integer.appointment_widget_api_limit)
        val apiLimit = MdocAppHelper.getInstance().getSubConfigValues(MdocConstants.MODULE_THERAPY, MdocConstants.WIDGET_API_DISPLAY_LIMIT)
//        val timeSpan = context.resources.getInteger(R.integer.appointment_widget_timespan_days)
        val timeSpan = MdocAppHelper.getInstance().getSubConfigValues(MdocConstants.MODULE_THERAPY, MdocConstants.WIDGET_TIME_SPAN_DAYS)
        isLoading.value = true

        val now: LocalDateTime = LocalDateTime.now()
        val dateFormat: DateTimeFormatter? = DateTimeFormat
                .forPattern("dd.MM.")
        val dayNameFormat: DateTimeFormatter? = DateTimeFormat
                .forPattern("E")
        day.value = dateFormat?.print(now)
        dayName.value = dayNameFormat?.print(now)

        val startOfDay=  DateTime.now().millis
        val endOfDay= DateTime.now().plusDays(timeSpan).millis
        val request= BookingRequest()
        with(request){
            limit=apiLimit
            query=""
            participant=MdocAppHelper.getInstance().username
            from=startOfDay
            to=endOfDay
            isLastSeenUpdate
            clinicIds= listOf()
        }

        val response= RestClient.getService().coroutineSearchAppointments(request).data
        if(response.list?.isNotEmpty()!!)response?.list?.get(0)?.isFirstItem = true
        var adapterItems = response?.list?.map {
            it.appointmentDetails.addAdditionalData(
                    it.id,
                    it.uts,
                    it.cts,
                    it.integrationType,
                    it.encounter,
                    it.metadataNeeded,
                    it.metadataPopulated,
                    it.isConfirmationRequired,
                    it.state,
                    it.newVersionReferenceId,
                    it.isFirstItem, false)
        }?.sortedWith(compareBy({ !it.isAllDay }, { it.start })) ?: listOf()

        if (adapterItems.getOrNull(0)?.isAllDay == true) {
            adapterItems[0].isFirst = true
        }

        run breaker@{
            adapterItems.forEach {
                if (!it.isConfirmationRequired && (it.start >= nowMilis &&  it.end >= nowMilis)) {
                    it.isDark = true
                    return@breaker
                }
            }
        }
        adapterItems = adapterItems.filter { Date().time <= it.start }
        list.value = adapterItems

        isLoading.value = false

        if (list.value.isNullOrEmpty()) {
            isShowNoDataMessage.value = true
            isShowContent.value = false
        } else {
            isShowNoDataMessage.value = false
            isShowContent.value = true
        }

        isLoading.value = false
    }
}
package de.mdoc.modules.profile.patient_details.data

data class InsuranceDataKt(
        val id: String?=null,
        val name: String?=null
                          )
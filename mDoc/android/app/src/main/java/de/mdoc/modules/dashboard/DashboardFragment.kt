package de.mdoc.modules.dashboard

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.observe
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.*
import de.mdoc.activities.login.DateOfBirthDialog
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.activities.navigation.permission.LowPermission
import de.mdoc.constants.MdocConstants
import de.mdoc.databinding.FragmentDashboardBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.airandpollen.data.AirAndPollenRepository
import de.mdoc.modules.airandpollen.widget.AirAndPollenWidgetBinding
import de.mdoc.modules.airandpollen.widget.AirAndPollenWidgetViewModel
import de.mdoc.modules.dashboard.header_widget.HeaderWidgetBinding
import de.mdoc.modules.media.media_adapters.MediaCarouselAdapter
import de.mdoc.modules.media.media_view_model.MediaStatisticsViewModel
import de.mdoc.modules.my_clinic.widget.MyClinicWidgetBinding
import de.mdoc.modules.my_clinic.widget.MyClinicWidgetViewModel
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.modules.questionnaire.widget.QuestionnaireWidgetBinding
import de.mdoc.modules.questionnaire.widget.QuestionnaireWidgetRepository
import de.mdoc.modules.questionnaire.widget.QuestionnaireWidgetViewModel
import de.mdoc.network.RestClient
import de.mdoc.network.managers.MdocManager
import de.mdoc.newlogin.Interactor.LogiActivty
import de.mdoc.pojo.Case
import de.mdoc.pojo.ConfigurationData
import de.mdoc.util.BandwidthCheck
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlinx.android.synthetic.main.layout_appointment_widget.*
import kotlinx.android.synthetic.main.layout_covid_hashtag.*
import kotlinx.android.synthetic.main.layout_meal_plan_widget.*
import kotlinx.android.synthetic.main.layout_media_widget.*
import kotlinx.android.synthetic.main.layout_my_stay.*
import kotlinx.android.synthetic.main.menu_layout.*
import kotlinx.android.synthetic.main.welcome_message.*

/**
 * Created by ema on 4/10/17.
 */
class DashboardFragment: NewBaseFragment(), AirAndPollenWidgetBinding.Callback
        , QuestionnaireWidgetBinding.Callback
        , MyClinicWidgetBinding.Callback {

    lateinit var activity: MdocActivity
    internal var phone: String? = null
    private var caseId: String = ""
    private var tempPatientSecret: String? = null
    private var patientSecret: String? = null
    private val airAndPollenWidgetViewModel by viewModel {
        AirAndPollenWidgetViewModel(
                mdocAppHelper = MdocAppHelper.getInstance(),
                repository = AirAndPollenRepository(RestClient.getService())
                                   )
    }
    private val questionnaireWidgetViewModel by viewModel {
        QuestionnaireWidgetViewModel(
                repository = QuestionnaireWidgetRepository()
                                    )
    }
    private val myClinicWidgetViewModel by viewModel {
        MyClinicWidgetViewModel(
                repository = RxStayRepository(RestClient.getService())
                               )
    }
    private val mediaStatisticsViewModel by viewModel {
        MediaStatisticsViewModel(mDocService = RestClient.getService())
    }
    private val mainActivityViewModel: MainActivityViewModel by activityViewModels()

    private val configViewModel by viewModel {
        MdocAppHelper.getInstance().getConfigurationData()
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity = getActivity() as MdocActivity
        caseId = if(MdocAppHelper.getInstance().caseId == null)  "" else MdocAppHelper.getInstance().caseId
        val binding: FragmentDashboardBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_dashboard, container, false)
        binding.configHandler = configViewModel

        DashboardViewModel(
                app = activity.application,
                mDocService = RestClient.getService(),
                binding = binding,
                resource = resources)

        var txtChangeCase = binding.headerWidget.findViewById<TextView>(R.id.txtChangeCase)
        if(resources.getBoolean(R.bool.has_multi_case)) {
            if(MdocConstants.caseList == null){
                txtChangeCase.visibility=View.GONE
            }else {
                if (MdocConstants.caseList.size <= 1)
                    txtChangeCase.visibility = View.GONE
                else
                    txtChangeCase.visibility = View.VISIBLE
            }
        }else{
            txtChangeCase.visibility = View.GONE
        }


        txtChangeCase.setOnClickListener { openCases() }

        binding.lifecycleOwner = this
        binding.shouldDisplayWidgets = LowPermission(resources).hasPermissionToDisplay()
        return binding.root
    }

    private fun openCases(){

        val intent = Intent(context, CaseActivity::class.java)
        intent.putExtra(MdocConstants.CASES, MdocConstants.caseList)
        startActivity(intent)
        activity.finish()
    }

    override val navigationItem: NavigationItem = NavigationItem.Dashboard

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (LowPermission(resources).hasPermissionToDisplay()) {
            setWidgetBindings()
        }

        mainActivityViewModel.isOpenMenu.observe(viewLifecycleOwner) {
            rv_media_carousel?.invalidate()
            setMediaBindings()
        }

        setLowPermissionWidgetBindings()
        setupOnClickListeners()
        setHashTagIfNeeded()

        if (MdocAppHelper.getInstance().publicUserDetailses.born == 0L && MdocAppHelper.getInstance().isSelfRegister) {
            DateOfBirthDialog().showDateOfBirthDialog(activity, MdocAppHelper.getInstance().userId)
        }
    }

    private fun setHashTagIfNeeded() {
        if (resources.getBoolean(R.bool.has_covid19_hashtag)) {
            val hashTagList = resources.getStringArray(R.array.covid_hashtag_list)
                    .map { it }
                    .toTypedArray()
            hashTagList.forEachIndexed { index, tag ->
                txtHashTag?.append(tag)
                if (index != hashTagList.size - 1) {
                    txtHashTag.append(" \n")
                }
            }
        }
    }

    private fun setLowPermissionWidgetBindings (){
        HeaderWidgetBinding(this, headerWidget)
    }

    private fun setWidgetBindings() {
        if (resources.getBoolean(R.bool.has_air_and_pollen_widget)) {
            AirAndPollenWidgetBinding(this, airAndPollenWidget, airAndPollenWidgetViewModel, this)
        }
        if (resources.getBoolean(R.bool.has_questionnaire_widget)) {
            QuestionnaireWidgetBinding(this, questionnaireLl, questionnaireWidgetViewModel, this)
        }

        if (resources.getBoolean(R.bool.has_my_clinic_widget)) {
            MyClinicWidgetBinding(this, myClinicWidget, myClinicWidgetViewModel, this)
        }
            setMediaBindings()
    }

    private fun setMediaBindings() {
        if (resources.getBoolean(R.bool.has_media_widget)) {
            mediaStatisticsViewModel.mediaStatistics.observe(viewLifecycleOwner, Observer { mediaResponse ->
                if (mediaResponse != null) {
                    val mediaCarouselAdapter = MediaCarouselAdapter(activity, mediaResponse)
                    rv_media_carousel?.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
                    rv_media_carousel?.adapter = mediaCarouselAdapter
                }
            })
        }
    }

    override fun onOpenPollenModule() {
        findNavController().navigate(R.id.navigation_air_and_pollen)
    }

    override fun openAllQuestionnaires() {
        findNavController().navigate(R.id.navigation_questionnaire)
    }

    override fun openMyClinic() {
        findNavController().navigate(R.id.navigation_my_stay)
    }

    private fun setupOnClickListeners() {
        viewChecklistTv?.setOnClickListener {
            findNavController().navigate(R.id.navigation_checklist)
        }

        pairRl?.setOnClickListener {
            if (MdocAppHelper.getInstance().getSubConfigVisibility(MdocConstants.MODULE_PAIR, MdocConstants.ENTER_QR_MANUALLY)) {
                scanQrDialog()
            }
            else {
                callScanActivity()
            }
        }

        viewCalendarTv?.setOnClickListener {
            findNavController().navigate(R.id.appointmentFragment)
        }

        txt_media?.setOnClickListener {
            findNavController().navigate(R.id.navigation_media)
        }

        therapyRl?.setOnClickListener {
            if (resources.getBoolean(R.bool.phone)) {
                findNavController().navigate(R.id.appointmentFragment)
            }
        }

        viewMoreMenuTv?.setOnClickListener {
            openMealPlan()
        }

        mealHeaderRl?.setOnClickListener {
            openMealPlan()
        }

        headerRl?.setOnClickListener {
            findNavController().navigate(R.id.appointmentFragment)
        }
    }

    private fun scanQrDialog() {
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_scan_qr, null)
        builder.setView(view)
        val dialog = builder.show()
        val scanBtn = view.findViewById<Button>(R.id.scanBtn)
        val insertBtn = view.findViewById<Button>(R.id.insertBtn)
        val secretEt = view.findViewById<EditText>(R.id.secretEt)

        scanBtn.setOnClickListener {
            BandwidthCheck.checkBandwidth(resources.getString(R.string.err_bandwidth), view)
            dialog.dismiss()
            callScanActivity()
        }

        insertBtn.setOnClickListener {
            tempPatientSecret = secretEt.text.toString()
            dialog.dismiss()

            confirmSecretDialog()
        }
    }

    private fun confirmSecretDialog() {
        val confirmBuilder = AlertDialog.Builder(activity)
        val inflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.dialog_confirm_secret, null)
        confirmBuilder.setView(view)
        val confirmDialog = confirmBuilder.show()
        val yesBtn = view.findViewById<Button>(R.id.yesBtn)
        val noBtn = view.findViewById<Button>(R.id.noBtn)
        val secretTv = view.findViewById<TextView>(R.id.secretTv)
        secretTv.text = tempPatientSecret

        yesBtn.setOnClickListener {
            patientSecret = tempPatientSecret
            if (!patientSecret.isNullOrEmpty()) {
                MdocAppHelper.getInstance()
                    .secretKey = ("otpauth://totp/Keycloak:"
                        + MdocAppHelper.getInstance().username
                        + "?secret="
                        + patientSecret
                        + "&digits=6&algorithm=SHA1&issuer=Keycloak&period=30")
            }
            confirmDialog.dismiss()
            logout()
        }
        noBtn.setOnClickListener {
            confirmDialog.dismiss()
        }
    }

    private fun logout() {
        MdocManager.logout()
        var loginIntent : Intent
        if(getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
            loginIntent = Intent(activity, LogiActivty::class.java)
        }else{
            loginIntent = Intent(activity, LoginActivity::class.java)
        }
        loginIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(loginIntent)
        activity.finish()
    }

    private fun openMealPlan() {
        if (!resources.getBoolean(R.bool.phone)) {
            if (resources.getBoolean(R.bool.can_user_choose_meal)) {
                activity.findNavController(R.id.navigation_host_fragment).navigate(R.id.fragmentMealPlanTabletNew)
            } else {
                activity.findNavController(R.id.navigation_host_fragment).navigate(R.id.fragmentMealPlanCantChooseTablet)
            }
        } else {
            if (resources.getBoolean(R.bool.can_user_choose_meal)) {
                activity.findNavController(R.id.navigation_host_fragment).navigate(R.id.fragmentMealPlanNew)

            } else {
                activity.findNavController(R.id.navigation_host_fragment).navigate(R.id.fragmentMealPlanCantChoose)
            }
        }
    }

    private fun callScanActivity() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.CAMERA),
                        MY_CAMERA_REQUEST_CODE)
            }
            else {
                startActivity(Intent(activity, ScanActivity::class.java))
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == MY_CAMERA_REQUEST_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startActivity(Intent(activity, ScanActivity::class.java))
            }
            else {
                MdocUtil.showToastLong(activity, "Permissions not granted!")
            }
        }
    }

    override fun onResume() {
        super.onResume()
        activity.parentProgressDialog?.dismiss()
        if (MdocAppHelper.getInstance().secretKey != null) {
            pairRl.visibility = View.GONE
        }
    }

    companion object {
        private const val MY_CAMERA_REQUEST_CODE = 100
    }
}
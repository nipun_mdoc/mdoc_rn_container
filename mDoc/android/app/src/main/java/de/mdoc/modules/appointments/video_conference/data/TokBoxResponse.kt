package de.mdoc.modules.appointments.video_conference.data

data class TokBoxResponse(
        var data: TokBoxData? = null)

data class TokBoxData(var tokboxToken: String? = null)
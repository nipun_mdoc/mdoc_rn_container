package de.mdoc.network.request;

import java.util.ArrayList;

import de.mdoc.pojo.Coverage;
import de.mdoc.pojo.PatientDetailsData;

public class MetaDataPostRequest {

    private PatientDetailsData user;
    private PatientDetailsData professional;
    private ArrayList<Coverage> coverages;
    public String appointmentId;

    public PatientDetailsData getUser() {
        return user;
    }

    public void setUser(PatientDetailsData user) {
        this.user = user;
    }

    public PatientDetailsData getProfessional() {
        return professional;
    }

    public void setProfessional(PatientDetailsData professional) {
        this.professional = professional;
    }

    public ArrayList<Coverage> getCoverages() {
        return coverages;
    }

    public void setCoverages(ArrayList<Coverage> coverages) {
        this.coverages = coverages;
    }
}

package de.mdoc.modules.my_stay

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMyStayBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.my_stay.data.RxStayRepository
import de.mdoc.modules.my_stay.widget.MyStayWidgetViewModel
import de.mdoc.network.RestClient
import de.mdoc.pojo.CurrentClinicData
import de.mdoc.util.MdocUtil
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_my_stay.*
import kotlinx.android.synthetic.main.fragment_my_stay_contact_info.*
import kotlinx.android.synthetic.main.layout_my_stay.*
import android.view.*
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.util.setActionTitle


class MyStayFragment : NewBaseFragment(),MyStayDetailsBinding.Callback {

    lateinit var activity: MdocActivity

    private var currentData:CurrentClinicData?=null
    private val args: MyStayFragmentArgs by navArgs()

    private var myStayDetailsBinding:MyStayDetailsBinding?=null

    private val myStayWidgetViewModel by viewModel {
        MyStayWidgetViewModel(
                RestClient.getService())
    }

    private val myStayViewModel by viewModel {
        MyStayViewModel(
                repository = RxStayRepository(RestClient.getService())
        )
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val binding: FragmentMyStayBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_my_stay, container, false)

        binding.lifecycleOwner=this

        binding.stayViewModel=myStayWidgetViewModel

        activity = getActivity() as MdocActivity

        args.clinic?.let {
            currentData = it
        }

        val isMyStayWidgetEnabled = context?.resources?.getBoolean(R.bool.has_my_stay_widget)?:false
        myStayWidgetViewModel.isShowWidgetInModule.value = (currentData == null) && isMyStayWidgetEnabled

        return binding.root
    }

    override val navigationItem: NavigationItem = NavigationItem.MyClinic

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (resources.getBoolean(R.bool.has_my_stay)) {
            setActionTitle(getString(R.string.my_stay))
        } else {
            setActionTitle(getString(R.string.my_clinic))
        }

        setHasOptionsMenu(true)
        ll_stay_title?.visibility = View.GONE

        if(currentData!=null) {

            myStayViewModel.clinicData.set(currentData!!)
        }else{
            myStayViewModel.getCurrentClinic()
        }

        myStayDetailsBinding =MyStayDetailsBinding(
                this,
                stay_details,
                myStayViewModel,
                this
        )

        email_bg.setOnClickListener {
            val mailer = Intent(Intent.ACTION_SENDTO)
            mailer.type = "text/plain"
            mailer.data = Uri.parse("mailto:"+txt_email.text.toString());
            startActivity(Intent.createChooser(mailer, getString(R.string.send_email)))
        }
    }

    override fun openMap() {
        myStayViewModel.clinicData.doIfSet {
            val direction = MyStayFragmentDirections.actionToFragmentMyStayMap(it.clinicDetails)
            findNavController().navigate(direction)
        }
    }

    override fun makeACall() {
        if(myStayViewModel.clinicData.isSet()){
            MdocUtil.callPhone(activity, myStayViewModel.clinicData.get().clinicDetails.contactDetails?.phone)
        }else{
            MdocUtil.callPhone(activity, "")
        }
    }

    override fun onResume() {
        super.onResume()
        myStayDetailsBinding?.addHandlerCallback()
    }

    override fun onPause() {
        super.onPause()
        myStayDetailsBinding?.removeHandlerCallback()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_my_stay)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.menuItemSearch) {
            val direction = MyStayFragmentDirections.actionToFragmentSearchClinics()
            findNavController().navigate(direction)
        }
        return super.onOptionsItemSelected(item)
    }
}
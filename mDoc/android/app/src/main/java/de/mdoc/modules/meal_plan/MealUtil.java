package de.mdoc.modules.meal_plan;

import android.widget.EditText;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.TimeZone;

import de.mdoc.R;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.MealChoiceRequest;
import de.mdoc.network.request.MealPlanRequest;
import de.mdoc.network.response.InfoResponse;
import de.mdoc.network.response.MealChoiceResponse;
import de.mdoc.network.response.MealPlanResponse;
import de.mdoc.pojo.MealChoice;
import de.mdoc.pojo.MealPlan;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ema on 3/8/18.
 */

public abstract class MealUtil extends MdocFragment {

    abstract void setData();

    static final long ONE_WEEK = 7 * 24 * 60 * 60 * 1000;
    protected ProgressDialog progressDialog;
    protected ArrayList<MealPlan> items = new ArrayList<>();
    protected ArrayList<MealChoice> mealChoices = new ArrayList<>();

    ImageView absoluteIv;
    ImageView yesIv;
    ImageView slightlyYesIv;
    ImageView mediumIv;
    ImageView noIv;
    EditText voteEdt;

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.MealPlan;
    }

    public void onAbsoluteIvClick(){
        absoluteIv.setTag(MdocConstants.SELECTED);
        yesIv.setTag(MdocConstants.NOT_SELECTED);
        slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
        mediumIv.setTag(MdocConstants.NOT_SELECTED);
        noIv.setTag(MdocConstants.NOT_SELECTED);
        setSelection();
    }

    public void onYesIvClick(){
        absoluteIv.setTag(MdocConstants.NOT_SELECTED);
        yesIv.setTag(MdocConstants.SELECTED);
        slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
        mediumIv.setTag(MdocConstants.NOT_SELECTED);
        noIv.setTag(MdocConstants.NOT_SELECTED);
        setSelection();
    }

    public void onSlightlYesIvClick(){
        absoluteIv.setTag(MdocConstants.NOT_SELECTED);
        yesIv.setTag(MdocConstants.NOT_SELECTED);
        slightlyYesIv.setTag(MdocConstants.SELECTED);
        mediumIv.setTag(MdocConstants.NOT_SELECTED);
        noIv.setTag(MdocConstants.NOT_SELECTED);
        setSelection();
    }

    public void onMediumIvClick(){
        absoluteIv.setTag(MdocConstants.NOT_SELECTED);
        yesIv.setTag(MdocConstants.NOT_SELECTED);
        slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
        mediumIv.setTag(MdocConstants.SELECTED);
        noIv.setTag(MdocConstants.NOT_SELECTED);
        setSelection();
    }

    public void onNoIvClick(){
        absoluteIv.setTag(MdocConstants.NOT_SELECTED);
        yesIv.setTag(MdocConstants.NOT_SELECTED);
        slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
        mediumIv.setTag(MdocConstants.NOT_SELECTED);
        noIv.setTag(MdocConstants.SELECTED);
        setSelection();
    }

    protected void setSelection(){
        if(absoluteIv.getTag().equals(MdocConstants.SELECTED)){
            absoluteIv.setImageResource(R.drawable.active_absolut);
        }else{
            absoluteIv.setImageResource(R.drawable.inactive_absolut);
        }
        if(yesIv.getTag().equals(MdocConstants.SELECTED)){
            yesIv.setImageResource(R.drawable.active_yes);
        }else{
            yesIv.setImageResource(R.drawable.inactive_yes);
        }
        if(slightlyYesIv.getTag().equals(MdocConstants.SELECTED)){
            slightlyYesIv.setImageResource(R.drawable.active_slightly_yes);
        }else{
            slightlyYesIv.setImageResource(R.drawable.inactive_slightly_yes);
        }
        if(mediumIv.getTag().equals(MdocConstants.SELECTED)){
            mediumIv.setImageResource(R.drawable.active_medium);
        }else{
            mediumIv.setImageResource(R.drawable.inactive_medium);
        }
        if(noIv.getTag().equals(MdocConstants.SELECTED)){
            noIv.setImageResource(R.drawable.active_no);
        }else{
            noIv.setImageResource(R.drawable.inactive_no);
        }
    }

    protected String getReview(){
        if(slightlyYesIv.getTag().equals(MdocConstants.SELECTED)){
            return MdocConstants.NORMAL;
        }else if(mediumIv.getTag().equals(MdocConstants.SELECTED)){
            return MdocConstants.POOR;
        }else if(yesIv.getTag().equals(MdocConstants.SELECTED)){
            return MdocConstants.EXCELLENT;
        }else if(noIv.getTag().equals(MdocConstants.SELECTED)){
            return MdocConstants.VERY_POOR;
        }else if(absoluteIv.getTag().equals(MdocConstants.SELECTED)){
            return MdocConstants.ABSOLUTE;
        } else{
            return null;
        }
    }

    protected void getMealInfo(){
        progressDialog.show();

        MdocManager.getMealInfo(new Callback<InfoResponse>() {
            @Override
            public void onResponse(Call<InfoResponse> call, Response<InfoResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        if (response.body().getData().getConfigurationDetailsMultiValue() != null) {
                            MdocAppHelper.getInstance().setAllergens(response.body().getData().getConfigurationDetailsMultiValue().getALLERGENS());
                            MdocAppHelper.getInstance().setAdditives(response.body().getData().getConfigurationDetailsMultiValue().getADDITIVES());
                        }
                    }
                }
                getMealPlan();
            }

            @Override
            public void onFailure(Call<InfoResponse> call, Throwable throwable) {
                getMealPlan();
            }
        });
    }


    protected void getMealPlan(){

        final MealPlanRequest request = new MealPlanRequest();
        request.setClinicId(MdocAppHelper.getInstance().getClinicId());
        request.setDaysAfter(20);
        request.setStandardOffersOnly(!MdocAppHelper.getInstance().isPremiumPatient());
        request.setTimeZone(TimeZone.getDefault().getID());

        Calendar c = Calendar.getInstance();
        c.setFirstDayOfWeek(Calendar.MONDAY);
        c.setTime(new Date());
        int today = c.get(Calendar.DAY_OF_WEEK);
        c.add(Calendar.DAY_OF_WEEK, -today+Calendar.MONDAY);
        request.setFromDate(c.getTimeInMillis() - ONE_WEEK);

        MdocManager.getMealPlan(request, new Callback<MealPlanResponse>() {
            @Override
            public void onResponse(Call<MealPlanResponse> call, Response<MealPlanResponse> response) {
                if (isAdded()) {
                    if (response.isSuccessful()) {
                        items.clear();
                        items.addAll(response.body().getData());
                        Collections.sort(items, new Comparator<MealPlan>() {
                            @Override
                            public int compare(MealPlan o1, MealPlan o2) {
                                return Long.valueOf(o1.getDateNumber()).compareTo(Long.valueOf(o2.getDateNumber()));
                            }
                        });

                        getChoises();
                    } else {
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<MealPlanResponse> call, Throwable throwable) {
                if(isAdded()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

    protected void getChoises() {
        ArrayList<String> ids = new ArrayList<String>();

        for (int i = 0; i < items.size(); i++) {
            ids.add(items.get(i).getMealPlanId());
        }
        getMealChoices(ids);
    }


    private void getMealChoices(final ArrayList<String> mealPlanIds){

        final MealChoiceRequest request = new MealChoiceRequest();
        request.setCaseId(MdocAppHelper.getInstance().getCaseId());
        request.setMealPlanIds(mealPlanIds);

        MdocManager.getMealChoice(request, new Callback<MealChoiceResponse>() {
            @Override
            public void onResponse(Call<MealChoiceResponse> call, Response<MealChoiceResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        mealChoices.clear();
                        if (response.body().getData().size() > 0) {
                            mealChoices.addAll(response.body().getData());

                            for (int i = 0; i < items.size(); i++) {
                                for (int j = 0; j < mealChoices.size(); j++) {
                                    for (int k = 0; k < items.get(i).getOffers().size(); k++) {
                                        if (mealChoices.get(j).getMealId() != null && mealChoices.get(j).getMealId().equals(items.get(i).getOffers().get(k).get_id())) {
                                            items.get(i).setMealChoice(mealChoices.get(j));
                                            items.get(i).getOffers().get(k).setSelected(true);
                                            if (mealChoices.get(j).getReview() != null && !mealChoices.get(j).getReview().isEmpty()) {
                                                items.get(i).getOffers().get(k).setRated(true);
                                            } else {
                                                items.get(i).getOffers().get(k).setRated(false);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        setData();
                    }
                    progressDialog.dismiss();
                }

            }
            @Override
            public void onFailure(Call<MealChoiceResponse> call, Throwable throwable) {
                if(isAdded()) {
                    progressDialog.dismiss();
                }
            }
        });
    }
}

package de.mdoc.modules.media.view

import android.content.Context
import android.os.AsyncTask
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle
import com.github.barteksc.pdfviewer.util.FitPolicy
import com.shockwave.pdfium.PdfDocument
import de.mdoc.R
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.view_media_pdf.view.*
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.lang.ref.WeakReference
import java.net.HttpURLConnection
import java.net.URL

class MediaPdfView(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs), OnLoadCompleteListener {

    init {
        inflate(context, R.layout.view_media_pdf, this)
    }

    fun load (url: String, previewOnly: Boolean) {
        if (url.isNotEmpty()) {
            val downloadTask = DownloadTask(previewOnly, WeakReference(this))
            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url)
        }
    }

    override fun loadComplete(nbPages: Int) {
        printBookmarksTree(pdfViewer!!.tableOfContents, "-")
    }

    private fun printBookmarksTree(tree: List<PdfDocument.Bookmark>, sep: String) {
        for (b in tree) {
            Timber.e("%s %s, p %d", sep, b.title, b.pageIdx)

            if (b.hasChildren()) {
                printBookmarksTree(b.children, "$sep-")
            }
        }
    }

    private class DownloadTask(val previewOnly: Boolean, val view: WeakReference<MediaPdfView>): AsyncTask<String, Int, ByteArray>() {

        override fun doInBackground(vararg sUrl: String): ByteArray? {
            var input: InputStream? = null
            val output: OutputStream? = null
            var connection: HttpURLConnection? = null
            val out = ByteArrayOutputStream()

            try {
                val url = URL(sUrl[0])
                val userAuth = MdocAppHelper.getInstance().accessToken
                connection = url.openConnection() as HttpURLConnection
                connection.setRequestProperty("Authorization", userAuth)
                connection.connect()
                val fileLength = connection.contentLength
                // download the file
                input = connection.inputStream
                val data = ByteArray(4096)
                var total: Long = 0
                var count: Int

                while (input!!.read(data)
                                .let {
                                    count = it; it != -1
                                }) {
                    if (isCancelled) {
                        input.close()
                        return null
                    }
                    total += count.toLong()
                    out.write(data, 0, count)
                    // publishing the progress....
                    if (fileLength > 0)
                    // only if total length is known
                        Timber.v("test %s", (total * 100 / fileLength).toInt())
                    publishProgress((total * 100 / fileLength).toInt())
                }
            } catch (e: Exception) {
                //            return e.toString();
            } finally {
                try {
                    output?.close()
                    input?.close()
                } catch (ignored: IOException) {
                }

                connection?.disconnect()
            }
            return out.toByteArray()
        }

        override fun onPostExecute(bytes: ByteArray) {
            super.onPostExecute(bytes)
            if (view.get() != null) {
                if (previewOnly) {
                    view.get()?.pdfViewer?.fromBytes(bytes)
                            ?.enableAnnotationRendering(true)
                            ?.spacing(5)
                            ?.enableAntialiasing(false)
                            ?.pages(0) // Load only first page
                            ?.pageFitPolicy(FitPolicy.WIDTH)
                            ?.load()
                }
                else {
                    view.get()?.pdfViewer?.fromBytes(bytes)
                            ?.enableAnnotationRendering(true)
                            ?.scrollHandle(DefaultScrollHandle(view.get()?.context))
                            ?.spacing(5)
                            ?.pageFitPolicy(FitPolicy.WIDTH)
                            ?.load()
                }

                view.get()?.progress?.visibility = View.GONE
            }
        }
    }
}
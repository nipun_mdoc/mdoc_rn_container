package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by ema on 3/20/17.
 */

public class AgendaDetails implements Serializable {

    private String _id;
    private ArrayList<Agenda> agendaDe;
    private ArrayList<Agenda> agendaEn;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<Agenda> getAgendaDe() {
        return agendaDe;
    }

    public void setAgendaDe(ArrayList<Agenda> agendaDe) {
        this.agendaDe = agendaDe;
    }

    public ArrayList<Agenda> getAgendaEn() {
        return agendaEn;
    }

    public void setAgendaEn(ArrayList<Agenda> agendaEn) {
        this.agendaEn = agendaEn;
    }
}

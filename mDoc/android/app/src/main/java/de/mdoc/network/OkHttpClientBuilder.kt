package de.mdoc.network

import de.mdoc.BuildConfig
import de.mdoc.network.interceptors.ErrorMessageInterceptor
import de.mdoc.network.interceptors.HeaderInterceptor
import de.mdoc.network.interceptors.HttpLoggingInterceptor
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

fun buildOkHttpClient(additionalInitializer: OkHttpClient.Builder.() -> Unit = {}, shouldAdd: Boolean = true): OkHttpClient {
    val builder = OkHttpClient().newBuilder()
    builder.connectTimeout(30, TimeUnit.SECONDS)
    builder.writeTimeout(30, TimeUnit.SECONDS)
    builder.readTimeout(30, TimeUnit.SECONDS)
    builder.addInterceptor(HeaderInterceptor(shouldAdd))
    builder.addInterceptor(ErrorMessageInterceptor())
    if (BuildConfig.DEBUG) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(interceptor)
    }
    builder.additionalInitializer()
    return builder.build()
}

fun buildOkHttpClient(additionalInitializer: OkHttpClient.Builder.() -> Unit = {}): OkHttpClient {
    val builder = OkHttpClient().newBuilder()
    builder.connectTimeout(30, TimeUnit.SECONDS)
    builder.writeTimeout(30, TimeUnit.SECONDS)
    builder.readTimeout(30, TimeUnit.SECONDS)
    builder.addInterceptor(HeaderInterceptor())
    builder.addInterceptor(ErrorMessageInterceptor())
    if (BuildConfig.DEBUG) {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        builder.addInterceptor(interceptor)
    }
    builder.additionalInitializer()
    return builder.build()
}
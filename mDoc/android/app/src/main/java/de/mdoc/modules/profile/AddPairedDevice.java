package de.mdoc.modules.profile;

import android.os.Bundle;
import android.webkit.WebView;

import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocBaseFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.pojo.CreateUserDeviceResponse;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.ProgressDialog;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;


public class AddPairedDevice extends MdocBaseFragment {

    MdocActivity activity;

    @BindView(R.id.webView)
    WebView webView;
    private ProgressDialog progressDialog;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_add_paired_device;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        progressDialog = new ProgressDialog(activity);
        progressDialog.setCancelable(false);
        progressDialog.show();
        getData();
    }

    private void getData() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        String deviceName = "Activated: " + sdf.format(date);//android.os.Build.MANUFACTURER + " " + android.os.Build.MODEL;
        JsonObject body = new JsonObject();
        body.addProperty(MdocConstants.USERNAME, MdocAppHelper.getInstance().getUsername());
        body.addProperty(MdocConstants.USER_DEVICE_ID, MdocAppHelper.getInstance().getUserId());
        body.addProperty(MdocConstants.FRIENDLY_NAME, deviceName);

        MdocManager.createUserDevice(body, new Callback<CreateUserDeviceResponse>() {
            @Override
            public void onResponse(Call<CreateUserDeviceResponse> call, Response<CreateUserDeviceResponse> response) {
                if(isAdded()) {
                    if (response.isSuccessful()) {
                        if (response.body().getData().getQrCode() != null) {
                            webView.loadData(response.body().getData().getQrCode(), "text/html", "UTF-8");
                            progressDialog.dismiss();
                        }
                    } else {
                        Timber.w("createUserDevice %s", APIErrorKt.getErrorDetails(response));
                        progressDialog.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(Call<CreateUserDeviceResponse> call, Throwable t) {
                Timber.w(t, "createUserDevice");
                if(isAdded()) {
                    progressDialog.dismiss();
                }
            }
        });
    }

}

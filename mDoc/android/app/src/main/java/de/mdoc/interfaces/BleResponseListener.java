package de.mdoc.interfaces;

import android.bluetooth.BluetoothGatt;

import java.util.ArrayList;

public interface BleResponseListener {
    void dataFromDeviceCallback(ArrayList<Byte> list, BluetoothGatt gatt);
}

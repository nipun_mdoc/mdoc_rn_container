package de.mdoc.modules.mental_goals.data_goals

data class GamificationResponse(
        val data:Map<String,Int>)

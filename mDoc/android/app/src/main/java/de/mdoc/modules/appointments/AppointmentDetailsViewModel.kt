package de.mdoc.modules.appointments

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.network.request.CodingRequest
import de.mdoc.network.response.CodingResponse
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.pojo.NewAppointmentSchedule
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class AppointmentDetailsViewModel(private val mDocService: IMdocService,
                                  private val additionalFields: BookingAdditionalFields?,
                                  private val appointmentDetails: AppointmentDetails?): ViewModel() {


    fun getSpecialities(onSuccess: (String?, String?, String?, String?) -> Unit, onError: () -> Unit) {
        viewModelScope.launch {
            try {
                val response = mDocService.getSpecialities(additionalFields?.specialtyProvider ?: "")
                val result = response.getSpecialtyDataByServiceId(additionalFields?.specialityName)
                val description = result.findDescriptionByServiceId(additionalFields?.specialityName)
                val appointmentType = appointmentDetails?.specialty
                val specialtyCode = result.code
                onSuccess(result.specialty, description, specialtyCode, appointmentType)
            } catch (e: java.lang.Exception) {
                onError()
            }
        }
    }

    fun getUpdatedAppointment(onSuccess: (NewAppointmentSchedule) -> Unit, onError: () -> Unit){
        viewModelScope.launch {
            try {
                val response = mDocService.getReschedulingAppointmentId(appointmentDetails?.appointmentId?: "")
                val result = response.data
                onSuccess(result)
            } catch (e: java.lang.Exception) {
                onError()
            }
        }
    }

    fun rescheduleAppointment(rescheduledAppointment: NewAppointmentSchedule, onSuccess: (NewAppointmentSchedule) -> Unit, onError: () -> Unit){
        viewModelScope.launch {
            try {
                val response = mDocService.confirmRescheduling(appointmentDetails?.appointmentId?: "", rescheduledAppointment).data
                onSuccess(response)
            } catch (e: java.lang.Exception) {
                onError()
            }
        }
    }

    fun getAppointmentDescriptionFromCoding(onSuccess: (CodingResponse?) -> Unit, onError: () -> Unit) {
        viewModelScope.launch {
            try {
                val response = mDocService.getCodingSuspend(
                        CodingRequest("https://mdoc.one/coding/metadata_triggers_notification"))
                onSuccess(response)
            } catch (e: java.lang.Exception) {
                onError()
            }
        }
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

/**
 * Created by ema on 4/19/17.
 */

public class NextDayMealOffer implements Serializable {

    private String description;
    private boolean glutenFree;
    private boolean kosher;
    private boolean vegetarian;
    private boolean halal;
    private String name;
    private String mealOfferId;
    private boolean reviewed;
    private boolean selected;
    private String desert;
    private int calories;
    private boolean pig;
    private boolean premium;
    private boolean cow;
    private boolean chicken;
    private boolean clickable;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isGlutenFree() {
        return glutenFree;
    }

    public void setGlutenFree(boolean glutenFree) {
        this.glutenFree = glutenFree;
    }

    public boolean isKosher() {
        return kosher;
    }

    public void setKosher(boolean kosher) {
        this.kosher = kosher;
    }

    public boolean isVegetarian() {
        return vegetarian;
    }

    public void setVegetarian(boolean vegetarian) {
        this.vegetarian = vegetarian;
    }

    public boolean isHalal() {
        return halal;
    }

    public void setHalal(boolean halal) {
        this.halal = halal;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMealOfferId() {
        return mealOfferId;
    }

    public void setMealOfferId(String mealOfferId) {
        this.mealOfferId = mealOfferId;
    }

    public boolean isReviewed() {
        return reviewed;
    }

    public void setReviewed(boolean reviewed) {
        this.reviewed = reviewed;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDesert() {
        return desert;
    }

    public void setDesert(String desert) {
        this.desert = desert;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public boolean isPig() {
        return pig;
    }

    public void setPig(boolean pig) {
        this.pig = pig;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public boolean isCow() {
        return cow;
    }

    public void setCow(boolean cow) {
        this.cow = cow;
    }

    public boolean isChicken() {
        return chicken;
    }

    public void setChicken(boolean chicken) {
        this.chicken = chicken;
    }

    public boolean isClickable() {
        return clickable;
    }

    public void setClickable(boolean clickable) {
        this.clickable = clickable;
    }
}

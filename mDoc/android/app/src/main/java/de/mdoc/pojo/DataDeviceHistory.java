package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by AdisMulabdic on 9/8/17.
 */

public class DataDeviceHistory {

    @SerializedName("activity")
    @Expose
    private ArrayList<Boolean> activity = null;

    public ArrayList<Boolean> getActivity() {
        return activity;
    }

    public void setActivity(ArrayList<Boolean> activity) {
        this.activity = activity;
    }
}

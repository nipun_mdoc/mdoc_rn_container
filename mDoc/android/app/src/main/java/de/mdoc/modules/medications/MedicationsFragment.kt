package de.mdoc.modules.medications

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.databinding.FragmentMedicationsBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.ListItemTypes
import de.mdoc.network.RestClient
import de.mdoc.viewmodel.viewModel

class MedicationsFragment: NewBaseFragment() {
    private val medicationsViewModel by viewModel {
        MedicationsViewModel(RestClient.getService())
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentMedicationsBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_medications, container, false)

        binding.lifecycleOwner = this
        binding.medicationsViewModel = medicationsViewModel
        binding.medicationsHandler = MedicationsHandler(medicationsViewModel)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        medicationsViewModel.getMedicationAdministration()
    }

    override val navigationItem: NavigationItem = NavigationItem.EMPTY
}
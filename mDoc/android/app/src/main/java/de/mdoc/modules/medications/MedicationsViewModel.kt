package de.mdoc.modules.medications

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.modules.medications.create_plan.data.MedicationAdapterData
import de.mdoc.modules.medications.data.*
import de.mdoc.service.IMdocService
import de.mdoc.util.DaysInWeek
import de.mdoc.util.MdocAppHelper
import kotlinx.coroutines.launch
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import timber.log.Timber
import java.util.*

class MedicationsViewModel(private val mDocService: IMdocService): ViewModel() {

    val onDemandEvents  = MutableLiveData<MedicationAdministrationResponse>()

    private val dateFormatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.ms'Z'")
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var medicationAdministration: MutableLiveData<MedicationAdministration> = MutableLiveData(
            MedicationAdministration())
    var isShowImage = MutableLiveData(false)
    val isMedicationDeleted = MutableLiveData(false)
    var imageContent: MutableLiveData<String> = MutableLiveData("")
    var content: MutableLiveData<List<MedicationAdapterData>> =
            MutableLiveData(emptyList())

    fun getOnDemandEvents () {
        viewModelScope.launch {
            try {
                // Indicators
                startRequestIndicators()

                // Request
                val response = mDocService.getMedicationOnDemandEvents()

                // Filter all object without event information
                val filtered = response.data?.list?.filter { it -> !it.dosageInstruction?.get(0)?.timing?.event.isNullOrEmpty()}
                response.data?.list = filtered

                // Set data to notify observer
                onDemandEvents.value = response

                // Check if data is valid
                isLoading.value = false
                onDemandEvents.value.let {
                    if (it?.data?.list.isNullOrEmpty()) {
                        isShowNoData.value = true
                        isShowContent.value = false
                    }
                    else {
                        isShowNoData.value = false
                        isShowContent.value = true
                    }
                }
            } catch (ex: Exception) {
                onError(ex)
            }
        }
    }

    fun getMedicationAdministration() {
        viewModelScope.launch {
            try {
                medicationAdministrationAsync()
            } catch (e: java.lang.Exception) {
                onError(e)
            }
        }
    }

    private suspend fun medicationAdministrationAsync() {
        startRequestIndicators()

        val today = formatter.print(DateTime.now())
        val tomorrow = formatter.print(DateTime.now().plusHours(24))

        val response = mDocService.getMedicationAdministration(effectivePeriodStartGTE = today,
                effectivePeriodStartLT = tomorrow,
                subject = MdocAppHelper.getInstance().userId)


        content.value = response.prepareAdapterData()

        isLoading.value = false

        if (content.value.isNullOrEmpty()) {
            isShowNoData.value = true
            isShowContent.value = false
        } else {
            isShowNoData.value = false
            isShowContent.value = true
        }
    }

    fun getMedicationImage(pznCode: String) {
        isLoading.value = true
        viewModelScope.launch {
            try {
                imageContent.value = mDocService.getMedicationImage(pznCode)
                    .data
                isLoading.value = false
                isShowImage.value = true
            } catch (e: java.lang.Exception) {
                onError(e)
            }
        }
    }

    fun deleteMedicationRequest(requestId: String?) {
        viewModelScope.launch {
            try {
                deleteMedicationRequestAsync(requestId)
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowContent.value = false
                isShowLoadingError.value = true
                isShowNoData.value = false
                isMedicationDeleted.value = false
            }
        }
    }

    private suspend fun deleteMedicationRequestAsync(requestId: String?) {
        isShowNoData.value = false
        isShowLoadingError.value = false

        mDocService.deleteMedicationRequest(requestId?:"")

        isMedicationDeleted.value = true
    }

    fun howOften(context: Context): String {
        val intake =
                medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
                    ?.timing?.repeat?.periodUnit ?: "WK"

        return if (intake == "WK") {
            context.resources.getString(R.string.med_detail_weekly_on)
        }
        else {
            ""
        }
    }

    fun dayOfWeek(context: Context): String {
        val daysList =
                medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
                    ?.timing?.repeat?.dayOfWeek
        var days = ""
        daysList?.forEachIndexed { index, item ->
            val day = DaysInWeek.valueOf(item ?: "")
            val dayLocalized = day.getName(context)
            val and = context.resources.getString(R.string.med_plan_and)
            days += if (index == daysList.size - 1 && daysList.size > 1) {
                " $and $dayLocalized"
            }
            else if (index == 0) {
                "$dayLocalized"
            }
            else {
                ", $dayLocalized"
            }
        }

        return days
    }

    fun periodStart(): String {
        val date =
                medicationAdministration.value?.medicationRequest?.dispenseRequest?.validityPeriod?.start
        return dateFormatter.print(date) ?: ""
    }

    fun periodEnd(): String {
        val date =
                medicationAdministration.value?.medicationRequest?.dispenseRequest?.validityPeriod?.end

        return dateFormatter.print(date) ?: ""
    }

    fun prescriptionReason(): String {
        return getDosageInstruction()?.text?:""
    }

    fun intakeInfo(): String {
        return getDosageInstruction()?.patientInstruction?:""
    }

    private fun getDosageInstruction(): DosageInstruction? {
        if (medicationAdministration.value?.medicationRequest?.dosageInstruction?.size != 0) {
            return medicationAdministration.value?.medicationRequest?.dosageInstruction?.get(0)
        } else if (medicationAdministration.value?.dosageInstruction?.size != 0) {
            return medicationAdministration.value?.dosageInstruction?.get(0)
        }
        return null
    }

    fun timeOfDayItems(context: Context): ArrayList<TimeOfDayAdapterItem> {
        val items: ArrayList<TimeOfDayAdapterItem> = arrayListOf()
        val dosage =
                medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
                    ?.doseQuantity ?: 0.0

        medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
            ?.timing?.repeat?.timeOfDay?.forEach {
            val hour = String.format("%02d", it?.hour)
            val minute = String.format("%02d", it?.minute)
            val timeOfDay = "$hour:$minute"
            val dayPeriod = context.resources.getString(R.string.time)


            items.add(TimeOfDayAdapterItem(dayPeriod,
                    MedicationUtil.dosageToString(dosage), timeOfDay))
        }


        return items
    }

    fun isHowOftenShowed(): Boolean {
        return medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
            ?.timing?.repeat?.periodUnit == "WK"
    }

    fun isTimeOfIntakeShown(): Boolean {
        return medicationAdministration.value?.medicationRequest?.dosageInstruction?.getOrNull(0)
            ?.timing?.repeat?.dayOfWeek?.isNotEmpty() == true
    }

    fun isPeriodOfIntakeShown(): Boolean {
        val isStartNotNull = medicationAdministration.value?.medicationRequest?.dispenseRequest?.validityPeriod?.start != null
        val isEndNotNull = medicationAdministration.value?.medicationRequest?.dispenseRequest?.validityPeriod?.end != null
        val isNotInfinite = medicationAdministration.value?.medicationRequest?.dispenseRequest?.infinite == false
        return isStartNotNull && isEndNotNull && isNotInfinite
    }

    private fun onError (e: java.lang.Exception) {
        Timber.d(e)
        isLoading.value = false
        isShowContent.value = false
        isShowLoadingError.value = true
        isShowNoData.value = false
    }

    private fun startRequestIndicators() {
        isLoading.value = true
        isShowNoData.value = false
        isShowLoadingError.value = false
    }
}
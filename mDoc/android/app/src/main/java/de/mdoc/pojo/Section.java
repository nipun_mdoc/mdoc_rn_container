package de.mdoc.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;

/**
 * Created by ema on 12/28/16.
 */

public class Section implements Serializable {

    private String pollId;
    private String pollAssignId;
    private Map<String,String> description;
    private Map<String,String> name;
    private String id;
    private ArrayList<Question> questions;
    private int index;

    public String getPollAssignId() {
        return pollAssignId;
    }

    public void setPollAssignId(String pollAssignId) {
        this.pollAssignId = pollAssignId;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getDescription(String language) {
        return description.get(language);
    }

    public String getName(String language) {
        return name.get(language);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<Question> questions) {
        this.questions = questions;
    }
}

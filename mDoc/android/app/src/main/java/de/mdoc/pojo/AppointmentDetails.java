package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AppointmentDetails implements Serializable {

    @SerializedName("specialty")
    @Expose
    private String specialty;
    @SerializedName("tokboxSessionId")
    @Expose
    private String tokboxSessionId;
    @SerializedName("resourceType")
    @Expose
    private String resourceType;
    @SerializedName("identifiers")
    @Expose
    private List<Object> identifiers = null;
    @SerializedName("reminder")
    @Expose
    private Reminder reminder;
    @SerializedName("clinicId")
    @Expose
    private String clinicId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("start")
    @Expose
    private long start;
    @SerializedName("end")
    @Expose
    private long end;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("participants")
    @Expose
    private ArrayList<Participants> participants = null;
    @SerializedName("showHint")
    @Expose
    private boolean isShowHint;
    @SerializedName("showEndTime")
    @Expose
    private boolean isShowEndTime;
    @SerializedName("appointmentStatus")
    private String appointmentStatus;
    @SerializedName("showStatus")
    private boolean showStatus;
    @SerializedName("showTherapistName")
    @Expose
    private boolean showTherapistName;
    @SerializedName("repeatDefinition")
    @Expose
    private RepeatDefinition repeatDefinition;

    private String appointmentType;

    private String location;
    private String specialtyProvider;
    private ArrayList<EncounterData> encounter;

    private boolean isFirst;
    private String appointmentId;
    private long uts;
    private long cts;

    private boolean allDay;
    private boolean firstItemInDay;
    private String personalInfo;

    public Boolean metadataNeeded = null;
    public Boolean metadataPopulated = null;

    public String status;
    public String additionalLocationInfo;
    private boolean online;

    private boolean confirmationRequired;
    private String state;
    private String newVersionReferenceId;
    private boolean isFirstAppointment;

    private boolean isMonthView;

    private boolean isDark = false;

    public AppointmentDetails addAdditionalData(String appointmentId, long uts, long cts, String specialtyProvider,
                                                boolean confirmationRequired, String state, String newVersionReferenceId,
                                                boolean isFirstAppointment) {
        this.appointmentId = appointmentId;
        this.uts = uts;
        this.cts = cts;
        this.specialtyProvider = specialtyProvider;
        this.confirmationRequired = confirmationRequired;
        this.state = state;
        this.newVersionReferenceId = newVersionReferenceId;
        this.isFirstAppointment = isFirstAppointment;
        return this;
    }


    public AppointmentDetails addAdditionalData(String appointmentId, long uts, long cts, String specialtyProvider,
                                                ArrayList<EncounterData> encounters, Boolean metadataNeeded, Boolean metadataPopulated,
                                                boolean confirmationRequired, String state, String newVersionReferenceId, boolean isFirstAppointment, boolean isMonthView) {
        this.appointmentId = appointmentId;
        this.uts = uts;
        this.cts = cts;
        this.specialtyProvider = specialtyProvider;
        this.encounter = encounters;
        this.metadataNeeded = metadataNeeded;
        this.metadataPopulated = metadataPopulated;
        this.confirmationRequired = confirmationRequired;
        this.state = state;
        this.newVersionReferenceId = newVersionReferenceId;
        this.isFirstAppointment = isFirstAppointment;
        this.isMonthView = isMonthView;
        return this;
    }

    public RepeatDefinition getRepeatDefinition() {
        return repeatDefinition;
    }

    public void setRepeatDefinition(RepeatDefinition repeatDefinition) {
        this.repeatDefinition = repeatDefinition;
    }

    public Boolean getMetadataNeeded() {
        return metadataNeeded;
    }

    public void setMetadataNeeded(Boolean metadataNeeded) {
        this.metadataNeeded = metadataNeeded;
    }

    public Boolean getMetadataPopulated() {
        return metadataPopulated;
    }

    public void setMetadataPopulated(Boolean metadataPopulated) {
        this.metadataPopulated = metadataPopulated;
    }

    public String getAdditionalLocationInfo() {
        return additionalLocationInfo;
    }

    public void setAdditionalLocationInfo(String additionalLocationInfo) {
        this.additionalLocationInfo = additionalLocationInfo;
    }

    public boolean isFirstAppointment() {
        return isFirstAppointment;
    }

    public void setFirstAppointment(boolean firstAppointment) {
        isFirstAppointment = firstAppointment;
    }

    public boolean isMonthView() {
        return isMonthView;
    }

    public void setMonthView(boolean monthView) {
        isMonthView = monthView;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    public void setConfirmationRequired(boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getNewVersionReferenceId() {
        return newVersionReferenceId;
    }

    public void setNewVersionReferenceId(String newVersionReferenceId) {
        this.newVersionReferenceId = newVersionReferenceId;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public boolean isShowStatus() {
        return showStatus;
    }

    public void setShowStatus(boolean showStatus) {
        this.showStatus = showStatus;
    }

    public boolean isShowTherapistName() {
        return showTherapistName;
    }

    public void setShowTherapistName(boolean showTherapistName) {
        this.showTherapistName = showTherapistName;
    }

    public boolean isShowEndTime() {
        return isShowEndTime;
    }

    public void setShowEndTime(boolean showEndTime) {
        isShowEndTime = showEndTime;
    }

    public boolean isShowHint() {
        return isShowHint;
    }

    public void setShowHint(boolean showHint) {
        isShowHint = showHint;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getSpecialtyProvider() {
        return specialtyProvider;
    }

    public void setSpecialtyProvider(String specialtyProvider) {
        this.specialtyProvider = specialtyProvider;
    }

    public long getUts() {
        return uts;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    public long getCts() {
        return cts;
    }

    public void setCts(long cts) {
        this.cts = cts;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public String getTokboxSessionId() {
        return tokboxSessionId;
    }

    public void setTokboxSessionId(String tokboxSessionId) {
        this.tokboxSessionId = tokboxSessionId;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public List<Object> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(List<Object> identifiers) {
        this.identifiers = identifiers;
    }

    public Reminder getReminder() {
        return reminder;
    }

    public void setReminder(Reminder reminder) {
        this.reminder = reminder;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<Participants> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<Participants> participants) {
        this.participants = participants;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public boolean isFirstItemInDay() {
        return firstItemInDay;
    }

    public void setFirstItemInDay(boolean firstItemInDay) {
        this.firstItemInDay = firstItemInDay;
    }

    public ArrayList<EncounterData> getEncounter() {
        return encounter;
    }

    public void setEncounter(ArrayList<EncounterData> encounter) {
        this.encounter = encounter;
    }


    public String getPersonalInfo() {
        return personalInfo;
    }

    public void setPersonalInfo(String personalInfo) {
        this.personalInfo = personalInfo;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }

    public boolean isAcuteClinic() {
        // Acute clinic appointments have encounter and Reha doesn't
        return isEncounterValid();
    }

    private boolean isEncounterValid() {
        return this.encounter != null && !this.encounter.isEmpty();
    }

    public Boolean isMetaDataPopulated() {
        return metadataPopulated != null? metadataPopulated:false;
    }

    public Boolean isMetaDataNeeded() {
        return metadataNeeded != null? metadataNeeded:false;
    }

    public Boolean isCanceled () {
        if (status != null && status.equals("CANCEL")){
            return true;
        } else {
            return false;
        }
    }

    public boolean isDark() {
        return isDark;
    }

    public void setDark(boolean dark) {
        isDark = dark;
    }
}

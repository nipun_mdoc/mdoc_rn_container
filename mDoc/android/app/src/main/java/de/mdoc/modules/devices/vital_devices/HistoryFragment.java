package de.mdoc.modules.devices.vital_devices;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.mikhaellopez.circularprogressbar.CircularProgressBar;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.pojo.HistoryData;
import de.mdoc.pojo.StatisticData;
import de.mdoc.util.MdocAppHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.listeners.OnMonthChangeListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;

public class HistoryFragment extends MdocFragment {

    MdocActivity activity;

    @BindView(R.id.circleStepsH)
    CircularProgressBar circleStepsH;
    @BindView(R.id.circleDistanceH)
    CircularProgressBar circleDistanceH;
    @BindView(R.id.circleCaloriesH)
    CircularProgressBar circleCaloriesH;

    @BindView(R.id.txtDateHistory)
    TextView txtDateHistory;
    @BindView(R.id.arrowLeftHistory)
    ImageView arrowLeftHistory;
    @BindView(R.id.arrowRightHistory)
    ImageView arrowRightHistory;
    @BindView(R.id.arrowLeftMonth)
    ImageView arrowLeftMonth;
    @BindView(R.id.arrowRightMonth)
    ImageView arrowRightMonth;

    @BindView(R.id.txtMonth)
    TextView txtMonth;
    @BindView(R.id.txtStepsCurrentH)
    TextView txtStepsCurrentH;
    @BindView(R.id.txtCalloriesCurrentH)
    TextView txtCalloriesCurrentH;
    @BindView(R.id.txtDistanceCurrentH)
    TextView txtDistanceCurrentH;

    @BindView(R.id.txtStepsDescH)
    TextView txtStepsDescH;
    @BindView(R.id.txtDistanceDescH)
    TextView txtDistanceDescH;
    @BindView(R.id.txtCaloriesDescH)
    TextView txtCaloriesDescH;

    @BindView(R.id.btnCancel)
    Button btnCancel;
    @BindView(R.id.btnSelect)
    Button btnSelect;

    @BindView(R.id.calendarView)
    ExpCalendarView calendarView;

    @BindView(R.id.relativeCalendar)
    RelativeLayout relativeCalendar;

    ProgressDialog progressDialog;

    private String deviceName;
    private String dateForClick;
    private boolean isShown = false;
    DateData currentSelection = new DateData(0, 0, 0);
    DateData tempDate = new DateData(0, 0, 0);
    LocalDate localDate = new LocalDate();

    @Override
    protected int setResourceId() {
        return R.layout.fragment_history;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(getString(R.string.please_wait));
        progressDialog.setCancelable(false);


        deviceName = getArguments().getString(MdocConstants.DEVICE_NAME);

        DateTime now = DateTime.now();
        currentSelection.setDay(now.getDayOfMonth());
        currentSelection.setMonth(now.getMonthOfYear());
        currentSelection.setYear(now.getYear());
        calendarView.markDate(currentSelection.setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, ContextCompat.getColor(activity, R.color.colorPrimary))));
        tempDate = currentSelection;

        String inputMonth = String.valueOf(currentSelection.getMonth());
        convertDateToStringMonth(inputMonth);


        txtDateHistory.setText(localDate.toString(DateTimeFormat.longDate()));

        DateTime dt = new DateTime();
        DateTime utc = new DateTime(dt, DateTimeZone.UTC);
        getProviderStatistic(String.valueOf(utc.getMillis()));

        calendarView.setOnMonthChangeListener(new OnMonthChangeListener() {
            @Override
            public void onMonthChange(int year, int month) {
                txtDateHistory.setText(String.format("%d-%d", year, month));
                getHistoryForDevice(month, year, deviceName);
                String inputMonth = String.valueOf(month);
                convertDateToStringMonth(inputMonth);
            }
        });

        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                calendarView.unMarkDate(date);
                calendarView.unMarkDate(currentSelection);
                calendarView.unMarkDate(tempDate);
                String inputDate = date.getMonth() + "/" + date.getDay() + "/" + localDate.getYear() + " " + "12:00:00";
                convertDateToString(inputDate);
                dateForClick = convertDateToMiliseconds(inputDate);
                calendarView.markDate(date.setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, ContextCompat.getColor(activity, R.color.colorPrimary))));
                btnSelect.setEnabled(true);
                tempDate = date;
            }
        });
    }

    private void getHistoryForDevice(final int month, final int year, String provider) {
        progressDialog.show();
        String userId = MdocAppHelper.getInstance().getUserId();

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("year", String.valueOf(year));
        params.put("month", String.valueOf(month));
        params.put("provider", provider);
        params.put("userId", userId);

        MdocManager.getHistoryForDevices(params, new Callback<HistoryData>() {
            @Override
            public void onResponse(Call<HistoryData> call, Response<HistoryData> response) {
                ArrayList<Boolean> items = response.body().getData().getActivity();
                if (items.size() >= 0) {
                    for (int i = 0; i < items.size(); i++) {
                        if (items.get(i)) {
                            calendarView.markDate(new DateData(year, month, i + 1).setMarkStyle(new MarkStyle(MarkStyle.DOT, ContextCompat.getColor(activity, R.color.colorPrimary))));
                        }
                    }
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<HistoryData> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void convertDateToString(String inputDate) {
        DateTime dateTime = DateTime.parse(inputDate, DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss"));
        txtDateHistory.setText(dateTime.toString(DateTimeFormat.longDate()));
    }

    private String convertDateToMiliseconds(String inputDate) {
        DateTime dtLocal = DateTime.parse(inputDate, DateTimeFormat.forPattern("MM/dd/yyyy HH:mm:ss").withZone(DateTimeZone.getDefault()));
        DateTime cdUtc = new DateTime(dtLocal, DateTimeZone.UTC);
        return String.valueOf(cdUtc.getMillis());
    }

    private void convertDateToStringMonth(String inputMonth) {
        DateTime dateTime = DateTime.parse(inputMonth, DateTimeFormat.forPattern("MM"));
        txtMonth.setText(dateTime.toString(DateTimeFormat.forPattern("MMMM")));
    }

    private void getProviderStatistic(String date) {

        String userId = MdocAppHelper.getInstance().getUserId();

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("date", date);
        params.put("provider", deviceName);
        params.put("userId", userId);

        MdocManager.getStatistics(params, new Callback<StatisticData>() {
            @Override
            public void onResponse(Call<StatisticData> call, Response<StatisticData> response) {
                int goalCallories = 0;
                double goalDistance = 0;
                int goalSteps = 0;
                int currentCallories = 0;
                double currentDistance = 0;
                int currentSteps = 0;

                try {
                    if (response.body() != null) {

                        if (response.body().getData().getActivity().getGoals() != null) {
                            goalCallories = response.body().getData().getActivity().getGoals().getCaloriesOut();
                            goalDistance = response.body().getData().getActivity().getGoals().getDistance();
                            goalSteps = response.body().getData().getActivity().getGoals().getSteps();
                        }

                        if (response.body().getData().getActivity().getSummary() != null) {
                            currentCallories = response.body().getData().getActivity().getSummary().getCaloriesOut();
                            currentDistance = response.body().getData().getActivity().getSummary().getDistances().get(0).getDistance();
                            currentSteps = response.body().getData().getActivity().getSummary().getSteps();
                        }

                        circleDistanceH.setProgressWithAnimation(calculateProcentFromNumbers(currentDistance, goalDistance), 800);
                        circleStepsH.setProgressWithAnimation(calculateProcentFromNumbers(currentSteps, goalSteps), 800);
                        circleCaloriesH.setProgressWithAnimation(calculateProcentFromNumbers(currentCallories, goalCallories), 800);

                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                    }
                } catch (Exception e) {
                    progressDialog.dismiss();
                }

                txtCalloriesCurrentH.setText(String.valueOf(currentCallories));
                txtStepsCurrentH.setText(String.valueOf(currentSteps));
                txtDistanceCurrentH.setText(String.valueOf(currentDistance));

                txtCaloriesDescH.setText(getResources().getString(R.string.calories_text_first) + " " + goalCallories + " " + getResources().getString(R.string.calories_text_second));
                txtDistanceDescH.setText(getResources().getString(R.string.distance_text_first) + " " + goalDistance + " " + getResources().getString(R.string.distacne_text_second));
                txtStepsDescH.setText(getResources().getString(R.string.step_text_first) + " " + goalSteps + " " + getResources().getString(R.string.step_test_second));
            }

            @Override
            public void onFailure(Call<StatisticData> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private int calculateProcentFromNumbers(double current, double total) {
        return calculateDegreseFromProcent((current / total) * 100);
    }

    private int calculateDegreseFromProcent(double procent) {
        double total = procent * 0.01;
        return (int) (total * 100);
    }

    @OnClick(R.id.arrowRightHistory)
    public void onArrowRightClick() {
        localDate = localDate.plusDays(1);
        txtDateHistory.setText(localDate.toString(DateTimeFormat.longDate()));
        String inputMonth = String.valueOf(localDate.getMonthOfYear());
        convertDateToStringMonth(inputMonth);
        progressDialog.show();
        getProviderStatistic(String.valueOf(localDate.toDate().getTime()));
    }

    @OnClick(R.id.arrowLeftHistory)
    public void onArrowLeftClick() {
        localDate = localDate.minusDays(1);
        txtDateHistory.setText(localDate.toString(DateTimeFormat.longDate()));
        String inputMonth = String.valueOf(localDate.getMonthOfYear());
        convertDateToStringMonth(inputMonth);
        progressDialog.show();
        getProviderStatistic(String.valueOf(localDate.toDate().getTime()));
    }

    @OnClick(R.id.arrowLeftMonth)
    public void onArrowLeftMontClick() {
        calendarView.unMarkDate(currentSelection);
        currentSelection.setMonth(currentSelection.getMonth() - 1);
        calendarView.travelTo(currentSelection);
        calendarView.markDate(currentSelection);
        String inputDate = currentSelection.getMonth() + "/" + currentSelection.getDay() + "/" + currentSelection.getYear();
        convertDateToString(inputDate);
        String inputMonth = String.valueOf(currentSelection.getMonth());
        convertDateToStringMonth(inputMonth);
    }

    @OnClick(R.id.arrowRightMonth)
    public void onArrowRightMontClick() {
        calendarView.unMarkDate(currentSelection);
        currentSelection.setMonth(currentSelection.getMonth() + 1);
        calendarView.travelTo(currentSelection);
        calendarView.markDate(currentSelection);
        String inputDate = currentSelection.getMonth() + "/" + currentSelection.getDay() + "/" + currentSelection.getYear();
        convertDateToString(inputDate);
        String inputMonth = String.valueOf(currentSelection.getMonth());
        convertDateToStringMonth(inputMonth);
    }

    @OnClick(R.id.txtDateHistory)
    public void onDateHistoryClick() {

        if (!isShown) {
            getHistoryForDevice(currentSelection.getMonth(), currentSelection.getYear(), deviceName);
            relativeCalendar.setVisibility(View.VISIBLE);
            isShown = true;
        } else {
            relativeCalendar.setVisibility(View.GONE);
            isShown = false;
        }
    }

    @OnClick(R.id.btnSelect)
    public void onSelectClick() {
        relativeCalendar.setVisibility(View.GONE);
        isShown = false;
        getProviderStatistic(dateForClick);
    }

    @OnClick(R.id.btnCancel)
    public void onCancelClick() {
        relativeCalendar.setVisibility(View.GONE);
        isShown = false;
    }

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Devices;
    }
}
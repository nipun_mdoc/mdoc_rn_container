package de.mdoc.modules.medications.create_plan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.databinding.FragmentDosageBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import de.mdoc.pojo.CodingItem
import kotlinx.android.synthetic.main.fragment_dosage.*

class DosageFragment internal constructor(val viewModel: CreatePlanViewModel): NewBaseFragment() {
    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar
    lateinit var activity: MdocActivity

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentDosageBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_dosage, container, false)
        activity = context as MdocActivity
        binding.createPlanViewModel = viewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSpinners()
    }

    private fun setupSpinners() {
        dosageSpinner()
        dosagePartialSpinner()
        unitSpinner()
    }

    private fun dosageSpinner() {
        val itemList: ArrayList<CodingItem> = arrayListOf()

        repeat(100) {
            itemList.add(CodingItem("", (it).toString(), (it).toString()))
        }

        val dosageSpinnerAdapter = SpinnerAdapter(activity, itemList)
        dosage_spinner?.adapter = dosageSpinnerAdapter

        dosage_spinner.setSelection(viewModel.dosageQuantity.value?.toInt()?: 1)
        dosage_spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                viewModel.dosageQuantity.value = p2
                    .toDouble()
            }
        }
    }

    private fun dosagePartialSpinner() {
        val itemList: ArrayList<CodingItem> = arrayListOf(
                CodingItem("", "0.0", "-"),
                CodingItem("", "0.5", "1/2"),
                CodingItem("", "0.33", "1/3"),
                CodingItem("", "0.25", "1/4"),
                CodingItem("", "0.66", "2/3"),
                CodingItem("", "0.75", "3/4"))
        val dosagePartialSpinnerAdapter = SpinnerAdapter(activity, itemList)
        dosage_partial_spinner?.adapter = dosagePartialSpinnerAdapter

        dosage_partial_spinner?.onItemSelectedListener =
                object: AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        viewModel.dosageQuantityPartial.value = itemList.getOrNull(p2)
                            ?.code?.toDouble()
                    }
                }
    }

    private fun unitSpinner() {
        val itemList: ArrayList<CodingItem> = viewModel.unitsOfMeasurement.value ?: arrayListOf()
        val unitSpinnerAdapter = SpinnerAdapter(activity, itemList)
        unit_spinner?.adapter = unitSpinnerAdapter

        setPreselected(itemList)

        unit_spinner?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                viewModel.doseQuantityCode.value = itemList.getOrNull(p2)
                    ?.code
            }
        }
    }

    private fun setPreselected(itemList: ArrayList<CodingItem>){
        itemList.forEachIndexed { index, codingItem ->
            if (codingItem.code == DEFAULT_SELECTION) {
                unit_spinner.setSelection(index)
            }
        }
    }
    companion object {
        val TAG = DosageFragment::class.java.canonicalName
        const val DEFAULT_SELECTION="Tablet"
    }
}

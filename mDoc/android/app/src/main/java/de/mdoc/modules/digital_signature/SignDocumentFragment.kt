package de.mdoc.modules.digital_signature

import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.util.ColorUtil
import kotlinx.android.synthetic.main.fragment_sign_document.*
import java.net.URI


/**
 * Created by Trenser on 17/12/20.
 */

class SignDocumentFragment : Fragment() {

    lateinit var webView : WebView
    private var toolbar : Toolbar? = null
    private var backBadgeView : ImageView? = null
    private var toolbarHead : TextView? = null
    private var toolbarUrl : TextView? = null

    var webUrl: String? = null
    var isFromReviewDocument: Boolean? = false

    private val model: SignDocumentViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        activity?.requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_SENSOR
        return inflater.inflate(R.layout.fragment_sign_document, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        webView = view.findViewById(R.id.documentWebView)
        toolbar = view.findViewById(R.id.documentToolbar)
        backBadgeView = view.findViewById(R.id.documentBadgeBack)
        toolbarHead = view.findViewById(R.id.documentToolbarHeadTxt)
        toolbarUrl = view.findViewById(R.id.documentToolbarUrlTxt)

        toolbarHead?.setTextColor(ColorUtil.contrastColor(resources))
        documentToolbarBack.setColorFilter(ColorUtil.contrastColor(resources))

        webUrl = arguments?.getString("url")
        isFromReviewDocument = arguments?.getBoolean("isFromReviewDocument", false)

        if(savedInstanceState == null){
            webLoad(webUrl)
            val uri = URI(webUrl)
            val domain: String = uri.host
            toolbarUrl?.text = if (domain.startsWith("www.")) domain.substring(4) else domain
            toolbarUrl?.setTextColor(ColorUtil.contrastColor(resources))
        }

        view.findViewById<ImageView>(R.id.documentToolbarBack)?.setOnClickListener { navigationBackToMessages() }

        backBadgeView?.setOnClickListener{
            val orientation = this.resources.configuration.orientation
            if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                navigationBackToMessages()
            } else {
                val url : String = webView.url;
                val uri = URI(url)
                val urlSegments = uri.path.split("/".toRegex()).toTypedArray()
                val lastPathComponent = urlSegments[urlSegments.size - 1]
                if(lastPathComponent != "finish" && webView?.canGoBack()!!) webView?.goBack() else navigationBackToMessages()
            }

        }

        model.fragmentHead.observe(viewLifecycleOwner, Observer<String> { item ->
            toolbarHead?.text = item
        })

        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                navigationBackToMessages()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)

    }

    private fun navigationBackToMessages () {
        if(isFromReviewDocument!!)  findNavController().popBackStack(R.id.documentReviewFragment, true) else  findNavController().popBackStack()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            toolbar?.visibility = View.GONE
            backBadgeView?.setImageResource(R.drawable.ic_close_24dp)
            backBadgeView?.scaleType = ImageView.ScaleType.CENTER_INSIDE
        } else {
            toolbar?.visibility = View.VISIBLE
            backBadgeView?.setImageResource(R.drawable.ic_back_arrow_circle)
            backBadgeView?.scaleType = ImageView.ScaleType.CENTER_CROP
        }
    }

    override fun onResume() {
        super.onResume()
        webView?.onResume()
    }

    override fun onPause() {
        super.onPause()
        webView?.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        activity?.requestedOrientation= ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        if (webView != null) {
            (webView?.parent as ViewGroup).removeView(webView)
            webView?.removeAllViews()
            webView?.destroy()
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun webLoad(Url: String?) {
        try {
            val webSettings = webView!!.settings
            webView!!.settings.javaScriptEnabled = true
            webView!!.settings.cacheMode = WebSettings.LOAD_CACHE_ELSE_NETWORK
            webView!!.settings.setAppCacheEnabled(true)
            webView!!.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
            webSettings.domStorageEnabled = true
            webSettings.layoutAlgorithm = WebSettings.LayoutAlgorithm.NARROW_COLUMNS
            webSettings.useWideViewPort = true
            webView!!.webViewClient = MyWebViewClient()
            webView!!.loadUrl(Url)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private inner class MyWebViewClient : WebViewClient() {

        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            model.fragmentHead.value = view?.title
        }
    }
}

class SignDocumentViewModel : ViewModel() {
    val fragmentHead = MutableLiveData<String>()
    fun setName(name: String) {
        fragmentHead.value = name
    }

    fun getName(): LiveData<String>? {
        return fragmentHead
    }
}
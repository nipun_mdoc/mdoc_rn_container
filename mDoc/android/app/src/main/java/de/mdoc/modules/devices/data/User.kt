package de.mdoc.modules.devices.data

data class User(
        val cts: Int,
        val superAdmin: Boolean,
        val uts: Int
)
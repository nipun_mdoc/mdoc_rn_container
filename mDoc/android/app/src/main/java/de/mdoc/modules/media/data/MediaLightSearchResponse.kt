package de.mdoc.modules.media.data

data class MediaLightSearchResponse(
        val data: MediaLightData
                                   ) {
    data class MediaLightData(
            val list: ArrayList<MediaLightItem>
                             )
}

package de.mdoc.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import de.mdoc.BuildConfig
import de.mdoc.R
import de.mdoc.adapters.CasesAdapter
import de.mdoc.constants.MdocConstants
import de.mdoc.network.RestClient
import de.mdoc.network.request.AllClinicsRequest
import de.mdoc.network.request.SearchClinicRequest
import de.mdoc.pojo.Case
import de.mdoc.pojo.ClinicDetails
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.activity_case.*
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch


class CaseActivity : AppCompatActivity() {

    var adapter: CasesAdapter? = null
    var content: MutableLiveData<ArrayList<ClinicDetails>> = MutableLiveData(ArrayList())
    private val scope = MainScope()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_case)
        try {
            searchClinicAsync(this, false)
        } catch (e: java.lang.Exception) {
            Log.e("test", e.toString())
        }

        val sw = findViewById<View>(R.id.toggleCase) as Switch
        sw.setOnCheckedChangeListener { buttonView, isChecked ->
            try {
                searchClinicAsync(this, isChecked)
            } catch (e: java.lang.Exception) {
                Log.e("test", e.toString())
            }
        }

        casesLv.setOnItemClickListener { adapter, _, i, _ ->
            val item = adapter?.getItemAtPosition(i) as Case
            MdocAppHelper.getInstance().caseId = item.caseId
            MdocAppHelper.getInstance().externalCaseId = item.externalCaseId
            MdocAppHelper.getInstance().clinicId = item.clinicId
            MdocAppHelper.getInstance().setIsPremiumPatient(item.isPremium)
            MdocAppHelper.getInstance().department = item.department
            MdocAppHelper.getInstance().externalCaseId = item.externalCaseId

            navigateToDashboard()
        }
    }
    var cases: ArrayList<Case> = ArrayList()
    private fun searchClinicAsync(context: Context, isActive : Boolean) {
        if (intent?.getSerializableExtra(MdocConstants.CASES) != null) {
            cases = intent?.getSerializableExtra(MdocConstants.CASES) as ArrayList<Case>
            if(isActive)
                cases = cases.filter { it.isActive == isActive } as ArrayList<Case>
            if(cases.size!=0){
                casesLv.visibility = View.VISIBLE
                txtNoCases.visibility = View.GONE
            var final1: ArrayList<Case> = ArrayList()
            scope.launch {
                content.value = RestClient.getService().searchClinics(AllClinicsRequest("", false))
                        .data
                for (item in cases) {
                    for (clinic in content.value!!) {
                        if (clinic.clinicId.equals(item.clinicId)) {
                            item.clinicName = clinic.name
                            item.clinicAddress = clinic.getFullAddress()
                            final1.add(item)
                        }
                    }
                }
                adapter = CasesAdapter(final1, context)
                casesLv.adapter = adapter
                Log.e("test", content.toString())
            }
            }else{
                casesLv.visibility = View.GONE
                txtNoCases.visibility = View.VISIBLE
            }
        }


    }

    fun navigateToDashboard() {
        MdocConstants.caseList = cases
        val intent = Intent(this, MdocActivity::class.java)
        startActivity(intent)
        if (BuildConfig.FLAV != "kiosk") {
            finish()
        }
    }
}

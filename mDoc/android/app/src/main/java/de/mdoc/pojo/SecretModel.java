package de.mdoc.pojo;


public class SecretModel {
    private String code;
    private long timestamp;
    private String message;
    private KeyObject data;

    public KeyObject getData() {
        return data;
    }

    public void setData(KeyObject data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public class KeyObject{
        public String key;

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }


}




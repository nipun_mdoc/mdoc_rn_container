package de.mdoc.modules.questionnaire.questionnaire_view_model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.mdoc.data.questionnaire.AnswersRequest
import de.mdoc.data.questionnaire.Data

class QuestionnaireViewModel:ViewModel(){

    private val repository = QuestionnaireRepository()

    private var questionnaireAnswerLive: MutableLiveData<Data>? = null


    fun questionnaireAnswers(request:AnswersRequest): LiveData<Data> {
        questionnaireAnswerLive = repository.getQuestionnaireAnswers(request)

        return questionnaireAnswerLive as LiveData<Data>
    }
}
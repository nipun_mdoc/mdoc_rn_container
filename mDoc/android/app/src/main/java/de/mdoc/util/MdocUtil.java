package de.mdoc.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.mdoc.R;
import de.mdoc.constants.MdocConstants;
import de.mdoc.pojo.Answer;
import timber.log.Timber;

/**
 * Created by ema on 4/6/17.
 */

public class MdocUtil {

    private static Pattern pattern;
    private static Matcher matcher;

    public static boolean isNotNull(String txt) {
        return txt != null && txt.trim().length() > 0;
    }

    public static void showToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static Toast getCustomToastShort(Activity activity, String message) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast, activity.findViewById(R.id.custom_toast_container));

        TextView text = layout.findViewById(R.id.text);
        text.setText(message);

        Toast toast = new Toast(activity);
        toast.setGravity(Gravity.TOP, 0, 450);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.setView(layout);
        return toast;
    }

    public static boolean isValidEmailFormat(String email) {
        pattern = Pattern.compile(MdocConstants.EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static int getMonthResIdByNumber(int number) {
        switch (number) {
            case 1:
                return R.string.january;
            case 2:
                return R.string.february;
            case 3:
                return R.string.march;
            case 4:
                return R.string.april;
            case 5:
                return R.string.may;
            case 6:
                return R.string.june;
            case 7:
                return R.string.july;
            case 8:
                return R.string.august;
            case 9:
                return R.string.september;
            case 10:
                return R.string.october;
            case 11:
                return R.string.november;
            case 12:
                return R.string.december;
            default:
                return 1;
        }
    }

    public static boolean isToday(int day, int month, int year) {

        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR) == year && now.get(Calendar.MONTH) == month && now.get(Calendar.DAY_OF_MONTH) == day;
    }

    public static boolean isToday(long time) {

        Calendar now = Calendar.getInstance();

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);

        return now.get(Calendar.YEAR) == c.get(Calendar.YEAR) &&
                now.get(Calendar.MONTH) == c.get(Calendar.MONTH) &&
                now.get(Calendar.DAY_OF_MONTH) == c.get(Calendar.DAY_OF_MONTH);
    }

    public static boolean isCurrentMonth(int month, int year) {
        Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR) == year && now.get(Calendar.MONTH) == month;
    }

    public static boolean isPast(long time) {
        Calendar now = Calendar.getInstance();

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        Timber.d("CALENDAR c %s %s %s", c.get(Calendar.MONTH), c.get(Calendar.YEAR), c.get(Calendar.DAY_OF_MONTH));
        Timber.d("CALENDAR now %s %s %s", now.get(Calendar.MONTH), now.get(Calendar.YEAR), now.get(Calendar.DAY_OF_MONTH));


        if (c.get(Calendar.YEAR) < now.get(Calendar.YEAR)) {
            return true;
        } else if (c.get(Calendar.YEAR) == now.get(Calendar.YEAR) &&
                c.get(Calendar.MONTH) < now.get(Calendar.MONTH)) {
            return true;
        } else return c.get(Calendar.YEAR) == now.get(Calendar.YEAR) &&
                c.get(Calendar.MONTH) == now.get(Calendar.MONTH) &&
                c.get(Calendar.DAY_OF_MONTH) < now.get(Calendar.DAY_OF_MONTH);

    }

    public static boolean isPast(int day, int month, int year) {

        Calendar now = Calendar.getInstance();
        return year <= now.get(Calendar.YEAR) && month <= now.get(Calendar.MONTH) && day < now.get(Calendar.DAY_OF_MONTH);
    }

    public static String getDayName(long time) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd. MMM");

        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        String dayName = c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
        String date = formatter.format(time);

        return dayName + ", " + date;
    }

    public static int calculateProgress(int completed, int total) {
        return (int) Math.ceil(((completed * 100.0f) / total));
    }

    public static boolean arrayHasString(ArrayList<String> answers, String value) {
        for (int i = 0; i < answers.size(); i++) {
            if (answers.get(i).equals(value)) {
                return true;
            }
        }
        return false;
    }

    public static Answer getAnswerByQuestionId(ArrayList<Answer> answersData, String questionId) {
        if (answersData != null) {
            for (int i = 0; i < answersData.size(); i++) {
                if (answersData.get(i).getPollQuestionId().equals(questionId)) {
                    return answersData.get(i);
                }
            }
        }
        return null;
    }

    public static boolean hasConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);

        NetworkInfo wifiNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiNetwork != null && wifiNetwork.isConnected()) {
            return true;
        }

        NetworkInfo mobileNetwork = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (mobileNetwork != null && mobileNetwork.isConnected()) {
            return true;
        }

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();

    }

    public static void setTextPostion(int how_many, SeekBar seekbar, TextView label, int position) {
        int offSet = 0;
        String what_to_say = String.valueOf(how_many);

        label.setText(what_to_say);

        if (seekbar.getMax() != 0) {
            offSet = (seekbar.getWidth() - seekbar.getPaddingLeft() - seekbar.getPaddingRight()) * position / seekbar.getMax();
        }

        label.setX(seekbar.getX() + offSet);
    }

    public static void setTextPositionVertical(int how_many, SeekBar seekBar, TextView label, ConstraintLayout labelLayout, int position) {
        int offSet = 0;
        String what_to_say = String.valueOf(how_many);

        label.setText(what_to_say);

        if (seekBar.getMax() != 0) {
            int revertedPosition = seekBar.getMax() - position;
            offSet = (seekBar.getHeight()  - seekBar.getPaddingLeft()-seekBar.getPaddingRight()) * revertedPosition / seekBar.getMax();
        }
        //calculate thumb position with screen density taken in calculation, in order to support different device screens
        float screenDensity = seekBar.getContext().getResources().getDisplayMetrics().density;
        float calculationFactor = 28.5f;
        float fineTuneTextPosition = screenDensity * calculationFactor;
        float y = seekBar.getY() + seekBar.getPaddingLeft() - fineTuneTextPosition + offSet;
        Log.e("Y Axis", String.valueOf(y));
        labelLayout.setY(y);
    }

    public static long getFirstDayOfWeek() {
        Calendar call = Calendar.getInstance();
        call.set(Calendar.HOUR_OF_DAY, 0);
        call.clear(Calendar.MINUTE);
        call.clear(Calendar.SECOND);
        call.clear(Calendar.MILLISECOND);
        call.setFirstDayOfWeek(Calendar.MONDAY);
        call.set(Calendar.DAY_OF_WEEK, call.getFirstDayOfWeek());

        return call.getTimeInMillis();
    }

    public static long getDayOfTheWeek(Date date, int amount, boolean isStartDate) {
        Calendar call = new GregorianCalendar();
        call.setTime(date);
        if (isStartDate) {
            call.set(Calendar.HOUR_OF_DAY, 0);
        } else {
            call.set(Calendar.HOUR_OF_DAY, 23);
        }
        call.set(Calendar.MINUTE, 0);
        call.set(Calendar.SECOND, 0);
        call.set(Calendar.MILLISECOND, 0);
        call.add(Calendar.DAY_OF_MONTH, amount);

        return call.getTimeInMillis();
    }

    public static String getMonth(Date date){
        return (String) DateFormat.format("MMMM",  date);
    }

    public static String getYear(Date date) {
        return (String) DateFormat.format("yyyy",  date);
    }

    public static long getYear(Date date, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.YEAR, year);
        return calendar.getTimeInMillis();
    }

    public static long getMonth(Date date, int month, boolean isLastDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, month);
        if(isLastDate) {
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        }
        return calendar.getTimeInMillis();
    }

    public static String getTimeFromMs(long miliseconds, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);

        return simpleDateFormat.format(calendar.getTime());
    }

    public static long getFirstMonthInYear(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 2);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        cal.set(Calendar.DAY_OF_YEAR, 1);

        return cal.getTimeInMillis();
    }

    public static long getLastMonthInYear(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, -2);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_YEAR, 1);
        cal.add(Calendar.YEAR, 1);

        return cal.getTimeInMillis();
    }

    public static long getFirstDayInMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 2);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);

        cal.set(Calendar.DAY_OF_MONTH, 1);

        return cal.getTimeInMillis();
    }

    public static long getLastDayInMonth() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, -2);
        cal.clear(Calendar.MINUTE);
        cal.clear(Calendar.SECOND);
        cal.clear(Calendar.MILLISECOND);
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.add(Calendar.MONTH, 1);

        return cal.getTimeInMillis();
    }

    public static void callPhone(Activity activity, String phone) {
        if (phone == null) {
            phone = "";
        }
        Intent in = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
        try {
            activity.startActivity(in);
        } catch (android.content.ActivityNotFoundException ex) {
            MdocUtil.showToastShort(activity, "Could not find an activity to place the call.");
        }
    }

    public static class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    public static String capitalizeFirstLetter(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public static int dpToPx(int dp, Context context) {
        Resources r = context.getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    public static String getParsedDate(String format, Calendar dateAndTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDateFormat.format(dateAndTime.getTime());
    }

    public static String getParsedDateDefault(String format, Calendar dateAndTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDateFormat.format(dateAndTime.getTime());
    }

    public static String getParsedDateUTC(String format, Calendar dateAndTime) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(dateAndTime.getTime());
    }

    public static String getDateFromMS(String format, long miliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(miliseconds);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());
        return simpleDateFormat.format(calendar.getTime());
    }

    public static long getTimeInMS(Integer year, Integer month, Integer day, Integer hour, Integer minute, Integer second) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);

        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);

        return cal.getTimeInMillis();
    }

    public static long getTimeInDays(Integer year, Integer month, Integer day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);

        return cal.getTimeInMillis();
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        if (html == null) {
            // return an empty spannable if the html is null
            return new SpannableString("");
        } else {
            String replaced = html.replace("\n","<br>");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // FROM_HTML_MODE_LEGACY is the behaviour that was used for versions below android N
                // we are using this flag to give a consistent behaviour
                return Html.fromHtml(replaced, Html.FROM_HTML_MODE_LEGACY);
            } else {
                return Html.fromHtml(replaced);
            }
        }
    }
}

package de.mdoc.modules.devices.vital_devices

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import de.mdoc.R
import kotlinx.android.synthetic.main.fragment_results.*

class ResultsFragment : Fragment(R.layout.fragment_results) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            tabLayout.setupWithViewPager(pager)
            pager.adapter = ResultFragmentPagerAdapter(parentFragmentManager, it, arguments)
        }
    }

    class ResultFragmentPagerAdapter(fm: FragmentManager, val context: Context, val arguments: Bundle?) : FragmentStatePagerAdapter(fm) {

        override fun getCount(): Int = 2

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> context.getString(R.string.actual_results)
                else -> context.getString(R.string.history)
            }
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    val fragment = ActualResultsFragment()
                    fragment.arguments = arguments
                    fragment
                }
                else -> {
                    val fragment = HistoryFragment()
                    fragment.arguments = arguments
                    fragment
                }
            }
        }
    }
}

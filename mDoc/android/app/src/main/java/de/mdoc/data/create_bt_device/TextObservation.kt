package de.mdoc.data.create_bt_device

data class TextObservation(
        val div: Div,
        val status: String
)
package de.mdoc.data.create_bt_device

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MedicalDeviceAdapterData(var deviceId:String="",
                                    var deviceName:String?=null,
                                    var timeOfObservation:Long=0):Parcelable
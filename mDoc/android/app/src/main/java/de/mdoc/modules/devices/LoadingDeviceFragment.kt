package de.mdoc.modules.devices

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothGatt
import android.bluetooth.BluetoothManager
import android.bluetooth.le.BluetoothLeScanner
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.AS87
import de.mdoc.constants.BM57
import de.mdoc.constants.GL50
import de.mdoc.data.create_bt_device.Identifier
import de.mdoc.data.create_bt_device.Observation
import de.mdoc.data.create_bt_device.Udi
import de.mdoc.data.requests.CreateDeviceRequest
import de.mdoc.data.responses.DevicesResponse
import de.mdoc.data.responses.SingleDeviceResponse
import de.mdoc.fragments.MdocBaseFragment
import de.mdoc.interfaces.BleResponseListener
import de.mdoc.interfaces.DeviceDataListener
import de.mdoc.modules.devices.bf600.connectionTimeoutAction
import de.mdoc.modules.devices.bluetooth_le.*
import de.mdoc.modules.devices.data.BleMessage
import de.mdoc.modules.devices.data.DevicesForUserRequest
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.modules.devices.devices_factory.Bf600Device
import de.mdoc.modules.devices.devices_factory.BluetoothDevice
import de.mdoc.modules.devices.devices_factory.BluetoothDeviceFactory
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.response.MdocResponse
import de.mdoc.network.response.getErrorDetails
import de.mdoc.util.*
import kotlinx.android.synthetic.main.fragment_loading_device.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.lang.ref.WeakReference

class LoadingDeviceFragment : MdocBaseFragment(), BleResponseListener, DeviceDataListener {

    lateinit var activity: MdocActivity
    private var bluetoothAdapter: BluetoothAdapter? = null
    private var bluetoothLeScanner: BluetoothLeScanner? = null
    private var callOnce = true
    lateinit var scanTimeoutHandler: Handler
    private var connectionTimeOutHandler: Handler? = null

    private val args: LoadingDeviceFragmentArgs by navArgs()

    private var scanCallBack: ScanCallback? = null
    private var btActionChangeReceiver: BroadcastReceiver? = null

    private var as87Ble :As87BleWrapper? = null

    override fun setResourceId(): Int {
        return R.layout.fragment_loading_device
    }

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
        scanTimeoutHandler = Handler()
        args.bf600Wrapper?.setBleResponseListener(this@LoadingDeviceFragment)
        val manager = activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = manager.adapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        manageUi()
        if (args.deviceData.deviceModel?.contains(BODY_SCALE_NAME) == false) {
            requestPermission()
        }
    }

    private fun manageUi() {
        txt_conn_status?.text =
                resources.getString(
                        R.string.connection_status,
                        args.deviceData.deviceModel,
                        args.deviceData.manufacturer?.name
                )
        iv_logo?.setImageBitmap(args.deviceData.manufacturer?.imageBase64?.toBitmap())

        args.deviceData.manufacturer?.display?.let {
            txt_conn_msg_4.text = it
        }
    }

    private fun requestPermission() {
        val locationPermission = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)

        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION
                    ), ENABLE_LOCATIONS_REQUEST_ID)
        } else {
            checkIfBluetoothIsEnabled()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ENABLE_LOCATIONS_REQUEST_ID) {
            if (permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkIfBluetoothIsEnabled()
            } else {
                findNavController().popBackStack()
            }
        }
    }

    private fun checkIfBluetoothIsEnabled() {
        if (bluetoothAdapter == null || !bluetoothAdapter!!.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBtIntent, ENABLE_BT_REQUEST_ID)
        } else if (bluetoothAdapter!!.isEnabled) {
            startScan()
        }
        if (!activity.packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(getActivity(), "BLE is not supported", Toast.LENGTH_SHORT)
                    .show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == ENABLE_BT_REQUEST_ID) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    startScan()
                } catch (e: Exception) {
                }
            } else {
                findNavController().popBackStack()
            }
        }
    }

    private fun startScan() {
        bluetoothLeScanner = bluetoothAdapter!!.bluetoothLeScanner

        initScanCallback(args.deviceData, WeakReference(this), WeakReference(bluetoothLeScanner), WeakReference(scanTimeoutHandler), WeakReference<LoadingDeviceFragment>(this))

        val settings = ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .build()
        bluetoothLeScanner!!.startScan(null, settings, scanCallBack)
        scanTimeoutHandler.postDelayed({
            cancelScan()
        }, args.deviceData.getSyncTimeoutMs())
    }


    private fun initScanCallback(deviceData: FhirConfigurationData, weakBleResponseListener: WeakReference<BleResponseListener>, bluetoothLeScanner: WeakReference<BluetoothLeScanner?>, scanTimeoutHandler: WeakReference<Handler>, weakFragment: WeakReference<LoadingDeviceFragment>) {
        scanCallBack = object : ScanCallback() {
            override fun onScanResult(callbackType: Int, result: ScanResult) {
                super.onScanResult(callbackType, result)
                //Timber.d("onScanResultDeviceName %s", result.device.name)
                //Timber.d("onScanResultDeviceAddress %s", result.device.address)

                if (result.device.name != null
                        && (result.device.name.contains(deviceData.deviceModel.toString())
                                || result.device.name.contains(SPIROMETER_ALTERNATE_NAME))) {
                    weakFragment.get()?.registerConnectionTimeoutHandler()

                    scanTimeoutHandler.get()?.removeCallbacksAndMessages(null)
                    bluetoothLeScanner.get()?.stopScan(this)

                    if (result.device == null) {
                        weakFragment.get()?.deviceAlreadyExistsError()

                    } else if (result.device != null) {
                        when {
                            result.device.name.contains(SPIROMETER_ALTERNATE_NAME) -> {
                                if (weakFragment.get()?.callOnce == true) {
                                    weakFragment.get()?.callOnce = false
                                    Timber.d("onScanResultLUNGFound %s", result.device.address)

                                    val context = weakFragment.get()?.context
                                    val listener = weakBleResponseListener.get()

                                    if (context != null && listener != null) {
                                        val lungWrapperLe = LungBleWrapper(WeakReference(context), result.device.address)
                                        lungWrapperLe.setBleResponseListener(weakBleResponseListener)
                                        lungWrapperLe.connect(false)
                                    }
                                }
                            }

                            result.device.name.contains(AS87) -> {
                                if (weakFragment.get()?.callOnce == true) {
                                    weakFragment.get()?.callOnce = false
                                    Timber.d("onScanResultAS87Found %s", result.device.address)

                                    val context = weakFragment.get()?.context
                                    val listener = weakBleResponseListener.get()

                                    if (context != null && listener != null) {
                                        weakFragment.get()?.as87Ble = As87BleWrapper(WeakReference(context), result.device.address)
                                        weakFragment.get()?.as87Ble?.setBleResponseListener(weakBleResponseListener)
                                        weakFragment.get()?.as87Ble?.connect(false)
                                    }
                                }
                            }

                            result.device.name.contains(BM57) -> {
                                if (weakFragment.get()?.callOnce == true) {
                                    weakFragment.get()?.callOnce = false
                                    Timber.d("onScanResultBM57Found %s", result.device.address)

                                    val context = weakFragment.get()?.context
                                    val listener = weakBleResponseListener.get()

                                    if (context != null && listener != null) {
                                        val bm57ble = Bm57BleWrapper(context, result.device.address)
                                        bm57ble.setBleResponseListener(listener)
                                    }
                                }
                            }

                            result.device.name.contains(GL50) -> {
                                if (weakFragment.get()?.callOnce == true) {
                                    weakFragment.get()?.callOnce = false
                                    Timber.d("onScanResultGL50Found %s", result.device.address)

                                    val context = weakFragment.get()?.context
                                    val listener = weakBleResponseListener.get()

                                    if (context != null && listener != null) {
                                        val gl50ble = Gl50BleWrapper(context, result.device.address)
                                        gl50ble.setBleResponseListener(listener)
                                    }
                                }
                            }

                            else -> {
                                if (weakFragment.get()?.callOnce == true) {
                                    weakFragment.get()?.callOnce = false

                                    val context = weakFragment.get()?.context
                                    val listener = weakBleResponseListener.get()

                                    if (context != null && listener != null) {
                                        val po60Ble = Po60BleWrapper(WeakReference(context), result.device.address)
                                        po60Ble.setBleResponseListener(weakBleResponseListener)
                                        po60Ble.connect(false)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            override fun onScanFailed(errorCode: Int) {
                super.onScanFailed(errorCode)
                weakFragment.get()?.context?.let {
                    MdocUtil.showToastLong(it, errorCode.toString())
                }
            }
        }
    }

    private fun registerConnectionTimeoutHandler() {
        if (connectionTimeOutHandler == null) {
            connectionTimeOutHandler = Handler()
            connectionTimeOutHandler!!.postDelayed({
                Toast.makeText(context, resources.getText(R.string.bluetooth_timeout), Toast.LENGTH_LONG)
                        .show()
                navigateBackToStartFragment()
            }, args.deviceData.getConnectionTimeoutMs())
        }
    }

    private fun navigateBackToStartFragment() {
        if (isAdded) {
            val isBackToInitializeBf600Fragment = findNavController().popBackStack(R.id.fragmentInitializeBf600, true)
            if (!isBackToInitializeBf600Fragment) {
                findNavController().popBackStack()
            }
        }
    }

    override fun dataFromDeviceCallback(deviceData: ArrayList<Byte>, gatt: BluetoothGatt) {
        as87Ble?.removeBleResponseListener()
        val factory = BluetoothDeviceFactory()
        val device = factory.createDevice(gatt, deviceData)
        responseFromDeviceHandler(device)
    }

    private fun getDevicesForUser(device: BluetoothDevice) {
        val request = DevicesForUserRequest()
        request.deviceId = FirebaseInstanceId.getInstance().id

        MdocManager.getDevicesForUser(request,
                object : Callback<DevicesResponse> {
                    override fun onResponse(call: Call<DevicesResponse>,
                                            response: Response<DevicesResponse>) {
                        if (response.isSuccessful) {
                            val macAddresses = DevicesUtil.filterMacAddresses(response.body()?.data)
                            if (!macAddresses.contains(device.deviceAddress())) {
                                createDeviceApi(device)
                            } else {
                                deviceAlreadyExistsError()
                            }
                        }
                    }

                    override fun onFailure(call: Call<DevicesResponse>, t: Throwable) {
                    }
                })
    }

    private fun deviceAlreadyExistsError() {
        activity.runOnUiThread {
            if (isAdded) {
                MdocUtil.showToastLong(activity, activity.resources.getString(R.string.err_device_already_added))

                val isBackToInitializeBf600Fragment = findNavController().popBackStack(R.id.fragmentInitializeBf600, true)
                if (!isBackToInitializeBf600Fragment) {
                    findNavController().popBackStack()
                }
            }
        }
    }

    private fun createDeviceApi(device: BluetoothDevice) {
        val request = CreateDeviceRequest()
        val bluetoothId = device.deviceAddress()
        val deviceName: String = when {
            device.deviceName()
                    .contains("LUNG") -> "TIO"
            device.deviceName()
                    .contains("GL50") -> "GL50"
            else -> device.deviceName()
        }

        request.udi = Udi(deviceName)
        request.model = deviceName
        val identifierOne = Identifier("SECONDARY", bluetoothId)
        val identifierTwo = Identifier("OFFICIAL", FirebaseInstanceId.getInstance().id)
        val listIdentifier: List<Identifier> = listOf(identifierOne, identifierTwo)
        request.identifier = listIdentifier

        MdocManager.createDevice(request, object : Callback<SingleDeviceResponse> {
            override fun onResponse(
                    call: Call<SingleDeviceResponse>,
                    response: Response<SingleDeviceResponse>
            ) {
                if (response.isSuccessful) {
                    val deviceId = response.body()?.data?.id ?: "null"
                    val observations =
                            device.getDeviceObservations(device.deviceData(), deviceId, null)

                    postDeviceData(observations)
                } else {
                    Timber.w("createDevice ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<SingleDeviceResponse>, t: Throwable) {
                Timber.w(t, "createDeviceApi")
            }
        })
    }

    private fun responseFromDeviceHandler(device: BluetoothDevice) {
        if (args.deviceId.isEmpty()) {
            getDevicesForUser(device)
        } else {
            val observations = device.getDeviceObservations(
                    device.deviceData(),
                    args.deviceId,
                    null)
            postDeviceData(observations)
        }
    }

    private fun postDeviceData(observations: ArrayList<Observation>) {

        val sortedObservations = observations.sortedBy { it.effectiveDateTime }
        MdocManager.createObservations(sortedObservations, object : Callback<MdocResponse> {
            override fun onResponse(call: Call<MdocResponse>, response: Response<MdocResponse>) {
                if (response.isSuccessful) {
                    view?.post {
                        val action = LoadingDeviceFragmentDirections.actionToVitalsNavigation()
                        findNavController().navigate(action)
                    }

                } else {
                    deviceRequestFailed()
                    Timber.w("postDeviceData ${response.getErrorDetails()}")
                }
            }

            override fun onFailure(call: Call<MdocResponse>, t: Throwable) {
                deviceRequestFailed()
                Timber.w(t, "postDeviceData")
            }
        })
    }

    private fun cancelScan() {
        activity.runOnUiThread {
            if (isAdded) {
                Toast.makeText(context, resources.getText(R.string.bluetooth_timeout), Toast.LENGTH_LONG)
                        .show()
                findNavController().popBackStack()
            }
        }
    }

    override fun genericDeviceResponse(list: ArrayList<Byte>?,
                                       gatt: BluetoothGatt?,
                                       message: BleMessage?) {
        activity.runOnUiThread {
            if (message == BleMessage.USER_MEASUREMENT) {
                val device = Bf600Device(gatt, list)

                responseFromDeviceHandler(device)
                args.bf600Wrapper?.disconnect()
            } else if (message == BleMessage.CONNECTION_TIMEOUT) {
                connectionTimeoutAction(this)
            }
        }
    }

    private fun initBroadcastReceiver(fragment: WeakReference<LoadingDeviceFragment>) {
        btActionChangeReceiver = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {
                //Fetching the download id received with the broadcast
                if (intent?.action == android.bluetooth.BluetoothDevice.ACTION_BOND_STATE_CHANGED) {
                    val state = intent.getIntExtra(android.bluetooth.BluetoothDevice.EXTRA_BOND_STATE, -1)
                    if (state == android.bluetooth.BluetoothDevice.BOND_NONE && fragment.get()?.isAdded == true) {
                        fragment.get()?.findNavController()?.popBackStack()
                    }
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        args.bf600Wrapper?.disconnect()
        bluetoothLeScanner?.stopScan(scanCallBack)
        scanTimeoutHandler.removeCallbacksAndMessages(null)
        connectionTimeOutHandler?.removeCallbacksAndMessages(null)
    }

    override fun onStart() {
        super.onStart()
        registerReceiver()
    }

    override fun onStop() {
        super.onStop()
        as87Ble?.removeBleResponseListener()
        unregisterReceiver()
    }

    private fun unregisterReceiver() {
        btActionChangeReceiver?.let { receiver ->
            requireActivity().unregisterReceiver(receiver)
        }
    }

    private fun registerReceiver() {
        initBroadcastReceiver(WeakReference<LoadingDeviceFragment>(this))
        requireActivity().registerReceiver(btActionChangeReceiver, IntentFilter(android.bluetooth.BluetoothDevice.ACTION_BOND_STATE_CHANGED))
    }

    companion object {
        private const val ENABLE_BT_REQUEST_ID = 1
        private const val ENABLE_LOCATIONS_REQUEST_ID = 2
        private const val BODY_SCALE_NAME = "BF600"
        private const val SPIROMETER_ALTERNATE_NAME = "LUNG"
    }
}
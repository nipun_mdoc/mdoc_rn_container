package de.mdoc.modules.questionnaire.answer_validation

import android.text.InputType
import android.view.View
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class TextAnswerValidator(view: View?,
                          validationKey: String?):
        AbstractValidator() {

    private val validation = PollQuestionTextType.fromKey(validationKey)
    private val editText = view?.findViewById<EditText>(R.id.answerEdt)
    private val textInputLayout = view?.findViewById<TextInputLayout>(R.id.answerWrapper)

    init {
        textInputLayout?.helperText = textInputLayout?.context?.getString(validation.description)

        when (validation) {
            PollQuestionTextType.INTEGER -> editText?.inputType = InputType.TYPE_CLASS_NUMBER
            PollQuestionTextType.DOUBLE  -> editText?.inputType =
                    InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            else                         ->{}
        }

        RxTextView.textChanges(editText as EditText)
            .debounce(100,
                    TimeUnit.MILLISECONDS,
                    AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                isValidAnswer()
            }
    }

    override fun isValidAnswer(): Boolean {
        editText?.let {
            return if (validateFactory(it.text.toString())) {
                setValidUi()
                true
            }
            else {
                setInvalidUi()
                false
            }
        }
        setValidUi()
        return true
    }

    private fun validateFactory(inputText:String): Boolean {
        return when (validation) {
            PollQuestionTextType.INTEGER -> validateInteger(inputText)
            PollQuestionTextType.DOUBLE  -> validateDecimal(inputText)
            else                         -> true
        }
    }

    private fun validateInteger(inputText:String): Boolean {
        return try {
            if (inputText.isEmpty()) {
                return true
            }
            else {
                inputText.toInt()
                setValidUi()
                true
            }
        } catch (e: NumberFormatException) {
            setInvalidUi()
            false
        }
    }

    private fun validateDecimal(inputText:String): Boolean {
        try {
            if (inputText.isEmpty()) {
                return true
            }
            else if (inputText.contains(".")) {
                val indexOfDecimal: Int = inputText.indexOf(".")
                val decimals = inputText.substring(indexOfDecimal)
                if (decimals.length<2) {
                    return false
                }
            }
            else if (inputText.contains(",")) {
                val indexOfDecimal: Int = inputText.indexOf(",")
                val decimals = inputText.substring(indexOfDecimal)
                if (decimals.length<2) {
                    return false
                }
            }else{
                return false
            }
            inputText.toDouble()
            return true
        } catch (e: NumberFormatException) {
            return false
        }
    }

    private fun setValidUi() {
        textInputLayout?.isErrorEnabled = false
    }

    private fun setInvalidUi() {
        textInputLayout?.isErrorEnabled = true
        textInputLayout?.errorIconDrawable = null
        textInputLayout?.error = textInputLayout?.context?.getString(validation.description)
    }
}
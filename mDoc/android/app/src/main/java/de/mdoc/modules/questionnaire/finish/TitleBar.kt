package de.mdoc.modules.questionnaire.finish

import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.mdoc.R
import de.mdoc.viewmodel.compositelist.ListItemTypeAdapter
import de.mdoc.viewmodel.compositelist.ListItemViewHolder
import kotlinx.android.synthetic.main.layout_questionnaire_finish_title.view.*

class FinishQuestionnaireTitle(
    val closingMessage: String,
    val showScoringResult: Boolean
)

object TitleBar : ListItemTypeAdapter<FinishQuestionnaireTitle, ListItemViewHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): ListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ListItemViewHolder(
            inflater.inflate(R.layout.layout_questionnaire_finish_title, parent, false)
        )
    }

    override fun onBind(position: Int, item: FinishQuestionnaireTitle, holder: ListItemViewHolder) {
        holder.itemView.closingMessage.text = Html.fromHtml(item.closingMessage)

        if (!item.showScoringResult) {
            holder.itemView.layoutNonScoring.visibility = View.VISIBLE
        }
    }
}
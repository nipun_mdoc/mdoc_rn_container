package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.MealChoice;

/**
 * Created by ema on 10/18/17.
 */

public class MealChoiceResponse extends MdocResponse {

    private ArrayList<MealChoice> data;

    public ArrayList<MealChoice> getData() {
        return data;
    }

    public void setData(ArrayList<MealChoice> data) {
        this.data = data;
    }
}

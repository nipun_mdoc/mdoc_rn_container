package de.mdoc.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import java.util.List;

import de.mdoc.R;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.newlogin.Interactor.LogiActivty;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import de.mdoc.util.token_otp.ScanAsyncTask;
import timber.log.Timber;

public class ScanActivity extends AppCompatActivity implements SurfaceHolder.Callback {
    private final Camera.CameraInfo mCameraInfo = new Camera.CameraInfo();
    private final ScanAsyncTask mScanAsyncTask;
    private final int mCameraId;
    private Handler mHandler;
    private Camera mCamera;


    private static class AutoFocusHandler extends Handler implements Camera.AutoFocusCallback {
        private final Camera mCamera;

        public AutoFocusHandler(Camera camera) {
            mCamera = camera;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            mCamera.autoFocus(this);
        }

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            sendEmptyMessageDelayed(0, 1000);
        }
    }


    private static int findCamera(Camera.CameraInfo cameraInfo) {
        int cameraId = Camera.getNumberOfCameras();

        // Find a back-facing camera. Otherwise, use the first camera.
        while (cameraId-- > 0) {
            Camera.getCameraInfo(cameraId, cameraInfo);
            if (cameraId == 0 || cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK)
                break;
        }

        return cameraId;
    }

    @SuppressLint({"StaticFieldLeak", "Assert"})
    public ScanActivity() {
        super();

        mCameraId = findCamera(mCameraInfo);
        assert mCameraId >= 0;

        // Create the decoder thread
        mScanAsyncTask = new ScanAsyncTask() {
            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                if (result.contains("otpauth://totp/Keycloak:")) {
                    MdocAppHelper.getInstance().setSecretKey(result);
                    MdocUtil.showToastLong(ScanActivity.this, getString(R.string.successfully_paired));
                    logout();
                } else {
                    onBackPressed();
                    MdocUtil.showToastLong(ScanActivity.this, getString(R.string.qr_code_error_msg));
                }
            }

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Timber.w("onPreExecute");
            }
        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_scan);
        mScanAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mScanAsyncTask.cancel(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        ((SurfaceView) findViewById(R.id.surfaceview)).getHolder().addCallback(this);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mCamera == null)
            return;

        // The code in this section comes from the developer docs. See:
        // http://developer.android.com/reference/android/hardware/Camera.html#setDisplayOrientation(int)

        int rotation = 0;
        switch (getWindowManager().getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_0:
                rotation = 0;
                break;
            case Surface.ROTATION_90:
                rotation = 90;
                break;
            case Surface.ROTATION_180:
                rotation = 180;
                break;
            case Surface.ROTATION_270:
                rotation = 270;
                break;
        }

        int result = 0;
        switch (mCameraInfo.facing) {
            case Camera.CameraInfo.CAMERA_FACING_FRONT:
                result = (mCameraInfo.orientation + rotation) % 360;
                result = (360 - result) % 360; // compensate the mirror
                break;

            case Camera.CameraInfo.CAMERA_FACING_BACK:
                result = (mCameraInfo.orientation - rotation + 360) % 360;
                break;
        }

        mCamera.setDisplayOrientation(result);
        mCamera.startPreview();

        if (mHandler != null)
            mHandler.sendEmptyMessageDelayed(0, 100);
    }

    @Override
    @TargetApi(14)
    public void surfaceCreated(SurfaceHolder holder) {
        surfaceDestroyed(holder);

        try {
            // Open the camera
            mCamera = Camera.open(mCameraId);
            mCamera.setPreviewDisplay(holder);
            mCamera.setPreviewCallback(mScanAsyncTask);
        } catch (Exception e) {
            e.printStackTrace();
            surfaceDestroyed(holder);

            // Show error message
            findViewById(R.id.surfaceview).setVisibility(View.INVISIBLE);
            findViewById(R.id.progress).setVisibility(View.INVISIBLE);
            findViewById(R.id.window).setVisibility(View.INVISIBLE);
            findViewById(R.id.textview).setVisibility(View.VISIBLE);
            return;
        }

        // Set auto-focus mode
        Camera.Parameters params = mCamera.getParameters();
        List<String> modes = params.getSupportedFocusModes();
        if (modes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE))
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        else if (modes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO))
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        else if (modes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
            mHandler = new AutoFocusHandler(mCamera);
        }
        mCamera.setParameters(params);
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera == null)
            return;

        if (mHandler != null) {
            mCamera.cancelAutoFocus();
            mHandler.removeMessages(0);
            mHandler = null;
        }

        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mCamera.release();
        mCamera = null;
    }

    private void logout() {
        MdocManager.logout();
        Intent loginIntent;
        if(getResources().getBoolean(R.bool.has_keycloak_decoupling)) {
            loginIntent = new Intent(this, LogiActivty.class);
        }else{
            loginIntent = new Intent(this, LoginActivity.class);
        }
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
        finish();
    }
}

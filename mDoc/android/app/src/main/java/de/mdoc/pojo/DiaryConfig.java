package de.mdoc.pojo;

public class DiaryConfig {

    private String id;
    private Long cts;
    private Long uts;
    private boolean freeTextEntryAllowed;
    private boolean shareAllowed;
    private boolean autoShare;
    private Coding coding;
    private String title;
    private String pollId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getCts() {
        return cts;
    }

    public void setCts(Long cts) {
        this.cts = cts;
    }

    public Long getUts() {
        return uts;
    }

    public void setUts(Long uts) {
        this.uts = uts;
    }

    public boolean isFreeTextEntryAllowed() {
        return freeTextEntryAllowed;
    }

    public void setFreeTextEntryAllowed(boolean freeTextEntryAllowed) {
        this.freeTextEntryAllowed = freeTextEntryAllowed;
    }

    public boolean isShareAllowed() {
        return shareAllowed;
    }

    public void setShareAllowed(boolean shareAllowed) {
        this.shareAllowed = shareAllowed;
    }

    public boolean isAutoShare() {
        return autoShare;
    }

    public void setAutoShare(boolean autoShare) {
        this.autoShare = autoShare;
    }

    public Coding getCoding() {
        return coding;
    }

    public void setCoding(Coding coding) {
        this.coding = coding;
    }
}

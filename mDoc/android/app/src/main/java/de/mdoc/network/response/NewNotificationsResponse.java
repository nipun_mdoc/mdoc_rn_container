package de.mdoc.network.response;

import java.util.ArrayList;

import de.mdoc.pojo.NewNotificationsData;

public class NewNotificationsResponse extends MdocResponse {

    private ArrayList<NewNotificationsData> data;

    public ArrayList<NewNotificationsData> getData() {
        return data;
    }

    public void setData(ArrayList<NewNotificationsData> data) {
        this.data = data;
    }
}

package de.mdoc.modules.checklist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import de.mdoc.R;
import de.mdoc.modules.checklist.ChecklistFragmentDirections;
import de.mdoc.pojo.ChecklistItem;

/**
 * Created by AdisMulabdic on 9/12/17.
 */

public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistViewHolder> {
    public Context context;
    public ArrayList<ChecklistItem> items;
    public NavController navController;

    public ChecklistAdapter(Context context, ArrayList<ChecklistItem> items, NavController navController) {
        this.context = context;
        this.items = items;
        this.navController = navController;
    }

    @Override
    public ChecklistViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checklist_item, parent, false);
        context = parent.getContext();
        return new ChecklistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ChecklistViewHolder holder, int position) {
        final ChecklistItem item = items.get(position);
        holder.updateChecklist(item, context);

        holder.checklistLinear.setOnClickListener(view -> {
            NavDirections action = ChecklistFragmentDirections.Companion.actionChecklistFragmentToChecklistDetailsFragment(item);
            navController.navigate(action);
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}

package de.mdoc.modules.files.storage.view

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import de.mdoc.R
import de.mdoc.modules.files.Config
import de.mdoc.network.response.files.ResponseSearchResultsFileStorageDocumentImpl_
import de.mdoc.util.animation.ProgressBarAnimation
import kotlinx.android.synthetic.main.view_storage_linear_progress.view.*


class StorageFileCounterView(context: Context?, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_storage_linear_progress, this)
        txtStorageCounter.text = resources.getString(R.string.files_storage_counter, 0, Config.MAX_UPLOAD_COUNT)
    }

    fun setValue(response: ResponseSearchResultsFileStorageDocumentImpl_) {
        val storage = response.data?.list?.last()
        val current = storage?.fileUploadsTotalCount ?: 0
        val max = Config.MAX_UPLOAD_COUNT

        // Progress calculation
        val calculation: Float = current.toFloat() / max
        val progress = calculation * 100
        storageCounterIndicator.progress = progress.toInt()

        // Set file counter value
        txtStorageCounter.text = resources.getString(R.string.files_storage_counter, current, max)

        // Change progress color based on percentage
        if (progress >= Config.PERCENTAGE_RED_STATE) {
            storageCounterIndicator.progressDrawable = context.getDrawable(R.drawable.red_progress_rounded_corners)
        } else {
            storageCounterIndicator.progressDrawable = context.getDrawable(R.drawable.primary_progress_rounded_corners)
        }

        // Animate
        val anim = ProgressBarAnimation(storageCounterIndicator, 0.0f, progress)
        anim.duration = Config.STORAGE_ANIMATION_DURATION
        storageCounterIndicator.startAnimation(anim)
    }
}
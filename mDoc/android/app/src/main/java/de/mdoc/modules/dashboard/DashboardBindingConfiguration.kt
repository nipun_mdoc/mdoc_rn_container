package de.mdoc.modules.dashboard

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import de.mdoc.MainNavDirections
import de.mdoc.NavigationTherapyPlanDirections
import de.mdoc.R
import de.mdoc.databinding.FragmentDashboardBinding
import de.mdoc.modules.appointments.AppointmentFragmentDirections
import de.mdoc.modules.appointments.AppointmentWidgetViewModel
import de.mdoc.modules.appointments.adapters.DayAppointmentAdapter
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.modules.messages.news_widget.NewsWidgetHandler
import de.mdoc.modules.messages.news_widget.NewsWidgetViewModel
import de.mdoc.modules.covid19.widget.Covid19WidgetHandler
import de.mdoc.modules.covid19.widget.Covid19WidgetViewModel
import de.mdoc.modules.dashboard.welcome_widget.WelcomeWidgetHandler
import de.mdoc.modules.dashboard.welcome_widget.WelcomeWidgetViewModel
import de.mdoc.modules.meal_plan.MealPlanWidgetHandler
import de.mdoc.modules.meal_plan.view_model.MealWidgetViewModel
import de.mdoc.modules.medications.MedicationsHandler
import de.mdoc.modules.medications.MedicationsViewModel
import de.mdoc.modules.medications.medication_widget.MedicationWidgetViewModel
import de.mdoc.modules.mental_goals.goals_widget.GoalsWidgetHandler
import de.mdoc.modules.mental_goals.goals_widget.GoalsWidgetViewModel
import de.mdoc.modules.messages.widget.ConversationsWidgetHandler
import de.mdoc.modules.messages.widget.ConversationsWidgetViewModel
import de.mdoc.modules.my_stay.widget.MyStayWidgetViewModel
import de.mdoc.modules.patient_journey.PatientJourneyHandler
import de.mdoc.modules.patient_journey.PatientJourneyWidgetViewModel
import de.mdoc.modules.vitals.vitals_widget.VitalsWidgetHandler
import de.mdoc.modules.vitals.vitals_widget.VitalsWidgetViewModel
import de.mdoc.network.RestClient
import de.mdoc.pojo.AppointmentDetails
import de.mdoc.util.MdocAppHelper

class DashboardBindingConfiguration: DayAppointmentAdapter.AppointmentDayCallback {

    fun bindConfiguration(binding: FragmentDashboardBinding,
                          context: Context) {
        // Widget can't be enabled without has_my_stay module enabled, but can be disabled from the dashboard if needed.
        val isMyStayWidgetEnabled = context.resources.getBoolean(R.bool.has_my_stay) && context.resources.getBoolean(
                R.bool.has_my_stay_widget)
        if (isMyStayWidgetEnabled) {
            val myStayWidgetViewModel = MyStayWidgetViewModel(RestClient.getService())
            binding.stayViewModel = myStayWidgetViewModel
        }

        binding.isMyStayWidgetEnabled = isMyStayWidgetEnabled

        if (context.resources.getBoolean(R.bool.has_therapy)) {
            binding.appointmentViewModel = AppointmentWidgetViewModel(context)
            binding.appointmentItemCallback = this
        }
        if (context.resources.getBoolean(R.bool.has_meal)) {
            binding.mealViewModel = MealWidgetViewModel(context)
            binding.mealPlanWidgetHandler = MealPlanWidgetHandler(context.resources)
        }
        if (context.resources.getBoolean(R.bool.has_patient_journey_widget)) {
            val patientJourneyWidgetViewModel = PatientJourneyWidgetViewModel(RestClient.getService())
            binding.journeyWidgetViewModel = patientJourneyWidgetViewModel
            binding.patientJourneyWidgetHandler = PatientJourneyHandler()
        }
        if (context.resources.getBoolean(R.bool.has_messages_widget)) {
            val conversationsViewModel = ConversationsWidgetViewModel(RestClient.getService())
            binding.conversationsWidgetViewmodel = conversationsViewModel
            binding.conversationHandler = ConversationsWidgetHandler()
        }
        if (context.resources.getBoolean(R.bool.has_medication_widget)) {
            val medicationWidgetViewModel = MedicationWidgetViewModel(RestClient.getService())
            binding.medicationWidgetViewModel = medicationWidgetViewModel
            medicationWidgetViewModel.getUpcomingMedicationAdministration()
            val medicationsViewModel = MedicationsViewModel(RestClient.getService())
            binding.medicationsViewModel = medicationsViewModel
            binding.medicationsHandler = MedicationsHandler(medicationsViewModel, medicationWidgetViewModel)
        }
        if (context.resources.getBoolean(R.bool.has_mental_goals_widget)) {
            val goalsWidgetViewModel = GoalsWidgetViewModel(RestClient.getService())
            goalsWidgetViewModel.getGoalsData()
            binding.goalsWidgetViewModel = goalsWidgetViewModel
            binding.goalsWidgetHandler = GoalsWidgetHandler()
        }

        if (context.resources.getBoolean(R.bool.has_vitals_widget)) {
            val vitalsWidgetViewModel = VitalsWidgetViewModel(RestClient.getService())
            binding.vitalsWidgetViewModel = vitalsWidgetViewModel
            vitalsWidgetViewModel.getObservations()
            binding.vitalsWidgetHandler = VitalsWidgetHandler()
        }
        if (context.resources.getBoolean(R.bool.has_news_widget)) {
            val newsWidgetViewModel =
                    NewsWidgetViewModel(
                            RestClient.getService())
            newsWidgetViewModel.getNews()
            binding.newsWidgetViewModel = newsWidgetViewModel
            binding.newsWidgetHandler =
                    NewsWidgetHandler()
        }
        if (context.resources.getBoolean(R.bool.has_covid19_widget)) {
            val covid19WidgetViewModel = Covid19WidgetViewModel(RestClient.getService())
            binding.covid19WidgetViewModel = covid19WidgetViewModel
            covid19WidgetViewModel.getHealthStatus()
            binding.covid19WidgetHandler = Covid19WidgetHandler()
        }

    }

    fun bindLowPermissionWidgets (binding: FragmentDashboardBinding,
                                  context: Context) {
        if (!MdocAppHelper.getInstance().isClosed) {
            val welcomeWidgetViewModel = WelcomeWidgetViewModel(context, RestClient.getService())
            binding.welcomeWidgetViewModel = welcomeWidgetViewModel
            binding.welcomeWidgetHandler = WelcomeWidgetHandler()
        }
    }

    override fun onDayItemClicked(item: AppointmentDetails, view: View?) {
        showAppointment(item, view)
    }

    override fun showAppointmentDetails(item: AppointmentDetails, view: View?) {
        showAppointment(item, view)
    }

    private fun showAppointment(item: AppointmentDetails, view: View?) {
        val booking = BookingAdditionalFields(
                specialtyProvider = item.specialtyProvider,
                appointmentId = item.appointmentId,
                clinicId = item.clinicId,
                specialityName = item.specialty,
                appointmentType = item.title)

        val action = MainNavDirections.globalActionToAppointmentDetails(item, booking)
        view?.findNavController()?.navigate(action)
    }
}
package de.mdoc.pojo;

import java.io.Serializable;

public class TenantPublicDocument implements Serializable {

    private boolean twoFactorEnabled;
    private FileConfig fileConfig;

    public boolean isTwoFactorEnabled() {
        return twoFactorEnabled;
    }

    public void setTwoFactorEnabled(boolean twoFactorEnabled) {
        this.twoFactorEnabled = twoFactorEnabled;
    }

    public FileConfig getFileConfig() {
        return fileConfig;
    }

    public void setFileConfig(FileConfig fileConfig) {
        this.fileConfig = fileConfig;
    }
}

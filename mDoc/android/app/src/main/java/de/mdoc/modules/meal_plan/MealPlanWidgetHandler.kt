package de.mdoc.modules.meal_plan

import android.content.res.Resources
import de.mdoc.pojo.MealOffer

class MealPlanWidgetHandler (val resources: Resources) {

    fun getMealName (item: MealOffer): String {
        return item.getName(resources)
    }
}
package de.mdoc.pojo;

import java.util.HashMap;

public class ConfigurationDetailsMultiValue {

    private HashMap<String, String> ADDITIVES;
    private HashMap<String, String> ALLERGENS;

    public HashMap<String, String> getADDITIVES() {
        return ADDITIVES;
    }

    public void setADDITIVES(HashMap<String, String> ADDITIVES) {
        this.ADDITIVES = ADDITIVES;
    }

    public HashMap<String, String> getALLERGENS() {
        return ALLERGENS;
    }

    public void setALLERGENS(HashMap<String, String> ALLERGENS) {
        this.ALLERGENS = ALLERGENS;
    }
}

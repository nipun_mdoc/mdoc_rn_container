package de.mdoc.pojo;

import java.io.Serializable;

import javax.annotation.Nullable;

public class DiaryList implements Serializable {

    private String id;
    private String ownerUsername;
    private String clinicId;
    private String title;
    private String description;
    private Boolean showToDoctor;
    private long start;
    private long end;
    private long created;
    private long updated;
    @Nullable
    private DiaryConfig diaryConfig;
    private int itemViewType;
    private int monthValue;
    private String pollVotingActionId;

    public DiaryList(int itemViewType, int monthValue) {
        this.itemViewType = itemViewType;
        this.monthValue = monthValue;
    }

    public String getPollVotingActionId() {
        return pollVotingActionId;
    }

    public void setPollVotingActionId(String pollVotingActionId) {
        this.pollVotingActionId = pollVotingActionId;
    }

    public int getMonthValue() {
        return monthValue;
    }

    public void setMonthValue(int monthValue) {
        this.monthValue = monthValue;
    }

    @Nullable
    public DiaryConfig getDiaryConfig() {
        return diaryConfig;
    }

    public void setDiaryConfig(DiaryConfig diaryConfig) {
        this.diaryConfig = diaryConfig;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getShowToDoctor() {
        return showToDoctor;
    }

    public void setShowToDoctor(Boolean showToDoctor) {
        this.showToDoctor = showToDoctor;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getUpdated() {
        return updated;
    }

    public void setUpdated(long updated) {
        this.updated = updated;
    }

    public int getItemViewType() {
        return itemViewType;
    }

    public void setItemViewType(int itemViewType) {
        this.itemViewType = itemViewType;
    }
}

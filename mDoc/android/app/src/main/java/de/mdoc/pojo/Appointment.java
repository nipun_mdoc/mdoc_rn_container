package de.mdoc.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ema on 11/15/16.
 */

public class Appointment implements Serializable{

    protected String name;
    @SerializedName("doctor")
    protected Doctor doctor;
    @SerializedName("clinic")
    protected Clinic clinic;
    protected String from;
    protected String to;
    protected boolean changed;
    protected boolean newTherapy;
    protected boolean today;
    protected String note;

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Clinic getClinic() {
        return clinic;
    }

    public void setClinic(Clinic clinic) {
        this.clinic = clinic;
    }

    public boolean isChanged() {
        return changed;
    }

    public void setChanged(boolean changed) {
        this.changed = changed;
    }

    public boolean isNewTherapy() {
        return newTherapy;
    }

    public void setNewTherapy(boolean newTherapy) {
        this.newTherapy = newTherapy;
    }

    public boolean isToday() {
        return today;
    }

    public void setToday(boolean today) {
        this.today = today;
    }
}

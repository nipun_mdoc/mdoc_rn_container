package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListData implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("confirmationRequired")
    @Expose
    private boolean confirmationRequired;
    @SerializedName("appointmentDetails")
    @Expose
    private AppointmentDetails appointmentDetails;
    @SerializedName("deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("cts")
    @Expose
    private long cts;
    @SerializedName("uts")
    @Expose
    private long uts;
    @SerializedName("integrationType")
    @Expose
    private String integrationType;
    @SerializedName("newVersionReferenceId")
    @Expose
    private String newVersionReferenceId;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("encounter")
    @Expose
    private ArrayList<EncounterData> encounter;
    @SerializedName("metadataNeeded")
    @Expose
    public Boolean metadataNeeded = null;
    @SerializedName("metadataPopulated")
    @Expose
    public Boolean metadataPopulated = null;

    private boolean firstItem;

    public boolean isFirstItem() {
        return firstItem;
    }

    public void setFirstItem(boolean firstItem) {
        this.firstItem = firstItem;
    }

    public Boolean getMetadataNeeded() {
        return metadataNeeded;
    }

    public void setMetadataNeeded(Boolean metadataNeeded) {
        this.metadataNeeded = metadataNeeded;
    }

    public Boolean getMetadataPopulated() {
        return metadataPopulated;
    }

    public void setMetadataPopulated(Boolean metadataPopulated) {
        this.metadataPopulated = metadataPopulated;
    }

    public String getNewVersionReferenceId() {
        return newVersionReferenceId;
    }

    public void setNewVersionReferenceId(String newVersionReferenceId) {
        this.newVersionReferenceId = newVersionReferenceId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIntegrationType() {
        return integrationType;
    }

    public void setIntegrationType(String integrationType) {
        this.integrationType = integrationType;
    }

    public boolean isConfirmationRequired() {
        return confirmationRequired;
    }

    public void setConfirmationRequired(boolean confirmationRequired) {
        this.confirmationRequired = confirmationRequired;
    }

    public long getCts() {
        return cts;
    }

    public void setCts(long cts) {
        this.cts = cts;
    }

    public long getUts() {
        return uts;
    }

    public void setUts(long uts) {
        this.uts = uts;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AppointmentDetails getAppointmentDetails() {
        return appointmentDetails;
    }

    public void setAppointmentDetails(AppointmentDetails appointmentDetails) {
        this.appointmentDetails = appointmentDetails;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public ArrayList<EncounterData> getEncounter() {
        return encounter;
    }

    public void setEncounter(ArrayList<EncounterData> encounter) {
        this.encounter = encounter;
    }
}

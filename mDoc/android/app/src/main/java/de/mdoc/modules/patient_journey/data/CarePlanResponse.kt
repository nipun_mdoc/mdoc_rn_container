package de.mdoc.modules.patient_journey.data

data class CarePlanResponse(
        val code: String? = "",
        val data: CarePlanData? = CarePlanData(),
        val message: String? = "",
        val timestamp: Long? = 0
                           )

data class CarePlanData(
        val list: List<CarePlan>? = listOf(),
        val moreDataAvailable: Boolean? = false,
        val totalCount: Int? = 0
                       )

data class CarePlan(
        val activity: List<Activity>? = listOf(),
        val addresses: List<Any?>? = listOf(),
        val author: List<Any?>? = listOf(),
        val basedOn: List<BasedOn?>? = listOf(),
        val careTeam: List<Any?>? = listOf(),
        val caseId: String? = "",
        val category: List<Category?>? = listOf(),
        val childCarePlanList: ArrayList<ChildCarePlan>? = arrayListOf(),
        val cts: Long? = 0,
        val definition: List<Any?>? = listOf(),
        val description: String? = "",
        val goal: List<Any?>? = listOf(),
        val id: String? = "",
        val identifier: List<Any?>? = listOf(),
        val meta: Meta? = Meta(),
        val note: List<Any?>? = listOf(),
        val partOf: List<Any?>? = listOf(),
        val period: Period? = Period(),
        val replaces: List<Any?>? = listOf(),
        val subject: Subject? = Subject(),
        val subjectIds: List<Any?>? = listOf(),
        val supportingInfo: List<Any?>? = listOf(),
        val title: String? = "",
        val uts: Long? = 0
                   ) {

    fun codingDisplayBySystem(query: String): String {
        var result = ""

        category?.forEach { category ->
            category?.coding?.forEach { coding ->
                if (coding?.system == query) {
                    result = coding.display ?: ""
                }
            }
        }

        return result
    }
}

data class ChildCarePlan(
        val activity: List<Activity>? = listOf(),
        val addresses: List<Any?>? = listOf(),
        val author: List<Any?>? = listOf(),
        val basedOn: List<Any?>? = listOf(),
        val careTeam: List<Any?>? = listOf(),
        val category: List<Category?>? = listOf(),
        val childCarePlanList: List<Any?>? = listOf(),
        val cts: Long? = 0,
        val definition: List<Any?>? = listOf(),
        val description: String? = "",
        val goal: List<Any?>? = listOf(),
        val id: String? = "",
        val identifier: List<Any?>? = listOf(),
        val note: List<Any?>? = listOf(),
        val partOf: List<PartOf?>? = listOf(),
        val period: Period = Period(),
        val replaces: List<Any?>? = listOf(),
        val subject: Subject? = Subject(),
        val subjectIds: List<Any?>? = listOf(),
        val supportingInfo: List<Any?>? = listOf(),
        val title: String? = "",
        val uts: Long? = 0,
        var preOrPost: String? = null,
        var isExpanded: Boolean = false
                        ) {

    fun codingDisplayBySystem(query: String): String {
        var result = ""

        category?.forEach { category ->
            category?.coding?.forEach { coding ->
                if (coding?.system == query) {
                    result = coding.code ?: ""
                }
            }
        }

        return result
    }
}

data class PartOf(
        val reference: String? = ""
                 )

data class Activity(
        val detail: Detail? = Detail(),
        val modifierExtension: List<Any?>? = listOf(),
        val outcomeCodeableConcept: List<Any?>? = listOf(),
        val outcomeReference: List<Any?>? = listOf(),
        val progress: List<Any?>? = listOf()
                   )

data class Detail(
        val code: Code? = Code(),
        val goal: List<Any?>? = listOf(),
        val kind: Kind? = Kind(),
        val modifierExtension: List<Any?>? = listOf(),
        val performer: List<Any?>? = listOf(),
        val reasonCode: List<Any?>? = listOf(),
        val reasonReference: List<ReasonReference?>? = listOf()
                 )

data class Code(
        val coding: List<Any?>? = listOf()
               )

data class Kind(
        val active: Boolean? = false,
        val code: String? = "",
        val system: String? = ""
               )

data class ReasonReference(
        val display: String? = "",
        val reference: String? = ""
                          )

data class BasedOn(
        val reference: String? = ""
                  )

data class Category(
        val coding: List<Coding?>? = listOf()
                   ) {
}

data class Coding(
        val active: Boolean? = false,
        val code: String? = "",
        val display: String? = "",
        val system: String? = ""
                 )

data class Meta(
        val lastUpdated: Long? = 0,
        val profile: List<Any?>? = listOf(),
        val security: List<Any?>? = listOf(),
        val tag: List<Any?>? = listOf()
               )

data class Period(
        val end: Long = 0,
        val start: Long = 0,
        val timingPeriod:Int=0,
        val timingPeriodUnit:String="D"
                 )

data class Subject(
        val reference: String? = ""
                  )
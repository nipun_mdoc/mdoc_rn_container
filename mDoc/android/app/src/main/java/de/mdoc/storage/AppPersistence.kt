package de.mdoc.storage

import de.mdoc.data.configuration.ConfigurationDetailsMultiValue
import de.mdoc.modules.devices.data.FhirConfigurationData
import de.mdoc.modules.devices.data.Measurement
import de.mdoc.modules.diary.filter.DiaryEntryType
import de.mdoc.modules.medications.create_plan.data.ListItems
import de.mdoc.pojo.CodingItem

object AppPersistence {

    var medicalDeviceMacAddresses = arrayListOf<String>()
    var deviceConfiguration: ConfigurationDetailsMultiValue =
            ConfigurationDetailsMultiValue(medicals = false, vitals = false)
    var fhirConfigurationData: List<Measurement>? = null
    var fhirDevicesData: List<FhirConfigurationData>? = null
    var diaryConfig: MutableList<DiaryEntryType>? = null
    var medicationUnitsCoding: List<CodingItem>? = null
    var timeCoding: List<ListItems>? = null
    var covidStatusCoding: List<CodingItem>? =  arrayListOf()
    var covidIllnesCoding: List<CodingItem>? =  arrayListOf()
    var relationshipCoding: List<CodingItem> =  arrayListOf()
    var appointmentTypeCoding: List<CodingItem> =  arrayListOf()

    var lastSeenUnitByCode: MutableMap<String, String> = mutableMapOf()
    var latestConnectedGL50Device: Int = -1

    fun clearData() {
        medicalDeviceMacAddresses.clear()
        deviceConfiguration = ConfigurationDetailsMultiValue(medicals = false, vitals = false)
        fhirConfigurationData = null
        fhirDevicesData = null
        diaryConfig = null
        medicationUnitsCoding = null
        timeCoding = null
    }
}
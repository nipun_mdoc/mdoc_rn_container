package de.mdoc.modules.medications.create_plan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.adapters.SpinnerAdapter
import de.mdoc.databinding.FragmentEveryCertainPeriodBinding
import de.mdoc.fragments.NewBaseFragment
import de.mdoc.modules.medications.create_plan.data.CreatePlanViewModel
import de.mdoc.pojo.CodingItem
import kotlinx.android.synthetic.main.fragment_every_certain_period.*
import java.util.*

class EveryCertainPeriodFragment internal constructor(val viewModel: CreatePlanViewModel):
        NewBaseFragment() {

    override val navigationItem: NavigationItem = NavigationItem.MedicationWithHiddenNavigationBar
    lateinit var activity: MdocActivity

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val binding: FragmentEveryCertainPeriodBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_every_certain_period, container, false)
        activity = context as MdocActivity

        binding.lifecycleOwner = this
        binding.createPlanViewModel = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSpinner()
    }

    private fun setupSpinner() {
        val itemList: ArrayList<CodingItem> = arrayListOf(
                CodingItem("", "D", resources.getString(R.string.days)),
                CodingItem("", "WK", resources.getString(R.string.weeks)))
        val spinnerAdapter = SpinnerAdapter(activity, itemList)
        unit_spinner?.adapter = spinnerAdapter

        unit_spinner.onItemSelectedListener = object: AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                viewModel.repeat.value?.periodUnit = itemList.getOrNull(p2)
                    ?.code
                viewModel.periodUnit.value = itemList.getOrNull(p2)
                    ?.code
            }
        }
    }
}

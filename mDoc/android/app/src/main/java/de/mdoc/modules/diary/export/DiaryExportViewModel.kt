package de.mdoc.modules.diary.export

import android.content.Context
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.common.export.BaseExportViewModel
import de.mdoc.network.request.export.DiaryExportRequest
import de.mdoc.pojo.mentalgoals.ExportType
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch
import timber.log.Timber

class DiaryExportViewModel(private val service: IMdocService, cb: ExportCallback) : BaseExportViewModel<DiaryExportRequest>(cb) {

    override fun download(request: DiaryExportRequest, type: ExportType, context: Context?) {
        isLoading.set(true)
        viewModelScope.launch {
            try {
                val response = service.downloadDiaries(type.value.toUpperCase(), request)
                writeFile(type, context, response)
                isLoading.set(false)
            } catch (e: java.lang.Exception) {
                Timber.d(e)
                isLoading.set(false)
            }
        }
    }
}
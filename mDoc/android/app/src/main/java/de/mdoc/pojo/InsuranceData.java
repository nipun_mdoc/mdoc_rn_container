package de.mdoc.pojo;

public class InsuranceData {

    private String id;
    private String name;

    public InsuranceData(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public InsuranceData(String id) {
        this.id = id;
    }

    public InsuranceData() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof InsuranceData)) {
            return false;
        }

        InsuranceData data = (InsuranceData) o;

        return data.id.equals(id);
    }

    //Idea from effective Java : Item 9
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + id.hashCode();
        return result;
    }
    @Override
    public String toString(){
        return name;
    }
}

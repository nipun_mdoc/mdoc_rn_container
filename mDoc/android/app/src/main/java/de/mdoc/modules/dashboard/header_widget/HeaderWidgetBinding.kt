package de.mdoc.modules.dashboard.header_widget

import android.view.View
import androidx.lifecycle.LifecycleOwner
import de.mdoc.R
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.view_dashboard_header.view.*
import java.text.SimpleDateFormat
import java.util.*

class HeaderWidgetBinding(
        private val lifecycleOwner: LifecycleOwner,
        private val view: View) : LifecycleOwner by lifecycleOwner {

    init {
        setSalutation()
        setUser()
    }

    private fun setSalutation() {
        when (getCurrentTime()) {

            // 5:00 AM — 11:59 AM
            in 500..1159 -> view.txtSalutation.setText(R.string.good_morning)

            // 12:00 PM — 4:59 PM
            in 1200..1659 -> view.txtSalutation.setText(R.string.good_afternoon)

            // 5:00 PM — 4:59 AM
            else -> view.txtSalutation.setText(R.string.good_evening)
        }
    }

    private fun setUser() {
        view.txtHeaderUser.text = "${MdocAppHelper.getInstance().userFirstName.capitalize()} ${MdocAppHelper.getInstance().userLastName.capitalize()}"
    }

    private fun getCurrentTime(): Int {
        return (getCurrentHour() + getCurrentMinutes()).toInt()
    }

    private fun getCurrentHour(): String {
       return getTimeWithPattern("HH")
    }

    private fun getCurrentMinutes(): String {
        return getTimeWithPattern("mm")
    }

    private fun getTimeWithPattern (pattern: String): String {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = System.currentTimeMillis()
        val dateFormat = SimpleDateFormat(pattern,  Locale.getDefault())
        return dateFormat.format(calendar.time)
    }
}
package de.mdoc.modules.mental_goals.goals_widget

import android.view.View
import androidx.navigation.findNavController
import de.mdoc.MainNavDirections

class GoalsWidgetHandler {

    fun openMentalGoals(view: View) {
        val action = MainNavDirections.globalActionToMentalGoal()
        view.findNavController()
            .navigate(action)
    }

    fun createMentalGoal(view: View) {
        val action = MainNavDirections.globalActionToMentalGoalCreation()
        view.findNavController()
            .navigate(action)
    }
}
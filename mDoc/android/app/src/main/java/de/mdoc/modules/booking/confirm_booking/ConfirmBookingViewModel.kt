package de.mdoc.modules.booking.confirm_booking

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.booking.data.BookingAdditionalFields
import de.mdoc.network.request.BookAppointmentRequest
import de.mdoc.pojo.FreeSlotParticipants
import de.mdoc.pojo.FreeSlotsAppointmentDetails
import de.mdoc.pojo.FreeSlotsList
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.getErrorMessage
import kotlinx.coroutines.launch
import org.joda.time.format.DateTimeFormat

class ConfirmBookingViewModel(private val mDocService: IMdocService,
                              val appointment: FreeSlotsList,
                              private val additionalFields:BookingAdditionalFields): ViewModel() {

    var isLoading = MutableLiveData(false)
    var bookingAdditionalFields:MutableLiveData<BookingAdditionalFields> = MutableLiveData(additionalFields)
    fun bookAppointment(onSuccess: () -> Unit, onError: (String?) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                if(additionalFields.appointmentId == null){
                    mDocService.bookAppointment( additionalFields.clinicId, bookAppointmentRequest())
                            .also {
                                if (it.isSuccessful) {
                                    onSuccess()
                                } else {
                                    onError(it.getErrorMessage())
                                }
                            }
                }else {
                    mDocService.updateAppointment(additionalFields.appointmentId!!, additionalFields.clinicId, bookAppointmentRequest())
                            .also {
                                if (it.isSuccessful) {
                                    onSuccess()
                                } else {
                                    onError(it.getErrorMessage())
                                }
                            }
                }
                isLoading.value = false

            } catch (e: retrofit2.HttpException) {
                isLoading.value = false
                onError(e.response()
                    ?.getErrorMessage())
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                onError(null)
            }
        }
    }

    fun getStartDate(): String {
        val formatter = DateTimeFormat.forPattern("dd/MM/yyyy")
        val input = appointment.appointmentDetails?.start

        return if (input != null) {
            formatter.print(input)
        }
        else {
            ""
        }
    }

    fun getTime(): String {
        val formatter = DateTimeFormat.forPattern("HH:mm")
        val input = appointment.appointmentDetails?.start
        val endTime = appointment.appointmentDetails?.end

        return when {
            input != null && endTime != null && appointment.appointmentDetails.isShowEndTime && appointment.appointmentDetails.end!=null && appointment.appointmentDetails.end.toString().trim()!="" && appointment.appointmentDetails.end != 0.toLong() -> {
                formatter.print(input)+" - "+formatter.print(endTime)
            }
            input != null && !appointment.appointmentDetails.isShowEndTime -> {
                formatter.print(input)
            }
            else -> {
                ""
            }
        }
    }

    private fun bookAppointmentRequest(): BookAppointmentRequest {
        val detailsRequest = FreeSlotsAppointmentDetails().also {
            it.participants = fillParticipants(appointment.appointmentDetails)
            it.specialty = appointment.appointmentDetails?.specialty
            it.resourceType = appointment.appointmentDetails?.resourceType
            it.title = appointment.appointmentDetails.title
            it.start = appointment.appointmentDetails?.start ?: 0
            it.end = appointment.appointmentDetails?.end ?: 0
            it.clinicId = appointment.appointmentDetails?.clinicId
            it.visible = true
            it.timeVisible = true
            it.comment = appointment.appointmentDetails?.comment
            it.location = appointment.appointmentDetails?.location
            if(appointment.appointmentDetails.isOnline){
                it.isOnline=true
            }
        }

        return BookAppointmentRequest().also {
            it.appointmentDetails = detailsRequest
            it.isDeleted = false
            it.id = appointment.id
//            if(bookingAdditionalFields.value?.isIntegration()==true) {
//                it.integrationType = bookingAdditionalFields.value?.specialtyProvider
//            }
        }
    }

    private fun shouldCancelPreviousAppointment() = !additionalFields.appointmentId.isNullOrEmpty()

    private fun fillParticipants(input: FreeSlotsAppointmentDetails?): ArrayList<FreeSlotParticipants> {
        return if (input?.hasGuest() == true) {
            input.participants
        }
        else {
            input?.addParticipant(FreeSlotParticipants().also {
                it.type = "GUEST"
                it.username = MdocAppHelper.getInstance()
                    .username
                it.firstName=MdocAppHelper.getInstance().userFirstName
                it.lastName=MdocAppHelper.getInstance().userLastName
            })

            input?.participants ?: arrayListOf()
        }
    }
}
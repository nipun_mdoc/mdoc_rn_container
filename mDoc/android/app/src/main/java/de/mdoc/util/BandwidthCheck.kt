package de.mdoc.util

import android.os.AsyncTask
import android.os.SystemClock
import android.view.View
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber
import java.net.URL
import java.util.concurrent.TimeUnit

private const val BANDWIDTH_CHECK_URL = "https://sit.mdoc.dev/bandwith/file.txt"
private val BANDWIDTH_CHECK_MINIMUM_INTERVAL = TimeUnit.SECONDS.toMillis(30)

object BandwidthCheck {

    private var latestBandwidth: Long? = null
    private var lastCheckAt: Long? = null

    private fun downloadFile(URL: String) {
        val lastCheckAt = this.lastCheckAt
        if (lastCheckAt == null
            // do not check too frequently
            || SystemClock.elapsedRealtime() - lastCheckAt > BANDWIDTH_CHECK_MINIMUM_INTERVAL
        ) {
            DownloadFileFromURL().execute(URL)
        }
    }

    internal class DownloadFileFromURL : AsyncTask<String, String, Long>() {

        override fun onPreExecute() {
            super.onPreExecute()
            println("Starting download")
        }

        override fun doInBackground(vararg f_url: String): Long? {
            return try {
                val url = URL(f_url[0])
                val connection = url.openConnection()
                connection.connect()
                var startTime = 0L
                val buffer = ByteArray(2048)
                startTime = SystemClock.elapsedRealtime()
                val input = url.openStream()
                var total = 0
                var count = 0
                do {
                    count = input.read(buffer)
                    if (count > 0) {
                        total += count
                    }
                } while (count > 0)
                val endTime = SystemClock.elapsedRealtime()
                input.close()
                endTime - startTime
            } catch (e: Exception) {
                Timber.w(e)
                null
            }
        }

        override fun onPostExecute(downloadTime: Long?) {
            println("Download completed $downloadTime")
            latestBandwidth = downloadTime
            if (downloadTime != null) {
                lastCheckAt = SystemClock.elapsedRealtime()
            } else {
                lastCheckAt = null // unsuccessful check
            }
        }
    }

    fun checkBandwidth(message: String, view: View) {
        if (latestBandwidth != null) {
            val bandwidth = latestBandwidth!!
            if (bandwidth > 500) {
                showSnackBar(message, view)
            }
        }
        downloadFile(BANDWIDTH_CHECK_URL)
    }

    fun initBandwidthCheck() {
        downloadFile(BANDWIDTH_CHECK_URL)
    }

    private fun showSnackBar(message: String, view: View) {
        val snackBar = Snackbar.make(
            view,
            message, Snackbar.LENGTH_LONG
        )
        snackBar.show()
    }

}
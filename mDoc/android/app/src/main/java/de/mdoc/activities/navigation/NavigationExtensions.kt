package de.mdoc.activities.navigation

import android.content.Context
import androidx.fragment.app.Fragment
import de.mdoc.activities.MainActivity

fun Fragment.setCurrentNavigationItem(navigationItem: NavigationItem) {
    val mainActivity = (activity as? MainActivity)
    mainActivity?.getNavigationViewModel()?.updateSelectedItem(navigationItem)
}

fun Context.isPhone(): Boolean {
    return resources.getBoolean(de.mdoc.R.bool.phone)
}

fun Context.isTablet(): Boolean {
    return !isPhone()
}
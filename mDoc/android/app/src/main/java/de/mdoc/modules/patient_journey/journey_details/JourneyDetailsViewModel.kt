package de.mdoc.modules.patient_journey.journey_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.modules.patient_journey.data.CarePlan
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class JourneyDetailsViewModel(private val mDocService: IMdocService,val carePlanId:String): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)

    var carePlan: MutableLiveData<CarePlan> =
            MutableLiveData(CarePlan())

    init {
        getJourneyDetails()
    }

    private fun getJourneyDetails() {
        viewModelScope.launch {
            isShowLoadingError.value = false
            try {
                isLoading.value = true
                carePlan.value= mDocService.getJourneyDetails(carePlanId,
                        FIELDS,
                        FIELDS2).data
                isShowContent.value=true
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowLoadingError.value = true
                isShowContent.value=false
            }
        }
    }

    companion object {
        const val FIELDS = "*activityDescription*"
        const val FIELDS2 = "*childCarePlans*"

    }
}
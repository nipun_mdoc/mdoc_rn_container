package de.mdoc.activities.login

import android.content.Context
import com.google.firebase.iid.FirebaseInstanceId
import de.mdoc.R
import de.mdoc.network.request.KeycloackLoginRequest
import java.net.URL
import java.net.URLDecoder

class LoginUtil(val context: Context) {

    private fun getUriBasedOnUrl(url: String): String? {
        return if (url.startsWith("m")) {
            MDOCAPP_URI
        }
        else {
            context.getString(R.string.base_url)
                .replace("/api/", "")
        }
    }

    fun generateLoginRequest(url: String): KeycloackLoginRequest {
        val request = KeycloackLoginRequest()
        val instanceId = FirebaseInstanceId.getInstance()
            .id
        val deviceFriendly = "$DEVICE_NAME # $instanceId"

        request.client_secret = context.getString(R.string.client_secret)
        request.grant_type = GRANT_TYPE
        request.redirect_uri = getUriBasedOnUrl(url)
        request.client_id = context.getString(R.string.keycloack_clinic_Id)
        request.code = extractCodeFromUrl(url)
        request.deviceName = deviceFriendly

        return request
    }

    fun extractCodeFromUrl(url: String): String? {
        return if (url.contains(CODE)) {
            var code = url.substring(url.lastIndexOf(CODE) + 5)
            if (code.contains("#")) {
                code = code.substring(0, code.indexOf("#"))
            }
            if (code.contains("?")) {
                code = code.substring(0, code.indexOf("?"))
            }
            code
        }
        else {
            null
        }
    }

    companion object {
        const val CODE = "code="
        const val MDOCAPP_URI = "mdocapp://mdocapp/"
        const val DEVICE_NAME = "android"
        const val GRANT_TYPE = "authorization_code"
    }
}
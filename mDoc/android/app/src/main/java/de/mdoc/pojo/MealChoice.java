package de.mdoc.pojo;

import java.io.Serializable;

import de.mdoc.network.response.VoteResponse;


/**
 * Created by ema on 10/18/17.
 */

public class MealChoice implements Serializable {

    private String caseId;
    private String description;
    private String mealChoiceId;
    private String mealId;
    private String mealPlanId;
    private String review;

    public MealChoice() {
    }

    public MealChoice(VoteResponse response) {
        this.caseId = response.getData().getCaseId();
        this.description = response.getDescription();
        this.mealChoiceId = response.getData().getMealChoiceId();
        this.mealId = response.getData().getMealId();
        this.mealPlanId = response.getData().getMealPlanId();
        this.review = response.getData().getReview();
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMealChoiceId() {
        return mealChoiceId;
    }

    public void setMealChoiceId(String mealChoiceId) {
        this.mealChoiceId = mealChoiceId;
    }

    public String getMealId() {
        return mealId;
    }

    public void setMealId(String mealId) {
        this.mealId = mealId;
    }

    public String getMealPlanId() {
        return mealPlanId;
    }

    public void setMealPlanId(String mealPlanId) {
        this.mealPlanId = mealPlanId;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }
}

package de.mdoc.modules.common.viewmodel

import de.mdoc.pojo.DiaryList

interface BaseSearchViewModelFunctions {
    fun requestSearch(query: String = "")
    fun requestDelete(query: DiaryList)
    fun requestUpdate(query: DiaryList)
}
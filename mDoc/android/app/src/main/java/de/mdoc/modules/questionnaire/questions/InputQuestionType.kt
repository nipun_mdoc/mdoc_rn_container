package de.mdoc.modules.questionnaire.questions

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.text.format.DateFormat
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import kotlinx.android.synthetic.main.short_text_layout.view.*
import org.joda.time.DateTimeFieldType
import org.joda.time.LocalDate
import org.joda.time.LocalTime
import org.joda.time.format.DateTimeFormat
import rx.android.schedulers.AndroidSchedulers

internal class InputQuestionType: AbstractTextQuestionType() {

    override val layoutResId: Int = R.layout.short_text_layout

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val view = super.onCreateQuestionView(parent, question)
        val answerEdt = view.answerEdt
        when (question.pollQuestionExternalType) {
            MdocConstants.EXTERNAL_INPUT_TYPE_DATE -> {
                view.inputIcon.setImageResource(R.drawable.ic_choose_date)
                setupDate(answerEdt)
            }

            MdocConstants.EXTERNAL_INPUT_TYPE_TIME -> {
                view.inputIcon.setImageResource(R.drawable.ic_choose_time)
                setupTime(answerEdt)
            }
        }
        RxTextView.textChanges(answerEdt)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                questionIsAnswered.value = it.isNotEmpty()
            }
        return view
    }

    private fun setupTime(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        val formatter = DateTimeFormat.forPattern("HH:mm")
        val listener = TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            val time = LocalTime.MIDNIGHT.plusHours(hourOfDay)
                .plusMinutes(minute)
            val text = formatter.print(time)
            editText.setText(text)
        }
        editText.setOnClickListener {
            val time = try {
                LocalTime.parse(editText.text.toString(), formatter)
            } catch (e: Exception) {
                LocalTime.now()
            }
            val dialog = TimePickerDialog(
                    editText.context,
                    listener,
                    time.get(DateTimeFieldType.hourOfDay()),
                    time.get(DateTimeFieldType.minuteOfHour()),
                    DateFormat.is24HourFormat(editText.context)
                                         )
            dialog.show()
        }
    }

    private fun setupDate(editText: EditText) {
        editText.isFocusable = false
        editText.isFocusableInTouchMode = false
        val formatter = DateTimeFormat.forPattern("dd.MM.yyyy")
        val listener = DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            val date = LocalDate().withYear(year)
                .withMonthOfYear(month + 1)
                .withDayOfMonth(dayOfMonth)
            val text = formatter.print(date)
            editText.setText(text)
        }
        editText.setOnClickListener {
            val date = try {
                LocalDate.parse(editText.text.toString(), formatter)
            } catch (e: Exception) {
                LocalDate.now()
            }
            val dialog = DatePickerDialog(
                    editText.context,
                    listener,
                    date.get(DateTimeFieldType.year()),
                    date.get(DateTimeFieldType.monthOfYear()) - 1,
                    date.get(DateTimeFieldType.dayOfMonth())
                                         )
            dialog.show()
        }
    }
}
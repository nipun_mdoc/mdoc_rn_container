package de.mdoc.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AdisMulabdic on 10/20/17.
 */

public class QRData {
    @SerializedName("qrCode")
    @Expose
    private String qrCode;

    public String getQrCode() {
        return qrCode;
    }

    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }
}

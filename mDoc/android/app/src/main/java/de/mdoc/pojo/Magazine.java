package de.mdoc.pojo;

/**
 * Created by AdisMulabdic on 1/16/18.
 */

public class Magazine {

    private String publicUrl;

    public String getPublicUrl() {
        return publicUrl;
    }

    public void setPublicUrl(String publicUrl) {
        this.publicUrl = publicUrl;
    }

}

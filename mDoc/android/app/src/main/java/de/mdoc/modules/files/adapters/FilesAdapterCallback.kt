package de.mdoc.modules.files.adapters

import de.mdoc.pojo.FileListItem

interface FilesAdapterCallback {

    fun onDeleteFileRequest(item: FileListItem)

    fun onEditFileRequest(item: FileListItem)

    fun onShareFileRequest(item: FileListItem)

    fun onDownloadFileRequest(item: FileListItem)
}
package de.mdoc.data.create_bt_device

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Coding(
        val code: String?,
        val display: String?,
        val system: String?):Parcelable
{

    fun isSystolic() : Boolean{
        if(code.equals("8480-6")){
            return true
        }
        return false
    }

    fun isDiastolic() : Boolean{
        if(code.equals("8462-4")){
            return true
        }
        return false
    }
}
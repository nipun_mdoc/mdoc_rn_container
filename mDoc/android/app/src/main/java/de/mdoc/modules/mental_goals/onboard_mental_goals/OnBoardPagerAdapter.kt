package de.mdoc.modules.mental_goals.onboard_mental_goals

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager


class OnBoardPagerAdapter internal constructor(fm: FragmentManager?, private val viewPager: ViewPager) : FragmentStatePagerAdapter(fm!!,
        BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT), ItemSelectionListener{

    private val count = 3

    override fun getItem(position: Int): Fragment {
        lateinit var fragment: Fragment
        when (position) {
            0 -> {
                fragment = FirstOnBoardFragment()
                fragment.setItemSelectionListener(this)
            }
            1 -> {
                fragment = SecondOnBoardFragment()
                fragment.setItemSelectionListener(this)
            }
            2 -> {fragment = ThirdOnBoardFragment()
                fragment.setItemSelectionListener(this)
            }
        }

        return fragment
    }

    override fun getCount(): Int {
        return count
    }

    override fun pagerIndex(index: Int) {
        viewPager.currentItem=index

    }




}

package de.mdoc.modules.messages.data

import de.mdoc.pojo.PatientDetailsData
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import org.joda.time.Instant
import org.joda.time.format.DateTimeFormat

data class MessagesResponse(val code: String = "",
                            val data: DataMessage,
                            val message: String = "",
                            val timestamp: Long = 0)

data class DataMessage(val moreDataAvailable: Boolean = false,
                       val totalCount: Int = 0,
                       val list: ArrayList<ListItemMessage>?)

data class Meta(val LINK_TEXT: String?)

data class ListItemMessage(val senderId: String = "",
                           val exportDate: Int = 0,
                           val cts: Long = 0,
                           val receiverId: String = "",
                           val uts: Long = 0,
                           val fileIdList: List<String>?,
                           val conversationId: String = "",
                           val id: String = "",
                           val text: String = "",
                           val encounterId: String = "",
                           val date: Instant?,
                           val priority: Int = 0,
                           val senderDTO: PatientDetailsData? = null,
                           val receiverDTO: PatientDetailsData? = null,
                           val href: String = "",
                           val meta : Meta? = null,
                           var fileList: ArrayList<FileListItem>) {

    fun isMine(): Boolean {
        return senderId == MdocAppHelper.getInstance().userId
    }

    fun getFormatedDate(): String {
        val formatterForToday = DateTimeFormat.forPattern("HH:mm")
        val formatterForPast = DateTimeFormat.forPattern("dd.MM.yyyy, HH:mm")

        if (date == null) {
            return ""
        }
        else if (MdocUtil.isToday(date.millis)) {
            return formatterForToday.print(date.millis)
        }
        else {
            return formatterForPast.print(date.millis)
        }
    }

    fun getFormatedDateArchived(): String {
        val formatterForPast = DateTimeFormat.forPattern("dd.MM.yyyy, HH:mm")

        if (date == null) {
            return ""
        }
        else {
            return formatterForPast.print(date.millis)
        }
    }

    fun hasFiles(): Boolean {
        return !fileList.isNullOrEmpty()
    }
}
package de.mdoc.modules.meal_plan;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.constants.MdocConstants;
import de.mdoc.interfaces.RecyclerViewClickListener;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapter;
import de.mdoc.modules.meal_plan.adapters.MealOfferAdapterTabletPast;
import de.mdoc.network.managers.MdocManager;
import de.mdoc.network.request.VoteRequest;
import de.mdoc.network.response.APIErrorKt;
import de.mdoc.network.response.VoteResponse;
import de.mdoc.pojo.MealChoice;
import de.mdoc.pojo.MealOffer;
import de.mdoc.pojo.MealPlan;
import de.mdoc.util.BandwidthCheck;
import de.mdoc.util.ErrorUtilsKt;
import de.mdoc.util.MdocAppHelper;
import de.mdoc.util.MdocUtil;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by ema on 1/26/18.
 */

public class MealPlanCantChooseFragment extends MealUtil {

    @BindView(R.id.monthAndDateTv)
    TextView monthAndDateTv;
    @BindView(R.id.container)
    LinearLayout container;
    @BindView(R.id.txtHelloName)
    TextView txtHelloName;

    MealOfferAdapter mealOfferAdapter;
    MealOfferAdapter adapter;

    MdocActivity activity;

    private int dayIndex = 0;
    private int todayIndex = -1;

    @Override
    protected int setResourceId() {
        return R.layout.fragment_cant_choose_meal;
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();
        progressDialog = activity.getParentProgressDialog();
        if (progressDialog != null) {
            progressDialog.setCancelable(false);

        }

        if(!getResources().getBoolean(R.bool.phone)) {
            if(savedInstanceState == null){
                progressDialog.show();
                getMealPlan();
            }
        }else{
            String patientName =  MdocAppHelper.getInstance().getUserFirstName() + " " + MdocAppHelper.getInstance().getUserLastName();
            txtHelloName.setText(getResources().getString(R.string.hello) + " " + patientName + ",");
            getMealInfo();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        BandwidthCheck.INSTANCE.checkBandwidth(getResources().getString(R.string.err_bandwidth),view);

    }

    @Override
    void setData(){
        for(int i=0; i<items.size(); i++){
            if(MdocUtil.isToday(items.get(i).getDateNumber())){
                todayIndex = i;
                dayIndex = i;
                monthAndDateTv.setText(getDate(items.get(i).getDateNumber()));
                setLayout();
            }
        }
    }

    private String getDate(long timeStamp){

        try{
            DateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    @OnClick(R.id.leftDayIv)
    public void onLeftMonthIvClick() {
        if (dayIndex > 0) {
            dayIndex--;
            monthAndDateTv.setText(getDate(items.get(dayIndex).getDateNumber()));
            setLayout();
        }
    }

    @OnClick(R.id.rightDayIv)
    public void onRightMonthIvClick() {

        if (dayIndex < items.size() - 1) {
            dayIndex++;
            monthAndDateTv.setText(getDate(items.get(dayIndex).getDateNumber()));
            setLayout();
        }
    }

    private void setLayout(){
        container.removeAllViews();

        LayoutInflater inflater = LayoutInflater.from(activity);
        View today= inflater.inflate(R.layout.layout_cant_choose_today , null, false);
        View future= inflater.inflate(R.layout.layout_meal_future, null, false);
        View past= inflater.inflate(R.layout.layout_cant_choose_past, null, false);
        if(items.size() > 0 && dayIndex != -1) {

            MealPlan item = items.get(dayIndex);

            if (MdocUtil.isPast(items.get(dayIndex).getDateNumber())) {
                container.addView(past);
                setPastLayout(item);
            } else if (MdocUtil.isToday(items.get(dayIndex).getDateNumber())) {
                if (item.isMealPlanRatingRule() && getResources().getBoolean(R.bool.rate_meal_after_13)) {
                    container.addView(past);
                    setPastLayout(item);
                } else {
                    container.addView(today);
                    setTodayLayout(item);
                }
            } else {
                container.addView(future);
                setFutureLayout();
            }
        }
    }

    private void setTodayLayout(MealPlan item) {
        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers(); //getTomorrowsMealOffer(thisWeek.getMealPlanOld().getWeeklyMealOffer(), nextWeek.getMealPlanOld().getWeeklyMealOffer());

        adapter = null;
        adapter = new MealOfferAdapter(tommorow, activity, items.get(dayIndex).getMealPlanId(), items.get(dayIndex).getMealChoice() != null ? items.get(dayIndex).getMealChoice().getMealChoiceId() : "");
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false));
        mealOfferRv.setAdapter(adapter);

    }

    private void setFutureLayout() {
        RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);
        mealOfferRv.setHasFixedSize(true);

        // use a linear layout manager
        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        ArrayList<MealOffer> tommorow = items.get(dayIndex).getOffers(); //getTomorrowsMealOffer(thisWeek.getMealPlanOld().getWeeklyMealOffer(), nextWeek.getMealPlanOld().getWeeklyMealOffer());

        MealOfferAdapter.previousItem = null;
        mealOfferAdapter = null;
        mealOfferAdapter = new MealOfferAdapter(tommorow, activity, items.get(dayIndex).getMealPlanId(), items.get(dayIndex).getMealChoice() != null ? items.get(dayIndex).getMealChoice().getMealChoiceId() : "");
        mealOfferRv.setAdapter(mealOfferAdapter);
    }

    MealOfferAdapterTabletPast pastAdapter;
    RecyclerViewClickListener listener;

    private void setPastLayout(final MealPlan item) {
        final LinearLayout yesterdayLl = container.findViewById(R.id.yesterdayLl);
        final RecyclerView mealOfferRv = container.findViewById(R.id.mealOfferRv);

        ArrayList<MealOffer> offers = new ArrayList<>();

        // use a linear layout manager

        listener = new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, final int position) {
                if(item.getOffers().get(position).isDessert() || item.getOffers().get(position).isSoup()){
                    return;
                }

                mealOfferRv.setVisibility(View.GONE);
                yesterdayLl.setVisibility(View.VISIBLE);

                TextView voteMealTv = container.findViewById(R.id.voteMealTv);
                TextView afterVoteMsg = container.findViewById(R.id.afterVoteTv);
                Button confirmBtn = container.findViewById(R.id.confirmBtn);

                absoluteIv = container.findViewById(R.id.absoluteIv);
                yesIv = container.findViewById(R.id.yesIv);
                slightlyYesIv = container.findViewById(R.id.slightlyYesIv);
                mediumIv = container.findViewById(R.id.mediumIv);
                noIv = container.findViewById(R.id.noIv);
                voteEdt = container.findViewById(R.id.voteEdt);

                absoluteIv.setTag(MdocConstants.NOT_SELECTED);
                yesIv.setTag(MdocConstants.NOT_SELECTED);
                slightlyYesIv.setTag(MdocConstants.NOT_SELECTED);
                mediumIv.setTag(MdocConstants.NOT_SELECTED);
                noIv.setTag(MdocConstants.NOT_SELECTED);

                voteMealTv.setText(item.getOffers().get(position).getDescription());

                if (item.getMealChoice() != null) {
                    confirmBtn.setVisibility(View.GONE);
                    afterVoteMsg.setVisibility(View.VISIBLE);
                }

                absoluteIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onAbsoluteIvClick();
                        }
                    }
                });

                yesIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onYesIvClick();
                        }
                    }
                });

                slightlyYesIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onSlightlYesIvClick();
                        }
                    }
                });

                mediumIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onMediumIvClick();
                        }
                    }
                });

                noIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onNoIvClick();
                        }
                    }
                });

                confirmBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                            onConfirmBtnClick(item, item.getOffers().get(position), mealOfferRv, yesterdayLl);
                        }
                        Timber.i("On clicked %s", new Gson().toJson(item));
                    }
                });
                if(item.getMealChoice() == null || item.getMealChoice().getMealChoiceId() == null || item.getMealChoice().getMealChoiceId().isEmpty()) {
                    voteEdt.setEnabled(true);
                    voteEdt.setFocusable(true);
                    voteEdt.setFocusableInTouchMode(true);
                }else{
                    voteEdt.setEnabled(false);
                    voteEdt.setFocusable(false);
                    voteEdt.setFocusableInTouchMode(false);
                }
                setRated(item, item.getOffers().get(position).get_id());

            }
        };

        mealOfferRv.setHasFixedSize(true);
        pastAdapter = new MealOfferAdapterTabletPast(item.getOffers(), activity, item.getMealPlanId(), item.getMealChoice() != null ? item.getMealChoice().getMealChoiceId() : "", listener, item.getMealChoice());

        mealOfferRv.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));

        mealOfferRv.setAdapter(pastAdapter);

        mealOfferRv.setVisibility(View.VISIBLE);
        yesterdayLl.setVisibility(View.GONE);

    }



    public void onConfirmBtnClick(final MealPlan mealPlan, MealOffer offer, RecyclerView mealOfferRv, LinearLayout yesterdayLl) {

        if (getReview() != null) {

            final VoteRequest request = new VoteRequest();
            request.setCaseId(MdocAppHelper.getInstance().getCaseId());
            request.setDescription(voteEdt.getText().toString());
            request.setMealId(offer.get_id());
            request.setMealPlanId(mealPlan.getMealPlanId());
            request.setReview(getReview());

            if(mealPlan.getMealChoice() == null || mealPlan.getMealChoice().getMealChoiceId() == null || mealPlan.getMealChoice().getMealChoiceId().isEmpty()) {
                MdocManager.vote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if (isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));

                                if (mealPlan.getMealChoice() == null) {
                                    mealPlan.setMealChoice(new MealChoice());
                                }
                                mealPlan.getMealChoice().setMealChoiceId(response.body().getData().getMealChoiceId());
                                mealPlan.getMealChoice().setReview(getReview());
                                mealPlan.getMealChoice().setDescription(voteEdt.getText().toString());
                                mealPlan.getMealChoice().setMealId(response.body().getData().getMealId());
                                mealOfferRv.setVisibility(View.VISIBLE);
                                yesterdayLl.setVisibility(View.GONE);
                                voteEdt.setText("");
                                pastAdapter = new MealOfferAdapterTabletPast(mealPlan.getOffers(), activity, mealPlan.getMealPlanId(), mealPlan.getMealChoice() != null ? mealPlan.getMealChoice().getMealChoiceId() : "", listener, mealPlan.getMealChoice());
                                mealOfferRv.setAdapter(pastAdapter);
                            } else {
                                Timber.w("vote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable t) {
                        Timber.w(t, "vote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), t.getMessage());
                        }
                    }
                });
            } else {
                request.setMealChoiceId(mealPlan.getMealChoice().getMealChoiceId());
                MdocManager.updateVote(request, new Callback<VoteResponse>() {
                    @Override
                    public void onResponse(Call<VoteResponse> call, Response<VoteResponse> response) {
                        if(isAdded()) {
                            if (response.isSuccessful()) {
                                MdocUtil.showToastShort(getContext(), getString(R.string.thank_you));
                                mealPlan.getMealChoice().setMealChoiceId(response.body().getData().getMealChoiceId());
                                mealPlan.getMealChoice().setReview(getReview());
                                mealPlan.getMealChoice().setDescription(voteEdt.getText().toString());
                                mealPlan.getMealChoice().setMealId(response.body().getData().getMealId());
                                mealOfferRv.setVisibility(View.VISIBLE);
                                yesterdayLl.setVisibility(View.GONE);
                                voteEdt.setText("");
                                pastAdapter = new MealOfferAdapterTabletPast(mealPlan.getOffers(), activity, mealPlan.getMealPlanId(), mealPlan.getMealChoice() != null ? mealPlan.getMealChoice().getMealChoiceId() : "", listener, mealPlan.getMealChoice());
                                mealOfferRv.setAdapter(pastAdapter);
                            } else {
                                Timber.w("updateVote %s", APIErrorKt.getErrorDetails(response));
                                MdocUtil.showToastShort(getContext(), ErrorUtilsKt.getErrorMessage(response));
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<VoteResponse> call, Throwable t) {
                        Timber.w(t, "updateVote");
                        if(isAdded()) {
                            MdocUtil.showToastShort(getContext(), t.getMessage());
                        }
                    }
                });
            }
        }
    }


    private void setRated(MealPlan item, String mealId) {
        if(item.getMealChoice() != null && item.getMealChoice().getMealId().equals(mealId)){
            if (item.getMealChoice() != null && item.getMealChoice().getReview() != null && !item.getMealChoice().getReview().isEmpty()) {
                switch (item.getMealChoice().getReview()) {
                    case MdocConstants.POOR:
                        mediumIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.NORMAL:
                        slightlyYesIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.EXCELLENT:
                        yesIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.VERY_POOR:
                        noIv.setTag(MdocConstants.SELECTED);
                        break;
                    case MdocConstants.ABSOLUTE:
                        absoluteIv.setTag(MdocConstants.SELECTED);
                        break;
                }

                if (item.getMealChoice().getDescription() != null && !item.getMealChoice().getDescription().isEmpty()) {
                    voteEdt.setText(item.getMealChoice().getDescription());
                }
            }
        }
        setSelection();
    }
}

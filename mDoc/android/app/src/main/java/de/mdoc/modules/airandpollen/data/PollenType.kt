package de.mdoc.modules.airandpollen.data

import android.content.Context
import androidx.annotation.StringRes
import de.mdoc.R

enum class PollenType(
    val jsonName: String,
    @StringRes val nameResId: Int
) {
    Hazel("Hasel", R.string.pollen_type_hazel),
    Alder("Erle", R.string.pollen_type_alder),
    Ash("Esche", R.string.pollen_type_ash),
    Birch("Birke", R.string.pollen_type_birch),
    Grasses("Graeser", R.string.pollen_type_grasses),
    Rye("Roggen", R.string.pollen_type_rye),
    Mugwort("Beifuss", R.string.pollen_type_mugwort),
    Ambrosia("Ambrosia", R.string.pollen_type_ambrosia);

    fun getName(context: Context?): String? {
        return context?.getString(nameResId)
    }

}
package de.mdoc.modules.profile

import android.content.Context
import androidx.annotation.NonNull
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.R
import de.mdoc.modules.profile.data.Contact
import de.mdoc.modules.profile.data.ContactsRequest
import de.mdoc.modules.profile.data.EmergencyInfoRequest
import de.mdoc.modules.profile.data.TherapyPrintRequest
import de.mdoc.network.managers.MdocManager
import de.mdoc.network.request.KeycloackLoginRequest
import de.mdoc.network.response.KeycloackLoginResponse
import de.mdoc.pojo.PublicUserDetails
import de.mdoc.service.IMdocService
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.token_otp.Token
import de.mdoc.util.token_otp.Token.TokenUriInvalidException
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MyProfileViewModel(private val mDocService: IMdocService): ViewModel() {
    var publicUserDetails: MutableLiveData<PublicUserDetails> = MutableLiveData(PublicUserDetails())
    var isLoading = MutableLiveData(false)
    var emergencyInfo = MutableLiveData("")
    var emergencyContacts: MutableLiveData<ArrayList<Contact>> = MutableLiveData(arrayListOf())

    fun getPublicUserDetails(onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true
                val response = mDocService.getPatientDetails()
                    .data
                publicUserDetails.value = response.publicUserDetails ?: PublicUserDetails()
                MdocAppHelper.getInstance().emergencyInfo=response.extendedUserDetails.emergencyInfo
                emergencyInfo.value = MdocAppHelper.getInstance().emergencyInfo

                if (response.publicUserDetails.emergencyContacts.isNullOrEmpty()) {
                    emergencyContacts.value = arrayListOf(Contact())
                }
                else {
                    emergencyContacts.value = response.publicUserDetails.emergencyContacts
                    MdocAppHelper.getInstance()
                        .publicUserDetailses.emergencyContacts = emergencyContacts.value
                }

                onSuccess()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun sendEmergencyInfo(onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true

                mDocService.patchEmergencyInfo(
                        EmergencyInfoRequest(emergencyInfo.value ?: ""))
                MdocAppHelper.getInstance().emergencyInfo=emergencyInfo.value
                onSuccess()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun updateTherapyPrintInfo(printPatientPlan: String, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true

                mDocService.patchTherapyPrint(
                        TherapyPrintRequest(printPatientPlan))
                onSuccess()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun updateEmergencyContacts(contacts: ArrayList<Contact>, onSuccess: () -> Unit, onError: (Throwable) -> Unit) {
        viewModelScope.launch {
            try {
                isLoading.value = true

                mDocService.patchEmergencyContacts(ContactsRequest(contacts))
                MdocAppHelper.getInstance()
                    .publicUserDetailses.emergencyContacts = contacts
                onSuccess()
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                onError(e)
                isLoading.value = false
            }
        }
    }

    fun validatePassword(context: Context?, password: String, successAction: () -> Unit, failAction: () -> Unit) {
        var otp = ""
        val time = System.currentTimeMillis()
        if (MdocAppHelper.getInstance()
                .hasSecretKey()) {
            try {
                val token = Token(MdocAppHelper.getInstance()
                    .secretKey)
                val code = token.generateCodes(time)
                otp = code.getCurrentCode(time)
            } catch (e: TokenUriInvalidException) {
                e.printStackTrace()
            }
        }
        val request = KeycloackLoginRequest().also {
            it.username = MdocAppHelper.getInstance().username
            it.password = password
            it.client_id = context?.resources?.getString(R.string.keycloack_clinic_Id)
            it.client_secret = context?.resources?.getString(R.string.client_secret)
            it.grant_type = "password"
            it.totp = otp
        }

        MdocManager.loginWithKeycloack(request, object: Callback<KeycloackLoginResponse> {
            override fun onResponse(call: Call<KeycloackLoginResponse>,
                                    response: Response<KeycloackLoginResponse>) {
                if (response.isSuccessful) {
                    successAction()
                }
                else {
                    failAction()
                }
            }

            override fun onFailure(@NonNull call: Call<KeycloackLoginResponse>, @NonNull
            t: Throwable) {
                failAction()
            }
        })
    }
}
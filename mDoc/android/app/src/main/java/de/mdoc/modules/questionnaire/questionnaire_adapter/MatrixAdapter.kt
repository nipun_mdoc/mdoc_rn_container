package de.mdoc.modules.questionnaire.questionnaire_adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import de.mdoc.R
import de.mdoc.interfaces.IPostAnswerForMatrix
import de.mdoc.modules.questionnaire.data.ChildQuestion
import de.mdoc.modules.questionnaire.data.QuestionTitle
import de.mdoc.pojo.Answer

class MatrixAdapter internal constructor(
        private val context: Context,
        private val questionTitle: ArrayList<QuestionTitle>,
        private val matrixQuestions: HashMap<QuestionTitle, ArrayList<ChildQuestion>>,
        private val questionIsAnswered: MutableLiveData<Boolean>,
        private var isNewAnswer: Boolean): BaseExpandableListAdapter(){

    var listener: IPostAnswerForMatrix? = null
    fun setPostAnswerListener(listener: IPostAnswerForMatrix) {
        this.listener = listener
    }

    override fun getGroup(groupPosition: Int): Any {
        return this.questionTitle.size
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        val listQuestionTitle = questionTitle[groupPosition]
        if (convertView == null){
            val  layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView =  layoutInflater.inflate(R.layout.matrix_list_title, null)
        }
        val expandableListTitle = convertView!!.findViewById<TextView>(R.id.questionTitleTv)
        val indicatorIv = convertView.findViewById<ImageView>(R.id.indicatorIv)
        expandableListTitle.text = listQuestionTitle.title
        indicatorIv.setImageResource(if (isExpanded) R.drawable.arrow_up_dark else R.drawable.arrow_down_dark)
        return convertView
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.matrixQuestions[this.questionTitle[groupPosition]]!!.size
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return this.matrixQuestions[this.questionTitle[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        val childQuestion = getChild(groupPosition, childPosition) as ChildQuestion
        val selections: ArrayList<String> = ArrayList()
        if (convertView == null){
            val  layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView =  layoutInflater.inflate(R.layout.radio_button_template, null)
        }
        val radioAnswer = convertView!!.findViewById<RadioButton>(R.id.radioAnswer)
        radioAnswer.text = childQuestion.value
        radioAnswer.setTag(R.id.selection,childQuestion.key)
        radioAnswer.setTag(R.id.questionId,childQuestion.questionId)

        radioAnswer.isChecked = childQuestion.answer != null

        radioAnswer.setOnClickListener { _ ->
            questionIsAnswered.value = true
            val selection = radioAnswer.getTag(R.id.selection) as String
            val questionId = radioAnswer.getTag(R.id.questionId) as String
            selections.clear()
            selections.add(selection)
            listener?.postAnswerForMatrix(radioAnswer.text.toString(), selections, questionId)
            for (child in matrixQuestions.values){
                for (answer in child){
                    if (answer.questionId == questionId) {
                        answer.answer = null
                    }
                }
            }
            val newAnswer = Answer(radioAnswer.text.toString(), selections, questionId)
            childQuestion.answer = newAnswer
            isNewAnswer = true
            notifyDataSetChanged()
        }

        if (childQuestion.answer != null) {
            questionIsAnswered.value = true
            radioAnswer.isChecked = true
        }

        return convertView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return this.questionTitle.size
    }
}
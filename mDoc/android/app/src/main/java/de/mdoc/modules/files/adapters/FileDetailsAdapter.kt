package de.mdoc.modules.files.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.files.adapters.viewholders.BaseFileItemViewHolder
import de.mdoc.modules.files.adapters.viewholders.FileDetailsListItemViewHolder
import de.mdoc.modules.files.adapters.viewholders.FileItemViewHolder
import de.mdoc.modules.files.adapters.viewholders.FileItemViewHolderCallback
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FileDetailsItemType
import de.mdoc.pojo.FileListItem

class FileDetailsAdapter(val items: Map<FileDetailsItemType, Any>? = mapOf(), val categories: List<CodingItem> = listOf(), private val callback: FilesAdapterCallback) : RecyclerView.Adapter<BaseFileItemViewHolder>(), FileItemViewHolderCallback {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseFileItemViewHolder {
        return when (viewType) {
            FileDetailsItemType.MAIN.ordinal -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.file_list_item, parent, false)
                FileItemViewHolder(view, this, parent.context)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.file_details_simple_item, parent, false)
                FileDetailsListItemViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: BaseFileItemViewHolder, position: Int) {

        when (holder) {
            is FileItemViewHolder -> {
                val item = items?.values?.elementAt(position)
                if (item is FileListItem) {
                    if(item.isRejected()){
                        holder.updateRejected(item = item, categories = categories)
                    }else{
                        holder.update(item = item, categories = categories)
                    }
                }
            }

            is FileDetailsListItemViewHolder -> {
                holder.bind(items?.entries?.elementAt(position))
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items?.keys?.elementAt(position)?.ordinal ?: super.getItemViewType(position)
    }

    override fun onClickMore(view: View, item: FileListItem, position: Int, holder: FileItemViewHolder) {
        holder.showPopup(view, item, callback)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }
}
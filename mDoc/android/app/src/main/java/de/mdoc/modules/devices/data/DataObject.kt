package de.mdoc.modules.devices.data

data class DataObject(val totalCount: Double,
                      val list:ArrayList<Observation>)
package de.mdoc.network.request;

/**
 * Created by ema on 4/2/18.
 */

public class SearchFileRequest {

    private int skip;
    private int limit;
    private String query;

    public SearchFileRequest(int skip, int limit, String query) {
        this.skip = skip;
        this.limit = limit;
        this.query = query;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}

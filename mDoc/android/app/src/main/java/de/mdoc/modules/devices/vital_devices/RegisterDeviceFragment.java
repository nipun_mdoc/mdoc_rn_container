package de.mdoc.modules.devices.vital_devices;


import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.navigation.Navigation;

import butterknife.BindView;
import de.mdoc.R;
import de.mdoc.activities.MdocActivity;
import de.mdoc.activities.navigation.NavigationItem;
import de.mdoc.constants.MdocConstants;
import de.mdoc.fragments.MdocFragment;
import timber.log.Timber;

public class RegisterDeviceFragment extends MdocFragment {
    public static String TAG = "RegisterDeviceFragment";

    @BindView(R.id.webViewDevices)
    WebView webView;

    @Override
    public NavigationItem getNavigationItem() {
        return NavigationItem.Devices;
    }

    MdocActivity activity;
    private static final String SUCCESS = "https://developer.w2e.fi/link/?succeeded=";

    @Override
    protected int setResourceId() {
        return R.layout.fragment_register_device;
    }

    @Override
    protected void init(Bundle savedInstanceState) {
        activity = getMdocActivity();

        String url = "";

        if (getArguments() != null) {
            url = getArguments().getString(MdocConstants.REGISTER_URL);
        }
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, String url) {
                return shouldOverrideUrlLoading(url);
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest request) {
                Uri uri = request.getUrl();
                return shouldOverrideUrlLoading(uri.toString());
            }

            private boolean shouldOverrideUrlLoading(final String url) {
                Timber.i(TAG, "shouldOverrideUrlLoading() URL :%s ", url);
                if (url.contains(SUCCESS)) {
                    View view = getView();
                    if (view != null) {
                        Navigation.findNavController(view).popBackStack();
                    }
                }

                return false;
            }
        });

        webView.loadUrl(url);
    }
}

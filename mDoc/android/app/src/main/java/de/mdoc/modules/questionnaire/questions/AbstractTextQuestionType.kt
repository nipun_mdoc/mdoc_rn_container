package de.mdoc.modules.questionnaire.questions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.questionnaire.answer_validation.PollQuestionTextType
import de.mdoc.pojo.ExtendedQuestion
import de.mdoc.pojo.Question
import kotlinx.android.synthetic.main.long_text_layout.view.*
import kotlinx.android.synthetic.main.short_text_layout.view.answerEdt
import rx.android.schedulers.AndroidSchedulers

internal abstract class AbstractTextQuestionType: QuestionType() {

    abstract val layoutResId: Int

    override fun onCreateQuestionView(parent: ViewGroup, question: ExtendedQuestion): View {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(layoutResId, parent, false)
        questionIsAnswered.value=false
        val answerEdt = view.answerEdt

        question.answer?.let {
            answerEdt.setText(it.answerData)
            answers.add(it.answerData)
        }

        RxTextView.textChanges(answerEdt)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                questionIsAnswered.value = it.isNotEmpty()
            }

        if (MdocConstants.EXTERNAL_INPUT_TYPE_TIME == question.pollQuestionExternalType) {
            answerEdt.isClickable = true
        }
        else if (MdocConstants.EXTERNAL_INPUT_TYPE_DATE == question.pollQuestionExternalType) {
            answerEdt.isClickable = true
        }
        return view
    }

    override fun onSaveQuestion(questionView: View, question: ExtendedQuestion) {
        super.onSaveQuestion(questionView, question)
        answers.clear()
        val answerEdt = questionView.answerEdt
        val newAnswer = answerEdt.text.toString()
        if (newAnswer.isNotEmpty()) {
            val oldAnswer = question.answer?.answerData
            if (oldAnswer == null || newAnswer != oldAnswer) {
                isNewAnswer = true
            }
            answers.add(newAnswer)
        }
    }
}
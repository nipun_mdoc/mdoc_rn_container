package de.mdoc.modules.questionnaire.answer_validation

import android.text.InputType
import android.view.View
import android.widget.EditText
import com.google.android.material.textfield.TextInputLayout
import com.jakewharton.rxbinding.widget.RxTextView
import de.mdoc.R
import rx.android.schedulers.AndroidSchedulers
import java.util.concurrent.TimeUnit

class MandatoryValidator(view: View?, validationKey: String?): AbstractValidator() {

    private val validation = PollQuestionTextType.fromKey(validationKey)
    private val editText = view?.findViewById<EditText>(R.id.answerEdt)
    private val textInputLayout = view?.findViewById<TextInputLayout>(R.id.answerWrapper)

    init {
        textInputLayout?.helperText = textInputLayout?.context?.getString(validation.description)

        when (validation) {
            PollQuestionTextType.INTEGER -> editText?.inputType = InputType.TYPE_CLASS_NUMBER
            PollQuestionTextType.DOUBLE  -> editText?.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
            PollQuestionTextType.STRING -> editText?.inputType = InputType.TYPE_CLASS_TEXT
        }

        RxTextView.textChanges(editText as EditText)
                .debounce(100,
                        TimeUnit.MILLISECONDS,
                        AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    isValidAnswer()
                }
    }

    override fun isValidAnswer(): Boolean {
        editText?.let {
            return if (validateFactory(it.text.toString())) {
                setValidUi()
                true
            }
            else {
                setInvalidUi()
                false
            }
        }
        setValidUi()
        return true
    }

    private fun validateFactory(inputText:String): Boolean {
        return when (validation) {
            PollQuestionTextType.INTEGER -> validateField(inputText)
            PollQuestionTextType.DOUBLE  -> validateDecimal(inputText)
            PollQuestionTextType.STRING -> validateField(inputText)
        }
    }

    private fun validateField(inputText: String): Boolean {
        return if (inputText.isEmpty()){
            setInvalidUi()
            false
        } else {
            setValidUi()
            true
        }
    }

    private fun validateDecimal(inputText:String): Boolean {
            if (inputText.isEmpty()) {
                setInvalidUi()
                return false
            }
            else if (inputText.contains(".")) {
                val indexOfDecimal: Int = inputText.indexOf(".")
                val decimals = inputText.substring(indexOfDecimal)
                if (decimals.length<2) {
                    return false
                }
            }
            else if (inputText.contains(",")) {
                val indexOfDecimal: Int = inputText.indexOf(",")
                val decimals = inputText.substring(indexOfDecimal)
                if (decimals.length<2) {
                    return false
                }
            }
            setValidUi()
            return true
    }

    private fun setValidUi() {
        textInputLayout?.isErrorEnabled = false
        editText?.background = textInputLayout?.context?.getDrawable(R.drawable.normal_stroke_text_field)
    }

    private fun setInvalidUi() {
        textInputLayout?.isErrorEnabled = true
        textInputLayout?.errorIconDrawable = null
        textInputLayout?.error = textInputLayout?.context?.getString(R.string.questionnaire_mandatory_warning_msg)
        editText?.background = textInputLayout?.context?.getDrawable(R.drawable.mandatory_stroke)
    }
}
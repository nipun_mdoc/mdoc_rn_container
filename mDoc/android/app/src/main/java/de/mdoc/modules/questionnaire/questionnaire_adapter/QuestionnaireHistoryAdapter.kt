package de.mdoc.modules.questionnaire.questionnaire_adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import de.mdoc.data.questionnaire.Questionnaire
import de.mdoc.modules.questionnaire.history.QuestionnaireHistoryItem
import de.mdoc.util.MdocUtil
import kotlinx.android.synthetic.main.placeholder_questionnaire_history.view.*

class QuestionnaireHistoryAdapter(
    private val listener: Listener
) : RecyclerView.Adapter<QuestionnaireHistoryViewHolder>() {

    interface Listener {

        fun onQuestionnaireSelected(questionnaire: Questionnaire)

    }

    var items: List<QuestionnaireHistoryItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): QuestionnaireHistoryViewHolder {


        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.placeholder_questionnaire_history, parent, false)

        return QuestionnaireHistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: QuestionnaireHistoryViewHolder, position: Int) {
        val questionnaireAnswer = items[position].questionnaire

        holder.txtTitle.text = questionnaireAnswer.pollTitle
        val finishTime = questionnaireAnswer.pollVotingActions[0].finished

        val date = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_DOT_FORMAT, finishTime)
        val time = MdocUtil.getDateFromMS(MdocConstants.HOUR_AND_MINUTES, finishTime)

        holder.txtCompleted.text =
            holder.itemView.resources.getString(R.string.closed_history_questionnaire, date, time)

        holder.cvMainThumbnails.setOnClickListener {
            listener.onQuestionnaireSelected(questionnaireAnswer)
        }

    }

    override fun getItemCount(): Int {
        return items.size
    }

}

class QuestionnaireHistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val txtTitle: TextView = itemView.txt_title
    val txtCompleted: TextView = itemView.txt_completed
    val cvMainThumbnails: CardView = itemView.cv_main_thumbnail
}


package de.mdoc.util

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView

class GenericRecyclerAdapter(val items: List<Any>,
                             private val layoutId: Int,
                             private val handler: Any? = null)
    : RecyclerView.Adapter<GenericRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater,
                layoutId,
                parent,
                false
                                                              )
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position],handler)

    inner class ViewHolder(val binding: ViewDataBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Any,handler:Any?) {
            binding.setVariable(BR.item, item)
            handler?.let {
                binding.setVariable(BR.handler, it)
            }
            binding.executePendingBindings()
        }
    }
}
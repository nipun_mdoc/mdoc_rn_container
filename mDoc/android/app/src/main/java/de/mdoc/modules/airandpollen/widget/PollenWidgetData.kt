package de.mdoc.modules.airandpollen.widget

import android.content.Context
import de.mdoc.modules.airandpollen.data.AirAndPollenInfo
import de.mdoc.modules.airandpollen.data.PollenLevel
import de.mdoc.modules.airandpollen.data.PollenType

class PollenWidgetData(
    val items: List<AirAndPollenInfo>
) {

    fun getItems(context: Context, count: Int) : List<PollenWidgetItem> {
        return items.sortedBy {
            it.type.getName(context)
        }.sortedByDescending {
            it.level.ordinal
        }.take(count).map {
            PollenWidgetItem(
                it.type,
                it.level
            )
        }
    }

}

class PollenWidgetItem(
    val type: PollenType,
    val level: PollenLevel
) {
    val amount = when (level) {
        PollenLevel.None -> 0
        PollenLevel.Low -> 1
        PollenLevel.Medium -> 2
        PollenLevel.High -> 3
    }
}
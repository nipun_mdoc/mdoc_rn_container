package de.mdoc.modules.files.base

interface FilesBaseFragmentAdditionalFunctions {
    fun reloadData()
    fun onFileDeleted(id: String)
}
package de.mdoc.modules.diary

import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.Base64
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.view.menu.MenuBuilder
import androidx.appcompat.view.menu.MenuPopupHelper
import androidx.appcompat.widget.PopupMenu
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.constants.MdocConstants
import de.mdoc.modules.diary.search.SearchDiaryFragmentDirections
import de.mdoc.pojo.DiaryList
import de.mdoc.util.MdocUtil

class DiaryAdapter(private val items: List<DiaryList>,
                   var activity: MdocActivity,
                   var isComingFromSearch: Boolean = false, private val callback: DiaryAdapterCallback):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        val inflater = LayoutInflater.from(parent.context)
        if (viewType == MdocConstants.DIARY_HEADER) {
            view = inflater.inflate(R.layout.diary_header_item, parent, false)
            return DiaryHeaderViewHolder(view)
        }
        return if (viewType == MdocConstants.DIARY_FOOTER) {
            view = inflater.inflate(R.layout.diary_footer_item, parent, false)
            DiaryFooterViewHolder(view)
        }
        else {
            view = inflater.inflate(R.layout.diary_entry_item, parent, false)
            DiaryViewHolder(view, isComingFromSearch, callback)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].itemViewType
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        // Note: You have the type information directly inside the ViewHolder so you don't need
        // to check the ViewHolder's instance type
        when {
            item.itemViewType == MdocConstants.DIARY_HEADER -> {
                (holder as DiaryHeaderViewHolder).updateUI(item)
            }

            item.itemViewType == MdocConstants.DIARY_FOOTER -> {
            }

            else                                            -> (holder as DiaryViewHolder).updateUI(item, activity)
        }
    }
}

internal class DiaryViewHolder(itemView: View, private val isComingFromSearch: Boolean, private val callback: DiaryAdapterCallback):
        RecyclerView.ViewHolder(itemView) {

    private val entryCategory: TextView
    private val diaryTextTv: TextView
    private val diaryDateTv: TextView
    var icon: ImageView
    var containerLl: LinearLayout
    var questionnaireTV: TextView
    private val moreIv: ImageView

    init {
        this.entryCategory = itemView.findViewById<View>(R.id.entryCategory) as TextView
        this.diaryTextTv = itemView.findViewById<View>(R.id.commentTv) as TextView
        this.diaryDateTv = itemView.findViewById<View>(R.id.dateTimeTv) as TextView
        this.icon = itemView.findViewById<View>(R.id.entryIcon) as ImageView
        this.containerLl = itemView.findViewById<View>(R.id.containerLl) as LinearLayout
        this.questionnaireTV = itemView.findViewById<View>(R.id.questionnaireTV) as TextView
        this.moreIv = itemView.findViewById<View>(R.id.moreIv) as ImageView
    }

    fun updateUI(list: DiaryList, activity: MdocActivity) {
        val title = if (
            list.diaryConfig?.title == null) {
            list.diaryConfig?.coding?.display
        }
        else {
            list.diaryConfig?.title
        }
        val description = list?.description
        val time = MdocUtil.getDateFromMS(MdocConstants.DAY_MONTH_TIME_DOT_FORMAT, list?.start)

        entryCategory.text = title
        diaryTextTv.text = description
        diaryDateTv.text = time


        list?.diaryConfig?.coding?.imageBase64?.let {
            var base64Image = "";

            base64Image = if (it.split(",").size > 1) {
                it.split(",")[1]
            }
            else {
                it.split(",")[0]
            }
            val decodedString = Base64.decode(base64Image, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            icon.setImageBitmap(decodedByte)

            list?.diaryConfig?.coding?.color?.let {
                val strokeWidth = 5
                val strokeColor = Color.parseColor(it)
                val fillColor = Color.parseColor("#ffffff")
                val gD = GradientDrawable()
                gD.setColor(fillColor)
                gD.shape = GradientDrawable.OVAL
                gD.setStroke(strokeWidth, strokeColor)
                icon.background = gD
            }
        }

        if (diaryTextTv.text.isNullOrEmpty()) {
            diaryTextTv.visibility = View.INVISIBLE
        } else {
            diaryTextTv.visibility = View.VISIBLE
        }

        containerLl.setOnClickListener {

        }

        if(!list?.pollVotingActionId.isNullOrEmpty()) {
            questionnaireTV.visibility = View.VISIBLE
        } else {
            questionnaireTV.visibility = View.GONE
        }

        questionnaireTV.setOnClickListener {
            val action = if (isComingFromSearch) {
                    SearchDiaryFragmentDirections.actionSearchDiaryFragmentToQuestionnaireAnswersFragment(
                            list.pollVotingActionId)
                } else {
                    DiaryDashboardFragmentDirections.actionDiaryDashboardFragmentToQuestionnaireAnswersFragment(list.pollVotingActionId)
                }
            activity?.findNavController(R.id.navigation_host_fragment)?.navigate(action)
        }

        moreIv.setOnClickListener {
            showPopup(it, list, callback)
        }
    }

    fun showPopup(v: View, diaryItem: DiaryList, callback: DiaryAdapterCallback) {
        val popup = PopupMenu(v.context, v)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.menu_diary_item_actions, popup.menu)
        popup.setOnMenuItemClickListener { item ->
            when (item.itemId) {
                R.id.edit -> callback.onEditFileRequest(diaryItem)
                R.id.delete -> callback.onDeleteFileRequest(diaryItem)
            }
            false
        }

        val menuHelper = MenuPopupHelper(v.context, popup.menu as MenuBuilder, v)
        menuHelper.setForceShowIcon(true)
        menuHelper.gravity = Gravity.START
        menuHelper.show()
    }
}

internal class DiaryHeaderViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val monthNameTv: TextView = itemView.findViewById<View>(R.id.monthNameTv) as TextView

    fun updateUI(list: DiaryList) {
        monthNameTv.setText(MdocUtil.getMonthResIdByNumber(list.monthValue))
    }
}

internal class DiaryFooterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
package de.mdoc.network.response;

import com.google.gson.annotations.SerializedName;

import de.mdoc.pojo.Diary;

public class DiaryResponse extends MdocResponse {

    @SerializedName("data")
    private Diary data;

    public Diary getData() {
        return data;
    }

    public void setData(Diary data) {
        this.data = data;
    }
}

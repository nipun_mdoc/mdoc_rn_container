package de.mdoc.modules.questionnaire.ext

import de.mdoc.pojo.ExtendedQuestion


fun ArrayList<ExtendedQuestion>.getIndexForFirstUnfilledQuestion():Int {
    this.forEachIndexed { index, question ->
        if (question.answer == null) {
            return index
        }
    }

    return 0
}

fun ArrayList<ExtendedQuestion>.getIndexForLastSeen(lastSeen: String?): Int {
    /*
    * Data class: de.mdoc.pojo.QuestionnaireDetailsData -> lastSeenPollQuestionId
    *
    * 'lastSeenPollQuestionId' data has been started to fill up on Friday, 27 December 2019.
    * This means that all users before that date will have no data about 'lastSeenPollQuestionId'
    *
    * UI part of this functionality is related to fragment_question_go_to.xml  -> btnGoToLastSession
    *
    * When this case happen, we'll hide btnGoToLastSession button.
    *
    * */
    if (lastSeen == null) {
        return -1
    }

    this.forEachIndexed { index, extendedQuestion ->
        if (extendedQuestion.id == lastSeen) {
            return index
        }
    }
    // Return default index in case when 'lastSeen' doesn't match to any question id
    return 0
}

fun ArrayList<ExtendedQuestion>.getQuestionIndexForFirstUnfilledQuestion():Int {
    this.forEachIndexed { index, question ->
        if (question.answer == null) {
            return question.index
        }
    }

    return 0
}

fun ArrayList<ExtendedQuestion>.getRealIndexForLastSeen(lastSeen: String?): Int {
    /*
    * Data class: de.mdoc.pojo.QuestionnaireDetailsData -> lastSeenPollQuestionId
    *
    * 'lastSeenPollQuestionId' data has been started to fill up on Friday, 27 December 2019.
    * This means that all users before that date will have no data about 'lastSeenPollQuestionId'
    *
    * UI part of this functionality is related to fragment_question_go_to.xml  -> btnGoToLastSession
    *
    * When this case happen, we'll hide btnGoToLastSession button.
    *
    * */
    if (lastSeen == null) {
        return -1
    }

    this.forEachIndexed { index, extendedQuestion ->
        if (extendedQuestion.id == lastSeen) {
            return extendedQuestion.index
        }
    }
    // Return default index in case when 'lastSeen' doesn't match to any question id
    return 0
}


fun ArrayList<ExtendedQuestion>.getSectionNameForFirstUnfilledQuestion(): String {
    this.forEachIndexed { index, question ->
        if (question.answer == null) {
            return question.sectionName
        }
    }

    return ""
}

fun ArrayList<ExtendedQuestion>.getSectionNameForLastSeen(lastSeen: String?): String {
    /*
    * Data class: de.mdoc.pojo.QuestionnaireDetailsData -> lastSeenPollQuestionId
    *
    * 'lastSeenPollQuestionId' data has been started to fill up on Friday, 27 December 2019.
    * This means that all users before that date will have no data about 'lastSeenPollQuestionId'
    *
    * UI part of this functionality is related to fragment_question_go_to.xml  -> btnGoToLastSession
    *
    * When this case happen, we'll hide btnGoToLastSession button.
    *
    * */
    if (lastSeen == null) {
        return ""
    }

    this.forEachIndexed { index, extendedQuestion ->
        if (extendedQuestion.id == lastSeen) {
            return extendedQuestion.sectionName
        }
    }
    // Return default index in case when 'lastSeen' doesn't match to any question id
    return ""
}
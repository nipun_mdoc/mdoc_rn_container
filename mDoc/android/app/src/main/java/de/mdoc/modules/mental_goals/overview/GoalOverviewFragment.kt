package de.mdoc.modules.mental_goals.overview

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.MainNavDirections
import de.mdoc.R
import de.mdoc.activities.MdocActivity
import de.mdoc.activities.navigation.NavigationItem
import de.mdoc.fragments.MdocFragment
import de.mdoc.modules.mental_goals.GoalCreatedDialogFragment
import de.mdoc.modules.mental_goals.MentalGoalsUtil
import de.mdoc.modules.mental_goals.data_goals.MentalGoalsData
import de.mdoc.modules.mental_goals.overview.data.MentalGoalsRepository
import de.mdoc.network.RestClient
import de.mdoc.util.MdocUtil
import de.mdoc.util.handleOnBackPressed
import de.mdoc.util.onCreateOptionsMenu
import de.mdoc.viewmodel.ValueLiveData
import de.mdoc.viewmodel.bindVisibleGone
import de.mdoc.viewmodel.viewModel
import kotlinx.android.synthetic.main.fragment_goal_overview.*

class GoalOverviewFragment: MdocFragment(), GoalsAdapter.Listener {

    private val viewModel by viewModel {
        GoalOverviewViewModel(
                repository = MentalGoalsRepository(
                        service = RestClient.getService()
                                                  )
                             )
    }
    private val goalsAdapter = GoalsAdapter(this)
    lateinit var activity: MdocActivity

    override fun setResourceId(): Int {
        return R.layout.fragment_goal_overview
    }

    override val navigationItem: NavigationItem = NavigationItem.MentalGoals

    override fun init(savedInstanceState: Bundle?) {
        activity = mdocActivity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        rv_goals.adapter = goalsAdapter
        rv_goals.layoutManager = LinearLayoutManager(
                activity,
                RecyclerView.VERTICAL,
                false
                                                    )

        fillNumber(viewModel.countNewGoals, txt_new_number)
        fillNumber(viewModel.countAlmostReached, txt_almost_number)
        fillNumber(viewModel.countCompletedGoals, txt_reached_number)


        viewModel.selectedTab.observe(this, ::fillSelectedTab)

        viewModel.listOfGoals.observe(this) {
            goalsAdapter.items = it ?: emptyList()
            rv_goals.visibility = if (it != null) View.VISIBLE else View.GONE
        }

        bindVisibleGone(progress, viewModel.isInProgress)
        bindVisibleGone(noOpenOrClosedGoals, viewModel.isShowNoOpenOrClosedGoals)
        bindVisibleGone(noNewGoals, viewModel.isShowNoNewGoals)
        bindVisibleGone(noClosedGoals, viewModel.isShowNoClosedGoals)

        viewModel.isInProgress.observe(this) {
            if (it) {
                progress.show()
            }
            else {
                progress.hideNow()
            }
        }

        viewModel.error.observerAndClean(this) {
            MdocUtil.showToastLong(activity, resources.getString(R.string.err_request_failed))
        }

        setupListeners()
        val isCreated = MentalGoalsUtil.showCreatedMessage
        showPopupIfNeeded(isCreated)

        handleOnBackPressed {
            val hasMoreFragment = findNavController().popBackStack(R.id.moreFragment, false)
            if (!hasMoreFragment) {
                val hasDashboardFragment = findNavController().popBackStack(R.id.dashboardFragment, false)
                if (!hasDashboardFragment) {
                    findNavController().popBackStack()
                }
            }
        }
    }

    private fun showPopupIfNeeded(isCreated: Boolean) {
        if (isCreated) {
            val shareFragment = GoalCreatedDialogFragment().newInstance()
            val fragmentManager = (activity as AppCompatActivity).supportFragmentManager
            shareFragment.show(fragmentManager, "goalCreation")
        }
        MentalGoalsUtil.showCreatedMessage = false
    }

    private fun fillNumber(source: ValueLiveData<Int>, target: TextView) {
        source.observe(this) {
            target.text = String.format("%02d", it)
        }
    }

    private fun setupListeners() {
        ll_open.setOnClickListener {
            viewModel.selectedTab.set(GoalOverviewTab.Open)
            viewModel.getOpenGoals()
        }
        ll_closed.setOnClickListener {
            viewModel.selectedTab.set(GoalOverviewTab.Closed)
            viewModel.getClosedGoals()
        }
    }

    private fun fillSelectedTab(tab: GoalOverviewTab) {
        openV.visibility = if (tab == GoalOverviewTab.Open) View.VISIBLE else View.INVISIBLE
        closedV.visibility = if (tab == GoalOverviewTab.Closed) View.VISIBLE else View.INVISIBLE
        val black = ContextCompat.getColor(requireContext(), R.color.black)
        val gray = ContextCompat.getColor(requireContext(), R.color.light_gray_text_88888)

        txt_open.setTextColor(if (tab == GoalOverviewTab.Open) black else gray)
        txt_closed.setTextColor(if (tab == GoalOverviewTab.Closed) black else gray)
    }

    override fun onMentalGoalClick(item: MentalGoalsData) {
        MentalGoalsUtil.goalTitle = item.title
        MentalGoalsUtil.goalDescription = item.description
        MentalGoalsUtil.timePeriod = item.dueDate
        MentalGoalsUtil.ifThenItems = item.ifThenPlans
        MentalGoalsUtil.actionItems = item.items
        MentalGoalsUtil.goalId = item.id
        MentalGoalsUtil.status = item.status
        MentalGoalsUtil.coachCreated = item.coachCreated
        val action = GoalOverviewFragmentDirections.actionGoalOverviewFragmentToGoalDetailFragment()
        findNavController().navigate(action)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        onCreateOptionsMenu(menu, inflater, R.menu.menu_mental_goal_overview)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.addNewGoal) {
            MentalGoalsUtil.clearData()
            val action = MainNavDirections.globalActionToMentalGoalCreation()
            findNavController().navigate(action)
        }
        return super.onOptionsItemSelected(item)
    }
}
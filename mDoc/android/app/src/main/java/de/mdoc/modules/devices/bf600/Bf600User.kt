package de.mdoc.modules.devices.bf600

data class Bf600User(val userIndex: String? = null,
                     val birthDate: Long? = null,
                     val height: Double? = null,
                     val gender: Int? = null,
                     val activityLevel: Int? = null)

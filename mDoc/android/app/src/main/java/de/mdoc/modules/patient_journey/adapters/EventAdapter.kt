package de.mdoc.modules.patient_journey.adapters

import android.animation.ObjectAnimator
import android.content.Context
import android.text.TextUtils
import android.util.TypedValue.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import de.mdoc.R
import de.mdoc.modules.patient_journey.data.ChildCarePlan
import kotlinx.android.synthetic.main.placeholder_journey_first.view.*
import kotlinx.android.synthetic.main.placeholder_main_event.view.*
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.*
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.ic_show_less
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.ic_show_more
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.rv_activities
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.txt_day_count
import kotlinx.android.synthetic.main.placeholder_secondary_event.view.txt_details
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class EventAdapter(val context: Context):
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items: ArrayList<ChildCarePlan> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): RecyclerView.ViewHolder {
        val view: View

        return if (viewType == MAIN_EVENT) {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_main_event,
                        parent, false)
            MainEventViewHolder(view)
        }
        else if (viewType == FIRST_ITEM) {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_journey_first,
                        parent, false)
            FirstItemViewHolder(view)
        }
        else {
            view = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_secondary_event,
                        parent, false)
            SecondaryEventViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when {
            position == 0                           -> (holder as FirstItemViewHolder).bind(item)
            getItemViewType(position) == MAIN_EVENT -> (holder as MainEventViewHolder).bind(item, context)
            else                                    -> (holder as SecondaryEventViewHolder).bind(item, items, context,
                    this)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            position == 0                                              -> FIRST_ITEM
            items[position].codingDisplayBySystem(TIMING) == MAIN_CODE -> MAIN_EVENT
            else                                                       -> SECONDARY_EVENT
        }
    }

    fun setItems(list: List<ChildCarePlan>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }

    companion object {
        const val TIMING = "http://hl7.org/fhir/ValueSet/timing"
        const val MAIN_CODE = "main_event"
        const val MAIN_EVENT = 0
        const val SECONDARY_EVENT = 1
        const val PRE_MAIN_CODE = "before_main_event"
        const val FIRST_ITEM = 2
    }
}

class SecondaryEventViewHolder(itemView: View):
        RecyclerView.ViewHolder(itemView) {

    private val title: TextView = itemView.txt_title
    private val details: TextView = itemView.txt_details
    private val activityAdapter: RecyclerView = itemView.rv_activities
    private val showMore: ImageView = itemView.ic_show_more
    private val showLess: ImageView = itemView.ic_show_less
    private val circle: ImageView = itemView.ic_circle
    private val dayCount: TextView = itemView.txt_day_count
    private val suffix: TextView = itemView.txt_day_sufix
    private val beforeAfter: TextView = itemView.txt_before_after
    private val sideLine: View = itemView.dashed_first

    fun bind(item: ChildCarePlan, items: List<ChildCarePlan>, context: Context, adapter: EventAdapter) {
        val startTime = DateTime(item.period.start)
        val endTime = DateTime(item.period.end)
        if (startTime.withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0).isBeforeNow && endTime.isAfterNow) {
            title.background = ContextCompat.getDrawable(context, R.drawable.background_opacity_10)
            sideLine.background = ContextCompat.getDrawable(context, R.drawable.rectangle_808080)
            circle.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.circle_primary))
            title.setTextSize(COMPLEX_UNIT_SP, 19f)
        }
        else {
            title.background = null
            sideLine.background = ContextCompat.getDrawable(context, R.drawable.horizontal_dashed)
            circle.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.circle_808080))
            title.setTextSize(COMPLEX_UNIT_SP, 17f)
        }

        details.text = de.mdoc.util.StringUtils.fromHtml(item.description)
        activityAdapter.layoutManager = LinearLayoutManager(context)
        val activityList = item.activity?.filter { it.detail?.reasonReference !== null && it.detail.reasonReference.getOrNull(0) !== null && !it.detail.reasonReference.getOrNull(0)?.display.isNullOrEmpty() }
        activityAdapter.adapter = InnerSecondaryEventAdapter(activityList ?: listOf(), context)
        title.text = item.title
        if (item.isExpanded) {
            expand()
        }
        else {
            collapse()
        }
        showMore.setOnClickListener {
            expand()
            items.forEach {
                it.isExpanded = false
            }
            item.isExpanded = true
            adapter.notifyDataSetChanged()
        }

        showLess.setOnClickListener {
            collapse()
        }
        val timeToEvent = item.period.timingPeriod
        val pluralFactory = when (item.period.timingPeriodUnit) {
            "D"  -> R.plurals.pat_journey_days
            "WK" -> R.plurals.pat_journey_weeks
            "MO" -> R.plurals.pat_journey_months
            else -> R.plurals.pat_journey_days
        }
        dayCount.text = context.resources.getQuantityString(pluralFactory, timeToEvent, timeToEvent)

        if (item.codingDisplayBySystem(EventAdapter.TIMING) == EventAdapter.PRE_MAIN_CODE) {
            suffix.text = context.resources.getString(R.string.pat_journey_before_operation)

            if (item.preOrPost == "pre") {
                beforeAfter.visibility = View.VISIBLE
                beforeAfter.text = context.resources.getText(R.string.pat_journey_before)
            }
            else {
                beforeAfter.visibility = View.GONE
            }
        }
        else {
            suffix.text = context.resources.getString(R.string.pat_journey_after_operation)
            if (item.preOrPost == "post") {
                beforeAfter.visibility = View.VISIBLE
                beforeAfter.text = context.resources.getText(R.string.pat_journey_after)
            }
            else {
                beforeAfter.visibility = View.GONE
            }
        }
    }

    private fun expand() {
        showMore.visibility = View.GONE
        showLess.visibility = View.VISIBLE
        activityAdapter.visibility = View.VISIBLE
        details.maxLines = Integer.MAX_VALUE
        details.ellipsize = null
    }

    private fun collapse() {
        showLess.visibility = View.INVISIBLE
        showMore.visibility = View.VISIBLE
        activityAdapter.visibility = View.GONE
        details.maxLines = 2
        details.ellipsize = TextUtils.TruncateAt.END
    }
}

class MainEventViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val month: TextView = itemView.txt_month_main
    private val title: TextView = itemView.txt_title_main
    private val formatter = DateTimeFormat.forPattern("dd MMMM - HH:mm")

    fun bind(item: ChildCarePlan, context: Context) {
        val mainEventTime = formatter.print(item.period.start) + " " + context.resources.getString(
                R.string.pat_journey_hour)
        title.text = de.mdoc.util.StringUtils.fromHtml(item.description)
        month.text = mainEventTime
    }
}

class FirstItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val details: TextView = itemView.txt_details
    private val fadingEdgeLayout = itemView.fadingEdgeLayout
    private val viewMore = itemView.view_more
    private val viewLess = itemView.view_less
    fun bind(item: ChildCarePlan) {
        details.text = de.mdoc.util.StringUtils.fromHtml(item.description)
        handleViewMoreView()
    }

    private fun viewMoreHandler() {
        fadingEdgeLayout?.setFadeEdges(false, false, false, false)
        val animator: ObjectAnimator = ObjectAnimator.ofInt(details, "maxLines", details.lineCount)
        animator.setDuration(100)
            .start()
        viewLess?.visibility = View.VISIBLE
        viewMore?.visibility = View.GONE
    }

    private fun viewLessHandler() {
        fadingEdgeLayout.setFadeEdges(false, false, true, false)
        val animator: ObjectAnimator = ObjectAnimator.ofInt(details, "maxLines", 2)
        animator.setDuration(100)
            .start()
        viewMore?.visibility = View.VISIBLE
        viewLess?.visibility = View.GONE
    }

    private fun setupTextFade() {
        val lenPx = applyDimension(COMPLEX_UNIT_DIP, 100000f,
                fadingEdgeLayout.context.resources.displayMetrics)
            .toInt()

        fadingEdgeLayout?.setFadeEdges(false, false, true, false)
        fadingEdgeLayout?.setFadeSizes(0, 0, lenPx, 0)
    }

    private fun handleViewMoreView() {
        details.post {
            val numberOfLines: Int = details.lineCount
            if (numberOfLines <= 2) { // maxLines = 2
                viewMore.visibility = View.GONE
            } else {
                if (viewMore.visibility == View.VISIBLE) {
                    setupTextFade()
                }
                viewMore.setOnClickListener { viewMoreHandler() }
                viewLess.setOnClickListener { viewLessHandler() }
            }
        }
    }

    private fun removeEmptyFromBack(elements: ArrayList<String>): ArrayList<String> {
        while (elements[elements.size - 1].isEmpty()) {
            elements.removeAt(elements.size - 1)
        }
        return elements
    }
}
package de.mdoc.network.request;

import java.util.ArrayList;

/**
 * Created by ema on 10/18/17.
 */

public class MealChoiceRequest {

    private String caseId;
    private ArrayList<String> mealPlanIds;

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public ArrayList<String> getMealPlanIds() {
        return mealPlanIds;
    }

    public void setMealPlanIds(ArrayList<String> mealPlanIds) {
        this.mealPlanIds = mealPlanIds;
    }
}

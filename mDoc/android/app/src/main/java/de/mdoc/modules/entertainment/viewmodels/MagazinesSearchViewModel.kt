package de.mdoc.modules.entertainment.viewmodels

import androidx.lifecycle.viewModelScope
import de.mdoc.modules.common.viewmodel.BaseSearchViewModel
import de.mdoc.network.response.entertainment.ResponseSearchResultsMediaLightDTO_
import de.mdoc.network.response.entertainment.listSize
import de.mdoc.pojo.DiaryList
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class MagazinesSearchViewModel(private val mDocService: IMdocService) : BaseSearchViewModel<ResponseSearchResultsMediaLightDTO_>() {

    override fun requestSearch(query: String) {
        super.requestSearch(query)
        if (query.isNotEmpty()) {
            viewModelScope.launch {
                try {
                    searchResponse.value = mDocService.getEntertainmentStatisticAllSearch(query)
                    onDataLoaded(searchResponse.value, searchResponse.value?.listSize())

                } catch (e: java.lang.Exception) {
                    onError(e)
                }
            }
        }
    }

    override fun requestDelete(query: DiaryList) {
    }

    override fun requestUpdate(query: DiaryList) {
    }
}
package de.mdoc.modules.vitals.vitals_widget

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import de.mdoc.data.create_bt_device.ObservationResponseData
import de.mdoc.service.IMdocService
import kotlinx.coroutines.launch

class VitalsWidgetViewModel(private val mDocService: IMdocService): ViewModel() {
    var isLoading = MutableLiveData(false)
    var isShowLoadingError = MutableLiveData(false)
    val isShowContent = MutableLiveData(false)
    val isShowNoData = MutableLiveData(false)
    var vitalsData = MutableLiveData(listOf<ObservationResponseData>())

    fun getObservations() {
        viewModelScope.launch {
            isShowLoadingError.value = false

            try {
                isLoading.value = true
                isShowContent.value = false
                isShowNoData.value = false
                val response = mDocService.coroutineGetLatestObservations()
                    .data

                if (response.isNullOrEmpty()) {
                    isShowContent.value = false
                    isShowNoData.value = true
                }
                else {
                    vitalsData.value = if (response.size > 1) {
                        response.subList(0, 2)
                    }
                    else {
                        response
                    }
                    isShowContent.value = true
                    isShowNoData.value = false
                }
                isLoading.value = false
            } catch (e: java.lang.Exception) {
                isLoading.value = false
                isShowNoData.value = true
                isShowContent.value = false
            }
        }
    }
}
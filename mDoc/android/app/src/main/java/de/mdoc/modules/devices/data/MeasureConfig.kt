package de.mdoc.modules.devices.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MeasureConfig(
        val name:String?=null,
        val display:String?=null
):Parcelable
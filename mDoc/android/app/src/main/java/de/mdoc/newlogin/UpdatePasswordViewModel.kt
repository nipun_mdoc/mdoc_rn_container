package de.mdoc.newlogin

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.mdoc.R
import de.mdoc.activities.login.ProgressHandler
import de.mdoc.network.RestClient
import de.mdoc.newlogin.Model.ImprintResponse
import de.mdoc.newlogin.Model.LoginResponseData
import de.mdoc.newlogin.Model.UpdatePasswordResponseData
import de.mdoc.util.MdocAppHelper
import de.mdoc.util.MdocUtil
import de.mdoc.util.token_otp.Token
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class UpdatePasswordViewModel : ViewModel() {
    var progressHandler: ProgressHandler? = null
    var context: Context? = null
    var authListener: AuthListener? = null
    var toastListener: RegistratioToastListener? = null
    var userMutableLiveData: MutableLiveData<UpdatePasswordRequest>? = null

    val password_edit: MutableLiveData<String> = MutableLiveData()
    val confirm_password_edit: MutableLiveData<String> = MutableLiveData()
    var token : String? = null

    val requestData: MutableLiveData<UpdatePasswordRequest>
        get() {
            if (userMutableLiveData == null) {
                userMutableLiveData = MutableLiveData()
            }
            return userMutableLiveData as MutableLiveData<UpdatePasswordRequest>
        }

    fun onUpdatePasswordClick(view: View?) {
        context = view!!.context
        progressHandler = ProgressHandler(this.context!!)

        var updatePasswordRequest = UpdatePasswordRequest()
        updatePasswordRequest!!.client_id = this.context?.getString(R.string.keycloack_clinic_Id)
        updatePasswordRequest!!.confirmPassword = if (confirm_password_edit.value != null) confirm_password_edit.value!! else ""
        updatePasswordRequest!!.password = if (password_edit.value != null) password_edit.value!! else ""
        updatePasswordRequest!!.token = token
        userMutableLiveData!!.value = updatePasswordRequest
    }

    fun onImpressumClick(view: View?) {
        loadImprintData("imprint", view!!.context)
    }

    fun onDatenschutzDieClick(view: View?) {
        loadImprintData("data", view!!.context)
    }

    fun onAGBClick(view: View?) {
        loadImprintData("privacy", view!!.context)
    }

    public fun loadData(updatePasswordRequest: UpdatePasswordRequest, context: Context) {
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().updatePassword(updatePasswordRequest, Locale.getDefault().language);
        call!!.enqueue(object : Callback<UpdatePasswordResponseData?> {
            override fun onResponse(call: Call<UpdatePasswordResponseData?>, response: Response<UpdatePasswordResponseData?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    MdocUtil.showToastLong(context, context?.resources?.getString(R.string.reset_password_success))
                    authListener?.sucess()

                } else {
                    try{
                        val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), UpdatePasswordResponseData::class.java)
                        MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.login_failed))
                    }catch (ex : Exception){
                        MdocUtil.showToastLong(context, context?.resources?.getString(R.string.login_failed))
                    }
                }
            }

            override fun onFailure(call: Call<UpdatePasswordResponseData?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.login_failed))
            }
        })
    }

    fun loadImprintData(requestType: String, context: Context) {
        progressHandler = ProgressHandler(context)
        progressHandler?.showProgress()
        var call = RestClient.getAuthServiceLogin().getImprintData(Locale.getDefault().language)
        call!!.enqueue(object : Callback<ImprintResponse?> {
            override fun onResponse(call: Call<ImprintResponse?>, response: Response<ImprintResponse?>) {
                progressHandler?.hideProgress()
                if (response.isSuccessful) {
                    if(requestType.equals("imprint")){
                        authListener?.onAGBClick(response.body()?.data?.get(0)?.display!!)
                    }else if(requestType.equals("data")){
                        authListener?.onAGBClick(response.body()?.data?.get(1)?.display!!)
                    }else if(requestType.equals("privacy")){
                        authListener?.onAGBClick(response.body()?.data?.get(2)?.display!!)
                    }

                } else {
                    val errorResponse = Gson().fromJson(response.errorBody()!!.charStream(), LoginResponseData::class.java)
                    MdocUtil.showToastLong(context, if(errorResponse !== null && errorResponse.message != "") errorResponse.message else context?.resources?.getString(R.string.error_upload))
                }
            }

            override fun onFailure(call: Call<ImprintResponse?>, t: Throwable) {
                progressHandler?.hideProgress()
                MdocUtil.showToastLong(context, context?.resources?.getString(R.string.error_upload))
            }
        })
    }

    fun onCloseClick(view: View?) {
        authListener?.onCloseClick()
    }

    fun onInfolick(view: View?) {
        toastListener?.registrationToast(view)

    }

}
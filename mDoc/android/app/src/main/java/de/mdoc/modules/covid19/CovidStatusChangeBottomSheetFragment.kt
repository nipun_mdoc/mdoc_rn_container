package de.mdoc.modules.covid19

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.res.ColorStateList
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import de.mdoc.R
import de.mdoc.modules.covid19.data.HealthStatus
import de.mdoc.pojo.CodingItem
import de.mdoc.storage.AppPersistence
import de.mdoc.util.MdocAppHelper
import kotlinx.android.synthetic.main.fragment_covid_status_change.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class CovidStatusChangeBottomSheetFragment: BottomSheetDialogFragment() {

    private var selectedItem: CodingItem? = null
    private var dateSelected: DateTime? = null
    private val formatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    private val dateSelectedFormatter = DateTimeFormat.forPattern("dd.MM.yyyy")
    private val timeSelectedFormatter = DateTimeFormat.forPattern("HH:mm")
    var healthStatusId:String?=null

    interface StatusChangeCallback {
        fun onStatusChangeClick(healthStatus: HealthStatus)
    }

    private var statusChangeCallback: StatusChangeCallback? = null

    fun setStatusChangeListener(statusChangeCallback: StatusChangeCallback) {
        this.statusChangeCallback = statusChangeCallback
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_covid_status_change, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtDateSelected.setText(dateSelectedFormatter.print(dateSelected))
        txtTimeSelected.setText(timeSelectedFormatter.print(dateSelected))
        fillStatus()
        img_close.setOnClickListener {
            dismiss()
        }
        btnProceed.setOnClickListener {
            onProceedClick()
        }
        txtDateSelected?.setOnClickListener {
            showDatePicker()
        }
        txtTimeSelected?.setOnClickListener {
            showTimePicker()
        }
    }

    private fun onProceedClick() {
        if (btnProceed.text == getString(R.string.save)) {
            val statusRequest = createStatusRequest()
            statusChangeCallback?.onStatusChangeClick(statusRequest)
            dismiss()
        }
        else {
            btnProceed.text = getString(R.string.save)
            txt_title.text = resources.getString(R.string.covid_set_test_date)
            statusPlaceholder.visibility = View.GONE
            datePickerPlaceholder.visibility = View.VISIBLE
        }
    }

    private fun fillStatus() {
        statusPlaceholder?.removeAllViews()
        AppPersistence.covidStatusCoding?.forEach { codingItem ->
            val childView = LayoutInflater.from(context)
                .inflate(R.layout.placeholder_covid_status, null)
            val txtStatus: TextView = childView.findViewById(R.id.txtStatus)
            txtStatus.text = codingItem.display
            setBackgroundColor(txtStatus, codingItem.code)

            statusPlaceholder.addView(childView)
            txtStatus.setOnClickListener {
                deselectAll()
                selectedItem = codingItem
                updateProceedButton(codingItem)
                btnProceed.isEnabled = selectedItem != null
                txtStatus.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, R.color.colorPrimary))
            }
        }
    }

    private fun setBackgroundColor(txtStatus: TextView, code: String?) {
        var backgroundColor = R.color.gray_e6e6e6
        if (code == POSITIVE) {
            backgroundColor = R.color.covid_positive
        }
        else if (code == NEGATIVE) {
            backgroundColor = R.color.covid_negative
        }
        txtStatus.backgroundTintList = ColorStateList.valueOf(ContextCompat.getColor(context!!, backgroundColor))
    }

    private fun deselectAll() {
        for (index in 0..statusPlaceholder.childCount) {
            if (statusPlaceholder.getChildAt(index) is LinearLayout) {
                val linearLayout: LinearLayout = statusPlaceholder.getChildAt(index) as LinearLayout
                for (ind in 0..linearLayout.childCount) {
                    if (linearLayout.getChildAt(ind) is TextView) {
                        val textView: TextView =
                                linearLayout.getChildAt(ind) as TextView
                        setBackgroundColor(textView, AppPersistence.covidStatusCoding?.getOrNull(index)?.code)
                    }
                }
            }
        }
    }

    private fun showDatePicker() {
        val mYear = dateSelected!!.year
        val mMonth = dateSelected!!.monthOfYear.minus(1)
        val mDay = dateSelected!!.dayOfMonth
        val datePickerDialog = DatePickerDialog(context!!, { _, year, month, dayOfMonth ->
            dateSelected = DateTime(year, month + 1, dayOfMonth, dateSelected!!.hourOfDay, dateSelected!!.minuteOfHour)
            txtDateSelected.setText(dateSelectedFormatter.print(dateSelected))
        }, mYear, mMonth, mDay)

        datePickerDialog.datePicker.maxDate = DateTime.now()
            .millis
        datePickerDialog.show()
    }

    private fun showTimePicker() {
        val timePickerDialog = TimePickerDialog(
                activity, TimePickerDialog.OnTimeSetListener
        { _, hourOfDay, minute ->
            dateSelected = dateSelected!!.withHourOfDay(hourOfDay)!!
                .withMinuteOfHour(minute)
            txtTimeSelected?.setText(timeSelectedFormatter.print(dateSelected))
        },
                dateSelected!!.hourOfDay,
                dateSelected!!.minuteOfHour,
                true
                                               )
        timePickerDialog.show()
    }

    private fun updateProceedButton(codingItem: CodingItem?) {
        val code = codingItem?.code
        if (code == NOT_TESTED || code == TEST_REQUESTED) {
            btnProceed.text = getString(R.string.save)
            dateSelected = null
        }
        else {
            btnProceed.text = getString(R.string.next)
            dateSelected = DateTime.now()
        }
    }

    private fun createStatusRequest(): HealthStatus {
        return HealthStatus(
                id = healthStatusId,
                healthStatus = selectedItem?.code,
                testDate = dateSelected?.millis,
                lastUpdate = formatter.print(DateTime.now()),
                userId = MdocAppHelper.getInstance().userId)
    }

    companion object {
        fun newInstance(): CovidStatusChangeBottomSheetFragment {
            return CovidStatusChangeBottomSheetFragment()
        }

        const val POSITIVE = "TEST_IS_RESULT_POSITIVE"
        const val NEGATIVE = "TEST_RESULT_NEGATIVE"
        const val TEST_REQUESTED = "REQUESTED_TEST"
        const val NOT_TESTED = "NOT_TESTED"
    }
}
package de.mdoc.activities

import android.content.IntentFilter
import android.os.Bundle
import androidx.recyclerview.widget.ItemTouchHelper
import de.mdoc.R
import de.mdoc.constants.MdocConstants
import kotlinx.android.synthetic.main.activity_mdoc.*


class MdocActivity : MainActivity() {

    override fun getResourceId(): Int {
        return R.layout.activity_mdoc
    }

    override fun init(savedInstanceState: Bundle?) {
        handleClickListeners()

        val filter = IntentFilter()
        filter.addAction(MdocConstants.APPOINTMENTS_NUMBER)
        registerReceiver(notifyReceiver, filter)

        getNotifications()

        if (!resources.getBoolean(R.bool.phone)) {

            if (savedInstanceState != null) {
                selectedItemNumber = savedInstanceState.getInt(SELECTED_ITEM)
                isOpenMenu = savedInstanceState.getBoolean(IS_OPEN_MENU)
            } else {
                if (intent != null && intent.getStringExtra(MdocConstants.EXTRA) != null) {
                    data = intent.getStringExtra(MdocConstants.EXTRA)
                    if (data.isEmpty()) {
                        selectedItemNumber = MdocConstants.DASHBOARD_FRAGMENT
                    } else if (data == MdocConstants.CLINIC) {
                        selectedItemNumber = MdocConstants.MY_STAY_FRAGMENT
                    } else if (data == MdocConstants.QUESTIONNAIRES) {
                        selectedItemNumber = MdocConstants.QUESTIONNAIRE_FRAGMENT
                    }
                } else {
                    selectedItemNumber = MdocConstants.DASHBOARD_FRAGMENT
                }
            }

            val itemTouchHelper = ItemTouchHelper(simpleCallback)
            itemTouchHelper.attachToRecyclerView(recylerViewNotification)

            if (isOpenMenu) {
                openMenu()
            } else {
                collapseMenu()
            }

        }

        setActiveFragment(selectedItemNumber)
    }

}

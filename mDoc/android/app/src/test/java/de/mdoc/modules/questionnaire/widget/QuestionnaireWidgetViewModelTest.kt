package de.mdoc.modules.questionnaire.widget

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.mdoc.modules.testutils.*
import org.junit.Test

class QuestionnaireWidgetViewModelTest : ViewModelTest() {

    private val repository = mock<QuestionnaireWidgetRepository>()

    private val viewModel by lazy {
        QuestionnaireWidgetViewModel(repository)
    }

    private fun assertLoading() {
        with(viewModel) {
            assertTrue(isLoading)
            assertFalse(isShowLoadingError)
            assertNotSet(content)
            assertFalse(isShowContent)
            assertFalse(isShowNoQuestionnairesMessage)
        }
    }

    @Test
    fun shouldShowData() {
        val answer = whenever(repository.getData(any(), any())).buildAnswer()

        viewModel.loadData()
        assertLoading()

        answer.invokeFirst(
            listOf(
                mock<SectionData>(), mock<SectionData>(), mock<SectionData>()
            )
        )

        with(viewModel) {
            assertFalse(isLoading)
            assertFalse(isShowLoadingError)
            assertValueIs(content) {
                it.size == 3
            }
            assertTrue(isShowContent)
            assertFalse(isShowNoQuestionnairesMessage)
        }

    }

    @Test
    fun shouldShowNoData() {
        val answer = whenever(repository.getData(any(), any())).buildAnswer()

        viewModel.loadData()
        assertLoading()

        answer.invokeFirst(emptyList<SectionData>())

        with(viewModel) {
            assertFalse(isLoading)
            assertFalse(isShowLoadingError)
            assertNotSet(content)
            assertFalse(isShowContent)
            assertTrue(isShowNoQuestionnairesMessage)
        }

    }

    @Test
    fun shouldShowError() {
        val answer = whenever(repository.getData(any(), any())).buildAnswer()

        viewModel.loadData()
        assertLoading()

        answer.invokeSecond(Exception("Failed to load"))

        with(viewModel) {
            assertFalse(isLoading)
            assertTrue(isShowLoadingError)
            assertNotSet(content)
            assertFalse(isShowContent)
            assertFalse(isShowNoQuestionnairesMessage)
        }

    }

}
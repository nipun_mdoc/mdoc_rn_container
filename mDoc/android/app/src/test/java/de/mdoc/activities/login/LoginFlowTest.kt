package de.mdoc.activities.login

import android.content.Context
import com.nhaarman.mockitokotlin2.mock
import de.mdoc.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class LoginFlowTest {
    private lateinit var loginUtil: LoginUtil
    @Before
    fun setup() {
        loginUtil = LoginUtil(mock())
    }

    @Test
    fun extractCodeWhenUrlIsNull() {
        val expected = null
        val actual = loginUtil.extractCodeFromUrl("null")

        assertEquals(expected, actual)
    }

    @Test
    fun extractCodeWhenUrlIsValid() {
        val dummyURL =
                "mdocapp://mdocapp/?session_state=cd51d6ac-ddf3-4227-93bb-9a569e448eb2&code=2d871202-3197-4a0e-a79a-f155a113e195.cd51d6ac-ddf3-4227-93bb-9a569e448eb2.2174d044-7112-4e41-bfdd-4f16227925e7"
        val expected =
                "2d871202-3197-4a0e-a79a-f155a113e195.cd51d6ac-ddf3-4227-93bb-9a569e448eb2.2174d044-7112-4e41-bfdd-4f16227925e7"
        val actual = loginUtil.extractCodeFromUrl(dummyURL)

        assertEquals(expected, actual)
    }
    @Test
    fun extractCodeWhenUrlIsCorrupted() {
        val dummyURL =
                "mdocapp://mdocapp/?session_state=cd51d6ac-ddf3-4227-93bb-9adfsgs71202-3197-4a0e-a7"
        val expected =null
        val actual = loginUtil.extractCodeFromUrl(dummyURL)

        assertEquals(expected, actual)
    }
}
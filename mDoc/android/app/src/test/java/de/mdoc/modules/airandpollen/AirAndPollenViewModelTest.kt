package de.mdoc.modules.airandpollen

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.mdoc.modules.airandpollen.data.AirAndPollenRepository
import de.mdoc.modules.airandpollen.data.Date
import de.mdoc.modules.airandpollen.data.Region
import de.mdoc.modules.airandpollen.data.RegionSelector
import de.mdoc.modules.testutils.*
import de.mdoc.util.MdocAppHelper
import org.junit.Test

class AirAndPollenViewModelTest : ViewModelTest() {

    val mdocAppHelper = mock<MdocAppHelper>()
    val repository = mock<AirAndPollenRepository>()

    private fun AirAndPollenViewModel.assertLoading() {
        assertNotSet(dates)
        assertTrue(isInProgress)
        assertFalse(isError)
        assertFalse(isContentVisible)
        assertNotSet(selectedDate)
        assertNotSet(regionsForSelection)
        assertNotSet(selectedRegion)
        assertFalse(showNoRegionSelectedMessage)
        assertFalse(showAirAndPollenInfo)
        assertValueIs(infoList) { it.isEmpty() }
        assertNotSet(selectionIndex)
    }

    private fun createTestRegions(): List<Region> {
        return listOf(
            Region(
                id = 1,
                name = "First",
                today = listOf(mock(), mock(), mock(), mock()),
                tomorrow = listOf(mock(), mock(), mock(), mock()),
                dayAfterTomorrow = listOf(mock(), mock(), mock(), mock())
            ),
            Region(
                id = 2,
                name = "Second",
                today = listOf(mock(), mock(), mock(), mock()),
                tomorrow = listOf(mock(), mock(), mock(), mock()),
                dayAfterTomorrow = listOf(mock(), mock(), mock(), mock())
            ),
            Region(
                id = 3,
                name = "Third",
                today = listOf(mock(), mock(), mock(), mock()),
                tomorrow = listOf(mock(), mock(), mock(), mock()),
                dayAfterTomorrow = listOf(mock(), mock(), mock(), mock())
            )
        )
    }

    @Test
    fun shouldShowCorrectData() {
        whenever(mdocAppHelper.selectedPollenRegionId).thenReturn(null)
        val answer = whenever(repository.getAirAndPollen(any(), any())).buildAnswer()

        val viewModel = AirAndPollenViewModel(mdocAppHelper, repository)

        viewModel.assertLoading()

        val testRegions = createTestRegions()
        answer.invokeFirst(testRegions)

        with(viewModel) {
            assertValueIs(dates) {
                it.size == Date.values().size
            }
            assertFalse(isInProgress)
            assertFalse(isError)
            assertTrue(isContentVisible)
            assertValueIs(selectedDate) {
                it.date == Date.Today
            }
            assertValueIs(regionsForSelection) {
                it.map { it.getRegion()?.name }.toTypedArray().contentDeepEquals(
                    arrayOf(null, "First", "Second", "Third")
                ) && it[0] is RegionSelector.NoSelection
            }
            assertValue(selectedRegion, null)
            assertTrue(showNoRegionSelectedMessage)
            assertFalse(showAirAndPollenInfo)
            assertValueIs(infoList) { it.isEmpty() }
            assertValue(selectionIndex, 0)
        }

        viewModel.selectedRegion.set(testRegions[1])

        with(viewModel) {
            assertValue(selectionIndex, 2)
            assertTrue(showAirAndPollenInfo)
            assertValue(infoList, testRegions[1].today)
        }

        viewModel.selectedDate.set(viewModel.dates.get()[2])

        with(viewModel) {
            assertValue(selectedRegion, testRegions[1])
            assertTrue(showAirAndPollenInfo)
            assertValue(infoList, testRegions[1].dayAfterTomorrow)
        }

    }

    @Test
    fun shouldShowPreviousSelectedRegion() {
        whenever(mdocAppHelper.selectedPollenRegionId).thenReturn(3)
        val answer = whenever(repository.getAirAndPollen(any(), any())).buildAnswer()

        val viewModel = AirAndPollenViewModel(mdocAppHelper, repository)

        viewModel.assertLoading()

        val testRegions = createTestRegions()
        answer.invokeFirst(testRegions)

        with(viewModel) {
            assertValueIs(dates) {
                it.size == Date.values().size
            }
            assertFalse(isInProgress)
            assertFalse(isError)
            assertTrue(isContentVisible)
            assertValueIs(selectedDate) {
                it.date == Date.Today
            }
            assertValueIs(regionsForSelection) {
                it.map { it.getRegion()?.name }.toTypedArray().contentDeepEquals(
                    arrayOf(null, "First", "Second", "Third")
                ) && it[0] is RegionSelector.NoSelection
            }
            assertValue(selectedRegion, testRegions[2])
            assertFalse(showNoRegionSelectedMessage)
            assertTrue(showAirAndPollenInfo)
            assertValue(infoList, testRegions[2].today)
            assertValue(selectionIndex, 3)
        }

    }

    @Test
    fun shouldShowError() {
        val answer = whenever(repository.getAirAndPollen(any(), any())).buildAnswer()

        val viewModel = AirAndPollenViewModel(mdocAppHelper, repository)

        viewModel.assertLoading()

        answer.invokeSecond(Exception("Some error during"))

        with(viewModel) {
            assertNotSet(dates)
            assertFalse(isInProgress)
            assertTrue(isError)
            assertFalse(isContentVisible)
            assertNotSet(selectedDate)
            assertNotSet(regionsForSelection)
            assertNotSet(selectedRegion)
            assertFalse(showNoRegionSelectedMessage)
            assertFalse(showAirAndPollenInfo)
            assertValueIs(infoList) { it.isEmpty() }
            assertNotSet(selectionIndex)

        }
    }

}
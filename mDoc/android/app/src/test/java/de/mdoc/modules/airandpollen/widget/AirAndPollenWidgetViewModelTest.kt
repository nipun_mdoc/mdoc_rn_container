package de.mdoc.modules.airandpollen.widget

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.mdoc.modules.airandpollen.data.AirAndPollenRepository
import de.mdoc.modules.airandpollen.data.Region
import de.mdoc.modules.testutils.*
import de.mdoc.util.MdocAppHelper
import org.junit.Test


class AirAndPollenWidgetViewModelTest : ViewModelTest() {

    val mdocAppHelper = mock<MdocAppHelper>()
    val repository = mock<AirAndPollenRepository>()

    @Test
    fun shouldShowNoRegionSelectedIsItIsNotSaved() {
        whenever(mdocAppHelper.selectedPollenRegionId).thenReturn(null)

        val viewModel = AirAndPollenWidgetViewModel(mdocAppHelper, repository)
        viewModel.loadData()

        with(viewModel) {
            assertFalse(isInProgress)
            assertValue(selectedRegionName, "")
            assertFalse(isSelectedRegionVisible)
            assertTrue(isNoRegionSelectedMessageVisible)
            assertFalse(isPollenInfoVisible)
            assertFalse(isLoadingErrorVisible)
            assertFalse(isBottomSectionVisible)
            assertNull(pollenData)
        }
    }

    @Test
    fun shouldShowNoRegionSelectedIfSavedNotFoundInData() {
        whenever(mdocAppHelper.selectedPollenRegionId).thenReturn(1)
        val answer = whenever(repository.getAirAndPollen(any(), any())).buildAnswer()

        val viewModel = AirAndPollenWidgetViewModel(mdocAppHelper, repository)
        viewModel.loadData()

        viewModel.assertLoadingStatus()

        answer.invokeFirst(emptyList<Region>())

        with(viewModel) {
            assertFalse(isInProgress)
            assertValue(selectedRegionName, "")
            assertFalse(isSelectedRegionVisible)
            assertTrue(isNoRegionSelectedMessageVisible)
            assertFalse(isPollenInfoVisible)
            assertFalse(isLoadingErrorVisible)
            assertFalse(isBottomSectionVisible)
            assertNull(pollenData)
        }
    }

    private fun AirAndPollenWidgetViewModel.assertLoadingStatus() {
        assertTrue(isInProgress)
        assertValue(selectedRegionName, "-")
        assertTrue(isSelectedRegionVisible)
        assertFalse(isNoRegionSelectedMessageVisible)
        assertFalse(isPollenInfoVisible)
        assertFalse(isLoadingErrorVisible)
        assertTrue(isBottomSectionVisible)
        assertNull(pollenData)
    }

    @Test
    fun shouldShowLoadingError() {
        whenever(mdocAppHelper.selectedPollenRegionId).thenReturn(1)
        val answer = whenever(repository.getAirAndPollen(any(), any())).buildAnswer()

        val viewModel = AirAndPollenWidgetViewModel(mdocAppHelper, repository)
        viewModel.loadData()

        viewModel.assertLoadingStatus()

        answer.invokeSecond(Exception("Loading is failed"))

        with(viewModel) {
            assertFalse(isInProgress)
            assertValue(selectedRegionName, "")
            assertFalse(isSelectedRegionVisible)
            assertFalse(isNoRegionSelectedMessageVisible)
            assertFalse(isPollenInfoVisible)
            assertTrue(isLoadingErrorVisible)
            assertFalse(isBottomSectionVisible)
            assertNull(pollenData)
        }
    }

}
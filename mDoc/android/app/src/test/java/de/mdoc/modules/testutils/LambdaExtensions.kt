package de.mdoc.modules.testutils

import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer
import org.mockito.stubbing.OngoingStubbing

fun <T> OngoingStubbing<T>.buildAnswer(): AnswerBuilder<T> {
    return buildAnswer({})
}

fun <T> OngoingStubbing<T>.buildAnswer(setupMethod: AnswerBuilder<T>.() -> Unit): AnswerBuilder<T> {
    val answer = ExtendedAnswer(setupMethod)
    then(answer)
    return answer.builder
}

class AnswerBuilder<T> {

    internal lateinit var invocation: InvocationOnMock
    internal var isAnswerSet = false
    internal var answer: T? = null
    internal var exception: Throwable? = null

    fun <E> invokeFirst(answer: E) {
        invoke(0, answer)
    }

    fun <E> invokeSecond(answer: E) {
        invoke(1, answer)
    }

    fun <E> invokeThird(answer: E) {
        invoke(2, answer)
    }

    fun <E> invoke(index: Int, answer: E) {
        val lambda = invocation.arguments[index] as (E) -> Unit
        lambda.invoke(answer)
    }

    fun returnValue(answer: T) {
        isAnswerSet = true
        this.answer = answer
    }

    fun throwException(exception: Throwable) {
        this.exception = exception
    }

}

private class ExtendedAnswer<T>(
    val setupMethod: (AnswerBuilder<T>) -> Unit
) : Answer<T> {

    val builder = AnswerBuilder<T>()

    override fun answer(invocation: InvocationOnMock): T {
        builder.invocation = invocation
        setupMethod.invoke(builder)
        if (builder.exception != null) {
            throw builder.exception!!
        } else if (builder.isAnswerSet) {
            return builder.answer!!
        } else {
            return Unit as T
        }
    }

}
package de.mdoc.modules.files

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.mdoc.modules.files.data.FilesData
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.files.filter.FilesFilter
import de.mdoc.modules.testutils.*
import de.mdoc.pojo.CodingData
import de.mdoc.pojo.CodingItem
import de.mdoc.pojo.FileListItem
import de.mdoc.pojo.SearchFilesData
import org.junit.Test

class FilesViewModelTest : ViewModelTest() {

    private val filesRepository = mock<FilesRepository>()
    private val categories = arrayListOf(
        CodingItem("system", "K1", "Cat1"),
        CodingItem("system", "K2", "Cat2"),
        CodingItem("system", "K3", "Cat3"),
        CodingItem("system", "U", "Unknown")
    )
    private val codingData = CodingData().also {
        it.list = categories
        it.totalCount = categories.size
    }

    private val viewModel by lazy {
        FilesViewModel(filesRepository)
    }

    private fun file(category: String) = FileListItem(
        id = "",
        name = "",
        owner = "",
        path = "",
        extension = "",
        size = 0,
        title = "",
        category = category,
        cts = 0L,
        description = "",
        sharingList = null
    )

    @Test
    fun shouldShowData() {
        val builder = whenever(filesRepository.getFilesData(any(), any(), any())).buildAnswer()
        assertTrue(viewModel.isLoading)
        assertFalse(viewModel.isError)

        val files = arrayListOf(
            file("K1"),
            file("K2"),
            file("K2"),
            file("U")
        )
        builder.invokeFirst(FilesData(SearchFilesData().also { it.list = files }, codingData))

        assertFalse(viewModel.isLoading)
        assertFalse(viewModel.isError)
        assertFalse(viewModel.isNoData)
        assertValueIs(viewModel.files) {
            it.toTypedArray().contentEquals(files.toTypedArray())
        }

        viewModel.filter.set(
            FilesFilter(
                from = null,
                to = null,
                categories = listOf(categories[1] /* K2 */)
            )
        )
        assertValueIs(viewModel.files) {
            it.toTypedArray().contentEquals(arrayOf(files[1], files[2]))
        }
    }

    @Test
    fun shouldShowNoData() {
        val builder = whenever(filesRepository.getFilesData(any(), any(), any())).buildAnswer()
        assertTrue(viewModel.isLoading)
        assertFalse(viewModel.isError)

        val files = arrayListOf<FileListItem>()
        builder.invokeFirst(FilesData(SearchFilesData().also { it.list = files }, codingData))

        assertFalse(viewModel.isLoading)
        assertFalse(viewModel.isError)
        assertTrue(viewModel.isNoData)
        assertValueIs(viewModel.files) {
            it.isEmpty()
        }

    }

}
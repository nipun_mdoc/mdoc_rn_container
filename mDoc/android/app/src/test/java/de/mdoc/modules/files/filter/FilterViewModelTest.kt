package de.mdoc.modules.files.filter

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import de.mdoc.modules.files.data.FilesRepository
import de.mdoc.modules.testutils.*
import de.mdoc.pojo.CodingData
import de.mdoc.pojo.CodingItem
import org.joda.time.LocalDate
import org.junit.Test

class FilterViewModelTest : ViewModelTest() {

    private var filesRepository = mock<FilesRepository>()
    private var repository = FilterRepository(filesRepository)
    private lateinit var initialFilter: FilesFilter

    private val categories = arrayListOf(
        CodingItem("system", "K1", "Cat1"),
        CodingItem("system", "K2", "Cat2"),
        CodingItem("system", "K3", "Cat3"),
        CodingItem("system", "U", "Unknown")
    )

    private val viewModel by lazy {
        FilterViewModel(
            repository
        )
    }

    private fun FilterItem.assertIsAllCategories(isCheckedValue: Boolean) {
        assert(category == FilterCategory.AllCategories)
        assert(this.isChecked == isCheckedValue)
    }

    private fun FilterItem.assertIsSingleCategory(
        codingItem: CodingItem,
        isCheckedValue: Boolean
    ) {
        assert(category is FilterCategory.SingleCategory)
        assert((category as FilterCategory.SingleCategory).data == codingItem)
        assert(this.isChecked == isCheckedValue)
    }

    private fun assertListChecked(vararg isChecked: Boolean) {
        val list = viewModel.categoryFilterItems.get()
        assert(list.size == categories.size + 1)
        assert(list.size == isChecked.size)
        list.forEachIndexed { index, filterItem ->
            if (index == 0) {
                filterItem.assertIsAllCategories(isChecked[index])
            } else {
                filterItem.assertIsSingleCategory(categories[index - 1], isChecked[index])
            }
        }

    }

    @Test
    fun shouldHandleFilters() {
        val from = LocalDate.now().minusMonths(4)
        val to = LocalDate.now().minusMonths(2)
        whenever(filesRepository.getCategoryCoding(any(), any())).buildAnswer {
            invokeFirst(CodingData().also {
                it.list = categories
                it.totalCount = categories.size
            })
        }
        initialFilter = FilesFilter(
            categories = listOf(
                categories[1], categories[2]
            ),
            from = from,
            to = to
        )

        assertTrue(viewModel.isClearFilterButtonEnabled)
        assertValue(viewModel.from, from)
        assertValue(viewModel.to, to)
        assertListChecked(false, false, true, true, false)

        viewModel.onCategoryClicked(viewModel.categoryFilterItems.get()[3].category)
        assertListChecked(false, false, true, false, false)
        viewModel.onCategoryClicked(viewModel.categoryFilterItems.get()[2].category)
        assertListChecked(true, false, false, false, false)
        viewModel.onCategoryClicked(viewModel.categoryFilterItems.get()[0].category)
        assertListChecked(true, false, false, false, false)
        viewModel.onCategoryClicked(viewModel.categoryFilterItems.get()[1].category)
        assertListChecked(false, true, false, false, false)

        val newFrom = LocalDate.now().minusMonths(3)
        val newTo = LocalDate.now().minusMonths(1)
        viewModel.from.set(newFrom)
        viewModel.to.set(newTo)
        viewModel.getFilter().let {
            assert(
                it.categories!!.toTypedArray().contentDeepEquals(
                    arrayOf(categories[0])
                )
            )
            assert(it.from == newFrom)
            assert(it.to == newTo)
        }

        viewModel.onDeleteAllFiltersClicked()

        assertFalse(viewModel.isClearFilterButtonEnabled)
        assertListChecked(true, false, false, false, false)
        assertValue(viewModel.from, null)
        assertValue(viewModel.to, null)

        viewModel.getFilter().let {
            assert(it.categories == null)
            assert(it.from == null)
            assert(it.to == null)
        }
    }

}
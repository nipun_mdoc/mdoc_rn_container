package de.mdoc.modules.testutils

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule

open class ViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

}
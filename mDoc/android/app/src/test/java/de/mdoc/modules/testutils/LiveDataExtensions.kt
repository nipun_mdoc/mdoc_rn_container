package de.mdoc.modules.testutils

import de.mdoc.viewmodel.ValueLiveData

fun assertTrue(data: ValueLiveData<Boolean>) {
    assert(data.get())
}

fun assertFalse(data: ValueLiveData<Boolean>) {
    assert(data.get().not())
}

fun <T> assertValue(data: ValueLiveData<T>, expected: T) {
    assert(data.get() == expected)
}

fun <T> assertValueIs(data: ValueLiveData<T>, expectedCheck: (T) -> Boolean) {
    assert(expectedCheck.invoke(data.get()))
}

fun <T> assertNull(data: ValueLiveData<T>) {
    assert(data.get() == null)
}

fun <T> assertNotNull(data: ValueLiveData<T>) {
    assert(data.get() != null)
}

fun <T> assertSet(data: ValueLiveData<T>) {
    assert(data.isSet())
}

fun <T> assertNotSet(data: ValueLiveData<T>) {
    assert(data.isSet().not())
}
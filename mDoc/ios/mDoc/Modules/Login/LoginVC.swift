//
//  LoginVC.swift
//  mDoc
//
//  Created by Sushil K Mishra on 31/05/21.
//

import UIKit

class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
  @IBAction func nextButton(_ sender: Any) {
    self.performSegue(withIdentifier: String(describing: SecondVC.self), sender: self)
  }
  @IBAction func dismissVC(_ sender: Any) {
    if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
      appDelegate.goToReactNative()
    }
  }
  
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

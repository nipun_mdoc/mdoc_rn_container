/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
    SafeAreaView,
    ScrollView,
    StatusBar,
    StyleSheet,
    Text,
    View,
    TouchableOpacity
} from 'react-native';

import {
    Colors,
    DebugInstructions,
    Header,
    LearnMoreLinks,
    ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import TestConnectNative from "./src/TestConnectNative";


const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
    backgroundStyle: {
        backgroundColor: Colors.darker
    }
});

export default class App extends Component {
  constructor(props) {
    super(props);
    rootTag = this.props.rootTag;
  }

    render() {
        return (
            <SafeAreaView style={styles.backgroundStyle}>
                <View style={{backgroundColor: "#FFFFFF"}}>
                    <Text>This is page navigated from native to react native {this.props.app_name}</Text>
                    <TouchableOpacity
                        style={{height: 100, width: 100, backgroundColor: 'gray'}}
                        onPress={() => {
                            // TestConnectNative.sendMessage("sushil K Mishra");
                            TestConnectNative.goToNative(this.props.rootTag);
                        }}
                    >
                        <Text>Click Me</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        );
    }
}
